
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object error403_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class error403 extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.1*/("""<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>AutoSherpa</title>
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />

    <!-- Bootstrap -->
   <link rel="stylesheet" href=""""),_display_(/*10.34*/routes/*10.40*/.Assets.at("css/bootstrap.min.css")),format.raw/*10.75*/("""">
   <link rel="stylesheet" href=""""),_display_(/*11.34*/routes/*11.40*/.Assets.at("css/AdminLTE.min.css")),format.raw/*11.74*/("""">
   <link rel="stylesheet" href=""""),_display_(/*12.34*/routes/*12.40*/.Assets.at("css/font-awesome.min.css")),format.raw/*12.78*/("""">

  </head>
  <body class="error" style="background-color:#ecf0f5">
    <!-- <div class="container">
      <div class="col-lg-8 col-lg-offset-2 text-center">
        <div class="logo">
          <h1></h1>
        </div>
        <p class="lead text-muted">Oops, an error has occurred. Forbidden!</p>
        <div class="clearfix"></div>
        
        <div class="clearfix"></div>
        <br>
        <div class="col-lg-6 col-lg-offset-3">
          <div class="btn-group btn-group-justified">
            <a href="/" class="btn btn-warning">Home</a>
             </div>
        </div>
      </div>
    </div> -->
	 <section class="content">

      <div class="">
        <h2 class="headline text-yellow"> </h2>

        <div class="col-lg-8 col-lg-offset-2 text-center">
          <h2><i class="fa fa-warning text-yellow"></i> Session Expired! </h2>

          <p>
            We could not find the page you were looking for.
            Meanwhile, you may<br /> <a href="/"><b><u>Return To Home</u></b></a> 
          </p>

          
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
  </body>
</html>


"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object error403 extends error403_Scope0.error403
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:26 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/error403.scala.html
                  HASH: e25d93d15da91d7d0f6e8113b9c18023d51b844f
                  MATRIX: 833->0|1167->307|1182->313|1238->348|1302->385|1317->391|1372->425|1436->462|1451->468|1510->506
                  LINES: 32->1|41->10|41->10|41->10|42->11|42->11|42->11|43->12|43->12|43->12
                  -- GENERATED --
              */
          