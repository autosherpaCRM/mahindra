
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object psfList30thForFeedBackOfService_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class psfList30thForFeedBackOfService extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealercode:String,dealerName:String,userName:String,oem:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.66*/("""

"""),_display_(/*3.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*3.65*/ {_display_(Seq[Any](format.raw/*3.67*/("""

"""),format.raw/*5.1*/("""<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">  PSF List For 30th Day FeedBack</div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li id="psf6thData" class="active"><a href="#home" data-toggle="tab" id="Tab1" value="psf30thday" onclick="assignedInteractionPSFData(result.name);" >30th Day PSF</a> </li>
                    <li><a href="#profile" data-toggle="tab" id="Tab2" onclick="ajaxCallForFollowUpRequiredPSFData();">Follow Up Required</a> </li>
                    <!-- <li><a href="#messages" data-toggle="tab" id="Tab3" >Incompleted Survey</a> </li> -->
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxCallForCompletedSurveyPSFData();" >Completed Survey</a> </li>
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxCallForNonContactsPSFData();">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxCallForDroppedCallsPSFData();">Dropped</a> </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="assignedInteractionPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>                                    
                                            <th>Vehicle RegNo.</th>
                                            <th>Model</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="tab-pane fade" id="profile">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForFollowUpRequiredPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>                                    
                                            <th>Vehicle RegNo.</th>
                                            <th>FollowUp Date</th>                                    
                                            <th>FollowUp Time</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>






                    <div class="tab-pane fade" id="messages">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForIncompletedSurveyPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>                                    
                                            <th>Vehicle RegNo.</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="tab-pane fade" id="settings">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForCompletedSurveyPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>                                    
                                            <th>Vehicle RegNo.</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="tab-pane fade" id="nonContacts">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForNonContactsPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>                                    
                                            <th>Vehicle RegNo.</th>
                                             <th>Last Disposition</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
                    
                    <div class="tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForDroppedCallsPSFData">
                                    <thead>
                                        <tr>
                                            <th>Customer Name</th>
                                            <th>Vehicle RegNo.</th>
                                            <th>Last Disposition</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>

                    <!-- /.panel --> 
                </div>
                <!-- /.col-lg-12 --> 
            </div>
        </div>
    </div>
</div>

""")))}),format.raw/*164.2*/("""


"""),format.raw/*167.1*/("""<script type="text/javascript">

window.onload = ajaxRequestPSFDataPageWithTabFor30th;
</script>



<script type="text/javascript">
var result = """),format.raw/*175.14*/("""{"""),format.raw/*175.15*/("""name:'psf30thday'"""),format.raw/*175.32*/("""}"""),format.raw/*175.33*/(""";
    function assignedInteractionPSFData(name) """),format.raw/*176.47*/("""{"""),format.raw/*176.48*/("""
        """),format.raw/*177.9*/("""$('#assignedInteractionPSFData').dataTable("""),format.raw/*177.52*/("""{"""),format.raw/*177.53*/("""
            """),format.raw/*178.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/assignedInteractionTablePSFData"+"/"+name+""
        """),format.raw/*186.9*/("""}"""),format.raw/*186.10*/("""),

         $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*188.66*/("""{"""),format.raw/*188.67*/("""
            """),format.raw/*189.13*/("""$($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
      """),format.raw/*191.7*/("""}"""),format.raw/*191.8*/(""");

    """),format.raw/*193.5*/("""}"""),format.raw/*193.6*/("""


    """),format.raw/*196.5*/("""function ajaxCallForFollowUpRequiredPSFData() """),format.raw/*196.51*/("""{"""),format.raw/*196.52*/("""
    	"""),format.raw/*197.6*/("""var psfDay ='psf30thday';
        //alert("follow up server side data");
        $('#ajaxCallForFollowUpRequiredPSFData').dataTable("""),format.raw/*199.60*/("""{"""),format.raw/*199.61*/("""
            """),format.raw/*200.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForFollowUpRequiredPSFData"+"/"+psfDay+""
        """),format.raw/*208.9*/("""}"""),format.raw/*208.10*/(""");

    """),format.raw/*210.5*/("""}"""),format.raw/*210.6*/("""



    """),format.raw/*214.5*/("""function ajaxCallForIncompletedSurveyPSFData() """),format.raw/*214.52*/("""{"""),format.raw/*214.53*/("""
    	"""),format.raw/*215.6*/("""var psfDay ='psf30thday';
        $('#ajaxCallForIncompletedSurveyPSFData').dataTable("""),format.raw/*216.61*/("""{"""),format.raw/*216.62*/("""
            """),format.raw/*217.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForIncompletedSurveyPSFData"+"/"+psfDay+""
        """),format.raw/*225.9*/("""}"""),format.raw/*225.10*/(""");

    """),format.raw/*227.5*/("""}"""),format.raw/*227.6*/("""

    """),format.raw/*229.5*/("""function ajaxCallForCompletedSurveyPSFData() """),format.raw/*229.50*/("""{"""),format.raw/*229.51*/("""
    	"""),format.raw/*230.6*/("""var psfDay ='psf30thday';
        $('#ajaxCallForCompletedSurveyPSFData').dataTable("""),format.raw/*231.59*/("""{"""),format.raw/*231.60*/("""
            """),format.raw/*232.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForCompletedSurveyPSFData"+"/"+psfDay+""
        """),format.raw/*240.9*/("""}"""),format.raw/*240.10*/(""");


    """),format.raw/*243.5*/("""}"""),format.raw/*243.6*/("""

    """),format.raw/*245.5*/("""function ajaxCallForNonContactsPSFData() """),format.raw/*245.46*/("""{"""),format.raw/*245.47*/("""
    	"""),format.raw/*246.6*/("""var psfDay ='psf30thday';
        $('#ajaxCallForNonContactsPSFData').dataTable("""),format.raw/*247.55*/("""{"""),format.raw/*247.56*/("""
            """),format.raw/*248.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForNonContactsPSFData"+"/"+psfDay+""
        """),format.raw/*256.9*/("""}"""),format.raw/*256.10*/(""");
    """),format.raw/*257.5*/("""}"""),format.raw/*257.6*/("""


    """),format.raw/*260.5*/("""function ajaxCallForDroppedCallsPSFData() """),format.raw/*260.47*/("""{"""),format.raw/*260.48*/("""
    	"""),format.raw/*261.6*/("""var psfDay ='psf30thday';
        $('#ajaxCallForDroppedCallsPSFData').dataTable("""),format.raw/*262.56*/("""{"""),format.raw/*262.57*/("""
            """),format.raw/*263.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForDroppedCallsPSFData"+"/"+psfDay+""
        """),format.raw/*271.9*/("""}"""),format.raw/*271.10*/(""");

    """),format.raw/*273.5*/("""}"""),format.raw/*273.6*/("""

"""),format.raw/*275.1*/("""</script>
"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem)

  def f:((String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem) => apply(dealercode,dealerName,userName,oem)

  def ref: this.type = this

}


}

/**/
object psfList30thForFeedBackOfService extends psfList30thForFeedBackOfService_Scope0.psfList30thForFeedBackOfService
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:36 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/psfList30thForFeedBackOfService.scala.html
                  HASH: 86bc840ecf5e6d5280bf7e7d23e5c1f68b8d9759
                  MATRIX: 818->1|977->65|1007->70|1078->133|1117->135|1147->139|9308->8269|9342->8275|9524->8428|9554->8429|9600->8446|9630->8447|9708->8496|9738->8497|9776->8507|9848->8550|9878->8551|9921->8565|10232->8848|10262->8849|10362->8920|10392->8921|10435->8935|10545->9017|10574->9018|10612->9028|10641->9029|10679->9039|10754->9085|10784->9086|10819->9093|10982->9227|11012->9228|11055->9242|11371->9530|11401->9531|11439->9541|11468->9542|11508->9554|11584->9601|11614->9602|11649->9609|11765->9696|11795->9697|11838->9711|12155->10000|12185->10001|12223->10011|12252->10012|12288->10020|12362->10065|12392->10066|12427->10073|12541->10158|12571->10159|12614->10173|12929->10460|12959->10461|12999->10473|13028->10474|13064->10482|13134->10523|13164->10524|13199->10531|13309->10612|13339->10613|13382->10627|13693->10910|13723->10911|13759->10919|13788->10920|13826->10930|13897->10972|13927->10973|13962->10980|14073->11062|14103->11063|14146->11077|14458->11361|14488->11362|14526->11372|14555->11373|14587->11377
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|195->164|198->167|206->175|206->175|206->175|206->175|207->176|207->176|208->177|208->177|208->177|209->178|217->186|217->186|219->188|219->188|220->189|222->191|222->191|224->193|224->193|227->196|227->196|227->196|228->197|230->199|230->199|231->200|239->208|239->208|241->210|241->210|245->214|245->214|245->214|246->215|247->216|247->216|248->217|256->225|256->225|258->227|258->227|260->229|260->229|260->229|261->230|262->231|262->231|263->232|271->240|271->240|274->243|274->243|276->245|276->245|276->245|277->246|278->247|278->247|279->248|287->256|287->256|288->257|288->257|291->260|291->260|291->260|292->261|293->262|293->262|294->263|302->271|302->271|304->273|304->273|306->275
                  -- GENERATED --
              */
          