
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object psfList15ForFeedBackOfService_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class psfList15ForFeedBackOfService extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealercode:String,dealerName:String,userName:String,oem:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.66*/("""

"""),_display_(/*3.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*3.65*/ {_display_(Seq[Any](format.raw/*3.67*/("""

"""),format.raw/*5.1*/("""<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">  PSF List For 15th Day FeedBack</div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li id="psf6thData" class="active"><a href="#home" data-toggle="tab" id="Tab1" value="psf15thday" onclick="assignedInteractionPSFData(result.name);" >15th Day PSF</a> </li>
                    <li><a href="#profile" data-toggle="tab"  id="Tab2" onclick="ajaxCallForFollowUpRequiredPSFData();">Follow Up Required</a> </li>
                    <!-- <li><a href="#messages" data-toggle="tab" id="Tab3" >Incompleted Survey</a> </li> -->
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxCallForCompletedSurveyPSFData();" >Completed Survey</a> </li>
                   <!--<li><a href="#appointments" data-toggle="tab" id="Tab8" onclick="ajaxCallForAppointmentPSFData();">Appointments</a></li>--> 
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxCallForNonContactsPSFData();">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxCallForDroppedCallsPSFData();">Dropped</a> </li>
                   <!--  <li><a href="#incompleteSurvey" data-toggle="tab" id="Tab7" onclick="ajaxCallForIncompletedSurveyPSFData();">Incomplete Survey</a></li> -->
                </ul>
                <div class="tab-content">
				<div class="panel panel-default" style="border-top: #fff;">
				<div class="panel-body">
                       <div class="row">
					<div class="col-md-3" id="SchedulefromBillDate" style="display:none">
					<label>From Bill Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="Enter From Bill Date" data-column-index="1" id="frombilldaterange" name="frombilldaterange" readonly>
				  </div>
				   <div class="col-md-3" id="ScheduletoBillDate" style="display:none">
					<label>To Bill Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="Enter To Bill Date" data-column-index="2" id="tobilldaterange" name="tobilldaterange" readonly>
				  </div>
				
				</div>
				    <div class="row" id="forschedule">
					<div class="col-md-3" id="fromCallDate" style="display:none">
					<label>From Call Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="Enter From Call Date" data-column-index="1" id="fromCalldaterange22" name="frombilldaterange22" readonly>
				  </div>
				   <div class="col-md-3" id="toCallDate" style="display:none">
					<label>To Call Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="Enter To call Date"data-column-index="2" id="tobCalldaterange33" name="tobilldaterange33" readonly>
				  </div>
				</div>
				</div>
				</div>
                    <div class="panel panel-default tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="assignedInteractionPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th> 
                                            <th>Mobile Number</th>                                   
                                            <th>Vehicle RegNo.</th>
                                            <th>Model</th>
                                            <th>RO Number</th>
                                            <th>RO Date</th>
                                            <th>Bill Date</th>
                                            <th>Category</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="panel panel-default tab-pane fade" id="profile">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForFollowUpRequiredPSFData">
                                    <thead>
                                        <tr>                                    
											<th>FollowUp Date</th>
											<th>FollowUp Time</th>
											<th>call_date</th>
											<th>Customer Name</th>
											<th>Mobile Number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>






                    <div class="panel panel-default tab-pane fade" id="messages">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForIncompletedSurveyPSFData">
                                    <thead>
                                        <tr>                                    
											<th>FollowUp Date</th>
											<th>FollowUp Time</th>
											<th>call_date</th>
											<th>Customer Name</th>
											<th>Mobile Number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="panel panel-default tab-pane fade" id="settings">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForCompletedSurveyPSFData">
                                    <thead>
                                        <tr>                                    
                                           	<th>Survey Date</th>																	
											<th>Customer Name</th>																	
											<th>Category</th>																	
											<th>vehicle RegNo</th>																	
											<th>Model</th>																	
											<th>RO Date</th>																	
											<th>RO Number</th>																	
											<th>Bill Date</th>
											<th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="panel panel-default tab-pane fade" id="nonContacts">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForNonContactsPSFDataCalls">
                                    <thead>
                                        <tr>                                   
											<th>Customer Name</th>
											<th>Category</th>
											<th>Mobile number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
											<th>Last Disposition</th>
											<th>Call Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
                    <div class="panel panel-default tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForDroppedCallsPSFData">
                                    <thead>
                                         <tr>                                    
                                            <th>Customer Name</th>
											<th>Category</th>
											<th>Mobile number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
											<th>Last Disposition</th>
											<th>Call Date</th>
											
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
 <div class="panel panel-default tab-pane fade" id="appointments">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForAppointmentPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>
											<th>Vehicle RegNo</th>
											<th>Appointment Date</th>
											<th>Mobile number</th>
											<th>Call Date</th>
											<th>Appointment Time</th>
											<th>RO Number</th>
											<th>RO Date</th>
											<th>Bill Date</th>
											<th>Survey date</th>
											<th>Model</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
                    <!-- /.panel --> 
                </div>
                <!-- /.col-lg-12 --> 
            </div>
        </div>
    </div>
</div>

""")))}),format.raw/*256.2*/("""
"""),format.raw/*257.1*/("""<script type="text/javascript">

window.onload = ajaxRequestPSFDataPageWithTabFor15th;
</script>
<script>

function FilterOptionData(table)"""),format.raw/*263.33*/("""{"""),format.raw/*263.34*/("""
	"""),format.raw/*264.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*265.39*/("""{"""),format.raw/*265.40*/("""
    	"""),format.raw/*266.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
		console.log(v);
		console.log(i);
	    
	    if(v.length>0)"""),format.raw/*272.20*/("""{"""),format.raw/*272.21*/("""
		    	   
		   """),format.raw/*274.6*/("""values[i] = v;
	    
	    """),format.raw/*276.6*/("""}"""),format.raw/*276.7*/("""
	    
	    	"""),format.raw/*278.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');

	 	 if(values.length>0) """),format.raw/*283.25*/("""{"""),format.raw/*283.26*/("""
			 """),format.raw/*284.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*284.37*/("""{"""),format.raw/*284.38*/("""	
			    """),format.raw/*285.8*/("""table.api().columns(i).search(values[i]);
			 """),format.raw/*286.5*/("""}"""),format.raw/*286.6*/("""
			 """),format.raw/*287.5*/("""table.api().draw();
			 """),format.raw/*288.5*/("""}"""),format.raw/*288.6*/(""" 
		 	  
	"""),format.raw/*290.2*/("""}"""),format.raw/*290.3*/(""");
	
	
"""),format.raw/*293.1*/("""}"""),format.raw/*293.2*/("""
"""),format.raw/*294.1*/("""</script>
<script type="text/javascript">
var result = """),format.raw/*296.14*/("""{"""),format.raw/*296.15*/("""name:'psf15thday'"""),format.raw/*296.32*/("""}"""),format.raw/*296.33*/(""";

    function assignedInteractionPSFData(name) """),format.raw/*298.47*/("""{"""),format.raw/*298.48*/("""
        """),format.raw/*299.9*/("""//alert("server side data load");
        $("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "block";
    	document.getElementById("ScheduletoBillDate").style.display = "block";
    	document.getElementById("fromCallDate").style.display = "none";
    	document.getElementById("toCallDate").style.display = "none";
       var table= $('#assignedInteractionPSFData').dataTable("""),format.raw/*308.62*/("""{"""),format.raw/*308.63*/("""
            """),format.raw/*309.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/assignedInteractionTablePSFData"+"/"+name+""
        """),format.raw/*317.9*/("""}"""),format.raw/*317.10*/(""");
       FilterOptionData(table);
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*319.65*/("""{"""),format.raw/*319.66*/("""
           """),format.raw/*320.12*/("""$($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust();
     """),format.raw/*322.6*/("""}"""),format.raw/*322.7*/(""");

    """),format.raw/*324.5*/("""}"""),format.raw/*324.6*/("""


    """),format.raw/*327.5*/("""function ajaxCallForFollowUpRequiredPSFData(name) """),format.raw/*327.55*/("""{"""),format.raw/*327.56*/("""
    	"""),format.raw/*328.6*/("""var psfDay ='psf15thday';
    	$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
        //alert("follow up server side data");
       var table =  $('#ajaxCallForFollowUpRequiredPSFData').dataTable("""),format.raw/*338.72*/("""{"""),format.raw/*338.73*/("""
            """),format.raw/*339.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForFollowUpRequiredPSFData"+"/"+psfDay+""
        """),format.raw/*347.9*/("""}"""),format.raw/*347.10*/(""");
       FilterOptionData(table);
    """),format.raw/*349.5*/("""}"""),format.raw/*349.6*/("""



    """),format.raw/*353.5*/("""function ajaxCallForIncompletedSurveyPSFData(name) """),format.raw/*353.56*/("""{"""),format.raw/*353.57*/("""
    	"""),format.raw/*354.6*/("""var psfDay ='psf15thday';
      	$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
       var table = $('#ajaxCallForIncompletedSurveyPSFData').dataTable("""),format.raw/*363.72*/("""{"""),format.raw/*363.73*/("""
            """),format.raw/*364.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForIncompletedSurveyPSFData"+"/"+psfDay+""
        """),format.raw/*372.9*/("""}"""),format.raw/*372.10*/(""");
       FilterOptionData(table);
    """),format.raw/*374.5*/("""}"""),format.raw/*374.6*/("""

    """),format.raw/*376.5*/("""function ajaxCallForCompletedSurveyPSFData(name) """),format.raw/*376.54*/("""{"""),format.raw/*376.55*/("""
    	"""),format.raw/*377.6*/("""var psfDay ='psf15thday';
    	$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
        var table = $('#ajaxCallForCompletedSurveyPSFData').dataTable("""),format.raw/*386.71*/("""{"""),format.raw/*386.72*/("""
            """),format.raw/*387.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForCompletedSurveyPSFData"+"/"+psfDay+""
        """),format.raw/*395.9*/("""}"""),format.raw/*395.10*/(""");

        FilterOptionData(table);
    """),format.raw/*398.5*/("""}"""),format.raw/*398.6*/("""

    """),format.raw/*400.5*/("""function ajaxCallForAppointmentPSFData(name) """),format.raw/*400.50*/("""{"""),format.raw/*400.51*/("""
    	"""),format.raw/*401.6*/("""$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
  
        
    	 var psfDay = 'psf15stday'; 
        var table = $('#ajaxCallForAppointmentPSFData').dataTable("""),format.raw/*412.67*/("""{"""),format.raw/*412.68*/("""
            """),format.raw/*413.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForAppointmentPSFData"+"/"+psfDay+""
        """),format.raw/*421.9*/("""}"""),format.raw/*421.10*/(""");
        FilterOptionData(table);
    """),format.raw/*423.5*/("""}"""),format.raw/*423.6*/("""
    
    """),format.raw/*425.5*/("""function ajaxCallForNonContactsPSFData(name) """),format.raw/*425.50*/("""{"""),format.raw/*425.51*/("""
    	"""),format.raw/*426.6*/("""var psfDay ='psf15thday';
    	$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
        var table = $('#ajaxCallForNonContactsPSFDataCalls').dataTable("""),format.raw/*435.72*/("""{"""),format.raw/*435.73*/("""
            """),format.raw/*436.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForNonContactsPSFData"+"/"+psfDay+""
        """),format.raw/*444.9*/("""}"""),format.raw/*444.10*/(""");
        FilterOptionData(table);
    """),format.raw/*446.5*/("""}"""),format.raw/*446.6*/("""


    """),format.raw/*449.5*/("""function ajaxCallForDroppedCallsPSFData(name) """),format.raw/*449.51*/("""{"""),format.raw/*449.52*/("""
    	"""),format.raw/*450.6*/("""var psfDay ='psf15thday';
    	$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
        var table = $('#ajaxCallForDroppedCallsPSFData').dataTable("""),format.raw/*459.68*/("""{"""),format.raw/*459.69*/("""
            """),format.raw/*460.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForDroppedCallsPSFData"+"/"+psfDay+""
        """),format.raw/*468.9*/("""}"""),format.raw/*468.10*/(""");
        FilterOptionData(table);
    """),format.raw/*470.5*/("""}"""),format.raw/*470.6*/("""

"""),format.raw/*472.1*/("""</script>
"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem)

  def f:((String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem) => apply(dealercode,dealerName,userName,oem)

  def ref: this.type = this

}


}

/**/
object psfList15ForFeedBackOfService extends psfList15ForFeedBackOfService_Scope0.psfList15ForFeedBackOfService
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:36 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/psfList15ForFeedBackOfService.scala.html
                  HASH: fa54f1c7929c8a0b5d8d59bfb3fb0d1d75a8a55f
                  MATRIX: 814->1|973->65|1003->70|1074->133|1113->135|1143->139|13330->12295|13360->12297|13534->12442|13564->12443|13595->12446|13680->12502|13710->12503|13745->12510|13956->12692|13986->12693|14033->12712|14089->12740|14118->12741|14161->12756|14362->12928|14392->12929|14426->12935|14487->12967|14517->12968|14555->12978|14630->13025|14659->13026|14693->13032|14746->13057|14775->13058|14815->13070|14844->13071|14882->13081|14911->13082|14941->13084|15027->13141|15057->13142|15103->13159|15133->13160|15213->13211|15243->13212|15281->13222|15865->13777|15895->13778|15938->13792|16249->14075|16279->14076|16409->14177|16439->14178|16481->14191|16589->14271|16618->14272|16656->14282|16685->14283|16723->14293|16802->14343|16832->14344|16867->14351|17498->14953|17528->14954|17571->14968|17887->15256|17917->15257|17986->15298|18015->15299|18055->15311|18135->15362|18165->15363|18200->15370|18785->15926|18815->15927|18858->15941|19175->16230|19205->16231|19274->16272|19303->16273|19339->16281|19417->16330|19447->16331|19482->16338|20064->16891|20094->16892|20137->16906|20452->17193|20482->17194|20554->17238|20583->17239|20619->17247|20693->17292|20723->17293|20758->17300|21352->17865|21382->17866|21425->17880|21736->18163|21766->18164|21836->18206|21865->18207|21905->18219|21979->18264|22009->18265|22044->18272|22627->18826|22657->18827|22700->18841|23011->19124|23041->19125|23111->19167|23140->19168|23178->19178|23253->19224|23283->19225|23318->19232|23897->19782|23927->19783|23970->19797|24282->20081|24312->20082|24382->20124|24411->20125|24443->20129
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|287->256|288->257|294->263|294->263|295->264|296->265|296->265|297->266|303->272|303->272|305->274|307->276|307->276|309->278|314->283|314->283|315->284|315->284|315->284|316->285|317->286|317->286|318->287|319->288|319->288|321->290|321->290|324->293|324->293|325->294|327->296|327->296|327->296|327->296|329->298|329->298|330->299|339->308|339->308|340->309|348->317|348->317|350->319|350->319|351->320|353->322|353->322|355->324|355->324|358->327|358->327|358->327|359->328|369->338|369->338|370->339|378->347|378->347|380->349|380->349|384->353|384->353|384->353|385->354|394->363|394->363|395->364|403->372|403->372|405->374|405->374|407->376|407->376|407->376|408->377|417->386|417->386|418->387|426->395|426->395|429->398|429->398|431->400|431->400|431->400|432->401|443->412|443->412|444->413|452->421|452->421|454->423|454->423|456->425|456->425|456->425|457->426|466->435|466->435|467->436|475->444|475->444|477->446|477->446|480->449|480->449|480->449|481->450|490->459|490->459|491->460|499->468|499->468|501->470|501->470|503->472
                  -- GENERATED --
              */
          