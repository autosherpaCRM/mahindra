
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object viewReport_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class viewReport extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,Form[WyzUser],List[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealerName:String,userName:String,form:Form[WyzUser],allSE:List[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import models._
import helper._
import scala._

Seq[Any](format.raw/*1.76*/("""
"""),_display_(/*5.2*/mainPageSalesManger("AutoSherpaCRM",userName,dealerName)/*5.58*/ {_display_(Seq[Any](format.raw/*5.60*/("""

"""),format.raw/*7.1*/("""<div class="panel panel-default">
  <div class="panel-heading">Chart Reports</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-5"> 
        <!-- LINE CHART -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Pi Chart</h3>
          </div>
          <div class="box-body">
            <canvas id="pieChartForSalesMan" style="height:250px"></canvas>
          </div>        
        </div><!-- /.box --> 
      </div>
      <div class="col-md-5"> 
        <!-- AREA CHART -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Calls made vs Test Drive</h3>
          </div>
          <div class="box-body">
            <div class="chart">
              <canvas id="areaChartForSalesMan" style="height:250px"></canvas>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-2"> 
        <!-- AREA CHART -->
        <div class="box box-primary">
          <div class="panel panel-default">
            <div class="panel-heading"> <a href="#" class="pull-right"> <em class="icon-plus text-muted"></em> </a>Contacts</div>
            <div class="list-group"> 
              <!-- START User status--> 
              <a href="#" class="media p mt0 list-group-item"> <span class="pull-right"> <span class="circle circle-success circle-lg"></span> </span> <span class="media-body"> <span class="media-heading"> <strong>"""),_display_(/*42.216*/{userName}),format.raw/*42.226*/("""</strong> <br>
              <small class="text-muted">Sales Manager</small> </span> </span> </a> 
              <!-- END User status--> 
              """),_display_(/*45.16*/for(data <- allSE) yield /*45.34*/{_display_(Seq[Any](format.raw/*45.35*/("""
              """),format.raw/*46.15*/("""<!-- START User status--> 
              <a href="#" class="media p mt0 list-group-item"> <span class="pull-right"> <span class="circle circle-success circle-lg"></span> </span> <span class="media-body"> <span class="media-heading"> <strong>"""),_display_(/*47.216*/data/*47.220*/.getUserName()),format.raw/*47.234*/("""</strong> <br>
              <small class="text-muted">Sales Executive</small> </span> </span> </a> 
              <!-- END User status--> 
              """)))}),format.raw/*50.16*/("""
             
              """),format.raw/*52.15*/("""<a href="#" class="media p mt0 list-group-item text-center text-muted">View all contacts</a> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
""")))}))
      }
    }
  }

  def render(dealerName:String,userName:String,form:Form[WyzUser],allSE:List[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(dealerName,userName,form,allSE)

  def f:((String,String,Form[WyzUser],List[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (dealerName,userName,form,allSE) => apply(dealerName,userName,form,allSE)

  def ref: this.type = this

}


}

/**/
object viewReport extends viewReport_Scope0.viewReport
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:42 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/viewReport.scala.html
                  HASH: 849bf6f602543fce88d240c84d4a359658c3a28e
                  MATRIX: 790->1|1005->75|1033->131|1097->187|1136->189|1166->193|2741->1740|2773->1750|2956->1906|2990->1924|3029->1925|3073->1941|3344->2184|3358->2188|3394->2202|3583->2360|3642->2391
                  LINES: 27->1|34->1|35->5|35->5|35->5|37->7|72->42|72->42|75->45|75->45|75->45|76->46|77->47|77->47|77->47|80->50|82->52
                  -- GENERATED --
              */
          