
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object addMasterDataSuperAdmin_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class addMasterDataSuperAdmin extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[String,String,String,String,List[Location],List[Workshop],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(dealercode:String,dealerName:String,userName:String,oem:String,locationList : List[Location],workshopList : List[Workshop]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.126*/("""

"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*4.56*/ {_display_(Seq[Any](format.raw/*4.58*/("""
"""),_display_(/*5.2*/helper/*5.8*/.form(action = routes.SuperAdminController.postLocationBySuperAdmin)/*5.76*/ {_display_(Seq[Any](format.raw/*5.78*/("""


"""),format.raw/*8.1*/("""<div class="row" >
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading">   
        Add Location </div>
		<div class="panel-body">
            <div class="col-lg-3">
              <div class="form-group">
                <label for="locationName">Location Name<i style="color:red">*</i>:</label>
                <input type="text" class="form-control txtOnly" maxlength="6" name="name"  id="locationName" autocomplete="off"  required/>
              </div>
            </div>
              <div class="col-lg-3">
              <div class="form-group">
                <label for="pincode">Pincode<i style="color:red">*</i>:</label>
                <input type="text" class="form-control numericOnly" name="pinCode" maxlength="6" id="pinCode" autocomplete="off"  required/>
              </div>
            </div>
              <div class="col-lg-3">
              <div class="form-group">
                <label for="locationCode">Location Code<i style="color:red">*</i>:</label>
                <input type="text" class="form-control txtOnly" name="locCode"  id="locationCode" autocomplete="off"  required/>
              </div>
            </div>
            <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">CITY </label>
                            <select id="city_id" name="city" class="filter form-control" data-column-index="0" onclick="ajaxCallToLoadWorkShopByCitySB();" required>
                                <option value="" >--Select--</option>
                                """),_display_(/*37.34*/for(location_list<-locationList) yield /*37.66*/{_display_(Seq[Any](format.raw/*37.67*/("""                       
                                	"""),format.raw/*38.34*/("""<option value=""""),_display_(/*38.50*/location_list/*38.63*/.getName()),format.raw/*38.73*/("""">"""),_display_(/*38.76*/location_list/*38.89*/.getName()),format.raw/*38.99*/("""</option>
                              	""")))}),format.raw/*39.33*/("""
                              """),format.raw/*40.31*/("""</select>
                        </div>
                    </div>
                <div class="col-lg-3">
                        <div class="form-group">
                            <label for="workshop">WORKSHOP </label>
                         	<input type="text" class="form-control txtOnly" name="workshopName"  id="workshop_id" autocomplete="off" required/>
                        </div>
                    </div>
                                
						
              <div class="col-lg-3">
              <div class="form-group">
                <label for="stateName">State<i style="color:red">*</i>:</label>
                <input type="text" class="form-control txtOnly" name="workshopAddress"  id="stateName" autocomplete="off"  required/>
              </div>
            </div>
             <div class="col-lg-3">
              <div class="form-group">
              <br />
                            <input type="submit" class="btn btn-primary"  id="locationSubmit" value="Submit" />
              </div>
            </div>
        </div>
     </div>
     </div>
     </div>
     """)))}),format.raw/*67.7*/("""
 """)))}),format.raw/*68.3*/("""
 
 """),format.raw/*70.2*/("""<script>
 $(".numericOnly").keypress(function (e) """),format.raw/*71.42*/("""{"""),format.raw/*71.43*/("""
	    """),format.raw/*72.6*/("""if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
	"""),format.raw/*73.2*/("""}"""),format.raw/*73.3*/(""");
 $( ".txtOnly" ).keypress(function(e) """),format.raw/*74.39*/("""{"""),format.raw/*74.40*/("""
     """),format.raw/*75.6*/("""var key = e.keyCode;
     if (key >= 48 && key <= 57) """),format.raw/*76.34*/("""{"""),format.raw/*76.35*/("""
         """),format.raw/*77.10*/("""e.preventDefault();
     """),format.raw/*78.6*/("""}"""),format.raw/*78.7*/("""
 """),format.raw/*79.2*/("""}"""),format.raw/*79.3*/(""");
 </script>"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String,locationList:List[Location],workshopList:List[Workshop]): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem,locationList,workshopList)

  def f:((String,String,String,String,List[Location],List[Workshop]) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem,locationList,workshopList) => apply(dealercode,dealerName,userName,oem,locationList,workshopList)

  def ref: this.type = this

}


}

/**/
object addMasterDataSuperAdmin extends addMasterDataSuperAdmin_Scope0.addMasterDataSuperAdmin
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:10 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/addMasterDataSuperAdmin.scala.html
                  HASH: 0a8283e914b48c0a2ac4c1e1acc96e8203f6b89e
                  MATRIX: 832->3|1052->127|1082->132|1144->186|1183->188|1211->191|1224->197|1300->265|1339->267|1371->273|3040->1915|3088->1947|3127->1948|3213->2006|3256->2022|3278->2035|3309->2045|3339->2048|3361->2061|3392->2071|3466->2114|3526->2146|4682->3272|4716->3276|4749->3282|4828->3333|4857->3334|4891->3341|4987->3410|5015->3411|5085->3453|5114->3454|5148->3461|5231->3516|5260->3517|5299->3528|5352->3554|5380->3555|5410->3558|5438->3559
                  LINES: 27->2|32->2|34->4|34->4|34->4|35->5|35->5|35->5|35->5|38->8|67->37|67->37|67->37|68->38|68->38|68->38|68->38|68->38|68->38|68->38|69->39|70->40|97->67|98->68|100->70|101->71|101->71|102->72|103->73|103->73|104->74|104->74|105->75|106->76|106->76|107->77|108->78|108->78|109->79|109->79
                  -- GENERATED --
              */
          