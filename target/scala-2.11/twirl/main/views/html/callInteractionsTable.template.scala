
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object callInteractionsTable_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class callInteractionsTable extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[String,String,String,String,List[String],List[CallDispositionData],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(fromDateValue:String,toDateValue:String,user:String,dealerName:String,allCRE:List[String],allDisposition:List[CallDispositionData],locations:List[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.157*/("""

"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*4.52*/ {_display_(Seq[Any](format.raw/*4.54*/("""
"""),format.raw/*5.1*/("""<input type="hidden" name="fromDateToAss" value=""""),_display_(/*5.51*/fromDateValue),format.raw/*5.64*/("""">
<input type="hidden" name="toDateToAss" value=""""),_display_(/*6.49*/toDateValue),format.raw/*6.60*/("""">
<div class="panel panel-primary">
        <div class="panel-heading"><strong>Select Date Range To view Call Interactions</strong></div>
        <div class="panel-body"> 
             """),_display_(/*10.15*/helper/*10.21*/.form(action = routes.AllCallInteractionController.downloadCallHistoryReport)/*10.98*/ {_display_(Seq[Any](format.raw/*10.100*/("""
  """),format.raw/*11.3*/("""<div class="row">
  <div class="col-md-2">   
      <div class="form-group">      
<label>Call Date From :<span style="color:red">*</span></label>     
		<input type="text" class="form-control datepickerFilter" id="frombilldaterange" name="frombilldaterange" readonly>
      </div>
      </div>
   <div class="col-md-2">   
      <div class="form-group">   
       <label>Call Date To :<span style="color:red">*</span></label>    
					<input type="text" class="form-control datepickerFilter"  id="tobilldaterange" name="tobilldaterange" readonly>
     	</div>
      </div>
       <div class="col-md-2">
                    <label>Select Location :</label>
                     <div class="form-group">
    		 	<select  multiple="multiple"  id="location"  name="locations[]" class="selectpicker form-control" onchange="ajaxCallToLoadWorkShopByCityCallhistory();" > 
    					                      			   
                     	"""),_display_(/*29.24*/for(data3 <- locations) yield /*29.47*/{_display_(Seq[Any](format.raw/*29.48*/("""                       	                         
                    
                        """),format.raw/*31.25*/("""<option value=""""),_display_(/*31.41*/data3),format.raw/*31.46*/("""">"""),_display_(/*31.49*/data3),format.raw/*31.54*/("""</option>
                      				
                      	""")))}),format.raw/*33.25*/("""			 	
                    	"""),format.raw/*34.22*/("""</select>	
           
      </div>
       </div>
       
       <div class="col-md-2">
   
                    <label>Select Workshops :</label>
                     <div class="form-group">
    		 	<select  multiple="multiple"  id="workshopname"  name="workshopname[]" class="selectpicker form-control"  onchange="ajaxCallHistoryReports();"  > 
    					                      			    
                     				 	
                    	</select>
                    			
           
      </div>
       </div>
       <div class="col-md-2">
                    <label>Select Dispositions :</label>
                     <div class="form-group">
    			<select  multiple="multiple" id="all_dispositions" name="all_dispositions[]" class="selectpicker form-control" >
    					                       			    
                     	"""),_display_(/*56.24*/for(data2 <- allDisposition) yield /*56.52*/{_display_(Seq[Any](format.raw/*56.53*/("""                      	                         
                    
                        """),format.raw/*58.25*/("""<option value=""""),_display_(/*58.41*/data2/*58.46*/.getId()),format.raw/*58.54*/("""">"""),_display_(/*58.57*/data2/*58.62*/.getDisposition()),format.raw/*58.79*/("""</option>
                      				
                      	""")))}),format.raw/*60.25*/("""			 	
                      					
                	"""),format.raw/*62.18*/("""</select>
                				
            </div>
             </div>
        <div class="col-md-2">
   
                    <label>Select CRE's :</label>
                     <div class="form-group">
              <select class="selectpicker form-control"  id="crename" name="crename[]" multiple>
    					                      			    
                     				 	
                    	</select>
           
      </div>
       </div>
</div>
<div class="col-md-12">
  <div class="form-group pull-right">
  		<button type="button" name="selectedFile" class="btn btn-sm btn-primary viewCallsforReports" value="viewCalls"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;View Calls</button>		     	
  		 <button type="submit" name="downloadFile" class="btn btn-sm btn-warning"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download</button>
		</div>
</div> 
  </div>
 
  """)))}),format.raw/*86.4*/("""
  """),format.raw/*87.3*/("""</div>      
<span type="label" id="viewCallsERR" style="color:red"></span>
<div class="row">
      <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading"><b> List Of  Call Interactions</b>
          </div>
          <div class="panel-body" style="overflow:auto">
          <table class="table table-striped table-bordered table-hover"
						id="datatableCallIntraction">
						<thead>
							<tr>

								<th>Location</th> 
                                    <th>CRE Name</th> 
                                    <th>Call Time</th>                                   
                                    <th>Call Date</th>
                                    <th>Preferred ContactNo</th>
                                    <th>Call Type</th>
                                    <th>Customer Name</th>
                                    <th>DOB</th>
                                    <th>Office Address</th>
                                    <th>Residential Address</th>
                                    <th>Permanent Address</th>
                                    <th>Email</th>
                                    <th>Customer Category</th>
                                    <th>Customer City</th>
                                    <th>Call Duration</th>
                                    <th>Customer Remarks</th>
                                    <th>Vehicle RegNo</th>
                                    <th>Chassis No</th>
                                    <th>Model</th>
                                    <th>Fuel Type</th>
                                    <th>Variant</th>
                                    <th>Last Service Date</th>
                                    <th>Last Service Type</th>
                                    <th>Next Service Date</th>
                                    <th>Next Service Type</th>
                                    <th>Forecast Logic</th>
                                    <th>Previous Disposition</th>
                                    <th>Primary Disposition</th>
                                    <th>Secondary Disposition</th>
                                    <th>Tertiary Disposition</th>
                                    <th>Service Type</th>
                                    <th>PSF Status</th>
                                    <th>Calling data type</th>
                                    <th>Type of Pickup</th>
                                    <th>Time of PickUp</th>
                                    <th>Up to</th>
                                    <th>Driver</th>
                                    <th>Service Advisor</th>
                                    <th>Upsell Type</th>
                                    <th>Assigned Date</th>
                                    <th>Is Call initiated</th>


							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
                     
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
""")))}),format.raw/*157.2*/("""
	
"""),format.raw/*159.1*/("""<script>
$(document).ready(function () """),format.raw/*160.31*/("""{"""),format.raw/*160.32*/("""
	"""),format.raw/*161.2*/("""$('.viewCallsforReports').click(function()"""),format.raw/*161.44*/("""{"""),format.raw/*161.45*/("""
    	
        """),format.raw/*163.9*/("""var fromDate     = $("#frombilldaterange").val();
        var toDate       = $("#tobilldaterange").val();
       
              
        var UserIds="",j,k;
        var dispositions="";
        var location ="";
        var workshop = "";
        
        myOption = document.getElementById('crename');
        console.log("here"+myOption);		        
    for (j=0;j<myOption.options.length;j++)"""),format.raw/*174.44*/("""{"""),format.raw/*174.45*/("""
        """),format.raw/*175.9*/("""if(myOption.options[j].selected)"""),format.raw/*175.41*/("""{"""),format.raw/*175.42*/("""
            """),format.raw/*176.13*/("""if(myOption.options[j].value != "Select")"""),format.raw/*176.54*/("""{"""),format.raw/*176.55*/("""
        		"""),format.raw/*177.11*/("""UserIds = UserIds + myOption.options[j].value + ",";
            """),format.raw/*178.13*/("""}"""),format.raw/*178.14*/("""else"""),format.raw/*178.18*/("""{"""),format.raw/*178.19*/("""
				"""),format.raw/*179.5*/("""UserIds ="";
            """),format.raw/*180.13*/("""}"""),format.raw/*180.14*/("""
        """),format.raw/*181.9*/("""}"""),format.raw/*181.10*/("""
    """),format.raw/*182.5*/("""}"""),format.raw/*182.6*/("""
    	"""),format.raw/*183.6*/("""if(UserIds.length > 0)"""),format.raw/*183.28*/("""{"""),format.raw/*183.29*/("""
        	"""),format.raw/*184.10*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
        	
    	"""),format.raw/*186.6*/("""}"""),format.raw/*186.7*/("""else"""),format.raw/*186.11*/("""{"""),format.raw/*186.12*/("""
    		"""),format.raw/*187.7*/("""UserIds="";
    	"""),format.raw/*188.6*/("""}"""),format.raw/*188.7*/("""

    	"""),format.raw/*190.6*/("""mydispo = document.getElementById('all_dispositions');
        console.log("here"+mydispo);
    	 for (j=0;j<mydispo.options.length;j++)"""),format.raw/*192.45*/("""{"""),format.raw/*192.46*/("""
    	        """),format.raw/*193.14*/("""if(mydispo.options[j].selected)"""),format.raw/*193.45*/("""{"""),format.raw/*193.46*/("""
    	            """),format.raw/*194.18*/("""if(mydispo.options[j].value != "Select")"""),format.raw/*194.58*/("""{"""),format.raw/*194.59*/("""
    	        		"""),format.raw/*195.16*/("""dispositions = dispositions + mydispo.options[j].value + ",";
    	            """),format.raw/*196.18*/("""}"""),format.raw/*196.19*/("""else"""),format.raw/*196.23*/("""{"""),format.raw/*196.24*/("""
						"""),format.raw/*197.7*/("""dispositions ="";

    	            """),format.raw/*199.18*/("""}"""),format.raw/*199.19*/("""
        	            
    	        """),format.raw/*201.14*/("""}"""),format.raw/*201.15*/("""
    	    """),format.raw/*202.10*/("""}"""),format.raw/*202.11*/("""	   	
    	 """),format.raw/*203.7*/("""if(dispositions.length > 0)"""),format.raw/*203.34*/("""{"""),format.raw/*203.35*/("""
    		 """),format.raw/*204.8*/("""dispositions = dispositions.substring(0, dispositions.length - 1);
         	
     	"""),format.raw/*206.7*/("""}"""),format.raw/*206.8*/("""else"""),format.raw/*206.12*/("""{"""),format.raw/*206.13*/("""
     		"""),format.raw/*207.8*/("""dispositions="";
     	"""),format.raw/*208.7*/("""}"""),format.raw/*208.8*/("""


    	 """),format.raw/*211.7*/("""myloc = document.getElementById('all_dispositions');
         console.log("here"+myloc);
     	 for (j=0;j<myloc.options.length;j++)"""),format.raw/*213.44*/("""{"""),format.raw/*213.45*/("""
     	        """),format.raw/*214.15*/("""if(myloc.options[j].selected)"""),format.raw/*214.44*/("""{"""),format.raw/*214.45*/("""
     	            """),format.raw/*215.19*/("""if(myloc.options[j].value != "Select")"""),format.raw/*215.57*/("""{"""),format.raw/*215.58*/("""
     	        		"""),format.raw/*216.17*/("""locations = location + myloc.options[j].value + ",";
     	            """),format.raw/*217.19*/("""}"""),format.raw/*217.20*/("""else"""),format.raw/*217.24*/("""{"""),format.raw/*217.25*/("""
						"""),format.raw/*218.7*/("""locations ="";
     	            """),format.raw/*219.19*/("""}"""),format.raw/*219.20*/("""
     	        """),format.raw/*220.15*/("""}"""),format.raw/*220.16*/("""
     	    """),format.raw/*221.11*/("""}"""),format.raw/*221.12*/("""	   	
     	 """),format.raw/*222.8*/("""if(location.length > 0)"""),format.raw/*222.31*/("""{"""),format.raw/*222.32*/("""
     		 """),format.raw/*223.9*/("""locations = location.substring(0, myloc.length - 1);
          	
      	"""),format.raw/*225.8*/("""}"""),format.raw/*225.9*/("""else"""),format.raw/*225.13*/("""{"""),format.raw/*225.14*/("""
      		"""),format.raw/*226.9*/("""locations="";
      	"""),format.raw/*227.8*/("""}"""),format.raw/*227.9*/("""

     	 """),format.raw/*229.8*/("""myworkshop = document.getElementById('workshopname');
         console.log("here"+myworkshop);
     	 for (j=0;j<myworkshop.options.length;j++)"""),format.raw/*231.49*/("""{"""),format.raw/*231.50*/("""
     	        """),format.raw/*232.15*/("""if(myworkshop.options[j].selected)"""),format.raw/*232.49*/("""{"""),format.raw/*232.50*/("""
     	            """),format.raw/*233.19*/("""if(myworkshop.options[j].value != "Select")"""),format.raw/*233.62*/("""{"""),format.raw/*233.63*/("""
     	        		"""),format.raw/*234.17*/("""workshop = workshop + myworkshop.options[j].value + ",";
     	        		
     	            """),format.raw/*236.19*/("""}"""),format.raw/*236.20*/("""else"""),format.raw/*236.24*/("""{"""),format.raw/*236.25*/("""
     	        		"""),format.raw/*237.17*/("""workshop = "";
         	            
     	            """),format.raw/*239.19*/("""}"""),format.raw/*239.20*/("""
     	        """),format.raw/*240.15*/("""}"""),format.raw/*240.16*/("""
     	    """),format.raw/*241.11*/("""}"""),format.raw/*241.12*/("""	   	
     	 """),format.raw/*242.8*/("""if(workshop.length > 0)"""),format.raw/*242.31*/("""{"""),format.raw/*242.32*/("""
     		 """),format.raw/*243.9*/("""workshop = workshop.substring(0, myworkshop.length - 1);
          	
      	"""),format.raw/*245.8*/("""}"""),format.raw/*245.9*/("""else"""),format.raw/*245.13*/("""{"""),format.raw/*245.14*/("""
      		"""),format.raw/*246.9*/("""workshop="";
      	"""),format.raw/*247.8*/("""}"""),format.raw/*247.9*/("""
     	"""),format.raw/*248.7*/("""var urlLink = "/CREManager/getAllCallHistoryData"
    	    
    	    $('#datatableCallIntraction').dataTable( """),format.raw/*250.51*/("""{"""),format.raw/*250.52*/("""
    	     """),format.raw/*251.11*/(""""bDestroy": true,
    	     "processing": true,
    	     "serverSide": true,
    	      "scrollY": 400,
 	        "scrollX": true,
    	     "paging": true,
    	     "searching": false,
    	     "ordering":false,
    	     "ajax": """),format.raw/*259.19*/("""{"""),format.raw/*259.20*/("""
    		        """),format.raw/*260.15*/("""'type': 'POST',
    		        'url': urlLink,
    		        'data': """),format.raw/*262.23*/("""{"""),format.raw/*262.24*/("""    		        	
    		        	"""),format.raw/*263.16*/("""fromDate:''+fromDate,
    		        	toDate:''+toDate ,
    		        	UserIds:''+UserIds,
    		        	dispositions:''+dispositions,
    		        	locations:''+locations,
    		        	workshop:''+workshop,	        	
    		        	
    		        	
    		        """),format.raw/*271.15*/("""}"""),format.raw/*271.16*/("""
        	        
    	     """),format.raw/*273.11*/("""}"""),format.raw/*273.12*/(""",
    	     "fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*274.67*/("""{"""),format.raw/*274.68*/("""
                 """),format.raw/*275.18*/("""$('td', nRow).attr('nowrap','nowrap');
                 return nRow;
                 """),format.raw/*277.18*/("""}"""),format.raw/*277.19*/("""
    	 """),format.raw/*278.7*/("""}"""),format.raw/*278.8*/(""" """),format.raw/*278.9*/(""");


    """),format.raw/*281.5*/("""}"""),format.raw/*281.6*/(""");
"""),format.raw/*282.1*/("""}"""),format.raw/*282.2*/(""");



</script>
"""))
      }
    }
  }

  def render(fromDateValue:String,toDateValue:String,user:String,dealerName:String,allCRE:List[String],allDisposition:List[CallDispositionData],locations:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(fromDateValue,toDateValue,user,dealerName,allCRE,allDisposition,locations)

  def f:((String,String,String,String,List[String],List[CallDispositionData],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (fromDateValue,toDateValue,user,dealerName,allCRE,allDisposition,locations) => apply(fromDateValue,toDateValue,user,dealerName,allCRE,allDisposition,locations)

  def ref: this.type = this

}


}

/**/
object callInteractionsTable extends callInteractionsTable_Scope0.callInteractionsTable
              /*
                  -- GENERATED --
                  DATE: Fri Dec 22 16:40:55 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/callInteractionsTable.scala.html
                  HASH: 241236b26645b2943548722b69cd26b6d133cd2b
                  MATRIX: 850->2|1101->157|1129->160|1187->210|1226->212|1253->213|1329->263|1362->276|1439->327|1470->338|1684->525|1699->531|1785->608|1826->610|1856->613|2810->1540|2849->1563|2888->1564|3011->1659|3054->1675|3080->1680|3110->1683|3136->1688|3228->1749|3283->1776|4134->2600|4178->2628|4217->2629|4339->2723|4382->2739|4396->2744|4425->2752|4455->2755|4469->2760|4507->2777|4599->2838|4678->2889|5588->3769|5618->3772|8763->6886|8794->6889|8862->6928|8892->6929|8922->6931|8993->6973|9023->6974|9066->6989|9489->7383|9519->7384|9556->7393|9617->7425|9647->7426|9689->7439|9759->7480|9789->7481|9829->7492|9923->7557|9953->7558|9986->7562|10016->7563|10049->7568|10103->7593|10133->7594|10170->7603|10200->7604|10233->7609|10262->7610|10296->7616|10347->7638|10377->7639|10416->7649|10511->7716|10540->7717|10573->7721|10603->7722|10638->7729|10683->7746|10712->7747|10747->7754|10912->7890|10942->7891|10985->7905|11045->7936|11075->7937|11122->7955|11191->7995|11221->7996|11266->8012|11374->8091|11404->8092|11437->8096|11467->8097|11502->8104|11567->8140|11597->8141|11662->8177|11692->8178|11731->8188|11761->8189|11801->8201|11857->8228|11887->8229|11923->8237|12035->8321|12064->8322|12097->8326|12127->8327|12163->8335|12214->8358|12243->8359|12280->8368|12441->8500|12471->8501|12515->8516|12573->8545|12603->8546|12651->8565|12718->8603|12748->8604|12794->8621|12894->8692|12924->8693|12957->8697|12987->8698|13022->8705|13084->8738|13114->8739|13158->8754|13188->8755|13228->8766|13258->8767|13299->8780|13351->8803|13381->8804|13418->8813|13518->8885|13547->8886|13580->8890|13610->8891|13647->8900|13696->8921|13725->8922|13762->8931|13934->9074|13964->9075|14008->9090|14071->9124|14101->9125|14149->9144|14221->9187|14251->9188|14297->9205|14418->9297|14448->9298|14481->9302|14511->9303|14557->9320|14642->9376|14672->9377|14716->9392|14746->9393|14786->9404|14816->9405|14857->9418|14909->9441|14939->9442|14976->9451|15080->9527|15109->9528|15142->9532|15172->9533|15209->9542|15257->9562|15286->9563|15321->9570|15460->9680|15490->9681|15530->9692|15793->9926|15823->9927|15867->9942|15964->10010|15994->10011|16054->10042|16351->10310|16381->10311|16439->10340|16469->10341|16566->10409|16596->10410|16643->10428|16758->10514|16788->10515|16823->10522|16852->10523|16881->10524|16918->10533|16947->10534|16978->10537|17007->10538
                  LINES: 27->2|32->2|34->4|34->4|34->4|35->5|35->5|35->5|36->6|36->6|40->10|40->10|40->10|40->10|41->11|59->29|59->29|59->29|61->31|61->31|61->31|61->31|61->31|63->33|64->34|86->56|86->56|86->56|88->58|88->58|88->58|88->58|88->58|88->58|88->58|90->60|92->62|116->86|117->87|187->157|189->159|190->160|190->160|191->161|191->161|191->161|193->163|204->174|204->174|205->175|205->175|205->175|206->176|206->176|206->176|207->177|208->178|208->178|208->178|208->178|209->179|210->180|210->180|211->181|211->181|212->182|212->182|213->183|213->183|213->183|214->184|216->186|216->186|216->186|216->186|217->187|218->188|218->188|220->190|222->192|222->192|223->193|223->193|223->193|224->194|224->194|224->194|225->195|226->196|226->196|226->196|226->196|227->197|229->199|229->199|231->201|231->201|232->202|232->202|233->203|233->203|233->203|234->204|236->206|236->206|236->206|236->206|237->207|238->208|238->208|241->211|243->213|243->213|244->214|244->214|244->214|245->215|245->215|245->215|246->216|247->217|247->217|247->217|247->217|248->218|249->219|249->219|250->220|250->220|251->221|251->221|252->222|252->222|252->222|253->223|255->225|255->225|255->225|255->225|256->226|257->227|257->227|259->229|261->231|261->231|262->232|262->232|262->232|263->233|263->233|263->233|264->234|266->236|266->236|266->236|266->236|267->237|269->239|269->239|270->240|270->240|271->241|271->241|272->242|272->242|272->242|273->243|275->245|275->245|275->245|275->245|276->246|277->247|277->247|278->248|280->250|280->250|281->251|289->259|289->259|290->260|292->262|292->262|293->263|301->271|301->271|303->273|303->273|304->274|304->274|305->275|307->277|307->277|308->278|308->278|308->278|311->281|311->281|312->282|312->282
                  -- GENERATED --
              */
          