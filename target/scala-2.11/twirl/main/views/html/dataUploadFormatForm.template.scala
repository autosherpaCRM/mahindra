
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object dataUploadFormatForm_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class dataUploadFormatForm extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(userName:String,dealerName:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import models._

Seq[Any](format.raw/*1.37*/("""
"""),_display_(/*3.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*3.56*/ {_display_(Seq[Any](format.raw/*3.58*/("""
"""),_display_(/*4.2*/helper/*4.8*/.form(action=routes.CallInteractionController.upload_file_Format_submit())/*4.82*/{_display_(Seq[Any](format.raw/*4.83*/("""
	
        
             """),format.raw/*7.14*/("""<div class="panel panel-info">
        <div class="panel-heading"><strong>Data Upload Format Creation</strong></div>
        <div class="panel-body">
             <div class="row">
               <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        
                        <div class="ibox-content">
                            <div class="form-horizontal">
                               <!-- <p>Please enter the headers of selected part master</p> -->
                                <div class="form-group">
                                    	<div class="col-lg-2" style="width: 191px;">
                                    		<select name="upload_format_name" id="upload_format_name" class="form-control" onChange="return ajaxRequest_getForm();">
	                        					<option value= "0" selected>--Select--</option>	                                                                
								                        		<option value= "sale_register">Sale Register</option>
							                                    <option value= "job_card">Job Card</option>
							                                    <option value= "bill">Bills</option>
							                                    <option value= "SMR">SMR</option>
							                                    <option value= "campaign">Campaign</option>
	                        					
	                        					
                            				</select>
                            			</div>	                       			
                            	</div>
                            </div>
                          </div>   
                          <div class="hr-line-dashed"></div>
                          <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-bordered header-fixed table-responsive" id="tableFormat" >
                                    <thead>
						                <tr>
                							<th>Required Column </th>
                							<th style="display:none">Data Type</th>
                							<th>Matching Excel Column Name</th>
										</tr>
                					</thead>
                					<tbody>
                					</tbody>
                				</table>
                			</div>
                			<div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary" type="submit" ><i class="fa fa-check"></i>&nbsp;Submit</button> <!-- onclick="return ajaxMap_MasterDataFormat();" -->
                                 	</div>
                            	 </div>
                            </div>
                             <div class="table-responsive">
                                <table class="table table-bordered header-fixed table-responsive" id="tableData" >
                                    <thead>
						                <tr>
                							
										</tr>
                					</thead>
                					<tbody>
                					</tbody>
                				</table>
                			</div>
                       </div>
                  </div>
               </div>
        </div>
             </div>


         
                                                  
""")))}),format.raw/*75.2*/("""  
""")))}),format.raw/*76.2*/("""                                               """))
      }
    }
  }

  def render(userName:String,dealerName:String): play.twirl.api.HtmlFormat.Appendable = apply(userName,dealerName)

  def f:((String,String) => play.twirl.api.HtmlFormat.Appendable) = (userName,dealerName) => apply(userName,dealerName)

  def ref: this.type = this

}


}

/**/
object dataUploadFormatForm extends dataUploadFormatForm_Scope0.dataUploadFormatForm
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:25 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/dataUploadFormatForm.scala.html
                  HASH: 1af310cbfccd2ff5b4a91e6ecf662e27a1be6893
                  MATRIX: 782->1|927->36|955->57|1017->111|1056->113|1084->116|1097->122|1179->196|1217->197|1272->225|4782->3705|4817->3710
                  LINES: 27->1|32->1|33->3|33->3|33->3|34->4|34->4|34->4|34->4|37->7|105->75|106->76
                  -- GENERATED --
              */
          