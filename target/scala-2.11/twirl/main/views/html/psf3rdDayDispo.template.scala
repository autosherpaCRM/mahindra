
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object psf3rdDayDispo_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class psf3rdDayDispo extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[Vehicle,String,String,Service,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(vehicleData:Vehicle,userName:String,dealername:String,latestService:Service):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.79*/("""

"""),format.raw/*3.1*/("""<div class="panel panel-primary">
<div class="panel-heading" align="center"; style="background-color:#1797be;"><img src=""""),_display_(/*4.89*/routes/*4.95*/.Assets.at("images/INTERACTION HISTORY.png")),format.raw/*4.139*/("""" style="width:17px"/>&nbsp;<b>PSF DISPOSITION FORM 3RD DAY</b> 
		</div>
  
    <div class="panel-body">
		<div class="col-md-12">
			<div class="form-group">
					<div id="PSFconnectCall1H" class="animated  bounceInRight">
		  <label for=""><b>Did you talk to the customer?</b></label>
		  <br>
		  <div class="radio-inline">
			<label>
			  <input type="radio" name="isContacted" value="PSF Yes" id="">
			  Yes </label>
		  </div>
		  <div class="radio-inline">
			<label>
			  <input type="radio" name="isContacted" value="PSF No" id="" >
			  No</label>
		  </div>
		 </div>
		 
		 <div style="display:none;" class="animated  bounceInRight" id="PSFNotSpeachDiv"><br>
			<label for=""><b>Choose reason for not being able to talk to customer:</b></label>
			<br>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="Ringing No response" id="">
				Ringing No response </label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="Busy" id="" >
				Busy</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="Switched Off / Unreachable" id="" >
				Switched Off / Unreachable</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="Invalid Number" id="" >
				Invalid Number</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="NoOther" id="NOOthersCheck" >
				Other</label>
			</div>
			
			
			<div class="row animated  bounceInRight"  id="PSFNoOthers">
			<div class="col-md-3">
			<label><b>Remarks</b></label>
			<textarea type="text" class="form-control" rows="1" name="OtherComments"></textarea>
			</div>
			</div>
			<div class="pull-right">
						
					<button type="button" class="btn btn-info" name="" id="BackTolatkNoPsf">Back</button>
					<button type="submit" class="btn btn-info" name="" value="">Submit</button>
			</div>
		  </div>
		  
		   <div style="display:none;" class="animated  bounceInRight" id="PSFYesTalkH" >
			<br />
			<div class="col-md-12">
			<p>Good Morning/Good Afternoon, I am """),_display_(/*71.42*/userName),format.raw/*71.50*/(""" """),format.raw/*71.51*/("""from """),_display_(/*71.57*/dealername),format.raw/*71.67*/(""". Am I speaking to """),_display_(/*71.87*/{vehicleData.getCustomer().getCustomerName()}),format.raw/*71.132*/("""?</p>
			<p>Thank you for choosing our """),_display_(/*72.35*/dealername),format.raw/*72.45*/(""" """),format.raw/*72.46*/("""for servicing of your """),_display_(/*72.69*/{vehicleData.getModel()}),format.raw/*72.93*/("""</p>
			<p>Can I talk to you for few minutes to take few feedback about the vehicle service.</p>
			<p>What did the Customer Say?</p>
		 <div class="radio-inline">
			<label>
			  <input type="radio" name="disposition" value="PSF_Yes" id="GoodMorningYes">
			  Ok, Proceed </label>
		  </div>
		   <div class="radio-inline">
			<label>
			  <input type="radio" name="disposition" value="Call Me Later" id="GoodMorningNo">
			  I am Busy </label>
		  </div>
		  
		  <div class="pull-right">
					<button type="button" class="btn btn-info" id="BackTo1stQ">Back</button>						
					<button type="button" class="btn btn-info" id="NextTO2ndQ">Next</button>
			</div>
		  </div>
		  </div>
		    <div style="display:none;" class="animated  bounceInRight" id="PSFYesNamaskarYesDivH" >
			<p>Sir/Madam, were all the requested jobs done to your complete satisfaction?</p>
			
				<div class="radio-inline">
					<label>
					  <input type="radio" name="q1_CompleteSatisfication" value="PSFSelf Yes" id="PSF2YesId">
					  Yes </label>
				</div>
				<div class="radio-inline">
					<label>
					  <input type="radio" name="q1_CompleteSatisfication" value="PSFSelf No" id="PSF2NoId">
					 No </label>
				</div>
				
				<div style="display:none;" class="row animated  bounceInRight" id="PsfSelfDriveInNo1H" >
				
				<div class="col-md-12">
				<p>Sir we are extremely sorry for an inconvinence caused.</p>
				<p>May I know the reason of dissatisfaction.</p>
				</div>
				<div class="col-md-12">
				<div class="col-md-3">
				<div class="form-group">
				<label>Function</label>
				<select class="form-control" name="q2_InconvinenceDSRFunction">
				<option value="0">--SELECT--</option>
					<option value="Washing">Washing</option>
					<option value="Delay Delivery">Delay Delivery</option>
					<option value="Discrepancy on Charges">Discrepancy on Charges</option>
					<option value="Requested Repair not Done">Requested Repair not Done</option>
					<option value="Quality of Work">Quality of Work</option>
					<option value="SA/Other Emp Behaviour">SA/Other Emp Behaviour</option>
					<option value="New Problem">New Problem</option>
					<option value="Others">Others</option>
				</select>
				</div>	
				</div>
				<div class="col-md-3">
				<div class="form-group">
				<label>Assigned To</label>
				<select class="form-control" name="q3_InconvinenceDSRAssignedTo">
				<option value="0">--SELECT--</option>
					<option value=""""),_display_(/*134.22*/{latestService.getSaName()}),format.raw/*134.49*/("""">"""),_display_(/*134.52*/{latestService.getSaName()}),format.raw/*134.79*/("""</option>
					
				</select>
				</div>	
				</div>
				<div class="col-md-3">
				<div class="form-group">
				<label>Remarks* (Mandate)</label>
				<textarea class="form-control" type="text" rows="2" name="remarksList[2]"></textarea>
					
				
				</div>	
				</div>
				
			</div>
			<div class="col-md-12">
				<p>I have recorded your concern and will inform the same to our service team and our service team will contact you soon. </p></div>
				
			
			
			</div>
			
			<div style="display:none;" class="row animated  bounceInRight" id="PsfSelfDriveINYesH" >

			
			<div class="col-md-12">
			<p>Sir/Madam, I would like to request you to rate following question on the scale of 1 to 10, where 1-8 is poor, 9 is good and 10 is excellent</p>
			<div class="col-md-8">
			 <label for="sel1">1. How would you rate the maintenance & repair work done on your car?</label>
			</div>
			<div class="col-md-2">
			<div class="form-group">
			<div class="form-inline">
     
      <select class="form-control" id="sel1" name="q17_RateMaintenanceAndRepair">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select>
	  </div>
	  </div>
			</div>
			
			<div class="col-md-8">
			<label for="sel2">2. How would you rate the condition or cleanliness of your car, on return?</label>
			</div>
			<div class="col-md-2">
			<div class="form-group">
	  <div class="form-inline">
      
      <select class="form-control" id="sel2" name="q7_RateCleanlinessOfCar">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select>
	  </div>
	  </div>
	  </div>
	  <div class="col-md-8">
		<label for="sel3">3. How would you rate the explanation given by service advisor on the work done and repair charges of the servicing?</label>
	  </div>
			<div class="col-md-2">
			    <div class="form-group">
	  <div class="form-inline">
  
      <select class="form-control" id="sel3" name="q5_RateExplanationOfSA">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select>
	  </div>
	  </div>
	  </div>
	  <div class="col-md-8">
	  <label for="">4. How would you rate the pick-up process of your car after servicing?</label>
	  </div>
	  <div class="col-md-2">
	     <div class="form-group">
	  <div class="form-inline">
      
      <select class="form-control" id="" name="q6_RatePickUpProcessOfCar">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select>
	  </div>
	  </div>
			</div>
			
			
			  <div class="col-md-8">
	  <label for="">5. Whether you have availed the Pick & Drop services provided by Dealer (YES/NO) </label>
	  </div>
	  <div class="col-md-2">
	     <div class="form-group">
	  <div class="form-inline">
      
      <select class="form-control" id="" name="q14_availedPickUpGivenByDealer">
        <option value="Yes">Yes</option>
        <option value="No">No</option>
       
      </select>
	  </div>
	  </div>
			</div>
			
			
			
			  <div class="col-md-8">
	  <label for="">6. How would you rate the cleanliness of Dealer Facility?</label>
	  </div>
			<div class="col-md-2">
	     <div class="form-group">
	  <div class="form-inline">
      
      <select class="form-control" id="" name="q16_RateCleanlinessOfDealerFacility">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select>
	  </div>
	  </div>
			</div>
			
			
			  <div class="col-md-8">
	  <label for="">7. How would you rate your overall interaction & experience with workshop?</label>
	  </div>
			<div class="col-md-2">
	     <div class="form-group">
	  <div class="form-inline">
      
      <select class="form-control" id="" name="q8_RateOverAllInteraction">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select>
	  </div>
	  </div>
			</div>
			  <div class="col-md-8">
	  <label for="">8. Did our dealer staff explain you about the "Refer & Win" Program? (YES/NO) </label>
	  </div>
	  <div class="col-md-2">
	     <div class="form-group">
	  <div class="form-inline">
      
      <select class="form-control" id="" name="q15_explainedReferAndEarnPRGM">
        <option value="Yes">Yes</option>
        <option value="No">No</option>
       
      </select>
	  </div>
	  </div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
  <label for="comment">Comments:</label>
  <textarea class="form-control" rows="2" id="comment" name="remarksList[1]"></textarea>
</div>
			</div>
			
			
			
			</div>
			</div>
			<div class="pull-right">
					<button type="button" class="btn btn-info" id="BackToSirMam">Back</button>						
					<button type="button" class="btn btn-info" id="NextToHowRate">Next</button>
			</div>
			
			
			</div>
			
			
			<div class="col-md-12 animated  bounceInRight" style="display:none;" id="upsell3rdDayH">
			<label>Capture Lead : Is there an Upsell Opportunity.</label>
		   <br>
		   
			<div class="radio-inline">
			  <label>
			<input type="radio" name="LeadYesH" value="Yes" id="LeadYesID3Hyndai" >
				Yes</label>
			</div>
			<div class="radio-inline">
			  <label>
				<input type="radio" name="LeadYesH" value="No" id="LeadNoID3Hyndai" checked>
				No</label>
			</div>
			<div class="row animated  bounceInRight" style="display:none;" id="LeadHyndai3rdDay">
			 <div class="col-md-12">
			  <div class="col-md-6">
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[0].upsellId" value="1" id="InsuranceIDCheck" onClick="loadLeadBasedOnUserLocation('InsuranceIDCheck','insuranceLead1')">
					Service</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="InsuranceSelect">
				 
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[0].taggedTo" id="insuranceLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments1" name="upsellLead[0].upsellComments"></textarea>
					</div>
				 
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[1].upsellId" value="2" id="MaxicareIDCheck" onClick="loadLeadBasedOnUserLocation('MaxicareIDCheck','maxicareLead')">
					Warranty / EW</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="MaxicareSelect">
					
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[1].taggedTo" id="maxicareLead">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments2" name="upsellLead[1].upsellComments"></textarea>
					</div>
				 
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="VAS myOutCheckbox" name="upsellLead[2].upsellId" value="3" id="VASID" onClick="loadLeadBasedOnUserLocation('VASID','vASLead1')"/>
					VAS</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="VASTagToSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[2].taggedTo" id="vASLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments3" name="upsellLead[2].upsellComments"></textarea>
					</div>
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[3].upsellId" value="4" id="ShieldID" onClick="loadLeadBasedOnUserLocation('ShieldID','warrantyLead1')" />
					Re-Finance / New Car Finance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="ShieldSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[3].taggedTo" id="warrantyLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments4" name="upsellLead[3].upsellComments"></textarea>
					</div>
				 </div>
				</div>
				<div class="col-md-6">
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="Loan myOutCheckbox" name="upsellLead[4].upsellId" value="5" id="RoadSideAsstID" onClick="loadLeadBasedOnUserLocation('RoadSideAsstID','RoadSideAssiLead1')"/>
					Sell Old Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="RoadSideAssiSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[4].taggedTo" id="RoadSideAssiLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments5" name="upsellLead[4].upsellComments"></textarea>
					</div>
				  </div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="EXCHANGE myOutCheckbox" name="upsellLead[5].upsellId" value="6" id="EXCHANGEID" onClick="loadLeadBasedOnUserLocation('EXCHANGEID','buyNewCarLead1')"/>
					Buy New Car / Exchange</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="EXCHANGEIDSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[5].taggedTo" id="buyNewCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments6" name="upsellLead[5].upsellComments"></textarea>
					</div>
				 	</div>
				  <div class="checkbox">
				  <label>
					<input type="checkbox" class="UsedCar myOutCheckbox" name="upsellLead[6].upsellId" value="9" id="UsedCarID" onClick="loadLeadBasedOnUserLocation('UsedCarID','usedCarLead1')"/>
					Used Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="UsedCarSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[6].taggedTo" id="usedCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
			<textarea class="form-control" rows="1" id="comments7" name="upsellLead[6].upsellComments"></textarea>
					</div>
				 
				</div>
		   
			  </div>
			</div>
			 
			</div>
		 <!-- 	<div class="row animated  bounceInRight" style="display:none;" id="LeadHyndai3rdDay">
			 <div class="col-md-12">
			  <div class="col-md-6">
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[0].upsellId" value="Insurance" id="InsuranceIDCheck">
					Service</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="InsuranceSelect">
				 
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[0].taggedTo" id="insuranceLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments1" name="upsellLead[0].upsellComments"></textarea>
					</div>
				 
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[1].upsellId" value="Warranty / EW" id="WARRANTYID" >
					Warranty / EW</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="WARRANTYSelect">
					
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[1].taggedTo" id="warrantyLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments2" name="upsellLead[1].upsellComments"></textarea>
					</div>
				 
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="VAS myOutCheckbox" name="upsellLead[2].upsellId" value="VAS" id="VASID" />
					VAS</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="VASTagToSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[2].taggedTo" id="vASLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments3" name="upsellLead[2].upsellComments"></textarea>
					</div>
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[3].upsellId" value="Re-Finance / New Car Finance" id="ReFinanceIDCheck" />
					Re-Finance / New Car Finance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="ReFinanceSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[3].taggedTo" id="reFinanceLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments4" name="upsellLead[3].upsellComments"></textarea>
					</div>
				 </div>
				</div>
				<div class="col-md-6">
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="Loan myOutCheckbox" name="upsellLead[4].upsellId" value="Sell Old Car" id="LoanID" />
					Sell Old Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="LoanSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[4].taggedTo" id="sellOldCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments5" name="upsellLead[4].upsellComments"></textarea>
					</div>
				  </div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="EXCHANGE myOutCheckbox" name="upsellLead[5].upsellId" value="Buy New Car / Exchange" id="EXCHANGEID" />
					Buy New Car / Exchange</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="EXCHANGEIDSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[5].taggedTo" id="buyNewCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments6" name="upsellLead[5].upsellComments"></textarea>
					</div>
				 	</div>
				  <div class="checkbox">
				  <label>
					<input type="checkbox" class="UsedCar myOutCheckbox" name="upsellLead[6].upsellId" value="UsedCar" id="UsedCarID" />
					Used Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="UsedCarSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[6].taggedTo" id="usedCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
			<textarea class="form-control" rows="1" id="comments7" name="upsellLead[6].upsellComments"></textarea>
					</div>
				 
				</div>
		   
			  </div>
			</div>
			 
			</div> -->
			  <div class="pull-right" >
			<button type="button" class="btn btn-info" id="BackTo3rdDayRate">Back</button>
			  <button type="button" class="btn btn-info" id="NextToUpsellInsu">Next</button>
			</div>
			</div>
		  
			
			 <div style="display:none;" class="row animated  bounceInRight" id="PSFYesNamaskarNoDivH" >
			 	<p>We are extremely sorry for an inconvenience </p>
			 	<p>May I know your Suitable time for your valuable feedback</p>
			 
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpDate">Call Back Date</label>
					  <input type="text" name="psfFollowUpDate" class="datepickerPlus30Days form-control" id="">
					</div>
				  </div>
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpTime">Call Back Time</label>
					  <input type="text" name="psfFollowUpTime" class="single-input form-control" id="">
					</div>
				  </div>
				  <div class="col-md-6">
					<div class="form-group">
					  <label for="followUpTime">Remarks</label>
					  <textarea type="text" name="remarksList[3]" class="form-control" id=""></textarea>
					</div>
				  </div>
				  </br>
				  <div class="pull-right" id="" >
					<button type="button" class="btn btn-info" name="" id="BackToDidUtlakPSFH" >Back</button>
					<button type="submit" class="btn btn-info" name="" id="" >Submit</button>
				</div>
				
			</div>
		  
		   <div style"""),
format.raw("""="display:none;" class="row animated  bounceInRight" id="PSFFeedbackQ" >
		   <div class="col-md-12">
			 	<p>May i take a moment to update your information </p>
			 				 
				  	<div class="radio-inline">
					<label>
					  <input type="radio" name="q12_FeedbackTaken" value="Yes" id="feedbackYes1" data-target="#addBtn" data-toggle="modal">
					  Yes </label>
				</div>
				<div class="radio-inline">
					<label>
					  <input type="radio" name="q12_FeedbackTaken" value="No" id="feedbackNo2" checked>
					 No </label>
				</div>

				
			
			 
			 <div style="display:none;" class="row animated  bounceInRight" id="feedbackPSFNo" >
			 <div class="col-md-12">
			 	<p>Sir, We shall again call you after 12 days to take your feedback. Our Contact No. is <Company Phone No.> till then should you  </p>
				<p>still have any area of concern please let us know. </p>
				<p>Thank you for your valuable feedback and time. Have a nice day. </p>
			 				 </div>
				 
			</div>
							 <div class="pull-right" id=""  >
					<button type="button" class="btn btn-info" name="" id="BackToSirMamDiv" >Back</button>
					<button type="submit" class="btn btn-info" name="" id="SubmitDivMamDiv" >Submit</button>
				</div>
				</div>
		  </div>
		  
		  
		  
			
			<!--panel Group End-->
		</div>
	</div><!--panel Body End-->
   
  </div>
</div>

<script src=""""),_display_(/*723.15*/routes/*723.21*/.Assets.at("javascripts/psf3rdDisposition.js")),format.raw/*723.67*/("""" type="text/javascript"></script>"""))
      }
    }
  }

  def render(vehicleData:Vehicle,userName:String,dealername:String,latestService:Service): play.twirl.api.HtmlFormat.Appendable = apply(vehicleData,userName,dealername,latestService)

  def f:((Vehicle,String,String,Service) => play.twirl.api.HtmlFormat.Appendable) = (vehicleData,userName,dealername,latestService) => apply(vehicleData,userName,dealername,latestService)

  def ref: this.type = this

}


}

/**/
object psf3rdDayDispo extends psf3rdDayDispo_Scope0.psf3rdDayDispo
              /*
                  -- GENERATED --
                  DATE: Sat Dec 16 16:03:44 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/psf3rdDayDispo.scala.html
                  HASH: c2237f02c47a8b841dc821fef8154a9fecff6956
                  MATRIX: 786->1|958->78|988->82|1137->205|1151->211|1216->255|3465->2477|3494->2485|3523->2486|3556->2492|3587->2502|3634->2522|3701->2567|3769->2608|3800->2618|3829->2619|3879->2642|3924->2666|6458->5172|6507->5199|6538->5202|6587->5229|28035->26629|28051->26635|28119->26681
                  LINES: 27->1|32->1|34->3|35->4|35->4|35->4|102->71|102->71|102->71|102->71|102->71|102->71|102->71|103->72|103->72|103->72|103->72|103->72|165->134|165->134|165->134|165->134|755->723|755->723|755->723
                  -- GENERATED --
              */
          