
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object psfListForFeedBackOfService_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class psfListForFeedBackOfService extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealercode:String,dealerName:String,userName:String,oem:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.66*/("""

"""),_display_(/*3.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*3.65*/ {_display_(Seq[Any](format.raw/*3.67*/("""

"""),format.raw/*5.1*/("""<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading"> PSF List For 6th Day FeedBack</div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li id="psf6thData" class="active"><a href="#home" data-toggle="tab" value ="psf6thday" id="Tab1" onclick="assignedInteractionPSFData(result.name);" >6th Day PSF</a> </li>
                    <li><a href="#profile" data-toggle="tab"  id="Tab2" onclick="ajaxCallForFollowUpRequiredPSFData();">Follow Up Required</a> </li>
                    <!-- <li><a href="#messages" data-toggle="tab" id="Tab3" >Incompleted Survey</a> </li> -->
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxCallForCompletedSurveyPSFData();" >Completed Survey</a> </li>
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxCallForNonContactsPSFData();">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxCallForDroppedCallsPSFData();">Dropped</a> </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="assignedInteractionPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>                                    
                                            <th>Vehicle RegNo.</th>
                                            <th>Model</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="tab-pane fade" id="profile">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForFollowUpRequiredPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>                                    
                                            <th>Vehicle RegNo.</th>
                                            <th>FollowUp Date</th>                                    
                                            <th>FollowUp Time</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>






                    <div class="tab-pane fade" id="messages">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForIncompletedSurveyPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>                                    
                                            <th>Vehicle RegNo.</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="tab-pane fade" id="settings">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForCompletedSurveyPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>                                    
                                            <th>Vehicle RegNo.</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="tab-pane fade" id="nonContacts">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForNonContactsPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>                                    
                                            <th>Vehicle RegNo.</th>
                                             <th>Last Disposition</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
                    <div class="tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForDroppedCallsPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>                                    
                                            <th>Vehicle RegNo.</th>
                                             <th>Last Disposition</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>

                    <!-- /.panel --> 
                </div>
                <!-- /.col-lg-12 --> 
            </div>
        </div>
    </div>
</div>

""")))}),format.raw/*163.2*/("""
"""),format.raw/*164.1*/("""<script type="text/javascript">
    window.onload = ajaxRequestPSFDataPageWithTab;
</script>
<script type="text/javascript">
var result = """),format.raw/*168.14*/("""{"""),format.raw/*168.15*/("""name:'psf6thday'"""),format.raw/*168.31*/("""}"""),format.raw/*168.32*/(""";

    function assignedInteractionPSFData(name) """),format.raw/*170.47*/("""{"""),format.raw/*170.48*/("""
        """),format.raw/*171.9*/("""// alert("server side data load");
           // alert(name);

        $('#assignedInteractionPSFData').dataTable("""),format.raw/*174.52*/("""{"""),format.raw/*174.53*/("""
            """),format.raw/*175.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax":"/assignedInteractionTablePSFData"+"/"+name+""
            
        """),format.raw/*184.9*/("""}"""),format.raw/*184.10*/("""),
     
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*186.65*/("""{"""),format.raw/*186.66*/("""
            """),format.raw/*187.13*/("""$($.fn.dataTable.tables(true)).DataTable()
               .columns.adjust();
         """),format.raw/*189.10*/("""}"""),format.raw/*189.11*/(""");


    """),format.raw/*192.5*/("""}"""),format.raw/*192.6*/("""

   
    """),format.raw/*195.5*/("""function ajaxCallForFollowUpRequiredPSFData() """),format.raw/*195.51*/("""{"""),format.raw/*195.52*/("""
    	 """),format.raw/*196.7*/("""var psfDay = 'psf6thday'; 
        //alert("follow up server side data");
        $('#ajaxCallForFollowUpRequiredPSFData').dataTable("""),format.raw/*198.60*/("""{"""),format.raw/*198.61*/("""
            """),format.raw/*199.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForFollowUpRequiredPSFData"+"/"+psfDay+""
        """),format.raw/*207.9*/("""}"""),format.raw/*207.10*/(""");
       
    """),format.raw/*209.5*/("""}"""),format.raw/*209.6*/("""



    """),format.raw/*213.5*/("""function ajaxCallForIncompletedSurveyPSFData() """),format.raw/*213.52*/("""{"""),format.raw/*213.53*/("""
    	 """),format.raw/*214.7*/("""var psfDay = 'psf6thday';
        $('#ajaxCallForIncompletedSurveyPSFData').dataTable("""),format.raw/*215.61*/("""{"""),format.raw/*215.62*/("""
            """),format.raw/*216.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForIncompletedSurveyPSFData"+"/"+psfDay+""
        """),format.raw/*224.9*/("""}"""),format.raw/*224.10*/(""");

    """),format.raw/*226.5*/("""}"""),format.raw/*226.6*/("""

    """),format.raw/*228.5*/("""function ajaxCallForCompletedSurveyPSFData() """),format.raw/*228.50*/("""{"""),format.raw/*228.51*/("""
    	 """),format.raw/*229.7*/("""var psfDay = 'psf6thday'; 
        $('#ajaxCallForCompletedSurveyPSFData').dataTable("""),format.raw/*230.59*/("""{"""),format.raw/*230.60*/("""
            """),format.raw/*231.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForCompletedSurveyPSFData"+"/"+psfDay+""
        """),format.raw/*239.9*/("""}"""),format.raw/*239.10*/(""");


    """),format.raw/*242.5*/("""}"""),format.raw/*242.6*/("""

    """),format.raw/*244.5*/("""function ajaxCallForNonContactsPSFData() """),format.raw/*244.46*/("""{"""),format.raw/*244.47*/("""
    	 """),format.raw/*245.7*/("""var psfDay = 'psf6thday'; 
        $('#ajaxCallForNonContactsPSFData').dataTable("""),format.raw/*246.55*/("""{"""),format.raw/*246.56*/("""
            """),format.raw/*247.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForNonContactsPSFData"+"/"+psfDay+""
        """),format.raw/*255.9*/("""}"""),format.raw/*255.10*/(""");
    """),format.raw/*256.5*/("""}"""),format.raw/*256.6*/("""


    """),format.raw/*259.5*/("""function ajaxCallForDroppedCallsPSFData() """),format.raw/*259.47*/("""{"""),format.raw/*259.48*/("""
    	 """),format.raw/*260.7*/("""var psfDay = 'psf6thday'; 
        $('#ajaxCallForDroppedCallsPSFData').dataTable("""),format.raw/*261.56*/("""{"""),format.raw/*261.57*/("""
            """),format.raw/*262.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForDroppedCallsPSFData"+"/"+psfDay+""
        """),format.raw/*270.9*/("""}"""),format.raw/*270.10*/(""");

    """),format.raw/*272.5*/("""}"""),format.raw/*272.6*/("""

"""),format.raw/*274.1*/("""</script>
"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem)

  def f:((String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem) => apply(dealercode,dealerName,userName,oem)

  def ref: this.type = this

}


}

/**/
object psfListForFeedBackOfService extends psfListForFeedBackOfService_Scope0.psfListForFeedBackOfService
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:37 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/psfListForFeedBackOfService.scala.html
                  HASH: 3e62e4f235bc859f0ee3d4e7464bf4657b0de297
                  MATRIX: 810->1|969->65|999->70|1070->133|1109->135|1139->139|9350->8319|9380->8321|9551->8463|9581->8464|9626->8480|9656->8481|9736->8532|9766->8533|9804->8543|9950->8660|9980->8661|10023->8675|10347->8971|10377->8972|10481->9047|10511->9048|10554->9062|10671->9150|10701->9151|10741->9163|10770->9164|10811->9177|10886->9223|10916->9224|10952->9232|11116->9367|11146->9368|11189->9382|11505->9670|11535->9671|11580->9688|11609->9689|11649->9701|11725->9748|11755->9749|11791->9757|11907->9844|11937->9845|11980->9859|12297->10148|12327->10149|12365->10159|12394->10160|12430->10168|12504->10213|12534->10214|12570->10222|12685->10308|12715->10309|12758->10323|13073->10610|13103->10611|13143->10623|13172->10624|13208->10632|13278->10673|13308->10674|13344->10682|13455->10764|13485->10765|13528->10779|13839->11062|13869->11063|13905->11071|13934->11072|13972->11082|14043->11124|14073->11125|14109->11133|14221->11216|14251->11217|14294->11231|14606->11515|14636->11516|14674->11526|14703->11527|14735->11531
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|194->163|195->164|199->168|199->168|199->168|199->168|201->170|201->170|202->171|205->174|205->174|206->175|215->184|215->184|217->186|217->186|218->187|220->189|220->189|223->192|223->192|226->195|226->195|226->195|227->196|229->198|229->198|230->199|238->207|238->207|240->209|240->209|244->213|244->213|244->213|245->214|246->215|246->215|247->216|255->224|255->224|257->226|257->226|259->228|259->228|259->228|260->229|261->230|261->230|262->231|270->239|270->239|273->242|273->242|275->244|275->244|275->244|276->245|277->246|277->246|278->247|286->255|286->255|287->256|287->256|290->259|290->259|290->259|291->260|292->261|292->261|293->262|301->270|301->270|303->272|303->272|305->274
                  -- GENERATED --
              */
          