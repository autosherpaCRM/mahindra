
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object rosterOfUnAvailibilityOfUserSelectedRange_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class rosterOfUnAvailibilityOfUserSelectedRange extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[Integer,String,String,List[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(row_count:Integer,dealerName:String,user:String,getUsersList:List[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.78*/("""

"""),_display_(/*5.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*5.52*/ {_display_(Seq[Any](format.raw/*5.54*/("""
"""),_display_(/*6.2*/helper/*6.8*/.form(action = routes.CallInteractionController.postUpdateRangeOfUnavailabiltyOfUsers(),'enctype -> "multipart/form-data")/*6.130*/{_display_(Seq[Any](format.raw/*6.131*/("""

"""),format.raw/*8.1*/("""<div class="row"> 
<div class="col-lg-12">
        <div class="panel panel-primary">
      <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
            <h4 class="panel-title accordion-toggle"></h4>
            <span style="width:45px;"/>&nbsp;</span> </div>
      <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
          <ul class="nav nav-tabs nav-justified">
                <li class="active"><a data-toggle="tab" href="#home"><b class="tabFonts">CRE Roster</b></a></li>
                <li><a data-toggle="tab" href="#menu1"><b class="tabFonts">Service Advisor Roster</b></a></li>
                <li><a data-toggle="tab" href="#menu2"><b class="tabFonts">Driver Roster</b></a></li>
              </ul>
          <div class="tab-content scrollable-menu">
                <div id="home" class="tab-pane fade in active"> <br>
                
                      <div class="col-md-12"> 
              
                     <table class="table table-striped table-bordered table-hover" >
              <thead>
                <tr>
                  <th>User Name</th>
                  <th>UnAvailable From</th>
                  <th>UnAvailable To</th>
                </tr>
              </thead>
           <tbody>
                 """),_display_(/*35.19*/for(i <- 0 to row_count) yield /*35.43*/{_display_(Seq[Any](format.raw/*35.44*/("""
                   """),_display_(/*36.21*/if(getUsersList(i).getRole()==("CRE"))/*36.59*/{_display_(Seq[Any](format.raw/*36.60*/("""
                   """),format.raw/*37.20*/("""<tr>
                       <input type="hidden" name="userList["""),_display_(/*38.61*/i),format.raw/*38.62*/("""].role" value=""""),_display_(/*38.78*/getUsersList(i)/*38.93*/.getRole()),format.raw/*38.103*/("""">
                       <input type="hidden" name="userList["""),_display_(/*39.61*/i),format.raw/*39.62*/("""].id" value=""""),_display_(/*39.76*/getUsersList(i)/*39.91*/.getId()),format.raw/*39.99*/("""">
                       <td><input type="text" name="userList["""),_display_(/*40.63*/i),format.raw/*40.64*/("""].userName" style="border:none;" readonly value=""""),_display_(/*40.114*/getUsersList(i)/*40.129*/.getUserName()),format.raw/*40.143*/(""""></td>
                      <td><input type="text" class="datepicker" name="unavaiabiltyList["""),_display_(/*41.89*/i),format.raw/*41.90*/("""].fromDate"></td>
                       <td><input type="text" class="datepicker" name="unavaiabiltyList["""),_display_(/*42.90*/i),format.raw/*42.91*/("""].toDate"></td>
                       
                    </tr>
                    """)))}),format.raw/*45.22*/("""
                   
                """)))}),format.raw/*47.18*/("""  
             """),format.raw/*48.14*/("""</tbody>      
            </table>   
         <button type="submit" class="btn btn-sm btn-primary" id='js-upload-submit' name="selectedFile" value="CRE">UPDATE</button>
                  </div>
     
       </div>
                <div id="menu1" class="tab-pane fade scrollable-menu1"> <br>
              <div class="col-md-12"> 
              
                     <table class="table table-striped table-bordered table-hover" >
              <thead>
                <tr>
                  <th>User Name</th>
                  <th>UnAvailable From</th>
                  <th>UnAvailable To</th>
                </tr>
              </thead>
           <tbody>
                """),_display_(/*66.18*/for(i <- 0 to row_count) yield /*66.42*/{_display_(Seq[Any](format.raw/*66.43*/("""
                  """),_display_(/*67.20*/if(getUsersList(i).getRole().equals("Service Advisor"))/*67.75*/{_display_(Seq[Any](format.raw/*67.76*/("""
                    """),format.raw/*68.21*/("""<tr> <input type="hidden" name="userList["""),_display_(/*68.63*/i),format.raw/*68.64*/("""].role" value=""""),_display_(/*68.80*/getUsersList(i)/*68.95*/.getRole()),format.raw/*68.105*/("""">
                        <input type="hidden" name="userList["""),_display_(/*69.62*/i),format.raw/*69.63*/("""].id" value=""""),_display_(/*69.77*/getUsersList(i)/*69.92*/.getId()),format.raw/*69.100*/("""">
                         <td><input type="text" name="userList["""),_display_(/*70.65*/i),format.raw/*70.66*/("""].userName" style="border:none;" readonly value=""""),_display_(/*70.116*/getUsersList(i)/*70.131*/.getUserName()),format.raw/*70.145*/(""""></td>
                         <td><input type="text" class="datepicker" name="unavaiabiltyList["""),_display_(/*71.92*/i),format.raw/*71.93*/("""].fromDate"></td>
                       <td><input type="text" class="datepicker" name="unavaiabiltyList["""),_display_(/*72.90*/i),format.raw/*72.91*/("""].toDate"></td>
                    </tr>
                    """)))}),format.raw/*74.22*/("""
                """)))}),format.raw/*75.18*/("""
                  
             """),format.raw/*77.14*/("""</tbody>      
            </table> 
                  
                <button type="submit" class="btn btn-sm btn-primary" id='js-upload-submit' name="selectedFile" value="ServiceAdvisor">UPDATE</button>
                  
                  </div>
            </div>
                <div id="menu2" class="tab-pane fade scrollable-menu2"> <br>
                     <div class="col-md-12"> 
              
                     <table class="table table-striped table-bordered table-hover" >
              <thead>
                <tr>
                  <th>User Name</th>
                  <th>UnAvailable From</th>
                  <th>UnAvailable To</th>                  
                </tr>
              </thead>
             <tbody>
                
                 """),_display_(/*97.19*/for(i <- 0 to row_count) yield /*97.43*/{_display_(Seq[Any](format.raw/*97.44*/("""
                  """),_display_(/*98.20*/if(getUsersList(i).getRole().equals("Driver"))/*98.66*/{_display_(Seq[Any](format.raw/*98.67*/("""
                 
                    """),format.raw/*100.21*/("""<tr>
                        <input type="hidden" name="userList["""),_display_(/*101.62*/i),format.raw/*101.63*/("""].role" value=""""),_display_(/*101.79*/getUsersList(i)/*101.94*/.getRole()),format.raw/*101.104*/("""">
                        <input type="hidden" name="userList["""),_display_(/*102.62*/i),format.raw/*102.63*/("""].id" value=""""),_display_(/*102.77*/getUsersList(i)/*102.92*/.getId()),format.raw/*102.100*/("""">
                       <td><input type="text" name="userList["""),_display_(/*103.63*/i),format.raw/*103.64*/("""].userName" style="border:none;" value=""""),_display_(/*103.105*/getUsersList(i)/*103.120*/.getUserName()),format.raw/*103.134*/("""" readonly></td>
                       <td><input type="text" class="datepicker" name="unavaiabiltyList["""),_display_(/*104.90*/i),format.raw/*104.91*/("""].fromDate"></td>
                       <td><input type="text" class="datepicker" name="unavaiabiltyList["""),_display_(/*105.90*/i),format.raw/*105.91*/("""].toDate"></td>
                    </tr>
                    """)))}),format.raw/*107.22*/("""
                """)))}),format.raw/*108.18*/(""" 
             
             """),format.raw/*110.14*/("""</tbody>     
            </table> 
             <button type="submit" class="btn btn-sm btn-primary" id='js-upload-submit' name="selectedFile" value="Driver">UPDATE</button>
                  </div>
                  
                
            </div>
            
              </div>
        </div>
          </div>
    </div>
      </div>
    </div>
  
""")))}),format.raw/*125.2*/("""
""")))}),format.raw/*126.2*/(""" """))
      }
    }
  }

  def render(row_count:Integer,dealerName:String,user:String,getUsersList:List[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(row_count,dealerName,user,getUsersList)

  def f:((Integer,String,String,List[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (row_count,dealerName,user,getUsersList) => apply(row_count,dealerName,user,getUsersList)

  def ref: this.type = this

}


}

/**/
object rosterOfUnAvailibilityOfUserSelectedRange extends rosterOfUnAvailibilityOfUserSelectedRange_Scope0.rosterOfUnAvailibilityOfUserSelectedRange
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:38 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/rosterOfUnAvailibilityOfUserSelectedRange.scala.html
                  HASH: acde8b28e315a51c8e39b5bf6474aafacd1d1270
                  MATRIX: 846->5|1017->81|1047->86|1105->136|1144->138|1172->141|1185->147|1316->269|1355->270|1385->274|2782->1644|2822->1668|2861->1669|2910->1691|2957->1729|2996->1730|3045->1751|3138->1817|3160->1818|3203->1834|3227->1849|3259->1859|3350->1923|3372->1924|3413->1938|3437->1953|3466->1961|3559->2027|3581->2028|3659->2078|3684->2093|3720->2107|3844->2204|3866->2205|4001->2313|4023->2314|4144->2404|4215->2444|4260->2461|4984->3158|5024->3182|5063->3183|5111->3204|5175->3259|5214->3260|5264->3282|5333->3324|5355->3325|5398->3341|5422->3356|5454->3366|5546->3431|5568->3432|5609->3446|5633->3461|5663->3469|5758->3537|5780->3538|5858->3588|5883->3603|5919->3617|6046->3717|6068->3718|6203->3826|6225->3827|6321->3892|6371->3911|6434->3946|7258->4743|7298->4767|7337->4768|7385->4789|7440->4835|7479->4836|7549->4877|7644->4944|7667->4945|7711->4961|7736->4976|7769->4986|7862->5051|7885->5052|7927->5066|7952->5081|7983->5089|8077->5155|8100->5156|8170->5197|8196->5212|8233->5226|8368->5333|8391->5334|8527->5442|8550->5443|8647->5508|8698->5527|8758->5558|9164->5933|9198->5936
                  LINES: 27->3|32->3|34->5|34->5|34->5|35->6|35->6|35->6|35->6|37->8|64->35|64->35|64->35|65->36|65->36|65->36|66->37|67->38|67->38|67->38|67->38|67->38|68->39|68->39|68->39|68->39|68->39|69->40|69->40|69->40|69->40|69->40|70->41|70->41|71->42|71->42|74->45|76->47|77->48|95->66|95->66|95->66|96->67|96->67|96->67|97->68|97->68|97->68|97->68|97->68|97->68|98->69|98->69|98->69|98->69|98->69|99->70|99->70|99->70|99->70|99->70|100->71|100->71|101->72|101->72|103->74|104->75|106->77|126->97|126->97|126->97|127->98|127->98|127->98|129->100|130->101|130->101|130->101|130->101|130->101|131->102|131->102|131->102|131->102|131->102|132->103|132->103|132->103|132->103|132->103|133->104|133->104|134->105|134->105|136->107|137->108|139->110|154->125|155->126
                  -- GENERATED --
              */
          