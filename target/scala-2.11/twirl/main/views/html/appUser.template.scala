
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object appUser_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class appUser extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[String,String,List[models.Dealer],List[WyzUser],List[WyzUser],play.data.Form[WyzUser],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(dealerName:String,userName:String,dealerdata:List[models.Dealer],credata:List[WyzUser],saledata:List[WyzUser],form:play.data.Form[WyzUser],listUsers:List[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.165*/("""
"""),_display_(/*3.2*/main("AutoSherpaCRM",userName,dealerName)/*3.43*/ {_display_(Seq[Any](format.raw/*3.45*/("""
"""),_display_(/*4.2*/helper/*4.8*/.form(action = routes.WyzUserController.addApplicationUser,'id -> "formSub")/*4.84*/ {_display_(Seq[Any](format.raw/*4.86*/("""
"""),format.raw/*5.1*/("""<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">User Creation </h3>
      </div>
      <div class="panel-body">
        <form >
          <div class='row'>
            <div class='col-sm-4'>
           <select id="checkUserName" style="display:none">
           
                 """),_display_(/*17.19*/for(checkData <- listUsers) yield /*17.46*/{_display_(Seq[Any](format.raw/*17.47*/("""
		
		 """),format.raw/*19.4*/("""<option value=""""),_display_(/*19.20*/checkData),format.raw/*19.29*/("""">"""),_display_(/*19.32*/checkData),format.raw/*19.41*/("""</option>
				""")))}),format.raw/*20.6*/("""
                """),format.raw/*21.17*/("""</select>
                
             <div class='form-group'>
             
                <label for="firstName">First Name : </label>  
                 <div class="input-group" data-validate="userName">      		
         		<input type="text" class="form-control" name="firstName" placeholder="Enter First Name ..." title="Your FirstName is Required" required>
         		
         		<span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span> </div>                		
                 </div>
               <div class='form-group'>
                <label for="lastName">Last Name :</label> 
                  <div class="input-group" data-validate="userName">        		
         		<input type="text" class="form-control" name="lastName" placeholder="Enter Last Name..." title="Your LastName is Required" required> 
         		<span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span> </div>                 		
                 </div>    	      
	           
             <div class="form-group">
		        <label for="phoneNumber">Phone No:</label>
        		<div class="input-group" data-validate="phoneNumber">
         		<input type="text" class="form-control" name="phoneNumber" placeholder="(814) 555-1234" title="10 Digits Mobile No is Required" required>
          		<span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span> </div>
     		 </div>
            </div>
            <div class='col-sm-4'>
            <div class="form-group">
		        <label for="phoneIMEINo">PhoneIMEI No:</label>
        		<div class="input-group" data-validate="phoneIMEINo">
         		<input type="text" class="form-control" name="phoneIMEINo" placeholder="Enter PhoneIMEI No..." title="15 Digits IMEINo is Required"required>
          		<span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span> </div>
     		 </div>
              <div class="form-group">
       				 <label for="emailId">Email</label>
        			 <div class="input-group" data-validate="emailId">
          			 <input type="text" class="form-control" name="emailId"  placeholder="Validate Email" title="You can use letters,Numbers and periods with @gmail.com" required>
         			 <span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span> </div>
      			</div>
      			<div class='form-group'>
                <label for="userName">User Name :</label>     
                <div class="input-group" data-validate="userName" >   		
         		<input type="text" class="form-control" name="userName" placeholder="Enter User Name..." onChange="validatingDuplicateUserName();" title="Add 7 Digits(min) only with a number, a lowercase and a uppercase" required>  
         		<span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span> </div>        		
                 </div> 
                     		    
                    
             
            </div>
            <div class='col-sm-4' >
            <div class='form-group'>
                <label for="password">Password :</label>  
                 <div class="input-group" data-validate="password">        		
         		<input type="password" class="form-control" name="password" placeholder="Enter Password..." title="Add 7 Digits(min) only with a number, a lowercase and a uppercase" required>   
         		<span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span> </div>        		
                 </div>  
              <div class='form-group'>
                <label >Role :</label>
                <select  id="role" name="role" class="form-control" onChange="dropdownChage();">
                  <option value="Select">Select Role</option>                  
                  <option value="CRE">CRE</option>
                  <option value="CREManager">CREManager</option>
                   <option value="SalesManager">SalesManager</option>
                    <option value="SalesExecutive">SalesExecutive</option>
                </select>
              </div>
              <div class='form-group' id="dealerCodeId" style="display:none">
              
                <label >Dealer Code :</label>
                <select  id="dealerCode" name="dealerCode" class="form-control">
                       	"""),_display_(/*89.26*/for(data <- dealerdata) yield /*89.49*/{_display_(Seq[Any](format.raw/*89.50*/("""                             
                          """),format.raw/*90.27*/("""<option value=""""),_display_(/*90.43*/data/*90.47*/.dealerCode),format.raw/*90.58*/("""">"""),_display_(/*90.61*/data/*90.65*/.dealerCode),format.raw/*90.76*/("""</option>
                      	""")))}),format.raw/*91.25*/("""
                """),format.raw/*92.17*/("""</select>
              </div>
              
             
              
              <div class='form-group' id="creManagerId" style="display:none">
              
                <label >CRE Manager :</label>
                <select  id="creManager" name="creManager" class="form-control">
                       	"""),_display_(/*101.26*/for(data <- credata) yield /*101.46*/{_display_(Seq[Any](format.raw/*101.47*/("""                             
                          """),format.raw/*102.27*/("""<option value=""""),_display_(/*102.43*/data/*102.47*/.getUserName()),format.raw/*102.61*/("""">"""),_display_(/*102.64*/data/*102.68*/.getUserName()),format.raw/*102.82*/("""</option>
                      	""")))}),format.raw/*103.25*/("""
                """),format.raw/*104.17*/("""</select>
              </div>
              
              <div class='form-group' id="salesManagerId" style="display:none">
              
                <label >Sales Manager :</label>
                <select  id="singleData" name="singleData" class="form-control">
                       	"""),_display_(/*111.26*/for(data <- saledata) yield /*111.47*/{_display_(Seq[Any](format.raw/*111.48*/("""                             
                          """),format.raw/*112.27*/("""<option value=""""),_display_(/*112.43*/data/*112.47*/.getUserName()),format.raw/*112.61*/("""">"""),_display_(/*112.64*/data/*112.68*/.getUserName()),format.raw/*112.82*/("""</option>
                      	""")))}),format.raw/*113.25*/("""
                """),format.raw/*114.17*/("""</select>
              </div>
              
              
              <div class='text-right'>
              <button type="submit" class="btn btn-primary pull-right" onclick="return validatingDuplicateUserName();" enabled>Submit</button>
                
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div> 
        """)))}),format.raw/*129.10*/("""
        

""")))}))
      }
    }
  }

  def render(dealerName:String,userName:String,dealerdata:List[models.Dealer],credata:List[WyzUser],saledata:List[WyzUser],form:play.data.Form[WyzUser],listUsers:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(dealerName,userName,dealerdata,credata,saledata,form,listUsers)

  def f:((String,String,List[models.Dealer],List[WyzUser],List[WyzUser],play.data.Form[WyzUser],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (dealerName,userName,dealerdata,credata,saledata,form,listUsers) => apply(dealerName,userName,dealerdata,credata,saledata,form,listUsers)

  def ref: this.type = this

}


}

/**/
object appUser extends appUser_Scope0.appUser
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:11 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/appUser.scala.html
                  HASH: 3c2c7d753b272cfc569689b51e97f3bb0400f1f4
                  MATRIX: 841->3|1100->166|1128->169|1177->210|1216->212|1244->215|1257->221|1341->297|1380->299|1408->301|1828->694|1871->721|1910->722|1946->731|1989->747|2019->756|2049->759|2079->768|2125->784|2171->802|6665->5270|6704->5293|6743->5294|6828->5351|6871->5367|6884->5371|6916->5382|6946->5385|6959->5389|6991->5400|7057->5435|7103->5453|7460->5782|7497->5802|7537->5803|7623->5860|7667->5876|7681->5880|7717->5894|7748->5897|7762->5901|7798->5915|7865->5950|7912->5968|8242->6270|8280->6291|8320->6292|8406->6349|8450->6365|8464->6369|8500->6383|8531->6386|8545->6390|8581->6404|8648->6439|8695->6457|9125->6855
                  LINES: 27->2|32->2|33->3|33->3|33->3|34->4|34->4|34->4|34->4|35->5|47->17|47->17|47->17|49->19|49->19|49->19|49->19|49->19|50->20|51->21|119->89|119->89|119->89|120->90|120->90|120->90|120->90|120->90|120->90|120->90|121->91|122->92|131->101|131->101|131->101|132->102|132->102|132->102|132->102|132->102|132->102|132->102|133->103|134->104|141->111|141->111|141->111|142->112|142->112|142->112|142->112|142->112|142->112|142->112|143->113|144->114|159->129
                  -- GENERATED --
              */
          