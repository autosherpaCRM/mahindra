
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object psfListNextDayFeedBack_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class psfListNextDayFeedBack extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealercode:String,dealerName:String,userName:String,oem:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.66*/("""

"""),_display_(/*3.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*3.65*/ {_display_(Seq[Any](format.raw/*3.67*/("""

"""),format.raw/*5.1*/("""<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading"> PSF List For Next Day FeedBack</div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li id="psf6thData" class="active"><a href="#home" data-toggle="tab" value ="psf1stday" id="Tab1" onclick="assignedInteractionPSFData(result.name);" >1st Day PSF</a> </li>
                    <li><a href="#profile" data-toggle="tab"  id="Tab2" onclick="ajaxCallForFollowUpRequiredPSFData();">Follow Up Required</a> </li>
                    <!-- <li><a href="#messages" data-toggle="tab" id="Tab3" >Incompleted Survey</a> </li> -->
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxCallForCompletedSurveyPSFData();" >Completed Survey</a> </li>
                    <li><a href="#appointments" data-toggle="tab" id="Tab8" onclick="ajaxCallForAppointmentPSFData();">Appointments</a></li>
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxCallForNonContactsPSFData();">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxCallForDroppedCallsPSFData();">Dropped</a> </li>
                   <!--  <li><a href="#incompleteSurvey" data-toggle="tab" id="Tab7" onclick="ajaxCallForIncompletedSurveyPSFData();">Incomplete Survey</a></li> -->
                    
                </ul>
                <div class="tab-content">
                      <div class="row">
					<div class="col-md-3" id="SchedulefromBillDate" style="display:none">
					<label>From Bill Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" id="frombilldaterange" name="frombilldaterange" readonly>
				  </div>
				   <div class="col-md-3" id="ScheduletoBillDate" style="display:none">
					<label>To Bill Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="2" id="tobilldaterange" name="tobilldaterange" readonly>
				  </div>
				
				</div>
				    <div class="row" id="forschedule">
					<div class="col-md-3" id="fromCallDate" style="display:none">
					<label>From Call Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" id="fromCalldaterange22" name="frombilldaterange22" readonly>
				  </div>
				   <div class="col-md-3" id="toCallDate" style="display:none">
					<label>To Call Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="2" id="tobCalldaterange33" name="tobilldaterange33" readonly>
				  </div>
				</div>
                    <div class="tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="assignedInteractionPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th> 
                                            <th>Mobile Number</th>                                   
                                            <th>Vehicle RegNo.</th>
                                            <th>Model</th>
                                            <th>RO Number</th>
                                            <th>RO Date</th>
                                            <th>Bill Date</th>
                                            <th>Category</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="tab-pane fade" id="profile">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForFollowUpRequiredPSFData">
                                    <thead>
                                        <tr>                                    
											<th>FollowUp Date</th>
											<th>FollowUp Time</th>
											<th>call_date</th>
											<th>Customer Name</th>
											<th>Mobile Number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>






                    <div class="tab-pane fade" id="incompleteSurvey">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForIncompletedSurveyPSFData">
                                    <thead>
                                         <tr>                                    
											<th>FollowUp Date</th>
											<th>FollowUp Time</th>
											<th>call_date</th>
											<th>Customer Name</th>
											<th>Mobile Number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="tab-pane fade" id="settings">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForCompletedSurveyPSFData">
                                    <thead>
                                        <tr>                                    
                                           	<th>Survey Date</th>																	
											<th>Customer Name</th>																	
											<th>Category</th>																	
											<th>vehicle RegNo</th>																	
											<th>Model</th>																	
											<th>RO Date</th>																	
											<th>RO Number</th>																	
											<th>Bill Date</th>
											<th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="tab-pane fade" id="nonContacts">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForNonContactsPSFDataCalls">
                                    <thead>
                                        <tr>                                   
											<th>Customer Name</th>
											<th>Category</th>
											<th>Mobile number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
											<th>Last Disposition</th>
											<th>Call Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
                    <div class="tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForDroppedCallsPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>
											<th>Category</th>
											<th>Mobile number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
											<th>Last Disposition</th>
											<th>Call Date</th>
											
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
                    <div class="tab-pane fade" id="appointments">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForAppointmentPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>
											<th>Vehicle RegNo</th>
											<th>Appointment Date</th>
											<th>Mobile number</th>
											<th>Call Date</th>
											<th>Appointment Time</th>
											<th>RO Number</th>
											<th>RO Date</th>
											<th>Bill Date</th>
											<th>Survey date</th>
											<th>Model</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>

                    <!-- /.panel --> 
                </div>
                <!-- /.col-lg-12 --> 
            </div>
        </div>
    </div>
</div>

""")))}),format.raw/*254.2*/("""
"""),format.raw/*255.1*/("""<script type="text/javascript">
    window.onload = ajaxRequestPSFDataPageWithTabNextDay;
</script>
<script>

function FilterOptionData(table)"""),format.raw/*260.33*/("""{"""),format.raw/*260.34*/("""
	"""),format.raw/*261.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*262.39*/("""{"""),format.raw/*262.40*/("""
    	"""),format.raw/*263.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
		console.log(v);
		console.log(i);
	    
	    if(v.length>0)"""),format.raw/*269.20*/("""{"""),format.raw/*269.21*/("""
		    	   
		   """),format.raw/*271.6*/("""values[i] = v;
	    
	    """),format.raw/*273.6*/("""}"""),format.raw/*273.7*/("""
	    
	    	"""),format.raw/*275.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');

	 	 if(values.length>0) """),format.raw/*280.25*/("""{"""),format.raw/*280.26*/("""
			 """),format.raw/*281.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*281.37*/("""{"""),format.raw/*281.38*/("""	
			    """),format.raw/*282.8*/("""table.api().columns(i).search(values[i]);
			 """),format.raw/*283.5*/("""}"""),format.raw/*283.6*/("""
			 """),format.raw/*284.5*/("""table.api().draw();
			 """),format.raw/*285.5*/("""}"""),format.raw/*285.6*/(""" 
		 	  
	"""),format.raw/*287.2*/("""}"""),format.raw/*287.3*/(""");
	
	
"""),format.raw/*290.1*/("""}"""),format.raw/*290.2*/("""
"""),format.raw/*291.1*/("""</script>
<script type="text/javascript">
var result = """),format.raw/*293.14*/("""{"""),format.raw/*293.15*/("""name:'psf1stday'"""),format.raw/*293.31*/("""}"""),format.raw/*293.32*/(""";

    function assignedInteractionPSFData(name) """),format.raw/*295.47*/("""{"""),format.raw/*295.48*/("""
    	"""),format.raw/*296.6*/("""$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "block";
    	document.getElementById("ScheduletoBillDate").style.display = "block";
    	document.getElementById("fromCallDate").style.display = "none";
    	document.getElementById("toCallDate").style.display = "none";
        var table = $('#assignedInteractionPSFData').dataTable("""),format.raw/*304.64*/("""{"""),format.raw/*304.65*/("""
            """),format.raw/*305.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax":"/assignedInteractionTablePSFData"+"/"+name+""
        """),format.raw/*313.9*/("""}"""),format.raw/*313.10*/(""");
        FilterOptionData(table);
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*315.65*/("""{"""),format.raw/*315.66*/("""
            """),format.raw/*316.13*/("""$($.fn.dataTable.tables(true)).DataTable()
               .columns.adjust();
         """),format.raw/*318.10*/("""}"""),format.raw/*318.11*/(""");
    """),format.raw/*319.5*/("""}"""),format.raw/*319.6*/("""

   
    """),format.raw/*322.5*/("""function ajaxCallForFollowUpRequiredPSFData(name) """),format.raw/*322.55*/("""{"""),format.raw/*322.56*/("""
    	 """),format.raw/*323.7*/("""var psfDay = 'psf1stday'; 
        //alert("follow up server side data");
        $("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
        var table =$('#ajaxCallForFollowUpRequiredPSFData').dataTable("""),format.raw/*333.71*/("""{"""),format.raw/*333.72*/("""
            """),format.raw/*334.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForFollowUpRequiredPSFData"+"/"+psfDay+""
        """),format.raw/*342.9*/("""}"""),format.raw/*342.10*/(""");
        FilterOptionData(table);
    """),format.raw/*344.5*/("""}"""),format.raw/*344.6*/("""



    """),format.raw/*348.5*/("""function ajaxCallForIncompletedSurveyPSFData(name) """),format.raw/*348.56*/("""{"""),format.raw/*348.57*/("""
    	"""),format.raw/*349.6*/("""$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
       var psfDay = 'psf1stday';
       var table = $('#ajaxCallForIncompletedSurveyPSFData').dataTable("""),format.raw/*358.72*/("""{"""),format.raw/*358.73*/("""
            """),format.raw/*359.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForIncompletedSurveyPSFData"+"/"+psfDay+""
        """),format.raw/*367.9*/("""}"""),format.raw/*367.10*/(""");
        FilterOptionData(table);
    """),format.raw/*369.5*/("""}"""),format.raw/*369.6*/("""

    """),format.raw/*371.5*/("""function ajaxCallForCompletedSurveyPSFData(name) """),format.raw/*371.54*/("""{"""),format.raw/*371.55*/("""
      	 """),format.raw/*372.9*/("""var psfDay = 'psf1stday'; 
         
      	$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
		var table = $('#ajaxCallForCompletedSurveyPSFData').dataTable("""),format.raw/*382.65*/("""{"""),format.raw/*382.66*/("""
            """),format.raw/*383.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForCompletedSurveyPSFData"+"/"+psfDay+""
        """),format.raw/*391.9*/("""}"""),format.raw/*391.10*/(""");
        FilterOptionData(table);
    """),format.raw/*393.5*/("""}"""),format.raw/*393.6*/("""

    
    """),format.raw/*396.5*/("""function ajaxCallForAppointmentPSFData(name) """),format.raw/*396.50*/("""{"""),format.raw/*396.51*/("""
    	"""),format.raw/*397.6*/("""$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
       var psfDay = 'psf1stday'; 
       var table = $('#ajaxCallForAppointmentPSFData').dataTable("""),format.raw/*406.66*/("""{"""),format.raw/*406.67*/("""
            """),format.raw/*407.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForAppointmentPSFData"+"/"+psfDay+""
        """),format.raw/*415.9*/("""}"""),format.raw/*415.10*/(""");
        FilterOptionData(table);
    """),format.raw/*417.5*/("""}"""),format.raw/*417.6*/("""

    
    """),format.raw/*420.5*/("""function ajaxCallForNonContactsPSFData(name) """),format.raw/*420.50*/("""{"""),format.raw/*420.51*/("""
    	"""),format.raw/*421.6*/("""$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
    	var psfDay = 'psf1stday'; 
        var table =$('#ajaxCallForNonContactsPSFDataCalls').dataTable("""),format.raw/*430.71*/("""{"""),format.raw/*430.72*/("""
            """),format.raw/*431.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForNonContactsPSFData"+"/"+psfDay+""
        """),format.raw/*439.9*/("""}"""),format.raw/*439.10*/(""");
        FilterOptionData(table);
    """),format.raw/*441.5*/("""}"""),format.raw/*441.6*/("""

    
    """),format.raw/*444.5*/("""function ajaxCallForDroppedCallsPSFData(name) """),format.raw/*444.51*/("""{"""),format.raw/*444.52*/("""
    	"""),format.raw/*445.6*/("""$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
    	var psfDay = 'psf1stday'; 
        var table = $('#ajaxCallForDroppedCallsPSFData').dataTable("""),format.raw/*454.68*/("""{"""),format.raw/*454.69*/("""
            """),format.raw/*455.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForDroppedCallsPSFData"+"/"+psfDay+""
        """),format.raw/*463.9*/("""}"""),format.raw/*463.10*/(""");
        FilterOptionData(table);
    """),format.raw/*465.5*/("""}"""),format.raw/*465.6*/("""

"""),format.raw/*467.1*/("""</script>
"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem)

  def f:((String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem) => apply(dealercode,dealerName,userName,oem)

  def ref: this.type = this

}


}

/**/
object psfListNextDayFeedBack extends psfListNextDayFeedBack_Scope0.psfListNextDayFeedBack
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:37 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/psfListNextDayFeedBack.scala.html
                  HASH: 52197833f91bd8a61a597e1c2dfbecf422400933
                  MATRIX: 800->1|959->65|989->70|1060->133|1099->135|1129->139|12962->11941|12992->11943|13168->12090|13198->12091|13229->12094|13314->12150|13344->12151|13379->12158|13590->12340|13620->12341|13667->12360|13723->12388|13752->12389|13795->12404|13996->12576|14026->12577|14060->12583|14121->12615|14151->12616|14189->12626|14264->12673|14293->12674|14327->12680|14380->12705|14409->12706|14449->12718|14478->12719|14516->12729|14545->12730|14575->12732|14661->12789|14691->12790|14736->12806|14766->12807|14846->12858|14876->12859|14911->12866|15454->13380|15484->13381|15527->13395|15837->13677|15867->13678|15998->13780|16028->13781|16071->13795|16188->13883|16218->13884|16254->13892|16283->13893|16324->13906|16403->13956|16433->13957|16469->13965|17103->14570|17133->14571|17176->14585|17492->14873|17522->14874|17592->14916|17621->14917|17661->14929|17741->14980|17771->14981|17806->14988|18391->15544|18421->15545|18464->15559|18781->15848|18811->15849|18881->15891|18910->15892|18946->15900|19024->15949|19054->15950|19092->15960|19682->16521|19712->16522|19755->16536|20070->16823|20100->16824|20170->16866|20199->16867|20241->16881|20315->16926|20345->16927|20380->16934|20960->17485|20990->17486|21033->17500|21344->17783|21374->17784|21444->17826|21473->17827|21515->17841|21589->17886|21619->17887|21654->17894|22237->18448|22267->18449|22310->18463|22621->18746|22651->18747|22721->18789|22750->18790|22792->18804|22867->18850|22897->18851|22932->18858|23512->19409|23542->19410|23585->19424|23897->19708|23927->19709|23997->19751|24026->19752|24058->19756
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|285->254|286->255|291->260|291->260|292->261|293->262|293->262|294->263|300->269|300->269|302->271|304->273|304->273|306->275|311->280|311->280|312->281|312->281|312->281|313->282|314->283|314->283|315->284|316->285|316->285|318->287|318->287|321->290|321->290|322->291|324->293|324->293|324->293|324->293|326->295|326->295|327->296|335->304|335->304|336->305|344->313|344->313|346->315|346->315|347->316|349->318|349->318|350->319|350->319|353->322|353->322|353->322|354->323|364->333|364->333|365->334|373->342|373->342|375->344|375->344|379->348|379->348|379->348|380->349|389->358|389->358|390->359|398->367|398->367|400->369|400->369|402->371|402->371|402->371|403->372|413->382|413->382|414->383|422->391|422->391|424->393|424->393|427->396|427->396|427->396|428->397|437->406|437->406|438->407|446->415|446->415|448->417|448->417|451->420|451->420|451->420|452->421|461->430|461->430|462->431|470->439|470->439|472->441|472->441|475->444|475->444|475->444|476->445|485->454|485->454|486->455|494->463|494->463|496->465|496->465|498->467
                  -- GENERATED --
              */
          