
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object psf4thDayDisposition_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class psf4thDayDisposition extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[Service,Vehicle,String,String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(service:Service,vehicleData:Vehicle,userName:String,dealerName:String,psfAgeDiv:Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.88*/("""

"""),format.raw/*3.1*/("""<div class="panel panel-primary">
    <div class="panel-heading" align="center"; style="background-color:#1797be; color: #fff;"><img src=""""),_display_(/*4.106*/routes/*4.112*/.Assets.at("images/INTERACTION HISTORY.png")),format.raw/*4.156*/("""" style="width:17px"/>&nbsp;<b>Mahindra PSF Form,   
    
    	"""),_display_(/*6.7*/if(vehicleData.isCommercialVehicle())/*6.44*/{_display_(Seq[Any](format.raw/*6.45*/("""
    		"""),format.raw/*7.7*/("""Commercial Vehicle    	
    	
    	""")))}/*9.7*/else/*9.11*/{_display_(Seq[Any](format.raw/*9.12*/("""
    	
    	"""),_display_(/*11.7*/if(vehicleData.getAge_of_vehicle()<=730)/*11.47*/{_display_(Seq[Any](format.raw/*11.48*/("""
    	
    	"""),format.raw/*13.6*/("""VEHICLE AGE IS less than or equal to 2 YRS
    	""")))}/*14.7*/else/*14.11*/{_display_(Seq[Any](format.raw/*14.12*/("""
    	
    	"""),format.raw/*16.6*/("""VEHICLE AGE IS greater than 2 YRS
    	""")))}),format.raw/*17.7*/("""
    	
    	""")))}),format.raw/*19.7*/("""  """),format.raw/*19.9*/("""</b> 
		</div>  	
    <div class="panel-body">
		<div class="col-md-12">
			<div class="form-group">
		<div id="mahindraAgeing1" class="animated  bounceInRight">
		  <p>Are you able to connect with the customer?</p>	
		  
		  <div class="radio-inline">
			<label>
			  <input type="radio" name="isContacted" value="Yes" id="">
			  Yes </label>
		  </div>
		  <div class="radio-inline">
			<label>
			  <input type="radio" name="isContacted" value="No" id="" >
			  No</label>
		  </div>
		 </div>
		 
		 <div style="display:none;" class="animated  bounceInRight" id="mahinAngNotConnect">
			<p>Choose reason for not being able to talk to customer:</b>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="Ringing No response" id="">
				Ringing No response </label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="Busy" id="" >
				Busy</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="Switched Off / Unreachable" id="" >
				Switched Off / Unreachable</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="Invalid Number" id="" >
				Invalid Number</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="NoOther" id="MaruthiOthersCheck" >
				Other</label>
			</div>
			
			
			<div class="row animated  bounceInRight"  id="psf4DayOthers" style="display:none;">
			<div class="col-md-3">
			<label><b>Remarks</b></label>
			<textarea type="text" class="form-control Psf4thdayRemarks" rows="1" name="OtherComments"></textarea>
			</div>
			</div>
			<div class="pull-right">
						
					<button type="button" class="btn btn-info" name="" id="BackToAgeing1stQ">Back</button>
					<button type="submit" class="btn btn-info" name="" id="psf4dayNoContact">Submit</button>
			</div>
		  </div>
		  
		   <div style="display:none;" class="animated  bounceInRight" id="MahiiAng1GoodM" >
			
			<div class="">
			
			<p>Mahindra Namaksar ! Sir/Madam, I am <b>"""),_display_(/*85.47*/userName),format.raw/*85.55*/(""" """),format.raw/*85.56*/("""</b>, Calling from <b>"""),_display_(/*85.79*/dealerName),format.raw/*85.89*/("""</b>, Am I speaking to Mr./Ms. <b>"""),_display_(/*85.124*/{vehicleData.getCustomer().getCustomerName()}),format.raw/*85.169*/("""</b> ? </p>
		 <div class="radio-inline">
			<label>
			  <input type="radio" name="qM4_confirmingCustomer" value="Yes" id="goodYesAgeing">
			  Yes </label>
		  </div>
		   <div class="radio-inline">
			<label>
			  <input type="radio" name="qM4_confirmingCustomer" value="No" id="goodNoAgeing" data-target="#addBtn" data-toggle="modal" data-backdrop="static" data-keyboard="false" >
			  No </label>
		  </div>
		  </div>
		   <div class="pull-right">
					<button type="button" class="btn btn-info" id="BackTo1stMQAge">Back</button>						
					<button type="button" class="btn btn-info" id="NextToQuest3Age">Next</button>
			</div>
		  </div>
		  
		  <div style="display:none;" class="row animated  bounceInRight" id="ManhinNotAgeing">
		    <div class="col-md-12">
			 	<p>Sorry to bother you. May I know the suitable time when I can speak to Mr./Ms. <b>"""),_display_(/*105.91*/{vehicleData.getCustomer().getCustomerName()}),format.raw/*105.136*/("""</b></p>
			 </div>
			 <div class="pull-right">
					<button type="button" class="btn btn-info" id="BackTo2ndQClLatAgeing">Back</button>						
					<button type="submit" class="btn btn-info" id="psf4thDay2ndsubmit">Submit</button>
			</div>
			</div>
			
				 <div style="display:none;" class="row animated  bounceInRight" id="agingPSF3rdQ">
		    <div class="col-md-12">
			 	<p>Sorry to bother you. May I know the suitable time when I can speak to you</p>
			 
				  <div class="pull-right">
					<button type="button" class="btn btn-info" id="BackTo3rdQMAhisAgeing">Back</button>						
					<button type="submit" class="btn btn-info" id="Samepsf4thDay2ndsubmit">Submit</button>
			</div>
				
			</div>
			</div>
			 <div class="" style="display:none" id="FollowUpDateDiv" >
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpDate">Follow Up Date</label>
					  <input type="text" name="psfFollowUpDate" class="showOnlyFutureDate form-control" id="followUpDT4thDay" readonly>
					</div>
				  </div>
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpTime">Follow Up Time</label>
					  <input type="text" name="psfFollowUpTime" class="single-input form-control" id="followTM4thDay" readonly>
					</div>
				  </div>
				   <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpTime">Remarks</label>
					  <textarea type="text" name="remarksList[1]" class="form-control" id="followUp4thComment"></textarea>
					</div>
				  </div>
				
			</div>
			<input type="hidden" id="ageOfVehicleIS" name="ageOfVehicleIS" value=""""),_display_(/*145.75*/{vehicleData.getAge_of_vehicle()}),format.raw/*145.108*/("""">
			<input type="hidden" id="commercialVeh" name="commercialVeh" value=""""),_display_(/*146.73*/{vehicleData.isCommercialVehicle()}),format.raw/*146.108*/("""">
		  
		    <div style="display:none;" class="row animated  bounceInRight" id="ManhinYesAgeing">
			<div class="col-md-12">
			<p>Thank You ! Is it right time to speak with you?</p>
			<p>I would like to enquire about your Mahindra vehicle <b>"""),_display_(/*151.63*/{vehicleData.getVehicleRegNo()}),format.raw/*151.94*/("""</b>, <b>"""),_display_(/*151.104*/{vehicleData.getModel()}),format.raw/*151.128*/("""</b>, attended by <b>"""),_display_(/*151.150*/{service.getSaName()}),format.raw/*151.171*/("""</b>, on,<b>"""),_display_(/*151.184*/{service.getJobCardDateStr()}),format.raw/*151.213*/("""</b>
			<date></p>
			 <div class="radio-inline">
			<label>
			  <input type="radio" name="qM4_RightTimeToEnquireOrderbillDate" value="Yes" id="righttimeAgeingYes">
			  Yes </label>
		  </div>
		   <div class="radio-inline">
			<label>
			  <input type="radio" name="qM4_RightTimeToEnquireOrderbillDate" value="No" id="righttimeAgeingNo">
			  No </label>
		  </div>
			  <div class="pull-right">
					<button type="button" class="btn btn-info" id="BackTo2ndMahiQAge">Back</button>						
					<button type="button" class="btn btn-info" id="NextToQuest4Age">Next</button>
			</div>
			</div>
			</div>
	<div class="col-md-12 animated  bounceInRight" style="display:none;" id="upsellPsf4thDayageing">
			<label>Capture Lead : Is there an Upsell Opportunity.</label>
		   <br>
		   
			<div class="radio-inline">
			  <label>
			<input type="radio" name="LeadYesH" value="Yes" id="LeadYesID3Hyndai" >
				Yes</label>
			</div>
			<div class="radio-inline">
			  <label>
				<input type="radio" name="LeadYesH" value="No" id="LeadNoID3Hyndai" checked>
				No</label>
			</div>
			<div class="row animated  bounceInRight" style="display:none;" id="LeadHyndai3rdDay">
			<div class="col-md-12">
			  <div class="col-md-6">
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[0].upsellId" value="1" id="InsuranceIDCheck" onClick="loadLeadBasedOnUserLocation('InsuranceIDCheck','insuranceLead1')">
					Service</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="InsuranceSelect">
				 
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[0].taggedTo" id="insuranceLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments1" name="upsellLead[0].upsellComments"></textarea>
					</div>
				 
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[1].upsellId" value="2" id="MaxicareIDCheck" onClick="loadLeadBasedOnUserLocation('MaxicareIDCheck','maxicareLead')">
					Warranty / EW</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="MaxicareSelect">
					
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[1].taggedTo" id="maxicareLead">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments2" name="upsellLead[1].upsellComments"></textarea>
					</div>
				 
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="VAS myOutCheckbox" name="upsellLead[2].upsellId" value="3" id="VASID" onClick="loadLeadBasedOnUserLocation('VASID','vASLead1')"/>
					VAS</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="VASTagToSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[2].taggedTo" id="vASLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments3" name="upsellLead[2].upsellComments"></textarea>
					</div>
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[3].upsellId" value="4" id="ShieldID" onClick="loadLeadBasedOnUserLocation('ShieldID','warrantyLead1')" />
					Re-Finance / New Car Finance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="ShieldSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[3].taggedTo" id="warrantyLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments4" name="upsellLead[3].upsellComments"></textarea>
					</div>
				 </div>
				</div>
				<div class="col-md-6">
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="Loan myOutCheckbox" name="upsellLead[4].upsellId" value="5" id="RoadSideAsstID" onClick="loadLeadBasedOnUserLocation('RoadSideAsstID','RoadSideAssiLead1')"/>
					Sell Old Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="RoadSideAssiSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[4].taggedTo" id="RoadSideAssiLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments5" name="upsellLead[4].upsellComments"></textarea>
					</div>
				  </div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="EXCHANGE myOutCheckbox" name="upsellLead[5].upsellId" value="6" id="EXCHANGEID" onClick="loadLeadBasedOnUserLocation('EXCHANGEID','buyNewCarLead1')"/>
					Buy New Car / Exchange</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="EXCHANGEIDSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[5].taggedTo" id="buyNewCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments6" name="upsellLead[5].upsellComments"></textarea>
					</div>
				 	</div>
				  <div class="checkbox">
				  <label>
					<input type="checkbox" class="UsedCar myOutCheckbox" name="upsellLead[6].upsellId" value="9" id="UsedCarID" onClick="loadLeadBasedOnUserLocation('UsedCarID','usedCarLead1')"/>
					Used Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="UsedCarSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[6].taggedTo" id="usedCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
			<textarea class="form-control" rows="1" id="comments7" name="upsellLead[6].upsellComments"></textarea>
					</div>
				 
				</div>
		   
			  </div>
			</div>
			 
			</div>
			  <div class="pull-right" >
			<button type="button" class="btn btn-info" id="BackToFeedBack4thDay">Back</button>
			  <button type="button" class="btn btn-info" id="nextTo4thDayRating">Next</button>
			</div>
			</div>
			<div style="display:none;" class="row animated  bounceInRight" id="fedBack4thDayDispo" >
		   <div class="col-md-12">
			 	<p>Can I take a moment to update your Information? </p>
			 				 
				  	<div class="radio-inline">
					<label>
					  <input type="radio" name="feedbackYes" value="Yes" id="" data-target="#addBtn" data-toggle="modal" data-backdrop="static" data-keyboard="false">
					  Yes </label>
				</div>
				<div class="radio-inline">
					<label>
					  <input type="radio" name="feedbackYes" value="No" id="" checked>
					 No </label>
				</div>
				<div class="pull-right">
<button type="button" class="btn btn-primary" id="BackToupsell4thDayPsd">Back</button>
<button type="submit" class="btn btn-success" id="ratingSubmit3rdDayM">Submit</button>
				</div>
				</div>
				</div>
	"""),_display_(/*347.3*/psfAgeDiv),format.raw/*347.12*/("""
			
          
          """),format.raw/*350.11*/("""</div>
        </div>
       
      </div>
    </div>
    <script src=""""),_display_(/*355.19*/routes/*355.25*/.Assets.at("javascripts/psf4thDayDisposition.js")),format.raw/*355.74*/("""" type="text/javascript"></script>"""))
      }
    }
  }

  def render(service:Service,vehicleData:Vehicle,userName:String,dealerName:String,psfAgeDiv:Html): play.twirl.api.HtmlFormat.Appendable = apply(service,vehicleData,userName,dealerName,psfAgeDiv)

  def f:((Service,Vehicle,String,String,Html) => play.twirl.api.HtmlFormat.Appendable) = (service,vehicleData,userName,dealerName,psfAgeDiv) => apply(service,vehicleData,userName,dealerName,psfAgeDiv)

  def ref: this.type = this

}


}

/**/
object psf4thDayDisposition extends psf4thDayDisposition_Scope0.psf4thDayDisposition
              /*
                  -- GENERATED --
                  DATE: Fri Dec 22 16:46:54 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/psf4thDayDisposition.scala.html
                  HASH: 10946a1a6a14e04267be3107db5ee3afcdc282b0
                  MATRIX: 803->1|984->87|1014->91|1181->231|1196->237|1261->281|1352->347|1397->384|1435->385|1469->393|1524->431|1536->435|1574->436|1615->451|1664->491|1703->492|1744->506|1812->556|1825->560|1864->561|1905->575|1976->616|2021->631|2050->633|4241->2797|4270->2805|4299->2806|4349->2829|4380->2839|4443->2874|4510->2919|5418->3799|5486->3844|7178->5508|7234->5541|7338->5617|7396->5652|7675->5903|7728->5934|7767->5944|7814->5968|7865->5990|7909->6011|7951->6024|8003->6053|16050->14073|16081->14082|16139->14111|16244->14188|16260->14194|16331->14243
                  LINES: 27->1|32->1|34->3|35->4|35->4|35->4|37->6|37->6|37->6|38->7|40->9|40->9|40->9|42->11|42->11|42->11|44->13|45->14|45->14|45->14|47->16|48->17|50->19|50->19|116->85|116->85|116->85|116->85|116->85|116->85|116->85|136->105|136->105|176->145|176->145|177->146|177->146|182->151|182->151|182->151|182->151|182->151|182->151|182->151|182->151|378->347|378->347|381->350|386->355|386->355|386->355
                  -- GENERATED --
              */
          