
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object userInfo_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class userInfo extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,List[models.WyzUser],Form[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealerName:String,userName:String,users:List[models.WyzUser],userform:Form[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import models._
import helper._
import scala._

Seq[Any](format.raw/*1.87*/("""
"""),_display_(/*5.2*/main("AutoSherpaCRM",userName,dealerName)/*5.43*/ {_display_(Seq[Any](format.raw/*5.45*/("""
"""),_display_(/*6.2*/helper/*6.8*/.form(action = routes.WyzUserController.userInformation)/*6.64*/ {_display_(Seq[Any](format.raw/*6.66*/("""

"""),format.raw/*8.1*/("""<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">List of Users Information</h1>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-info">
      <div class="panel-heading"> List of Users </div>
      <div class="panel-body">
        <div class="dataTable_wrapper">
          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th colspan="2">Actions</th>               
                <th>User Name</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Role</th>
                <th>Dealer Name</th>
                <th>Dealer Id</th>
                <th>Phone Number</th>               
                <th>Email Id</th>
              </tr>
            </thead>
            <tbody>
            
            """),_display_(/*35.14*/for(post <- users) yield /*35.32*/ {_display_(Seq[Any](format.raw/*35.34*/("""
            """),format.raw/*36.13*/("""<tr>
              <td><a href="/superAdmin/user/delete/"""),_display_(/*37.53*/post/*37.57*/.id),format.raw/*37.60*/(""""><i class="fa fa-trash-o"></i></a></td>
              <td><a href="/superAdmin/user/edit/"""),_display_(/*38.51*/post/*38.55*/.id),format.raw/*38.58*/(""""><i class="fa fa-pencil-square-o"></i></a></td>              
              <td>"""),_display_(/*39.20*/post/*39.24*/.userName),format.raw/*39.33*/("""</td>
              <td>"""),_display_(/*40.20*/post/*40.24*/.firstName),format.raw/*40.34*/("""</td>
              <td>"""),_display_(/*41.20*/post/*41.24*/.lastName),format.raw/*41.33*/("""</td>
              <td>"""),_display_(/*42.20*/post/*42.24*/.role),format.raw/*42.29*/("""</td>
              <td>"""),_display_(/*43.20*/post/*43.24*/.dealerName),format.raw/*43.35*/("""</td>
              <td>"""),_display_(/*44.20*/post/*44.24*/.dealerId),format.raw/*44.33*/("""</td>
              <td>"""),_display_(/*45.20*/post/*45.24*/.phoneNumber),format.raw/*45.36*/("""</td>              
              <td>"""),_display_(/*46.20*/post/*46.24*/.emailId),format.raw/*46.32*/("""</td>
            </tr>
            """)))}),format.raw/*48.14*/("""
              """),format.raw/*49.15*/("""</tbody>
            
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
        """)))}),format.raw/*57.10*/("""

""")))}))
      }
    }
  }

  def render(dealerName:String,userName:String,users:List[models.WyzUser],userform:Form[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(dealerName,userName,users,userform)

  def f:((String,String,List[models.WyzUser],Form[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (dealerName,userName,users,userform) => apply(dealerName,userName,users,userform)

  def ref: this.type = this

}


}

/**/
object userInfo extends userInfo_Scope0.userInfo
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:40 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/userInfo.scala.html
                  HASH: d9fe65fd0e8bd6065d2d81d716e8515ac08ded9c
                  MATRIX: 793->1|1019->86|1047->142|1096->183|1135->185|1163->188|1176->194|1240->250|1279->252|1309->256|2265->1185|2299->1203|2339->1205|2381->1219|2466->1277|2479->1281|2503->1284|2622->1376|2635->1380|2659->1383|2769->1466|2782->1470|2812->1479|2865->1505|2878->1509|2909->1519|2962->1545|2975->1549|3005->1558|3058->1584|3071->1588|3097->1593|3150->1619|3163->1623|3195->1634|3248->1660|3261->1664|3291->1673|3344->1699|3357->1703|3390->1715|3457->1755|3470->1759|3499->1767|3569->1806|3613->1822|3757->1935
                  LINES: 27->1|34->1|35->5|35->5|35->5|36->6|36->6|36->6|36->6|38->8|65->35|65->35|65->35|66->36|67->37|67->37|67->37|68->38|68->38|68->38|69->39|69->39|69->39|70->40|70->40|70->40|71->41|71->41|71->41|72->42|72->42|72->42|73->43|73->43|73->43|74->44|74->44|74->44|75->45|75->45|75->45|76->46|76->46|76->46|78->48|79->49|87->57
                  -- GENERATED --
              */
          