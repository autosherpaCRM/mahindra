
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object assignCallBasedOnSelectedRangeInsurance_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class assignCallBasedOnSelectedRangeInsurance extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template15[String,Long,Long,Long,String,String,Boolean,Boolean,List[String],List[controllers.webmodels.CallLogAjaxLoadInsurance],String,String,List[Campaign],List[Location],List[Campaign],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(cityName:String,workshopId:Long,campaignNameCat:Long,campaignTypeCat:Long,fromDateValue:String,toDateValue:String,isRangeSelected:Boolean,toAssigncall:Boolean,allCRE:List[String],filteredData:List[controllers.webmodels.CallLogAjaxLoadInsurance],dealerName:String,user:String,campaign_list:List[Campaign],locationList:List[Location],campaignList:List[Campaign]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.363*/("""

"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*4.52*/ {_display_(Seq[Any](format.raw/*4.54*/("""

"""),_display_(/*6.2*/helper/*6.8*/.form(action = routes.InsuranceController.postAssignCallInsurance(),'enctype -> "multipart/form-data")/*6.110*/{_display_(Seq[Any](format.raw/*6.111*/("""

"""),format.raw/*8.1*/("""<input type="hidden" name="fromDateToAss" value=""""),_display_(/*8.51*/fromDateValue),format.raw/*8.64*/("""">
<input type="hidden" name="toDateToAss" value=""""),_display_(/*9.49*/toDateValue),format.raw/*9.60*/("""">
<input type="hidden" name="campaignTypeToAss" value=""""),_display_(/*10.55*/campaignTypeCat),format.raw/*10.70*/("""">
<input type="hidden" name="campaignNameToAss" value=""""),_display_(/*11.55*/campaignNameCat),format.raw/*11.70*/("""">
<input type="hidden" name="cityNameToAss" value=""""),_display_(/*12.51*/cityName),format.raw/*12.59*/("""">
<input type="hidden" name="workshopToAss" value=""""),_display_(/*13.51*/workshopId),format.raw/*13.61*/("""">




<div class="panel panel-info">
        <div class="panel-heading"><strong>Call Allocations| To Assign Calls</strong> </div>
        <div class="panel-body">
            <div class="row">
                
                """),_display_(/*23.18*/if(toAssigncall)/*23.34*/{_display_(Seq[Any](format.raw/*23.35*/("""     
      """),format.raw/*24.7*/("""<div class="col-sm-3">   
      <div class="form-group">
       <label>Assignment Type :</label>    
      <select class="form-control" name="campaignType" id="campaignTypeData"> 
      <option value='0'>--Select--</option>         
          """),_display_(/*29.12*/for(list_camp <- campaign_list) yield /*29.43*/{_display_(Seq[Any](format.raw/*29.44*/("""
		"""),format.raw/*30.3*/("""<option value=""""),_display_(/*30.19*/list_camp/*30.28*/.id),format.raw/*30.31*/("""">"""),_display_(/*30.34*/list_camp/*30.43*/.campaignType),format.raw/*30.56*/("""</option>		
	 """)))}),format.raw/*31.4*/("""
      """),format.raw/*32.7*/("""</select>
      </div>
      </div>
      """)))}),format.raw/*35.8*/(""" 
 """),_display_(/*36.3*/if(toAssigncall)/*36.19*/{_display_(Seq[Any](format.raw/*36.20*/("""  

            """),format.raw/*38.13*/("""<div class="col-md-3" >
                                <div class="form-group">
                                    <label for="">Select Campaign Name</label>
                                    <select class="form-control" id="campaignName" name="campaignName" value=""""),_display_(/*41.112*/campaignNameCat),format.raw/*41.127*/("""">	
                                        <option value='0'>--Select--</option>
                                        """),_display_(/*43.42*/for(list_camp <- campaignList) yield /*43.72*/{_display_(Seq[Any](format.raw/*43.73*/("""
                                """),format.raw/*44.33*/("""<option value=""""),_display_(/*44.49*/list_camp/*44.58*/.id),format.raw/*44.61*/("""">"""),_display_(/*44.64*/list_camp/*44.73*/.campaignName),format.raw/*44.86*/("""</option>		
                                         """)))}),format.raw/*45.43*/("""
                                    
                                     
                                    """),format.raw/*48.37*/("""</select>

                                </div>
                            </div>
                            """)))}),format.raw/*52.30*/("""
                           				
                                
          
            """),_display_(/*56.14*/if(isRangeSelected)/*56.33*/{_display_(Seq[Any](format.raw/*56.34*/("""
            """),format.raw/*57.13*/("""<div class=col-md-3>
            <div class="form-group">
                    <label>Select CRE's</label>
    			<select  multiple="multiple" id="data[]" name="data[]" class="selectpicker" required="required">                        			    
                     	"""),_display_(/*61.24*/for(data1 <- allCRE) yield /*61.44*/{_display_(Seq[Any](format.raw/*61.45*/("""                      	                         
                    
                        """),format.raw/*63.25*/("""<option value=""""),_display_(/*63.41*/data1),format.raw/*63.46*/("""">"""),_display_(/*63.49*/data1),format.raw/*63.54*/("""</option>
                      				
                      	""")))}),format.raw/*65.25*/("""			 	
                      					
                	"""),format.raw/*67.18*/("""</select>
                				
            </div>
            </div>
            """)))}),format.raw/*71.14*/("""
            """),_display_(/*72.14*/if(isRangeSelected)/*72.33*/{_display_(Seq[Any](format.raw/*72.34*/(""" 
            """),format.raw/*73.13*/("""<div class="col-sm-3">
        <div class="form-group" >
         <label></label>  
         <button type="submit" class="btn btn-sm btn-primary" id="js-upload-submit1" name="selectedFile" style="margin-top:26px" value="assignCalls">Assign Calls</button>
     
      </div>
      </div>
           """)))}),format.raw/*80.13*/("""
       """),_display_(/*81.9*/if(toAssigncall)/*81.25*/{_display_(Seq[Any](format.raw/*81.26*/("""     
      """),format.raw/*82.7*/("""<div class="col-sm-3">   
      <div class="form-group">
       <label>From Date :</label>    
      <input type="text-primary" class="datepicker form-control" id="singleData" name="singleData" value=""""),_display_(/*85.108*/fromDateValue),format.raw/*85.121*/("""" readonly>
      </div>
      </div>
      """)))}),format.raw/*88.8*/("""
      """),_display_(/*89.8*/if(toAssigncall)/*89.24*/{_display_(Seq[Any](format.raw/*89.25*/("""
      """),format.raw/*90.7*/("""<div class="col-sm-3">   
      <div class="form-group">   
      <label>To Date :</label>    
      <input type="text-primary" class="datepicker form-control" id="tranferData" name="tranferData" value=""""),_display_(/*93.110*/toDateValue),format.raw/*93.121*/("""" readonly>       
     	</div>
      </div>
      """)))}),format.raw/*96.8*/("""
	    """),format.raw/*97.6*/("""</div>   
              
        <div class="row">
	  """),_display_(/*100.5*/if(toAssigncall)/*100.21*/{_display_(Seq[Any](format.raw/*100.22*/("""
          """),format.raw/*101.11*/("""<div class="col-md-3 locationSelect" >
                                           
            <label for="cityName">Select City</label>
            <select class="form-control" id="city" name="cityName" onchange="ajaxCallToLoadWorkShopByCity();">	
               <option value='select'>--Select City--</option>
                 """),_display_(/*106.19*/for(location_list<-locationList) yield /*106.51*/{_display_(Seq[Any](format.raw/*106.52*/("""
               """),format.raw/*107.16*/("""<option value=""""),_display_(/*107.32*/location_list/*107.45*/.getName()),format.raw/*107.55*/("""">"""),_display_(/*107.58*/location_list/*107.71*/.getName()),format.raw/*107.81*/("""</option>
                   """)))}),format.raw/*108.21*/("""
            """),format.raw/*109.13*/("""</select>

                                          
          </div>
          <div class="col-md-3 workshopSel">
            
                  <label for="workshopId">Select Location</label>
                  <select class="form-control" id="workshop" name="workshopId" >					  
                     <option value=""></option>
                  </select>
             		 
          </div>
      """)))}),format.raw/*121.8*/(""" 
	  
	   """),_display_(/*123.6*/if(toAssigncall)/*123.22*/{_display_(Seq[Any](format.raw/*123.23*/("""
     	"""),format.raw/*124.7*/("""<div class="col-sm-3">
        <div class="form-group" >
        <button type="submit" id="js-upload-submit" name="selectedFile" class="btn btn-sm btn-primary viewAssignCalls" style="margin-top:26px" value="viewAssignCalls">View Calls</button>
     
      </div>
      </div>
           """)))}),format.raw/*130.13*/("""
	   """),format.raw/*131.5*/("""</div>
	   </div>      
           
           
        </div>
          

<div class="row">
      <div class="col-lg-12">
        <div class="panel panel-success">
          <div class="panel-heading"> List Of  Calls To Assign</div>
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="dataTable_wrapper">
              <table class="table table-striped table-bordered table-hover" id="dataTables-example25">
                <thead>
                  <tr>
                  								<th>Campaign</th>
                                             	<th>Customer Name</th>
                                             	<th>Phone</th>
                                             	<th>RegNo.</th>  
                                             	<th>Chassis No</th>                                             
                                                <th>Policy Due Month</th>
                                                <th>Policy Due Date</th>
                                                <th>Next Renewal Type</th>
                                                <th>Last Insurance Company</th>
                                    
                                    
                  </tr>
                </thead>
                <tbody>
               
                  
                  		
                             """),_display_(/*165.31*/for(post<-filteredData) yield /*165.54*/{_display_(Seq[Any](format.raw/*165.55*/("""
                               """),format.raw/*166.32*/("""<tr >
                  				<td>"""),_display_(/*167.28*/post/*167.32*/.getCampaign()),format.raw/*167.46*/("""</td>
                   				<td>"""),_display_(/*168.29*/post/*168.33*/.getCustomerName()),format.raw/*168.51*/("""</td>
                              	<td>"""),_display_(/*169.37*/post/*169.41*/.getPhone()),format.raw/*169.52*/("""</td>                              	
                              	<td>"""),_display_(/*170.37*/post/*170.41*/.getRegNo()),format.raw/*170.52*/("""</td>
                              	<td>"""),_display_(/*171.37*/post/*171.41*/.getChassisNo()),format.raw/*171.56*/("""</td>
                                <td>"""),_display_(/*172.38*/post/*172.42*/.getPolicyDueMonth()),format.raw/*172.62*/("""</td>                                
                                <td>"""),_display_(/*173.38*/post/*173.42*/.getPolicyDueDate),format.raw/*173.59*/("""</td>   
                                <td>"""),_display_(/*174.38*/post/*174.42*/.getNextRenewalType()),format.raw/*174.63*/("""</td>
                                <td>"""),_display_(/*175.38*/post/*175.42*/.getLastInsuranceCompany()),format.raw/*175.68*/("""</td>                         	
                             
                                
                              	</tr>
                              	
                              	
                  """)))}),format.raw/*181.20*/("""                 
                   
                  
                """),format.raw/*184.17*/("""</tbody>
              </table>
            </div>
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>

""")))}),format.raw/*195.2*/("""
""")))}),format.raw/*196.2*/("""
"""))
      }
    }
  }

  def render(cityName:String,workshopId:Long,campaignNameCat:Long,campaignTypeCat:Long,fromDateValue:String,toDateValue:String,isRangeSelected:Boolean,toAssigncall:Boolean,allCRE:List[String],filteredData:List[controllers.webmodels.CallLogAjaxLoadInsurance],dealerName:String,user:String,campaign_list:List[Campaign],locationList:List[Location],campaignList:List[Campaign]): play.twirl.api.HtmlFormat.Appendable = apply(cityName,workshopId,campaignNameCat,campaignTypeCat,fromDateValue,toDateValue,isRangeSelected,toAssigncall,allCRE,filteredData,dealerName,user,campaign_list,locationList,campaignList)

  def f:((String,Long,Long,Long,String,String,Boolean,Boolean,List[String],List[controllers.webmodels.CallLogAjaxLoadInsurance],String,String,List[Campaign],List[Location],List[Campaign]) => play.twirl.api.HtmlFormat.Appendable) = (cityName,workshopId,campaignNameCat,campaignTypeCat,fromDateValue,toDateValue,isRangeSelected,toAssigncall,allCRE,filteredData,dealerName,user,campaign_list,locationList,campaignList) => apply(cityName,workshopId,campaignNameCat,campaignTypeCat,fromDateValue,toDateValue,isRangeSelected,toAssigncall,allCRE,filteredData,dealerName,user,campaign_list,locationList,campaignList)

  def ref: this.type = this

}


}

/**/
object assignCallBasedOnSelectedRangeInsurance extends assignCallBasedOnSelectedRangeInsurance_Scope0.assignCallBasedOnSelectedRangeInsurance
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:12 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/assignCallBasedOnSelectedRangeInsurance.scala.html
                  HASH: 746c95d9b173f87b5ed4af549c20b788605d69f1
                  MATRIX: 984->5|1441->366|1471->371|1529->421|1568->423|1598->428|1611->434|1722->536|1761->537|1791->541|1867->591|1900->604|1978->656|2009->667|2094->725|2130->740|2215->798|2251->813|2332->867|2361->875|2442->929|2473->939|2738->1177|2763->1193|2802->1194|2842->1207|3118->1456|3165->1487|3204->1488|3235->1492|3278->1508|3296->1517|3320->1520|3350->1523|3368->1532|3402->1545|3448->1561|3483->1569|3559->1615|3590->1620|3615->1636|3654->1637|3700->1655|4002->1929|4039->1944|4191->2069|4237->2099|4276->2100|4338->2134|4381->2150|4399->2159|4423->2162|4453->2165|4471->2174|4505->2187|4591->2242|4734->2357|4883->2475|5004->2569|5032->2588|5071->2589|5113->2603|5408->2871|5444->2891|5483->2892|5607->2988|5650->3004|5676->3009|5706->3012|5732->3017|5826->3080|5907->3133|6024->3219|6066->3234|6094->3253|6133->3254|6176->3269|6513->3575|6549->3585|6574->3601|6613->3602|6653->3615|6886->3820|6921->3833|6999->3881|7034->3890|7059->3906|7098->3907|7133->3915|7368->4122|7401->4133|7486->4188|7520->4195|7605->4253|7631->4269|7671->4270|7712->4282|8075->4617|8124->4649|8164->4650|8210->4667|8254->4683|8277->4696|8309->4706|8340->4709|8363->4722|8395->4732|8458->4763|8501->4777|8944->5189|8984->5202|9010->5218|9050->5219|9086->5227|9412->5521|9446->5527|10890->6943|10930->6966|10970->6967|11032->7000|11094->7034|11108->7038|11144->7052|11207->7087|11221->7091|11261->7109|11332->7152|11346->7156|11379->7167|11481->7241|11495->7245|11528->7256|11599->7299|11613->7303|11650->7318|11722->7362|11736->7366|11778->7386|11882->7462|11896->7466|11935->7483|12010->7530|12024->7534|12067->7555|12139->7599|12153->7603|12201->7629|12454->7850|12559->7926|12797->8133|12831->8136
                  LINES: 27->2|32->2|34->4|34->4|34->4|36->6|36->6|36->6|36->6|38->8|38->8|38->8|39->9|39->9|40->10|40->10|41->11|41->11|42->12|42->12|43->13|43->13|53->23|53->23|53->23|54->24|59->29|59->29|59->29|60->30|60->30|60->30|60->30|60->30|60->30|60->30|61->31|62->32|65->35|66->36|66->36|66->36|68->38|71->41|71->41|73->43|73->43|73->43|74->44|74->44|74->44|74->44|74->44|74->44|74->44|75->45|78->48|82->52|86->56|86->56|86->56|87->57|91->61|91->61|91->61|93->63|93->63|93->63|93->63|93->63|95->65|97->67|101->71|102->72|102->72|102->72|103->73|110->80|111->81|111->81|111->81|112->82|115->85|115->85|118->88|119->89|119->89|119->89|120->90|123->93|123->93|126->96|127->97|130->100|130->100|130->100|131->101|136->106|136->106|136->106|137->107|137->107|137->107|137->107|137->107|137->107|137->107|138->108|139->109|151->121|153->123|153->123|153->123|154->124|160->130|161->131|195->165|195->165|195->165|196->166|197->167|197->167|197->167|198->168|198->168|198->168|199->169|199->169|199->169|200->170|200->170|200->170|201->171|201->171|201->171|202->172|202->172|202->172|203->173|203->173|203->173|204->174|204->174|204->174|205->175|205->175|205->175|211->181|214->184|225->195|226->196
                  -- GENERATED --
              */
          