
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object downloadUploadExcelFormat_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class downloadUploadExcelFormat extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(userName:String,dealerName:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import models._

Seq[Any](format.raw/*1.37*/("""
"""),_display_(/*3.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*3.56*/ {_display_(Seq[Any](format.raw/*3.58*/("""

	"""),format.raw/*5.2*/("""<div class="panel panel-info">
        <div class="panel-heading"><strong>Download Upload Format</strong></div>
        <div class="panel-body">
             <div class="row">
               <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        
                        <div class="ibox-content">
                            <div class="form-horizontal">
                              
                                <div class="form-group">
                                    	<div class="col-lg-2" style="width: 191px;">
                                    		<select name="upload_format_name" id="upload_format_name" class="form-control" onChange="ajax_get_downloadFormat();">
	                        					<option value= "0" selected>--Select--</option>	                                                                
								                        		<option value= "sale_register">Sale Register</option>
							                                    <option value= "job_card">Job Card</option>
							                                    <option value= "bill">Bills</option>
							                                    <option value= "SMR">SMR</option>
							                                    <option value= "campaign">Campaign</option>
	                        					
	                        					
                            				</select>
                            			</div>	                       			
                            	</div>
                            </div>
                          </div>
                          
                          
                            
                              
                                <div class="form-group">
                                    	
                          
                             <div class="table-responsive">
                                <table class="table table-bordered header-fixed table-responsive" id="downloadTable" >
                                    <thead>
						                <tr>
                							
										</tr>
                					</thead>
                					<tbody>
                					</tbody>
                				</table>
                			</div>
                				                       			
                            	</div>
                           
                         
                       </div>
                  </div>
               </div>
        </div>
             </div>                             
 
""")))}),format.raw/*59.2*/("""
"""),format.raw/*60.1*/("""<script>

function ajax_get_downloadFormat()"""),format.raw/*62.35*/("""{"""),format.raw/*62.36*/("""

	"""),format.raw/*64.2*/("""var table_name="downloadTable";	
	var upload_format = document.getElementById('upload_format_name').value;

	//alert(table_name);
	//alert(upload_format);
	
	var url_path = "/getExcelColumns/"+ upload_format;

	$.ajax("""),format.raw/*72.9*/("""{"""),format.raw/*72.10*/("""
		"""),format.raw/*73.3*/("""url : url_path
	"""),format.raw/*74.2*/("""}"""),format.raw/*74.3*/(""").done(function(json)"""),format.raw/*74.24*/("""{"""),format.raw/*74.25*/("""
		"""),format.raw/*75.3*/("""if (json != null && json != '') """),format.raw/*75.35*/("""{"""),format.raw/*75.36*/("""

	        """),format.raw/*77.10*/("""if ($.fn.DataTable.isDataTable('#' + table_name)) """),format.raw/*77.60*/("""{"""),format.raw/*77.61*/("""
	            """),format.raw/*78.14*/("""var table = $('#' + table_name).DataTable();
	            table.clear();
	            table.destroy();
	        """),format.raw/*81.10*/("""}"""),format.raw/*81.11*/("""
	        """),format.raw/*82.10*/("""$('#' + table_name + ' thead tr').empty();
	        th = $('<tr />');
	        for (var i = 0; i < json.length; i++) """),format.raw/*84.48*/("""{"""),format.raw/*84.49*/("""
	            """),format.raw/*85.14*/("""th.append('<th style="background-color :#B39DDB;text-align:center;display:none">' + json[i] + '</th>');
	        """),format.raw/*86.10*/("""}"""),format.raw/*86.11*/("""
	        """),format.raw/*87.10*/("""$('#' + table_name + ' thead').append(th);
	        $('#' + table_name).dataTable("""),format.raw/*88.40*/("""{"""),format.raw/*88.41*/("""
	            """),format.raw/*89.14*/(""""paging": false,
	            "ordering": false,
	            "info": false,
	            "bFilter": false,
	            dom: '<"html5buttons"B>lTfgitp',
	            buttons: [
	                """),format.raw/*95.18*/("""{"""),format.raw/*95.19*/("""extend: 'excel', title: 'ExampleFile', text: 'Download Excel Format'"""),format.raw/*95.87*/("""}"""),format.raw/*95.88*/(""",
	            ]
	        """),format.raw/*97.10*/("""}"""),format.raw/*97.11*/(""");
	        $('#' + table_name + ' tbody tr').empty();
		"""),format.raw/*99.3*/("""}"""),format.raw/*99.4*/("""
	"""),format.raw/*100.2*/("""}"""),format.raw/*100.3*/(""");
	    
		
"""),format.raw/*103.1*/("""}"""),format.raw/*103.2*/("""
"""),format.raw/*104.1*/("""</script>"""))
      }
    }
  }

  def render(userName:String,dealerName:String): play.twirl.api.HtmlFormat.Appendable = apply(userName,dealerName)

  def f:((String,String) => play.twirl.api.HtmlFormat.Appendable) = (userName,dealerName) => apply(userName,dealerName)

  def ref: this.type = this

}


}

/**/
object downloadUploadExcelFormat extends downloadUploadExcelFormat_Scope0.downloadUploadExcelFormat
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:25 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/downloadUploadExcelFormat.scala.html
                  HASH: 9fe9d0ca8a6cd56e3eb93efc7088f492d6e4f254
                  MATRIX: 792->1|937->36|965->57|1027->111|1066->113|1097->118|3697->2688|3726->2690|3800->2736|3829->2737|3861->2742|4114->2968|4143->2969|4174->2973|4218->2990|4246->2991|4295->3012|4324->3013|4355->3017|4415->3049|4444->3050|4485->3063|4563->3113|4592->3114|4635->3129|4778->3244|4807->3245|4846->3256|4993->3375|5022->3376|5065->3391|5207->3505|5236->3506|5275->3517|5386->3600|5415->3601|5458->3616|5687->3817|5716->3818|5812->3886|5841->3887|5897->3915|5926->3916|6012->3975|6040->3976|6071->3979|6100->3980|6143->3995|6172->3996|6202->3998
                  LINES: 27->1|32->1|33->3|33->3|33->3|35->5|89->59|90->60|92->62|92->62|94->64|102->72|102->72|103->73|104->74|104->74|104->74|104->74|105->75|105->75|105->75|107->77|107->77|107->77|108->78|111->81|111->81|112->82|114->84|114->84|115->85|116->86|116->86|117->87|118->88|118->88|119->89|125->95|125->95|125->95|125->95|127->97|127->97|129->99|129->99|130->100|130->100|133->103|133->103|134->104
                  -- GENERATED --
              */
          