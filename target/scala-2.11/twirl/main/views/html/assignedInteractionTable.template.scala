
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object assignedInteractionTable_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class assignedInteractionTable extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[List[AssignedInteraction],String,String,List[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(assignData:List[AssignedInteraction],dealerName:String,user:String,allCRE:List[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.91*/("""

"""),_display_(/*3.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*3.52*/ {_display_(Seq[Any](format.raw/*3.54*/("""

  """),format.raw/*5.3*/("""<div class="row">
      <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading"> Assigned List Of Calls</div>
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="dataTable_wrapper">
              <table class="table table-striped table-bordered table-hover" id="dataTables-example36">
                <thead>
				  <tr>
                                    
                                    <th>Agent Name</th>
                                    <th>Customer Name</th>
                                    <th>Change Assignment</th>
                                    <th>Mobile Number</th>                                    
                                    <th>Vehicle RegNo.</th>
                                    <th>Model</th> 
                                    <th>Variant</th> 
                                                       
                                   
                                    
                  </tr>
                </thead>
                <tbody>
                             """),_display_(/*29.31*/for(assign_data<-assignData) yield /*29.59*/{_display_(Seq[Any](format.raw/*29.60*/("""
                               """),format.raw/*30.32*/("""<tr >
				
                  		<td id="changename">"""),_display_(/*32.42*/assign_data/*32.53*/.getWyzUser().userName),format.raw/*32.75*/("""</td>		
                   		<td>"""),_display_(/*33.27*/assign_data/*33.38*/.getCustomer().customerName),format.raw/*33.65*/("""</td>
                                <td> 
				<select id="assignedNewAgent"  required="required" onchange="funcReAssignAgent('"""),_display_(/*35.86*/assign_data/*35.97*/.id),format.raw/*35.100*/("""','"""),_display_(/*35.104*/assign_data/*35.115*/.getWyzUser().id),format.raw/*35.131*/("""');">
				<option  value="0"> Select</option>
				"""),_display_(/*37.6*/for(data1 <- allCRE) yield /*37.26*/{_display_(Seq[Any](format.raw/*37.27*/("""                      	                         
				"""),format.raw/*38.5*/("""<option value=""""),_display_(/*38.21*/data1/*38.26*/.id),format.raw/*38.29*/("""">"""),_display_(/*38.32*/data1/*38.37*/.userName),format.raw/*38.46*/("""</option>
				""")))}),format.raw/*39.6*/("""			 	
                	          """),format.raw/*40.28*/("""</select>
                                     </td>
                              	<td id="get_preffered_contact_num" value=""""),_display_(/*42.75*/assign_data/*42.86*/.getCustomer().getId()),format.raw/*42.108*/("""">
                              	"""),_display_(/*43.33*/{if(assign_data.getCustomer().getPreferredPhone()!=null){
                              	
                              	assign_data.getCustomer().getPreferredPhone().getPhoneNumber()
                              	}
                              	}),format.raw/*47.33*/("""
                              	
                              	"""),format.raw/*49.32*/("""</td>
                              	<td>"""),_display_(/*50.37*/assign_data/*50.48*/.getVehicle().vehicleRegNo),format.raw/*50.74*/("""</td>
                              	<td>"""),_display_(/*51.37*/assign_data/*51.48*/.getVehicle().model),format.raw/*51.67*/("""</td> 
                              	<td>"""),_display_(/*52.37*/assign_data/*52.48*/.getVehicle().variant),format.raw/*52.69*/("""</td>
                              	</tr>
                  """)))}),format.raw/*54.20*/("""
                  
				  
                """),format.raw/*57.17*/("""</tbody>
              </table>
            </div>
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    
  
""")))}),format.raw/*69.2*/("""
"""),format.raw/*70.1*/("""<script type="text/javascript">
window.onload = ajaxReqForCustomerPhone;
</script>

<script>

function funcReAssignAgent(id,userid)
"""),format.raw/*77.1*/("""{"""),format.raw/*77.2*/("""

 """),format.raw/*79.2*/("""var rowId = id;
 var wyzUserId = userid;
  var e = document.getElementById("assignedNewAgent");
  var post_id = e.options[e.selectedIndex].value;
  
  //alert("rowId is:"+rowId+"userId is:"+wyzUserId+"postId is:"+post_id)
 

 
  var urlDisposition = "/CRE/reAssignAgent/"+ rowId+"/"+wyzUserId+"/"+post_id+"";
  
   $.ajax("""),format.raw/*90.11*/("""{"""),format.raw/*90.12*/("""
        """),format.raw/*91.9*/("""url: urlDisposition

    """),format.raw/*93.5*/("""}"""),format.raw/*93.6*/(""").done(function (data) """),format.raw/*93.29*/("""{"""),format.raw/*93.30*/("""
        """),format.raw/*94.9*/("""$("#changename").html(data);
      //  alert("data is :"+data);
	
	   if(data!=null)"""),format.raw/*97.19*/("""{"""),format.raw/*97.20*/("""					
                """),format.raw/*98.17*/("""Lobibox.alert('info', """),format.raw/*98.39*/("""{"""),format.raw/*98.40*/("""
                """),format.raw/*99.17*/("""msg: "Assigned interaction changed"
            """),format.raw/*100.13*/("""}"""),format.raw/*100.14*/(""");

          """),format.raw/*102.11*/("""}"""),format.raw/*102.12*/("""
	
	"""),format.raw/*104.2*/("""//window.location.reload(); 
  
"""),format.raw/*106.1*/("""}"""),format.raw/*106.2*/(""");

"""),format.raw/*108.1*/("""}"""),format.raw/*108.2*/("""
"""),format.raw/*109.1*/("""</script>"""))
      }
    }
  }

  def render(assignData:List[AssignedInteraction],dealerName:String,user:String,allCRE:List[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(assignData,dealerName,user,allCRE)

  def f:((List[AssignedInteraction],String,String,List[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (assignData,dealerName,user,allCRE) => apply(assignData,dealerName,user,allCRE)

  def ref: this.type = this

}


}

/**/
object assignedInteractionTable extends assignedInteractionTable_Scope0.assignedInteractionTable
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:13 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/assignedInteractionTable.scala.html
                  HASH: f9517a003102bffb68b03d4cc84b62722a1225ae
                  MATRIX: 830->1|1014->90|1044->95|1102->145|1141->147|1173->153|2341->1294|2385->1322|2424->1323|2485->1356|2566->1410|2586->1421|2629->1443|2691->1478|2711->1489|2759->1516|2917->1647|2937->1658|2962->1661|2994->1665|3015->1676|3053->1692|3132->1745|3168->1765|3207->1766|3288->1820|3331->1836|3345->1841|3369->1844|3399->1847|3413->1852|3443->1861|3489->1877|3551->1911|3707->2040|3727->2051|3771->2073|3834->2109|4108->2362|4202->2428|4272->2471|4292->2482|4339->2508|4409->2551|4429->2562|4469->2581|4540->2625|4560->2636|4602->2657|4697->2721|4771->2767|5016->2982|5045->2984|5211->3123|5239->3124|5271->3129|5632->3462|5661->3463|5698->3473|5752->3500|5780->3501|5831->3524|5860->3525|5897->3535|6012->3622|6041->3623|6092->3646|6142->3668|6171->3669|6217->3687|6295->3736|6325->3737|6370->3753|6400->3754|6434->3760|6496->3794|6525->3795|6559->3801|6588->3802|6618->3804
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|60->29|60->29|60->29|61->30|63->32|63->32|63->32|64->33|64->33|64->33|66->35|66->35|66->35|66->35|66->35|66->35|68->37|68->37|68->37|69->38|69->38|69->38|69->38|69->38|69->38|69->38|70->39|71->40|73->42|73->42|73->42|74->43|78->47|80->49|81->50|81->50|81->50|82->51|82->51|82->51|83->52|83->52|83->52|85->54|88->57|100->69|101->70|108->77|108->77|110->79|121->90|121->90|122->91|124->93|124->93|124->93|124->93|125->94|128->97|128->97|129->98|129->98|129->98|130->99|131->100|131->100|133->102|133->102|135->104|137->106|137->106|139->108|139->108|140->109
                  -- GENERATED --
              */
          