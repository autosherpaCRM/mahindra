
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object editDealerInfo_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class editDealerInfo extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,String,Long,models.Dealer,Form[Dealer],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealerName:String,userName:String,did: Long,editDealerdata:models.Dealer,form:Form[Dealer]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import models._
import scala._

Seq[Any](format.raw/*1.94*/("""
"""),_display_(/*4.2*/main("AutoSherpaCRM",userName,dealerName)/*4.43*/ {_display_(Seq[Any](format.raw/*4.45*/("""
"""),_display_(/*5.2*/helper/*5.8*/.form(routes.DealerController.postEditDealer(did), args= 'class -> "form-horizontal",'enctype -> "multipart/form-data", 'id -> "updateForm")/*5.148*/  {_display_(Seq[Any](format.raw/*5.151*/("""

"""),format.raw/*7.1*/("""<div class="fluid-container">
 
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Dealer Update </h3>
        </div>
        <div class="panel-body">
          <form class="form-inline">
                  
          <div class="row">
          <div class="col-lg-12">
            <div class="col-lg-6">
                <div class="form-group">
                  <label  for="dealerId">Dealer Id:</label>
                  <input type="text" class="form-control" name="dealerId" value='"""),_display_(/*21.83*/{editDealerdata.dealerId}),format.raw/*21.108*/("""' id="dealerId" autocomplete="off"  />
                </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                  <label for="dealerName">Dealer Name :</label>
                  <input type="text" class="form-control" name="dealerName" value='"""),_display_(/*27.85*/{editDealerdata.dealerName}),format.raw/*27.112*/("""' id="dealerName" autocomplete="off"  />
                </div>
            </div>           
            
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="col-lg-6">
             <div class="form-group">
                  <label for="dealerAddress">Dealer Address :</label>
                  <input type="text" class="form-control" name="dealerAddress" value='"""),_display_(/*38.88*/{editDealerdata.dealerAddress}),format.raw/*38.118*/("""' id="dealerAddress" autocomplete="off"  />
                </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                  <label for="dealerCode">Dealer Code :</label>
                  <input type="text" class="form-control" name="dealerCode" value='"""),_display_(/*44.85*/{editDealerdata.dealerCode}),format.raw/*44.112*/("""' id="dealerCode" autocomplete="off"  />
                </div>
            </div>
            <div class='text-right'>
                  <input type="submit" onclick="myfunction()" class="btn btn-primary" value="UPDATE" />
                </div>           
            
          </div>
        </div>
          </form>
        </div>
      </div>
   
</div>      
  
        """)))}),format.raw/*59.10*/("""
        

""")))}))
      }
    }
  }

  def render(dealerName:String,userName:String,did:Long,editDealerdata:models.Dealer,form:Form[Dealer]): play.twirl.api.HtmlFormat.Appendable = apply(dealerName,userName,did,editDealerdata,form)

  def f:((String,String,Long,models.Dealer,Form[Dealer]) => play.twirl.api.HtmlFormat.Appendable) = (dealerName,userName,did,editDealerdata,form) => apply(dealerName,userName,did,editDealerdata,form)

  def ref: this.type = this

}


}

/**/
object editDealerInfo extends editDealerInfo_Scope0.editDealerInfo
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:26 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/editDealerInfo.scala.html
                  HASH: 54aadac896ad357c8182cce0aa5685c3d12df540
                  MATRIX: 802->1|1019->93|1047->131|1096->172|1135->174|1163->177|1176->183|1325->323|1366->326|1396->330|1983->890|2030->915|2366->1224|2415->1251|2882->1691|2934->1721|3275->2035|3324->2062|3748->2455
                  LINES: 27->1|33->1|34->4|34->4|34->4|35->5|35->5|35->5|35->5|37->7|51->21|51->21|57->27|57->27|68->38|68->38|74->44|74->44|89->59
                  -- GENERATED --
              */
          