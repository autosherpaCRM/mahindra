
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object callDispositionPageCREManager_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class callDispositionPageCREManager extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[String],List[CallDispositionData],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long,String],campaignList :List[Campaign],serviceTypeList :List[ServiceTypes],bookedServiceTypeList:List[String],dispoList:List[CallDispositionData]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.221*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*2.52*/ {_display_(Seq[Any](format.raw/*2.54*/("""	



"""),format.raw/*6.1*/("""<style>
    td, th """),format.raw/*7.12*/("""{"""),format.raw/*7.13*/("""
                
        """),format.raw/*9.9*/("""border: 1px solid black;
        overflow: hidden; 
        text-align:center!important;
    """),format.raw/*12.5*/("""}"""),format.raw/*12.6*/("""

"""),format.raw/*14.1*/("""</style>
 
  <div>

        <div class="panel panel-primary">
            <div class="panel-heading">   
                Scheduled Call Log Information </div>
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab" id="Tab1" onclick="assignedInteractionDataMR();">Scheduled</a> </li>
                    <li><a href="#profile" data-toggle="tab" id="Tab2" onclick="ajaxCallForFollowUpRequiredServerMR();">Follow Up Required</a> </li>
                    <li><a href="#messages" data-toggle="tab" id="Tab3" onclick="ajaxCallForserviceBookedServerDataMR();">Service Booked</a> </li>
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxCallForServiceNotRequiredServerMR();" >Service Not Required</a> </li>
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxCallForNonContactsServerMR();">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxCallFordroppedCallsServerDataMR();">Dropped</a> </li>
					<li><a href="#missedCallBuc" data-toggle="tab" id="Tab7" onclick="ajaxCallMissedCallsServerDataMR();">Missed Calls</a> </li>
                    <li><a href="#incomingCallBuc" data-toggle="tab" id="Tab8" onclick="ajaxCallForincomingCallsServerDataMR();">Incoming Calls</a> </li>  
                    <li><a href="#outgoingCallBuc" data-toggle="tab" id="Tab9" onclick="ajaxCallForOutgoingCallBucCallsServerDataMR();">Outgoing Calls</a> </li>
                    
                </ul>
                <div class="tab-content">
                <div class="panel panel-default" style="border-top: #fff;">
				<div class="panel-body">
                <div class="row">
		<div class="col-md-2" id="cityDiv">
		<div class="form-group">
                <label>City </label>
               
               <select class="form-control" id="city" name="cityName" onchange="ajaxCallToLoadWorkShopByCity();"> 
                <option value='--Select--'>--Select--</option>
                
				"""),_display_(/*45.6*/for(city <- listCity) yield /*45.27*/{_display_(Seq[Any](format.raw/*45.28*/("""                  	                         
                    """),format.raw/*46.21*/("""<option value=""""),_display_(/*46.37*/city/*46.41*/.getName()),format.raw/*46.51*/("""">"""),_display_(/*46.54*/city/*46.58*/.getName()),format.raw/*46.68*/("""</option>
                    """)))}),format.raw/*47.22*/("""
                """),format.raw/*48.17*/("""</select>
</div>
            </div> 
			<div class="col-md-2" id="workshopDiv">
			<div class="form-group">
                <label>WorkShop Location </label>               
                <select class="form-control" id="workshop" name="workshopId" onchange="ajaxCallToLoadCRESByWorkshop();">                 			    
                

                </select>
			</div>
            </div> 
		
            <div class="col-md-2" id="cresDiv">
			 <label>Select CRE's </label>
			<div class="form-group">
              <select class="selectpicker filter form-control" data-column-index="7" id="ddlCreIds" name="ddlCreIds" multiple>
                
                    
                </select> 
</div>


            </div>
            
                <div class="col-md-2 tab-pane" id="campaignDiv">
                                
                                <label>Select Campaign</label>
						<select class="filter form-control" id="campaignName" data-column-index="0" name="campaignName">
							<option value="0" >--Select--</option>
							"""),_display_(/*78.9*/for(campaign_List<-campaignList) yield /*78.41*/{_display_(Seq[Any](format.raw/*78.42*/("""
		                                """),format.raw/*79.35*/("""<option value=""""),_display_(/*79.51*/campaign_List/*79.64*/.getCampaignName()),format.raw/*79.82*/("""">"""),_display_(/*79.85*/campaign_List/*79.98*/.getCampaignName()),format.raw/*79.116*/("""</option>
		                             """)))}),format.raw/*80.33*/("""
		                                
						"""),format.raw/*82.7*/("""</select>
					</div>
					
					<div class="col-md-2" id="fromDateDiv">
					<label>From Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" id="fromduedaterange" name="fromduedaterange" readonly>
				  </div>
				  <div class="col-md-2" id="toDateDiv">
					<label>To Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="2" id="toduedaterange" name="toduedaterange" readonly>
				  </div>
				  </div>
				  <div class="row">
				   
						<div class="col-md-2" id="serviceTypeDiv">
					
			
						<label>Select service type</label>
						<select class="filter form-control" id="ServiceTypeName" data-column-index="3" name="ServiceTypeName">
							<option value="0" >--Select--</option>
							"""),_display_(/*102.9*/for(serviceType_List<-serviceTypeList) yield /*102.47*/{_display_(Seq[Any](format.raw/*102.48*/("""
			                                """),format.raw/*103.36*/("""<option value=""""),_display_(/*103.52*/serviceType_List/*103.68*/.getServiceTypeName()),format.raw/*103.89*/("""">"""),_display_(/*103.92*/serviceType_List/*103.108*/.getServiceTypeName()),format.raw/*103.129*/("""</option>
			                             """)))}),format.raw/*104.34*/("""
			                                
						"""),format.raw/*106.7*/("""</select>
					</div>
					 
					<div class="col-md-2" id="serviceBookTypeDiv" style="display:none">						
					<label>Select Booked service type</label>
						<select class="filter form-control" id="serviceBookedType" data-column-index="4" name="serviceBookedType">
							<option value="0" >--Select--</option>
							"""),_display_(/*113.9*/for(bookedServiceType_List<-bookedServiceTypeList) yield /*113.59*/{_display_(Seq[Any](format.raw/*113.60*/("""
 """),format.raw/*114.2*/("""<option value=""""),_display_(/*114.18*/bookedServiceType_List),format.raw/*114.40*/("""">"""),_display_(/*114.43*/bookedServiceType_List),format.raw/*114.65*/("""</option>					                   
           """)))}),format.raw/*115.13*/("""
					                                
						"""),format.raw/*117.7*/("""</select>
					</div>
					<div class="col-md-2" id="lastDispoTypeDiv" style="display:none">						
					<label>Last Disposition</label>
						<select class="filter form-control" id="lastDispo" data-column-index="5" >
							<option value="0" >--Select--</option>
								"""),_display_(/*123.10*/for(dispo <- dispoList) yield /*123.33*/{_display_(Seq[Any](format.raw/*123.34*/("""
			"""),format.raw/*124.4*/("""<option value=""""),_display_(/*124.20*/dispo/*124.25*/.getDisposition()),format.raw/*124.42*/("""">"""),_display_(/*124.45*/dispo/*124.50*/.getDisposition()),format.raw/*124.67*/("""</option>
			
		""")))}),format.raw/*126.4*/("""
							
					                                
						"""),format.raw/*129.7*/("""</select>
					</div>
					<div class="col-md-2" id="droppedcountDiv" style="display:none">						
					<label>Dropped Count</label>
						<select class="filter form-control" id="droppedCount" data-column-index="6">
							<option value="0" >--Select--</option>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							
					                                
						</select>
					</div>
				</div>
				</div>
				</div>
                
                    <div class="panel panel-default tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="assignedInteractionTableMR">
                                        <thead>
                                            <tr> 

                                             <th>Customer Name</th>
                                                <th>RegNo.</th>
                                                <th>Model</th> 
                                                <th>Category</th>
                                                <th>Loyalty</th>
                                                <th>DueDate</th>
                                                <th>Type</th>
                                                <th>Forecast </th>
                                                <th>PSFStatus</th>
                                                <th>DND</th>
                                                <th>Complaint</th>
                                                <th>CRE Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>								
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade" id="profile">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="followUpRequiredServerDataMR" >
                                        <thead>
                                         <tr> 
                                          		<th>Campaign Name</th>
                                          		<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>                                                                   
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>CRE Name</th>
                                                <th>FollowUp Date</th>                                    
                                                <th>FollowUp Time</th>
                                                <th>Last Disposition</th>
												<th>Is Call Initiated</th>         
                                               <!--  <th>Download Media</th> -->
                                            </tr>   
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade" id="messages">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="serviceBookedServerDataMR">
                                        <thead>
                                            <tr> 
                                            	<th>Campaign Name</th>
                                            	<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>CRE Name</th>
                                                <th>Service ScheduledDate</th>  
                                                <th>Service ScheduledTime</th>  
                                                <th>Booking Status</th>
												<th>Is Call Initiated</th>
                                                <!--  <th>Download Media</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade" id="settings">
                         <div class="panel-body inf-content">
                        <div class="dataTable_wrapper">                            
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="serviceNotRequiredServerDataMR">
                                    <thead>
                                        <tr> 
                                        	<th>Campaign Name</th>
                                        	<th>Call Date</th>             	
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Service Due</th>
                                            <th>CRE Name</th>
                                            <th>Last Disposition</th>
                                            <th>Reason</th>
                                           <!--  <th>Download Media</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div><!--End tab panel-->
                    <div class="panel panel-default tab-pane fade " id="nonContacts">
                        <div class="panel-body inf-content"> 
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="nonContactsServerDataMR">
                                    <thead>
                                        <tr> 
                                        <th>Campaign Name</th>
                                        <th>Call Date</th>                         
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Service Due</th>
                                            <th>CRE Name</th>
                                            <th>Last Disposition</th>
                                            <th>Is Call Initiated</th>         											
                                         <!--  <th>Download Media</th> -->   
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="droppedCallsServerDataMR">
                                        <thead>
                                            <tr>
                                            	<th>Campaign Name</th> 
                                            	<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>CRE Name</th>
												<th>Is Call Initiated</th>         
                                                <th>Last Disposition</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
					 <div class="panel panel-default tab-pane fade" id="missedCallBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="missedCallsServerDataMR" width="100%">
                                        <thead>
                                            <tr>
       											<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>                                               
                                                <th>Mobile Number</th>
                                                <th>CRE Name</th>
                                                <!--  <th>Download Media</th> -->
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default tab-pane fade" id="incomingCallBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="incomingCallsServerDataMR" width="100%">
                                        <thead>
                                            <tr>
                                            	<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>                                                
                                                <th>Mobile Number</th>
                                                <th>CRE Name</th>
                                               <!--  <th>Download Media</th> -->
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default tab-pane fade" id="outgoingCallBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="outgoingCallsServerDataMR" width="100%">
                                        <thead>
                                            <tr>
                                            	<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>                                                
                                                <th>Mobile Number</th>
                                                <th>CRE Name</th>
                                               <!--  <th>Download Media</th> -->
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div><!--End tab content-->
            </div><!--End panel body-->
        </div>
   
</div>
""")))}),format.raw/*386.2*/(""" 

"""),format.raw/*388.1*/("""<script type="text/javascript"> 

function assignedInteractionDataMR()"""),format.raw/*390.37*/("""{"""),format.raw/*390.38*/("""
    """),format.raw/*391.5*/("""// alert("server side data load");
    document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
    document.getElementById("serviceBookTypeDiv").style.display = "none";
    document.getElementById("serviceTypeDiv").style.display = "block";
    document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";

    var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*406.40*/("""{"""),format.raw/*406.41*/("""
    """),format.raw/*407.5*/("""if(myOption.options[i].selected)"""),format.raw/*407.37*/("""{"""),format.raw/*407.38*/("""
        """),format.raw/*408.9*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*408.54*/("""{"""),format.raw/*408.55*/("""
    	 """),format.raw/*409.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*410.9*/("""}"""),format.raw/*410.10*/("""
    """),format.raw/*411.5*/("""}"""),format.raw/*411.6*/("""
"""),format.raw/*412.1*/("""}"""),format.raw/*412.2*/("""
"""),format.raw/*413.1*/("""if(UserIds.length > 0)"""),format.raw/*413.23*/("""{"""),format.raw/*413.24*/("""
    	"""),format.raw/*414.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
"""),format.raw/*415.1*/("""}"""),format.raw/*415.2*/("""
"""),format.raw/*416.1*/("""else"""),format.raw/*416.5*/("""{"""),format.raw/*416.6*/("""
"""),format.raw/*417.1*/("""UserIds="Select";
"""),format.raw/*418.1*/("""}"""),format.raw/*418.2*/("""
    """),format.raw/*419.5*/("""var ajaxUrl = "/assignedInteractionTableDataMR/"+UserIds+"";
    
    
    var table= $('#assignedInteractionTableMR').dataTable( """),format.raw/*422.60*/("""{"""),format.raw/*422.61*/("""
        """),format.raw/*423.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,	
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*432.59*/("""{"""),format.raw/*432.60*/("""
              """),format.raw/*433.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*435.15*/("""}"""),format.raw/*435.16*/("""
    """),format.raw/*436.5*/("""}"""),format.raw/*436.6*/(""" """),format.raw/*436.7*/(""");

    FilterOptionDataMR(table);
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*440.61*/("""{"""),format.raw/*440.62*/("""
      """),format.raw/*441.7*/("""$($.fn.dataTable.tables(true)).DataTable()
         .columns.adjust();
   """),format.raw/*443.4*/("""}"""),format.raw/*443.5*/(""");
    
    """),format.raw/*445.5*/("""}"""),format.raw/*445.6*/("""




"""),format.raw/*450.1*/("""function ajaxCallForFollowUpRequiredServerMR()"""),format.raw/*450.47*/("""{"""),format.raw/*450.48*/("""
	"""),format.raw/*451.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "block";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";
	          

	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*466.41*/("""{"""),format.raw/*466.42*/("""
	    """),format.raw/*467.6*/("""if(myOption.options[i].selected)"""),format.raw/*467.38*/("""{"""),format.raw/*467.39*/("""
	        """),format.raw/*468.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*468.55*/("""{"""),format.raw/*468.56*/("""
	    	 """),format.raw/*469.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*470.10*/("""}"""),format.raw/*470.11*/("""
	    """),format.raw/*471.6*/("""}"""),format.raw/*471.7*/("""
	"""),format.raw/*472.2*/("""}"""),format.raw/*472.3*/("""
	"""),format.raw/*473.2*/("""if(UserIds.length > 0)"""),format.raw/*473.24*/("""{"""),format.raw/*473.25*/("""
	    	"""),format.raw/*474.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*475.2*/("""}"""),format.raw/*475.3*/("""
	"""),format.raw/*476.2*/("""else"""),format.raw/*476.6*/("""{"""),format.raw/*476.7*/("""
	"""),format.raw/*477.2*/("""UserIds="Select";
	"""),format.raw/*478.2*/("""}"""),format.raw/*478.3*/("""
	    """),format.raw/*479.6*/("""var ajaxUrl = "/followUpCallLogTableDataMR/"+UserIds+"";
	    
	    
	    var table= $('#followUpRequiredServerDataMR').dataTable( """),format.raw/*482.63*/("""{"""),format.raw/*482.64*/("""
	        """),format.raw/*483.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*492.60*/("""{"""),format.raw/*492.61*/("""
              """),format.raw/*493.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*495.15*/("""}"""),format.raw/*495.16*/("""
	    """),format.raw/*496.6*/("""}"""),format.raw/*496.7*/(""" """),format.raw/*496.8*/(""");

	    FilterOptionDataMR(table);
	    
	"""),format.raw/*500.2*/("""}"""),format.raw/*500.3*/("""


"""),format.raw/*503.1*/("""function ajaxCallForserviceBookedServerDataMR()"""),format.raw/*503.48*/("""{"""),format.raw/*503.49*/("""
	"""),format.raw/*504.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	document.getElementById("serviceBookTypeDiv").style.display = "block";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";

	          

	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*520.41*/("""{"""),format.raw/*520.42*/("""
	    """),format.raw/*521.6*/("""if(myOption.options[i].selected)"""),format.raw/*521.38*/("""{"""),format.raw/*521.39*/("""
	        """),format.raw/*522.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*522.55*/("""{"""),format.raw/*522.56*/("""
	    	 """),format.raw/*523.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*524.10*/("""}"""),format.raw/*524.11*/("""
	    """),format.raw/*525.6*/("""}"""),format.raw/*525.7*/("""
	"""),format.raw/*526.2*/("""}"""),format.raw/*526.3*/("""
	"""),format.raw/*527.2*/("""if(UserIds.length > 0)"""),format.raw/*527.24*/("""{"""),format.raw/*527.25*/("""
	    	"""),format.raw/*528.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*529.2*/("""}"""),format.raw/*529.3*/("""
	"""),format.raw/*530.2*/("""else"""),format.raw/*530.6*/("""{"""),format.raw/*530.7*/("""
	"""),format.raw/*531.2*/("""UserIds="Select";
	"""),format.raw/*532.2*/("""}"""),format.raw/*532.3*/("""
	    """),format.raw/*533.6*/("""var ajaxUrl = "/serviceBookedServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#serviceBookedServerDataMR').dataTable( """),format.raw/*536.60*/("""{"""),format.raw/*536.61*/("""
	        """),format.raw/*537.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*546.60*/("""{"""),format.raw/*546.61*/("""
              """),format.raw/*547.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*549.15*/("""}"""),format.raw/*549.16*/("""
	    """),format.raw/*550.6*/("""}"""),format.raw/*550.7*/(""" """),format.raw/*550.8*/(""");

	    FilterOptionDataMR(table);
	    
	   	    
	    """),format.raw/*555.6*/("""}"""),format.raw/*555.7*/("""

"""),format.raw/*557.1*/("""function ajaxCallForNonContactsServerMR()"""),format.raw/*557.42*/("""{"""),format.raw/*557.43*/("""
	"""),format.raw/*558.2*/("""//alert("noncontact");
	
		document.getElementById("cresDiv").style.display = "block";
		document.getElementById("workshopDiv").style.display = "block";
		document.getElementById("cityDiv").style.display = "block";	
		document.getElementById("campaignDiv").style.display = "block";
		document.getElementById("serviceBookTypeDiv").style.display = "none";
		document.getElementById("serviceTypeDiv").style.display = "none";
		document.getElementById("fromDateDiv").style.display = "block";
		document.getElementById("toDateDiv").style.display = "block";
		document.getElementById("lastDispoTypeDiv").style.display = "block";
		document.getElementById("droppedcountDiv").style.display = "block";

	var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*574.40*/("""{"""),format.raw/*574.41*/("""
    """),format.raw/*575.5*/("""if(myOption.options[i].selected)"""),format.raw/*575.37*/("""{"""),format.raw/*575.38*/("""
        """),format.raw/*576.9*/("""if(myOption.options[i].value != "Select")"""),format.raw/*576.50*/("""{"""),format.raw/*576.51*/("""
    		"""),format.raw/*577.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*578.9*/("""}"""),format.raw/*578.10*/("""
    """),format.raw/*579.5*/("""}"""),format.raw/*579.6*/("""
"""),format.raw/*580.1*/("""}"""),format.raw/*580.2*/("""
	"""),format.raw/*581.2*/("""if(UserIds.length > 0)"""),format.raw/*581.24*/("""{"""),format.raw/*581.25*/("""
    	"""),format.raw/*582.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*583.2*/("""}"""),format.raw/*583.3*/("""
	"""),format.raw/*584.2*/("""else"""),format.raw/*584.6*/("""{"""),format.raw/*584.7*/("""
		"""),format.raw/*585.3*/("""UserIds="Select";
	"""),format.raw/*586.2*/("""}"""),format.raw/*586.3*/("""
    """),format.raw/*587.5*/("""var ajaxUrl = "/nonContactsServerDataTableMR/"+UserIds
     
    var table = $('#nonContactsServerDataMR').dataTable( """),format.raw/*589.58*/("""{"""),format.raw/*589.59*/("""
        """),format.raw/*590.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*599.59*/("""{"""),format.raw/*599.60*/("""
              """),format.raw/*600.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*602.15*/("""}"""),format.raw/*602.16*/("""
    """),format.raw/*603.5*/("""}"""),format.raw/*603.6*/(""" """),format.raw/*603.7*/(""");

    FilterOptionDataMR(table);
    
    """),format.raw/*607.5*/("""}"""),format.raw/*607.6*/("""





"""),format.raw/*613.1*/("""function ajaxCallForServiceNotRequiredServerMR()"""),format.raw/*613.49*/("""{"""),format.raw/*613.50*/("""
	"""),format.raw/*614.2*/("""//alert("servicenot required");
	
	document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "none";
	document.getElementById("toDateDiv").style.display = "none";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";

	var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*630.40*/("""{"""),format.raw/*630.41*/("""
    """),format.raw/*631.5*/("""if(myOption.options[i].selected)"""),format.raw/*631.37*/("""{"""),format.raw/*631.38*/("""
        """),format.raw/*632.9*/("""if(myOption.options[i].value != "Select")"""),format.raw/*632.50*/("""{"""),format.raw/*632.51*/("""
    		"""),format.raw/*633.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*634.9*/("""}"""),format.raw/*634.10*/("""
    """),format.raw/*635.5*/("""}"""),format.raw/*635.6*/("""
"""),format.raw/*636.1*/("""}"""),format.raw/*636.2*/("""
	"""),format.raw/*637.2*/("""if(UserIds.length > 0)"""),format.raw/*637.24*/("""{"""),format.raw/*637.25*/("""
    	"""),format.raw/*638.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*639.2*/("""}"""),format.raw/*639.3*/("""
	"""),format.raw/*640.2*/("""else"""),format.raw/*640.6*/("""{"""),format.raw/*640.7*/("""
		"""),format.raw/*641.3*/("""UserIds="Select";
	"""),format.raw/*642.2*/("""}"""),format.raw/*642.3*/("""
    """),format.raw/*643.5*/("""var ajaxUrl = "/serviceNotRequiredServerDataTableMR/"+UserIds
     
    var table= $('#serviceNotRequiredServerDataMR').dataTable( """),format.raw/*645.64*/("""{"""),format.raw/*645.65*/("""
        """),format.raw/*646.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*654.59*/("""{"""),format.raw/*654.60*/("""
              """),format.raw/*655.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*657.15*/("""}"""),format.raw/*657.16*/("""
    """),format.raw/*658.5*/("""}"""),format.raw/*658.6*/(""" """),format.raw/*658.7*/(""");

    FilterOptionDataMR(table);
    
    """),format.raw/*662.5*/("""}"""),format.raw/*662.6*/("""

"""),format.raw/*664.1*/("""function ajaxCallFordroppedCallsServerDataMR()"""),format.raw/*664.47*/("""{"""),format.raw/*664.48*/("""
	"""),format.raw/*665.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "none";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "none";
	document.getElementById("toDateDiv").style.display = "none";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";


	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*680.41*/("""{"""),format.raw/*680.42*/("""
	    """),format.raw/*681.6*/("""if(myOption.options[i].selected)"""),format.raw/*681.38*/("""{"""),format.raw/*681.39*/("""
	        """),format.raw/*682.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*682.55*/("""{"""),format.raw/*682.56*/("""
	    	 """),format.raw/*683.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*684.10*/("""}"""),format.raw/*684.11*/("""
	    """),format.raw/*685.6*/("""}"""),format.raw/*685.7*/("""
	"""),format.raw/*686.2*/("""}"""),format.raw/*686.3*/("""
	"""),format.raw/*687.2*/("""if(UserIds.length > 0)"""),format.raw/*687.24*/("""{"""),format.raw/*687.25*/("""
	    	"""),format.raw/*688.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*689.2*/("""}"""),format.raw/*689.3*/("""
	"""),format.raw/*690.2*/("""else"""),format.raw/*690.6*/("""{"""),format.raw/*690.7*/("""
	"""),format.raw/*691.2*/("""UserIds="Select";
	"""),format.raw/*692.2*/("""}"""),format.raw/*692.3*/("""
	    """),format.raw/*693.6*/("""var ajaxUrl = "/droppedCallsServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#droppedCallsServerDataMR').dataTable( """),format.raw/*696.59*/("""{"""),format.raw/*696.60*/("""
	        """),format.raw/*697.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*706.60*/("""{"""),format.raw/*706.61*/("""
              """),format.raw/*707.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*709.15*/("""}"""),format.raw/*709.16*/("""
	    """),format.raw/*710.6*/("""}"""),format.raw/*710.7*/(""" """),format.raw/*710.8*/(""");

	    FilterOptionDataMR(table);
	   
	    
	"""),format.raw/*715.2*/("""}"""),format.raw/*715.3*/("""
"""),format.raw/*716.1*/("""function ajaxCallMissedCallsServerDataMR()"""),format.raw/*716.43*/("""{"""),format.raw/*716.44*/("""
	"""),format.raw/*717.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "none";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";


	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*732.41*/("""{"""),format.raw/*732.42*/("""
	    """),format.raw/*733.6*/("""if(myOption.options[i].selected)"""),format.raw/*733.38*/("""{"""),format.raw/*733.39*/("""
	        """),format.raw/*734.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*734.55*/("""{"""),format.raw/*734.56*/("""
	    	 """),format.raw/*735.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*736.10*/("""}"""),format.raw/*736.11*/("""
	    """),format.raw/*737.6*/("""}"""),format.raw/*737.7*/("""
	"""),format.raw/*738.2*/("""}"""),format.raw/*738.3*/("""
	"""),format.raw/*739.2*/("""if(UserIds.length > 0)"""),format.raw/*739.24*/("""{"""),format.raw/*739.25*/("""
	    	"""),format.raw/*740.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*741.2*/("""}"""),format.raw/*741.3*/("""
	"""),format.raw/*742.2*/("""else"""),format.raw/*742.6*/("""{"""),format.raw/*742.7*/("""
	"""),format.raw/*743.2*/("""UserIds="Select";
	"""),format.raw/*744.2*/("""}"""),format.raw/*744.3*/("""
	    """),format.raw/*745.6*/("""var ajaxUrl = "/missedCallsServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#missedCallsServerDataMR').dataTable( """),format.raw/*748.58*/("""{"""),format.raw/*748.59*/("""
	        """),format.raw/*749.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*758.60*/("""{"""),format.raw/*758.61*/("""
              """),format.raw/*759.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*761.15*/("""}"""),format.raw/*761.16*/("""
	    """),format.raw/*762.6*/("""}"""),format.raw/*762.7*/(""" """),format.raw/*762.8*/(""");

	    FilterOptionDataMR(table);
	   
	    
	"""),format.raw/*767.2*/("""}"""),format.raw/*767.3*/("""

"""),format.raw/*769.1*/("""function ajaxCallForincomingCallsServerDataMR()"""),format.raw/*769.48*/("""{"""),format.raw/*769.49*/("""
	"""),format.raw/*770.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "none";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";


	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*785.41*/("""{"""),format.raw/*785.42*/("""
	    """),format.raw/*786.6*/("""if(myOption.options[i].selected)"""),format.raw/*786.38*/("""{"""),format.raw/*786.39*/("""
	        """),format.raw/*787.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*787.55*/("""{"""),format.raw/*787.56*/("""
	    	 """),format.raw/*788.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*789.10*/("""}"""),format.raw/*789.11*/("""
	    """),format.raw/*790.6*/("""}"""),format.raw/*790.7*/("""
	"""),format.raw/*791.2*/("""}"""),format.raw/*791.3*/("""
	"""),format.raw/*792.2*/("""if(UserIds.length > 0)"""),format.raw/*792.24*/("""{"""),format.raw/*792.25*/("""
	    	"""),format.raw/*793.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*794.2*/("""}"""),format.raw/*794.3*/("""
	"""),format.raw/*795.2*/("""else"""),format.raw/*795.6*/("""{"""),format.raw/*795.7*/("""
	"""),format.raw/*796.2*/("""UserIds="Select";
	"""),format.raw/*797.2*/("""}"""),format.raw/*797.3*/("""
	    """),format.raw/*798.6*/("""var ajaxUrl = "/incomingCallsServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#incomingCallsServerDataMR').dataTable( """),format.raw/*801.60*/("""{"""),format.raw/*801.61*/("""
	        """),format.raw/*802.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*811.60*/("""{"""),format.raw/*811.61*/("""
              """),format.raw/*812.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*814.15*/("""}"""),format.raw/*814.16*/("""
	    """),format.raw/*815.6*/("""}"""),format.raw/*815.7*/(""" """),format.raw/*815.8*/(""");

	    FilterOptionDataMR(table);
	   
	    
	"""),format.raw/*820.2*/("""}"""),format.raw/*820.3*/("""

"""),format.raw/*822.1*/("""function ajaxCallForOutgoingCallBucCallsServerDataMR()"""),format.raw/*822.55*/("""{"""),format.raw/*822.56*/("""
	"""),format.raw/*823.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "none";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";


	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*838.41*/("""{"""),format.raw/*838.42*/("""
	    """),format.raw/*839.6*/("""if(myOption.options[i].selected)"""),format.raw/*839.38*/("""{"""),format.raw/*839.39*/("""
	        """),format.raw/*840.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*840.55*/("""{"""),format.raw/*840.56*/("""
	    	 """),format.raw/*841.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*842.10*/("""}"""),format.raw/*842.11*/("""
	    """),format.raw/*843.6*/("""}"""),format.raw/*843.7*/("""
	"""),format.raw/*844.2*/("""}"""),format.raw/*844.3*/("""
	"""),format.raw/*845.2*/("""if(UserIds.length > 0)"""),format.raw/*845.24*/("""{"""),format.raw/*845.25*/("""
	    	"""),format.raw/*846.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*847.2*/("""}"""),format.raw/*847.3*/("""
	"""),format.raw/*848.2*/("""else"""),format.raw/*848.6*/("""{"""),format.raw/*848.7*/("""
	"""),format.raw/*849.2*/("""UserIds="Select";
	"""),format.raw/*850.2*/("""}"""),format.raw/*850.3*/("""
	    """),format.raw/*851.6*/("""var ajaxUrl = "/outgoingCallsServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#outgoingCallsServerDataMR').dataTable( """),format.raw/*854.60*/("""{"""),format.raw/*854.61*/("""
	        """),format.raw/*855.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*864.60*/("""{"""),format.raw/*864.61*/("""
              """),format.raw/*865.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*867.15*/("""}"""),format.raw/*867.16*/("""
	    """),format.raw/*868.6*/("""}"""),format.raw/*868.7*/(""" """),format.raw/*868.8*/(""");

	    FilterOptionDataMR(table);
	   
"""),format.raw/*872.1*/("""}"""),format.raw/*872.2*/("""
"""),format.raw/*873.1*/("""</script>
<script type="text/javascript">  
function FilterOptionDataMR(table)"""),format.raw/*875.35*/("""{"""),format.raw/*875.36*/("""
	"""),format.raw/*876.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*877.39*/("""{"""),format.raw/*877.40*/("""
    	"""),format.raw/*878.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
	    console.log("i : "+i);
		console.log("v length "+v.length);
	    
	    if(v.length>0)"""),format.raw/*884.20*/("""{"""),format.raw/*884.21*/("""
		    
		    """),format.raw/*886.7*/("""if(i == 7)"""),format.raw/*886.17*/("""{"""),format.raw/*886.18*/("""	

		    	"""),format.raw/*888.8*/("""var UserIds="",j;
		        myOption = document.getElementById('ddlCreIds');
		        console.log("here"+myOption);		        
		    for (j=0;j<myOption.options.length;j++)"""),format.raw/*891.46*/("""{"""),format.raw/*891.47*/("""
		        """),format.raw/*892.11*/("""if(myOption.options[j].selected)"""),format.raw/*892.43*/("""{"""),format.raw/*892.44*/("""
		            """),format.raw/*893.15*/("""if(myOption.options[j].value != "Select")"""),format.raw/*893.56*/("""{"""),format.raw/*893.57*/("""
		        		"""),format.raw/*894.13*/("""UserIds = UserIds + myOption.options[j].value + ",";
		            """),format.raw/*895.15*/("""}"""),format.raw/*895.16*/("""
		        """),format.raw/*896.11*/("""}"""),format.raw/*896.12*/("""
		    """),format.raw/*897.7*/("""}"""),format.raw/*897.8*/("""
		    	"""),format.raw/*898.8*/("""if(UserIds.length > 0)"""),format.raw/*898.30*/("""{"""),format.raw/*898.31*/("""
		        	"""),format.raw/*899.12*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
		        	values[7]=UserIds;
		    	"""),format.raw/*901.8*/("""}"""),format.raw/*901.9*/("""else"""),format.raw/*901.13*/("""{"""),format.raw/*901.14*/("""
		    		"""),format.raw/*902.9*/("""UserIds="Select";
		    		values[7]=UserIds;
		    	"""),format.raw/*904.8*/("""}"""),format.raw/*904.9*/("""
		    
		    """),format.raw/*906.7*/("""}"""),format.raw/*906.8*/("""else"""),format.raw/*906.12*/("""{"""),format.raw/*906.13*/("""
		    	"""),format.raw/*907.8*/("""values[i] = v;
			    """),format.raw/*908.8*/("""}"""),format.raw/*908.9*/("""
	
			       
		  
		    """),format.raw/*912.7*/("""}"""),format.raw/*912.8*/("""		    
	    
	    	"""),format.raw/*914.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');
	 	 //var table = $('#nonContactsServerDataMR').dataTable();
	 	 if(values.length>0) """),format.raw/*919.25*/("""{"""),format.raw/*919.26*/("""
			 """),format.raw/*920.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*920.37*/("""{"""),format.raw/*920.38*/("""
				 """),format.raw/*921.6*/("""console.log("values[i] : "+values[i]);	
			    table.api().columns(i).search(values[i]);
			 """),format.raw/*923.5*/("""}"""),format.raw/*923.6*/("""
			 """),format.raw/*924.5*/("""table.api().draw(); 
		"""),format.raw/*925.3*/("""}"""),format.raw/*925.4*/("""
	 	
		 	  
	"""),format.raw/*928.2*/("""}"""),format.raw/*928.3*/(""");
	
	
"""),format.raw/*931.1*/("""}"""),format.raw/*931.2*/("""
"""),format.raw/*932.1*/("""</script>

 <script type="text/javascript">
	window.onload = assignedInteractionDataMR();
</script>
    """))
      }
    }
  }

  def render(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long, String],campaignList:List[Campaign],serviceTypeList:List[ServiceTypes],bookedServiceTypeList:List[String],dispoList:List[CallDispositionData]): play.twirl.api.HtmlFormat.Appendable = apply(listCity,dealerName,user,allCREHash,campaignList,serviceTypeList,bookedServiceTypeList,dispoList)

  def f:((List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[String],List[CallDispositionData]) => play.twirl.api.HtmlFormat.Appendable) = (listCity,dealerName,user,allCREHash,campaignList,serviceTypeList,bookedServiceTypeList,dispoList) => apply(listCity,dealerName,user,allCREHash,campaignList,serviceTypeList,bookedServiceTypeList,dispoList)

  def ref: this.type = this

}


}

/**/
object callDispositionPageCREManager extends callDispositionPageCREManager_Scope0.callDispositionPageCREManager
              /*
                  -- GENERATED --
                  DATE: Sat Dec 16 16:03:40 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/callDispositionPageCREManager.scala.html
                  HASH: 9b64e07e2d4885a91fcb0367406eac8b7cfea712
                  MATRIX: 906->1|1221->220|1249->223|1307->273|1346->275|1381->284|1428->304|1456->305|1510->333|1633->429|1661->430|1692->434|3852->2568|3889->2589|3928->2590|4022->2656|4065->2672|4078->2676|4109->2686|4139->2689|4152->2693|4183->2703|4246->2735|4292->2753|5403->3838|5451->3870|5490->3871|5554->3907|5597->3923|5619->3936|5658->3954|5688->3957|5710->3970|5750->3988|5824->4031|5895->4075|6747->4900|6802->4938|6842->4939|6908->4976|6952->4992|6978->5008|7021->5029|7052->5032|7079->5048|7123->5069|7199->5113|7272->5158|7628->5487|7695->5537|7735->5538|7766->5541|7810->5557|7854->5579|7885->5582|7929->5604|8008->5651|8083->5698|8389->5976|8429->5999|8469->6000|8502->6005|8546->6021|8561->6026|8600->6043|8631->6046|8646->6051|8685->6068|8735->6087|8819->6143|23798->21091|23831->21096|23932->21168|23962->21169|23996->21175|24864->22014|24894->22015|24928->22021|24989->22053|25019->22054|25057->22064|25131->22109|25161->22110|25197->22118|25287->22180|25317->22181|25351->22187|25380->22188|25410->22190|25439->22191|25469->22193|25520->22215|25550->22216|25585->22223|25666->22276|25695->22277|25725->22279|25757->22283|25786->22284|25816->22286|25863->22305|25892->22306|25926->22312|26088->22445|26118->22446|26156->22456|26472->22743|26502->22744|26547->22760|26654->22838|26684->22839|26718->22845|26747->22846|26776->22847|26909->22951|26939->22952|26975->22960|27079->23036|27108->23037|27150->23051|27179->23052|27217->23062|27292->23108|27322->23109|27353->23112|28189->23919|28219->23920|28254->23927|28315->23959|28345->23960|28385->23971|28459->24016|28489->24017|28526->24026|28618->24089|28648->24090|28683->24097|28712->24098|28743->24101|28772->24102|28803->24105|28854->24127|28884->24128|28920->24136|29002->24190|29031->24191|29062->24194|29094->24198|29123->24199|29154->24202|29202->24222|29231->24223|29266->24230|29429->24364|29459->24365|29499->24376|29824->24672|29854->24673|29899->24689|30006->24767|30036->24768|30071->24775|30100->24776|30129->24777|30204->24824|30233->24825|30267->24831|30343->24878|30373->24879|30404->24882|31242->25691|31272->25692|31307->25699|31368->25731|31398->25732|31438->25743|31512->25788|31542->25789|31579->25798|31671->25861|31701->25862|31736->25869|31765->25870|31796->25873|31825->25874|31856->25877|31907->25899|31937->25900|31973->25908|32055->25962|32084->25963|32115->25966|32147->25970|32176->25971|32207->25974|32255->25994|32284->25995|32319->26002|32483->26137|32513->26138|32553->26149|32878->26445|32908->26446|32953->26462|33060->26540|33090->26541|33125->26548|33154->26549|33183->26550|33273->26612|33302->26613|33334->26617|33404->26658|33434->26659|33465->26662|34320->27488|34350->27489|34384->27495|34445->27527|34475->27528|34513->27538|34583->27579|34613->27580|34649->27588|34739->27650|34769->27651|34803->27657|34832->27658|34862->27660|34891->27661|34922->27664|34973->27686|35003->27687|35038->27694|35120->27748|35149->27749|35180->27752|35212->27756|35241->27757|35273->27761|35321->27781|35350->27782|35384->27788|35533->27908|35563->27909|35601->27919|35919->28208|35949->28209|35994->28225|36101->28303|36131->28304|36165->28310|36194->28311|36223->28312|36299->28360|36328->28361|36368->28373|36445->28421|36475->28422|36506->28425|37356->29246|37386->29247|37420->29253|37481->29285|37511->29286|37549->29296|37619->29337|37649->29338|37685->29346|37775->29408|37805->29409|37839->29415|37868->29416|37898->29418|37927->29419|37958->29422|38009->29444|38039->29445|38074->29452|38156->29506|38185->29507|38216->29510|38248->29514|38277->29515|38309->29519|38357->29539|38386->29540|38420->29546|38582->29679|38612->29680|38650->29690|38947->29958|38977->29959|39022->29975|39129->30053|39159->30054|39193->30060|39222->30061|39251->30062|39327->30110|39356->30111|39388->30115|39463->30161|39493->30162|39524->30165|40345->30957|40375->30958|40410->30965|40471->30997|40501->30998|40541->31009|40615->31054|40645->31055|40682->31064|40774->31127|40804->31128|40839->31135|40868->31136|40899->31139|40928->31140|40959->31143|41010->31165|41040->31166|41076->31174|41158->31228|41187->31229|41218->31232|41250->31236|41279->31237|41310->31240|41358->31260|41387->31261|41422->31268|41584->31401|41614->31402|41654->31413|41979->31709|42009->31710|42054->31726|42161->31804|42191->31805|42226->31812|42255->31813|42284->31814|42365->31867|42394->31868|42424->31870|42495->31912|42525->31913|42556->31916|43379->32710|43409->32711|43444->32718|43505->32750|43535->32751|43575->32762|43649->32807|43679->32808|43716->32817|43808->32880|43838->32881|43873->32888|43902->32889|43933->32892|43962->32893|43993->32896|44044->32918|44074->32919|44110->32927|44192->32981|44221->32982|44252->32985|44284->32989|44313->32990|44344->32993|44392->33013|44421->33014|44456->33021|44616->33152|44646->33153|44686->33164|45011->33460|45041->33461|45086->33477|45193->33555|45223->33556|45258->33563|45287->33564|45316->33565|45397->33618|45426->33619|45458->33623|45534->33670|45564->33671|45595->33674|46418->34468|46448->34469|46483->34476|46544->34508|46574->34509|46614->34520|46688->34565|46718->34566|46755->34575|46847->34638|46877->34639|46912->34646|46941->34647|46972->34650|47001->34651|47032->34654|47083->34676|47113->34677|47149->34685|47231->34739|47260->34740|47291->34743|47323->34747|47352->34748|47383->34751|47431->34771|47460->34772|47495->34779|47659->34914|47689->34915|47729->34926|48054->35222|48084->35223|48129->35239|48236->35317|48266->35318|48301->35325|48330->35326|48359->35327|48440->35380|48469->35381|48501->35385|48584->35439|48614->35440|48645->35443|49468->36237|49498->36238|49533->36245|49594->36277|49624->36278|49664->36289|49738->36334|49768->36335|49805->36344|49897->36407|49927->36408|49962->36415|49991->36416|50022->36419|50051->36420|50082->36423|50133->36445|50163->36446|50199->36454|50281->36508|50310->36509|50341->36512|50373->36516|50402->36517|50433->36520|50481->36540|50510->36541|50545->36548|50709->36683|50739->36684|50779->36695|51104->36991|51134->36992|51179->37008|51286->37086|51316->37087|51351->37094|51380->37095|51409->37096|51482->37141|51511->37142|51541->37144|51650->37224|51680->37225|51711->37228|51796->37284|51826->37285|51861->37292|52101->37503|52131->37504|52175->37520|52214->37530|52244->37531|52284->37543|52488->37718|52518->37719|52559->37731|52620->37763|52650->37764|52695->37780|52765->37821|52795->37822|52838->37836|52935->37904|52965->37905|53006->37917|53036->37918|53072->37926|53101->37927|53138->37936|53189->37958|53219->37959|53261->37972|53380->38063|53409->38064|53442->38068|53472->38069|53510->38079|53592->38133|53621->38134|53665->38150|53694->38151|53727->38155|53757->38156|53794->38165|53845->38188|53874->38189|53931->38218|53960->38219|54009->38240|54270->38472|54300->38473|54334->38479|54395->38511|54425->38512|54460->38519|54583->38614|54612->38615|54646->38621|54698->38645|54727->38646|54771->38662|54800->38663|54838->38673|54867->38674|54897->38676
                  LINES: 27->1|32->1|33->2|33->2|33->2|37->6|38->7|38->7|40->9|43->12|43->12|45->14|76->45|76->45|76->45|77->46|77->46|77->46|77->46|77->46|77->46|77->46|78->47|79->48|109->78|109->78|109->78|110->79|110->79|110->79|110->79|110->79|110->79|110->79|111->80|113->82|133->102|133->102|133->102|134->103|134->103|134->103|134->103|134->103|134->103|134->103|135->104|137->106|144->113|144->113|144->113|145->114|145->114|145->114|145->114|145->114|146->115|148->117|154->123|154->123|154->123|155->124|155->124|155->124|155->124|155->124|155->124|155->124|157->126|160->129|417->386|419->388|421->390|421->390|422->391|437->406|437->406|438->407|438->407|438->407|439->408|439->408|439->408|440->409|441->410|441->410|442->411|442->411|443->412|443->412|444->413|444->413|444->413|445->414|446->415|446->415|447->416|447->416|447->416|448->417|449->418|449->418|450->419|453->422|453->422|454->423|463->432|463->432|464->433|466->435|466->435|467->436|467->436|467->436|471->440|471->440|472->441|474->443|474->443|476->445|476->445|481->450|481->450|481->450|482->451|497->466|497->466|498->467|498->467|498->467|499->468|499->468|499->468|500->469|501->470|501->470|502->471|502->471|503->472|503->472|504->473|504->473|504->473|505->474|506->475|506->475|507->476|507->476|507->476|508->477|509->478|509->478|510->479|513->482|513->482|514->483|523->492|523->492|524->493|526->495|526->495|527->496|527->496|527->496|531->500|531->500|534->503|534->503|534->503|535->504|551->520|551->520|552->521|552->521|552->521|553->522|553->522|553->522|554->523|555->524|555->524|556->525|556->525|557->526|557->526|558->527|558->527|558->527|559->528|560->529|560->529|561->530|561->530|561->530|562->531|563->532|563->532|564->533|567->536|567->536|568->537|577->546|577->546|578->547|580->549|580->549|581->550|581->550|581->550|586->555|586->555|588->557|588->557|588->557|589->558|605->574|605->574|606->575|606->575|606->575|607->576|607->576|607->576|608->577|609->578|609->578|610->579|610->579|611->580|611->580|612->581|612->581|612->581|613->582|614->583|614->583|615->584|615->584|615->584|616->585|617->586|617->586|618->587|620->589|620->589|621->590|630->599|630->599|631->600|633->602|633->602|634->603|634->603|634->603|638->607|638->607|644->613|644->613|644->613|645->614|661->630|661->630|662->631|662->631|662->631|663->632|663->632|663->632|664->633|665->634|665->634|666->635|666->635|667->636|667->636|668->637|668->637|668->637|669->638|670->639|670->639|671->640|671->640|671->640|672->641|673->642|673->642|674->643|676->645|676->645|677->646|685->654|685->654|686->655|688->657|688->657|689->658|689->658|689->658|693->662|693->662|695->664|695->664|695->664|696->665|711->680|711->680|712->681|712->681|712->681|713->682|713->682|713->682|714->683|715->684|715->684|716->685|716->685|717->686|717->686|718->687|718->687|718->687|719->688|720->689|720->689|721->690|721->690|721->690|722->691|723->692|723->692|724->693|727->696|727->696|728->697|737->706|737->706|738->707|740->709|740->709|741->710|741->710|741->710|746->715|746->715|747->716|747->716|747->716|748->717|763->732|763->732|764->733|764->733|764->733|765->734|765->734|765->734|766->735|767->736|767->736|768->737|768->737|769->738|769->738|770->739|770->739|770->739|771->740|772->741|772->741|773->742|773->742|773->742|774->743|775->744|775->744|776->745|779->748|779->748|780->749|789->758|789->758|790->759|792->761|792->761|793->762|793->762|793->762|798->767|798->767|800->769|800->769|800->769|801->770|816->785|816->785|817->786|817->786|817->786|818->787|818->787|818->787|819->788|820->789|820->789|821->790|821->790|822->791|822->791|823->792|823->792|823->792|824->793|825->794|825->794|826->795|826->795|826->795|827->796|828->797|828->797|829->798|832->801|832->801|833->802|842->811|842->811|843->812|845->814|845->814|846->815|846->815|846->815|851->820|851->820|853->822|853->822|853->822|854->823|869->838|869->838|870->839|870->839|870->839|871->840|871->840|871->840|872->841|873->842|873->842|874->843|874->843|875->844|875->844|876->845|876->845|876->845|877->846|878->847|878->847|879->848|879->848|879->848|880->849|881->850|881->850|882->851|885->854|885->854|886->855|895->864|895->864|896->865|898->867|898->867|899->868|899->868|899->868|903->872|903->872|904->873|906->875|906->875|907->876|908->877|908->877|909->878|915->884|915->884|917->886|917->886|917->886|919->888|922->891|922->891|923->892|923->892|923->892|924->893|924->893|924->893|925->894|926->895|926->895|927->896|927->896|928->897|928->897|929->898|929->898|929->898|930->899|932->901|932->901|932->901|932->901|933->902|935->904|935->904|937->906|937->906|937->906|937->906|938->907|939->908|939->908|943->912|943->912|945->914|950->919|950->919|951->920|951->920|951->920|952->921|954->923|954->923|955->924|956->925|956->925|959->928|959->928|962->931|962->931|963->932
                  -- GENERATED --
              */
          