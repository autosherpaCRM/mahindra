
package views.html.recording

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object callRecordingPagePSF_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class callRecordingPagePSF extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,String,List[CallDispositionData],List[Campaign],List[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(user:String,dealer:String,dispoList:List[CallDispositionData],campaignList :List[Campaign],crelist :List[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.117*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealer)/*2.48*/ {_display_(Seq[Any](format.raw/*2.50*/("""
	

    """),format.raw/*5.5*/("""<div class="panel panel-primary">
      <div class="panel-heading"><b>PSF Call Recording</b></div>
      <div class="panel-body">
	  
      <div class="row">
      	<div class="col-md-12">
		<div class="panel panel-default">
	  <div class="panel-body">
      		   <div class="row">
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Call Date From</label>
      					<input type="text" class="filter form-control datepickerFilter" data-column-index="0" placeholder="Call Date From" readonly/>	
      				</div>
      			</div>
      		 <div class="col-md-2">
      				<div class="form-group">
      				<label>Call Date To</label>
      					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" placeholder="Call Date To" readonly/>	
      				</div>
      			</div>
      		
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Call Type</label>
      						
      						<select class="filter form-control" data-column-index="2">      						
      						<option value="0" >--Select--</option>
      						<option value="OUTGOING" >OUTGOING</option>
      						<option value="INCOMING" >INCOMING</option>
      						<option value="MISSED" >MISSED</option>
                
                    
                </select>
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Campaign</label>
      						
      						<select class="filter form-control" data-column-index="3">
      						<option value="0" >--Select--</option>
      						"""),_display_(/*47.14*/for(campaign_List<-campaignList) yield /*47.46*/{_display_(Seq[Any](format.raw/*47.47*/("""
		                                """),format.raw/*48.35*/("""<option value=""""),_display_(/*48.51*/campaign_List/*48.64*/.getCampaignName()),format.raw/*48.82*/("""">"""),_display_(/*48.85*/campaign_List/*48.98*/.getCampaignName()),format.raw/*48.116*/("""</option>
		                             """)))}),format.raw/*49.33*/("""
                
                    
                """),format.raw/*52.17*/("""</select>
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>CRM Initiated ?</label>
      						
      						<select class="filter form-control" data-column-index="4">
      						<option value="0" >--Select--</option>
      						<option value="Initiated" >YES</option>
      						<option value="NotDailed" >NO</option>
      						<option value="Not Disposed">Not Disposed</option>
                
                    
                </select>
      				</div>
      			</div>
      			
      			
      			
      			
      			
      	</div>
     
 
      <div class="row">
      
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Disposition</label>
      					
      					<select class="filter form-control" data-column-index="5"> 
      					<option value="0" >--Select--</option>               
                    """),_display_(/*85.22*/for(dispo <- dispoList) yield /*85.45*/{_display_(Seq[Any](format.raw/*85.46*/("""
			"""),format.raw/*86.4*/("""<option value=""""),_display_(/*86.20*/dispo/*86.25*/.getDisposition()),format.raw/*86.42*/("""">"""),_display_(/*86.45*/dispo/*86.50*/.getDisposition()),format.raw/*86.67*/("""</option>
		""")))}),format.raw/*87.4*/("""
                """),format.raw/*88.17*/("""</select>
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>User Name</label>
      					
      					<select class="filter form-control selectpicker" data-column-index="6" id="ddlCreIds" name="ddlCreIds" multiple> 
      					"""),_display_(/*96.13*/for(crelistIs <- crelist) yield /*96.38*/{_display_(Seq[Any](format.raw/*96.39*/("""
			"""),format.raw/*97.4*/("""<option value=""""),_display_(/*97.20*/crelistIs/*97.29*/.getId()),format.raw/*97.37*/("""">"""),_display_(/*97.40*/crelistIs/*97.49*/.getUserName()),format.raw/*97.63*/("""</option>
		""")))}),format.raw/*98.4*/("""
                
                    
                """),format.raw/*101.17*/("""</select>	
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>&nbsp;</label><br />
      					 <button type="submit"  id="searchCust" class="btn btn-primary btn-block">
                    			<i class="fa fa-search"></i>
                    			<span>Search</span>
        				</button>        				 
 	
      				</div>
      			</div>
      			 
      	
      </div>
      </div>
      </div>
      
           <div class="panel panel-default">
	  <div class="panel-body">
      <div class="dataTable_wrapper">
      <table class="table table-striped table-bordered table-hover" id="recordingTable">
      
       
                 <thead>
                                 <tr>
                                    <th>Call Date</th>
                                    <th>Call Time</th>
                                    <th>Call Type</th>                                   
                                    <th>CRM Initiated ?</th>                                    
                                    <th>User Name</th>
                                    <th>Campaign</th>
                                    <th>Customer Name</th>
                                    <th>Phone No</th>
                                    <th>Veh No</th>
                                    <th>Chasssis No</th>
                                    <th>Disposition</th>
                                    <th>Media</th>
                                 </tr>
                              </thead>

                                <tbody>
                                   
                   				</tbody>
           </table>
        </div>
    
       </div>
       </div>
       </div>
    </div>
  </div>
</div>

""")))}),format.raw/*156.2*/("""


"""),format.raw/*159.1*/("""<script type="text/javascript">

 $(document).ready(function() """),format.raw/*161.31*/("""{"""),format.raw/*161.32*/("""
    """),format.raw/*162.5*/("""var table = $('#recordingTable').dataTable( """),format.raw/*162.49*/("""{"""),format.raw/*162.50*/("""
    	"""),format.raw/*163.6*/(""""lengthMenu": [ 10, 25, 50, 75, 100 ],
        "bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
        "paging": true,
        "searching":true,
        "ordering":false,
        "ajax": "/CREManager/recordingDataListPSF"
        
    """),format.raw/*173.5*/("""}"""),format.raw/*173.6*/(""" """),format.raw/*173.7*/(""");

    var values=[];

  
	
   $('.filter').on('change', function() """),format.raw/*179.41*/("""{"""),format.raw/*179.42*/("""
	   
	   """),format.raw/*181.5*/("""var i= $(this).data('columnIndex');
	   var v = $(this).val();
	   if(v.length>0)"""),format.raw/*183.19*/("""{"""),format.raw/*183.20*/("""

		   """),format.raw/*185.6*/("""if(i == 6)"""),format.raw/*185.16*/("""{"""),format.raw/*185.17*/("""
		   """),format.raw/*186.6*/("""var UserIds="",j;
	        myOption = document.getElementById('ddlCreIds');
	        console.log("here"+myOption);		        
	    for (j=0;j<myOption.options.length;j++)"""),format.raw/*189.45*/("""{"""),format.raw/*189.46*/("""
	        """),format.raw/*190.10*/("""if(myOption.options[j].selected)"""),format.raw/*190.42*/("""{"""),format.raw/*190.43*/("""
	            """),format.raw/*191.14*/("""if(myOption.options[j].value != "Select")"""),format.raw/*191.55*/("""{"""),format.raw/*191.56*/("""
	        		"""),format.raw/*192.12*/("""UserIds = UserIds + myOption.options[j].value + ",";
	            """),format.raw/*193.14*/("""}"""),format.raw/*193.15*/("""
	        """),format.raw/*194.10*/("""}"""),format.raw/*194.11*/("""
	    """),format.raw/*195.6*/("""}"""),format.raw/*195.7*/("""
	    	"""),format.raw/*196.7*/("""if(UserIds.length > 0)"""),format.raw/*196.29*/("""{"""),format.raw/*196.30*/("""
	        	"""),format.raw/*197.11*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	        	values[6]=UserIds;
	    	"""),format.raw/*199.7*/("""}"""),format.raw/*199.8*/("""else"""),format.raw/*199.12*/("""{"""),format.raw/*199.13*/("""
	    		"""),format.raw/*200.8*/("""UserIds="Select";
	    		values[6]=UserIds;
	    	"""),format.raw/*202.7*/("""}"""),format.raw/*202.8*/("""
		   """),format.raw/*203.6*/("""}"""),format.raw/*203.7*/("""else"""),format.raw/*203.11*/("""{"""),format.raw/*203.12*/("""
			   """),format.raw/*204.7*/("""values[i] = v;
			   """),format.raw/*205.7*/("""}"""),format.raw/*205.8*/("""
	    	
		   
		 """),format.raw/*208.4*/("""// if(i < 8)
		  
		/*  else if(i == 9)"""),format.raw/*210.22*/("""{"""),format.raw/*210.23*/("""			  
			  """),format.raw/*211.6*/("""values[9] = v;
			  values[4] = values[4]+","+values[9];"""),format.raw/*212.42*/("""}"""),format.raw/*212.43*/("""*/
		   
	   """),format.raw/*214.5*/("""}"""),format.raw/*214.6*/("""
	   
	   	"""),format.raw/*216.6*/("""console.log(JSON.stringify(values));
		console.log(i);
	   	
     	  //clear global search values
    	  //table.api().search('');
    	  $(this).data('columnIndex');
    	  
    	  
   """),format.raw/*224.4*/("""}"""),format.raw/*224.5*/(""");


   $('#searchCust').on('click',function()"""),format.raw/*227.42*/("""{"""),format.raw/*227.43*/("""

	   """),format.raw/*229.5*/("""//alert(values.length);
	   if(values.length>0) """),format.raw/*230.25*/("""{"""),format.raw/*230.26*/("""
			"""),format.raw/*231.4*/("""for(var i=0;i<values.length;i++)"""),format.raw/*231.36*/("""{"""),format.raw/*231.37*/("""
				
			   """),format.raw/*233.7*/("""table.api().columns(i).search(values[i]);
			"""),format.raw/*234.4*/("""}"""),format.raw/*234.5*/("""
			"""),format.raw/*235.4*/("""table.api().draw();
	   """),format.raw/*236.5*/("""}"""),format.raw/*236.6*/("""
	"""),format.raw/*237.2*/("""}"""),format.raw/*237.3*/(""");
	"""),format.raw/*238.2*/("""}"""),format.raw/*238.3*/(""");

	</script>"""))
      }
    }
  }

  def render(user:String,dealer:String,dispoList:List[CallDispositionData],campaignList:List[Campaign],crelist:List[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(user,dealer,dispoList,campaignList,crelist)

  def f:((String,String,List[CallDispositionData],List[Campaign],List[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (user,dealer,dispoList,campaignList,crelist) => apply(user,dealer,dispoList,campaignList,crelist)

  def ref: this.type = this

}


}

/**/
object callRecordingPagePSF extends callRecordingPagePSF_Scope0.callRecordingPagePSF
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:42 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/recording/callRecordingPagePSF.scala.html
                  HASH: 6853dd357001a5a7ee40a750d54280db1a1506da
                  MATRIX: 847->1|1058->116|1086->119|1140->165|1179->167|1216->178|2887->1822|2935->1854|2974->1855|3038->1891|3081->1907|3103->1920|3142->1938|3172->1941|3194->1954|3234->1972|3308->2015|3394->2073|4379->3031|4418->3054|4457->3055|4489->3060|4532->3076|4546->3081|4584->3098|4614->3101|4628->3106|4666->3123|4710->3137|4756->3155|5085->3457|5126->3482|5165->3483|5197->3488|5240->3504|5258->3513|5287->3521|5317->3524|5335->3533|5370->3547|5414->3561|5501->3619|7365->5452|7399->5458|7493->5523|7523->5524|7557->5530|7630->5574|7660->5575|7695->5582|8018->5877|8047->5878|8076->5879|8180->5954|8210->5955|8250->5967|8362->6050|8392->6051|8429->6060|8468->6070|8498->6071|8533->6078|8734->6250|8764->6251|8804->6262|8865->6294|8895->6295|8939->6310|9009->6351|9039->6352|9081->6365|9177->6432|9207->6433|9247->6444|9277->6445|9312->6452|9341->6453|9377->6461|9428->6483|9458->6484|9499->6496|9616->6585|9645->6586|9678->6590|9708->6591|9745->6600|9825->6652|9854->6653|9889->6660|9918->6661|9951->6665|9981->6666|10017->6674|10067->6696|10096->6697|10144->6717|10214->6758|10244->6759|10284->6771|10370->6828|10400->6829|10443->6844|10472->6845|10513->6858|10735->7052|10764->7053|10842->7102|10872->7103|10908->7111|10986->7160|11016->7161|11049->7166|11110->7198|11140->7199|11182->7213|11256->7259|11285->7260|11318->7265|11371->7290|11400->7291|11431->7294|11460->7295|11493->7300|11522->7301
                  LINES: 27->1|32->1|33->2|33->2|33->2|36->5|78->47|78->47|78->47|79->48|79->48|79->48|79->48|79->48|79->48|79->48|80->49|83->52|116->85|116->85|116->85|117->86|117->86|117->86|117->86|117->86|117->86|117->86|118->87|119->88|127->96|127->96|127->96|128->97|128->97|128->97|128->97|128->97|128->97|128->97|129->98|132->101|187->156|190->159|192->161|192->161|193->162|193->162|193->162|194->163|204->173|204->173|204->173|210->179|210->179|212->181|214->183|214->183|216->185|216->185|216->185|217->186|220->189|220->189|221->190|221->190|221->190|222->191|222->191|222->191|223->192|224->193|224->193|225->194|225->194|226->195|226->195|227->196|227->196|227->196|228->197|230->199|230->199|230->199|230->199|231->200|233->202|233->202|234->203|234->203|234->203|234->203|235->204|236->205|236->205|239->208|241->210|241->210|242->211|243->212|243->212|245->214|245->214|247->216|255->224|255->224|258->227|258->227|260->229|261->230|261->230|262->231|262->231|262->231|264->233|265->234|265->234|266->235|267->236|267->236|268->237|268->237|269->238|269->238
                  -- GENERATED --
              */
          