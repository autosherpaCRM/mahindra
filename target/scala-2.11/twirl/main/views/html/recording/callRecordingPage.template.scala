
package views.html.recording

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object callRecordingPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class callRecordingPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,String,List[CallDispositionData],List[Campaign],List[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(user:String,dealer:String,dispoList:List[CallDispositionData],campaignList :List[Campaign],crelist :List[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.117*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealer)/*2.48*/ {_display_(Seq[Any](format.raw/*2.50*/("""
	

    """),format.raw/*5.5*/("""<div class="panel panel-primary">
      <div class="panel-heading"><b>Call Recording</b></div>
      <div class="panel-body">
      <div class="row">
      	<div class="col-md-12">
      		 <div class="panel panel-default">
				<div class="panel-body">
				<div class="row">
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Call Date From</label>
      					<input type="text" class="filter form-control datepickerFilter" data-column-index="0" placeholder="Call Date From" readonly/>	
      				</div>
      			</div>
      		 <div class="col-md-2">
      				<div class="form-group">
      				<label>Call Date To</label>
      					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" placeholder="Call Date To" readonly/>	
      				</div>
      			</div>
      		
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Call Type</label>
      						
      						<select class="filter form-control" data-column-index="2">      						
      						<option value="0" >--Select--</option>
      						<option value="OUTGOING" >OUTGOING</option>
      						<option value="INCOMING" >INCOMING</option>
      						<option value="MISSED" >MISSED</option>
                
                    
                </select>
      				</div>
      			</div>
      			<div class="col-md-3">
      				<div class="form-group">
      				<label>Campaign</label>
      						
      						<select class="filter form-control" data-column-index="3">
      						<option value="0" >--Select--</option>
      						"""),_display_(/*46.14*/for(campaign_List<-campaignList) yield /*46.46*/{_display_(Seq[Any](format.raw/*46.47*/("""
		                                """),format.raw/*47.35*/("""<option value=""""),_display_(/*47.51*/campaign_List/*47.64*/.getCampaignName()),format.raw/*47.82*/("""">"""),_display_(/*47.85*/campaign_List/*47.98*/.getCampaignName()),format.raw/*47.116*/("""</option>
		                             """)))}),format.raw/*48.33*/("""
                
                    
                """),format.raw/*51.17*/("""</select>
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>CRM Initiated ?</label>
      						
      						<select class="filter form-control" data-column-index="4">
      						<option value="0" >--Select--</option>
      						<option value="Initiated" >YES</option>
      						<option value="NotDailed" >NO</option>
      						<option value="Not Disposed">Not Disposed</option>
                
                    
                </select>
      				</div>
      			</div>
      			</div>
      			
      			
     
      <div class="row">
      
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Disposition</label>
      					
      					<select class="filter form-control" data-column-index="5"> 
      					<option value="0" >--Select--</option>               
                    """),_display_(/*80.22*/for(dispo <- dispoList) yield /*80.45*/{_display_(Seq[Any](format.raw/*80.46*/("""
			"""),format.raw/*81.4*/("""<option value=""""),_display_(/*81.20*/dispo/*81.25*/.getDisposition()),format.raw/*81.42*/("""">"""),_display_(/*81.45*/dispo/*81.50*/.getDisposition()),format.raw/*81.67*/("""</option>
		""")))}),format.raw/*82.4*/("""
                """),format.raw/*83.17*/("""</select>
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>User Name</label>
      					
      					<select class="filter form-control selectpicker" data-column-index="6" id="ddlCreIds" name="ddlCreIds" multiple> 
      					"""),_display_(/*91.13*/for(crelistIs <- crelist) yield /*91.38*/{_display_(Seq[Any](format.raw/*91.39*/("""
			"""),format.raw/*92.4*/("""<option value=""""),_display_(/*92.20*/crelistIs/*92.29*/.getId()),format.raw/*92.37*/("""">"""),_display_(/*92.40*/crelistIs/*92.49*/.getUserName()),format.raw/*92.63*/("""</option>
		""")))}),format.raw/*93.4*/("""
                
                    
                """),format.raw/*96.17*/("""</select>	
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>&nbsp;</label><br />
      					 <button type="submit"  id="searchCust" class="btn btn-primary btn-block">
                    			<i class="fa fa-search"></i>
                    			<span>Search</span>
        				</button>        				 
 	
      				</div>
      			</div>
      			 
      
      </div>
      </div>
      </div>
      <div class="panel panel-default">
	  <div class="panel-body">
      <div class="dataTable_wrapper">
      <table class="table table-striped table-bordered table-hover" id="recordingTable">
      
       
                 <thead>
                                 <tr>
                                    <th>Call Date</th>
                                    <th>Call Time</th>
                                    <th>Call Type</th>                                   
                                    <th>CRM Initiated ?</th>                                    
                                    <th>User Name</th>
                                    <th>Campaign</th>
                                    <th>Customer Name</th>
                                    <th>Phone No</th>
                                    <th>Veh No</th>
                                    <th>Chasssis No</th>
                                    <th>Disposition</th>
                                    <th>Media</th>
                                 </tr>
                              </thead>

                                <tbody>
                                   
                   				</tbody>
           </table>
        </div>
        </div>
    
       </div>
       </div>
    </div>
  </div>
</div>
""")))}),format.raw/*149.2*/("""


"""),format.raw/*152.1*/("""<script type="text/javascript">

 $(document).ready(function() """),format.raw/*154.31*/("""{"""),format.raw/*154.32*/("""
    """),format.raw/*155.5*/("""var table = $('#recordingTable').dataTable( """),format.raw/*155.49*/("""{"""),format.raw/*155.50*/("""
    	"""),format.raw/*156.6*/(""""lengthMenu": [ 10, 25, 50, 75, 100 ],
        "bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
        "paging": true,
        "searching":true,
        "ordering":false,
        "ajax": "/CREManager/recordingDataList"
        
    """),format.raw/*166.5*/("""}"""),format.raw/*166.6*/(""" """),format.raw/*166.7*/(""");

    var values=[];

  
	
   $('.filter').on('change', function() """),format.raw/*172.41*/("""{"""),format.raw/*172.42*/("""
	   
	   """),format.raw/*174.5*/("""var i= $(this).data('columnIndex');
	   var v = $(this).val();
	   if(v.length>0)"""),format.raw/*176.19*/("""{"""),format.raw/*176.20*/("""

		   """),format.raw/*178.6*/("""if(i == 6)"""),format.raw/*178.16*/("""{"""),format.raw/*178.17*/("""
		   """),format.raw/*179.6*/("""var UserIds="",j;
	        myOption = document.getElementById('ddlCreIds');
	        console.log("here"+myOption);		        
	    for (j=0;j<myOption.options.length;j++)"""),format.raw/*182.45*/("""{"""),format.raw/*182.46*/("""
	        """),format.raw/*183.10*/("""if(myOption.options[j].selected)"""),format.raw/*183.42*/("""{"""),format.raw/*183.43*/("""
	            """),format.raw/*184.14*/("""if(myOption.options[j].value != "Select")"""),format.raw/*184.55*/("""{"""),format.raw/*184.56*/("""
	        		"""),format.raw/*185.12*/("""UserIds = UserIds + myOption.options[j].value + ",";
	            """),format.raw/*186.14*/("""}"""),format.raw/*186.15*/("""
	        """),format.raw/*187.10*/("""}"""),format.raw/*187.11*/("""
	    """),format.raw/*188.6*/("""}"""),format.raw/*188.7*/("""
	    	"""),format.raw/*189.7*/("""if(UserIds.length > 0)"""),format.raw/*189.29*/("""{"""),format.raw/*189.30*/("""
	        	"""),format.raw/*190.11*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	        	values[6]=UserIds;
	    	"""),format.raw/*192.7*/("""}"""),format.raw/*192.8*/("""else"""),format.raw/*192.12*/("""{"""),format.raw/*192.13*/("""
	    		"""),format.raw/*193.8*/("""UserIds="Select";
	    		values[6]=UserIds;
	    	"""),format.raw/*195.7*/("""}"""),format.raw/*195.8*/("""
		   """),format.raw/*196.6*/("""}"""),format.raw/*196.7*/("""else"""),format.raw/*196.11*/("""{"""),format.raw/*196.12*/("""
			   """),format.raw/*197.7*/("""values[i] = v;
			   """),format.raw/*198.7*/("""}"""),format.raw/*198.8*/("""
	    	
		   
		 """),format.raw/*201.4*/("""// if(i < 8)
		  
		/*  else if(i == 9)"""),format.raw/*203.22*/("""{"""),format.raw/*203.23*/("""			  
			  """),format.raw/*204.6*/("""values[9] = v;
			  values[4] = values[4]+","+values[9];"""),format.raw/*205.42*/("""}"""),format.raw/*205.43*/("""*/
		   
	   """),format.raw/*207.5*/("""}"""),format.raw/*207.6*/("""
	   
	   	"""),format.raw/*209.6*/("""console.log(JSON.stringify(values));
		console.log(i);
	   	
     	  //clear global search values
    	  //table.api().search('');
    	  $(this).data('columnIndex');
    	  
    	  
   """),format.raw/*217.4*/("""}"""),format.raw/*217.5*/(""");


   $('#searchCust').on('click',function()"""),format.raw/*220.42*/("""{"""),format.raw/*220.43*/("""

	  """),format.raw/*222.4*/("""// alert(values.length);
	   if(values.length>0) """),format.raw/*223.25*/("""{"""),format.raw/*223.26*/("""
			"""),format.raw/*224.4*/("""for(var i=0;i<values.length;i++)"""),format.raw/*224.36*/("""{"""),format.raw/*224.37*/("""
				
			   """),format.raw/*226.7*/("""table.api().columns(i).search(values[i]);
			"""),format.raw/*227.4*/("""}"""),format.raw/*227.5*/("""
			"""),format.raw/*228.4*/("""table.api().draw();
	   """),format.raw/*229.5*/("""}"""),format.raw/*229.6*/("""
	"""),format.raw/*230.2*/("""}"""),format.raw/*230.3*/(""");
	"""),format.raw/*231.2*/("""}"""),format.raw/*231.3*/(""");

	</script>"""))
      }
    }
  }

  def render(user:String,dealer:String,dispoList:List[CallDispositionData],campaignList:List[Campaign],crelist:List[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(user,dealer,dispoList,campaignList,crelist)

  def f:((String,String,List[CallDispositionData],List[Campaign],List[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (user,dealer,dispoList,campaignList,crelist) => apply(user,dealer,dispoList,campaignList,crelist)

  def ref: this.type = this

}


}

/**/
object callRecordingPage extends callRecordingPage_Scope0.callRecordingPage
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:42 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/recording/callRecordingPage.scala.html
                  HASH: f13e5dfb2e1764f6c1efd58d4abcdfc01b654ca7
                  MATRIX: 841->1|1052->116|1080->119|1134->165|1173->167|1210->178|2873->1814|2921->1846|2960->1847|3024->1883|3067->1899|3089->1912|3128->1930|3158->1933|3180->1946|3220->1964|3294->2007|3380->2065|4331->2989|4370->3012|4409->3013|4441->3018|4484->3034|4498->3039|4536->3056|4566->3059|4580->3064|4618->3081|4662->3095|4708->3113|5037->3415|5078->3440|5117->3441|5149->3446|5192->3462|5210->3471|5239->3479|5269->3482|5287->3491|5322->3505|5366->3519|5452->3577|7301->5395|7335->5401|7429->5466|7459->5467|7493->5473|7566->5517|7596->5518|7631->5525|7951->5817|7980->5818|8009->5819|8113->5894|8143->5895|8183->5907|8295->5990|8325->5991|8362->6000|8401->6010|8431->6011|8466->6018|8667->6190|8697->6191|8737->6202|8798->6234|8828->6235|8872->6250|8942->6291|8972->6292|9014->6305|9110->6372|9140->6373|9180->6384|9210->6385|9245->6392|9274->6393|9310->6401|9361->6423|9391->6424|9432->6436|9549->6525|9578->6526|9611->6530|9641->6531|9678->6540|9758->6592|9787->6593|9822->6600|9851->6601|9884->6605|9914->6606|9950->6614|10000->6636|10029->6637|10077->6657|10147->6698|10177->6699|10217->6711|10303->6768|10333->6769|10376->6784|10405->6785|10446->6798|10668->6992|10697->6993|10775->7042|10805->7043|10840->7050|10919->7100|10949->7101|10982->7106|11043->7138|11073->7139|11115->7153|11189->7199|11218->7200|11251->7205|11304->7230|11333->7231|11364->7234|11393->7235|11426->7240|11455->7241
                  LINES: 27->1|32->1|33->2|33->2|33->2|36->5|77->46|77->46|77->46|78->47|78->47|78->47|78->47|78->47|78->47|78->47|79->48|82->51|111->80|111->80|111->80|112->81|112->81|112->81|112->81|112->81|112->81|112->81|113->82|114->83|122->91|122->91|122->91|123->92|123->92|123->92|123->92|123->92|123->92|123->92|124->93|127->96|180->149|183->152|185->154|185->154|186->155|186->155|186->155|187->156|197->166|197->166|197->166|203->172|203->172|205->174|207->176|207->176|209->178|209->178|209->178|210->179|213->182|213->182|214->183|214->183|214->183|215->184|215->184|215->184|216->185|217->186|217->186|218->187|218->187|219->188|219->188|220->189|220->189|220->189|221->190|223->192|223->192|223->192|223->192|224->193|226->195|226->195|227->196|227->196|227->196|227->196|228->197|229->198|229->198|232->201|234->203|234->203|235->204|236->205|236->205|238->207|238->207|240->209|248->217|248->217|251->220|251->220|253->222|254->223|254->223|255->224|255->224|255->224|257->226|258->227|258->227|259->228|260->229|260->229|261->230|261->230|262->231|262->231
                  -- GENERATED --
              */
          