
package views.html.recording

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object otherCallRecordingPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class otherCallRecordingPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[String,String,List[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(user:String,dealer:String,crelist :List[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.52*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealer)/*2.48*/ {_display_(Seq[Any](format.raw/*2.50*/("""
	

    """),format.raw/*5.5*/("""<div class="panel panel-primary">
      <div class="panel-heading"><b>Other Call Recording</b></div>
      <div class="panel-body">
      <div class="row">
      	<div class="col-md-12">
      		 <div class="panel panel-default">
				<div class="panel-body">
				<div class="row">
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Call Date From</label>
      					<input type="text" class="filter form-control datepickerFilter" data-column-index="0" placeholder="Call Date From" readonly/>	
      				</div>
      			</div>
      		 <div class="col-md-2">
      				<div class="form-group">
      				<label>Call Date To</label>
      					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" placeholder="Call Date To" readonly/>	
      				</div>
      			</div>
      		
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Call Type</label>
      						
      						<select class="filter form-control" data-column-index="2">      						
      						<option value="0" >--Select--</option>
      						<option value="OUTGOING" >OUTGOING</option>
      						<option value="INCOMING" >INCOMING</option>
      						<option value="MISSED" >MISSED</option>
                
                    
                </select>
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>User Name</label>
      					
      					<select class="filter form-control selectpicker" data-column-index="6" id="ddlCreIds" name="ddlCreIds" multiple> 
      					"""),_display_(/*45.13*/for(crelistIs <- crelist) yield /*45.38*/{_display_(Seq[Any](format.raw/*45.39*/("""
			"""),format.raw/*46.4*/("""<option value=""""),_display_(/*46.20*/crelistIs/*46.29*/.getId()),format.raw/*46.37*/("""">"""),_display_(/*46.40*/crelistIs/*46.49*/.getUserName()),format.raw/*46.63*/("""</option>
		""")))}),format.raw/*47.4*/("""
                
                    
                """),format.raw/*50.17*/("""</select>	
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>&nbsp;</label><br />
      					 <button type="submit"  id="searchCust" class="btn btn-primary btn-block">
                    			<i class="fa fa-search"></i>
                    			<span>Search</span>
        				</button>        				 
 	
      				</div>
      			</div>
      			
      			</div>
      	
      </div>
      </div>
      <div class="panel panel-default">
	  <div class="panel-body">
      <div class="dataTable_wrapper">
      <table class="table table-striped table-bordered table-hover" id="recordingTableOther">
      
       
                 <thead>
                                 <tr>
                                    <th>Call Date</th>
                                    <th>Call Time</th>
                                    <th>Call Type</th> 
                                    <th>User Name</th>
                                    <th>Customer Name</th>
                                    <th>Phone No</th>
                                    <th>Veh No</th>
                                    <th>Chasssis No</th>                                   
                                    <th>Media</th>
                                 </tr>
                              </thead>

                                <tbody>
                                   
                   				</tbody>
           </table>
        </div>
        </div>
    
       </div>
       </div>
    </div>
  </div>
</div>
""")))}),format.raw/*100.2*/("""


"""),format.raw/*103.1*/("""<script type="text/javascript">

 $(document).ready(function() """),format.raw/*105.31*/("""{"""),format.raw/*105.32*/("""
    """),format.raw/*106.5*/("""var table = $('#recordingTableOther').dataTable( """),format.raw/*106.54*/("""{"""),format.raw/*106.55*/("""
    	"""),format.raw/*107.6*/(""""lengthMenu": [ 10, 25, 50, 75, 100 ],
        "bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
        "paging": true,
        "searching":true,
        "ordering":false,
        "ajax": "/CREManager/otherRecordingDataList"
        
    """),format.raw/*117.5*/("""}"""),format.raw/*117.6*/(""" """),format.raw/*117.7*/(""");

    var values=[]; 
	
   $('.filter').on('change', function() """),format.raw/*121.41*/("""{"""),format.raw/*121.42*/("""
	   
	   """),format.raw/*123.5*/("""var i= $(this).data('columnIndex');
	   var v = $(this).val();
	   if(v.length>0)"""),format.raw/*125.19*/("""{"""),format.raw/*125.20*/("""

		   """),format.raw/*127.6*/("""if(i == 6)"""),format.raw/*127.16*/("""{"""),format.raw/*127.17*/("""
		   """),format.raw/*128.6*/("""var UserIds="",j;
	        myOption = document.getElementById('ddlCreIds');
	        console.log("here"+myOption);		        
	    for (j=0;j<myOption.options.length;j++)"""),format.raw/*131.45*/("""{"""),format.raw/*131.46*/("""
	        """),format.raw/*132.10*/("""if(myOption.options[j].selected)"""),format.raw/*132.42*/("""{"""),format.raw/*132.43*/("""
	            """),format.raw/*133.14*/("""if(myOption.options[j].value != "Select")"""),format.raw/*133.55*/("""{"""),format.raw/*133.56*/("""
	        		"""),format.raw/*134.12*/("""UserIds = UserIds + myOption.options[j].value + ",";
	            """),format.raw/*135.14*/("""}"""),format.raw/*135.15*/("""
	        """),format.raw/*136.10*/("""}"""),format.raw/*136.11*/("""
	    """),format.raw/*137.6*/("""}"""),format.raw/*137.7*/("""
	    	"""),format.raw/*138.7*/("""if(UserIds.length > 0)"""),format.raw/*138.29*/("""{"""),format.raw/*138.30*/("""
	        	"""),format.raw/*139.11*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	        	values[6]=UserIds;
	    	"""),format.raw/*141.7*/("""}"""),format.raw/*141.8*/("""else"""),format.raw/*141.12*/("""{"""),format.raw/*141.13*/("""
	    		"""),format.raw/*142.8*/("""UserIds="Select";
	    		values[6]=UserIds;
	    	"""),format.raw/*144.7*/("""}"""),format.raw/*144.8*/("""
		   """),format.raw/*145.6*/("""}"""),format.raw/*145.7*/("""else"""),format.raw/*145.11*/("""{"""),format.raw/*145.12*/("""
			   """),format.raw/*146.7*/("""values[i] = v;
			   """),format.raw/*147.7*/("""}"""),format.raw/*147.8*/("""
	    	
		 		   
	   """),format.raw/*150.5*/("""}"""),format.raw/*150.6*/("""
	   
	   	"""),format.raw/*152.6*/("""console.log(JSON.stringify(values));
		console.log(i);
	   	
    	  $(this).data('columnIndex');
    	  
    	  
   """),format.raw/*158.4*/("""}"""),format.raw/*158.5*/(""");


   $('#searchCust').on('click',function()"""),format.raw/*161.42*/("""{"""),format.raw/*161.43*/("""

	  """),format.raw/*163.4*/("""// alert(values.length);
	   if(values.length>0) """),format.raw/*164.25*/("""{"""),format.raw/*164.26*/("""
			"""),format.raw/*165.4*/("""for(var i=0;i<values.length;i++)"""),format.raw/*165.36*/("""{"""),format.raw/*165.37*/("""
				
			   """),format.raw/*167.7*/("""table.api().columns(i).search(values[i]);
			"""),format.raw/*168.4*/("""}"""),format.raw/*168.5*/("""
			"""),format.raw/*169.4*/("""table.api().draw();
	   """),format.raw/*170.5*/("""}"""),format.raw/*170.6*/("""
	"""),format.raw/*171.2*/("""}"""),format.raw/*171.3*/(""");
	"""),format.raw/*172.2*/("""}"""),format.raw/*172.3*/(""");

	</script>"""))
      }
    }
  }

  def render(user:String,dealer:String,crelist:List[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(user,dealer,crelist)

  def f:((String,String,List[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (user,dealer,crelist) => apply(user,dealer,crelist)

  def ref: this.type = this

}


}

/**/
object otherCallRecordingPage extends otherCallRecordingPage_Scope0.otherCallRecordingPage
              /*
                  -- GENERATED --
                  DATE: Sat Dec 16 16:03:46 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/recording/otherCallRecordingPage.scala.html
                  HASH: 5775383a7dc2a58209ba0e3d211a3eb9bf0ef9a6
                  MATRIX: 810->1|955->51|983->54|1037->100|1076->102|1113->113|2783->1756|2824->1781|2863->1782|2895->1787|2938->1803|2956->1812|2985->1820|3015->1823|3033->1832|3068->1846|3112->1860|3198->1918|4845->3534|4879->3540|4973->3605|5003->3606|5037->3612|5115->3661|5145->3662|5180->3669|5505->3966|5534->3967|5563->3968|5662->4038|5692->4039|5732->4051|5844->4134|5874->4135|5911->4144|5950->4154|5980->4155|6015->4162|6216->4334|6246->4335|6286->4346|6347->4378|6377->4379|6421->4394|6491->4435|6521->4436|6563->4449|6659->4516|6689->4517|6729->4528|6759->4529|6794->4536|6823->4537|6859->4545|6910->4567|6940->4568|6981->4580|7098->4669|7127->4670|7160->4674|7190->4675|7227->4684|7307->4736|7336->4737|7371->4744|7400->4745|7433->4749|7463->4750|7499->4758|7549->4780|7578->4781|7630->4805|7659->4806|7700->4819|7850->4941|7879->4942|7957->4991|7987->4992|8022->4999|8101->5049|8131->5050|8164->5055|8225->5087|8255->5088|8297->5102|8371->5148|8400->5149|8433->5154|8486->5179|8515->5180|8546->5183|8575->5184|8608->5189|8637->5190
                  LINES: 27->1|32->1|33->2|33->2|33->2|36->5|76->45|76->45|76->45|77->46|77->46|77->46|77->46|77->46|77->46|77->46|78->47|81->50|131->100|134->103|136->105|136->105|137->106|137->106|137->106|138->107|148->117|148->117|148->117|152->121|152->121|154->123|156->125|156->125|158->127|158->127|158->127|159->128|162->131|162->131|163->132|163->132|163->132|164->133|164->133|164->133|165->134|166->135|166->135|167->136|167->136|168->137|168->137|169->138|169->138|169->138|170->139|172->141|172->141|172->141|172->141|173->142|175->144|175->144|176->145|176->145|176->145|176->145|177->146|178->147|178->147|181->150|181->150|183->152|189->158|189->158|192->161|192->161|194->163|195->164|195->164|196->165|196->165|196->165|198->167|199->168|199->168|200->169|201->170|201->170|202->171|202->171|203->172|203->172
                  -- GENERATED --
              */
          