
package views.html.recording

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object callRecordingPageInsurance_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class callRecordingPageInsurance extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,String,List[CallDispositionData],List[Campaign],List[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(user:String,dealer:String,dispoList:List[CallDispositionData],campaignList :List[Campaign],crelist :List[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.117*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealer)/*2.48*/ {_display_(Seq[Any](format.raw/*2.50*/("""
	

    """),format.raw/*5.5*/("""<div class="panel panel-primary">
      <div class="panel-heading"><b>Insurance Call Recording</b></div>
      <div class="panel-body">

      <div class="row">
      	<div class="col-md-12">
      		  	  <div class="panel panel-default">
	  <div class="panel-body">
	  <div class="row">
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Call Date From</label>
      					<input type="text" class="filter form-control datepickerFilter" data-column-index="0" placeholder="Call Date From" readonly/>	
      				</div>
      			</div>
      		 <div class="col-md-2">
      				<div class="form-group">
      				<label>Call Date To</label>
      					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" placeholder="Call Date To" readonly/>	
      				</div>
      			</div>
      		
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Call Type</label>
      						
      						<select class="filter form-control" data-column-index="2">      						
      						<option value="0" >--Select--</option>
      						<option value="OUTGOING" >OUTGOING</option>
      						<option value="INCOMING" >INCOMING</option>
      						<option value="MISSED" >MISSED</option>
                
                    
                </select>
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Campaign</label>
      						
      						<select class="filter form-control" data-column-index="3">
      						<option value="0" >--Select--</option>
      						"""),_display_(/*47.14*/for(campaign_List<-campaignList) yield /*47.46*/{_display_(Seq[Any](format.raw/*47.47*/("""
		                                """),format.raw/*48.35*/("""<option value=""""),_display_(/*48.51*/campaign_List/*48.64*/.getCampaignName()),format.raw/*48.82*/("""">"""),_display_(/*48.85*/campaign_List/*48.98*/.getCampaignName()),format.raw/*48.116*/("""</option>
		                             """)))}),format.raw/*49.33*/("""
                
                    
                """),format.raw/*52.17*/("""</select>
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>CRM Initiated ?</label>
      						
      						<select class="filter form-control" data-column-index="4">
      						<option value="0" >--Select--</option>
      						<option value="Initiated" >YES</option>
      						<option value="NotDailed" >NO</option>
      						<option value="Not Disposed">Not Disposed</option>
                
                    
                </select>
      				</div>
      			</div>
      			
      			
      			
      			
      			
      	</div>
  
   
      <div class="row">
      	
      			<div class="col-md-2">
      				<div clas="form-group">
      				<label>Disposition</label>
      					
      					<select class="filter form-control" data-column-index="5"> 
      					<option value="0" >--Select--</option>               
                    """),_display_(/*85.22*/for(dispo <- dispoList) yield /*85.45*/{_display_(Seq[Any](format.raw/*85.46*/("""
			"""),format.raw/*86.4*/("""<option value=""""),_display_(/*86.20*/dispo/*86.25*/.getDisposition()),format.raw/*86.42*/("""">"""),_display_(/*86.45*/dispo/*86.50*/.getDisposition()),format.raw/*86.67*/("""</option>
		""")))}),format.raw/*87.4*/("""
                """),format.raw/*88.17*/("""</select>
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div clas="form-group">
      				<label>User Name</label>
      					
      					<select class="filter form-control selectpicker" data-column-index="6" id="ddlCreIds" name="ddlCreIds" multiple> 
      					"""),_display_(/*96.13*/for(crelistIs <- crelist) yield /*96.38*/{_display_(Seq[Any](format.raw/*96.39*/("""
			"""),format.raw/*97.4*/("""<option value=""""),_display_(/*97.20*/crelistIs/*97.29*/.getId()),format.raw/*97.37*/("""">"""),_display_(/*97.40*/crelistIs/*97.49*/.getUserName()),format.raw/*97.63*/("""</option>
		""")))}),format.raw/*98.4*/("""
                
                    
                """),format.raw/*101.17*/("""</select>	
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div clas="form-group">
      				<label>&nbsp;</label><br />
      					 <button type="submit"  id="searchCust" class="btn btn-primary btn-block">
                    			<i class="fa fa-search"></i>
                    			<span>Search</span>
        				</button>        				 
 	
      				</div>
      			</div>
      			 
      	</div>
      </div>
      </div>
    
      
           <div class="panel panel-default">
	  <div class="panel-body">
      <div class="dataTable_wrapper">
      <table class="table table-striped table-bordered table-hover" id="recordingTable">
      
       
                 <thead>
                                 <tr>
                                    <th>Call Date</th>
                                    <th>Call Time</th>
                                    <th>Call Type</th>                                   
                                    <th>CRM Initiated ?</th>                                    
                                    <th>User Name</th>
                                    <th>Campaign</th>
                                    <th>Customer Name</th>
                                    <th>Phone No</th>
                                    <th>Veh No</th>
                                    <th>Chasssis No</th>
                                    <th>Disposition</th>
                                    <th>Media</th>
                                 </tr>
                              </thead>

                                <tbody>
                                   
                   				</tbody>
           </table>
        </div>
    
       </div>
    </div>
  </div>
</div>
</div>
</div>
""")))}),format.raw/*155.2*/("""


"""),format.raw/*158.1*/("""<script type="text/javascript">

 $(document).ready(function() """),format.raw/*160.31*/("""{"""),format.raw/*160.32*/("""
    """),format.raw/*161.5*/("""var table = $('#recordingTable').dataTable( """),format.raw/*161.49*/("""{"""),format.raw/*161.50*/("""
    	"""),format.raw/*162.6*/(""""lengthMenu": [ 10, 25, 50, 75, 100 ],
        "bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
        "paging": true,
        "searching":true,
        "ordering":false,
        "ajax": "/CREManager/recordingDataListInsurance"
        
    """),format.raw/*172.5*/("""}"""),format.raw/*172.6*/(""" """),format.raw/*172.7*/(""");

    var values=[];

  
	
   $('.filter').on('change', function() """),format.raw/*178.41*/("""{"""),format.raw/*178.42*/("""
	   
	   """),format.raw/*180.5*/("""var i= $(this).data('columnIndex');
	   var v = $(this).val();
	   if(v.length>0)"""),format.raw/*182.19*/("""{"""),format.raw/*182.20*/("""

		   """),format.raw/*184.6*/("""if(i == 6)"""),format.raw/*184.16*/("""{"""),format.raw/*184.17*/("""
		   """),format.raw/*185.6*/("""var UserIds="",j;
	        myOption = document.getElementById('ddlCreIds');
	        console.log("here"+myOption);		        
	    for (j=0;j<myOption.options.length;j++)"""),format.raw/*188.45*/("""{"""),format.raw/*188.46*/("""
	        """),format.raw/*189.10*/("""if(myOption.options[j].selected)"""),format.raw/*189.42*/("""{"""),format.raw/*189.43*/("""
	            """),format.raw/*190.14*/("""if(myOption.options[j].value != "Select")"""),format.raw/*190.55*/("""{"""),format.raw/*190.56*/("""
	        		"""),format.raw/*191.12*/("""UserIds = UserIds + myOption.options[j].value + ",";
	            """),format.raw/*192.14*/("""}"""),format.raw/*192.15*/("""
	        """),format.raw/*193.10*/("""}"""),format.raw/*193.11*/("""
	    """),format.raw/*194.6*/("""}"""),format.raw/*194.7*/("""
	    	"""),format.raw/*195.7*/("""if(UserIds.length > 0)"""),format.raw/*195.29*/("""{"""),format.raw/*195.30*/("""
	        	"""),format.raw/*196.11*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	        	values[6]=UserIds;
	    	"""),format.raw/*198.7*/("""}"""),format.raw/*198.8*/("""else"""),format.raw/*198.12*/("""{"""),format.raw/*198.13*/("""
	    		"""),format.raw/*199.8*/("""UserIds="Select";
	    		values[6]=UserIds;
	    	"""),format.raw/*201.7*/("""}"""),format.raw/*201.8*/("""
		   """),format.raw/*202.6*/("""}"""),format.raw/*202.7*/("""else"""),format.raw/*202.11*/("""{"""),format.raw/*202.12*/("""
			   """),format.raw/*203.7*/("""values[i] = v;
			   """),format.raw/*204.7*/("""}"""),format.raw/*204.8*/("""
	    	
		   
		 """),format.raw/*207.4*/("""// if(i < 8)
		  
		/*  else if(i == 9)"""),format.raw/*209.22*/("""{"""),format.raw/*209.23*/("""			  
			  """),format.raw/*210.6*/("""values[9] = v;
			  values[4] = values[4]+","+values[9];"""),format.raw/*211.42*/("""}"""),format.raw/*211.43*/("""*/
		   
	   """),format.raw/*213.5*/("""}"""),format.raw/*213.6*/("""
	   
	   	"""),format.raw/*215.6*/("""console.log(JSON.stringify(values));
		console.log(i);
	   	
     	  //clear global search values
    	  //table.api().search('');
    	  $(this).data('columnIndex');
    	  
    	  
   """),format.raw/*223.4*/("""}"""),format.raw/*223.5*/(""");


   $('#searchCust').on('click',function()"""),format.raw/*226.42*/("""{"""),format.raw/*226.43*/("""

	   """),format.raw/*228.5*/("""//alert(values.length);
	   if(values.length>0) """),format.raw/*229.25*/("""{"""),format.raw/*229.26*/("""
			"""),format.raw/*230.4*/("""for(var i=0;i<values.length;i++)"""),format.raw/*230.36*/("""{"""),format.raw/*230.37*/("""
				
			   """),format.raw/*232.7*/("""table.api().columns(i).search(values[i]);
			"""),format.raw/*233.4*/("""}"""),format.raw/*233.5*/("""
			"""),format.raw/*234.4*/("""table.api().draw();
	   """),format.raw/*235.5*/("""}"""),format.raw/*235.6*/("""
	"""),format.raw/*236.2*/("""}"""),format.raw/*236.3*/(""");
	"""),format.raw/*237.2*/("""}"""),format.raw/*237.3*/(""");

	</script>"""))
      }
    }
  }

  def render(user:String,dealer:String,dispoList:List[CallDispositionData],campaignList:List[Campaign],crelist:List[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(user,dealer,dispoList,campaignList,crelist)

  def f:((String,String,List[CallDispositionData],List[Campaign],List[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (user,dealer,dispoList,campaignList,crelist) => apply(user,dealer,dispoList,campaignList,crelist)

  def ref: this.type = this

}


}

/**/
object callRecordingPageInsurance extends callRecordingPageInsurance_Scope0.callRecordingPageInsurance
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:42 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/recording/callRecordingPageInsurance.scala.html
                  HASH: 9173653ca425ea4c1f3d4a24edaa2024f948ccea
                  MATRIX: 859->1|1070->116|1098->119|1152->165|1191->167|1228->178|2905->1828|2953->1860|2992->1861|3056->1897|3099->1913|3121->1926|3160->1944|3190->1947|3212->1960|3252->1978|3326->2021|3412->2079|4396->3036|4435->3059|4474->3060|4506->3065|4549->3081|4563->3086|4601->3103|4631->3106|4645->3111|4683->3128|4727->3142|4773->3160|5101->3461|5142->3486|5181->3487|5213->3492|5256->3508|5274->3517|5303->3525|5333->3528|5351->3537|5386->3551|5430->3565|5517->3623|7362->5437|7396->5443|7490->5508|7520->5509|7554->5515|7627->5559|7657->5560|7692->5567|8021->5868|8050->5869|8079->5870|8183->5945|8213->5946|8253->5958|8365->6041|8395->6042|8432->6051|8471->6061|8501->6062|8536->6069|8737->6241|8767->6242|8807->6253|8868->6285|8898->6286|8942->6301|9012->6342|9042->6343|9084->6356|9180->6423|9210->6424|9250->6435|9280->6436|9315->6443|9344->6444|9380->6452|9431->6474|9461->6475|9502->6487|9619->6576|9648->6577|9681->6581|9711->6582|9748->6591|9828->6643|9857->6644|9892->6651|9921->6652|9954->6656|9984->6657|10020->6665|10070->6687|10099->6688|10147->6708|10217->6749|10247->6750|10287->6762|10373->6819|10403->6820|10446->6835|10475->6836|10516->6849|10738->7043|10767->7044|10845->7093|10875->7094|10911->7102|10989->7151|11019->7152|11052->7157|11113->7189|11143->7190|11185->7204|11259->7250|11288->7251|11321->7256|11374->7281|11403->7282|11434->7285|11463->7286|11496->7291|11525->7292
                  LINES: 27->1|32->1|33->2|33->2|33->2|36->5|78->47|78->47|78->47|79->48|79->48|79->48|79->48|79->48|79->48|79->48|80->49|83->52|116->85|116->85|116->85|117->86|117->86|117->86|117->86|117->86|117->86|117->86|118->87|119->88|127->96|127->96|127->96|128->97|128->97|128->97|128->97|128->97|128->97|128->97|129->98|132->101|186->155|189->158|191->160|191->160|192->161|192->161|192->161|193->162|203->172|203->172|203->172|209->178|209->178|211->180|213->182|213->182|215->184|215->184|215->184|216->185|219->188|219->188|220->189|220->189|220->189|221->190|221->190|221->190|222->191|223->192|223->192|224->193|224->193|225->194|225->194|226->195|226->195|226->195|227->196|229->198|229->198|229->198|229->198|230->199|232->201|232->201|233->202|233->202|233->202|233->202|234->203|235->204|235->204|238->207|240->209|240->209|241->210|242->211|242->211|244->213|244->213|246->215|254->223|254->223|257->226|257->226|259->228|260->229|260->229|261->230|261->230|261->230|263->232|264->233|264->233|265->234|266->235|266->235|267->236|267->236|268->237|268->237
                  -- GENERATED --
              */
          