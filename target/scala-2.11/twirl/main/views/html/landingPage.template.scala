
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object landingPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class landingPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""<!DOCTYPE html>

<html>
<head>
<title>AutoSherpaCRM</title>
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<meta http-equiv="cache-control" content="max-age=0" >
<meta http-equiv="expires" content="0" >
<meta http-equiv="pragma" content="no-cache" >
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="shortcut icon" type="image/png" href=""""),_display_(/*12.51*/routes/*12.57*/.Assets.at("images/favicon1.ico")),format.raw/*12.90*/("""">
<link rel="stylesheet" href=""""),_display_(/*13.31*/routes/*13.37*/.Assets.at("css/bootstrap.css")),format.raw/*13.68*/("""">
<link rel="stylesheet" href=""""),_display_(/*14.31*/routes/*14.37*/.Assets.at("css/bootstrap-responsive.css")),format.raw/*14.79*/("""">
<link rel="stylesheet" href=""""),_display_(/*15.31*/routes/*15.37*/.Assets.at("css/color1.css")),format.raw/*15.65*/("""">
<link rel="stylesheet" href=""""),_display_(/*16.31*/routes/*16.37*/.Assets.at("css/font-awesome.min.css")),format.raw/*16.75*/("""">
<link rel="stylesheet" href=""""),_display_(/*17.31*/routes/*17.37*/.Assets.at("css/style.css")),format.raw/*17.64*/("""">
<link rel="stylesheet" href=""""),_display_(/*18.31*/routes/*18.37*/.Assets.at("css/transitions.css")),format.raw/*18.70*/("""">
<style>
.header """),format.raw/*20.9*/("""{"""),format.raw/*20.10*/("""
    """),format.raw/*21.5*/("""padding: 15px 0;
    background: #fff;
    -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.4);
    box-shadow: 0 0 12px rgba(0, 10, 0, 0.4);
"""),format.raw/*25.1*/("""}"""),format.raw/*25.2*/("""
"""),format.raw/*26.1*/("""</style>
</head>
<body  >
<div class="navigation-bar">
  <div class="container">
    <div class="logo"> <a href="#"><img src=""""),_display_(/*31.47*/routes/*31.53*/.Assets.at("images/dealercustomerconnectlogoblack2.png")),format.raw/*31.109*/("""" alt="AutoSherpa" data-toggle="tooltip" title="AUTOSherpa"></a> </div>
  </div>
</div>
<div class="contant">
<section>
  <div class="container">
    <div class="sec-header">
      <h3>
        <p>Automotive Dealers CRM</p>
      </h3>
      <span></span> <span></span> <span></span> </div>
      <div class="container">
    <div class="row">
    <!-- <a href="/superAdmin">
      <div class="span2 offset1" data-toggle="tooltip" title="Super Admin">
        <div class="services">
          <div class="header"> <i class="fa fa-tablet"></i> <i class="fa fa-user inner-icon"></i> </div>
          <div class="text">
            <h3>Super Admin</h3>
            <p>Please click here to Login</p>
          </div>
        </div>
      </div>
      </a> -->
      <a href="/CREManager">
      <div class="span3" data-toggle="tooltip" title="CRE Manager">
        <div class="services">
          <div class="header"> <i class="fa fa-tablet"></i> <i class="fa fa-sitemap inner-icon"></i> </div>
          <div class="text">
            <h3>CRE Manager</h3>
            <p>Please click here to Login</p>
          </div>
        </div>
      </div>
      </a>
      <a href="/CRE">
      <div class="span3" data-toggle="tooltip" title="CRE">
        <div class="services">
          <div class="header"> <i class="fa fa-tablet"></i> <i class="fa fa-users inner-icon"></i> </div>
          <div class="text">
            <h3>Customer Relationship Executive(CRE)</h3>
            <!--<h3>CRE</h3>-->
            <p>Please click here to Login</p>
          </div>
        </div>
      </div>
      </a>
      <a href="/ServiceAdvisor">
      <div class="span3" data-toggle="tooltip" title="SA">
        <div class="services">
          <div class="header"> <i class="fa fa-tablet"></i> <i class="fa fa-users inner-icon"></i> </div>
          <div class="text">
            <h3>Service Advisor(SA)</h3>
            <!--<h3>CRE</h3>-->
            <p>Please click here to Login</p>
          </div>
        </div>
      </div>
      </a>
      <a href="/OthersLogins">
      <div class="span3" data-toggle="tooltip" title="SA">
        <div class="services">
          <div class="header"> <i class="fa fa-tablet"></i> <i class="fa fa-users inner-icon"></i> </div>
          <div class="text">
            <h3>OTHERS</h3>
            <!--<h3>CRE</h3>-->
            <p>Please click here to Login</p>
          </div>
        </div>
      </div>
      </a>
      
      
     </div>
     </div>
  </div>
</section>
   <div class="navigation-bar">
   <br>
       <footer class="main-footer" style="text-align:center">
            <strong>AutoSherpa &copy; 2016 <a href="http://www.autosherpas.com/">Company</a>.</strong> All rights reserved.
       </footer>
   </br>
</div>
</div>
</body>
</html>

"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object landingPage extends landingPage_Scope0.landingPage
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:30 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/landingPage.scala.html
                  HASH: f79940081da81bf2b66d8fe0194e571dd5297510
                  MATRIX: 839->2|1286->422|1301->428|1355->461|1416->495|1431->501|1483->532|1544->566|1559->572|1622->614|1683->648|1698->654|1747->682|1808->716|1823->722|1882->760|1943->794|1958->800|2006->827|2067->861|2082->867|2136->900|2184->921|2213->922|2246->928|2411->1066|2439->1067|2468->1069|2627->1201|2642->1207|2720->1263
                  LINES: 32->2|42->12|42->12|42->12|43->13|43->13|43->13|44->14|44->14|44->14|45->15|45->15|45->15|46->16|46->16|46->16|47->17|47->17|47->17|48->18|48->18|48->18|50->20|50->20|51->21|55->25|55->25|56->26|61->31|61->31|61->31
                  -- GENERATED --
              */
          