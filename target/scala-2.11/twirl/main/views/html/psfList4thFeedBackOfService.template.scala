
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object psfList4thFeedBackOfService_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class psfList4thFeedBackOfService extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealercode:String,dealerName:String,userName:String,oem:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.66*/("""

"""),_display_(/*3.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*3.65*/ {_display_(Seq[Any](format.raw/*3.67*/("""

"""),format.raw/*5.1*/("""<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading"> PSF List For 4th Day FeedBack</div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li id="psf6thData" class="active"><a href="#home" data-toggle="tab" value ="psf4thday" id="Tab1" onclick="assignedInteractionPSFData(result.name);" >4th Day PSF</a> </li>
                    <li><a href="#profile" data-toggle="tab"  id="Tab2" onclick="ajaxCallForFollowUpRequiredPSFData();">Follow Up Required</a> </li>
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxCallForCompletedSurveyPSFData();" >Completed Survey</a> </li>
                    <!-- <li><a href="#appointments" data-toggle="tab" id="Tab8" onclick="ajaxCallForAppointmentPSFData();">Appointments</a></li> -->
                    <li><a href="#dissatisfied" data-toggle="tab" id="Tab" onclick="ajaxCallForDissatisfiedPSFData();">Dissatisfied</a></li>
                     <li><a href="#incompleteSurvey" data-toggle="tab" id="Tab7" onclick="ajaxCallForIncompletedSurveyPSFData();">Incomplete Survey</a></li>                    
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxCallForNonContactsPSFData();">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxCallForDroppedCallsPSFData();">Dropped</a> </li>
                   
                </ul>
                <div class="tab-content">
                     <div class="row">
					<div class="col-md-3" id="SchedulefromBillDate" style="display:none">
					<label>From Bill Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" id="frombilldaterange" name="frombilldaterange" readonly>
				  </div>
				   <div class="col-md-3" id="ScheduletoBillDate" style="display:none">
					<label>To Bill Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="2" id="tobilldaterange" name="tobilldaterange" readonly>
				  </div>
				
				</div>
				    <div class="row" id="forschedule">
					<div class="col-md-3" id="fromCallDate" style="display:none">
					<label>From Call Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" id="fromCalldaterange22" name="frombilldaterange22" readonly>
				  </div>
				   <div class="col-md-3" id="toCallDate" style="display:none">
					<label>To Call Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="2" id="tobCalldaterange33" name="tobilldaterange33" readonly>
				  </div>
				</div>
                    <div class="tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="assignedInteractionPSFData">
                                    <thead>
                                         <tr>                                    
                                            <th>Customer Name</th> 
                                            <th>Mobile Number</th>                                   
                                            <th>Vehicle RegNo.</th>
                                            <th>Model</th>
                                            <th>RO Number</th>
                                            <th>RO Date</th>
                                            <th>Bill Date</th>
                                            <th>Category</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="tab-pane fade" id="profile">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForFollowUpRequiredPSFData">
                                    <thead>
                                        <tr>                                    
											<th>FollowUp Date</th>
											<th>FollowUp Time</th>
											<th>call_date</th>
											<th>Customer Name</th>
											<th>Mobile Number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>






                    <div class="tab-pane fade" id="incompleteSurvey">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForIncompletedSurveyPSFData">
                                    <thead>
                                        <tr>                                    
											<th>FollowUp Date</th>
											<th>FollowUp Time</th>
											<th>call_date</th>
											<th>Customer Name</th>
											<th>Mobile Number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="tab-pane fade" id="settings">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForCompletedSurveyPSFData">
                                    <thead>
                                       <tr>                                    
                                           	<th>Survey Date</th>																	
											<th>Customer Name</th>																	
											<th>Category</th>																	
											<th>vehicle RegNo</th>																	
											<th>Model</th>																	
											<th>RO Date</th>																	
											<th>RO Number</th>																	
											<th>Bill Date</th>
											<th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
                    
                    <div class="tab-pane fade" id="dissatisfied">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForDissatisfiedPSFData">
                                    <thead>
                                       <tr>                                    
                                           	<th>Survey Date</th>																	
											<th>Customer Name</th>																	
											<th>Category</th>																	
											<th>vehicle RegNo</th>																	
											<th>Model</th>																	
											<th>RO Date</th>																	
											<th>RO Number</th>																	
											<th>Bill Date</th>
											<th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="tab-pane fade" id="nonContacts">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForNonContactsPSFData">
                                    <thead>
                                        <tr>                                   
											<th>Customer Name</th>
											<th>Category</th>
											<th>Mobile number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
											<th>Last Disposition</th>
											<th>Call Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
                    <div class="tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForDroppedCallsPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>
											<th>Category</th>
											<th>Mobile number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
											<th>Last Disposition</th>
											<th>Call Date</th>
											
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
					
					 <div class="tab-pane fade" id="appointments">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForAppointmentPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>
											<th>Vehicle RegNo</th>
											<th>Appointment Date</th>
											<th>Mobile number</th>
											<th>Call Date</th>
											<th>Appointment Time</th>
											<th>RO Number</th>
											<th>RO Date</th>
											<th>Bill Date</th>
											<th>Survey date</th>
											<th>Model</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>

                    <!-- /.panel --> 
                </div>
                <!-- /.col-lg-12 --> 
            </div>
        </div>
    </div>
</div>

""")))}),format.raw/*281.2*/("""
"""),format.raw/*282.1*/("""<script type="text/javascript">
    window.onload = ajaxRequestPSFDataPageWithTab4thDay;
</script>
<script>
function FilterOptionData(table)"""),format.raw/*286.33*/("""{"""),format.raw/*286.34*/("""
	"""),format.raw/*287.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*288.39*/("""{"""),format.raw/*288.40*/("""
    	"""),format.raw/*289.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
		console.log(v);
		console.log(i);
	    if(v.length>0)"""),format.raw/*294.20*/("""{"""),format.raw/*294.21*/("""
		   """),format.raw/*295.6*/("""values[i] = v;   
	    """),format.raw/*296.6*/("""}"""),format.raw/*296.7*/("""
	    """),format.raw/*297.6*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');
	 	 if(values.length>0) """),format.raw/*301.25*/("""{"""),format.raw/*301.26*/("""
			 """),format.raw/*302.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*302.37*/("""{"""),format.raw/*302.38*/("""	
			    """),format.raw/*303.8*/("""table.api().columns(i).search(values[i]);
			 """),format.raw/*304.5*/("""}"""),format.raw/*304.6*/("""
			 """),format.raw/*305.5*/("""table.api().draw();
			 """),format.raw/*306.5*/("""}"""),format.raw/*306.6*/(""" 
	"""),format.raw/*307.2*/("""}"""),format.raw/*307.3*/(""");	
"""),format.raw/*308.1*/("""}"""),format.raw/*308.2*/("""
"""),format.raw/*309.1*/("""</script>
<script type="text/javascript">
var result = """),format.raw/*311.14*/("""{"""),format.raw/*311.15*/("""name:'psf4thday'"""),format.raw/*311.31*/("""}"""),format.raw/*311.32*/(""";
    function assignedInteractionPSFData(name) """),format.raw/*312.47*/("""{"""),format.raw/*312.48*/("""
        """),format.raw/*313.9*/("""// alert("server side data load");
        // alert(name);
        $("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
        document.getElementById("SchedulefromBillDate").style.display = "block";
    	document.getElementById("ScheduletoBillDate").style.display = "block";
    	document.getElementById("fromCallDate").style.display = "none";
    	document.getElementById("toCallDate").style.display = "none";

        var table =$('#assignedInteractionPSFData').dataTable("""),format.raw/*324.63*/("""{"""),format.raw/*324.64*/("""
            """),format.raw/*325.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax":"/assignedInteractionTablePSFData"+"/"+name+""        
        """),format.raw/*333.9*/("""}"""),format.raw/*333.10*/(""");
        FilterOptionData(table);
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*335.65*/("""{"""),format.raw/*335.66*/("""
            """),format.raw/*336.13*/("""$($.fn.dataTable.tables(true)).DataTable()
               .columns.adjust();
         """),format.raw/*338.10*/("""}"""),format.raw/*338.11*/(""");
    """),format.raw/*339.5*/("""}"""),format.raw/*339.6*/("""

   
    """),format.raw/*342.5*/("""function ajaxCallForFollowUpRequiredPSFData() """),format.raw/*342.51*/("""{"""),format.raw/*342.52*/("""
    	"""),format.raw/*343.6*/("""var psfDay = 'psf4thday'; 
    	$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
    	var psfDay = 'psf4thday';
     	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
        //alert("follow up server side data");
       var table = $('#ajaxCallForFollowUpRequiredPSFData').dataTable("""),format.raw/*354.71*/("""{"""),format.raw/*354.72*/("""
            """),format.raw/*355.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForFollowUpRequiredPSFData"+"/"+psfDay+""
        """),format.raw/*363.9*/("""}"""),format.raw/*363.10*/(""");
       FilterOptionData(table);
    """),format.raw/*365.5*/("""}"""),format.raw/*365.6*/("""



    """),format.raw/*369.5*/("""function ajaxCallForIncompletedSurveyPSFData() """),format.raw/*369.52*/("""{"""),format.raw/*369.53*/("""
    	"""),format.raw/*370.6*/("""var psfDay = 'psf4thday';
    	$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
    	var psfDay = 'psf4thday'; 
     	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
        //alert("follow up server side data");
        var table = $('#ajaxCallForIncompletedSurveyPSFData').dataTable("""),format.raw/*381.73*/("""{"""),format.raw/*381.74*/("""
            """),format.raw/*382.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForIncompletedSurveyPSFData"+"/"+psfDay+""
        """),format.raw/*390.9*/("""}"""),format.raw/*390.10*/(""");
        FilterOptionData(table);
    """),format.raw/*392.5*/("""}"""),format.raw/*392.6*/("""

    """),format.raw/*394.5*/("""function ajaxCallForCompletedSurveyPSFData() """),format.raw/*394.50*/("""{"""),format.raw/*394.51*/("""
    	 """),format.raw/*395.7*/("""var psfDay = 'psf4thday'; 
    	 $("#fromCalldaterange22").val('');
     	$("#tobCalldaterange33").val('');
     	$("#frombilldaterange").val('');
     	$("#tobilldaterange").val('');
     	var psfDay = 'psf4thday';
      	document.getElementById("SchedulefromBillDate").style.display = "none";
     	document.getElementById("ScheduletoBillDate").style.display = "none";
     	document.getElementById("fromCallDate").style.display = "block";
     	document.getElementById("toCallDate").style.display = "block";
       var table = $('#ajaxCallForCompletedSurveyPSFData').dataTable("""),format.raw/*405.70*/("""{"""),format.raw/*405.71*/("""
            """),format.raw/*406.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForCompletedSurveyPSFData"+"/"+psfDay+""
        """),format.raw/*414.9*/("""}"""),format.raw/*414.10*/(""");
        FilterOptionData(table);
    """),format.raw/*416.5*/("""}"""),format.raw/*416.6*/("""

    """),format.raw/*418.5*/("""function ajaxCallForDissatisfiedPSFData() """),format.raw/*418.47*/("""{"""),format.raw/*418.48*/("""
        """),format.raw/*419.9*/("""//alert("ajaxCallForDissatisfiedPSFData");
   	 var psfDay = 'psf4thday'; 
   	 $("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
    	var psfDay = 'psf4thday';
     	document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
      var table = $('#ajaxCallForDissatisfiedPSFData').dataTable("""),format.raw/*430.66*/("""{"""),format.raw/*430.67*/("""
           """),format.raw/*431.12*/(""""bDestroy": true,
           "processing": true,
           "serverSide": true,
           "scrollY": 300,
           "paging": true,
           "searching": true,
           "ordering": false,
           "ajax": "/ajaxCallForDissatisfiedPSFData"+"/"+psfDay+""
       """),format.raw/*439.8*/("""}"""),format.raw/*439.9*/(""");
       FilterOptionData(table);
   """),format.raw/*441.4*/("""}"""),format.raw/*441.5*/("""

    """),format.raw/*443.5*/("""function ajaxCallForNonContactsPSFData() """),format.raw/*443.46*/("""{"""),format.raw/*443.47*/("""
       """),format.raw/*444.8*/("""var psfDay = 'psf4thday'; 
       $("#fromCalldaterange22").val('');
   	$("#tobCalldaterange33").val('');
   	$("#frombilldaterange").val('');
   	$("#tobilldaterange").val('');
      var psfDay = 'psf4thday'; 
   	document.getElementById("SchedulefromBillDate").style.display = "none";
   	document.getElementById("ScheduletoBillDate").style.display = "none";
   	document.getElementById("fromCallDate").style.display = "block";
   	document.getElementById("toCallDate").style.display = "block";
       var table =  $('#ajaxCallForNonContactsPSFData').dataTable("""),format.raw/*454.67*/("""{"""),format.raw/*454.68*/("""
            """),format.raw/*455.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForNonContactsPSFData"+"/"+psfDay+""
        """),format.raw/*463.9*/("""}"""),format.raw/*463.10*/(""");
       FilterOptionData(table);
    """),format.raw/*465.5*/("""}"""),format.raw/*465.6*/("""


    """),format.raw/*468.5*/("""function ajaxCallForDroppedCallsPSFData() """),format.raw/*468.47*/("""{"""),format.raw/*468.48*/("""
      """),format.raw/*469.7*/("""var psfDay = 'psf4thday'; 
      $("#fromCalldaterange22").val('');
  	$("#tobCalldaterange33").val('');
  	$("#frombilldaterange").val('');
  	$("#tobilldaterange").val('');
    var psfDay = 'psf4thday'; 
 	document.getElementById("SchedulefromBillDate").style.display = "none";
	document.getElementById("ScheduletoBillDate").style.display = "none";
	document.getElementById("fromCallDate").style.display = "block";
	document.getElementById("toCallDate").style.display = "block";
       var table =  $('#ajaxCallForDroppedCallsPSFData').dataTable("""),format.raw/*479.68*/("""{"""),format.raw/*479.69*/("""
            """),format.raw/*480.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForDroppedCallsPSFData"+"/"+psfDay+""
        """),format.raw/*488.9*/("""}"""),format.raw/*488.10*/(""");
       FilterOptionData(table);
    """),format.raw/*490.5*/("""}"""),format.raw/*490.6*/("""

"""),format.raw/*492.1*/("""</script>
"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem)

  def f:((String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem) => apply(dealercode,dealerName,userName,oem)

  def ref: this.type = this

}


}

/**/
object psfList4thFeedBackOfService extends psfList4thFeedBackOfService_Scope0.psfList4thFeedBackOfService
              /*
                  -- GENERATED --
                  DATE: Fri Dec 22 16:46:54 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/psfList4thFeedBackOfService.scala.html
                  HASH: 03ffa5cf2cc9f19fac4f454b24120c8ad1c48e25
                  MATRIX: 810->1|969->65|999->70|1070->133|1109->135|1139->139|14276->13245|14306->13247|14479->13391|14509->13392|14540->13395|14625->13451|14655->13452|14690->13459|14894->13634|14924->13635|14959->13642|15011->13666|15040->13667|15075->13674|15274->13844|15304->13845|15338->13851|15399->13883|15429->13884|15467->13894|15542->13941|15571->13942|15605->13948|15658->13973|15687->13974|15719->13978|15748->13979|15781->13984|15810->13985|15840->13987|15926->14044|15956->14045|16001->14061|16031->14062|16109->14111|16139->14112|16177->14122|16788->14704|16818->14705|16861->14719|17179->15009|17209->15010|17340->15112|17370->15113|17413->15127|17530->15215|17560->15216|17596->15224|17625->15225|17666->15238|17741->15284|17771->15285|17806->15292|18465->15922|18495->15923|18538->15937|18854->16225|18884->16226|18953->16267|18982->16268|19022->16280|19098->16327|19128->16328|19163->16335|19824->16967|19854->16968|19897->16982|20214->17271|20244->17272|20314->17314|20343->17315|20379->17323|20453->17368|20483->17369|20519->17377|21138->17967|21168->17968|21211->17982|21526->18269|21556->18270|21626->18312|21655->18313|21691->18321|21762->18363|21792->18364|21830->18374|22485->19000|22515->19001|22557->19014|22861->19290|22890->19291|22958->19331|22987->19332|23023->19340|23093->19381|23123->19382|23160->19391|23763->19965|23793->19966|23836->19980|24147->20263|24177->20264|24246->20305|24275->20306|24313->20316|24384->20358|24414->20359|24450->20367|25037->20925|25067->20926|25110->20940|25422->21224|25452->21225|25521->21266|25550->21267|25582->21271
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|312->281|313->282|317->286|317->286|318->287|319->288|319->288|320->289|325->294|325->294|326->295|327->296|327->296|328->297|332->301|332->301|333->302|333->302|333->302|334->303|335->304|335->304|336->305|337->306|337->306|338->307|338->307|339->308|339->308|340->309|342->311|342->311|342->311|342->311|343->312|343->312|344->313|355->324|355->324|356->325|364->333|364->333|366->335|366->335|367->336|369->338|369->338|370->339|370->339|373->342|373->342|373->342|374->343|385->354|385->354|386->355|394->363|394->363|396->365|396->365|400->369|400->369|400->369|401->370|412->381|412->381|413->382|421->390|421->390|423->392|423->392|425->394|425->394|425->394|426->395|436->405|436->405|437->406|445->414|445->414|447->416|447->416|449->418|449->418|449->418|450->419|461->430|461->430|462->431|470->439|470->439|472->441|472->441|474->443|474->443|474->443|475->444|485->454|485->454|486->455|494->463|494->463|496->465|496->465|499->468|499->468|499->468|500->469|510->479|510->479|511->480|519->488|519->488|521->490|521->490|523->492
                  -- GENERATED --
              */
          