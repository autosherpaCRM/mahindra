
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object changeAssinedCalls_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class changeAssinedCalls extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[List[WyzUser],List[CallDispositionData],List[Campaign],String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(userlist:List[WyzUser],dispoList:List[CallDispositionData],camplist:List[Campaign],user:String,dealercode:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.116*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealercode)/*2.52*/{_display_(Seq[Any](format.raw/*2.53*/("""

"""),format.raw/*4.1*/("""<div class="panel panel-primary">
  <div class="panel-heading">Change Assignment</div>
  <div class="panel-body">
  <div class="row">
  <div class="col-md-2">
	<label>Agent Name : </label>
	<div class="form-group">
	<select class="multiSelectClass form-control" id="crename" multiple="multiple">
		
		"""),_display_(/*13.4*/for(user <- userlist) yield /*13.25*/{_display_(Seq[Any](format.raw/*13.26*/("""
			"""),format.raw/*14.4*/("""<option value=""""),_display_(/*14.20*/user/*14.24*/.getId()),format.raw/*14.32*/("""">"""),_display_(/*14.35*/user/*14.39*/.getUserName()),format.raw/*14.53*/("""</option>
			
		""")))}),format.raw/*16.4*/("""
	
	"""),format.raw/*18.2*/("""</select>
  </div>
  </div>
  <div class="col-md-2">
  <div class="form-group">
	<label>From Assign Date : </label>
	<input type="text" class="form-control AssFromDtClass datepicker" id="assignFromDate" readonly>
  </div>
  </div>
   <div class="col-md-2">
   <div class="form-group">
	<label>To Assign Date : </label>
	<input type="text" class="form-control AssToDtClass datepicker" id="assignToDate" readonly>
  </div>
  </div>
   <div class="col-md-2">
	<label>Disposition Status : </label>
	<div class="form-group">
	<select class="multiSelectClass form-control" id="all_dispositions" multiple="multiple">
	
		<option value="0">Not yet Called</option>
		"""),_display_(/*39.4*/for(dispo <- dispoList) yield /*39.27*/{_display_(Seq[Any](format.raw/*39.28*/("""
			"""),format.raw/*40.4*/("""<option value=""""),_display_(/*40.20*/dispo/*40.25*/.getId()),format.raw/*40.33*/("""">"""),_display_(/*40.36*/dispo/*40.41*/.getDisposition()),format.raw/*40.58*/("""</option>
			
		""")))}),format.raw/*42.4*/("""
	"""),format.raw/*43.2*/("""</select>
  </div>
  </div>
  <div class="col-md-2">
	<label>Campaign : </label>
	<div class="form-group">
	<select class="form-control CampaignClass" id="campaignSelected">
		<option value="0">--Select--</option>		
		"""),_display_(/*51.4*/for(camlist <- camplist) yield /*51.28*/{_display_(Seq[Any](format.raw/*51.29*/("""
			"""),format.raw/*52.4*/("""<option value=""""),_display_(/*52.20*/camlist/*52.27*/.getId()),format.raw/*52.35*/("""">"""),_display_(/*52.38*/camlist/*52.45*/.getCampaignName),format.raw/*52.61*/("""</option>
			
		""")))}),format.raw/*54.4*/("""
	"""),format.raw/*55.2*/("""</select>
  </div>
  </div>
  <div class="col-md-2"><br />
	<button type="button" class="btn btn-info" id="clearAllId">Clear All</button>
	<button type="button" class="btn btn-info" id="ViewAlltblBox" onclick="ajaxViewAssignDataList()">View</button>
	
  </div>
  
  </div>
  <br />
  
   <span type="label" id="viewCallsERR" style="color:red"></span>
 
  <div class="row" id="selectAllDIv" style="display:none">
  <div class="col-md-12">
  <table id="tbl" class="table table-bordered table-responsive">
<thead>
<tr>
<th style="width: 100px;"> <input type="checkbox" id ="chckHead"/> Select All</th>
<th>User Name</th>
<th>Customer Name</th>
<th>Mobile Number</th>
<th>Vehicle RegNo.</th>
<th>Disposition</th>
<th>Campaign</th>
<th style="display:none;"></th>

</tr>
</thead>
<tbody>

</tbody>
</table>
  </div>
  </div>
  
  <span type="label" id="assignCallsERR" style="color:red"></span>
  <div class="row">
	<div class="col-md-2">
	<label>Assign To</label>
	<div class="form-group">
	<select class="multiSelectTop form-control" id="userlist" multiple="multiple" name="userId[]" required>
		"""),_display_(/*98.4*/for(user <- userlist) yield /*98.25*/{_display_(Seq[Any](format.raw/*98.26*/("""
			"""),format.raw/*99.4*/("""<option value=""""),_display_(/*99.20*/user/*99.24*/.getId()),format.raw/*99.32*/("""">"""),_display_(/*99.35*/user/*99.39*/.getUserName()),format.raw/*99.53*/("""</option>
			
		""")))}),format.raw/*101.4*/("""
	"""),format.raw/*102.2*/("""</select>
  </div>
  </div>
  	<div class="col-md-2"><br />
		 <button type="button" class="btn btn-info" onclick="ajaxAssignDataList();" >Change Assignment</button>
	  </div>
 
  </div>
  
  </div>
</div>



""")))}),format.raw/*116.2*/("""

"""),format.raw/*118.1*/("""<script>
 $(document).ready(function() """),format.raw/*119.31*/("""{"""),format.raw/*119.32*/("""
 
       """),format.raw/*121.8*/("""$('.multiSelectClass').multiselect("""),format.raw/*121.43*/("""{"""),format.raw/*121.44*/(""" 
         """),format.raw/*122.10*/("""includeSelectAllOption: true,
           enableFiltering:true,
           buttonWidth: '190px'        
           
     """),format.raw/*126.6*/("""}"""),format.raw/*126.7*/(""");
	 $('.multiSelectTop').multiselect("""),format.raw/*127.36*/("""{"""),format.raw/*127.37*/(""" 
         """),format.raw/*128.10*/("""includeSelectAllOption: true,
           enableFiltering:true,
           buttonWidth: '190px',
maxHeight: 150,
            dropUp: true	    
           
     """),format.raw/*134.6*/("""}"""),format.raw/*134.7*/(""");
     
"""),format.raw/*136.1*/("""}"""),format.raw/*136.2*/(""");
</script>

<script type="text/javascript">
    $('.chcktbl').click(function () """),format.raw/*140.37*/("""{"""),format.raw/*140.38*/("""
            
           
            """),format.raw/*143.13*/("""$('.chcktbl:not(:checked)').attr('disabled', true);
      
            $('.chcktbl:not(:checked)').attr('disabled', false);

            
        
    """),format.raw/*149.5*/("""}"""),format.raw/*149.6*/(""");
</script>
<script type="text/javascript">
    $('#chckHead').click(function () """),format.raw/*152.38*/("""{"""),format.raw/*152.39*/("""
 
        """),format.raw/*154.9*/("""if (this.checked == false) """),format.raw/*154.36*/("""{"""),format.raw/*154.37*/("""
 
            """),format.raw/*156.13*/("""$('.chcktbl:checked').attr('checked', false);
        """),format.raw/*157.9*/("""}"""),format.raw/*157.10*/("""
        """),format.raw/*158.9*/("""else """),format.raw/*158.14*/("""{"""),format.raw/*158.15*/("""
            """),format.raw/*159.13*/("""$('.chcktbl:not(:checked)').attr('checked', true);
 
        """),format.raw/*161.9*/("""}"""),format.raw/*161.10*/("""
    """),format.raw/*162.5*/("""}"""),format.raw/*162.6*/("""); 
 
</script>
<script>
	$("#clearAllId").click(function()"""),format.raw/*166.35*/("""{"""),format.raw/*166.36*/("""
		"""),format.raw/*167.3*/("""$(".AssFromDtClass").val('');
		$(".AssToDtClass").val('');
		$(".CampaignClass").val('0');
		$("#selectAllDIv").hide();
			
		
	"""),format.raw/*173.2*/("""}"""),format.raw/*173.3*/(""");
	$("#ViewAlltblBox").click(function()"""),format.raw/*174.38*/("""{"""),format.raw/*174.39*/("""
		"""),format.raw/*175.3*/("""$("#selectAllDIv").show();
	"""),format.raw/*176.2*/("""}"""),format.raw/*176.3*/(""");
</script>
<script>
function ajaxViewAssignDataList()"""),format.raw/*179.34*/("""{"""),format.raw/*179.35*/("""

	"""),format.raw/*181.2*/("""var campSelec = $('#campaignSelected').val();
	var fromdate = $('#assignFromDate').val();
	var todate = $('#assignToDate').val();
	var u = $("#crename").val();
	var v = $("#all_dispositions").val();
	
	
	if(campSelec =='0' || v ==null || u ==null || fromdate=='' || todate=='')"""),format.raw/*188.75*/("""{"""),format.raw/*188.76*/("""
		"""),format.raw/*189.3*/("""document.getElementById("viewCallsERR").innerText="please provide all values";
        return false;
	"""),format.raw/*191.2*/("""}"""),format.raw/*191.3*/("""else"""),format.raw/*191.7*/("""{"""),format.raw/*191.8*/("""

		"""),format.raw/*193.3*/("""document.getElementById("viewCallsERR").innerText="";

	var selected_cres;
	var x = document.getElementById("crename");
	var j = 0;
	for (var i = 0; i < x.options.length; i++) """),format.raw/*198.45*/("""{"""),format.raw/*198.46*/("""
		"""),format.raw/*199.3*/("""if (x.options[i].selected == true) """),format.raw/*199.38*/("""{"""),format.raw/*199.39*/("""
			"""),format.raw/*200.4*/("""if (j == 0)
				selected_cres = x.options[i].value;
			else
				selected_cres = selected_cres + ","
						+ x.options[i].value;

			//console.log(selected_cres);
			j++;
		"""),format.raw/*208.3*/("""}"""),format.raw/*208.4*/("""
	"""),format.raw/*209.2*/("""}"""),format.raw/*209.3*/("""
	
	"""),format.raw/*211.2*/("""var selected_dispositions;
	var y = document.getElementById("all_dispositions");
	var k = 0;
	for (var a = 0; a < y.options.length; a++) """),format.raw/*214.45*/("""{"""),format.raw/*214.46*/("""
		"""),format.raw/*215.3*/("""if (y.options[a].selected == true) """),format.raw/*215.38*/("""{"""),format.raw/*215.39*/("""
			"""),format.raw/*216.4*/("""if (k == 0)
				selected_dispositions = y.options[a].value;
			else
				selected_dispositions = selected_dispositions
						+ "," + y.options[a].value;

			//console.log(selected_dispositions);
			k++;
		"""),format.raw/*224.3*/("""}"""),format.raw/*224.4*/("""
	"""),format.raw/*225.2*/("""}"""),format.raw/*225.3*/("""

	"""),format.raw/*227.2*/("""var urlLink = "/CREManager/changeAssignedCalls	"
    
    $('#tbl').dataTable( """),format.raw/*229.26*/("""{"""),format.raw/*229.27*/("""
     """),format.raw/*230.6*/(""""bDestroy": true,
     "processing": true,
     "serverSide": true,
     "scrollY": 300,
     "paging": true,
     "searching": false,
     "ordering":false,
     "ajax": """),format.raw/*237.14*/("""{"""),format.raw/*237.15*/("""
	        """),format.raw/*238.10*/("""'type': 'POST',
	        'url': urlLink,
	        'data': """),format.raw/*240.18*/("""{"""),format.raw/*240.19*/("""
	        	"""),format.raw/*241.11*/("""campSelec: ''+campSelec,
	        	fromdate:''+fromdate,
	        	todate:''+todate,
	        	selected_cres:''+selected_cres,
	        	selected_dispositions:''+selected_dispositions,
	        	
	        """),format.raw/*247.10*/("""}"""),format.raw/*247.11*/(""",
     """),format.raw/*248.6*/("""}"""),format.raw/*248.7*/("""
 """),format.raw/*249.2*/("""}"""),format.raw/*249.3*/(""" """),format.raw/*249.4*/("""); 
"""),format.raw/*250.1*/("""}"""),format.raw/*250.2*/("""
     
 """),format.raw/*252.2*/("""}"""),format.raw/*252.3*/("""


"""),format.raw/*255.1*/("""function ajaxAssignDataList()"""),format.raw/*255.30*/("""{"""),format.raw/*255.31*/("""

	"""),format.raw/*257.2*/("""var campSelec = $('#campaignSelected').val();
	var fromdate = $('#assignFromDate').val();
	var todate = $('#assignToDate').val();

	

	var u=$('#userlist').val();
	var selected_camps = new Array();
	$("input[name='callId[]']:checked").each(function(i) """),format.raw/*265.55*/("""{"""),format.raw/*265.56*/("""
		"""),format.raw/*266.3*/("""selected_camps.push($(this).val());
	"""),format.raw/*267.2*/("""}"""),format.raw/*267.3*/(""");

	//alert("u :"+u);
	//alert("selected_camps :"+selected_camps);
	
	if(u =='' || selected_camps =='')"""),format.raw/*272.35*/("""{"""),format.raw/*272.36*/("""

		"""),format.raw/*274.3*/("""document.getElementById("assignCallsERR").innerText="please provide all values for Assignment";
        return false;

		"""),format.raw/*277.3*/("""}"""),format.raw/*277.4*/("""else"""),format.raw/*277.8*/("""{"""),format.raw/*277.9*/("""

			"""),format.raw/*279.4*/("""document.getElementById("assignCallsERR").innerText="";

			var selected_cres_filter;
			var z = document.getElementById("crename");
			var l = 0;
			for (var i = 0; i < z.options.length; i++) """),format.raw/*284.47*/("""{"""),format.raw/*284.48*/("""
				"""),format.raw/*285.5*/("""if (z.options[i].selected == true) """),format.raw/*285.40*/("""{"""),format.raw/*285.41*/("""
					"""),format.raw/*286.6*/("""if (l == 0)
						selected_cres_filter = z.options[i].value;
					else
						selected_cres_filter = selected_cres_filter + ","
								+ z.options[i].value;

					//console.log(selected_cres);
					l++;
				"""),format.raw/*294.5*/("""}"""),format.raw/*294.6*/("""
			"""),format.raw/*295.4*/("""}"""),format.raw/*295.5*/("""
			
			"""),format.raw/*297.4*/("""var selected_dispositions;
			var y = document.getElementById("all_dispositions");
			var k = 0;
			for (var a = 0; a < y.options.length; a++) """),format.raw/*300.47*/("""{"""),format.raw/*300.48*/("""
				"""),format.raw/*301.5*/("""if (y.options[a].selected == true) """),format.raw/*301.40*/("""{"""),format.raw/*301.41*/("""
					"""),format.raw/*302.6*/("""if (k == 0)
						selected_dispositions = y.options[a].value;
					else
						selected_dispositions = selected_dispositions
								+ "," + y.options[a].value;

					//console.log(selected_dispositions);
					k++;
				"""),format.raw/*310.5*/("""}"""),format.raw/*310.6*/("""
			"""),format.raw/*311.4*/("""}"""),format.raw/*311.5*/("""		

	"""),format.raw/*313.2*/("""var selected_cres;
	var x = document.getElementById("userlist");
	var j = 0;
	for (var i = 0; i < x.options.length; i++) """),format.raw/*316.45*/("""{"""),format.raw/*316.46*/("""
		"""),format.raw/*317.3*/("""if (x.options[i].selected == true) """),format.raw/*317.38*/("""{"""),format.raw/*317.39*/("""
			"""),format.raw/*318.4*/("""if (j == 0)
				selected_cres = x.options[i].value;
			else
				selected_cres = selected_cres + ","
						+ x.options[i].value;

			//console.log(selected_cres);
			j++;
		"""),format.raw/*326.3*/("""}"""),format.raw/*326.4*/("""
	"""),format.raw/*327.2*/("""}"""),format.raw/*327.3*/("""

	
	
	"""),format.raw/*331.2*/("""var selected_camps = new Array();
	$("input[name='callId[]']:checked").each(function(i) """),format.raw/*332.55*/("""{"""),format.raw/*332.56*/("""
		"""),format.raw/*333.3*/("""selected_camps.push($(this).val());
	"""),format.raw/*334.2*/("""}"""),format.raw/*334.3*/(""");
	
	//alert(selected_camps.length);
	
	var selectAllStatus = document.getElementById("chckHead").checked;
                alert(selectAllStatus);
	
	
	
	
	var urlLink = "/CREManager/assignCallToAgents"
		$.ajax("""),format.raw/*345.10*/("""{"""),format.raw/*345.11*/("""
	        """),format.raw/*346.10*/("""type: 'POST',	       
	        url: urlLink,
	        data: """),format.raw/*348.16*/("""{"""),format.raw/*348.17*/("""
	        	
	        	"""),format.raw/*350.11*/("""selected_cres:''+selected_cres,
	        	selected_camps:''+selected_camps,
	        	selectAllStatus:''+selectAllStatus,
	        	campSelec: ''+campSelec,
	        	fromdate:''+fromdate,
	        	todate:''+todate,
	        	selected_cres_filter:''+selected_cres_filter,
	        	selected_dispositions:''+selected_dispositions,
	        	
	        	
	        """),format.raw/*360.10*/("""}"""),format.raw/*360.11*/(""",
	        success: function (json) """),format.raw/*361.35*/("""{"""),format.raw/*361.36*/("""
				
	        	"""),format.raw/*363.11*/("""window.location='changeAssignedCalls';
	        """),format.raw/*364.10*/("""}"""),format.raw/*364.11*/(""",
	        error: function () """),format.raw/*365.29*/("""{"""),format.raw/*365.30*/("""
	        	
	        	
	        """),format.raw/*368.10*/("""}"""),format.raw/*368.11*/("""
	       
	    """),format.raw/*370.6*/("""}"""),format.raw/*370.7*/(""");

		"""),format.raw/*372.3*/("""}"""),format.raw/*372.4*/("""
	    
	
"""),format.raw/*375.1*/("""}"""),format.raw/*375.2*/("""



  """),format.raw/*379.3*/("""</script>
 """))
      }
    }
  }

  def render(userlist:List[WyzUser],dispoList:List[CallDispositionData],camplist:List[Campaign],user:String,dealercode:String): play.twirl.api.HtmlFormat.Appendable = apply(userlist,dispoList,camplist,user,dealercode)

  def f:((List[WyzUser],List[CallDispositionData],List[Campaign],String,String) => play.twirl.api.HtmlFormat.Appendable) = (userlist,dispoList,camplist,user,dealercode) => apply(userlist,dispoList,camplist,user,dealercode)

  def ref: this.type = this

}


}

/**/
object changeAssinedCalls extends changeAssinedCalls_Scope0.changeAssinedCalls
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:20 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/changeAssinedCalls.scala.html
                  HASH: 1fcb58b4d7af8cb28f7e3801985a9c0cf64f0b52
                  MATRIX: 833->1|1043->115|1071->118|1129->168|1167->169|1197->173|1534->484|1571->505|1610->506|1642->511|1685->527|1698->531|1727->539|1757->542|1770->546|1805->560|1854->579|1887->585|2593->1265|2632->1288|2671->1289|2703->1294|2746->1310|2760->1315|2789->1323|2819->1326|2833->1331|2871->1348|2920->1367|2950->1370|3203->1597|3243->1621|3282->1622|3314->1627|3357->1643|3373->1650|3402->1658|3432->1661|3448->1668|3485->1684|3534->1703|3564->1706|4727->2843|4764->2864|4803->2865|4835->2870|4878->2886|4891->2890|4920->2898|4950->2901|4963->2905|4998->2919|5048->2938|5079->2941|5334->3165|5366->3169|5435->3209|5465->3210|5505->3222|5569->3257|5599->3258|5640->3270|5792->3394|5821->3395|5889->3434|5919->3435|5960->3447|6153->3612|6182->3613|6221->3624|6250->3625|6365->3711|6395->3712|6465->3753|6650->3910|6679->3911|6793->3996|6823->3997|6864->4010|6920->4037|6950->4038|6996->4055|7079->4110|7109->4111|7147->4121|7181->4126|7211->4127|7254->4141|7345->4204|7375->4205|7409->4211|7438->4212|7530->4275|7560->4276|7592->4280|7755->4415|7784->4416|7854->4457|7884->4458|7916->4462|7973->4491|8002->4492|8089->4550|8119->4551|8152->4556|8465->4840|8495->4841|8527->4845|8659->4949|8688->4950|8720->4954|8749->4955|8783->4961|8993->5142|9023->5143|9055->5147|9119->5182|9149->5183|9182->5188|9390->5368|9419->5369|9450->5372|9479->5373|9513->5379|9682->5519|9712->5520|9744->5524|9808->5559|9838->5560|9871->5565|10111->5777|10140->5778|10171->5781|10200->5782|10233->5787|10343->5868|10373->5869|10408->5876|10615->6054|10645->6055|10685->6066|10774->6126|10804->6127|10845->6139|11085->6350|11115->6351|11151->6359|11180->6360|11211->6363|11240->6364|11269->6365|11302->6370|11331->6371|11369->6381|11398->6382|11432->6388|11490->6417|11520->6418|11553->6423|11842->6683|11872->6684|11904->6688|11970->6726|11999->6727|12137->6836|12167->6837|12201->6843|12353->6967|12382->6968|12414->6972|12443->6973|12478->6980|12705->7178|12735->7179|12769->7185|12833->7220|12863->7221|12898->7228|13141->7443|13170->7444|13203->7449|13232->7450|13270->7460|13445->7606|13475->7607|13509->7613|13573->7648|13603->7649|13638->7656|13892->7882|13921->7883|13954->7888|13983->7889|14018->7896|14171->8020|14201->8021|14233->8025|14297->8060|14327->8061|14360->8066|14568->8246|14597->8247|14628->8250|14657->8251|14696->8262|14814->8351|14844->8352|14876->8356|14942->8394|14971->8395|15224->8619|15254->8620|15294->8631|15385->8693|15415->8694|15468->8718|15869->9090|15899->9091|15965->9128|15995->9129|16042->9147|16120->9196|16150->9197|16210->9228|16240->9229|16304->9264|16334->9265|16379->9282|16408->9283|16444->9291|16473->9292|16513->9304|16542->9305|16580->9315
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|44->13|44->13|44->13|45->14|45->14|45->14|45->14|45->14|45->14|45->14|47->16|49->18|70->39|70->39|70->39|71->40|71->40|71->40|71->40|71->40|71->40|71->40|73->42|74->43|82->51|82->51|82->51|83->52|83->52|83->52|83->52|83->52|83->52|83->52|85->54|86->55|129->98|129->98|129->98|130->99|130->99|130->99|130->99|130->99|130->99|130->99|132->101|133->102|147->116|149->118|150->119|150->119|152->121|152->121|152->121|153->122|157->126|157->126|158->127|158->127|159->128|165->134|165->134|167->136|167->136|171->140|171->140|174->143|180->149|180->149|183->152|183->152|185->154|185->154|185->154|187->156|188->157|188->157|189->158|189->158|189->158|190->159|192->161|192->161|193->162|193->162|197->166|197->166|198->167|204->173|204->173|205->174|205->174|206->175|207->176|207->176|210->179|210->179|212->181|219->188|219->188|220->189|222->191|222->191|222->191|222->191|224->193|229->198|229->198|230->199|230->199|230->199|231->200|239->208|239->208|240->209|240->209|242->211|245->214|245->214|246->215|246->215|246->215|247->216|255->224|255->224|256->225|256->225|258->227|260->229|260->229|261->230|268->237|268->237|269->238|271->240|271->240|272->241|278->247|278->247|279->248|279->248|280->249|280->249|280->249|281->250|281->250|283->252|283->252|286->255|286->255|286->255|288->257|296->265|296->265|297->266|298->267|298->267|303->272|303->272|305->274|308->277|308->277|308->277|308->277|310->279|315->284|315->284|316->285|316->285|316->285|317->286|325->294|325->294|326->295|326->295|328->297|331->300|331->300|332->301|332->301|332->301|333->302|341->310|341->310|342->311|342->311|344->313|347->316|347->316|348->317|348->317|348->317|349->318|357->326|357->326|358->327|358->327|362->331|363->332|363->332|364->333|365->334|365->334|376->345|376->345|377->346|379->348|379->348|381->350|391->360|391->360|392->361|392->361|394->363|395->364|395->364|396->365|396->365|399->368|399->368|401->370|401->370|403->372|403->372|406->375|406->375|410->379
                  -- GENERATED --
              */
          