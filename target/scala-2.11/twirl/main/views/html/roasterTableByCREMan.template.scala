
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object roasterTableByCREMan_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class roasterTableByCREMan extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[Integer,String,String,String,List[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(row_count:Integer,dealercode:String,dealerName:String,userName:String,getUsersList:List[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.100*/(""" 

"""),_display_(/*3.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*3.56*/ {_display_(Seq[Any](format.raw/*3.58*/("""

 """),_display_(/*5.3*/helper/*5.9*/.form(action = routes.CallInteractionController.toUpdateRoasterUnAvailablity(),'enctype -> "multipart/form-data")/*5.122*/{_display_(Seq[Any](format.raw/*5.123*/("""
"""),format.raw/*6.1*/("""<div class="row"> 
<div class="col-lg-12">
        <div class="panel panel-primary">
      <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
            <h4 class="panel-title accordion-toggle"></h4>
            <span style="width:45px;"/>&nbsp;</span> </div>
      <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
          <ul class="nav nav-tabs nav-justified">
                <li class="active"><a data-toggle="tab" href="#home"><b class="tabFonts">CRE Roster</b></a></li>
                <li><a data-toggle="tab" href="#menu1"><b class="tabFonts">Service Advisor Roster</b></a></li>
                <li><a data-toggle="tab" href="#menu2"><b class="tabFonts">Driver Roster</b></a></li>
              </ul>
          <div class="tab-content scrollable-menu">
                <div id="home" class="tab-pane fade in active"> <br>
                
                      <div class="col-md-12"> 
              
                     <table class="table table-striped table-bordered table-hover" >
              <thead>
                <tr>
                  <th>User Name</th>
                  <th>Check UnAvailable<th>
                   
                
                </tr>
              </thead>
           <tbody>
                   """),_display_(/*34.21*/for(i <- 0 to row_count) yield /*34.45*/{_display_(Seq[Any](format.raw/*34.46*/("""
                   """),_display_(/*35.21*/if(getUsersList(i).getRole()==("CRE"))/*35.59*/{_display_(Seq[Any](format.raw/*35.60*/("""
                   """),format.raw/*36.20*/("""<tr>
                       <input type="hidden" name="userList["""),_display_(/*37.61*/i),format.raw/*37.62*/("""].role" value=""""),_display_(/*37.78*/getUsersList(i)/*37.93*/.getRole()),format.raw/*37.103*/("""">
                       <td><input type="text" name="userList["""),_display_(/*38.63*/i),format.raw/*38.64*/("""].userName" style="border:none;" readonly value=""""),_display_(/*38.114*/getUsersList(i)/*38.129*/.getUserName()),format.raw/*38.143*/(""""></td>
                       <td><input type="checkbox" name="userList["""),_display_(/*39.67*/i),format.raw/*39.68*/("""].unAvailable" value=true></td>
                    </tr>
                    """)))}),format.raw/*41.22*/("""
                   
                """)))}),format.raw/*43.18*/("""
             """),format.raw/*44.14*/("""</tbody>      
            </table>   
                            <button type="submit" class="btn btn-sm btn-primary" id='js-upload-submit' name="selectedFile" value="CRE">UPDATE</button>
                  </div>
     
       </div>
                <div id="menu1" class="tab-pane fade scrollable-menu1"> <br>
              <div class="col-md-12"> 
              
                     <table class="table table-striped table-bordered table-hover" >
              <thead>
                <tr>
                  <th>User Name</th>
                   <th>Check UnAvailable<th>
                   
                </tr>
              </thead>
           <tbody>
                  """),_display_(/*62.20*/for(i <- 0 to row_count) yield /*62.44*/{_display_(Seq[Any](format.raw/*62.45*/("""
                  """),_display_(/*63.20*/if(getUsersList(i).getRole().equals("Service Advisor"))/*63.75*/{_display_(Seq[Any](format.raw/*63.76*/("""
                    """),format.raw/*64.21*/("""<tr> <input type="hidden" name="userList["""),_display_(/*64.63*/i),format.raw/*64.64*/("""].role" value=""""),_display_(/*64.80*/getUsersList(i)/*64.95*/.getRole()),format.raw/*64.105*/("""">
                         <td><input type="text" name="userList["""),_display_(/*65.65*/i),format.raw/*65.66*/("""].userName" style="border:none;" readonly value=""""),_display_(/*65.116*/getUsersList(i)/*65.131*/.getUserName()),format.raw/*65.145*/(""""></td>
                         <td><input type="checkbox" name="userList["""),_display_(/*66.69*/i),format.raw/*66.70*/("""].unAvailable" value=true></td>
                    </tr>
                    """)))}),format.raw/*68.22*/("""
                """)))}),format.raw/*69.18*/(""" 
             """),format.raw/*70.14*/("""</tbody>      
            </table> 
                   <button type="submit" class="btn btn-sm btn-primary" id='js-upload-submit' name="selectedFile" value="ServiceAdvisor">UPDATE</button>
                 
                  
                  </div>
            </div>
                <div id="menu2" class="tab-pane fade scrollable-menu2"> <br>
                     <div class="col-md-12"> 
              
                     <table class="table table-striped table-bordered table-hover" >
              <thead>
                <tr>
                   <th>User Name</th>
                  <th>Check UnAvailable <th>
                      
                  
                  
                </tr>
              </thead>
             <tbody>
                
                 """),_display_(/*92.19*/for(i <- 0 to row_count) yield /*92.43*/{_display_(Seq[Any](format.raw/*92.44*/("""
                  """),_display_(/*93.20*/if(getUsersList(i).getRole().equals("Driver"))/*93.66*/{_display_(Seq[Any](format.raw/*93.67*/("""
                 
                    """),format.raw/*95.21*/("""<tr>
                        <input type="hidden" name="userList["""),_display_(/*96.62*/i),format.raw/*96.63*/("""].role" value=""""),_display_(/*96.79*/getUsersList(i)/*96.94*/.getRole()),format.raw/*96.104*/("""">
                       <td><input type="text" name="userList["""),_display_(/*97.63*/i),format.raw/*97.64*/("""].userName" style="border:none;" value=""""),_display_(/*97.105*/getUsersList(i)/*97.120*/.getUserName()),format.raw/*97.134*/("""" readonly></td>
                       <td><input type="checkbox" name="userList["""),_display_(/*98.67*/i),format.raw/*98.68*/("""].unAvailable" value=true></td>
                    </tr>
                    """)))}),format.raw/*100.22*/("""
                """)))}),format.raw/*101.18*/("""
             
             """),format.raw/*103.14*/("""</tbody>     
            </table> 
             <button type="submit" class="btn btn-sm btn-primary" id='js-upload-submit' name="selectedFile" value="Driver">UPDATE</button>
                  </div>
                  
                
            </div>
            
              </div>
        </div>
          </div>
    </div>
      </div>
    </div>
""")))}),format.raw/*117.2*/("""
""")))}))
      }
    }
  }

  def render(row_count:Integer,dealercode:String,dealerName:String,userName:String,getUsersList:List[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(row_count,dealercode,dealerName,userName,getUsersList)

  def f:((Integer,String,String,String,List[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (row_count,dealercode,dealerName,userName,getUsersList) => apply(row_count,dealercode,dealerName,userName,getUsersList)

  def ref: this.type = this

}


}

/**/
object roasterTableByCREMan extends roasterTableByCREMan_Scope0.roasterTableByCREMan
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:38 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/roasterTableByCREMan.scala.html
                  HASH: c273b5d0c2972777a889d768916de9b09d1d38ca
                  MATRIX: 811->1|1005->99|1036->105|1098->159|1137->161|1168->167|1181->173|1303->286|1342->287|1370->289|2765->1657|2805->1681|2844->1682|2893->1704|2940->1742|2979->1743|3028->1764|3121->1830|3143->1831|3186->1847|3210->1862|3242->1872|3335->1938|3357->1939|3435->1989|3460->2004|3496->2018|3598->2093|3620->2094|3732->2175|3803->2215|3846->2230|4570->2927|4610->2951|4649->2952|4697->2973|4761->3028|4800->3029|4850->3051|4919->3093|4941->3094|4984->3110|5008->3125|5040->3135|5135->3203|5157->3204|5235->3254|5260->3269|5296->3283|5400->3360|5422->3361|5534->3442|5584->3461|5628->3477|6459->4281|6499->4305|6538->4306|6586->4327|6641->4373|6680->4374|6749->4415|6843->4482|6865->4483|6908->4499|6932->4514|6964->4524|7057->4590|7079->4591|7148->4632|7173->4647|7209->4661|7320->4745|7342->4746|7455->4827|7506->4846|7565->4876|7967->5247
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|36->5|36->5|36->5|37->6|65->34|65->34|65->34|66->35|66->35|66->35|67->36|68->37|68->37|68->37|68->37|68->37|69->38|69->38|69->38|69->38|69->38|70->39|70->39|72->41|74->43|75->44|93->62|93->62|93->62|94->63|94->63|94->63|95->64|95->64|95->64|95->64|95->64|95->64|96->65|96->65|96->65|96->65|96->65|97->66|97->66|99->68|100->69|101->70|123->92|123->92|123->92|124->93|124->93|124->93|126->95|127->96|127->96|127->96|127->96|127->96|128->97|128->97|128->97|128->97|128->97|129->98|129->98|131->100|132->101|134->103|148->117
                  -- GENERATED --
              */
          