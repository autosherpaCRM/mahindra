
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object viewReportCREManager_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class viewReportCREManager extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,Form[WyzUser],List[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealerName:String,user:String,form:Form[WyzUser],allcre:List[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import models._
import helper._
import scala._

Seq[Any](format.raw/*1.73*/("""
"""),_display_(/*5.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*5.52*/ {_display_(Seq[Any](format.raw/*5.54*/("""

"""),format.raw/*7.1*/("""<div class="panel panel-info">
  <div class="panel-heading">Chart Reports</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-5"> 
        <!-- LINE CHART -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Call type Pie</h3>
          </div>
          <div class="box-body">
            <canvas id="pieChart" style="height:250px"></canvas>
          </div>
        </div>        <!-- /.box --> 
      </div>
      <div class="col-md-5"> 
        <!-- AREA CHART -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Calls made vs Service Booked</h3>
          </div>
          <div class="box-body">
            <div class="chart">
              <canvas id="areaChart" style="height:250px"></canvas>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-2"> 
        <!-- AREA CHART -->
        <div class="box box-primary">
          <div class="panel panel-default">
            <div class="panel-heading"> <a href="#" class="pull-right"> <em class="icon-plus text-muted"></em> </a>Contacts</div>
            <div class="list-group"> 
              <!-- START User status--> 
              <a href="#" class="media p mt0 list-group-item"> <span class="pull-right"> <span class="circle circle-success circle-lg"></span> </span> <span class="media-body"> <span class="media-heading"> <strong>"""),_display_(/*42.216*/{user}),format.raw/*42.222*/("""</strong> <br>
              <small class="text-muted">Manager</small> </span> </span> </a> 
              <!-- END User status--> 
              """),_display_(/*45.16*/for(data <- allcre) yield /*45.35*/{_display_(Seq[Any](format.raw/*45.36*/("""
              """),format.raw/*46.15*/("""<!-- START User status--> 
              <a href="#" class="media p mt0 list-group-item"> <span class="pull-right"> <span class="circle circle-success circle-lg"></span> </span> <span class="media-body"> <span class="media-heading"> <strong>"""),_display_(/*47.216*/data/*47.220*/.getUserName()),format.raw/*47.234*/("""</strong> <br>
              <small class="text-muted">Agent</small> </span> </span> </a> 
              <!-- END User status--> 
              """)))}),format.raw/*50.16*/("""
             
              """),format.raw/*52.15*/("""<a href="#" class="media p mt0 list-group-item text-center text-muted">View all contacts</a> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
""")))}))
      }
    }
  }

  def render(dealerName:String,user:String,form:Form[WyzUser],allcre:List[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(dealerName,user,form,allcre)

  def f:((String,String,Form[WyzUser],List[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (dealerName,user,form,allcre) => apply(dealerName,user,form,allcre)

  def ref: this.type = this

}


}

/**/
object viewReportCREManager extends viewReportCREManager_Scope0.viewReportCREManager
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:42 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/viewReportCREManager.scala.html
                  HASH: f8a862106bfe612f94cbb69a5c41faa5c0e98fce
                  MATRIX: 810->1|1022->72|1050->128|1108->178|1147->180|1177->184|2736->1715|2764->1721|2941->1871|2976->1890|3015->1891|3059->1907|3330->2150|3344->2154|3380->2168|3559->2316|3618->2347
                  LINES: 27->1|34->1|35->5|35->5|35->5|37->7|72->42|72->42|75->45|75->45|75->45|76->46|77->47|77->47|77->47|80->50|82->52
                  -- GENERATED --
              */
          