
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object customerForm_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class customerForm extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[String,String,play.data.Form[Customer],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealerName:String,userName:String,form: play.data.Form[Customer]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.68*/("""
"""),_display_(/*2.2*/main("AutoSherpaCRM",userName,dealerName)/*2.43*/ {_display_(Seq[Any](format.raw/*2.45*/("""
"""),_display_(/*3.2*/helper/*3.8*/.form(action = routes.CustomerScheduledController.addcustomerInfo)/*3.74*/ {_display_(Seq[Any](format.raw/*3.76*/("""

"""),format.raw/*5.1*/("""<div class="fluid-container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Customer form </h3>
        </div>
        <div class="panel-body">
          <div class="row">
          <form >
            <div class="col-lg-12">
              <div class="col-lg-3">
                <div class="form-group"> """),_display_(/*17.43*/helper/*17.49*/.inputText(form("customerId"), 'class->"form-control", 'placeholder->"Enter Customer Id...", '_label->"Customer Id:")),format.raw/*17.166*/(""" """),format.raw/*17.167*/("""</div>
              </div>
              <div class="col-lg-3">
                <div class="form-group"> """),_display_(/*20.43*/helper/*20.49*/.inputText(form("customerName"), 'class->"form-control", 'placeholder->"Enter Customer Name...", '_label->"Customer Name :")),format.raw/*20.173*/(""" """),format.raw/*20.174*/("""</div>
              </div>
              <div class="col-lg-3">
                <div class="form-group"> """),_display_(/*23.43*/helper/*23.49*/.inputText(form("customerMobileNo"), 'class->"form-control", 'placeholder->"Enter Customer Mobile No...", '_label->"Customer MobileNo :")),format.raw/*23.186*/(""" """),format.raw/*23.187*/("""</div>
              </div>
              <div class="col-lg-3">
                <div class="form-group"> """),_display_(/*26.43*/helper/*26.49*/.inputText(form("customerAddress"), 'class->"form-control", 'placeholder->"Enter Customer Address...", '_label->"Customer Address:")),format.raw/*26.181*/(""" """),format.raw/*26.182*/("""</div>
              </div>
            </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <div class="col-lg-3">
                  <div class="form-group"> """),_display_(/*33.45*/helper/*33.51*/.inputText(form("customerEmail"), 'class->"form-control", 'placeholder->"Enter customer Email...", '_label->"customer Email :")),format.raw/*33.178*/(""" """),format.raw/*33.179*/("""</div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group"> """),_display_(/*36.45*/helper/*36.51*/.inputText(form("dao"), 'class->"form-control", 'placeholder->"Enter DAO...", '_label->"DAO :")),format.raw/*36.146*/(""" """),format.raw/*36.147*/("""</div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group"> """),_display_(/*39.45*/helper/*39.51*/.inputText(form("dob"), 'class->"form-control", 'placeholder->"Enter DOB...", '_label->"DOB :")),format.raw/*39.146*/(""" """),format.raw/*39.147*/("""</div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class='text-right'>
          <input type="submit" onclick="myFunction()" class="btn btn-primary" value="SUBMIT" />
        </div>
      </div>
    </div>
  </div>
</div>
</div>      
   """)))}),format.raw/*53.5*/("""

""")))}))
      }
    }
  }

  def render(dealerName:String,userName:String,form:play.data.Form[Customer]): play.twirl.api.HtmlFormat.Appendable = apply(dealerName,userName,form)

  def f:((String,String,play.data.Form[Customer]) => play.twirl.api.HtmlFormat.Appendable) = (dealerName,userName,form) => apply(dealerName,userName,form)

  def ref: this.type = this

}


}

/**/
object customerForm extends customerForm_Scope0.customerForm
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:25 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/customerForm.scala.html
                  HASH: ffd2d1d3460e0561e6c1c0d30891ff04cda56cca
                  MATRIX: 791->1|952->67|980->70|1029->111|1068->113|1096->116|1109->122|1183->188|1222->190|1252->194|1708->623|1723->629|1862->746|1892->747|2029->857|2044->863|2190->987|2220->988|2357->1098|2372->1104|2531->1241|2561->1242|2698->1352|2713->1358|2867->1490|2897->1491|3148->1715|3163->1721|3312->1848|3342->1849|3485->1965|3500->1971|3617->2066|3647->2067|3790->2183|3805->2189|3922->2284|3952->2285|4300->2603
                  LINES: 27->1|32->1|33->2|33->2|33->2|34->3|34->3|34->3|34->3|36->5|48->17|48->17|48->17|48->17|51->20|51->20|51->20|51->20|54->23|54->23|54->23|54->23|57->26|57->26|57->26|57->26|64->33|64->33|64->33|64->33|67->36|67->36|67->36|67->36|70->39|70->39|70->39|70->39|84->53
                  -- GENERATED --
              */
          