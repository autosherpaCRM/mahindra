
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object psf15thDayDispo_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class psf15thDayDispo extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Vehicle,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(vehicleData:Vehicle,userName:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.39*/("""

"""),format.raw/*3.1*/("""<div class="panel panel-primary">
		<div class="panel-heading" align="center"; style="background-color:#1797be;"><img src=""""),_display_(/*4.91*/routes/*4.97*/.Assets.at("images/INTERACTION HISTORY.png")),format.raw/*4.141*/("""" style="width:17px"/>&nbsp;<b>PSF DISPOSITION FORM 15TH DAY</b> 
		</div>
		
			<div class="panel-body">
			<div class="col-md-12">
				<div class="form-group">
					<div id="PSFconnectCall15th">
						<label for=""><b>Are you able to connect with the customer ?</b></label>
						<br>
						<div class="radio-inline">
							<label>
								<input type="radio" name="isContacted" value="Yes" id="isContactedId">
								Yes </label>
						</div>
						<div class="radio-inline">
							<label>
								<input type="radio" name="isContacted" value="No" id="" >
								No</label>
						</div>
					</div>
					<div style="display:none;" class="row animated  bounceInRight" id="15thDayPSFNotSpeachDiv"><br>
						<div class="col-md-12">
							<label for=""><b>Choose reason for not being able to talk to customer:</b></label>
							<br>
							<div class="radio">
								<label>
									<input type="radio" name="PSFDispositon" value="Ringing No response" id="">
									Ringing No response </label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="PSFDispositon" value="Busy" id="" >
									Busy</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="PSFDispositon" value="Switched Off / Unreachable" id="" >
									Switched Off / Unreachable</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="PSFDispositon" value="Invalid Number" id="" >
									Invalid Number</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="PSFDispositon" value="NoOther" id="15thDayOther1" >
									Other</label>
							</div>


							<div class="row animated  bounceInRight" style="display:none;"  id="15thDayPSFOtherDiv">
								<div class="col-md-4">
									<label><b>Please Specify</b></label>
									<textarea type="text" class="form-control textOnlyAccepted" rows="2" name="OtherComments" id="15thDayOtherPSF"></textarea>
								</div>
							</div>
							<div class="pull-right">
								
								<button type="submit" class="btn btn-info" id="15thDaytalkCustomerSubmit">Submit</button>
							</div>
						</div>
					</div>

					<div style="display:none;" class="row animated  bounceInRight" id="15thDayPSFYesTalk" >
						<div class="col-md-12">
							<p>Namaskar ! I am """),_display_(/*70.28*/userName),format.raw/*70.36*/(""" """),format.raw/*70.37*/("""from Sherpas Auto</p>
							<p>We want to take your feedback on the performance of your """),_display_(/*71.69*/{vehicleData.getModel()}),format.raw/*71.93*/(""",  """),_display_(/*71.97*/{vehicleData.getVehicleRegNo()}),format.raw/*71.128*/("""</p>
							<p>Please may I speak to you for few minutes?</p>
							<div class="radio-inline">
								<label>
									<input type="radio" name="disposition" value="PSF_Yes" id=""  class='15thDayYes'>
									Yes </label>
							</div>
							<div class="radio-inline">
								<label>
									<input type="radio" name="disposition" value="Call Me Later" class="15thDayNo" >
									No(Call Me Later) </label>
							</div>
							<div class="radio-inline">
								<label>
									<input type="radio" name="disposition" value="Not Interested"  class='15thDayNot'>
									Not Interested </label>
							</div>
						</div>
					</div>
					<!-----------------------------15th Day--------------------------------->
				<div class="row animated  bounceInRight" id="15thDayFormPSF" style="display:none;">


<div class="col-sm-12" id="" >
		  <label for="">How is your vehicle performance after service/repairs in our workshop?</label><br>
			 
			  <div class="radio-inline">
				<label>
			<input type="radio" name="vehiclePerformanceAfterService" value="Satisfied" id="SatisfiedPSF15">
				  Satisfied </label>
			  </div>
			  <div class="radio-inline">
				<label>
	 <input type="radio" name="vehiclePerformanceAfterService" value="Dissatisfied" id="DissatisfiedPSF15" >
				  Dissatisfied</label>
			  </div>
		</div>

		<div class="animated  bounceInRight">
			<div class="col-md-6">
				<textarea class="form-control" name="remarksList[1]" rows="2" type="text" placeholder="Remarks" id="remarks15thDay"></textarea>
			</div>
		</div>
		<br>
		<div class="animated  bounceInRight" id="">
							<div class="col-md-12">
								<label>If you are satisfied, please rate us 10/10 for all the surveys/feedbacks. </label><br>
							</div>
							<div class="col-md-3">
								<select class="form-control" id="15thDayRatting" name="feedbackrating" >
									<option value="0">--Select--</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
								</select>

							</div>
						</div>
						<div class="col-md-12">
						<div class="pull-right" id="" style="">
							<button type="button" class="btn btn-info" id="15thDayPSFBack">Back</button>
						<button type="button" class="btn btn-info" id="15thDayPSFSatisfiedUpsell" >Next</button>
						</div>
						</div>
		</div>
		<!-------Upsell Opportunity ------------------->
		<div class="row animated  bounceInRight" id="UpsellPSF15thday" style="display:none;">
						  <div class="col-md-12">
				   
					<label>Capture Lead : Is there an Upsell Opportunity.</label>
				   <br>
				   
					<div class="radio-inline">
					  <label>
						<input type="radio" name="LeadYes15thDayPSF" onclick="loadLeadBasedOnLocation();"  value="Yes" id="15thDayUpsellYes" >
						Yes</label>
					</div>
					<div class="radio-inline">
					  <label>
						<input type="radio" name="LeadYes15thDayPSF" value="No" id="15thDayUpsellNo" checked>
						No</label>
					</div>
					<div class="row animated  bounceInRight" style="display:none;" id="LeadDiv15thDayPSF">
					  <div class="col-md-6">
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="Upsell15thPSF" name="upsellLead[0].upSellType" value="Insurance" id="InsuranceIDCheckAlreadyServiced">
							Insurance</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="InsuranceSelectAlreadyService">
						  <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[0].taggedTo" id="insuranceLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[0].upsellComments"></textarea>
							</div>
						   </div>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="Upsell15thPSF" name="upsellLead[1].upSellType" value="Warranty / EW" id="WARRANTYIDAlreadyService" >
							Warranty / EW</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="WARRANTYSelectAlreadyService">
							<div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[1].taggedTo" id="warrantyLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[1].upsellComments"></textarea>
							</div>
						   </div>
						  
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="Upsell15thPSF" name="upsellLead[2].upSellType" value="VAS" id="VASIDAlreadyService" />
							VAS</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="VASTagToSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[2].taggedTo" id="vASLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[2].upsellComments"></textarea>
							</div>
						   </div>
						  
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="Upsell15thPSF" name="upsellLead[3].upSellType" value="Re-Finance / New Car Finance" id="ReFinanceIDCheckAlreadyService" />
							Re-Finance / New Car Finance</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="ReFinanceSelectAlreadyService">
						  <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[3].taggedTo" id="reFinanceLead">
								<option >Select</option>	
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[3].upsellComments"></textarea>
							</div>
						   </div>
						 
						</div>
						</div>
						 <div class="col-md-6">
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="Upsell15thPSF" name="upsellLead[4].upSellType" value="Sell Old Car" id="LoanIDAlreadyService" />
							Sell Old Car</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="LoanSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[4].taggedTo" id="sellOldCarLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[4].upsellComments"></textarea>
							</div>
						   </div>
					
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="Upsell15thPSF" name="upsellLead[5].upSellType" value="Buy New Car / Exchange" id="EXCHANGEIDAlreadyService" />
							Buy New Car / Exchange</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="EXCHANGEIDSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[5].taggedTo" id="buyNewCarLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[5].upsellComments"></textarea>
							</div>
						   </div>
						
						</div>
						  <div class="checkbox">
						  <label>
							<input type="checkbox" class="Upsell15thPSF" name="upsellLead[6].upSellType" value="UsedCar" id="UsedCarIDAlreadyService" />
							Used Car</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="UsedCarSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[6].taggedTo" id="usedCarLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[6].upsellComments"></textarea>
							</div>
						   </div>
						  
						</div>
				   
					  </div>
					</div>
					  <div class="pull-right" >
					<button type="button" class="btn btn-primary" id="BackTo15thSatisfed">Back</button>
					  <button type="submit" class="btn btn-primary" id="15thDayPSFSubmit" />Submit</button>
					  
					</div>
					
						  </div>
				
							</div>
					<!------------------Call Me Latter----------------->

					<div style="display:none;" class="row animated  bounceInRight" id="15thDayPSFYesNamaskarNoDiv" >

						<div class="col-md-3">
							<div class="form-group">
								<label for="followUpDate">Date:</label>
								<input type="text" name="psfFollowUpDate" class="datepickerPlus30Days form-control" id="time_FromIn15thDay" readonly>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="followUpTime">Time:</label>
								<input type="text" name="psfFollowUpTime" class="single-input form-control" id="time_ToIn15thDay" readonly>
							</div>
						</div>
						<div class="col-md-6">
					<div class="form-group">
					  <label for="followUpTime">Remarks</label>
					  <textarea type="text" name="remarksList[3]" class="form-control" id=""></textarea>
					</div>
				  </div>
						<br>
						<div class="pull-right" id="" >
							<button type="button" class="btn btn-info" name="" id="15thDayBackToDidUtlakPSF" >Back</button>
							<button type="submit" class="btn btn-info" name="" id="15thDaynotCallMeLaterSubmit" >Submit</button>
						</div>

					</div>
					<!--------------------------Not Interested------------------------>


					<div style="display:none;" class="row animated  bounceInRight" id="15thDayNotInterestedBtns" >

						<div class="col-md-6">
							<label>Remarks</label><br>
							<textarea class="form-control textOnlyAccepted" rows="2" name="remarksList[2]" id="15thDayremarksValueForNoIntrst"></textarea>
						</div>
						<br>
						<div class="pull-right"><br>
							<button type="button" class="btn btn-info" name="" id="15thDayBackToDidUtlakPSF1" >Back</button>
							<button type="submit" class="btn btn-info" name="" id="15thDaynotInterestedSubmit" >Submit</button>
						</div>
					</div>



				</div><!-----panel Group End---->
			</div>
		</div><!---panel Body End--->

	</div>
	
	<script src=""""),_display_(/*368.16*/routes/*368.22*/.Assets.at("javascripts/psfDispositionNew.js")),format.raw/*368.68*/("""" type="text/javascript"></script>"""))
      }
    }
  }

  def render(vehicleData:Vehicle,userName:String): play.twirl.api.HtmlFormat.Appendable = apply(vehicleData,userName)

  def f:((Vehicle,String) => play.twirl.api.HtmlFormat.Appendable) = (vehicleData,userName) => apply(vehicleData,userName)

  def ref: this.type = this

}


}

/**/
object psf15thDayDispo extends psf15thDayDispo_Scope0.psf15thDayDispo
              /*
                  -- GENERATED --
                  DATE: Sat Dec 16 16:03:44 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/psf15thDayDispo.scala.html
                  HASH: 771adb48bb889bd1248e56d2413928d57c9d01df
                  MATRIX: 773->1|905->38|935->42|1086->167|1100->173|1165->217|3623->2648|3652->2656|3681->2657|3799->2748|3844->2772|3875->2776|3928->2807|15746->14597|15762->14603|15830->14649
                  LINES: 27->1|32->1|34->3|35->4|35->4|35->4|101->70|101->70|101->70|102->71|102->71|102->71|102->71|399->368|399->368|399->368
                  -- GENERATED --
              */
          