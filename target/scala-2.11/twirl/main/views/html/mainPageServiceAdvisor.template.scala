
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object mainPageServiceAdvisor_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class mainPageServiceAdvisor extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,String,String,String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String,userName : String,dealerName:String,dealercode:String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.86*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>
<html>
<head>
<title>"""),_display_(/*6.9*/title),format.raw/*6.14*/("""</title>
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<meta http-equiv="cache-control" content="max-age=0" >
<meta http-equiv="expires" content="0" >
<meta http-equiv="pragma" content="no-cache" >
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<link rel="shortcut icon" type="image/png" href=""""),_display_(/*13.51*/routes/*13.57*/.Assets.at("images/favicon1.ico")),format.raw/*13.90*/("""">
<!--<script src=""""),_display_(/*14.19*/routes/*14.25*/.Assets.at("javascripts/jquery-1.7.1.min.js")),format.raw/*14.70*/("""" type="text/javascript"></script>-->
<script src=""""),_display_(/*15.15*/routes/*15.21*/.Assets.at("javascripts/jquery.js")),format.raw/*15.56*/("""" type="text/javascript"></script> 
<link rel="stylesheet" href=""""),_display_(/*16.31*/routes/*16.37*/.Assets.at("css/jquery-ui.min.css")),format.raw/*16.72*/("""">
<link rel="stylesheet" href=""""),_display_(/*17.31*/routes/*17.37*/.Assets.at("css/bootstrap.min.css")),format.raw/*17.72*/("""">

<link rel="stylesheet" href=""""),_display_(/*19.31*/routes/*19.37*/.Assets.at("css/skin-red.css")),format.raw/*19.67*/("""">
<link rel="stylesheet" href=""""),_display_(/*20.31*/routes/*20.37*/.Assets.at("css/AdminLTE.min.css")),format.raw/*20.71*/("""">
<link rel="stylesheet" href=""""),_display_(/*21.31*/routes/*21.37*/.Assets.at("css/dataTables.bootstrap.css")),format.raw/*21.79*/("""">
<link rel="stylesheet" href=""""),_display_(/*22.31*/routes/*22.37*/.Assets.at("css/dataTables.responsive.css")),format.raw/*22.80*/("""">
<link rel="stylesheet" href=""""),_display_(/*23.31*/routes/*23.37*/.Assets.at("css/font-awesome.min.css")),format.raw/*23.75*/("""">
<link rel="stylesheet" href=""""),_display_(/*24.31*/routes/*24.37*/.Assets.at("css/ionicons.min.css")),format.raw/*24.71*/("""">
<link rel="stylesheet" href=""""),_display_(/*25.31*/routes/*25.37*/.Assets.at("css/bootstrap-clockpicker.min.css")),format.raw/*25.84*/("""">
<link rel="stylesheet" href=""""),_display_(/*26.31*/routes/*26.37*/.Assets.at("css/animate.css")),format.raw/*26.66*/("""">
<link rel="stylesheet" href=""""),_display_(/*27.31*/routes/*27.37*/.Assets.at("css/lobibox.min.css")),format.raw/*27.70*/("""">
<link rel="stylesheet" href=""""),_display_(/*28.31*/routes/*28.37*/.Assets.at("css/Wyz_css.css")),format.raw/*28.66*/("""">
<link rel="stylesheet" href=""""),_display_(/*29.31*/routes/*29.37*/.Assets.at("css/multiselect.css")),format.raw/*29.70*/("""">
<link rel="stylesheet" href=""""),_display_(/*30.31*/routes/*30.37*/.Assets.at("css/bootstrap-datepicker.min.css")),format.raw/*30.83*/("""">
<link rel="stylesheet" href=""""),_display_(/*31.31*/routes/*31.37*/.Assets.at("css/sidenavi-left.css")),format.raw/*31.72*/("""">
<link rel="stylesheet" href=""""),_display_(/*32.31*/routes/*32.37*/.Assets.at("css/slideCss.css")),format.raw/*32.67*/("""">


<script type="text/javascript">
        function noBack()
         """),format.raw/*37.10*/("""{"""),format.raw/*37.11*/("""
             """),format.raw/*38.14*/("""window.history.forward();
         """),format.raw/*39.10*/("""}"""),format.raw/*39.11*/("""
        """),format.raw/*40.9*/("""noBack();
        window.onload = noBack;
       window.onpageshow = function(evt) """),format.raw/*42.42*/("""{"""),format.raw/*42.43*/(""" """),format.raw/*42.44*/("""if (evt.persisted) noBack() """),format.raw/*42.72*/("""}"""),format.raw/*42.73*/(""" 
        """),format.raw/*43.9*/("""window.onunload = function() """),format.raw/*43.38*/("""{"""),format.raw/*43.39*/(""" """),format.raw/*43.40*/("""void (0) """),format.raw/*43.49*/("""}"""),format.raw/*43.50*/("""
    """),format.raw/*44.5*/("""</script>
    
</head>


<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
        <div class="wrapper"> 
  <div id="throbber" style="display:none;">
<img src=""""),_display_(/*52.12*/routes/*52.18*/.Assets.at("images/loading_black.gif")),format.raw/*52.56*/(""""/>
</div>
  <!-- Main Header -->
  <header class="main-header"> 
    
    <!-- Logo --> 
    <a href="/ServiceAdvisor" class="logo"> <span class="logo-mini"><b>A</b>CRM</span> <span> <img src=""""),_display_(/*58.106*/routes/*58.112*/.Assets.at("images/dealercustomerconnectlogoblack2.png")),format.raw/*58.168*/("""" width="200" height="44" style="margin-left: -20px;"/></span> </a> 
    
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation"> 
      <!-- Sidebar toggle button--> 
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a>
      
      <div class="navbar-custom-menu">
         <ul class="nav navbar-nav">     
        
         
         <li><a href="#"><i class="fa fa-user fa-fw"></i><span class="hidden-xs hidden-sm">"""),_display_(/*69.93*/dealerName),format.raw/*69.103*/("""<span></a></li>
     	 <li><a href="#"><i class="fa fa-user fa-fw"></i><span class="hidden-xs hidden-sm">"""),_display_(/*70.91*/userName),format.raw/*70.99*/("""<span></a></li> 
     	  <li><a href="/ServiceAdvisor/changepassword"><i class="fa fa-sign-out fa-fw"></i><span class="hidden-xs hidden-sm">Change Password<span></a></li>      
                   
                    <li><a href="/logoutUser" onclick="noBack()"><i class="fa fa-sign-out fa-fw"></i>LogOut</a></li>              
            
             
            </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#"><i class="fa fa-sitemap"></i> <span>Complaints<span class="fa arrow"></span></span> <i class="fa fa-angle-left pull-right"></i></a>
           
          <ul class="treeview-menu"> 
       <!--   <li><a href="/ServiceAdvisor/PSFList"><i class="fa fa-phone-square"></i>PSF Call Log</a></li> -->
         
          <li><a href="/ServiceAdvisor/resolveComplaint"><i class="fa fa-phone-square"></i>Resolve Complaint</a></li>
          </ul>
        </li>-->
      </ul>      <!-- /.sidebar-menu --> 
    </section>   <!-- /.sidebar --> 
  </aside>
  <div class="content-wrapper">
    <section class="content">
           
      """),_display_(/*97.8*/content),format.raw/*97.15*/("""
      """),format.raw/*98.7*/("""</section>
    </div>
  <footer class="main-footer">
      <div class="pull-right hidden-xs">AutoSherpa</div>
      <strong>AutoSherpa &copy; 2016 <a href="http://www.autosherpas.com/">Company</a>.</strong> All rights reserved.
    </footer>
</div>
<script src=""""),_display_(/*105.15*/routes/*105.21*/.Assets.at("javascripts/jquery.js")),format.raw/*105.56*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*106.15*/routes/*106.21*/.Assets.at("javascripts/jquery-ui.min.js")),format.raw/*106.63*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*107.15*/routes/*107.21*/.Assets.at("javascripts/bootstrap.min.js")),format.raw/*107.63*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*108.15*/routes/*108.21*/.Assets.at("javascripts/bootstrap-clockpicker.min.js")),format.raw/*108.75*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*109.15*/routes/*109.21*/.Assets.at("javascripts/app.js")),format.raw/*109.53*/("""" type="text/javascript"></script> 
 <script src=""""),_display_(/*110.16*/routes/*110.22*/.Assets.at("javascripts/jquery.blockUI.js")),format.raw/*110.65*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*111.15*/routes/*111.21*/.Assets.at("javascripts/multiselect.js")),format.raw/*111.61*/("""" type="text/javascript"></script>

<script src=""""),_display_(/*113.15*/routes/*113.21*/.Assets.at("javascripts/Chart.min.js")),format.raw/*113.59*/("""" type="text/javascript"></script> 

<script src=""""),_display_(/*115.15*/routes/*115.21*/.Assets.at("javascripts/fastclick.min.js")),format.raw/*115.63*/("""" type="text/javascript"></script>

  <script src=""""),_display_(/*117.17*/routes/*117.23*/.Assets.at("javascripts/lobibox.js")),format.raw/*117.59*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*118.15*/routes/*118.21*/.Assets.at("javascripts/callDisposition.js")),format.raw/*118.65*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*119.15*/routes/*119.21*/.Assets.at("javascripts/ajaxCallScripts.js")),format.raw/*119.65*/("""" type="text/javascript"></script>

<script src=""""),_display_(/*121.15*/routes/*121.21*/.Assets.at("javascripts/Wyz_Scripts.js")),format.raw/*121.61*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*122.15*/routes/*122.21*/.Assets.at("javascripts/datatables.min.js")),format.raw/*122.64*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*123.15*/routes/*123.21*/.Assets.at("javascripts/servicesBookedScripts.js")),format.raw/*123.71*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*124.15*/routes/*124.21*/.Assets.at("javascripts/SideNavi.js")),format.raw/*124.58*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*125.15*/routes/*125.21*/.Assets.at("javascripts/newSideslide.js")),format.raw/*125.62*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*126.15*/routes/*126.21*/.Assets.at("javascripts/slideJS.js")),format.raw/*126.57*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*127.15*/routes/*127.21*/.Assets.at("javascripts/tabSlideOUtv1.3.js")),format.raw/*127.65*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*128.15*/routes/*128.21*/.Assets.at("javascripts/disposition.js")),format.raw/*128.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*129.15*/routes/*129.21*/.Assets.at("javascripts/psfDisposition.js")),format.raw/*129.64*/("""" type="text/javascript"></script>
</body>
</html>
"""))
      }
    }
  }

  def render(title:String,userName:String,dealerName:String,dealercode:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title,userName,dealerName,dealercode)(content)

  def f:((String,String,String,String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title,userName,dealerName,dealercode) => (content) => apply(title,userName,dealerName,dealercode)(content)

  def ref: this.type = this

}


}

/**/
object mainPageServiceAdvisor extends mainPageServiceAdvisor_Scope0.mainPageServiceAdvisor
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:33 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/mainPageServiceAdvisor.scala.html
                  HASH: 4ca7bda6f5cfd69a34413f71091db96800a19f29
                  MATRIX: 805->1|984->85|1014->89|1080->130|1105->135|1499->502|1514->508|1568->541|1617->563|1632->569|1698->614|1778->667|1793->673|1849->708|1943->775|1958->781|2014->816|2075->850|2090->856|2146->891|2209->927|2224->933|2275->963|2336->997|2351->1003|2406->1037|2467->1071|2482->1077|2545->1119|2606->1153|2621->1159|2685->1202|2746->1236|2761->1242|2820->1280|2881->1314|2896->1320|2951->1354|3012->1388|3027->1394|3095->1441|3156->1475|3171->1481|3221->1510|3282->1544|3297->1550|3351->1583|3412->1617|3427->1623|3477->1652|3538->1686|3553->1692|3607->1725|3668->1759|3683->1765|3750->1811|3811->1845|3826->1851|3882->1886|3943->1920|3958->1926|4009->1956|4114->2033|4143->2034|4186->2049|4250->2085|4279->2086|4316->2096|4429->2181|4458->2182|4487->2183|4543->2211|4572->2212|4610->2223|4667->2252|4696->2253|4725->2254|4762->2263|4791->2264|4824->2270|5040->2459|5055->2465|5114->2503|5343->2704|5359->2710|5437->2766|6006->3308|6038->3318|6172->3425|6201->3433|7424->4630|7452->4637|7487->4645|7785->4915|7801->4921|7858->4956|7937->5007|7953->5013|8017->5055|8096->5106|8112->5112|8176->5154|8255->5205|8271->5211|8347->5265|8426->5316|8442->5322|8496->5354|8576->5406|8592->5412|8657->5455|8735->5505|8751->5511|8813->5551|8893->5603|8909->5609|8969->5647|9050->5700|9066->5706|9130->5748|9212->5802|9228->5808|9286->5844|9364->5894|9380->5900|9446->5944|9524->5994|9540->6000|9606->6044|9686->6096|9702->6102|9764->6142|9843->6193|9859->6199|9924->6242|10002->6292|10018->6298|10090->6348|10168->6398|10184->6404|10243->6441|10322->6492|10338->6498|10401->6539|10480->6590|10496->6596|10554->6632|10632->6682|10648->6688|10714->6732|10792->6782|10808->6788|10870->6828|10948->6878|10964->6884|11029->6927
                  LINES: 27->1|32->1|34->3|37->6|37->6|44->13|44->13|44->13|45->14|45->14|45->14|46->15|46->15|46->15|47->16|47->16|47->16|48->17|48->17|48->17|50->19|50->19|50->19|51->20|51->20|51->20|52->21|52->21|52->21|53->22|53->22|53->22|54->23|54->23|54->23|55->24|55->24|55->24|56->25|56->25|56->25|57->26|57->26|57->26|58->27|58->27|58->27|59->28|59->28|59->28|60->29|60->29|60->29|61->30|61->30|61->30|62->31|62->31|62->31|63->32|63->32|63->32|68->37|68->37|69->38|70->39|70->39|71->40|73->42|73->42|73->42|73->42|73->42|74->43|74->43|74->43|74->43|74->43|74->43|75->44|83->52|83->52|83->52|89->58|89->58|89->58|100->69|100->69|101->70|101->70|128->97|128->97|129->98|136->105|136->105|136->105|137->106|137->106|137->106|138->107|138->107|138->107|139->108|139->108|139->108|140->109|140->109|140->109|141->110|141->110|141->110|142->111|142->111|142->111|144->113|144->113|144->113|146->115|146->115|146->115|148->117|148->117|148->117|149->118|149->118|149->118|150->119|150->119|150->119|152->121|152->121|152->121|153->122|153->122|153->122|154->123|154->123|154->123|155->124|155->124|155->124|156->125|156->125|156->125|157->126|157->126|157->126|158->127|158->127|158->127|159->128|159->128|159->128|160->129|160->129|160->129
                  -- GENERATED --
              */
          