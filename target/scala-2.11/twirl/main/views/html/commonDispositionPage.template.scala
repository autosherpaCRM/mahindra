
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object commonDispositionPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class commonDispositionPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template20[String,List[String],Insurance,Long,String,String,String,Customer,Vehicle,List[Location],WyzUser,Service,List[SMSTemplate],List[String],List[ServiceTypes],CallInteraction,List[SpecialOfferMaster],String,Html,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(oem:String,statesList:List[String],latestinsurance:Insurance,uniqueid:Long,dealercode:String,dealerName:String,userName:String,customerData:Customer,vehicleData:Vehicle,locationList:List[Location],userData:WyzUser,latestService:Service,smsTemplates:List[SMSTemplate],complaintOFCust:List[String],servicetypeList:List[ServiceTypes],interOfCall:CallInteraction,offersList:List[SpecialOfferMaster],typeDispo:String,dispoOut:Html,dispoInBound:Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.447*/("""

"""),_display_(/*3.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*3.65*/ {_display_(Seq[Any](format.raw/*3.67*/("""
"""),format.raw/*4.1*/("""<style>
	.ColorGreen"""),format.raw/*5.13*/("""{"""),format.raw/*5.14*/("""
		"""),format.raw/*6.3*/("""background-color:green;
	"""),format.raw/*7.2*/("""}"""),format.raw/*7.3*/("""
	
	"""),format.raw/*9.2*/(""".ColorRed"""),format.raw/*9.11*/("""{"""),format.raw/*9.12*/("""
		"""),format.raw/*10.3*/("""background-color:red;
	"""),format.raw/*11.2*/("""}"""),format.raw/*11.3*/("""
"""),format.raw/*12.1*/("""</style>

"""),_display_(/*14.2*/helper/*14.8*/.form(action=routes.InsuranceController.postCommonCallDispositionForm())/*14.80*/{_display_(Seq[Any](format.raw/*14.81*/("""
"""),format.raw/*15.1*/("""<link rel="stylesheet" href=""""),_display_(/*15.31*/routes/*15.37*/.Assets.at("css/cre.css")),format.raw/*15.62*/("""">
<input type="hidden" id="dndCust" value=""""),_display_(/*16.43*/{customerData.isDoNotDisturb()}),format.raw/*16.74*/("""">
<input type="hidden" id="typeOfDispoPageView" name="typeOfDispoPageView" value=""""),_display_(/*17.82*/typeDispo),format.raw/*17.91*/("""">
<input type="hidden" id="pFlag" name="pFlag" value=""""),_display_(/*18.54*/{if(customerData.getPreferredAdress()!=null){customerData.getPreferredAdress().getAddressType()}}),format.raw/*18.151*/("""">
<input type="hidden" name="customer_Id" id="customer_Id" value=""""),_display_(/*19.66*/{customerData.getId()}),format.raw/*19.88*/("""">
<input type="hidden" id="wyzUser_Id" name="wyzUser_Id" value=""""),_display_(/*20.64*/{userData.getId()}),format.raw/*20.82*/("""">
<input type="hidden" id="vehical_Id" name="vehicalId" value=""""),_display_(/*21.63*/{vehicleData.getVehicle_id()}),format.raw/*21.92*/("""">
<input type="hidden" name="uniqueidForCallSync" value=""""),_display_(/*22.57*/uniqueid),format.raw/*22.65*/("""">
<!-- <input type="hidden" id="workshopIdis" value=""""),_display_(/*23.53*/{if(latestService.getWorkshop()!=null){latestService.getWorkshop().getId()}}),format.raw/*23.129*/(""""> -->
<input type="hidden" id="workshopIdis" value="0">

<input type="hidden" id="location_Id" value=""""),_display_(/*26.47*/{userData.getLocation().getCityId()}),format.raw/*26.83*/("""">

<input type="hidden" name="callInteractionId" value=""""),_display_(/*28.55*/{interOfCall.getId()}),format.raw/*28.76*/("""">

<input type="hidden" id="srdispo_id" value=""""),_display_(/*30.46*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getId()}}),format.raw/*30.128*/("""">
<input type="hidden" id="servicebook_id" name="serviceBookId" value=""""),_display_(/*31.71*/{if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getServiceBookedId()}}),format.raw/*31.166*/("""">
<input type="hidden" id="selectedFBComp" value=""""),_display_(/*32.50*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getComplaintOrFB_TagTo()}}),format.raw/*32.149*/("""">			  
<input type="hidden" id="enteredRMFB" value=""""),_display_(/*33.47*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getRemarksOfFB()}}),format.raw/*33.138*/("""">			  
<input type="hidden" id="depForFB" value=""""),_display_(/*34.44*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getDepartmentForFB()}}),format.raw/*34.139*/("""">			  
<input type="hidden" id="selectedUpselOpp" value=""""),_display_(/*35.52*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getUpsellCount()}}),format.raw/*35.143*/("""">
<input type="hidden" id="selectedModeOfCont" value=""""),_display_(/*36.54*/{if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getTypeOfPickup()}}),format.raw/*36.146*/("""">

	

			

<div class="row">
<!-- 4User div Starts -->
	<div class="col-md-4">
			  <div id="panelDemo10" class="panel panel-info custinfoData">
      <div class="panel-heading" style="text-align:center; padding: 5px;"> <b><i class="fa fa-user"></i>&nbsp;
        
        """),_display_(/*48.10*/{if(customerData.getCustomerName()!=null){
        
        customerData.getCustomerName();
        
        }else{
        
        customerData.getConcatinatedName();							
        
        }}),format.raw/*56.11*/(""" """),format.raw/*56.12*/("""</b><br />
        <b><i class="fa fa-user-circle">
		<input type="text" id="driverIdUpdate" style="border:none;background-color:#1797be;" value=""""),_display_(/*58.96*/{if(customerData.getUserDriver()!=null){
        
        customerData.getUserDriver();
        
        }}),format.raw/*62.11*/("""" readonly></i>
         </b></div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-6 phoneselect">
            <div class="col-xs-3">
             <!--  <input type="button"  class="btn btn-primary" value=""""),_display_(/*68.74*/{customerData.getPreferredPhone().getPhoneNumber()}),format.raw/*68.125*/("""" id="ddl_phone_no" name="ddl_phone_no" style="cursor:default ;width: 150px;margin-left: -137px;font-size: medium;"> -->
			 <select class="btn btn-primary" id="ddl_phone_no" name="ddl_phone_no" style="cursor:default ;width: 150px;margin-left: -137px;font-size: medium;">
									"""),_display_(/*70.11*/for( phoneList <- customerData.getPhones()) yield /*70.54*/{_display_(Seq[Any](format.raw/*70.55*/("""
											
									"""),_display_(/*72.11*/{if(phoneList.isIsPreferredPhone ==true){
									
									if(phoneList.getPhoneNumber()!=null){
									<option value={phoneList.getPhoneNumber()} selected="selected">{phoneList.getPhoneNumber()}</option>
									}}else{ 
									
										if(phoneList.getPhoneNumber()!=null){
										<option value={phoneList.getPhoneNumber()} >{phoneList.getPhoneNumber()}</option>
										}}}),format.raw/*80.14*/("""
									""")))}),format.raw/*81.11*/("""						
								 """),format.raw/*82.10*/("""</select>
            </div>
            <button type="button" class="btn btn-success phoneinfobtn"  data-target="#addBtn" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil-square-o fa-lg"></i></button>
             """),_display_(/*85.15*/if(typeDispo=="insurance")/*85.41*/{_display_(Seq[Any](format.raw/*85.42*/("""
          	"""),format.raw/*86.12*/("""<!--  <button type="button" class="btn btn-primary" id="" data-target="#InsuPremiumPopup" data-toggle="modal" data-backdrop="static" data-keyboard="false" onClick="ajaxODPercentage();"><i class="fa fa-calculator fa-lg"></i></button> -->
            """)))}),format.raw/*87.14*/("""
"""),format.raw/*88.1*/("""<button type="button" style="Display:none" id="DNDbtn" class="btn btn-danger">DND</button>			
            <!--<button type="button" class="btn btn-success test" id="btnDnd" data-target="#DNDConfirm" data-toggle="modal" data-backdrop="static" data-keyboard="false">DND</button>--> 
            
          </div>
          <div class="col-sm-12" style="margin-top: 10px;">
            <div class="col-xs-4">
              <input type="hidden" id="isCallinitaited" name="isCallinitaited" value="NotDailed">
              <input type="hidden" id="pref_number_callini" value=""""),_display_(/*95.69*/{customerData.getPreferredPhone().getPhone_Id()}),format.raw/*95.117*/("""">
              <button type="button" id="callIdBtn" class="btn btn-primary btn-lg" onclick ="callfunctionFromPage('"""),_display_(/*96.116*/{customerData.getPreferredPhone().getPhone_Id()}),format.raw/*96.164*/("""','"""),_display_(/*96.168*/uniqueid),format.raw/*96.176*/("""','"""),_display_(/*96.180*/customerData/*96.192*/.getId()),format.raw/*96.200*/("""');"  style="border-radius: 29px;" ><i class="fa fa-phone-square fa-2x" aria-hidden="true" title="Make Call" ></i></button>
            </div>
            <div class="col-xs-4">
              <button type="button" class="btn btn-primary btn-lg " style="border-radius: 29px;"><i class="fa fa-envelope-square fa-2x" aria-hidden="true"></i></button>
            </div>
            <div class="col-xs-4">
              <button type="button" data-toggle="modal" data-target="#smsPopuId" data-backdrop="static" data-keyboard="false" class="btn btn-primary  btn-lg" style="border-radius: 29px;"><i class="fa fa-comment fa-2x" title="SMS" ></i></button>
            </div>
          </div>
          <div class="col-sm-12" style="margin-top: 6px;">
            <div class="row">
              <div class="col-xs-12" > <span><b><u>Email:</u>&nbsp; </b></span> <span style="font-size:15px" id="ddl_email"> """),_display_(/*107.127*/{if(customerData.getPreferredEmail()!=null){
                customerData.getPreferredEmail().getEmailAddress()
                }}),format.raw/*109.19*/(""" """),format.raw/*109.20*/("""</span> </div>
            </div>
            <div class="row">
              <div class="col-xs-6"> <span><b><u>DOA:</u>&nbsp; </b></span> <span>"""),_display_(/*112.84*/{customerData.getAnniversaryDateStr()}),format.raw/*112.122*/("""</span> </div>
              <div class="col-xs-6" > <span><b><u>DOB:</u>&nbsp; </b></span> <span>"""),_display_(/*113.85*/{customerData.getDobStr()}),format.raw/*113.111*/("""</span> </div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="row">
              <div class="col-xs-12"> <span><b><u>Address:</u>&nbsp; </b></span> <span class="addressBlog" id="prefAdressUpdate"> """),_display_(/*118.132*/{if(customerData.getPreferredAdress()!=null){
                if(customerData.getPreferredAdress().getConcatenatedAdress()!=null){
                customerData.getPreferredAdress().getConcatenatedAdress();
                }else{
                if(customerData.getConcatinatedPrefAddree()!=null){
                customerData.getConcatinatedPrefAddree();
                }    
                }
				}}),format.raw/*126.7*/(""" """),format.raw/*126.8*/("""</span> </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-6"> <b><u>Pref Time:</u>&nbsp;</b><span>"""),_display_(/*131.75*/{customerData.getPreferred_time_start()}),format.raw/*131.115*/(""" """),format.raw/*131.116*/("""To """),_display_(/*131.120*/{customerData.getPreferred_time_end()}),format.raw/*131.158*/("""</span> </div>
              <div class="col-xs-6"> <b><u>Pref Contact:</u>&nbsp;</b> <span>"""),_display_(/*132.79*/{customerData.getMode_of_contact()}),format.raw/*132.114*/("""</span> </div>
            </div>
          </div>
		  <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-12"> <b><u>Pref Day:</u>&nbsp;</b> <span>"""),_display_(/*137.76*/{customerData.getPreferred_day()}),format.raw/*137.109*/("""</span> </div>
              
            </div>
          </div>
          <!--button-->
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-6"> """),_display_(/*144.39*/if(customerData.getSegment()!=null)/*144.74*/{_display_(Seq[Any](format.raw/*144.75*/("""
                
                """),_display_(/*146.18*/for(segmentIterate <- customerData.getSegment()) yield /*146.66*/{_display_(Seq[Any](format.raw/*146.67*/("""
                """),format.raw/*147.17*/("""<button type="button" class="btn btn-info btn-flat btn-block loya" style="cursor:default;"><b style="font-size:15px"> """),_display_(/*147.136*/if(segmentIterate.getName()!="" && segmentIterate.getName()!=null)/*147.202*/{_display_(Seq[Any](format.raw/*147.203*/("""
                """),_display_(/*148.18*/segmentIterate/*148.32*/.getName()),format.raw/*148.42*/("""
                """)))}/*149.18*/else/*149.22*/{_display_(Seq[Any](format.raw/*149.23*/("""
                """),format.raw/*150.17*/("""NA
                """)))}),format.raw/*151.18*/(""" """),format.raw/*151.19*/("""</b></button>
                """)))}),format.raw/*152.18*/("""
                """)))}),format.raw/*153.18*/("""
				"""),format.raw/*154.5*/("""</div>
            </div>
          </div>
        </div>
      </div>
    </div>
			   </div>
		  <div class="col-md-8" style="padding: 0px;">
							<div class="col-sm-4" style="padding: 0px;">
			<!-- START panel-->
					<div id="panelDemo10" class="panel panel-info">
					<div class="panel-heading"><b>VEHICLE&nbsp;INFO</b><img src=""""),_display_(/*165.68*/routes/*165.74*/.Assets.at("images/car.png")),format.raw/*165.102*/("""" class="pull-right" style="width:28px;"></img></div>
						<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
						"""),_display_(/*167.8*/if(vehicleData!=null)/*167.29*/{_display_(Seq[Any](format.raw/*167.30*/("""
		
						
						"""),format.raw/*170.7*/("""<span><b>"""),_display_(/*170.17*/{vehicleData.getChassisNo()}),format.raw/*170.45*/("""</b></span>&nbsp;|&nbsp;<span><b>"""),_display_(/*170.79*/{vehicleData.getVehicleRegNo()}),format.raw/*170.110*/("""</b></span>&nbsp;|&nbsp;<span><b>"""),_display_(/*170.144*/{vehicleData.getModel()}),format.raw/*170.168*/("""</b></span></br>
						<span><b>"""),_display_(/*171.17*/{vehicleData.getVariant()}),format.raw/*171.43*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*171.64*/{vehicleData.getColor()}),format.raw/*171.88*/("""</b></span></br>
						<span>MILEAGE(Kms):&nbsp;<b>"""),_display_(/*172.36*/{latestService.getServiceOdometerReading()}),format.raw/*172.79*/("""</b></span></br>
						<span>SALE&nbsp;DATE:&nbsp;<b class="servi" style="background-color: #f05050; color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;padding:2px">"""),_display_(/*173.175*/{vehicleData.getSaleDateStr()}),format.raw/*173.205*/("""</b></b></span>
										
						
				""")))}),format.raw/*176.6*/("""
						"""),format.raw/*177.7*/("""</div>
									</div>
									<!-- END panel-->
							</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading specialoffer"><b>NEXT&nbsp;SERVICE&nbsp;DUE</b>
<!-- <div class="wrapper">
       <div class="ribbon-wrapper-green"><div class="ribbon-green"  data-toggle="modal" data-target="#offer" data-backdrop="static" data-keyboard="false">Special <br/> Offer</div></div>
</div> -->

<div class="pull-right">
<img src="/assets/images/specialoffer.png"  style="width: 80px; height: 80px; margin-top: -11px; margin-right: -17px;" data-toggle="modal" data-target="#offer" data-backdrop="static" data-keyboard="false">
</div>
</div>
<div class="panel-body" style="height: 110px;overflow-x:hidden;padding: 5px;">
	
		
						

<span><b class="servi" style="background-color: #f05050; color: #fff;border-radius: 3px;font-size: 13px;border-color: transparent;padding:2px">"""),_display_(/*198.145*/{vehicleData.getNextServicedateStr()}),format.raw/*198.182*/(""" """),format.raw/*198.183*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*198.204*/{vehicleData.getNextServicetype()}),format.raw/*198.238*/("""</b></span><br>

<span>FORECAST&nbsp;LOGIC: &nbsp;<b>"""),_display_(/*200.38*/{vehicleData.getForecastLogic()}),format.raw/*200.70*/("""</b></span><br>
<span>AVG&nbsp;RUNNING/MONTH(Kms):&nbsp;<b>"""),_display_(/*201.45*/{vehicleData.getAverageRunning()}),format.raw/*201.78*/("""</b> </span></br>
<span>NO&nbsp;SHOW(Months):&nbsp;<b>"""),_display_(/*202.38*/{vehicleData.getRunningBetweenvisits()}),format.raw/*202.77*/("""</b> </span>



</div>
</div>

</div>
							
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>LAST&nbsp;SERVICE&nbsp;STATUS</b><img src=""""),_display_(/*214.75*/routes/*214.81*/.Assets.at("images/car-insurance.png")),format.raw/*214.119*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
"""),_display_(/*216.2*/if(vehicleData!=null)/*216.23*/{_display_(Seq[Any](format.raw/*216.24*/("""
"""),format.raw/*217.1*/("""<span><b style="background-color: #f05050; color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;padding:2px">"""),_display_(/*217.134*/latestService/*217.147*/.getLastServiceDateStr()),format.raw/*217.171*/("""</b> &nbsp;|&nbsp;<b>"""),_display_(/*217.193*/latestService/*217.206*/.getLastServiceType()),format.raw/*217.227*/("""</b></span><br>
<span>WORKSHOP:&nbsp;<b>
"""),_display_(/*219.2*/{if(latestService.getWorkshop()!=null){
latestService.getWorkshop().getWorkshopName()
}
}),format.raw/*222.2*/("""
""")))}),format.raw/*223.2*/("""

"""),format.raw/*225.1*/("""</b></span><br>
<span>PSF STATUS :&nbsp;<b>"""),_display_(/*226.29*/latestService/*226.42*/.getLastPSFStatus()),format.raw/*226.61*/("""</b></span><br>
<span>PSF DATE :&nbsp;<b></b></span>
</div>

</div>
<!-- END panel-->
</div>

<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>INSURANCE</b><img src=""""),_display_(/*237.55*/routes/*237.61*/.Assets.at("images/truck.png")),format.raw/*237.91*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">

		

<span>EXPIRES&nbsp;ON:&nbsp;<b class="servi" style="background-color: #f05050;color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;">"""),_display_(/*242.158*/{latestinsurance.getPolicyDueDateStr()}),format.raw/*242.197*/("""</b></span><br>
<span style="font-size:13px">NEW&nbsp;INDIA&nbsp;ASSURANCE:&nbsp; <b>"""),_display_(/*243.71*/{latestinsurance.getInsuranceCompanyName()}),format.raw/*243.114*/("""</b></span><br>
<span style="font-size:13px">POLICY&nbsp;NO:&nbsp;<b>"""),_display_(/*244.55*/{latestinsurance.getPolicyNo()}),format.raw/*244.86*/("""</b></span><br>
<span style="font-size:13px">PREMIUM(Rs.):&nbsp;<b>"""),_display_(/*245.53*/{latestinsurance.getPremiumAmountAfterTax()}),format.raw/*245.97*/("""</b></span>


</div>

</div>
<!-- END panel-->
</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>OTHER&nbsp;UPSELL</b><img src=""""),_display_(/*256.63*/routes/*256.69*/.Assets.at("images/warranty-certificate.png")),format.raw/*256.114*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span>MAXICARE:&nbsp;<b class="servi" style="background-color: #f05050;color: #fff;border-radius: 3px; font-size: 13px;
border-color: transparent;"></b></span><br>
<span>RSA:&nbsp;<b class="servi" style="font-size: 13px;">"""),_display_(/*260.60*/latestService/*260.73*/.getRsaExpiryDate()),format.raw/*260.92*/("""</b></span><br>
<span style="font-size: 13px;">SHIELD:&nbsp;<b>"""),_display_(/*261.49*/latestService/*261.62*/.getShieldExpiryDate()),format.raw/*261.84*/("""</b></span></br>
<span>&nbsp;</span>
</div>
</div><!-- END panel-->
</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>COMPLAINTS</b><img src=""""),_display_(/*269.56*/routes/*269.62*/.Assets.at("images/businessman.png")),format.raw/*269.98*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span>OPEN:<b class="servi" style="background-color: #f05050; margin-left: 15px; color: #fff;border-radius: 3px; font-size: 17px;    border-color: transparent;">"""),_display_(/*271.163*/complaintOFCust/*271.178*/.get(0)),format.raw/*271.185*/("""</b></span><br>
<span> CLOSED:<b class="servi" style="background-color: #27c24c; color: #fff;border-radius: 3px;     font-size: 13px;
border-color: transparent;">"""),_display_(/*273.30*/complaintOFCust/*273.45*/.get(1)),format.raw/*273.52*/("""</b></span><br>
<span style="font-size: 14PX;">LAST&nbsp;RAISED&nbsp;ON:&nbsp;<b>"""),_display_(/*274.67*/complaintOFCust/*274.82*/.get(2)),format.raw/*274.89*/("""</b></span><br>
<span style="font-size: 13px;">RESOLVED&nbsp;BY:&nbsp;<b>"""),_display_(/*275.59*/complaintOFCust/*275.74*/.get(3)),format.raw/*275.81*/("""</b>&nbsp;</span>

</div>

</div>
<!-- END panel-->
</div>
			</div>
   <div class="modal fade" id="addBtn" role="dialog">
								<div class="modal-dialog modal-lg">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header" style="padding:1px;text-align:center;color:red;">
											<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*289.81*/routes/*289.87*/.Assets.at("images/close1.png")),format.raw/*289.118*/(""""></button>
											<h5 class="modal-title">UPDATE CUSTOMER INFORMATION</h5>
										</div>
										
								 <div class="modal-body">
					 <div class="row">
						  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <label><b>Owner:</b></label>
								  <input type="text" class="form-control textOnlyAccepted" id="customerNameEdit" value=""""),_display_(/*298.98*/customerData/*298.110*/.getCustomerName()),format.raw/*298.128*/("""" name="" readonly>
								  <br/>
								</div>
								
								<div class="col-sm-3">
								  <label><b>User/Driver:</b></label>
								  <input type="text" class="form-control textOnlyAccepted" id="driverNameEdit" value=""""),_display_(/*304.96*/customerData/*304.108*/.getUserDriver()),format.raw/*304.124*/("""" name="">
								  <br/>
								</div>
								<div class="col-sm-3">
								  <label><b>DND</b></label><br />
								  
								<label class="radio-inline"><input type="radio" name="doNotDisturb" id="dndcheckbox1" value="true">YES</label>
								<label class="radio-inline"><input type="radio" name="doNotDisturb" id="dndcheckbox2" value="false" checked>NO</label>
								 
								</div>
								
								
							  </div>
							  </br>
							  <div class="col-sm-12">
								<div class="col-sm-3">
								  <label><b>Permanent Address:</b></label>
								  <textarea class="form-control permanentAddress" rows="2" id="permanentAddress" name="permanentAddress" readonly >"""),_display_(/*321.125*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine1();
	  
	  }}),format.raw/*325.6*/("""_
	  """),_display_(/*326.5*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine2();
	  
	  }}),format.raw/*330.6*/("""_
	  """),_display_(/*331.5*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine3();
	  
	  }}),format.raw/*335.6*/("""</textarea>
								
								</div>
								<div class="col-sm-1">
								  <br/>
								  <button type="button" class="btn btn-success peditAddressmodal" data-toggle="modal" data-target="#driver_modal1" style="margin-top: 5px;">Add</button>
								</div>
								<div class="col-md-3">
								  <label><b>Residence Address:</b></label>
								  <textarea class="form-control" rows="2" id="residenceAddress" name="residenceAddress" readonly>"""),_display_(/*344.107*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine1();
	  
	  }}),format.raw/*348.6*/("""_
	  """),_display_(/*349.5*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine2();
	  
	  }}),format.raw/*353.6*/("""_
	  """),_display_(/*354.5*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine3();
	  
	  }}),format.raw/*358.6*/("""</textarea>
								</div>
								<div class="col-md-1">
								  <br/>
								  <button type="button" class="btn btn-success reditAddressmodal"  data-toggle="modal" data-target="#driver_modal2">Add</button>
								</div>
								<div class="col-md-3">
								  <label><b>Office Address:</b></label>
								  <textarea class="form-control" rows="2" id="officeAddress" name="officeAddress" readonly >"""),_display_(/*366.102*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine1();
	  
	  }}),format.raw/*370.6*/("""_
	  """),_display_(/*371.5*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine2();
	  
	  }}),format.raw/*375.6*/("""_
	  """),_display_(/*376.5*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine3();
	  
	  }}),format.raw/*380.6*/("""</textarea>
								</div>
								<div class="col-md-1">
								  <br/>
								  <button type="button" class="btn btn-success oeditAddressmodal" data-toggle="modal" data-target="#driver_modal3" >Add</button>
								</div>
							  </div>
							  <hr>
							  <div class="col-sm-12">
								<h5 style="color:Red;"><b>PREFERRED COMMUNICATION ADDRESS</b></h5>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox1" name="ADDRESS" value="0">
								  Permanent Address</div>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox2" name="ADDRESS" value="2" >
								  Residence Address</div>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox3" name="ADDRESS" value="1" >
								  Office Address</div>
							  </div>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <label><b>Preferred Email:</b></label>
								  <select class="form-control" id="email" name="email">
									"""),_display_(/*405.11*/for( emailList <- customerData.getEmails()) yield /*405.54*/{_display_(Seq[Any](format.raw/*405.55*/("""
											
									"""),_display_(/*407.11*/{if(emailList.isIsPreferredEmail() ==true)
									
									<option value={emailList.getEmailAddress()} selected="selected">{emailList.getEmailAddress()}</option>
									else 
										<option value={emailList.getEmailAddress()} >{emailList.getEmailAddress()}</option>
										}),format.raw/*412.12*/("""
									""")))}),format.raw/*413.11*/("""						
								 """),format.raw/*414.10*/("""</select>
								</div>
								<div class="col-xs-1">
								  <br/>
								  <button type="button" class="btn btn-success oeditEmailmodal" data-toggle="modal" data-target="#driver_modal4" style="margin-top: 5px;">Add</button>
								</div>
								<div class="col-sm-3">
								  <label><b>Date OF Birth:</b></label>
								  <input type="text" class="datepicMYDropDown form-control" id="dob" value=""""),_display_(/*422.86*/{customerData.getDob()}),format.raw/*422.109*/("""" name="dob" readonly>
								</div>
								<div class="col-md-3">
								  <label><b>Date of Anniversary</b></label>
								  <input type="text" class="datepicMYDropDown form-control" id="anniversary_date" value=""""),_display_(/*426.99*/{customerData.getAnniversary_date()}),format.raw/*426.135*/("""" name="anniversary_date" readonly>
								</div>
							  </div>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<h5 style="color:Red;"><b>MANAGE PREFERENCES</b></h5>
								<div class="col-sm-3">
								  <label><b>Preferred Contact #:</b></label>
								  <select class="form-control" id="preffered_contact_num" name="preffered_contact_num">
									"""),_display_(/*435.11*/for( phoneList <- customerData.getPhones()) yield /*435.54*/{_display_(Seq[Any](format.raw/*435.55*/("""
											
									"""),_display_(/*437.11*/{if(phoneList.isIsPreferredPhone ==true)
									
									<option value={phoneList.phone_Id.toString()} selected="selected">{phoneList.getPhoneNumber()}</option>
									else 
										<option value={phoneList.phone_Id.toString()} >{phoneList.getPhoneNumber()}</option>
										}),format.raw/*442.12*/("""
									""")))}),format.raw/*443.11*/("""						
								 """),format.raw/*444.10*/("""</select>
								  
								</div>
								<div class="col-sm-1">
								<br />
								 <input type="button"  class="btn btn-success"  id="tanniversary_edit" data-toggle="modal" data-target="#myModalAddPhone" value="Add">
										   </div>
								<div class="col-md-3">
								  <label><b>Preferred Mode of Contact:</b></label>
								  <div class="col-sm-12" id="modeOfCon"  style="display:none">
									<select class="form-control" id="preffered_mode_contact" name="preffered_mode_contact" multiple="multiple" >
									  <option value="Email">Email</option>
									  <option value="Phone">Phone</option>
									  <option value="SMS">SMS</option>
									</select>
								  </div>
								  <input type="text" class="form-control" id="tpreffered_mode_contact"  value=""""),_display_(/*460.89*/{customerData.getMode_of_contact()}),format.raw/*460.124*/("""" readonly>
								</div>
								<div class="col-md-1">
								<br />
								  <input type="button"  class="btn btn-success"  id="tpreffered_mode_contact_edit" value="Add">
								  
								</div>
								<div class="col-md-3">
								  <label><b>Preferred Day to Contact:</b></label>
								  <div class="col-sm-12" id="daysWeek" style="display:none">
									<select class="form-control" id="preffered_day_contact" name="preffered_day_contact" multiple="multiple">
									  <option value="Sunday">Sunday</option>
									  <option value="Monday">Monday</option>
									  <option value="Tuesday">Tuesday</option>
									  <option value="Wednesday">Wednesday</option>
									  <option value="Thursday">Thursday</option>
									  <option value="Friday">Friday</option>
									  <option value="Saturday">Saturday</option>
									</select>
								  </div>
								  <input type="text" class="form-control" id="tpreffered_day_contact"  value=""""),_display_(/*480.88*/{customerData.getPreferred_day()}),format.raw/*480.121*/("""" readonly>
								</div>
								<div class="col-md-1">
								<br />
								  <input type="button"  class="btn btn-success"  id="tpreffered_day_contact_edit" value="Add">
								</div>
							  </div>
							  <br>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <label><b>Preferred Time Of Contact(Start Time):</b></label>
								  <input type="text" class="form-control single-input" id="dateStarttime" name="dateStarttime" value=""""),_display_(/*492.112*/{customerData.getPreferred_time_start()}),format.raw/*492.152*/("""" readonly>
								</div>
								<div class="col-md-3">
								  <label><b>Preferred Time Of Contact(End Time):</b></label>
								  <input type="text" class="form-control single-input" id="dateEndtime" name="dateEndtime" value=""""),_display_(/*496.108*/{customerData.getPreferred_time_end()}),format.raw/*496.146*/("""" readonly>
								</div>
								<div class="col-sm-6">
								  <label><b>Comments:(250 Characters Only)</b></label>
								  <textarea type="text" class="form-control" id="custEditComment" rows="2" maxlength="250" name="custEditComment" value=""""),_display_(/*500.131*/{customerData.getComments()}),format.raw/*500.159*/("""">"""),_display_(/*500.162*/{customerData.getComments()}),format.raw/*500.190*/("""</textarea>
								</div>
							  </div>
							 
							</div>
				</div>

										<div class="modal-footer">
											<button type="button" id="pn_save" class="btn btn-success" onClick="ajaxCallForAddcustomerinfo();" data-dismiss="modal">Save</button>

										</div>
									</div>

								</div>
							</div>
		  
			  
		  <!-- 4User Div End -->
   


</div>

<!--Shasi dispositio-->
<div class="row">
<div class="col-lg-12 disposit">
<div class="panel panel-primary">
<div class="panel-heading " align="center"; style="background-color:#1797be;" ><img src=""""),_display_(/*527.91*/routes/*527.97*/.Assets.at("images/phone1.png")),format.raw/*527.128*/("""" style="width:17px"/>&nbsp;<b>DISPOSITION FORM</b> 
</div>
<div class="panel-body disposit">
<div class="row animated  bounceInRight">

<div class="col-sm-12" id="SMRInteractionFirst" >
		  <label for=""><h4>Select The Mode of Interaction :-&nbsp;&nbsp;</h4></label>
			 
			  <div class="radio-inline">
				<label>
				  <input type="radio" name="InOutCallName" value="OutCall" checked id="OutGoingID">
				  Outbound Call </label>
			  </div>
			  <div class="radio-inline">
				<label>
				  <input type="radio" name="InOutCallName" value="InCall" id="" >
				  Inbound Call</label>
			  </div>
		</div>
		<div>
		
		"""),_display_(/*548.4*/dispoInBound),format.raw/*548.16*/("""
		
		 """),format.raw/*550.4*/("""</div>
	  <div class="col-md-12 animated  bounceInRight" id="OutBoundDiv" style="display:none;">
		<div class="form-group">
		<div id="DidYouTalkDiv">
		  <label for=""><b>Did you talk to the customer ?</b></label>
		  <br>
		  <div class="radio-inline">
			<label>
			  <input type="radio" name="typeOfDisposition" value="Contact" id="SpeakYes">
			  Yes </label>
		  </div>
		  <div class="radio-inline">
			<label>
			  <input type="radio" name="typeOfDisposition" value="NonContact" id="SpeakNo" >
			  No</label>
		  </div>
		 </div>
		  <!--No-->
		  
		  <div style="display:none;" class="animated  bounceInRight" id="NotSpeachDiv"><br>
			<label for=""><b>Choose reason for not being able to talk to customer:</b></label>
			<br>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Ringing No response" id="">
				Ringing No response </label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Wrong Number" id="">
				Wrong Number</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Busy" id="" >
				Busy</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Switched Off / Unreachable" id="" >
				Switched Off / Unreachable</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Invalid Number" id="" >
				Invalid Number</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="NoOther" id="NOOthersCheck" >
				Other</label>
			</div>
			
			
			<div class="row animated  bounceInRight" style="display:none;"  id="NoOthers">
			<div class="col-md-3">
			<label><b>Please Specify</b></label>
			<textarea type="text" class="form-control NoOthersText1" rows="1" name="other"></textarea>
			</div>
			</div>
			<div class="pull-right">
					<button type="button" class="btn btn-info" id="DidUTalkNO" style="background-color:#1797BE">Back</button>						
					<button type="submit" class="btn btn-info" name="typeOfsubmit" style="background-color:#1797BE" id="nonContactValidation" value="nonContact">Submit</button>
			</div>
		  </div>
		  <!--Book My Service-->
		  <div style="display:none;" class="animated  bounceInRight whatDidCustSayDiv" id="WhatdidtheCustomersayDIV" >
		  <div id="whatDidCustSayDiv">
			<label for=""><b>What did the customer say?</b></label>
			<br>
			
		"""),_display_(/*621.4*/if(typeDispo=="insurance")/*621.30*/{_display_(Seq[Any](format.raw/*621.31*/("""	
			
			"""),format.raw/*623.4*/("""<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Book Appointment" id="BookMyAppointment" >
				Book Appointment </label>
			</div>
			<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Renewal Not Required" id="RenewalNotRequired" >
				Renewal Not Required</label>
			</div>
			
		
			""")))}),format.raw/*635.5*/("""

			"""),_display_(/*637.5*/if(interOfCall.getServiceBooked()==null && typeDispo != "insurance")/*637.73*/{_display_(Seq[Any](format.raw/*637.74*/(""" 
						
			"""),format.raw/*639.4*/("""<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Book My Service" id="BookMyService" >
				Book My Service </label>
			</div>
			
			<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Service Not Required" id="ServiceNotRequired" >
				Service Not Required</label>
			</div>
				
		""")))}),format.raw/*651.4*/("""	"""),_display_(/*651.6*/if(interOfCall.getServiceBooked() !=null && typeDispo != "insurance")/*651.75*/{_display_(Seq[Any](format.raw/*651.76*/(""" 
		"""),format.raw/*652.3*/("""<div class="radio-inline" >
			  <label>
				<input type="radio" name="disposition" value="Confirmed" id="ConfirmedId" >
				Confirmed </label>
			</div>
			
		<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Book My Service" id="BookMyService" >
				Reschedule </label>
			</div>
				
			
			<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Cancelled" id="CancelId" >
				Cancel</label>
			</div>
		""")))}),format.raw/*670.4*/("""
		"""),format.raw/*671.3*/("""<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Call Me Later" id="CallMeLatter" >
				Call Me Later</label>
			</div>
			
		
		

		
			</div>
		  </div>
		  <div>

		  
		   """),_display_(/*686.7*/dispoOut),format.raw/*686.15*/(""" 		  
		
		  """),format.raw/*688.5*/("""<!--call me latter-->
		 </div>
		  </div>
		
		  </div>
		 </div>
		 </div>
		 </div>
		 </div>
		 <!-----------INTERACTIONH HISTORY--------->
		 <div class="col-lg-12">
	<div class="panel panel-primary">
	<div class="panel-heading " align="center"; style="background-color:#1797be;" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne3"><img src=""""),_display_(/*700.167*/routes/*700.173*/.Assets.at("images/INTERACTION HISTORY.png")),format.raw/*700.217*/("""" style="width:17px"/>&nbsp;<b>INTERACTION HISTORY</b> 
</div>

  <div id="collapseOne3" class="panel-collapse collapse in">
		<div class="panel-body">
	  <div class="panel with-nav-tabs">
			<div class="panel-heading clearfix">
		  <div>
				<ul class="nav nav-tabs">
			  <li class="expand"><a href="#tab1primary" data-toggle="tab" onclick="callHistoryByVehicle();">PREVIOUS COMMENTS</a></li>
			  <li class="expand"><a href="#tab2primary" data-toggle="tab" onclick="callHistoryByVehicle();">DISPOSITIONS</a></li>
			  <li class="expand"><a href="#tab3primary" data-toggle="tab" onclick="smsHistoryOfUser();">SMS</a></li>
			  <li class="expand"><a href="#tab4primary" data-toggle="tab">E-Mail</a></li>
			  <li class="expand"><a href="#tab5primary" data-toggle="tab" onclick="complaintsOFVehicle();">COMPLAINTS</a></li>
			 """),_display_(/*714.6*/if(typeDispo=="insurance")/*714.32*/{_display_(Seq[Any](format.raw/*714.33*/(""" 
			  """),format.raw/*715.6*/("""<li class="expand"><a href="#tab6primary" data-toggle="tab" onclick="insuranceHistoryOfCustomer();">APPOINTMENT HISTORY</a></li>
			 """)))}),format.raw/*716.6*/(""" 
			"""),format.raw/*717.4*/("""</ul>
			  </div>
		</div>
			<div class="panel-body">
		  <div class="tab-content">
				<div class="tab-pane fade in active" id="tab1primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information" id="previousCommentTable">
					  <thead>
							<tr>
						  <th></th>
						  <th></th>						  
						 
						</tr>
						  </thead>
					  <tbody>
						  
					 
						</tbody>
					  
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
				<div class="tab-pane fade" id="tab2primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive" id="dispositionHistory">
					  <thead>
							<tr>
						  <th>Date</th>
						  <th>Time</th>
						  <th>Call Type</th>
						  <th>Disposition</th>
						  <th>Next FollowUpDate</th>
						  <th>Next FollowUpTime</th>
						  <th>CRE ID</th>
						 
						</tr>
						  </thead>
					  <tbody>
						  
					 
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			<div class="tab-pane fade" id="tab3primary">
			  <div class="row">
				<div class="col-sm-12">
				
						<table class="table table-bordered table-responsive" id="sms_history">
						 <thead>
							<tr>
						  <th>DATE</th>
						  <th>TIME</th>
						  <th>SOURCE AUTO/CRE ID</th>
						  <th>SCENARIO</th>
						  <th>MESSAGE</th>
						  <th>STATUS</th>
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					
				</div>
				
				</div>
			</div>
			<div class="tab-pane fade" id="tab4primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information">
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			<div class="tab-pane fade" id="tab5primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information" id="complaint_history">
					  <thead>
							<tr>
						  <th>COMPLAINT No.</th>
						  <th>ISSUE DATE</th>
						  <th>SOURCE</th>
						  <th>FUNCTION</th>
						  <th>CATEGORY</th>
						  <th>STATUS</th>
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			
			<div class="tab-pane fade" id="tab6primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information" id="insurance_history">
					  <thead>
							<tr>
						  <th>INSURANCE COMPANY</th>
						  <th>IDV</th>
						  <th>OD %</th>
						  <th>OD (Rs.)</th>
						  <th>NCB %</th>
						  <th>NCB (Rs.)</th>
						  <th>DISCOUNT%</th>
						  <th>OD PREMIUM</th>
						  <th>LIABILITY PREMIUM</th>
						  <th>ADD-ON PREMIUM</th>
						  <th>PREMIUM (Before Tax)</th>
						  <th>TAX %</th>
						  <th>PREMIUM (After Tax)</th>
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			  </div>
		</div>
		  </div>
	</div>
	  </div>
</div>
  </div>
		 </div>
		






		
		




<!--sashi model-->
<div class="modal fade" id="driver_modal1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*895.72*/routes/*895.78*/.Assets.at("images/close1.png")),format.raw/*895.109*/(""""></button>
		<h4 class="modal-title">Permanent Address</h4>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control paddr_line1" rows="1"   placeholder="Please enter Company Name/Flat No & Building name." ></textarea>
			  <br/></td>
		 
			<td>Address Line2:</td>
			<td><textarea class="form-control paddr_line2" rows="1"   placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
			  <td>Address Line3:</td>
			<td><textarea class="form-control paddr_line3" rows="1"  placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
		  </tr>
		  <tr>
		  
		  	<td>State</td>
			<td><select  class="form-control paddr_line5" id="pstateId" onchange="getCityByStateSelection('pstateId','paddrrCity');">
			<option value="--SELECT--">--SELECT--</option>
			"""),_display_(/*917.5*/for(states <- statesList) yield /*917.30*/{_display_(Seq[Any](format.raw/*917.31*/("""
           
          """),format.raw/*919.11*/("""<option value=""""),_display_(/*919.27*/states),format.raw/*919.33*/("""">"""),_display_(/*919.36*/states),format.raw/*919.42*/("""</option>
                              
	  """)))}),format.raw/*921.5*/("""
			
			"""),format.raw/*923.4*/("""</select>
			<br/></td>
		  
			<td>City</td>			
		  <td><select  class="form-control paddr_line4" id="paddrrCity">
			                    
	  </select><br/></td>
			</tr>

		  <tr>
			<td>PinCode</td>			
			<td><input type="text"  class="form-control paddr_line6 numberOnly" id="ppincode" maxlength="6" rows="1" value=""""),_display_(/*934.117*/{if( customerData.getPermanentAddressData()!=null){ customerData.getPermanentAddressData().getPincode()} }),format.raw/*934.223*/("""" placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control paddr_line7" rows="1" value="India"  placeholder="Please enter Country" readonly/><br/></td>
		  </tr>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary paddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="driver_modal2" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*951.72*/routes/*951.78*/.Assets.at("images/close1.png")),format.raw/*951.109*/(""""></button>
		<h4 class="modal-title">Residence Address</h4>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control raddr_line1" rows="1"  name="raddressline1" placeholder="Please enter Company Name/Flat No & Building name." ></textarea>
			  <br/></td>
		
			<td>Address Line2:</td>
			<td><textarea class="form-control raddr_line2" rows="1"  name="raddressline2" placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
			  
			 <td>Address Line3:</td>
			<td><textarea class="form-control raddr_line3" rows="1"  name="raddressline3" placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td> 
		  </tr>
		   <tr>
		   
		   <td>State</td>			
		 <td><select  class="form-control raddr_line5" id="rstateId" onchange="getCityByStateSelection('rstateId','raddrrCity');">
			<option value="--SELECT--">--SELECT--</option>
			"""),_display_(/*974.5*/for(states <- statesList) yield /*974.30*/{_display_(Seq[Any](format.raw/*974.31*/("""
           
          """),format.raw/*976.11*/("""<option value=""""),_display_(/*976.27*/states),format.raw/*976.33*/("""">"""),_display_(/*976.36*/states),format.raw/*976.42*/("""</option>
                              
	  """)))}),format.raw/*978.5*/("""
			
			"""),format.raw/*980.4*/("""</select>
			<br/></td>
			<td>City</td>
			
		    <td><select  class="form-control raddr_line4" id="raddrrCity">
			                    
	  </select><br/></td>
			 </tr>

		  <tr>
			<td>PinCode</td>
			<td><input type="text"  class="form-control raddr_line6 numberOnly" rows="1" maxlength="6" value=""""),_display_(/*991.103*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getPincode()} }),format.raw/*991.209*/(""""placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control raddr_line7" rows="1" value="India" placeholder="Please enter Country" readonly/><br/></td>
		  </tr>
		
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary raddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="driver_modal3" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1009.72*/routes/*1009.78*/.Assets.at("images/close1.png")),format.raw/*1009.109*/(""""></button>
		<h4 class="modal-title">Office Address</h4>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control oaddr_line1" rows="2"  placeholder="Please enter Company Name/Flat No & Building name." name="oaddressline1">"""),_display_(/*1016.148*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine1()} }),format.raw/*1016.253*/("""</textarea>
			  <br/></td>
		  
			<td>Address Line2:</td>
			<td><textarea class="form-control oaddr_line2" rows="2" placeholder="Please enter Road number/ Road name." name="oaddressline2">"""),_display_(/*1020.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine2()} }),format.raw/*1020.238*/("""</textarea>
			  <br/></td>
			<td>Address Line3:</td>
			<td><textarea class="form-control oaddr_line3" rows="2" placeholder="Please enter Road number/ Road name." name="oaddressline3">"""),_display_(/*1023.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine3()} }),format.raw/*1023.238*/("""</textarea>
			  <br/></td>
		  </tr>
		  <tr>
		  
		  <td>State</td>
			
		 <td><select  class="form-control oaddr_line4" id="paddr_line4" name="paddressline4"  onchange="getCityByStateSelection('paddr_line4','paddr_line7');">
			<option value="--SELECT--">--SELECT--</option>
			"""),_display_(/*1032.5*/for(states <- statesList) yield /*1032.30*/{_display_(Seq[Any](format.raw/*1032.31*/("""
           
          """),format.raw/*1034.11*/("""<option value=""""),_display_(/*1034.27*/states),format.raw/*1034.33*/("""">"""),_display_(/*1034.36*/states),format.raw/*1034.42*/("""</option>
                              
	  """)))}),format.raw/*1036.5*/("""
			
			"""),format.raw/*1038.4*/("""</select>
			<br/></td>
			<td>City</td>			
		  <td><select  class="form-control oaddr_line7"  id="paddr_line7" name="paddressline3">			                    
	  </select><br/></td>
			 </tr>

		  <tr>
			<td>PinCode</td>
			<td><input type="text"  class="form-control oaddr_line5 numberOnly" maxlength="6" id="paddr_line5" rows="1" value=""""),_display_(/*1047.120*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getPincode()} }),format.raw/*1047.220*/("""" name="paddressline5" placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control oaddr_line6" id="paddr_line6" rows="1" value="India" name="paddressline6" placeholder="Please enter Country" readonly/><br/></td>
		  </tr>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary oaddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="driver_modal4" role="dialog">
    <div class="modal-dialog modal-xs">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1067.80*/routes/*1067.86*/.Assets.at("images/close1.png")),format.raw/*1067.117*/(""""></button>
          <h4 class="modal-title">ADD EMAIL</h4>
        </div>
		
		
        <div class="modal-body">
		<div class="row">
        <div class="col-xs-12">
        <div class="col-xs-4">
        <input type="text" name="myEmailNum" class="form-control email"  maxlength="30" min="5" max="30" id="myEmailNum" >
      </div>
        </div>
        </div>
		</div>
        <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal" id="saveEmailCust">SAVE</button>
        </div>
      </div>
      
    </div>
  </div>		  
<div id="smallModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h4 class="modal-title">Message</h4>
	  </div>
	  <div class="modal-body">
		<p>This feature is not enabled for """),_display_(/*1096.39*/userName),format.raw/*1096.47*/(""".<br>
		  Contact Administrator.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>
  </div>
</div>
<div id="smallModal1" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h4 class="modal-title">E-Mail</h4>
	  </div>
	  <div class="modal-body">
		<p>This feature is not enabled for """),_display_(/*1113.39*/userName),format.raw/*1113.47*/(""" """),format.raw/*1113.48*/(""".<br>
		  Contact Administrator.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="sa_modal" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1126.72*/routes/*1126.78*/.Assets.at("images/close1.png")),format.raw/*1126.109*/(""""></button>
		<h4 class="modal-title">CHECK SMR LOAD</h4>
		<br>
		<h5 class="modal-title">Auto Service</h5>
	  </div>
	  <div class="modal-body">
		<table border="1" class="table table-striped table-bordered table-hover" id="dataTables-example60">
		  <thead>
			<tr>
			  
			  <th>DATE</th>
			  <th>SMR BOOKED</th>
			  <th>CAPACITY</th>
			</tr>
		  </thead>
		  <tbody>    
			
			
			
		  </tbody>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="sa_modal1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1158.72*/routes/*1158.78*/.Assets.at("images/close1.png")),format.raw/*1158.109*/(""""></button>
		<h4 class="modal-title">CHECK DRIVER'S SCHEDULE</h4>
		<br>
		<h5 class="modal-title">Auto Service</h5>
	  </div>
	  <div class="modal-body">
		<table border="1"  class="table table-striped table-bordered table-hover" id="">
		  <thead>
			<tr>
			  <th>PICK-UP-BY</th>
			  <th>FROM TIME</th>
			  <th>TO TIME</th>
			  
			</tr>
		  </thead>
		  <tbody>
			
			
		  </tbody>
		</table>
	  </div>
	  <div class="row">
		<div class="col-md-10 col-md-offset-1" style="background-color: #ddd;">
		 
		</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="workshop_modal" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1194.72*/routes/*1194.78*/.Assets.at("images/close1.png")),format.raw/*1194.109*/(""""></button>
		<h4 class="modal-title">Workshop Details</h4>
	  </div>
	  <div class="modal-body">
		<table class="table table-striped table-bordered table-hover" id="">
		  <thead>
			<tr>
			  <th>Workshop Name</th>
			  <th>Service Booked Date</th>
			</tr>
		  </thead>
		  <tbody>
		  </tbody>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>


<!-- Address Popup Model -->
<div class="modal fade" id="AddNewAddressPopup" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1224.72*/routes/*1224.78*/.Assets.at("images/close1.png")),format.raw/*1224.109*/(""""></button>
  <h4 class="modal-title"><b style="text-align:center; color:Red;">Add New Address</b></h4>
</div>
<div class="modal-body">
 
  <div class="row" id="AddAddressDiv">
   <div class="col-md-12">
<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>Add Address1<b></label>
<textarea class="form-control" rows="1" id="AddAddress1All" name="address1New" ></textarea>
  </div>
</div>
<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>Add Address2<b></label>
<textarea class="form-control" rows="1" id="AddAddress2All" name="address2New" ></textarea>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>State<b></label>
<select class="form-control" name="stateNew" id="AddAddrMMSState" onchange="getCityByStateSelection('AddAddrMMSState','cityInputPopup3');">
  <option value="0">--SELECT--</option>
"""),_display_(/*1249.2*/for(states <- statesList) yield /*1249.27*/{_display_(Seq[Any](format.raw/*1249.28*/("""
           
          """),format.raw/*1251.11*/("""<option value=""""),_display_(/*1251.27*/states),format.raw/*1251.33*/("""">"""),_display_(/*1251.36*/states),format.raw/*1251.42*/("""</option>
                              
  """)))}),format.raw/*1253.4*/("""
"""),format.raw/*1254.1*/("""</select>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>City<b></label>
<select class="form-control" name="cityNew" placeholder="City" id="cityInputPopup3">	
                                        
                                    </select>
  </div>
</div>
<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>PinCode<b></label>
<input type="text" class="form-control numberOnly" name="pincodeNew" value="0" id="PinCode1MMS" maxlength="6" />
  </div>
</div>

</div>
   
  </div>
  

<div class="modal-footer">
  <button type="button" class="btn btn-success" id="AddAddrMMSSave" data-dismiss="modal">Save</button>
</div>
</div>

</div>
</div>
</div>  <!-- end popup -->  <!-- end popup -->


<!---bottom fixed popup----->
<div class="modal fade" id="bottonFixedId" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1295.73*/routes/*1295.79*/.Assets.at("images/close1.png")),format.raw/*1295.110*/(""""></button>
	  <h4 class="modal-title">Call Script</h4>
	</div>
	<div class="modal-body">
	  <p>Hi Good Morning!. </p>
	</div>
	<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
  </div>
  
</div>
</div>



<div class="modal fade" id="offer" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color:#1797be;color:#ffffff;text-align:center">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1317.80*/routes/*1317.86*/.Assets.at("images/close1.png")),format.raw/*1317.117*/(""""></button>
          <h4 class="modal-title">Select Offer For """),_display_(/*1318.53*/if(vehicleData!=null)/*1318.74*/{_display_(Seq[Any](format.raw/*1318.75*/("""
		"""),format.raw/*1319.3*/("""<b>  """),_display_(/*1319.9*/{vehicleData.getVehicleRegNo()}),format.raw/*1319.40*/("""</b>
		  """)))}),format.raw/*1320.6*/(""" """),format.raw/*1320.7*/("""</h4>
        </div>
        <div class="modal-body ofeerData">
          <table class="table table-striped">
    <thead class="makefixed">
      <tr>
	  <th>#</th>
        <th>OFFER&nbsp;CODE</th>
        <th>OFFER&nbsp;DETAIL</th>
        <th>CONDITION</th>
		<th>BENEFIT</th>
        <th>VALIDITY</th>
      </tr>
    </thead>
   <tbody>
     """),_display_(/*1335.7*/for(offer_list <- offersList) yield /*1335.36*/{_display_(Seq[Any](format.raw/*1335.37*/("""
           """),format.raw/*1336.12*/("""<tr id="row12">
        <td><input type="radio" class="case" name="case[]" value=""""),_display_(/*1337.68*/offer_list/*1337.78*/.id),format.raw/*1337.81*/("""" ></td>
		 <td>"""),_display_(/*1338.9*/offer_list/*1338.19*/.offerCode),format.raw/*1338.29*/("""</td>
        <td>"""),_display_(/*1339.14*/offer_list/*1339.24*/.offerDetails),format.raw/*1339.37*/("""</td>
        <td>"""),_display_(/*1340.14*/offer_list/*1340.24*/.offerConditions),format.raw/*1340.40*/("""</td>
		  <td>"""),_display_(/*1341.10*/offer_list/*1341.20*/.offerBenefit),format.raw/*1341.33*/("""</td>
        <td>"""),_display_(/*1342.14*/offer_list/*1342.24*/.offerValidity),format.raw/*1342.38*/("""</td>
      </tr>
	  """)))}),format.raw/*1344.5*/("""
    """),format.raw/*1345.5*/("""</tbody>
  </table>
      </div>
        <div class="modal-footer" style="padding: 7px;">
          <button type="button" class="btn btn-success btn-xs" id="applyOffers">Apply&nbsp;Offer</button>
        </div>
		<div id="offerSelected">
		<b style="margin-left:6px;">OFFER SELECTED</b>
<div class="modal-footer" style="border: 2px solid #1797be;margin-left: 6px;margin-right: 5px;">
<div class="col-sm-10 pull-left">
<span id="selectChkVal" style="text-align:left"></span>
<input type="hidden" id="offerSelectedId" name="offerSelId" value=0>

</div>
<div class="col-sm-2 pull-right">
          <button type="button" class="btn btn-danger btn-xs removeOffer" >REMOVE OFFER</button>
		  
		  </div>
		  
		 
        </div>
		
		</br>
      </div>
	  <div style="text-align:center">
	   <button type="button" class="btn btn-success saveOffer" data-dismiss="modal" style="margin-top:-24px;">SAVE</button><br />
	   </div>
      </div>
    </div>
  </div>
  
  
  <div class="modal fade" id="DNDConfirm" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1383.80*/routes/*1383.86*/.Assets.at("images/close1.png")),format.raw/*1383.117*/(""""></button>
          <h4 class="modal-title" style="text-align:center;">Do Not Disturb(DND) Tagging</h4>
        </div>
        <div class="modal-body">
          <b>Do you want to tag the customer """),_display_(/*1387.47*/customerData/*1387.59*/.getCustomerName()),format.raw/*1387.77*/("""  """),format.raw/*1387.79*/("""as �Do Not Disturb /Call� ?<br> Click �YES� to confirm</b><br><br>
		  
  <div class="radio-inline">
      <label><input type="radio" name="RadioDND" id="RadioDNDYes" value="makeDND">YES</label>
    </div>
    <div class="radio-inline">
      <label><input type="radio" name="RadioDND" id="RadioDNDNo" value="removeDND">NO</label>
    </div>
        </div>
        <div class="modal-footer" style="text-align:center;" >
         
<button type="button" class="btn btn-success" id="submitDndBtn" data-dismiss="modal"
>submit</button>		  
        </div>
      </div>
      
    </div>
  </div>
  
     <div class="modal fade" id="smsPopuId" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="text-align:center;background-color: #106A86;color: #fff;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1412.80*/routes/*1412.86*/.Assets.at("images/close1.png")),format.raw/*1412.117*/(""""></button>
          <h4 class="modal-title" ><b>SMS OPTIONS<b></h4>
        </div>
        <div class="">
   <ul class="nav nav-pills nav-stacked col-md-3">
   """),_display_(/*1417.5*/for(smsTemplate<-smsTemplates) yield /*1417.35*/{_display_(Seq[Any](format.raw/*1417.36*/("""
   		"""),format.raw/*1418.6*/("""<li><a class="clssTemplType" id="typAnch_"""),_display_(/*1418.48*/{smsTemplate.getSmsId()}),format.raw/*1418.72*/("""" href="#tab_"""),_display_(/*1418.86*/{smsTemplate.getSmsId()}),format.raw/*1418.110*/("""" data-toggle="pill">"""),_display_(/*1418.132*/{smsTemplate.getSmsType()}),format.raw/*1418.158*/("""</a></li>
   	""")))}),format.raw/*1419.6*/("""
 
"""),format.raw/*1421.1*/("""</ul>
<div class="tab-content col-md-9 smstabclass">
<input type="hidden" id="smsrequestReference" value=""""),_display_(/*1423.55*/{vehicleData.getVehicle_id()}),format.raw/*1423.84*/("""" />
  """),_display_(/*1424.4*/for(smsTemplate<-smsTemplates) yield /*1424.34*/{_display_(Seq[Any](format.raw/*1424.35*/("""
  	

        """),format.raw/*1427.9*/("""<div class="tab-pane """),_display_(/*1427.31*/if(smsTemplate.getSmsId()==1)/*1427.60*/{_display_(Seq[Any](format.raw/*1427.61*/("""active""")))}),format.raw/*1427.68*/("""" id="tab_"""),_display_(/*1427.79*/{smsTemplate.getSmsId()}),format.raw/*1427.103*/("""">
        <input type="hidden" id="typ_"""),_display_(/*1428.39*/{smsTemplate.getSmsId()}),format.raw/*1428.63*/(""""  />
             <h4><u>"""),_display_(/*1429.22*/{smsTemplate.getSmsType()}),format.raw/*1429.48*/("""</u></h4>
			 <textarea name="text" rows="5" cols="45" id="txt_"""),_display_(/*1430.55*/{smsTemplate.getSmsId()}),format.raw/*1430.79*/("""" class="cl_"""),_display_(/*1430.92*/{smsTemplate.getSmsId()}),format.raw/*1430.116*/("""" disabled="disabled" style="border:none; background-color:#fff;">"""),_display_(/*1430.183*/{smsTemplate.getSmsTemplate()}),format.raw/*1430.213*/("""</textarea>
<br/>
		<input type="button" name="set_Value" id="ed_"""),_display_(/*1432.49*/{smsTemplate.getSmsId()}),format.raw/*1432.73*/("""" class="btn btn-info cmmnbtnclass"  value="Edit"  >
     

        </div>
 
 """)))}),format.raw/*1437.3*/("""
 """),format.raw/*1438.2*/("""</div>	
<!-- tab content -->
        </div>
        <div class="modal-footer">
          <button type="button" id="smsSendbtn" class="btn btn-success" data-dismiss="modal">SEND SMS</button>
        </div>
      </div>
    </div>
  </div>
 

  <!-- Modal -->
  
  
  <div class="modal fade" id="myModalAddPhone" role="dialog">
    <div class="modal-dialog modal-xs">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1458.80*/routes/*1458.86*/.Assets.at("images/close1.png")),format.raw/*1458.117*/(""""></button>
          <h4 class="modal-title">ADD PHONE NUMBER</h4>
        </div>
		
		
        <div class="modal-body">
		<div class="row">
        <div class="col-xs-12">
        <div class="col-xs-4">
        <input type="text" name="myPhoneNum" class="form-control numberOnly" maxlength="10" min="10" max="10" id="myPhoneNum" >
      </div>
        </div>
        </div>
		</div>
        <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal" id="savePhoneCust">SAVE</button>
        </div>
      </div>
      
    </div>
  </div>
   <!--------------------------------Side bar popup start------------------------------------------>
  <div id="fixedsocial">
    <div class="facebookflat" data-toggle="modal" data-target="#myModal1" >
	<!-- <img src='"""),_display_(/*1482.18*/routes/*1482.24*/.Assets.at("images/car.png")),format.raw/*1482.52*/("""' style="width: 50px;" /> -->
	<img src='"""),_display_(/*1483.13*/routes/*1483.19*/.Assets.at("images/sports-car.jpg")),format.raw/*1483.54*/("""' style="width: 50px; border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Vehicle" />
	</div>
    <div class="twitterflat" data-toggle="modal" data-target="#myModal2" >
	<img src='"""),_display_(/*1486.13*/routes/*1486.19*/.Assets.at("images/service.jpg")),format.raw/*1486.51*/("""' style="width: 50px;border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Service" onclick="ajaxCallServiceLoadOfCustomer();"/>
	<!-- <img src='"""),_display_(/*1487.18*/routes/*1487.24*/.Assets.at("images/car-insurance.png")),format.raw/*1487.62*/("""' style="width: 50px;" /> -->
	</div> 
     <div class="twitterflat1" data-toggle="modal" data-target="#myModal3" >
	 <img src='"""),_display_(/*1490.14*/routes/*1490.20*/.Assets.at("images/car-insu.jpg")),format.raw/*1490.53*/("""' style="width: 50px; border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Insurance"/> 
	 <!-- <img src='"""),_display_(/*1491.19*/routes/*1491.25*/.Assets.at("images/INSURANCE1.png")),format.raw/*1491.60*/("""' style="width: 50px;" /> --> 
	 </div> 
      
</div>
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1501.80*/routes/*1501.86*/.Assets.at("images/close1.png")),format.raw/*1501.117*/(""""></button>
          <h4 class="modal-title">VEHICLE INFORMATION</h4>
        </div>
        <div class="modal-body"  style="padding: 5px;">
		<div class="table table-responsive">
          <table id="example700" class = "table table-bordered">
 
  <thead class="makefixed">
				   <tr>
				  <th >Model</th>
			   <th>Variant</th>
				<th >Color</th>
			   <th >Registration&nbsp;No</th>
				<th>Engine&nbsp;No</th>
				<th >Chassis&nbsp;No</th>
				  <th >Sale&nbsp;Date</th>
				<th >Fuel&nbsp;Type</th>
				</tr>
			  </thead>
			  <tbody>
			   
			"""),_display_(/*1522.5*/for(post <- customerData.getVehicles()) yield /*1522.44*/ {_display_(Seq[Any](format.raw/*1522.46*/("""
			"""),format.raw/*1523.4*/("""<tr>
			  <td><input type="text-primary" id="model" value=""""),_display_(/*1524.56*/post/*1524.60*/.getModel()),format.raw/*1524.71*/(""""  style="border:none;text-align:center" readonly></td>
			  <td><input type="text-primary" id="variant" value=""""),_display_(/*1525.58*/post/*1525.62*/.getVariant()),format.raw/*1525.75*/("""" style="border:none;text-align:center" readonly></td>
			  <td><input type="text-primary" id="color" value=""""),_display_(/*1526.56*/post/*1526.60*/.getColor()),format.raw/*1526.71*/("""" style="border:none;text-align:center" readonly></td>
			  <td>
			  <div class="col-sm-12">
			  <div class="col-sm-8">
			  <input type="text-primary" class="vehicalRegNo" id="vehicalRegNo" value=""""),_display_(/*1530.80*/post/*1530.84*/.getVehicleRegNo()),format.raw/*1530.102*/("""" style="border:none;margin-right: 15px;" readonly>
				</p>
				</div>
				<div class="col-sm-2">
				  <!-- <input type="button" value="+"  id="editRegistrationno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);"> -->
				  
				  <input type="button" value="+"  id="" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				 </div>
				 <div class="col-sm-2">
				  <!-- <input type="button" class="btn btn-success btn-xs" value="Add" id="updateRegistrationno" onmouseenter="ajaxAddRegistrationno();" style="display:none;"> -->
				  
				</div>
			  </div></td>
			  <td> 
			  <div class="col-sm-12">
			   <div class="col-sm-8">
			  <input type="text-primary" id="engineNo" value=""""),_display_(/*1546.55*/post/*1546.59*/.getEngineNo()),format.raw/*1546.73*/("""" style="border:none;margin-right: 15px;" readonly>
				</p>
				  </div>
			<!-- <div class="col-sm-2">
				  <input type="button" value="+" id="editEngineno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				</div>  <div class="col-sm-2">
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateEngineno" onmouseenter="ajaxAddEngineno();" style="display:none;">
				</div>-->
				</div></td>
			  <td><div class="col-sm-12 ">
			   <div class="col-sm-8">
			  <input type="text-primary" id="chassisNo" value=""""),_display_(/*1557.56*/post/*1557.60*/.getChassisNo()),format.raw/*1557.75*/("""" style="border:none;margin-right: 15px;" readonly>
				 </div>
			<!--	<div class="col-sm-2">
				  <input type="button" value="+" id="editChassisno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				  </div>  
				  <div class="col-sm-2">  
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateChassisno" onmouseenter="ajaxAddChassisno();" style="display:none;">
				</div>-->
				</div></td>
			    <td><input type="text-primary" id="saleDate" value=""""),_display_(/*1566.61*/post/*1566.65*/.getSaleDateStr()),format.raw/*1566.82*/("""" style="border:none;text-align:center" readonly></td>
			  <td><input type="text-primary" id="fuelType" value=""""),_display_(/*1567.59*/post/*1567.63*/.getFuelType()),format.raw/*1567.77*/("""" style="border:none;text-align:center" readonly></td>
			</tr>			""")))}),format.raw/*1568.13*/("""
				"""),format.raw/*1569.5*/("""</tbody>
</table>
</div>
        </div>
     
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-lg" style="width:1200px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1586.80*/routes/*1586.86*/.Assets.at("images/close1.png")),format.raw/*1586.117*/(""""></button>
          <h4 class="modal-title">SERVICE INFORMATION</h4>
        </div>
        <div class="modal-body"  style="padding: 5px;">
		<div class="table table-responsive">
          <table id="example800" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
 
   <thead class="makefixed">
                <tr>
						<th>Repair Order&nbsp;Date</th>
						<th>Repair Order&nbsp;No.</th>
						<th>Bill&nbsp;Date</th>
						<th>Kilometer</th>
						<th>Model</th>
						<th>Registration No.</th>
						<th>Service Type</th>
						<th>Menu Code Description</th>
						<th>Service Advisor Name</th>
						<th>Customer Name</th>
						<th>Group (Category)</th>
						<th>Part Amount</th>
						<th>Labour Amount</th>
						<th>Total Amount</th> 
					</tr>
                  </thead>
				   <tbody>
                                   
                    </tbody>
			  
</table>

</div>
        </div>
     
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog modal-lg" style="width:1200px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1632.80*/routes/*1632.86*/.Assets.at("images/close1.png")),format.raw/*1632.117*/(""""></button>
          <h4 class="modal-title">INSURANCE INFORMATION</h4>
        </div>
        <div class="modal-body"  style="padding: 5px;">
		<div class="table table-responsive">
          <table id="" class = "table table-bordered">
	 
	   <thead class="makefixed">
					  <tr>
					<th>Policy Issue Date</th>
					<th>Policy Number</th>
					<th>Policy type</th>
					<th>Class</th>
					<th>Add-On</th>
					<th>Due Date</th>
					<th>Gross Premium</th>
					<th>IDV</th>
					<th>OD(%)</th>
					<th>Basic OD</th>
					<th>NCB(%)</th>
					<th>NCB Value</th>
					<th>Discount</th>
					<th>OD Premium</th>
					<th>TP Premium</th>
					<th>PA Premium</th>
					<th>Legal Liability</th>
					<th>Net Liability</th>
					<th>Add-On Premium</th>
					<th>Other Premium</th>
					<th>Net Premium</th>
					<th>Tax</th>
					<!--<th>Gross Premium</th>
					<th>Coverage&nbsp;Period</th>
					<th>Policy&nbsp;Due&nbsp;Date</th>
					<th>Insurance&nbsp;Company</th>
					<th>NCB(%age)</th>
					<th>NCB&nbsp;Amount(Rs.)</th>
					<th>PREMIUM</th>
					<th>Service&nbsp;Tax</th>
					<th>Total&nbsp;Premium</th>
					<th>Last&nbsp;Renewal&nbsp;By</th>-->
					
					</tr>
				  </thead>
				<tbody>
				"""),_display_(/*1677.6*/for(insuranceData <-customerData.getInsurances()) yield /*1677.55*/{_display_(Seq[Any](format.raw/*1677.56*/("""
				 
				  """),format.raw/*1679.7*/("""<tr>
				  <!-- <td><a data-toggle="modal" data-target="#otherdetails" ><i class="fa fa-info-circle" data-toggel="tooltip" title="Other Details" style="font-size:30px;color:red;"></i></a></td> -->
					<td>"""),_display_(/*1681.11*/insuranceData/*1681.24*/.getPolicyDueDateStr()),format.raw/*1681.46*/("""</td>
					<td>"""),_display_(/*1682.11*/insuranceData/*1682.24*/.getPolicyNo()),format.raw/*1682.38*/("""</td>
					<td>"""),_display_(/*1683.11*/insuranceData/*1683.24*/.getInsuranceCompanyName()),format.raw/*1683.50*/("""</td>
					<td>"""),_display_(/*1684.11*/insuranceData/*1684.24*/.getPolicyType()),format.raw/*1684.40*/("""</td>
					<td>"""),_display_(/*1685.11*/insuranceData/*1685.24*/.getClassType()),format.raw/*1685.39*/("""</td>
					<td>"""),_display_(/*1686.11*/insuranceData/*1686.24*/.getAddOn()),format.raw/*1686.35*/("""</td>
					<td>"""),_display_(/*1687.11*/insuranceData/*1687.24*/.getPolicyDueDate()),format.raw/*1687.43*/("""</td>
					<td>"""),_display_(/*1688.11*/insuranceData/*1688.24*/.getGrossPremium()),format.raw/*1688.42*/("""</td>
					<td>"""),_display_(/*1689.11*/insuranceData/*1689.24*/.getIdv()),format.raw/*1689.33*/("""</td>
					<td>"""),_display_(/*1690.11*/insuranceData/*1690.24*/.getOdPercentage()),format.raw/*1690.42*/("""</td>
					 <td>"""),_display_(/*1691.12*/insuranceData/*1691.25*/.getNcBPercentage()),format.raw/*1691.44*/("""</td>
					<td>"""),_display_(/*1692.11*/insuranceData/*1692.24*/.getNcBAmountStr()),format.raw/*1692.42*/("""</td>
					<td>"""),_display_(/*1693.11*/insuranceData/*1693.24*/.getDiscountPercentage()),format.raw/*1693.48*/("""</td>
					<td>"""),_display_(/*1694.11*/insuranceData/*1694.24*/.getODpremium()),format.raw/*1694.39*/("""</td>
					<td>"""),_display_(/*1695.11*/insuranceData/*1695.24*/.getaPPremium()),format.raw/*1695.39*/("""</td>
					<td>"""),_display_(/*1696.11*/insuranceData/*1696.24*/.getpAPremium()),format.raw/*1696.39*/("""</td>
					<td>"""),_display_(/*1697.11*/insuranceData/*1697.24*/.getLegalLiability()),format.raw/*1697.44*/("""</td>
					<td>"""),_display_(/*1698.11*/insuranceData/*1698.24*/.getNetLiability()),format.raw/*1698.42*/("""</td>
					<td>"""),_display_(/*1699.11*/insuranceData/*1699.24*/.getAdd_ON_Premium()),format.raw/*1699.44*/("""</td>
					<td>"""),_display_(/*1700.11*/insuranceData/*1700.24*/.getOtherPremium()),format.raw/*1700.42*/("""</td>
					<td>"""),_display_(/*1701.11*/insuranceData/*1701.24*/.getPremiumAmountStr()),format.raw/*1701.46*/("""</td>
					<td>"""),_display_(/*1702.11*/insuranceData/*1702.24*/.getServiceTax()),format.raw/*1702.40*/("""</td>
				  </tr>
				  """)))}),format.raw/*1704.8*/("""
					"""),format.raw/*1705.6*/("""</tbody> 
	</table>
        </div>
		</div>
       
      </div>
      
    </div>
  </div>
  <!------------Service Popup Model---------->
  <div class="modal fade" id="myModalForInfo" role="dialog">
    <div class="modal-dialog modal-lg" style="width:1200px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color:#1797BE;text-align:center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1721.80*/routes/*1721.86*/.Assets.at("images/close1.png")),format.raw/*1721.117*/(""""></button>
          <h4 class="modal-title heading" style="color:#fff"><b>SERVICE HISTORY -<<VEH No.>> - <<SERVICE DATE>><b> </h4>
        </div>
        <div class="modal-body">
         

   <div class="row" style="border:2px solid #1797BE;margin-right: -8px;margin-left: -6px;">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>CUSTOMER ID</strong></td>
                      <td class="text-primary"> 1311935638 </td>
                    </tr>
					
					<tr>
                      <td><strong>S/ADVISOR</strong></td>
                      <td class="text-primary"> </td>
                    </tr>
					<tr>
                      <td><strong>MILEAGE (Kms)</strong></td>
                      <td class="text-primary">34233</td>
                    </tr>
					  
					 <tr>
                      <td><strong>PART DISC</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                   <tr>
                      <td><strong>PARTS BILL</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                 
					<tr>
                      <td><strong> JOBCARD No.</strong></td>
                      <td class="text-primary">JC13010045</td>
                    </tr>
				
					<tr>
                      <td><strong>TECHNICIAN</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					<tr>
                      <td><strong>Bill No.</strong></td>
                      <td class="text-primary">BR13010474</td>
                    </tr>
					 
					
					<tr>
                      <td><strong>LAB DISC</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					 <tr>
                      <td><strong>LAB BILL</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                   
                  </tbody>
                </table>
                </div>
                 <div class="col-md-4">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                
					<tr>
                      <td><strong>W/SHOP</strong></td>
                      <td class="text-primary">UNIV RD</td>
                    </tr>
					
					
					<tr>
                      <td><strong>S/TYPE</strong></td>
                      <td class="text-primary">FR3</td>
                    </tr>
					<tr>
                      <td><strong>Bill Date</strong></td>
                      <td class="text-primary">10/27/2015</td>
                    </tr>
					
					
					 <tr>
                      <td><strong>DISCOUNT</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                  
					 <tr>
                      <td><strong>BILL AMOUNT</strong></td>
                      <td class="text-primary">656577</td>
                    </tr>
                  </tbody>
                </table>
                </div>
	</div>
			<div class="row" style="border:2px solid #1797BE;margin-top: 5px;margin-right: -8px;margin-left: -6px;">
	   
		
			<h4 style="text-align:center;"><b>REMARKS<b></h4>
    			<div class="col-lg-4" style="text-align: center;">
  			       <div class="form-group">
     			    <label for="">DEFECT DETAILS</label>
     			     <textarea type="" class="form-control" rows="3" id=""  readonly></textarea>
 			     </div>
		 </div> 
  		    <div class="col-lg-4" style="    text-align: center;">
  			<div class="form-group">
     			 <label for="">LABOUR DETAILS</label>
     			 <textarea type="" class="form-control" id=""  rows="3" readonly></textarea>
 			 </div>
		 </div>
       
  		<div class="col-lg-4" style="    text-align: center;">
  			<div class="form-group">
     			 <label for="">JC REMARKS</label>
     			  <textarea type="" class="form-control" id=""  rows="3" readonly></textarea>
 			 </div>
		 </div>
		 
 

</div>

<div class="row" style="border:2px solid #1797BE;    margin-right: -8px;
    margin-left: -6px;    margin-top: 5px;">
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>3rd DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4" >
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
				
				
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>6th DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
				
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>30th DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
</div>
        </div>
      
      </div>
      
    </div>
  </div>
	 <!------------------Calculator modal Popup------------------------->
   <!------------------Calculator modal Popup------------------------->
  <div class="modal fade" id="InsuPremiumPopup" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 10px; color: red">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1992.72*/routes/*1992.78*/.Assets.at("images/close1.png")),format.raw/*1992.109*/(""""></button>
         
          <p class="modal-title" style="text-align:center;"><b>INSURANCE PREMIUM CALCULATOR</b></p>
        </div>
        <div class="modal-body" style="padding-bottom: 0px;">
       
        
        <b style="color:red;"><u>VEHICLE DETAILS:</u></b>
         <div class="row">
      
      <div class="col-xs-2">
       <div class="form-group">
        <label>Cubic&nbsp;Capacity(CC)</label>
         <select class="form-control input-sm" name="cubicCapacity" id="cubicCapacityId" onchange="ajaxODPercentage();">
		 <option value="1001 -1500 CC">1001 -1500 CC</option>
          <option value="<=1000 CC"><=1000 CC</option>
          <option value=">=1501 CC"> >=1501 CC</option>
        </select>
      </div>
      </div>
       <div class="col-xs-2">
        <div class="form-group">
        <label>Vehicle&nbsp;Age</label>
         <select class="form-control input-sm" name="vehicleAge" id="vehicleAgeId" onchange="ajaxODPercentage();">
          <option value="<= 5 Years"><= 5 Years</option>
          <option value="5-10 Years">5-10 Years</option>
          <option value=">10 Years"> >10 Years </option>
        </select>
      </div>
      </div>
      <div class="col-xs-2">
       <div class="form-group">
        <label>City</label>
         <select class="form-control input-sm" name="vehicleCity">
          <option value="Rest of India">Rest of India</option>
          <option value="Ahmedabad">Ahmedabad</option>
          <option value="Bangalore">Bangalore</option>
          <option value="Chennai">Chennai</option>
          <option value="Hyderabad">Hyderabad</option>
          <option value="Kolkata">Kolkata</option>
          <option value="Mumbai">Mumbai</option>
          <option value="New Delhi">New Delhi</option>
          <option value="Pune">Pune</option>
             
        </select>
      </div>
      </div>
      <div class="col-xs-2">
       <div class="form-group">
        <label>Zone</label>
         <select class="form-control input-sm" name="vehicleZone" id="zoneId" onchange="ajaxODPercentage();">
		 <option value="Zone B">Zone B</option>
         <option value="Zone A">Zone A</option>
           </select>
         </div>
         </div>

      <div class="col-xs-2">
       <div class="form-group">
        <label>Ex&nbsp;Showroom&nbsp;Price</label>
         <input type="text" class="form-control input-sm numberOnly" value="0" name="exShowroomPrice">
         </div>
         </div>
      
        </div>
         <b style="color:red;"><u>OWN DAMAGE PREMIUM (I):</u></b>
<div class="row">
         <div class="col-xs-2">
      <div class="form-group">
        <label>IDV</label>
        <input class="form-control input-sm numberOnly" id="idvId" value=""""),_display_(/*2062.76*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getIdv()}else{0}}else{0}}),format.raw/*2062.241*/("""" name="idv" type="text">
     </div>
     </div>
       <div class="col-xs-2">
       <div class="form-group">
        <label>OD(%)</label>
         <input class="form-control input-sm" id="odId" name="odPercentage" value=""""),_display_(/*2068.85*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getOdPercentage()}else{0}}else{0}}),format.raw/*2068.259*/("""" type="text" readonly>
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>Basic OD</label>
         <input class="form-control input-sm" id="basicODId" name="odAmount" value=""""),_display_(/*2074.86*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getOdAmount()}else{0}}else{0}}),format.raw/*2074.256*/("""" type="text" readonly>
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>NCB(%)</label>
         <input class="form-control input-sm" id="ncbPercenId" name="ncBPercentage" value=""""),_display_(/*2080.93*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getNcBPercentage()}else{0}}else{0}}),format.raw/*2080.268*/("""" type="text">
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>NCB Value</label>
         <input class="form-control input-sm" id="ncbValueId"  name="ncBAmount" value=""""),_display_(/*2086.89*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getNcBAmount()}else{0}}else{0}}),format.raw/*2086.260*/("""" type="text" readonly>
      </div>
      </div>
	  </div>
 <div class="row">
      <div class="col-xs-2">
      <div class="form-group">
        <label>OD Premium</label>
         <input class="form-control input-sm" id="odPremiumId" name="ODpremium" value=""""),_display_(/*2094.89*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getODpremium()}else{0}}else{0}}),format.raw/*2094.260*/("""" type="text" readonly>
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>Commercial&nbsp;Disc(%)</label>
         <input class="form-control input-sm" id="commercialDiscId" value=""""),_display_(/*2100.77*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getDiscountPercentage()}else{0}}else{0}}),format.raw/*2100.257*/("""" name="discountPercentage" type="text">
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>Disc Value </label>
         <input class="form-control input-sm" id="discValueId" name="discValue" value=""""),_display_(/*2106.89*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getDiscValue()}else{0}}else{0}}),format.raw/*2106.260*/("""" type="text" readonly>
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
         <label>Total&nbsp;OD&nbsp;Premium</label>
         <input class="form-control input-sm" id="totalODPremiumId" name="totalODpremium" value=""""),_display_(/*2112.99*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getTotalODpremium()}else{0}}else{0}}),format.raw/*2112.275*/("""" type="text" readonly>
      </div>
      </div>
    
    
        </div>
        <b style="color:red;"><u>LIABILITY & BENEFIT (II):</u></b>
		<b style="color:red;margin-left: 272px;"><u>ADD ON COVERS (III):</u></b>
         <div class="row">
      
      <div class="col-xs-2">
       <div class="form-group">
        <label>3rd&nbsp;Party&nbsp;Premium</label>
         <input type="text" class="form-control input-sm" name="" id="thirdPartyPremId" readonly>
      </div>
      </div>
       <div class="col-xs-2">
        <div class="form-group">
        <label>PA&nbsp;Premium</label>
         <input type="text" class="form-control input-sm" name="" value="100" readonly>
      </div>
      </div>
      <div class="col-xs-2">
       <div class="form-group">
          <label>Total&nbsp;Premium</label>
         <input type="text" class="form-control input-sm" name="" id="totalPremiumID" readonly>
      </div>
      </div>
	  
	    <div class="col-xs-2">
       <div class="form-group">
        <label>Add&nbsp;On&nbsp;Cover(%)</label>
         <input type="text" class="form-control input-sm" value=""""),_display_(/*2144.67*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getAdd_ON_CoverPercentage()}else{0}}else{0}}),format.raw/*2144.251*/("""" name="add_ON_CoverPercentage" id="add_ON_CoverPercentageId" >
      </div>
      </div>
       <div class="col-xs-2">
        <div class="form-group">
        <label>Add&nbsp;On&nbsp;Premium </label>
         <input type="text" class="form-control input-sm" name="add_ON_Premium" value=""""),_display_(/*2150.89*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getAdd_ON_Premium()}else{0}}else{0}}),format.raw/*2150.265*/("""" id="addOnPremiumId">
      </div>
      </div>
      
      
        </div>
        
	    <b style="color:red;"><u>PACKAGE PREMIUM (I + II + III):</u></b>
         <div class="row">
     
      <div class="col-xs-2">
        <div class="form-group">
         <label>Total&nbsp;Pkg&nbsp;Premium</label>
         <input type="text" class="form-control input-sm" value=""""),_display_(/*2163.67*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getPremiumAmountBeforeTax()}else{0}}else{0}}),format.raw/*2163.251*/("""" name="premiumAmountBeforeTax" id="totalPackagePremiumId" readonly>
      </div>
      </div>
      <div class="col-xs-2">
        <div class="form-group">
        <label>Service&nbsp;Tax(%)</label>
         <input type="text" class="form-control input-sm" name="serviceTax" value="15" readonly>
      </div>
      </div>
      <div class="col-xs-2">
        <div class="form-group">
        <label>Final Premium</label>
         <input type="text" class="form-control input-sm" value=""""),_display_(/*2175.67*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getPremiumAmountAfterTax()}else{0}}else{0}}),format.raw/*2175.250*/("""" name="premiumAmountAfterTax" id="finalPremiumId" readonly style="background-color: #8BC34A; color:#fff; font-size: 18px;">
      </div>
      </div>
       </div>
</div>
      
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="saveFinalPremium">Save</button>
        </div>
      </div>
      </div>
      
    </div>
	
  <!--------------------------------Side bar popup end------------------------------------------>
   <!--Popup For Driver Allocation-->
<div class="modal fade" id="DriverAllcationPOPUp" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
	   <div class="modal-header" style="text-align:center">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Driver Allocation</h4>																													
        </div>
             <div class="modal-body" style="padding:4px;">
  <div class="panel panel-primary" >
	<div class="panel-heading" style="padding:0px;"> </div>
	<div class="panel-body" style="height: 500px; overflow: auto; ">
			<div class="col-md-12">
			
			<div class="form-inline">
  <div class="form-group">
<label for="comments"><b>CHANGE SERVICE BOOKING DATE :<b></label>
<input type="text" class="form-control datepickerchange" id="changeserviceBookingDate" onchange="ajaxAssignBtnBkreview('workshop','date12345');"   readonly>
  </div>
  </div>
<br />
</div>

              <div class="col-md-6">
                <table class="table table-responsive">
                  <tbody>
                    <tr>
                      <td><strong> New Booking Date: </strong></td>
                      <td class="text-primary" id="newbookingdate"></td>
                    </tr>
                     <tr>
                      <td><strong> New Driver Name:</strong></td>
                      <td class="text-primary" id="newDriver"></td>
                    </tr>
					<tr>
                      <td><strong>New Time Slot:</strong></td>
                      <td class="text-primary" id="newTimeSlot"></td>
                    </tr>
                  </tbody>
                </table>
                </div>
              
	 <div>
        <input type="hidden" id="tempValue" value="0">
        <input type="hidden" id="tempIncreValue" value="0">
        <input type="hidden" name="time_From" id="startValue" value="0">
        <input type="hidden" name="time_To" id="endValue" value="0">
        <input type="hidden" name="driverId" id="driverValue" value="0">
      </div>
  <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="tableID"  style="cursor: pointer;">
    <thead>  
        
    </thead>
    <tbody> 
           
    </tbody>
  </table>
  <button type="button" id="allcateSubmit" class="btn btn-primary pull-right" data-dismiss="modal">Submit</button>
</div>
</div>
 </div>
       </div>
      
    </div>
  </div>
  <!-- Hidden div with details for Auto selection of SA  -->
  <div style="display:none">
  	<input type="hidden" id="preSaDetails"  />
					<input type="hidden" id="newSaDetails"  />
					<select class="form-control" id="serviceAdvisorTemp"  name="serviceAdvisorTemp" style="display:none">
						<option value="0">--Select--</option>
					</select>
  </div>
  
  <div style="display:none">
  	<input type="hidden" id="preSaDetailsIns"  />
					<input type="hidden" id="newSaDetailsIns"  />
					<select class="form-control" id="serviceAdvisorTempIns"  name="serviceAdvisorTempIns" style="display:none">
						<option value="0">--Select--</option>
					</select>
  </div>

""")))}),format.raw/*2274.2*/("""
"""),format.raw/*2275.1*/("""<script src=""""),_display_(/*2275.15*/routes/*2275.21*/.Assets.at("javascripts/commondisposition.js")),format.raw/*2275.67*/("""" type="text/javascript"></script>
""")))}),format.raw/*2276.2*/("""
"""))
      }
    }
  }

  def render(oem:String,statesList:List[String],latestinsurance:Insurance,uniqueid:Long,dealercode:String,dealerName:String,userName:String,customerData:Customer,vehicleData:Vehicle,locationList:List[Location],userData:WyzUser,latestService:Service,smsTemplates:List[SMSTemplate],complaintOFCust:List[String],servicetypeList:List[ServiceTypes],interOfCall:CallInteraction,offersList:List[SpecialOfferMaster],typeDispo:String,dispoOut:Html,dispoInBound:Html): play.twirl.api.HtmlFormat.Appendable = apply(oem,statesList,latestinsurance,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,locationList,userData,latestService,smsTemplates,complaintOFCust,servicetypeList,interOfCall,offersList,typeDispo,dispoOut,dispoInBound)

  def f:((String,List[String],Insurance,Long,String,String,String,Customer,Vehicle,List[Location],WyzUser,Service,List[SMSTemplate],List[String],List[ServiceTypes],CallInteraction,List[SpecialOfferMaster],String,Html,Html) => play.twirl.api.HtmlFormat.Appendable) = (oem,statesList,latestinsurance,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,locationList,userData,latestService,smsTemplates,complaintOFCust,servicetypeList,interOfCall,offersList,typeDispo,dispoOut,dispoInBound) => apply(oem,statesList,latestinsurance,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,locationList,userData,latestService,smsTemplates,complaintOFCust,servicetypeList,interOfCall,offersList,typeDispo,dispoOut,dispoInBound)

  def ref: this.type = this

}


}

/**/
object commonDispositionPage extends commonDispositionPage_Scope0.commonDispositionPage
              /*
                  -- GENERATED --
                  DATE: Wed Jan 10 12:18:12 IST 2018
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/commonDispositionPage.scala.html
                  HASH: f89aac6210a35a49b509a2a3064544c588f686f1
                  MATRIX: 983->1|1524->446|1552->449|1623->512|1662->514|1689->515|1736->535|1764->536|1793->539|1844->564|1871->565|1901->569|1937->578|1965->579|1995->582|2045->605|2073->606|2101->607|2138->618|2152->624|2233->696|2272->697|2300->698|2357->728|2372->734|2418->759|2490->804|2542->835|2653->919|2683->928|2766->984|2885->1081|2980->1149|3023->1171|3116->1237|3155->1255|3247->1320|3297->1349|3383->1408|3412->1416|3494->1471|3592->1547|3723->1651|3780->1687|3865->1745|3907->1766|3983->1815|4087->1897|4187->1970|4304->2065|4383->2117|4504->2216|4585->2270|4698->2361|4776->2412|4893->2507|4979->2566|5092->2657|5175->2713|5289->2805|5591->3080|5807->3275|5836->3276|6010->3423|6138->3530|6411->3776|6484->3827|6793->4109|6852->4152|6891->4153|6941->4176|7352->4566|7394->4577|7438->4593|7720->4848|7755->4874|7794->4875|7834->4887|8115->5137|8143->5138|8742->5710|8812->5758|8958->5876|9028->5924|9060->5928|9090->5936|9122->5940|9144->5952|9174->5960|10100->6857|10252->6987|10282->6988|10457->7135|10518->7173|10645->7272|10694->7298|10969->7544|11391->7945|11420->7946|11637->8135|11700->8175|11731->8176|11764->8180|11825->8218|11946->8311|12004->8346|12216->8530|12272->8563|12492->8755|12537->8790|12577->8791|12640->8826|12705->8874|12745->8875|12791->8892|12939->9011|13016->9077|13057->9078|13103->9096|13127->9110|13159->9120|13197->9138|13211->9142|13251->9143|13297->9160|13349->9180|13379->9181|13442->9212|13492->9230|13525->9235|13892->9574|13908->9580|13959->9608|14129->9751|14160->9772|14200->9773|14245->9790|14283->9800|14333->9828|14395->9862|14449->9893|14512->9927|14559->9951|14620->9984|14668->10010|14717->10031|14763->10055|14843->10107|14908->10150|15128->10341|15181->10371|15251->10410|15286->10417|16272->11374|16332->11411|16363->11412|16413->11433|16470->11467|16552->11521|16606->11553|16694->11613|16749->11646|16832->11701|16893->11740|17154->11973|17170->11979|17231->12017|17389->12148|17420->12169|17460->12170|17489->12171|17651->12304|17675->12317|17722->12341|17773->12363|17797->12376|17841->12397|17910->12439|18020->12528|18053->12530|18083->12532|18155->12576|18178->12589|18219->12608|18508->12869|18524->12875|18576->12905|18897->13197|18959->13236|19073->13322|19139->13365|19237->13435|19290->13466|19386->13534|19452->13578|19709->13807|19725->13813|19793->13858|20174->14211|20197->14224|20238->14243|20330->14307|20353->14320|20397->14342|20668->14585|20684->14591|20742->14627|21063->14919|21089->14934|21119->14941|21310->15104|21335->15119|21364->15126|21474->15208|21499->15223|21528->15230|21630->15304|21655->15319|21684->15326|22114->15728|22130->15734|22184->15765|22602->16155|22625->16167|22666->16185|22925->16416|22948->16428|22987->16444|23701->17129|23847->17254|23880->17260|24026->17385|24059->17391|24205->17516|24681->17963|24827->18088|24860->18094|25006->18219|25039->18225|25185->18350|25620->18756|25760->18875|25793->18881|25933->19000|25966->19006|26106->19125|27151->20142|27211->20185|27251->20186|27302->20209|27609->20494|27652->20505|27697->20521|28134->20930|28180->20953|28427->21172|28486->21208|28906->21600|28966->21643|29006->21644|29057->21667|29364->21952|29407->21963|29452->21979|30268->22767|30326->22802|31319->23767|31375->23800|31911->24307|31974->24347|32237->24581|32298->24619|32578->24870|32629->24898|32661->24901|32712->24929|33315->25504|33331->25510|33385->25541|34034->26163|34068->26175|34103->26182|36601->28653|36637->28679|36677->28680|36714->28689|37113->29057|37146->29063|37224->29131|37264->29132|37304->29144|37698->29507|37727->29509|37806->29578|37846->29579|37878->29583|38392->30066|38423->30069|38668->30287|38698->30295|38739->30308|39140->30680|39157->30686|39224->30730|40079->31558|40115->31584|40155->31585|40190->31592|40355->31726|40388->31731|44086->35401|44102->35407|44156->35438|45045->36300|45087->36325|45127->36326|45179->36349|45223->36365|45251->36371|45282->36374|45310->36380|45386->36425|45422->36433|45772->36754|45901->36860|46562->37493|46578->37499|46632->37530|47592->38463|47634->38488|47674->38489|47726->38512|47770->38528|47798->38534|47829->38537|47857->38543|47933->38588|47969->38596|48301->38899|48430->39005|49093->39639|49110->39645|49165->39676|49484->39965|49613->40070|49835->40262|49964->40367|50181->40554|50310->40659|50621->40942|50664->40967|50705->40968|50758->40991|50803->41007|50832->41013|50864->41016|50893->41022|50970->41067|51007->41075|51376->41414|51500->41514|52274->42259|52291->42265|52346->42296|53315->43236|53346->43244|53937->43806|53968->43814|53999->43815|54441->44228|54458->44234|54513->44265|55337->45060|55354->45066|55409->45097|56342->46001|56359->46007|56414->46038|57180->46775|57197->46781|57252->46812|58172->47704|58215->47729|58256->47730|58309->47753|58354->47769|58383->47775|58415->47778|58444->47784|58520->47828|58550->47829|59612->48862|59629->48868|59684->48899|60317->49503|60334->49509|60389->49540|60482->49604|60514->49625|60555->49626|60587->49629|60621->49635|60675->49666|60717->49676|60747->49677|61122->50024|61169->50053|61210->50054|61252->50066|61364->50149|61385->50159|61411->50162|61456->50179|61477->50189|61510->50199|61558->50218|61579->50228|61615->50241|61663->50260|61684->50270|61723->50286|61767->50301|61788->50311|61824->50324|61872->50343|61893->50353|61930->50367|61984->50389|62018->50394|63274->51621|63291->51627|63346->51658|63575->51858|63598->51870|63639->51888|63671->51890|64630->52820|64647->52826|64702->52857|64893->53020|64941->53050|64982->53051|65017->53057|65088->53099|65135->53123|65178->53137|65226->53161|65278->53183|65328->53209|65375->53224|65407->53227|65543->53334|65595->53363|65631->53371|65679->53401|65720->53402|65763->53416|65814->53438|65854->53467|65895->53468|65935->53475|65975->53486|66023->53510|66093->53551|66140->53575|66196->53602|66245->53628|66338->53692|66385->53716|66427->53729|66475->53753|66572->53820|66626->53850|66721->53916|66768->53940|66879->54019|66910->54021|67486->54568|67503->54574|67558->54605|68384->55402|68401->55408|68452->55436|68523->55478|68540->55484|68598->55519|68828->55720|68845->55726|68900->55758|69093->55922|69110->55928|69171->55966|69329->56095|69346->56101|69402->56134|69557->56260|69574->56266|69632->56301|70032->56672|70049->56678|70104->56709|70688->57265|70745->57304|70787->57306|70820->57310|70909->57370|70924->57374|70958->57385|71100->57498|71115->57502|71151->57515|71290->57625|71305->57629|71339->57640|71569->57841|71584->57845|71626->57863|72404->58612|72419->58616|72456->58630|73056->59201|73071->59205|73109->59220|73655->59737|73670->59741|73710->59758|73852->59871|73867->59875|73904->59889|74004->59956|74038->59961|74513->60407|74530->60413|74585->60444|75966->61796|75983->61802|76038->61833|77271->63038|77338->63087|77379->63088|77421->63101|77657->63308|77681->63321|77726->63343|77771->63359|77795->63372|77832->63386|77877->63402|77901->63415|77950->63441|77995->63457|78019->63470|78058->63486|78103->63502|78127->63515|78165->63530|78210->63546|78234->63559|78268->63570|78313->63586|78337->63599|78379->63618|78424->63634|78448->63647|78489->63665|78534->63681|78558->63694|78590->63703|78635->63719|78659->63732|78700->63750|78746->63767|78770->63780|78812->63799|78857->63815|78881->63828|78922->63846|78967->63862|78991->63875|79038->63899|79083->63915|79107->63928|79145->63943|79190->63959|79214->63972|79252->63987|79297->64003|79321->64016|79359->64031|79404->64047|79428->64060|79471->64080|79516->64096|79540->64109|79581->64127|79626->64143|79650->64156|79693->64176|79738->64192|79762->64205|79803->64223|79848->64239|79872->64252|79917->64274|79962->64290|79986->64303|80025->64319|80082->64344|80117->64350|80641->64845|80658->64851|80713->64882|89601->73741|89618->73747|89673->73778|92437->76513|92626->76678|92880->76903|93078->77077|93334->77304|93528->77474|93789->77706|93988->77881|94239->78103|94434->78274|94724->78535|94919->78706|95181->78939|95385->79119|95664->79369|95859->79540|96147->79799|96347->79975|97485->81084|97693->81268|98012->81558|98212->81734|98611->82104|98819->82288|99336->82776|99543->82959|103260->86644|103290->86645|103333->86659|103350->86665|103419->86711|103487->86747
                  LINES: 27->1|32->1|34->3|34->3|34->3|35->4|36->5|36->5|37->6|38->7|38->7|40->9|40->9|40->9|41->10|42->11|42->11|43->12|45->14|45->14|45->14|45->14|46->15|46->15|46->15|46->15|47->16|47->16|48->17|48->17|49->18|49->18|50->19|50->19|51->20|51->20|52->21|52->21|53->22|53->22|54->23|54->23|57->26|57->26|59->28|59->28|61->30|61->30|62->31|62->31|63->32|63->32|64->33|64->33|65->34|65->34|66->35|66->35|67->36|67->36|79->48|87->56|87->56|89->58|93->62|99->68|99->68|101->70|101->70|101->70|103->72|111->80|112->81|113->82|116->85|116->85|116->85|117->86|118->87|119->88|126->95|126->95|127->96|127->96|127->96|127->96|127->96|127->96|127->96|138->107|140->109|140->109|143->112|143->112|144->113|144->113|149->118|157->126|157->126|162->131|162->131|162->131|162->131|162->131|163->132|163->132|168->137|168->137|175->144|175->144|175->144|177->146|177->146|177->146|178->147|178->147|178->147|178->147|179->148|179->148|179->148|180->149|180->149|180->149|181->150|182->151|182->151|183->152|184->153|185->154|196->165|196->165|196->165|198->167|198->167|198->167|201->170|201->170|201->170|201->170|201->170|201->170|201->170|202->171|202->171|202->171|202->171|203->172|203->172|204->173|204->173|207->176|208->177|229->198|229->198|229->198|229->198|229->198|231->200|231->200|232->201|232->201|233->202|233->202|245->214|245->214|245->214|247->216|247->216|247->216|248->217|248->217|248->217|248->217|248->217|248->217|248->217|250->219|253->222|254->223|256->225|257->226|257->226|257->226|268->237|268->237|268->237|273->242|273->242|274->243|274->243|275->244|275->244|276->245|276->245|287->256|287->256|287->256|291->260|291->260|291->260|292->261|292->261|292->261|300->269|300->269|300->269|302->271|302->271|302->271|304->273|304->273|304->273|305->274|305->274|305->274|306->275|306->275|306->275|320->289|320->289|320->289|329->298|329->298|329->298|335->304|335->304|335->304|352->321|356->325|357->326|361->330|362->331|366->335|375->344|379->348|380->349|384->353|385->354|389->358|397->366|401->370|402->371|406->375|407->376|411->380|436->405|436->405|436->405|438->407|443->412|444->413|445->414|453->422|453->422|457->426|457->426|466->435|466->435|466->435|468->437|473->442|474->443|475->444|491->460|491->460|511->480|511->480|523->492|523->492|527->496|527->496|531->500|531->500|531->500|531->500|558->527|558->527|558->527|579->548|579->548|581->550|652->621|652->621|652->621|654->623|666->635|668->637|668->637|668->637|670->639|682->651|682->651|682->651|682->651|683->652|701->670|702->671|717->686|717->686|719->688|731->700|731->700|731->700|745->714|745->714|745->714|746->715|747->716|748->717|926->895|926->895|926->895|948->917|948->917|948->917|950->919|950->919|950->919|950->919|950->919|952->921|954->923|965->934|965->934|982->951|982->951|982->951|1005->974|1005->974|1005->974|1007->976|1007->976|1007->976|1007->976|1007->976|1009->978|1011->980|1022->991|1022->991|1040->1009|1040->1009|1040->1009|1047->1016|1047->1016|1051->1020|1051->1020|1054->1023|1054->1023|1063->1032|1063->1032|1063->1032|1065->1034|1065->1034|1065->1034|1065->1034|1065->1034|1067->1036|1069->1038|1078->1047|1078->1047|1098->1067|1098->1067|1098->1067|1127->1096|1127->1096|1144->1113|1144->1113|1144->1113|1157->1126|1157->1126|1157->1126|1189->1158|1189->1158|1189->1158|1225->1194|1225->1194|1225->1194|1255->1224|1255->1224|1255->1224|1280->1249|1280->1249|1280->1249|1282->1251|1282->1251|1282->1251|1282->1251|1282->1251|1284->1253|1285->1254|1326->1295|1326->1295|1326->1295|1348->1317|1348->1317|1348->1317|1349->1318|1349->1318|1349->1318|1350->1319|1350->1319|1350->1319|1351->1320|1351->1320|1366->1335|1366->1335|1366->1335|1367->1336|1368->1337|1368->1337|1368->1337|1369->1338|1369->1338|1369->1338|1370->1339|1370->1339|1370->1339|1371->1340|1371->1340|1371->1340|1372->1341|1372->1341|1372->1341|1373->1342|1373->1342|1373->1342|1375->1344|1376->1345|1414->1383|1414->1383|1414->1383|1418->1387|1418->1387|1418->1387|1418->1387|1443->1412|1443->1412|1443->1412|1448->1417|1448->1417|1448->1417|1449->1418|1449->1418|1449->1418|1449->1418|1449->1418|1449->1418|1449->1418|1450->1419|1452->1421|1454->1423|1454->1423|1455->1424|1455->1424|1455->1424|1458->1427|1458->1427|1458->1427|1458->1427|1458->1427|1458->1427|1458->1427|1459->1428|1459->1428|1460->1429|1460->1429|1461->1430|1461->1430|1461->1430|1461->1430|1461->1430|1461->1430|1463->1432|1463->1432|1468->1437|1469->1438|1489->1458|1489->1458|1489->1458|1513->1482|1513->1482|1513->1482|1514->1483|1514->1483|1514->1483|1517->1486|1517->1486|1517->1486|1518->1487|1518->1487|1518->1487|1521->1490|1521->1490|1521->1490|1522->1491|1522->1491|1522->1491|1532->1501|1532->1501|1532->1501|1553->1522|1553->1522|1553->1522|1554->1523|1555->1524|1555->1524|1555->1524|1556->1525|1556->1525|1556->1525|1557->1526|1557->1526|1557->1526|1561->1530|1561->1530|1561->1530|1577->1546|1577->1546|1577->1546|1588->1557|1588->1557|1588->1557|1597->1566|1597->1566|1597->1566|1598->1567|1598->1567|1598->1567|1599->1568|1600->1569|1617->1586|1617->1586|1617->1586|1663->1632|1663->1632|1663->1632|1708->1677|1708->1677|1708->1677|1710->1679|1712->1681|1712->1681|1712->1681|1713->1682|1713->1682|1713->1682|1714->1683|1714->1683|1714->1683|1715->1684|1715->1684|1715->1684|1716->1685|1716->1685|1716->1685|1717->1686|1717->1686|1717->1686|1718->1687|1718->1687|1718->1687|1719->1688|1719->1688|1719->1688|1720->1689|1720->1689|1720->1689|1721->1690|1721->1690|1721->1690|1722->1691|1722->1691|1722->1691|1723->1692|1723->1692|1723->1692|1724->1693|1724->1693|1724->1693|1725->1694|1725->1694|1725->1694|1726->1695|1726->1695|1726->1695|1727->1696|1727->1696|1727->1696|1728->1697|1728->1697|1728->1697|1729->1698|1729->1698|1729->1698|1730->1699|1730->1699|1730->1699|1731->1700|1731->1700|1731->1700|1732->1701|1732->1701|1732->1701|1733->1702|1733->1702|1733->1702|1735->1704|1736->1705|1752->1721|1752->1721|1752->1721|2023->1992|2023->1992|2023->1992|2093->2062|2093->2062|2099->2068|2099->2068|2105->2074|2105->2074|2111->2080|2111->2080|2117->2086|2117->2086|2125->2094|2125->2094|2131->2100|2131->2100|2137->2106|2137->2106|2143->2112|2143->2112|2175->2144|2175->2144|2181->2150|2181->2150|2194->2163|2194->2163|2206->2175|2206->2175|2305->2274|2306->2275|2306->2275|2306->2275|2306->2275|2307->2276
                  -- GENERATED --
              */
          