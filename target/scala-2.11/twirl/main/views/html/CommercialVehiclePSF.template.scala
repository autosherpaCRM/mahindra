
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object CommercialVehiclePSF_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class CommercialVehiclePSF extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.4*/("""
"""),format.raw/*2.1*/("""<div style="display:none;" class="animated  bounceInRight" id="commercialVehicle" >		
			<div class="row">
				<table class=" table table-responsive table-striped table-bordered">
        <thead>
            <tr>
                <th>Sl No.</th>
                <th>Commercial Vehicle</th>
                <th>Rating</th>
                
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Overall service experience</td>
                <td > <select class="form-control input-sm" id="qMC1_OverallServiceExp" name="qMC1_OverallServiceExp">
        <option value="0">--Select--</option>
        <option value="Not Happy">1.Not Happy</option>
        <option value="Ok">2.Ok</option>
        <option value="Good">3.Good</option>
        <option value="No Ratings">4.No Ratings</option>
     
      </select></td>
               
            </tr>
			
				<tr>
					<td>2</td>
					<td>Happy with repair job</td>
					<td > <select class="form-control input-sm" id="qMC2_HappyWithRepairJob" name="qMC2_HappyWithRepairJob">
        <option value="0">--Select--</option>
        <option value="Not Happy">1.Not Happy</option>
        <option value="Ok">2.Ok</option>
        <option value="Good">3.Good</option>
        <option value="No Ratings">4.No Ratings</option>
     
      </select></td>
				</tr>
				<tr>
					<td>3</td>
					<td>Courteousness and Helpfulness of Service Advisor</td>
					<td > <select class="form-control input-sm" id="qMC3_CoutnessHelpfulnessOfSA" name="qMC3_CoutnessHelpfulnessOfSA">
        <option value="0">--Select--</option>
        <option value="Not Happy">1.Not Happy</option>
        <option value="Ok">2.Ok</option>
        <option value="Good">3.Good</option>
        <option value="No Ratings">4.No Ratings</option>
     
      </select></td>
				</tr>
				<tr>
					<td>4</td>
					<td>Time taken to complete job</td>
					<td > <select class="form-control input-sm" id="qMC4_TimeTakenCompleteJob" name="qMC4_TimeTakenCompleteJob">
        <option value="0">--Select--</option>
        <option value="Not Happy">1.Not Happy</option>
        <option value="Ok">2.Ok</option>
        <option value="Good">3.Good</option>
        <option value="No Ratings">4.No Ratings</option>
     
      </select></td>
				</tr>
				<tr>
					<td>5</td>
					<td>Cost estimate adherence</td>
					<td > <select class="form-control input-sm" id="qMC5_CostEstimateAdherance" name="qMC5_CostEstimateAdherance">
        <option value="0">--Select--</option>
        <option value="Not Happy">1.Not Happy</option>
        <option value="Ok">2.Ok</option>
        <option value="Good">3.Good</option>
        <option value="No Ratings">4.No Ratings</option>
     
      </select></td>
				</tr>
				<tr>
					<td>6</td>
					<td>Able to get service at convenient time</td>
					<td > <select class="form-control input-sm" id="qMC6_ServiceConvinenantTime" name="qMC6_ServiceConvinenantTime">
        <option value="0">--Select--</option>
        <option value="Not Happy">1.Not Happy</option>
        <option value="Ok">2.Ok</option>
        <option value="Good">3.Good</option>
        <option value="No Ratings">4.No Ratings</option>
     
      </select></td>
				</tr>
				<tr>
					<td>7</td>
					<td>Would like to revisit dealer</td>
					<td > <select class="form-control input-sm" id="qMC7_LikeToRevistDealer" name="qMC7_LikeToRevistDealer">
        <option value="0">--Select--</option>
        <option value="Not Happy">1.Not Happy</option>
        <option value="Ok">2.Ok</option>
        <option value="Good">3.Good</option>
        <option value="No Ratings">4.No Ratings</option>
     
      </select></td>
				</tr>
				
			</tbody>
			</table>
			 </div> 
              <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Remarks</label>
                      <textarea type="text" name="remarksList[2]" class="form-control" rows="2" id=""></textarea>
                    </div>
                  </div>
			 							
					<div class="pull-right">
                      <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#IncompleteSurvey">Incomplete Survey</button>
					<button type="button" class="btn btn-info" id="BackTo3rdQuesMaAgeing">Back</button>	
                    <button type="button" class="btn btn-primary" id="nextToCommercial">Next</button> 
					
                   
					<!-- <button type="submit" class="btn btn-info" id="psfAgeAbove2Submit">Submit</button> -->
			</div>
			

"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object CommercialVehiclePSF extends CommercialVehiclePSF_Scope0.CommercialVehiclePSF
              /*
                  -- GENERATED --
                  DATE: Sat Dec 16 16:03:41 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/CommercialVehiclePSF.scala.html
                  HASH: d371b67b0391ac761737615d6c86499239820a9b
                  MATRIX: 768->1|864->3|892->5
                  LINES: 27->1|32->1|33->2
                  -- GENERATED --
              */
          