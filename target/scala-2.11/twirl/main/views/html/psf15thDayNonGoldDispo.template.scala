
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object psf15thDayNonGoldDispo_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class psf15thDayNonGoldDispo extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[Vehicle,String,String,Service,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(vehicleData:Vehicle,userName:String,dealername:String,latestService:Service):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.79*/("""
"""),format.raw/*2.1*/("""<div class="panel panel-primary">
    <div class="panel-heading" align="center"; style="background-color:#1797be;"><img src=""""),_display_(/*3.93*/routes/*3.99*/.Assets.at("images/INTERACTION HISTORY.png")),format.raw/*3.143*/("""" style="width:17px"/>&nbsp;<b>PSF DISPOSITION FORM 15TH DAY</b> 
		</div>
    <div class="panel-body">
		<div class="col-md-12">
			<div class="form-group">
					<div id="PSFconnectCall1H" class="animated  bounceInRight">
		  <label for=""><b>Did you talk to the customer?</b></label>
		  <br>
		  <div class="radio-inline">
			<label>
			  <input type="radio" name="isContacted" value="PSF Yes" id="">
			  Yes </label>
		  </div>
		  <div class="radio-inline">
			<label>
			  <input type="radio" name="isContacted" value="PSF No" id="" >
			  No</label>
		  </div>
		 </div>
		 
		 <div style="display:none;" class="animated  bounceInRight" id="PSFNotSpeachDiv"><br>
			<label for=""><b>Choose reason for not being able to talk to customer:</b></label>
			<br>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="Ringing No response" id="">
				Ringing No response </label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="Busy" id="" >
				Busy</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="Switched Off / Unreachable" id="" >
				Switched Off / Unreachable</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="Invalid Number" id="" >
				Invalid Number</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="PSFDispositon" value="NoOther" id="NOOthersCheck" >
				Other</label>
			</div>
			
			
			<div class="row animated  bounceInRight"  id="PSFNoOthers">
			<div class="col-md-3">
			<label><b>Remarks</b></label>
			<textarea type="text" class="form-control" rows="1" name="OtherComments"></textarea>
			</div>
			</div>
			<div class="pull-right">
						
					<button type="button" class="btn btn-info" name="" id="BackTolatkNoPsf">Back</button>
					<button type="submit" class="btn btn-info" name="" value="">Submit</button>
			</div>
		  </div>
		  
		   <div style="display:none;" class="animated  bounceInRight" id="PSFYesTalkH" >
			<br />
			<div class="col-md-12">
			<p>Good Morning/Good Afternoon, I am  """),_display_(/*69.43*/userName),format.raw/*69.51*/(""" """),format.raw/*69.52*/("""from """),_display_(/*69.58*/dealername),format.raw/*69.68*/(""". Am I speaking to """),_display_(/*69.88*/{vehicleData.getCustomer().getCustomerName()}),format.raw/*69.133*/("""?</p>
			<p>Thank you for choosing our """),_display_(/*70.35*/dealername),format.raw/*70.45*/(""" """),format.raw/*70.46*/("""for servicing of your Hyundai """),_display_(/*70.77*/{vehicleData.getModel()}),format.raw/*70.101*/("""</p>
			<p>Can I talk to you for few minutes to take few feedback about the vehicle service.</p>
			<p>What did the Customer Say?</p>
		 <div class="radio-inline">
			<label>
			  <input type="radio" name="disposition" value="PSF_Yes" id="GoodMorningYes">
			  Ok, Proceed </label>
		  </div>
		   <div class="radio-inline">
			<label>
			  <input type="radio" name="disposition" value="Call Me Later" id="GoodMorningNo">
			  I am Busy </label>
		  </div>
		  
		  <div class="pull-right">
					<button type="button" class="btn btn-info" id="BackTo1stQ">Back</button>						
					<button type="button" class="btn btn-info" id="NextTO2ndQ">Next</button>
			</div>
		  </div>
		  </div>
		    <div style="display:none;" class="animated  bounceInRight" id="PSFYesNamaskarYesDivH" >
			
			<p>Sir/Madam, were all the requested jobs done to your complete satisfaction?</p>
			
				<div class="radio-inline">
					<label>
					  <input type="radio" name="q1_CompleteSatisfication" value="PSFSelf Yes" id="PSF2YesId">
					  Yes </label>
				</div>
				<div class="radio-inline">
					<label>
					  <input type="radio" name="q1_CompleteSatisfication" value="PSFSelf No" id="PSF2NoId">
					 No </label>
				</div>
				
				<div style="display:none;" class="row animated  bounceInRight" id="PsfSelfDriveInNo1H" >
				
				<div class="col-md-12">
				<p>Sir we are extremely sorry for an inconvinence caused.</p>
				<p>May I know the reason of dissatisfaction.</p>
				</div>
				<div class="col-md-12">
				<div class="col-md-3">
				<div class="form-group">
				<label>Function</label>
				<select class="form-control" name="q2_InconvinenceDSRFunction">
				<option value="0">--SELECT--</option>
					<option value="Washing">Washing</option>
					<option value="Delay Delivery">Delay Delivery</option>
					<option value="Discrepancy on Charges">Discrepancy on Charges</option>
					<option value="Requested Repair not Done">Requested Repair not Done</option>
					<option value="Quality of Work">Quality of Work</option>
					<option value="SA/Other Emp Behaviour">SA/Other Emp Behaviour</option>
					<option value="New Problem">New Problem</option>
					<option value="Others">Others</option>
				</select>
				</div>	
				</div>
				<div class="col-md-3">
				<div class="form-group">
				<label>Assigned To</label>
				<select class="form-control" name="q3_InconvinenceDSRAssignedTo">
				<option value="0">--SELECT--</option>
					<option value=""""),_display_(/*133.22*/{latestService.getSaName()}),format.raw/*133.49*/("""">"""),_display_(/*133.52*/{latestService.getSaName()}),format.raw/*133.79*/("""</option>
					
				</select>
				</div>	
				</div>
				<div class="col-md-3">
				<div class="form-group">
				<label>Remarks* (Mandate)</label>
				<textarea class="form-control" type="text" rows="2" name="remarksList[1]"></textarea>
					
				
				</div>	
				</div>
				
			</div>
			<div class="col-md-12">
				<p>I have recorded your concern and will inform the same to our service team and our service team will contact you soon. </p></div>
				
			
			
			</div>
			
			<div style="display:none;" class="row animated  bounceInRight" id="PsfSelfDriveINYesH" >
			
			<div class="col-md-12">
			
						<div class="col-md-3">
	  <p>Is your Car Performing Well?</p>
	  </div>
			<div class="col-md-2">
	     <div class="form-group">
	  <div class="form-inline">
      
       <select class="form-control" id="sel4" name="q13_IsCarPerformingWell">
       <option value="0">--SELECT--</option>
         <option value="Yes">Yes</option>
        <option value="No">No</option>
        
      </select>
	  </div>
	  </div>
			</div><br />
			</div>
			</div>
			<div class="pull-right">
					<button type="button" class="btn btn-info" id="BackToSirMam">Back</button>						
					<button type="button" class="btn btn-info" id="NextToHowRate">Next</button>
			</div>
			
			
			</div>
			
			
			<div class="col-md-12 animated  bounceInRight" style="display:none;" id="upsell3rdDayH">
			<label>Capture Lead : Is there an Upsell Opportunity.</label>
		   <br>
		   
			<div class="radio-inline">
			  <label>
			<input type="radio" name="LeadYesH" value="Yes" id="LeadYesID3Hyndai" >
				Yes</label>
			</div>
			<div class="radio-inline">
			  <label>
				<input type="radio" name="LeadYesH" value="No" id="LeadNoID3Hyndai" checked>
				No</label>
			</div>
			<div class="row animated  bounceInRight" style="display:none;" id="LeadHyndai3rdDay">
			<div class="col-md-12">
			  <div class="col-md-6">
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[0].upsellId" value="1" id="InsuranceIDCheck" onClick="loadLeadBasedOnUserLocation('InsuranceIDCheck','insuranceLead1')">
					Service</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="InsuranceSelect">
				 
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[0].taggedTo" id="insuranceLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments1" name="upsellLead[0].upsellComments"></textarea>
					</div>
				 
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[1].upsellId" value="2" id="MaxicareIDCheck" onClick="loadLeadBasedOnUserLocation('MaxicareIDCheck','maxicareLead')">
					Warranty / EW</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="MaxicareSelect">
					
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[1].taggedTo" id="maxicareLead">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments2" name="upsellLead[1].upsellComments"></textarea>
					</div>
				 
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="VAS myOutCheckbox" name="upsellLead[2].upsellId" value="3" id="VASID" onClick="loadLeadBasedOnUserLocation('VASID','vASLead1')"/>
					VAS</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="VASTagToSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[2].taggedTo" id="vASLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments3" name="upsellLead[2].upsellComments"></textarea>
					</div>
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[3].upsellId" value="4" id="ShieldID" onClick="loadLeadBasedOnUserLocation('ShieldID','warrantyLead1')" />
					Re-Finance / New Car Finance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="ShieldSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[3].taggedTo" id="warrantyLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments4" name="upsellLead[3].upsellComments"></textarea>
					</div>
				 </div>
				</div>
				<div class="col-md-6">
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="Loan myOutCheckbox" name="upsellLead[4].upsellId" value="5" id="RoadSideAsstID" onClick="loadLeadBasedOnUserLocation('RoadSideAsstID','RoadSideAssiLead1')"/>
					Sell Old Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="RoadSideAssiSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[4].taggedTo" id="RoadSideAssiLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments5" name="upsellLead[4].upsellComments"></textarea>
					</div>
				  </div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="EXCHANGE myOutCheckbox" name="upsellLead[5].upsellId" value="6" id="EXCHANGEID" onClick="loadLeadBasedOnUserLocation('EXCHANGEID','buyNewCarLead1')"/>
					Buy New Car / Exchange</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="EXCHANGEIDSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[5].taggedTo" id="buyNewCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments6" name="upsellLead[5].upsellComments"></textarea>
					</div>
				 	</div>
				  <div class="checkbox">
				  <label>
					<input type="checkbox" class="UsedCar myOutCheckbox" name="upsellLead[6].upsellId" value="9" id="UsedCarID" onClick="loadLeadBasedOnUserLocation('UsedCarID','usedCarLead1')"/>
					Used Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="UsedCarSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[6].taggedTo" id="usedCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
			<textarea class="form-control" rows="1" id="comments7" name="upsellLead[6].upsellComments"></textarea>
					</div>
				 
				</div>
		   
			  </div>
			</div>
			 
			</div>
			  <div class="pull-right" >
			<button type="button" class="btn btn-info" id="BackTo3rdDayRate">Back</button>
			  <button type="button" class="btn btn-info" id="NextToUpsellInsu">Next</button>
			</div>
			</div>
		  
			
			 <div style="display:none;" class="row animated  bounceInRight" id="PSFYesNamaskarNoDivH" >
			 	<p>We are extremely sorry for an inconvenience </p>
			 	<p>May I know your Suitable time for your valuable feedback</p>
			 
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpDate">Call Back Date</label>
					  <input type="text" name="psfFollowUpDate" class="datepickerPlus30Days form-control" id="">
					</div>
				  </div>
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpTime">Call Back Time</label>
					  <input type="text" name="psfFollowUpTime" class="single-input form-control" id="">
					</div>
				  </div>
				  <div class="col-md-6">
					<div class="form-group">
					  <label for="followUpTime">Remarks</label>
					  <textarea type="text" name="remarksList[2]" class="form-control" id=""></textarea>
					</div>
				  </div>
				  </br>
				  <div class="pull-right" id="" >
					<button type="button" class="btn btn-info" name="" id="BackToDidUtlakPSFH" >Back</button>
					<button type="submit" class="btn btn-info" name="" id="" >Submit</button>
				</div>
				
			</div>
		  
		   <div style="display:none;" class="row animated  bounceInRight" id="PSFFeedbackQ" >
		   <div class="col-md-12">
			 	<p>May i take a moment to update your information </p>
			 				 
				  	<div class="radio-inline">
					<label>
					  <input type="radio" name="q12_FeedbackTaken" value="Yes" id="feedbackYes1" data-target="#addBtn" data-toggle="modal">
					  Yes </label>
				</div>
				<div class="radio-inline">
					<label>
					  <input type="radio" name="q12_FeedbackTaken" value="No" id="feedbackNo2" checked>
					 No </label>
				</div>

				
			
			 
			 <div style="display:none;" class="row animated  bounceInRight" id="feedbackPSFNo" >
			 <div class="col-md-12">
			 
				<p>Thank you for your valuable feedback and time. Have a nice day. </p>
			 				 </div>
				 
			</div>
							 <div class="pull-right" id=""  >
					<button type="button" class="btn btn-info" name="" id="BackToSirMamDiv" >Back</button>
					<button type="submit" class="btn btn-info" name="" id="SubmitDivMamDiv" >Submit</button>
				</div>
				</div>
		  </div>
		  
		  
		  
			
			<!--panel Group End-->
		</div>
	</div><!--panel Body End-->
   
  </div>
</div>

<script src=""""),_display_(/*418.15*/routes/*418.21*/.Assets.at("javascripts/psf15thNonGoldDisposition.js")),format.raw/*418.75*/("""" type="text/javascript"></script>
"""))
      }
    }
  }

  def render(vehicleData:Vehicle,userName:String,dealername:String,latestService:Service): play.twirl.api.HtmlFormat.Appendable = apply(vehicleData,userName,dealername,latestService)

  def f:((Vehicle,String,String,Service) => play.twirl.api.HtmlFormat.Appendable) = (vehicleData,userName,dealername,latestService) => apply(vehicleData,userName,dealername,latestService)

  def ref: this.type = this

}


}

/**/
object psf15thDayNonGoldDispo extends psf15thDayNonGoldDispo_Scope0.psf15thDayNonGoldDispo
              /*
                  -- GENERATED --
                  DATE: Sat Dec 16 16:03:44 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/psf15thDayNonGoldDispo.scala.html
                  HASH: 7c0e16abd841d93e4e056dad760be4ceae5e668f
                  MATRIX: 802->1|974->78|1002->80|1155->207|1169->213|1234->257|3481->2477|3510->2485|3539->2486|3572->2492|3603->2502|3650->2522|3717->2567|3785->2608|3816->2618|3845->2619|3903->2650|3949->2674|6488->5185|6537->5212|6568->5215|6617->5242|16979->15576|16995->15582|17071->15636
                  LINES: 27->1|32->1|33->2|34->3|34->3|34->3|100->69|100->69|100->69|100->69|100->69|100->69|100->69|101->70|101->70|101->70|101->70|101->70|164->133|164->133|164->133|164->133|449->418|449->418|449->418
                  -- GENERATED --
              */
          