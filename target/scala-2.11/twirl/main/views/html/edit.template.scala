
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object edit_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class edit extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,String,Long,models.WyzUser,Form[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealerName:String,userName:String,id: Long,editUser:models.WyzUser,form:Form[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import models._
import scala._

Seq[Any](format.raw/*1.89*/("""
"""),_display_(/*4.2*/main("AutoSherpaCRM",userName,dealerName)/*4.43*/ {_display_(Seq[Any](format.raw/*4.45*/("""
"""),_display_(/*5.2*/helper/*5.8*/.form(routes.WyzUserController.postEditUser(id), args= 'class -> "form-horizontal",'enctype ->     "multipart/form-data", 'id -> "updateForm")/*5.150*/  {_display_(Seq[Any](format.raw/*5.153*/("""
"""),format.raw/*6.1*/("""<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Edit User </h3>
      </div>
      <div class="panel-body">
        <form >
          <div class='row'>
            <div class='col-sm-12'>
              <div class='col-sm-4'>
                <div class='form-group'>
                  <label for="firstName">First Name:</label>
                  <input type="text" class="form-control" name="firstName" value='"""),_display_(/*19.84*/{editUser.firstName}),format.raw/*19.104*/("""' id="firstName" autocomplete="off"  />
                </div>
              </div>
              <div class='col-sm-4'>
                <div class='form-group'>
                  <label for="lastName">Last Name :</label>
                  <input type="text" class="form-control" name="lastName" value='"""),_display_(/*25.83*/{editUser.lastName}),format.raw/*25.102*/("""' id="lastName" autocomplete="off"  />
                </div>
              </div>
              <div class='col-sm-4'>
                <div class='form-group'>
                  <label for="userName">User Name :</label>
                  <input type="text" class="form-control" name="userName" value='"""),_display_(/*31.83*/{editUser.userName}),format.raw/*31.102*/("""' id="userName" autocomplete="off"  />
                </div>
              </div>
              
            </div>
          </div>
          <div class='row'>
            <div class='col-sm-12'>
              
              <div class='col-sm-4'>
                <div class='form-group'>
                  <label for="phoneNumber">PhoneNumber :</label>
                  <input type="text" class="form-control" name="phoneNumber" value='"""),_display_(/*43.86*/{editUser.phoneNumber}),format.raw/*43.108*/("""' id="phoneNumber" autocomplete="off"  />
                </div>
              </div>
              <div class='col-sm-4'>
                <div class='form-group'>
                  <label for="phoneIMENo">Phone IMEI No :</label>
                  <input type="text" class="form-control" name="phoneIMEINo" value='"""),_display_(/*49.86*/{editUser.phoneIMEINo}),format.raw/*49.108*/("""' id="phoneIMEINo" autocomplete="off"  />
                </div>
              </div>
               <div class='col-sm-4'>
                <div class='form-group'>
                  <label for="emailId">EmailId :</label>
                  <input type="text" class="form-control" name="emailId" value='"""),_display_(/*55.82*/{editUser.emailId}),format.raw/*55.100*/("""' id="emailId" autocomplete="off"  />
                </div>
              </div>
            </div>
          </div>
          <div class='row'>
            <div class='col-sm-12'>
              
             
              <div class='col-sm-4'>
                <div class='form-group'>
                  <label for="password">Password :</label>
                  <input type="text" class="form-control" name="password" value='"""),_display_(/*67.83*/{editUser.password}),format.raw/*67.102*/("""' id="password" autocomplete="off"  />
                </div>
              </div>
              <div class='text-right'>
                <input type="submit" onclick="myFunction()" class="btn btn-primary" value="UPDATE" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>   

        """)))}),format.raw/*81.10*/("""
        

""")))}))
      }
    }
  }

  def render(dealerName:String,userName:String,id:Long,editUser:models.WyzUser,form:Form[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(dealerName,userName,id,editUser,form)

  def f:((String,String,Long,models.WyzUser,Form[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (dealerName,userName,id,editUser,form) => apply(dealerName,userName,id,editUser,form)

  def ref: this.type = this

}


}

/**/
object edit extends edit_Scope0.edit
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:26 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/edit.scala.html
                  HASH: 3b955f190cb3ae747d6881314e42d3e693b162d9
                  MATRIX: 784->1|996->88|1024->126|1073->167|1112->169|1140->172|1153->178|1304->320|1345->323|1373->325|1923->848|1965->868|2302->1178|2343->1197|2679->1506|2720->1525|3200->1978|3244->2000|3592->2321|3636->2343|3972->2652|4012->2670|4481->3112|4522->3131|4917->3495
                  LINES: 27->1|33->1|34->4|34->4|34->4|35->5|35->5|35->5|35->5|36->6|49->19|49->19|55->25|55->25|61->31|61->31|73->43|73->43|79->49|79->49|85->55|85->55|97->67|97->67|111->81
                  -- GENERATED --
              */
          