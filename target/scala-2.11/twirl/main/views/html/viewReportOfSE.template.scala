
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object viewReportOfSE_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class viewReportOfSE extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[String,String,Form[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealerName:String,userName:String,form:Form[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import models._
import helper._
import scala._

Seq[Any](format.raw/*1.56*/("""
"""),_display_(/*5.2*/mainPageSalesExecutive("AutoSherpaCRM",userName,dealerName)/*5.61*/ {_display_(Seq[Any](format.raw/*5.63*/("""

"""),format.raw/*7.1*/("""<div class="panel panel-default">
  <div class="panel-heading">Chart Reports</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-5"> 
        <!-- LINE CHART -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Pi Chart</h3>
          </div>
          <div class="box-body">
            <canvas id="pieChartForSE" style="height:250px"></canvas>
          </div>
        </div><!-- /.box --> 
      </div>
      <div class="col-md-5"> 
        <!-- AREA CHART -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Calls made vs Test Drive</h3>
          </div>
          <div class="box-body">
            <div class="chart">
              <canvas id="areaChartForSE" style="height:250px"></canvas>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-2"> 
        <!-- AREA CHART -->
        <div class="box box-primary">
          <div class="panel panel-default">
            <div class="panel-heading"> <a href="#" class="pull-right"> <em class="icon-plus text-muted"></em> </a>Contacts</div>
            <div class="list-group"> 
              
              <!-- START User status--> 
              <a href="#" class="media p mt0 list-group-item"> <span class="pull-right"> <span class="circle circle-success circle-lg"></span> </span> <span class="media-body"> <span class="media-heading"> <strong>"""),_display_(/*43.216*/{userName}),format.raw/*43.226*/("""</strong> <br>
              <small class="text-muted">Sales Executive</small> </span> </span> </a> 
                <!-- END User status-->
              <a href="#" class="media p mt0 list-group-item text-center text-muted">View all contacts</a> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
""")))}))
      }
    }
  }

  def render(dealerName:String,userName:String,form:Form[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(dealerName,userName,form)

  def f:((String,String,Form[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (dealerName,userName,form) => apply(dealerName,userName,form)

  def ref: this.type = this

}


}

/**/
object viewReportOfSE extends viewReportOfSE_Scope0.viewReportOfSE
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:42 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/viewReportOfSE.scala.html
                  HASH: 9c8c5734f365bb50231bfb9caa484359b2fd9263
                  MATRIX: 784->1|979->55|1007->111|1074->170|1113->172|1143->176|2714->1719|2746->1729
                  LINES: 27->1|34->1|35->5|35->5|35->5|37->7|73->43|73->43
                  -- GENERATED --
              */
          