
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object PSFList3rdForFeedbackOfService_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class PSFList3rdForFeedbackOfService extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealercode:String,dealerName:String,userName:String,oem:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.66*/("""

"""),_display_(/*3.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*3.65*/ {_display_(Seq[Any](format.raw/*3.67*/("""

"""),format.raw/*5.1*/("""<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading"> PSF List For 3rd Day FeedBack</div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li id="psf6thData" class="active"><a href="#home" data-toggle="tab" value ="psf3rdday" id="Tab1" onclick="assignedInteractionPSFData(result.name);" >3rd Day PSF</a> </li>
                    <li><a href="#profile" data-toggle="tab"  id="Tab2" onclick="ajaxCallForFollowUpRequiredPSFData();">Follow Up Required</a> </li>
                    <!--<li><a href="#messages" data-toggle="tab" id="Tab3">Incompleted Survey</a></li>-->
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxCallForCompletedSurveyPSFData();" >Completed Survey</a> </li>
                   <!--<li><a href="#appointments" data-toggle="tab" id="Tab8" onclick="ajaxCallForAppointmentPSFData();">Appointments</a></li>--> 
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxCallForNonContactsPSFData();">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxCallForDroppedCallsPSFData();">Dropped</a> </li>
                </ul>
                <div class="tab-content">
				<div class="panel panel-default" style="border-top: #fff;">
				<div class="panel-body">
                       <div class="row">
					<div class="col-md-3" id="SchedulefromBillDate" style="display:none">
					<label>From Bill Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="Enter From Bill Date" data-column-index="1" id="frombilldaterange" name="frombilldaterange" readonly>
				  </div>
				   <div class="col-md-3" id="ScheduletoBillDate" style="display:none">
					<label>To Bill Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="Enter To Bill Date" data-column-index="2" id="tobilldaterange" name="tobilldaterange" readonly>
				  </div>
				
				</div>
				
				    <div class="row" id="forschedule">
					<div class="col-md-3" id="fromCallDate" style="display:none">
					<label>From Call Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="Enter From Call Date" data-column-index="1" id="fromCalldaterange22" name="frombilldaterange22" readonly>
				  </div>
				   <div class="col-md-3" id="toCallDate" style="display:none">
					<label>To Call Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="Enter To Call Date" data-column-index="2" id="tobCalldaterange33" name="tobilldaterange33" readonly>
				  </div>
				</div>
				</div>
				</div>
                    <div class="panel panel-default tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="assignedInteractionPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th> 
                                            <th>Mobile Number</th>                                   
                                            <th>Vehicle RegNo.</th>
                                            <th>Model</th>
                                            <th>RO Number</th>
                                            <th>RO Date</th>
                                            <th>Bill Date</th>
                                            <th>Category</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="panel panel-default tab-pane fade" id="profile">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForFollowUpRequiredPSFData">
                                    <thead>
                                        <tr>                                    
											<th>FollowUp Date</th>
											<th>FollowUp Time</th>
											<th>call_date</th>
											<th>Customer Name</th>
											<th>Mobile Number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>






                    <div class="panel panel-default tab-pane fade" id="messages">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForIncompletedSurveyPSFData">
                                    <thead>
                                        <tr>                                    
											<th>FollowUp Date</th>
											<th>FollowUp Time</th>
											<th>call_date</th>
											<th>Customer Name</th>
											<th>Mobile Number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="panel panel-default tab-pane fade" id="settings">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForCompletedSurveyPSFData">
                                    <thead>
                                        <tr>                                    
                                           	<th>Survey Date</th>																	
											<th>Customer Name</th>																	
											<th>Category</th>																	
											<th>vehicle RegNo</th>																	
											<th>Model</th>																	
											<th>RO Date</th>																	
											<th>RO Number</th>																	
											<th>Bill Date</th>
											<th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>



                    <div class="panel panel-default tab-pane fade" id="nonContacts">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForNonContactsPSFDataCalls">
                                    <thead>
                                        <tr>                                   
											<th>Customer Name</th>
											<th>Category</th>
											<th>Mobile number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
											<th>Last Disposition</th>
											<th>Call Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
                    <div class="panel panel-default tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForDroppedCallsPSFData">
                                    <thead>
                                         <tr>                                    
                                            <th>Customer Name</th>
											<th>Category</th>
											<th>Mobile number</th>
											<th>Vehicle RegNo</th>
											<th>Model</th>
											<th>RO Date</th>
											<th>RO Number</th>
											<th>Bill Date</th>
											<th>Last Disposition</th>
											<th>Call Date</th>											
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
 <div class="panel panel-default tab-pane fade" id="appointments">
                        <div class="panel-body inf-content">  
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="ajaxCallForAppointmentPSFData">
                                    <thead>
                                        <tr>                                    
                                            <th>Customer Name</th>
											<th>Vehicle RegNo</th>
											<th>Appointment Date</th>
											<th>Mobile number</th>
											<th>Call Date</th>
											<th>Appointment Time</th>
											<th>RO Number</th>
											<th>RO Date</th>
											<th>Bill Date</th>
											<th>Survey date</th>
											<th>Model</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body --> 
                    </div>
                    <!-- /.panel --> 
                </div>
                <!-- /.col-lg-12 --> 
            </div>
        </div>
    </div>
</div>

""")))}),format.raw/*255.2*/("""
"""),format.raw/*256.1*/("""<script type="text/javascript">
    window.onload = ajaxRequestPSFDataPageWithTabFor3rd;
</script>
<script>

function FilterOptionData(table)"""),format.raw/*261.33*/("""{"""),format.raw/*261.34*/("""
	"""),format.raw/*262.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*263.39*/("""{"""),format.raw/*263.40*/("""
    	"""),format.raw/*264.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
		console.log(v);
		console.log(i);
	    
	    if(v.length>0)"""),format.raw/*270.20*/("""{"""),format.raw/*270.21*/("""
		    	   
		   """),format.raw/*272.6*/("""values[i] = v;
	    
	    """),format.raw/*274.6*/("""}"""),format.raw/*274.7*/("""
	    
	    	"""),format.raw/*276.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');

	 	 if(values.length>0) """),format.raw/*281.25*/("""{"""),format.raw/*281.26*/("""
			 """),format.raw/*282.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*282.37*/("""{"""),format.raw/*282.38*/("""	
			    """),format.raw/*283.8*/("""table.api().columns(i).search(values[i]);
			 """),format.raw/*284.5*/("""}"""),format.raw/*284.6*/("""
			 """),format.raw/*285.5*/("""table.api().draw();
			 """),format.raw/*286.5*/("""}"""),format.raw/*286.6*/(""" 
		 	  
	"""),format.raw/*288.2*/("""}"""),format.raw/*288.3*/(""");
	
	
"""),format.raw/*291.1*/("""}"""),format.raw/*291.2*/("""
"""),format.raw/*292.1*/("""</script>
<script type="text/javascript">
var result = """),format.raw/*294.14*/("""{"""),format.raw/*294.15*/("""name:'psf3rdday'"""),format.raw/*294.31*/("""}"""),format.raw/*294.32*/(""";

    function assignedInteractionPSFData(name) """),format.raw/*296.47*/("""{"""),format.raw/*296.48*/("""
        """),format.raw/*297.9*/("""//alert("server side data load");
           //alert(name);
		$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
         	document.getElementById("SchedulefromBillDate").style.display = "block";
    	document.getElementById("ScheduletoBillDate").style.display = "block";
    	document.getElementById("fromCallDate").style.display = "none";
		    	document.getElementById("toCallDate").style.display = "none";


    	var table=  $('#assignedInteractionPSFData').dataTable("""),format.raw/*309.61*/("""{"""),format.raw/*309.62*/("""
            """),format.raw/*310.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax":"/assignedInteractionTablePSFData"+"/"+name+""
            
        """),format.raw/*319.9*/("""}"""),format.raw/*319.10*/(""");
        FilterOptionData(table);
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*322.65*/("""{"""),format.raw/*322.66*/("""
            """),format.raw/*323.13*/("""$($.fn.dataTable.tables(true)).DataTable()
               .columns.adjust();
         """),format.raw/*325.10*/("""}"""),format.raw/*325.11*/(""");


    """),format.raw/*328.5*/("""}"""),format.raw/*328.6*/("""

   
    """),format.raw/*331.5*/("""function ajaxCallForFollowUpRequiredPSFData(name) """),format.raw/*331.55*/("""{"""),format.raw/*331.56*/("""
    	"""),format.raw/*332.6*/("""var psfDay = 'psf3rdday'; 
        //alert("follow up server side data");
        $("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
        document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
    	var table=   $('#ajaxCallForFollowUpRequiredPSFData').dataTable("""),format.raw/*342.70*/("""{"""),format.raw/*342.71*/("""
            """),format.raw/*343.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForFollowUpRequiredPSFData"+"/"+psfDay+""
        """),format.raw/*351.9*/("""}"""),format.raw/*351.10*/(""");
        FilterOptionData(table);
        
    """),format.raw/*354.5*/("""}"""),format.raw/*354.6*/("""



    """),format.raw/*358.5*/("""function ajaxCallForIncompletedSurveyPSFData(name) """),format.raw/*358.56*/("""{"""),format.raw/*358.57*/("""
    	"""),format.raw/*359.6*/("""var psfDay = 'psf3rdday';
    	$("#fromCalldaterange22").val('');
     	$("#tobCalldaterange33").val('');
     	$("#frombilldaterange").val('');
     	$("#tobilldaterange").val('');
        document.getElementById("SchedulefromBillDate").style.display = "none";
     	document.getElementById("ScheduletoBillDate").style.display = "none";
     	document.getElementById("fromCallDate").style.display = "block";
     	document.getElementById("toCallDate").style.display = "block";
       var table= $('#ajaxCallForIncompletedSurveyPSFData').dataTable("""),format.raw/*368.71*/("""{"""),format.raw/*368.72*/("""
            """),format.raw/*369.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForIncompletedSurveyPSFData"+"/"+psfDay+""
        """),format.raw/*377.9*/("""}"""),format.raw/*377.10*/(""");
        FilterOptionData(table);

    """),format.raw/*380.5*/("""}"""),format.raw/*380.6*/("""

    """),format.raw/*382.5*/("""function ajaxCallForCompletedSurveyPSFData(name) """),format.raw/*382.54*/("""{"""),format.raw/*382.55*/("""
    	"""),format.raw/*383.6*/("""var psfDay = 'psf3rdday'; 
    	$("#fromCalldaterange22").val('');
     	$("#tobCalldaterange33").val('');
     	$("#frombilldaterange").val('');
     	$("#tobilldaterange").val('');
        document.getElementById("SchedulefromBillDate").style.display = "none";
     	document.getElementById("ScheduletoBillDate").style.display = "none";
     	document.getElementById("fromCallDate").style.display = "block";
     	document.getElementById("toCallDate").style.display = "block";
        var table=$('#ajaxCallForCompletedSurveyPSFData').dataTable("""),format.raw/*392.69*/("""{"""),format.raw/*392.70*/("""
            """),format.raw/*393.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForCompletedSurveyPSFData"+"/"+psfDay+""
        """),format.raw/*401.9*/("""}"""),format.raw/*401.10*/(""");
        FilterOptionData(table);
    """),format.raw/*403.5*/("""}"""),format.raw/*403.6*/("""
    """),format.raw/*404.5*/("""function ajaxCallForAppointmentPSFData(name) """),format.raw/*404.50*/("""{"""),format.raw/*404.51*/("""
    	"""),format.raw/*405.6*/("""$("#fromCalldaterange22").val('');
    	$("#tobCalldaterange33").val('');
    	$("#frombilldaterange").val('');
    	$("#tobilldaterange").val('');
        document.getElementById("SchedulefromBillDate").style.display = "none";
    	document.getElementById("ScheduletoBillDate").style.display = "none";
    	document.getElementById("fromCallDate").style.display = "block";
    	document.getElementById("toCallDate").style.display = "block";
        
       var psfDay = 'psf3rdday'; 
       var table =  $('#ajaxCallForAppointmentPSFData').dataTable("""),format.raw/*415.67*/("""{"""),format.raw/*415.68*/("""
            """),format.raw/*416.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForAppointmentPSFData"+"/"+psfDay+""
        """),format.raw/*424.9*/("""}"""),format.raw/*424.10*/(""");
       FilterOptionData(table);
    """),format.raw/*426.5*/("""}"""),format.raw/*426.6*/("""

    
    """),format.raw/*429.5*/("""function ajaxCallForNonContactsPSFData(name) """),format.raw/*429.50*/("""{"""),format.raw/*429.51*/("""
    	"""),format.raw/*430.6*/("""var psfDay = 'psf3rdday'; 
    	$("#fromCalldaterange22").val('');
     	$("#tobCalldaterange33").val('');
     	$("#frombilldaterange").val('');
     	$("#tobilldaterange").val('');
        document.getElementById("SchedulefromBillDate").style.display = "none";
     	document.getElementById("ScheduletoBillDate").style.display = "none";
     	document.getElementById("fromCallDate").style.display = "block";
     	document.getElementById("toCallDate").style.display = "block";
       var table= $('#ajaxCallForNonContactsPSFDataCalls').dataTable("""),format.raw/*439.70*/("""{"""),format.raw/*439.71*/("""
            """),format.raw/*440.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForNonContactsPSFData"+"/"+psfDay+""
        """),format.raw/*448.9*/("""}"""),format.raw/*448.10*/(""");
       FilterOptionData(table);
    """),format.raw/*450.5*/("""}"""),format.raw/*450.6*/("""


    """),format.raw/*453.5*/("""function ajaxCallForDroppedCallsPSFData(name) """),format.raw/*453.51*/("""{"""),format.raw/*453.52*/("""
    	"""),format.raw/*454.6*/("""var psfDay = 'psf3rdday'; 
    	$("#fromCalldaterange22").val('');
     	$("#tobCalldaterange33").val('');
     	$("#frombilldaterange").val('');
     	$("#tobilldaterange").val('');
        document.getElementById("SchedulefromBillDate").style.display = "none";
     	document.getElementById("ScheduletoBillDate").style.display = "none";
     	document.getElementById("fromCallDate").style.display = "block";
     	document.getElementById("toCallDate").style.display = "block";
       var table = $('#ajaxCallForDroppedCallsPSFData').dataTable("""),format.raw/*463.67*/("""{"""),format.raw/*463.68*/("""
            """),format.raw/*464.13*/(""""bDestroy": true,
            "processing": true,
            "serverSide": true,
            "scrollY": 300,
            "paging": true,
            "searching": true,
            "ordering": false,
            "ajax": "/ajaxCallForDroppedCallsPSFData"+"/"+psfDay+""
        """),format.raw/*472.9*/("""}"""),format.raw/*472.10*/(""");
       FilterOptionData(table);
    """),format.raw/*474.5*/("""}"""),format.raw/*474.6*/("""

"""),format.raw/*476.1*/("""</script>
"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem)

  def f:((String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem) => apply(dealercode,dealerName,userName,oem)

  def ref: this.type = this

}


}

/**/
object PSFList3rdForFeedbackOfService extends PSFList3rdForFeedbackOfService_Scope0.PSFList3rdForFeedbackOfService
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:36 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/PSFList3rdForFeedbackOfService.scala.html
                  HASH: 1626319f153d87dd478d3ece39efb455ce1d4e8d
                  MATRIX: 816->1|975->65|1005->70|1076->133|1115->135|1145->139|13163->12126|13193->12128|13368->12274|13398->12275|13429->12278|13514->12334|13544->12335|13579->12342|13790->12524|13820->12525|13867->12544|13923->12572|13952->12573|13995->12588|14196->12760|14226->12761|14260->12767|14321->12799|14351->12800|14389->12810|14464->12857|14493->12858|14527->12864|14580->12889|14609->12890|14649->12902|14678->12903|14716->12913|14745->12914|14775->12916|14861->12973|14891->12974|14936->12990|14966->12991|15046->13042|15076->13043|15114->13053|15724->13634|15754->13635|15797->13649|16121->13945|16151->13946|16292->14058|16322->14059|16365->14073|16482->14161|16512->14162|16552->14174|16581->14175|16622->14188|16701->14238|16731->14239|16766->14246|17397->14848|17427->14849|17470->14863|17786->15151|17816->15152|17896->15204|17925->15205|17965->15217|18045->15268|18075->15269|18110->15276|18696->15833|18726->15834|18769->15848|19086->16137|19116->16138|19188->16182|19217->16183|19253->16191|19331->16240|19361->16241|19396->16248|19981->16804|20011->16805|20054->16819|20369->17106|20399->17107|20469->17149|20498->17150|20532->17156|20606->17201|20636->17202|20671->17209|21260->17769|21290->17770|21333->17784|21644->18067|21674->18068|21743->18109|21772->18110|21814->18124|21888->18169|21918->18170|21953->18177|22539->18734|22569->18735|22612->18749|22923->19032|22953->19033|23022->19074|23051->19075|23089->19085|23164->19131|23194->19132|23229->19139|23812->19693|23842->19694|23885->19708|24197->19992|24227->19993|24296->20034|24325->20035|24357->20039
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|286->255|287->256|292->261|292->261|293->262|294->263|294->263|295->264|301->270|301->270|303->272|305->274|305->274|307->276|312->281|312->281|313->282|313->282|313->282|314->283|315->284|315->284|316->285|317->286|317->286|319->288|319->288|322->291|322->291|323->292|325->294|325->294|325->294|325->294|327->296|327->296|328->297|340->309|340->309|341->310|350->319|350->319|353->322|353->322|354->323|356->325|356->325|359->328|359->328|362->331|362->331|362->331|363->332|373->342|373->342|374->343|382->351|382->351|385->354|385->354|389->358|389->358|389->358|390->359|399->368|399->368|400->369|408->377|408->377|411->380|411->380|413->382|413->382|413->382|414->383|423->392|423->392|424->393|432->401|432->401|434->403|434->403|435->404|435->404|435->404|436->405|446->415|446->415|447->416|455->424|455->424|457->426|457->426|460->429|460->429|460->429|461->430|470->439|470->439|471->440|479->448|479->448|481->450|481->450|484->453|484->453|484->453|485->454|494->463|494->463|495->464|503->472|503->472|505->474|505->474|507->476
                  -- GENERATED --
              */
          