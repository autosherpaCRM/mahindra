
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object searchCustomerCRE_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class searchCustomerCRE extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealercode:String,dealerName:String,userName:String,oem:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.66*/("""
"""),_display_(/*2.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*2.65*/ {_display_(Seq[Any](format.raw/*2.67*/("""

"""),format.raw/*4.1*/("""<script type="text/javascript">

 $(document).ready(function() """),format.raw/*6.31*/("""{"""),format.raw/*6.32*/("""
    """),format.raw/*7.5*/("""var table = $('#customerSearchTable').dataTable( """),format.raw/*7.54*/("""{"""),format.raw/*7.55*/("""
        """),format.raw/*8.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
        "paging": true,
        "searching":true,
        "ordering":false,
        "ajax": "/searchByCustomerv2"
        
    """),format.raw/*17.5*/("""}"""),format.raw/*17.6*/(""" """),format.raw/*17.7*/(""");

    var values=[];

  
	
   $('.filter').on('change', function() """),format.raw/*23.41*/("""{"""),format.raw/*23.42*/("""
	   
	   """),format.raw/*25.5*/("""var i= $(this).data('columnIndex');
	   var v = $(this).val();
	   if(v.length>0)"""),format.raw/*27.19*/("""{"""),format.raw/*27.20*/("""
		  """),format.raw/*28.5*/("""if(i < 9)
		   values[i] = v;
		  else if(i == 9)"""),format.raw/*30.20*/("""{"""),format.raw/*30.21*/("""			  
			  """),format.raw/*31.6*/("""values[9] = v;
			  values[4] = values[4]+","+values[9];"""),format.raw/*32.42*/("""}"""),format.raw/*32.43*/("""
		   
	   """),format.raw/*34.5*/("""}"""),format.raw/*34.6*/("""
	   
	   	"""),format.raw/*36.6*/("""console.log(JSON.stringify(values));
	   	
     	  //clear global search values
    	  //table.api().search('');
    	  $(this).data('columnIndex');
    	  
    	  
   """),format.raw/*43.4*/("""}"""),format.raw/*43.5*/(""");


   $('#searchCust').on('click',function()"""),format.raw/*46.42*/("""{"""),format.raw/*46.43*/("""

	  """),format.raw/*48.4*/("""// alert(values.length);
	   if(values.length>0) """),format.raw/*49.25*/("""{"""),format.raw/*49.26*/("""
			"""),format.raw/*50.4*/("""for(var i=0;i<values.length;i++)"""),format.raw/*50.36*/("""{"""),format.raw/*50.37*/("""
				
			   """),format.raw/*52.7*/("""table.api().columns(i).search(values[i]);
			"""),format.raw/*53.4*/("""}"""),format.raw/*53.5*/("""
			"""),format.raw/*54.4*/("""table.api().draw();
	   """),format.raw/*55.5*/("""}"""),format.raw/*55.6*/("""
	"""),format.raw/*56.2*/("""}"""),format.raw/*56.3*/("""); 

   $('#saveSearchbtn').on('click',function()"""),format.raw/*58.45*/("""{"""),format.raw/*58.46*/("""	  
	  """),format.raw/*59.4*/("""var searchnamesave = document.getElementById('searchnamesave').value;
      var urlDisposition = "/checkIfSearchNameExists/" + searchnamesave + "";
          $.ajax("""),format.raw/*61.18*/("""{"""),format.raw/*61.19*/("""
        """),format.raw/*62.9*/("""url: urlDisposition

    """),format.raw/*64.5*/("""}"""),format.raw/*64.6*/(""").done(function (data) """),format.raw/*64.29*/("""{"""),format.raw/*64.30*/("""
	"""),format.raw/*65.2*/("""console.log("data:"+data);
	
        if(data == 0)"""),format.raw/*67.22*/("""{"""),format.raw/*67.23*/("""
        	"""),format.raw/*68.10*/("""if(values.length>0) """),format.raw/*68.30*/("""{"""),format.raw/*68.31*/("""

                
    			"""),format.raw/*71.8*/("""for(var i=0;i<values.length;i++)"""),format.raw/*71.40*/("""{"""),format.raw/*71.41*/("""
    				
    			   """),format.raw/*73.11*/("""table.api().columns(i).search(values[i]);
    			"""),format.raw/*74.8*/("""}"""),format.raw/*74.9*/("""
    			"""),format.raw/*75.8*/("""table.api().draw();
    			Lobibox.notify('success', """),format.raw/*76.34*/("""{"""),format.raw/*76.35*/("""
    	            """),format.raw/*77.18*/("""msg: 'Sucessfully saved....'
    	        """),format.raw/*78.14*/("""}"""),format.raw/*78.15*/(""");
    			
    	   """),format.raw/*80.9*/("""}"""),format.raw/*80.10*/("""
        """),format.raw/*81.9*/("""}"""),format.raw/*81.10*/("""else"""),format.raw/*81.14*/("""{"""),format.raw/*81.15*/("""
        	"""),format.raw/*82.10*/("""Lobibox.notify('warning', """),format.raw/*82.36*/("""{"""),format.raw/*82.37*/("""
	            """),format.raw/*83.14*/("""msg: 'Saved Search name already exists....'
	        """),format.raw/*84.10*/("""}"""),format.raw/*84.11*/(""");
        """),format.raw/*85.9*/("""}"""),format.raw/*85.10*/("""
        
    """),format.raw/*87.5*/("""}"""),format.raw/*87.6*/(""");
   """),format.raw/*88.4*/("""}"""),format.raw/*88.5*/(""");
     
   
   
   $('#clearFilters').on('click',function()"""),format.raw/*92.44*/("""{"""),format.raw/*92.45*/("""

	 
	
		"""),format.raw/*96.3*/("""$('.filter').each(function()"""),format.raw/*96.31*/("""{"""),format.raw/*96.32*/("""
				"""),format.raw/*97.5*/("""$(this).val('');
			"""),format.raw/*98.4*/("""}"""),format.raw/*98.5*/(""");
		
		table = $('#customerSearchTable').dataTable( """),format.raw/*100.48*/("""{"""),format.raw/*100.49*/("""
	        """),format.raw/*101.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
	        "paging": true,
	        "searching":true,
	        "ordering":false,
	        "ajax": "/searchByCustomerv2"
	        
	    """),format.raw/*110.6*/("""}"""),format.raw/*110.7*/(""" """),format.raw/*110.8*/(""");
	   
	"""),format.raw/*112.2*/("""}"""),format.raw/*112.3*/(""");
  
    
 
"""),format.raw/*116.1*/("""}"""),format.raw/*116.2*/(""");
</script>	
	
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading"><b>Customer Search</b></div>
      <div class="panel-body">
	  <div class="panel panel-default">
	  <div class="panel-body">
      <div class="row">
      	<div class="col-md-12">
      		
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Name</label>
      						<input type="text" class="filter form-control" data-column-index="0" placeholder="Search Name" />
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Vehicle</label>
      						<input type="text" class="filter form-control" data-column-index="1" placeholder="Search Vehicle" />
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Category</label>
      						<input type="text" class="filter form-control" data-column-index="2" placeholder="Search Category" />
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Phone</label>
      						<input type="text" class="filter form-control" data-column-index="3" placeholder="Search Phone" />
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Service Date From</label>
      					<input type="text" class="form-control" data-column-index="4" placeholder="Search Sercie Date" />	
      				</div>
      			</div>
      		 <div class="col-md-2">
      				<div class="form-group">
      				<label>Search Service Date To</label>
      					<input type="text" class="form-control" data-column-index="9" placeholder="Search Sercie Date" />	
      				</div>
      			</div>
      			
      			
      			
      	</div>
      </div>
  
      <div class="row">
      	<div class="col-md-12">
      		<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Model</label>
      					<input type="text" class="filter form-control" data-column-index="5" placeholder="Search Model" />	
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Variant</label>
      					<input type="text" class="filter form-control" data-column-index="6" placeholder="Search Variant" />
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search disposition</label>
      					<input type="text" class="filter form-control" data-column-index="7" placeholder="Search disposition" />	
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>&nbsp;</label>
      					 <button type="submit" id="searchCust" class="btn btn-primary start btn-block">
                    			<i class="fa fa-search"></i>
                    			<span>Search</span>
        				</button>
						</div>
						</div>
						<div class="col-md-2">
      				<div class="form-group">
      				<label>&nbsp;</label>
        				 <button type="submit" id="clearFilters" class="btn btn-primary start btn-block">
                    			<i class="fa fa-eraser"></i>
                    			<span>Clear</span>
        				</button>
 	
      				</div>
      			</div>
      			 
      	</div>
      </div>
      </div>
      </div>
      
       
		  <div class="panel panel-default">
		  <div class="panel-body">
      <div class="dataTable_wrapper">
      <table class="table table-striped table-bordered table-hover" id="customerSearchTable">
      
       
                 <thead>
                                 <tr>
                                    <th>Customer Name</th>
                                    <th>Vehicle RegNo.</th>
                                    <th>Category</th>                                   
                                    <th>Phone Number</th>                                    
                                    <th>Next Service Date</th>
                                    <th>Model</th>
                                    <th>Variant</th>
                                    <th>Last Disposition</th>
                                    <th></th>
                                 </tr>
                              </thead>

                                <tbody>
                                   
                   				</tbody>
           </table>
        </div>
        </div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<div class="col-md-2">
        			<div class="form-group">
        			<label>Saved Search</label>
        			<input type="text"  id ="searchnamesave" class="filter form-control" data-column-index="8" placeholder="Search Name">
        			</div>
        		</div>
        		<div class="col-md-2">
        			<div class="form-group">
        			<label>&nbsp;</label> <br />
        			  <button type="submit" id="saveSearchbtn" class="btn btn-primary start">
                    <i class="fa floppy-o"></i>
                    <span>Save</span>
        </button>
        			</div>
        		</div>
        	</div>
        </div>
     
    
       </div>
    </div>
  </div>
</div>

""")))}))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem)

  def f:((String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem) => apply(dealercode,dealerName,userName,oem)

  def ref: this.type = this

}


}

/**/
object searchCustomerCRE extends searchCustomerCRE_Scope0.searchCustomerCRE
              /*
                  -- GENERATED --
                  DATE: Tue Dec 26 13:38:06 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/searchCustomerCRE.scala.html
                  HASH: 606c9d5fea086193d8b61d798316ed8023a5f6ee
                  MATRIX: 790->1|949->65|977->68|1048->131|1087->133|1117->137|1209->202|1237->203|1269->209|1345->258|1373->259|1409->269|1670->503|1698->504|1726->505|1829->580|1858->581|1897->593|2008->676|2037->677|2070->683|2149->734|2178->735|2217->747|2302->804|2331->805|2371->818|2399->819|2439->832|2641->1007|2669->1008|2746->1057|2775->1058|2809->1065|2887->1115|2916->1116|2948->1121|3008->1153|3037->1154|3078->1168|3151->1214|3179->1215|3211->1220|3263->1245|3291->1246|3321->1249|3349->1250|3428->1301|3457->1302|3492->1310|3687->1477|3716->1478|3753->1488|3807->1515|3835->1516|3886->1539|3915->1540|3945->1543|4025->1595|4054->1596|4093->1607|4141->1627|4170->1628|4226->1657|4286->1689|4315->1690|4365->1712|4442->1762|4470->1763|4506->1772|4588->1826|4617->1827|4664->1846|4735->1889|4764->1890|4812->1911|4841->1912|4878->1922|4907->1923|4939->1927|4968->1928|5007->1939|5061->1965|5090->1966|5133->1981|5215->2035|5244->2036|5283->2048|5312->2049|5355->2065|5383->2066|5417->2073|5445->2074|5537->2138|5566->2139|5606->2152|5662->2180|5691->2181|5724->2187|5772->2208|5800->2209|5884->2264|5914->2265|5954->2276|6225->2519|6254->2520|6283->2521|6322->2532|6351->2533|6396->2550|6425->2551
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|37->6|37->6|38->7|38->7|38->7|39->8|48->17|48->17|48->17|54->23|54->23|56->25|58->27|58->27|59->28|61->30|61->30|62->31|63->32|63->32|65->34|65->34|67->36|74->43|74->43|77->46|77->46|79->48|80->49|80->49|81->50|81->50|81->50|83->52|84->53|84->53|85->54|86->55|86->55|87->56|87->56|89->58|89->58|90->59|92->61|92->61|93->62|95->64|95->64|95->64|95->64|96->65|98->67|98->67|99->68|99->68|99->68|102->71|102->71|102->71|104->73|105->74|105->74|106->75|107->76|107->76|108->77|109->78|109->78|111->80|111->80|112->81|112->81|112->81|112->81|113->82|113->82|113->82|114->83|115->84|115->84|116->85|116->85|118->87|118->87|119->88|119->88|123->92|123->92|127->96|127->96|127->96|128->97|129->98|129->98|131->100|131->100|132->101|141->110|141->110|141->110|143->112|143->112|147->116|147->116
                  -- GENERATED --
              */
          