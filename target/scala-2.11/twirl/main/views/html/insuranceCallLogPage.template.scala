
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object insuranceCallLogPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class insuranceCallLogPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template10[String,String,String,String,String,String,String,String,String,List[Campaign],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(toActivateTab1:String,toActivateTab2:String,toActivateTab3:String,toActivateTab4:String,toActivateTab5:String,userName:String,dealerName:String,dealercode:String,oem:String,campaignList:List[Campaign]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.204*/("""
"""),_display_(/*2.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*2.65*/ {_display_(Seq[Any](format.raw/*2.67*/("""
	
	"""),format.raw/*4.2*/("""<input type="hidden" id="user" value=""""),_display_(/*4.41*/userName),format.raw/*4.49*/("""">

<input type="hidden" id="toActivateInsTab1" value=""""),_display_(/*6.53*/toActivateTab1),format.raw/*6.67*/("""">
<input type="hidden" id="toActivateInsTab2" value=""""),_display_(/*7.53*/toActivateTab2),format.raw/*7.67*/("""">
<input type="hidden" id="toActivateInsTab3" value=""""),_display_(/*8.53*/toActivateTab3),format.raw/*8.67*/("""">
<input type="hidden" id="toActivateInsTab4" value=""""),_display_(/*9.53*/toActivateTab4),format.raw/*9.67*/("""">
<input type="hidden" id="toActivateInsTab5" value=""""),_display_(/*10.53*/toActivateTab5),format.raw/*10.67*/("""">
	


<div class="row" >
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">  <b>INSURANCE CALL LOG</b> </div>
            <div class="panel-body" style="overflow-x: auto;">
            
            
            <div class="panel panel-default">
             <ul class="nav nav-tabs">
                    <li ><a href="#homeIns" data-toggle="tab" id="TabIns1" onclick="ajaxInsuranceRemainder();">Scheduled calls</a> </li>
                    <li ><a href="#profileIns" data-toggle="tab" id="TabIns2" onclick="ajaxInsuranceDisposition('4');">Follow Up Required</a> </li>
                    <li ><a href="#messagesIns" data-toggle="tab" id="TabIns3" onclick="ajaxInsuranceDisposition('25');">Appointments</a> </li>
                    <li ><a href="#settingsIns" data-toggle="tab" id="TabIns4" onclick="ajaxInsuranceDisposition('26');">Renewal Not Required</a> </li>
                    <li ><a href="#nonContactsIns" data-toggle="tab" id="TabIns5" onclick="ajaxInsuranceNonContactDispo('1');">Non Contacts</a> </li>
                    <li ><a href="#droppedBucIns" data-toggle="tab" id="TabIns6" onclick="ajaxInsuranceNonContactDispo('2');">Dropped</a> </li>
                </ul>
            
            
            <div class="tab-content">
			 <div class="panel panel-default" style="border-top: #fff;">
				<div class="panel-body">
            <div class="row">
            <div class="col-md-3">
            
               <label>Select Campaign</label>
						<select class="filter form-control" id="campaignName" data-column-index="3" name="campaignName">
							<option value="0" >--Select--</option>
							"""),_display_(/*41.9*/for(campaign_List<-campaignList) yield /*41.41*/{_display_(Seq[Any](format.raw/*41.42*/("""
		                                """),format.raw/*42.35*/("""<option value=""""),_display_(/*42.51*/campaign_List/*42.64*/.getId()),format.raw/*42.72*/("""">"""),_display_(/*42.75*/campaign_List/*42.88*/.getCampaignName()),format.raw/*42.106*/("""</option>
		                             """)))}),format.raw/*43.33*/("""
		                                
						"""),format.raw/*45.7*/("""</select>
            
            </div>
            
					<div class="col-md-3" id="SchedulefromBillDate">
					<label>From Due Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="Enter From Due Date" data-column-index="1" id="frombilldaterange" name="frombilldaterange" readonly>
				  </div>
				   <div class="col-md-3" id="ScheduletoBillDate">
					<label>To Due Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="Enter To Due Date" data-column-index="2" id="tobilldaterange" name="tobilldaterange" readonly>
				  </div>
				
				
					<div class="col-md-3">
				  <label for="">Renewal Type</label>
				  <select class="filter form-control"  name="renewalType" data-column-index="4">
						<option value="0">--Select--</option>
						<option value="1st Renewal">1st Renewal</option>
						<option value="2nd Renewal">2nd Renewal</option>
						<option value="3rd Renewal">3rd Renewal</option>
						<option value="4th Renewal">4th Renewal</option>
						<option value="5th Renewal">5th Renewal</option>
						<option value="6th Renewal">6th Renewal</option>
						<option value="7th Renewal">7th Renewal</option>
						<option value="8th Renewal">8th Renewal</option>
					 </select>
				</div>
				</div>
				</div>
				</div>
                    <div class="panel panel-default tab-pane fade """),_display_(/*76.68*/toActivateTab1),format.raw/*76.82*/("""" id="homeIns">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                               	
                <table id="assignedInsuInteraction" class="table table-striped table-bordered" cellspacing="0">
                    <thead>
                        <tr>
                                                            <th class="no-sort"></th>

                       
                            					<th>Campaign</th>
                                             	<th>Customer Name</th>
                                             	<th>Mobile No.</th>
                                             	<th>RegNo.</th>  
                                             	<th>Chassis No</th>                                             
                                                <th>Policy Due Month</th>
                                                <th>Policy Due Date</th>
                                                <th>Next Renewal Type</th>
                                                <th>Last Insurance Company</th>                                                
												<th class="no-sort"></th>
                        </tr>
                    </thead>

                    <tbody>
                        
                    </tbody>
                </table>
            
            
            </div>
            </div>
            </div>
           
                    <div class="panel panel-default tab-pane fade """),_display_(/*110.68*/toActivateTab2),format.raw/*110.82*/("""" id="profileIns">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                
                <table id="dispoInsuInteraction4" class="table table-striped table-bordered" cellspacing="0">
                  <thead>
                                         <tr> 
                                      	
                                                                                          <th class="no-sort"></th>
  
                                                <th>Campaign Name</th>  
                                                <th>Call Date</th>
                                                <th>Customer Name</th>                       	
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                                <th>Policy Due Date</th>
                                                <th>Renewal Type</th>
                                                <th>FollowUp Date</th>
                                                <th>FollowUp Time</th>
                                               	<th></th>
                                               	<th>Last Disposition</th>
                                                <th class="no-sort"></th>
                                            </tr>   
                                        </thead>
                                        <tbody>

                                        </tbody> 
                </table>
           
            
            </div>
            </div>
            </div>
            
                    <div class="panel panel-default tab-pane fade """),_display_(/*145.68*/toActivateTab3),format.raw/*145.82*/("""" id="messagesIns">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                
                <table id="dispoInsuInteraction25" class="table table-striped table-bordered nowrap" cellspacing="0">
                
                 <thead>
                                            <tr> 
                                                                                   <th class="no-sort"></th>

												<th>Campaign Name</th>
                                                <th>Call Date</th>
                                               	<th>Customer Name</th>
                                                <th>Mobile Number</th>                                                                   
                                                <th>Vehicle RegNo.</th>                                               
                                                <th>Policy Due Date</th>
                                                <th>Renewal Type</th>
                                                <th>Appointment Date</th>
                                                <th>Appointment Time</th>
                                                <th>Insurance Agent</th>
                                                <th>Appointment Status</th>
                                                <th>Last Disposition</th>                                   
                                                <th class="no-sort"></th>
                                                  
                                                



                                            </tr>
                                        </thead>
                                        <tbody>




                                        </tbody>
                    
                </table>
           
            </div>
            </div>
            </div>

                    <div class="panel panel-default tab-pane fade """),_display_(/*189.68*/toActivateTab4),format.raw/*189.82*/("""" id="settingsIns">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                	
                <table id="dispoInsuInteraction26" class="table table-striped table-bordered" cellspacing="0">
                   <thead>
                                        <tr> 
                                                                                         <th class="no-sort"></th>
                  	
                                          		<th>Campaign Name</th>  
                                                <th>Call Date</th>
                                                <th>Customer Name</th>                       	
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                                <th>Policy Due Date</th>
                                                <th>Renewal Type</th>
                                               	<th>Last Disposition</th>
                                                <th>Reason</th>  
                                                <th class="no-sort"></th>



                                        </tr>
                                    </thead>
                                    <tbody>




                                    </tbody>
                </table>
         
            
            </div>
            </div>
            </div>
          
                    <div class="panel panel-default tab-pane fade """),_display_(/*227.68*/toActivateTab5),format.raw/*227.82*/("""" id="nonContactsIns">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                
								
                <table id="dispoInsuNonContact1" class="table table-striped table-bordered" width="100%">
                    
                    <thead>
                                        <tr> 
                                            <th class="no-sort"></th>
											<th>Campaign Name</th>
											<th>Call Date</th>
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Policy Due Date</th>
                                            <th>Last Disposition</th>
                                            <th class="no-sort"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                </table>
         
		 
            
            </div>
            </div>
            </div>

                    <div class="panel panel-default tab-pane fade" id="droppedBucIns">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                
                <table id="dispoInsuNonContact2" class="table table-striped table-bordered" width="100%">
                    
                    <thead>
                                            <tr> 
			                                            <th class="no-sort"></th>

												<th>Campaign Name</th>
												<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                               <th>Policy Due Date</th>
                                                <th>Last Disposition</th>
                                                <th>No. of Attempts</th>
                                            <th class="no-sort"></th>
												




                                            </tr>
                                        </thead>
                                        <tbody>



                                        </tbody>
                </table>
           
            </div>
            
            </div>
            </div>
            </div>
            
            
            </div>
            </div>
            </div>
            </div>



""")))}),format.raw/*306.2*/("""

"""),format.raw/*308.1*/("""<script>

function FilterOptionData(table)"""),format.raw/*310.33*/("""{"""),format.raw/*310.34*/("""
	"""),format.raw/*311.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*312.39*/("""{"""),format.raw/*312.40*/("""
    	"""),format.raw/*313.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
		console.log(v);
		console.log(i);
	    
	    if(v.length>0)"""),format.raw/*319.20*/("""{"""),format.raw/*319.21*/("""
		    	   
		   """),format.raw/*321.6*/("""values[i] = v;
	    
	    """),format.raw/*323.6*/("""}"""),format.raw/*323.7*/("""
	    
	    	"""),format.raw/*325.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');

	 	 if(values.length>0) """),format.raw/*330.25*/("""{"""),format.raw/*330.26*/("""
			 """),format.raw/*331.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*331.37*/("""{"""),format.raw/*331.38*/("""	
			    """),format.raw/*332.8*/("""table.api().columns(i).search(values[i]);
			 """),format.raw/*333.5*/("""}"""),format.raw/*333.6*/("""
			 """),format.raw/*334.5*/("""table.api().draw();
			 """),format.raw/*335.5*/("""}"""),format.raw/*335.6*/(""" 
		 	  
	"""),format.raw/*337.2*/("""}"""),format.raw/*337.3*/(""");
	
	
"""),format.raw/*340.1*/("""}"""),format.raw/*340.2*/("""


"""),format.raw/*343.1*/("""</script>
<script type="text/javascript">
      
 function ajaxInsuranceRemainder()"""),format.raw/*346.35*/("""{"""),format.raw/*346.36*/("""
    """),format.raw/*347.5*/("""//alert("server side data load");
    
     var table=$('#assignedInsuInteraction').dataTable( """),format.raw/*349.57*/("""{"""),format.raw/*349.58*/("""
        """),format.raw/*350.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": "/insurance/assignedInsuInteraction",
		 "fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*359.60*/("""{"""),format.raw/*359.61*/("""
              """),format.raw/*360.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*362.15*/("""}"""),format.raw/*362.16*/("""
    """),format.raw/*363.5*/("""}"""),format.raw/*363.6*/(""" """),format.raw/*363.7*/(""");
    FilterOptionData(table);
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*366.61*/("""{"""),format.raw/*366.62*/("""
      """),format.raw/*367.7*/("""$($.fn.dataTable.tables(true)).DataTable()
         .columns.adjust();
   """),format.raw/*369.4*/("""}"""),format.raw/*369.5*/(""");
    
    """),format.raw/*371.5*/("""}"""),format.raw/*371.6*/("""

 """),format.raw/*373.2*/("""function ajaxInsuranceDisposition(typeOfdispo)"""),format.raw/*373.48*/("""{"""),format.raw/*373.49*/("""
	"""),format.raw/*374.2*/("""// alert(typeOfdispo);
	var table=$('#dispoInsuInteraction'+typeOfdispo).dataTable( """),format.raw/*375.62*/("""{"""),format.raw/*375.63*/("""
        """),format.raw/*376.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": "/insurance/contactedDispoInteractions/"+typeOfdispo+"",
		 "fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*385.60*/("""{"""),format.raw/*385.61*/("""
              """),format.raw/*386.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*388.15*/("""}"""),format.raw/*388.16*/("""
            
    	"""),format.raw/*390.6*/("""}"""),format.raw/*390.7*/(""");
    FilterOptionData(table);
    
	 """),format.raw/*393.3*/("""}"""),format.raw/*393.4*/("""

"""),format.raw/*395.1*/("""function ajaxInsuranceNonContactDispo(typeOfdispo)"""),format.raw/*395.51*/("""{"""),format.raw/*395.52*/("""
	"""),format.raw/*396.2*/("""var table=$('#dispoInsuNonContact'+typeOfdispo).dataTable( """),format.raw/*396.61*/("""{"""),format.raw/*396.62*/("""
        """),format.raw/*397.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": "/insurance/nonContactedDispoInterac/"+typeOfdispo+"",
		 "fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*406.60*/("""{"""),format.raw/*406.61*/("""
              """),format.raw/*407.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*409.15*/("""}"""),format.raw/*409.16*/("""
            
    """),format.raw/*411.5*/("""}"""),format.raw/*411.6*/(""");
    FilterOptionData(table);
	
"""),format.raw/*414.1*/("""}"""),format.raw/*414.2*/("""
 
 """),format.raw/*416.2*/("""</script>
 
 <script type="text/javascript">
    window.onload = ajaxRequestDataOFInsuranceIndexPageWithTab;
</script>"""))
      }
    }
  }

  def render(toActivateTab1:String,toActivateTab2:String,toActivateTab3:String,toActivateTab4:String,toActivateTab5:String,userName:String,dealerName:String,dealercode:String,oem:String,campaignList:List[Campaign]): play.twirl.api.HtmlFormat.Appendable = apply(toActivateTab1,toActivateTab2,toActivateTab3,toActivateTab4,toActivateTab5,userName,dealerName,dealercode,oem,campaignList)

  def f:((String,String,String,String,String,String,String,String,String,List[Campaign]) => play.twirl.api.HtmlFormat.Appendable) = (toActivateTab1,toActivateTab2,toActivateTab3,toActivateTab4,toActivateTab5,userName,dealerName,dealercode,oem,campaignList) => apply(toActivateTab1,toActivateTab2,toActivateTab3,toActivateTab4,toActivateTab5,userName,dealerName,dealercode,oem,campaignList)

  def ref: this.type = this

}


}

/**/
object insuranceCallLogPage extends insuranceCallLogPage_Scope0.insuranceCallLogPage
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:27 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/insuranceCallLogPage.scala.html
                  HASH: 66c92ed8ae2b7f80985d5d1a504ea34285912700
                  MATRIX: 847->1|1145->203|1173->206|1244->269|1283->271|1315->277|1380->316|1408->324|1492->382|1526->396|1608->452|1642->466|1724->522|1758->536|1840->592|1874->606|1957->662|1992->676|3728->2386|3776->2418|3815->2419|3879->2455|3922->2471|3944->2484|3973->2492|4003->2495|4025->2508|4065->2526|4139->2569|4210->2613|5664->4040|5699->4054|7332->5659|7368->5673|9255->7532|9291->7546|11439->9666|11475->9680|13188->11365|13224->11379|16197->14321|16229->14325|16302->14369|16332->14370|16363->14373|16448->14429|16478->14430|16513->14437|16724->14619|16754->14620|16801->14639|16857->14667|16886->14668|16929->14683|17130->14855|17160->14856|17194->14862|17255->14894|17285->14895|17323->14905|17398->14952|17427->14953|17461->14959|17514->14984|17543->14985|17583->14997|17612->14998|17650->15008|17679->15009|17713->15015|17828->15101|17858->15102|17892->15108|18018->15205|18048->15206|18086->15216|18433->15534|18463->15535|18508->15551|18615->15629|18645->15630|18679->15636|18708->15637|18737->15638|18866->15738|18896->15739|18932->15747|19036->15823|19065->15824|19107->15838|19136->15839|19169->15844|19244->15890|19274->15891|19305->15894|19419->15979|19449->15980|19487->15990|19853->16327|19883->16328|19928->16344|20035->16422|20065->16423|20114->16444|20143->16445|20213->16487|20242->16488|20274->16492|20353->16542|20383->16543|20414->16546|20502->16605|20532->16606|20570->16616|20934->16951|20964->16952|21009->16968|21116->17046|21146->17047|21194->17067|21223->17068|21288->17105|21317->17106|21351->17112
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|35->4|35->4|37->6|37->6|38->7|38->7|39->8|39->8|40->9|40->9|41->10|41->10|72->41|72->41|72->41|73->42|73->42|73->42|73->42|73->42|73->42|73->42|74->43|76->45|107->76|107->76|141->110|141->110|176->145|176->145|220->189|220->189|258->227|258->227|337->306|339->308|341->310|341->310|342->311|343->312|343->312|344->313|350->319|350->319|352->321|354->323|354->323|356->325|361->330|361->330|362->331|362->331|362->331|363->332|364->333|364->333|365->334|366->335|366->335|368->337|368->337|371->340|371->340|374->343|377->346|377->346|378->347|380->349|380->349|381->350|390->359|390->359|391->360|393->362|393->362|394->363|394->363|394->363|397->366|397->366|398->367|400->369|400->369|402->371|402->371|404->373|404->373|404->373|405->374|406->375|406->375|407->376|416->385|416->385|417->386|419->388|419->388|421->390|421->390|424->393|424->393|426->395|426->395|426->395|427->396|427->396|427->396|428->397|437->406|437->406|438->407|440->409|440->409|442->411|442->411|445->414|445->414|447->416
                  -- GENERATED --
              */
          