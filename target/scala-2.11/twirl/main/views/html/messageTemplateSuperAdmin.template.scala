
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object messageTemplateSuperAdmin_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class messageTemplateSuperAdmin extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[String,String,String,String,List[SMSTemplate],List[MessageParameters],List[SavedSearchResult],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(dealercode:String,dealerName:String,userName:String,oem:String,smsTemplateList :List[SMSTemplate],msgParameters :List[MessageParameters], savedSearchList :List[SavedSearchResult]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.182*/("""

"""),_display_(/*5.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*5.56*/ {_display_(Seq[Any](format.raw/*5.58*/("""
"""),format.raw/*6.1*/("""<link rel="stylesheet" href=""""),_display_(/*6.31*/routes/*6.37*/.Assets.at("css/gsdk-base.css")),format.raw/*6.68*/("""">

<div class="container-fluid">
  <div class="panel panel-primary">
    <div class="panel-heading" style="text-align:center;"><strong>SEARCH CUSTOMER</strong> </div>
    <div class="panel-body">
      
      <!---------------------Wizard1----------------------------------------->
      <div class="wizard-container">
        <div class="card wizard-card ct-wizard-green" id="wizard"> 
          
          <!--        You can switch "ct-wizard-azzure"  with one of the next bright colors: "ct-wizard-blue", "ct-wizard-green", "ct-wizard-orange", "ct-wizard-red"             -->
          
          <ul>
            <li><a href="#step1" data-toggle="tab" class="tabRound" >SEARCH</a></li>
            <li><a href="#step2" data-toggle="tab" class="tabRound">MESSAGE</a></li>
            <li><a href="#step14" data-toggle="tab" class="tabRound">LIST</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane animated  bounceInRight" id="step1"> 
              
              <!-------------------------------Step1------------------------------------->
              <div class="panel panel-primary" >
                <div class="panel-heading" style="text-align:center;">Search 
                             </div>
                <div class="panel-body" style="overflow:auto">
				
				<label class="radio-inline">
				<input type="hidden" id="hiddenSelectAllFlag" value="">
      <input type="radio" name="SearchCust" value="Search Customer">Search Customer
      
    </label>
    <label class="radio-inline">
     <a href="/fileuploadPage" style="color: black;"><input type="radio" name="SearchCust" value="Upload Customer">Upload Customer</a> 
    </label> 
				
				<div style="display:none" id="SarchDivID" class="animated  bounceInRight">
                  <div class="row">
                    <div class="col-md-12">
				<div class="col-md-3">
	<div class="form-group">	<br />
	<a href="/searchByCustomerManager"><button class="btn btn-info" >Create New Search</button></a>
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
	<label>Select Saved Search</label>
	<select class="form-control" id="savedName" name="savedName">
		<option value="0" >--Select--</option>
                                """),_display_(/*55.34*/for(savedSearch_List<-savedSearchList) yield /*55.72*/{_display_(Seq[Any](format.raw/*55.73*/("""
                                """),format.raw/*56.33*/("""<option value=""""),_display_(/*56.49*/savedSearch_List/*56.65*/.getSavedName()),format.raw/*56.80*/("""">"""),_display_(/*56.83*/savedSearch_List/*56.99*/.getSavedName()),format.raw/*56.114*/("""</option>
                             """)))}),format.raw/*57.31*/("""
	"""),format.raw/*58.2*/("""</select>
  </div>
  </div>
  <br />
  <button type="button" id="SearchId" class="btn btn-info" >Get Customer</button>
  </div>
  <br />

                  </div>
                  </div>
				  <br />
				   <br />
<div style="display:none;" id="uploadDivID" class="animated  bounceInRight" >
                  <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="uploadfile" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="button" class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" class="toggle">
                <span></span>
                <!--<button class="btn btn-success wyzprocess" data-type="POST" data-url=""""),format.raw/*89.91*/("""{"""),format.raw/*89.92*/("""%=file.processUrl%"""),format.raw/*89.110*/("""}"""),format.raw/*89.111*/("""""""),format.raw/*89.112*/("""{"""),format.raw/*89.113*/("""% if (file.deleteWithCredentials) """),format.raw/*89.147*/("""{"""),format.raw/*89.148*/(""" """),format.raw/*89.149*/("""%"""),format.raw/*89.150*/("""}"""),format.raw/*89.151*/(""" """),format.raw/*89.152*/("""data-xhr-fields='"""),format.raw/*89.169*/("""{"""),format.raw/*89.170*/(""""withCredentials":true"""),format.raw/*89.192*/("""}"""),format.raw/*89.193*/("""'"""),format.raw/*89.194*/("""{"""),format.raw/*89.195*/("""% """),format.raw/*89.197*/("""}"""),format.raw/*89.198*/(""" """),format.raw/*89.199*/("""%"""),format.raw/*89.200*/("""}"""),format.raw/*89.201*/(""">
                    <i class="glyphicon glyphicon-check"></i>
                    <span>Submit For Reporting</span>
                </button>
				<input type="checkbox" class="poggle">-->
                
               <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
                  </div>
				  
				  
				
				    <div class="SearchCustId" style="display:none;" >
					 <div class="panel panel-primary" >
                <div class="panel-heading" style="text-align:center;">Search List </div>
                <div class="panel-body">
      <table class="table table-striped table-bordered table-hover" id="customerSearchTables">
                                    <thead>
                                 <tr>
                                    <th>Customer Name</th>
                                    <th>Vehicle RegNo.</th>
                                    <th>Category</th>                                   
                                    <th>Phone Number</th>                                    
                                    <th>Next Service Date</th>
                                    <th>Model</th>
                                    <th>Variant</th>
                                    <th>Last Disposition</th>
									<th>Select All this <input type="checkbox" id="selectAll"/></th>
                                 </tr>
                              </thead>
                                <tbody>
							
                   </tbody>
           </table>
        </div>
        </div>
        </div>
		
		
		
		
		<div class="UploadCustIdDiv" style="display:none;" >
		<div class="panel panel-primary" >
                <div class="panel-heading" style="text-align:center;">Upload List </div>
                <div class="panel-body">
      <table class="table table-striped table-bordered table-hover" id="customerUploadTable">
                                    <thead>
                                 <tr>
                                    
                                    <th>Customer Name</th>
                                    <th>Vehicle RegNo.</th>
                                    <th>Category</th>                                   
                                    <th>Phone Number</th>                                    
                                    <th>Next Service Date</th>
                                    <th>Model</th>
                                    <th>Variant</th>
                                    <th>Last Disposition</th>
									<th>Select All <input type="checkbox" id=""/></th>

                                    
                                 </tr>
                              </thead>
                                <tbody>
								<tr>
									<td>Rohit</td>
									<td>KA-06 N-2201</td>
									<td></td>
									<td>9879908703</td>
									<td>30/06/2017</td>
									<td>i10 Honda</td>
									<td>Blue</td>
									<td></td>
									<td><input type="checkbox" name="uploadname"/></td>

								</tr>
								
								<tr>
									<td>Lohit</td>
									<td>KA-06 N-2202</td>
									<td></td>
									<td>9879908703</td>
									<td>30/06/2017</td>
									<td>i11 Honda</td>
									<td>Red</td>
									<td></td>
									<td><input type="checkbox" name="uploadname"/></td>

								</tr>
                                   
                   </tbody>
           </table>
        </div>
        </div>
        </div>
                  <!-- <div class="row">
			<div class="col-md-12">
			<div class="pull-right">
					<button type="button" class="btn btn-success"><i class="fa fa-check-square" aria-hidden="true"></i> &nbsp;Submit</button>
			</div>
			</div>
		</div> --> 
                
                </div>
              </div>
            </div>
            <div class="tab-pane animated  bounceInRight" id="step2" > 
              <!----------------------------------Step2----------------------------->
              <div class="panel panel-primary">
                <div class="panel-heading" style="text-align:center;">MESSAGE
                 
                </div>
                <div class="panel-body">
               <div>
	<div class="row">
    <div class="col-sm-4">
     <div class="form-group">
  <label for="sel1">Message Template</label>
 <select id="msgTypeID" name="smsTemplate" class="form-control">
                               <option value="0" >--Select--</option>
                               """),_display_(/*219.33*/for(smsTemplate_List<-smsTemplateList) yield /*219.71*/{_display_(Seq[Any](format.raw/*219.72*/("""
                               """),format.raw/*220.32*/("""<option value=""""),_display_(/*220.48*/smsTemplate_List/*220.64*/.getSmsId()),format.raw/*220.75*/("""">"""),_display_(/*220.78*/smsTemplate_List/*220.94*/.getSmsTemplate()),format.raw/*220.111*/("""</option>
                            """)))}),format.raw/*221.30*/("""
    
    
  """),format.raw/*224.3*/("""</select>
  
</div>
    </div>
    
    <div class="col-sm-4">
		<div class="form-group">
  <label for="sel1">Message Parameters</label>
  <select class="form-control" id="ParameteID" >	
     <option value="0" >--Select--</option>
                                """),_display_(/*234.34*/for(msgParameters_list<-msgParameters) yield /*234.72*/{_display_(Seq[Any](format.raw/*234.73*/("""
                                """),format.raw/*235.33*/("""<option value=""""),_display_(/*235.49*/msgParameters_list/*235.67*/.getMessageParameter()),format.raw/*235.89*/("""">"""),_display_(/*235.92*/msgParameters_list/*235.110*/.getMessageParameter()),format.raw/*235.132*/("""</option>
                             """)))}),format.raw/*236.31*/("""
    
  """),format.raw/*238.3*/("""</select>
  
</div>
    </div>
     <div class="col-sm-3">
		<div class="form-group">
  <label for="sel1">SMS API</label>
  <input type="text" class="form-control" name="smsAPI" id="smsAPI">
</div>
    </div>
	<div class="col-sm-3"><br />
	<button id="AddId" class="btn btn-info">Add</button>
	
	</div>
	
  </div>
  <div class="row">
	<div class="col-sm-4">
    <div class="form-group">
  <label for="comment"><label>
  <textarea class="form-control addedTxt" rows="4" cols="90" id="txtAreaId">
  </textarea>
</div>
    </div>
  </div>
  <button class="btn btn-success" id="SaveTemp" onclick="saveNewSmsTemplate();">Save Template</button> 
  </div>
                </div>
              </div>
            </div>
            
            <!------------------------------------step14--------------------------------------------->
            <div class="tab-pane animated  bounceInRight" id="step14">
              <div class="panel panel-primary">
                <div class="panel-heading" style="text-align:center;">List</div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="selectedCustomers">
                                    <thead>
	                                 <tr>
	                                    <th>Customer Name</th>
	                                    <th>Vehicle RegNo.</th>
	                                    <th>Category</th>                                   
	                                    <th>Phone Number</th>                                    
	                                    <th>Next Service Date</th>
	                                    <th>Model</th>
	                                    <th>Variant</th>
	                                    <th>Last Disposition</th>                                    
	                                 </tr>
                              </thead>
                                <tbody>
							
                   </tbody>
           </table>
                </div>
                </div>
                
             
            </div>
          </div>
          <div class="wizard-footer">
            <div class="pull-right">
              <button step2='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' id=""  name='next' value='Next' />
              Next
              </button>
              <button step2='button' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Finish' onclick="sendBulkSMS();"/>
              <i class="fa fa-check-square" aria-hidden="true" ></i>&nbsp;Send Bulk SMS
              </button>
            </div>
            <div class="pull-left">
              <button step2='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
              Previous
              </button>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <!-- wizard container --> 
      <!----------------------------Wizard End-----------------------------------> 
      
      <!------------------------------------Last--------------------------------------------->
      <div class="panel panel-primary" style="display:none;">
        <div class="panel-heading" style="text-align:center;">INTERACTION HISTORY</div>
        <div class="panel-body" style="overflow:auto"> </div>
      </div>
      <!---------------------------------------- Last End----------------------------------------------------> 
      
    </div>
  </div>
</div>
""")))}),format.raw/*328.2*/("""

"""),format.raw/*330.1*/("""<script>
var selectedUsers = [];
var dataInfo ="""),format.raw/*332.15*/("""{"""),format.raw/*332.16*/("""}"""),format.raw/*332.17*/(""";
var setFlag = false;
$("#selectAll").click(function()"""),format.raw/*334.33*/("""{"""),format.raw/*334.34*/("""
	"""),format.raw/*335.2*/("""$("input[name=searchname]").prop('checked', $(this).prop("checked"));
	setFlag = true;   
		$("#hiddenSelectAllFlag").val("selectAll");
		var savedsearchname = $("#savedName").val(); 
		
		getData(selectedUsers,setFlag,savedsearchname);
"""),format.raw/*341.1*/("""}"""),format.raw/*341.2*/(""");

function getSelectedUser()"""),format.raw/*343.27*/("""{"""),format.raw/*343.28*/("""
	"""),format.raw/*344.2*/("""$("input[name='searchname']:checked").each(function(i) """),format.raw/*344.57*/("""{"""),format.raw/*344.58*/("""
		"""),format.raw/*345.3*/("""selectedUsers.indexOf($(this).val()) === -1 ? selectedUsers.push($(this).val()) : console.log("This item already exists");	
		 """),format.raw/*346.4*/("""}"""),format.raw/*346.5*/("""); 

		var savedsearchname = $("#savedName").val(); 
		if($("#hiddenSelectAllFlag").val() == "selectAll")"""),format.raw/*349.53*/("""{"""),format.raw/*349.54*/("""
			"""),format.raw/*350.4*/("""setFlag = true;
			 
		"""),format.raw/*352.3*/("""}"""),format.raw/*352.4*/("""else"""),format.raw/*352.8*/("""{"""),format.raw/*352.9*/("""
			"""),format.raw/*353.4*/("""setFlag = false;
		"""),format.raw/*354.3*/("""}"""),format.raw/*354.4*/("""

	 	"""),format.raw/*356.4*/("""getData(selectedUsers,setFlag,savedsearchname);
		
"""),format.raw/*358.1*/("""}"""),format.raw/*358.2*/("""


"""),format.raw/*361.1*/("""function getData(selectedUsers,setFlag,savedsearchname)"""),format.raw/*361.56*/("""{"""),format.raw/*361.57*/("""
	"""),format.raw/*362.2*/("""if(setFlag == true)"""),format.raw/*362.21*/("""{"""),format.raw/*362.22*/("""
		"""),format.raw/*363.3*/("""selectedUsers = "all";
	"""),format.raw/*364.2*/("""}"""),format.raw/*364.3*/("""
	"""),format.raw/*365.2*/("""var urlLink="/getSelectedUserListforDatatable/"+ selectedUsers + "/"+setFlag+"/"+savedsearchname+""; 
	console.log(urlLink);
	 $('#selectedCustomers').dataTable("""),format.raw/*367.37*/("""{"""),format.raw/*367.38*/("""
	     """),format.raw/*368.7*/(""""bDestroy": true,
	     "processing": true,
	     "serverSide": true,
	     "scrollY": 300,
	     "paging": true,
	     "searching": false,
	     "ordering":false,
	     "ajax":  urlLink
	     """),format.raw/*376.7*/("""}"""),format.raw/*376.8*/(""");
	
"""),format.raw/*378.1*/("""}"""),format.raw/*378.2*/("""
"""),format.raw/*379.1*/("""function saveNewSmsTemplate()"""),format.raw/*379.30*/("""{"""),format.raw/*379.31*/("""
	 """),format.raw/*380.3*/("""var messageTemplate = $("#txtAreaId").val(); 
	 	 var msgAPI = $("#smsAPI").val();

	 alert(messageTemplate);
		var urlLink="/postSMSTemplate/"+messageTemplate+"/"+msgAPI+""; 
	   $.ajax("""),format.raw/*385.12*/("""{"""),format.raw/*385.13*/("""
	       """),format.raw/*386.9*/("""url: urlLink
	   """),format.raw/*387.5*/("""}"""),format.raw/*387.6*/(""").done(function () """),format.raw/*387.25*/("""{"""),format.raw/*387.26*/("""		   
		 """),format.raw/*388.4*/("""alert("saved template");
	   """),format.raw/*389.5*/("""}"""),format.raw/*389.6*/(""");
"""),format.raw/*390.1*/("""}"""),format.raw/*390.2*/("""







"""),format.raw/*398.1*/("""function sendBulkSMS()"""),format.raw/*398.23*/("""{"""),format.raw/*398.24*/("""
	"""),format.raw/*399.2*/("""//	var searchResultIds = $("#hiddenSelectAllFlag").val();
		var savedName 		= $("#savedName").val(); 
		var smstemplateId   =  $('#msgTypeID option:selected').val();

		
		console.log("after sendBulkSms"+selectedUsers);
		var searchResultIds = selectedUsers;

		dataInfo = """),format.raw/*407.14*/("""{"""),format.raw/*407.15*/("""
				"""),format.raw/*408.5*/("""savedName: savedName,
				smstemplateId: smstemplateId,
				searchResultIds: searchResultIds,
				setFlag: setFlag
		       """),format.raw/*412.10*/("""}"""),format.raw/*412.11*/(""";

		
		console.log(JSON.stringify(dataInfo));
		//var urlLink = "/postSMSBulk";
		
		 $.ajax("""),format.raw/*418.11*/("""{"""),format.raw/*418.12*/("""
			 """),format.raw/*419.5*/("""type: "POST",
	           url: "/postSMSBulk",
	           dataType: "json",
	           data: JSON.stringify(dataInfo),
	           contentType: "application/json",
	           success: function () """),format.raw/*424.34*/("""{"""),format.raw/*424.35*/("""
	             """),format.raw/*425.15*/("""alert("data sent to controller");
	           """),format.raw/*426.13*/("""}"""),format.raw/*426.14*/(""",

	       """),format.raw/*428.9*/("""}"""),format.raw/*428.10*/(""");
"""),format.raw/*429.1*/("""}"""),format.raw/*429.2*/("""


"""),format.raw/*432.1*/("""$(document).ready(function() """),format.raw/*432.30*/("""{"""),format.raw/*432.31*/("""
"""),format.raw/*433.1*/("""$('#SearchId').on('click',function()"""),format.raw/*433.37*/("""{"""),format.raw/*433.38*/("""
	 """),format.raw/*434.3*/("""var savedsearchname = $("#savedName").val(); 
	 var urlLink="/getCustomersListBySavedName/"+savedsearchname+""; 
	 console.log(urlLink);
	 $('#customerSearchTables').dataTable("""),format.raw/*437.40*/("""{"""),format.raw/*437.41*/("""
	     """),format.raw/*438.7*/(""""bDestroy": true,
	     "processing": true,
	     "serverSide": true,
	     "scrollY": 300,
	     "paging": true,
	     "searching": false,
	     "ordering":false,
	     "ajax":  urlLink
	     """),format.raw/*446.7*/("""}"""),format.raw/*446.8*/(""");
	 """),format.raw/*447.3*/("""}"""),format.raw/*447.4*/("""); 





$('#AddId').click(function()"""),format.raw/*453.29*/("""{"""),format.raw/*453.30*/(""" 
    """),format.raw/*454.5*/("""var vpara=$('#ParameteID :selected').val();
	var xpara = "("+vpara+")";
	var enteredText = $('#txtAreaId').val();
	$('.addedTxt').val(xpara);	
	var res = enteredText.concat(xpara);
	$('#txtAreaId').val(res);
"""),format.raw/*460.1*/("""}"""),format.raw/*460.2*/(""");


/*var mydropdown = document.getElementById('msgTypeID');
var mytextbox = document.getElementById('txtAreaId');
mydropdown.onchange = function()"""),format.raw/*465.33*/("""{"""),format.raw/*465.34*/("""
	 """),format.raw/*466.3*/("""mytextbox.focus();
     mytextbox.value = mytextbox.value  + this.value;
"""),format.raw/*468.1*/("""}"""),format.raw/*468.2*/("""*/


$('#msgTypeID').on('click',function()"""),format.raw/*471.38*/("""{"""),format.raw/*471.39*/("""
"""),format.raw/*472.1*/("""var	mydropdown = $('#msgTypeID option:selected').text();

$('#txtAreaId').val(mydropdown);
	
"""),format.raw/*476.1*/("""}"""),format.raw/*476.2*/(""");
$("#UploadID").click(function()"""),format.raw/*477.32*/("""{"""),format.raw/*477.33*/("""
	"""),format.raw/*478.2*/("""$(".dataTable_wrapper").show();
"""),format.raw/*479.1*/("""}"""),format.raw/*479.2*/(""");
$("#checkAll").change(function () """),format.raw/*480.35*/("""{"""),format.raw/*480.36*/("""
    """),format.raw/*481.5*/("""$("input[name=Searchname]").prop('checked', $(this).prop("checked"));
"""),format.raw/*482.1*/("""}"""),format.raw/*482.2*/(""");




//$(this).prop("checked"));
/*$(function()"""),format.raw/*488.15*/("""{"""),format.raw/*488.16*/("""
	"""),format.raw/*489.2*/("""$(".selectAll").change(function () """),format.raw/*489.37*/("""{"""),format.raw/*489.38*/("""
	    """),format.raw/*490.6*/("""var sel = $('input[name=searchname]').map(function(_, el) """),format.raw/*490.64*/("""{"""),format.raw/*490.65*/("""
	        """),format.raw/*491.10*/("""return $(el).val();
	    """),format.raw/*492.6*/("""}"""),format.raw/*492.7*/(""").get();
	    alert(sel);
    """),format.raw/*494.5*/("""}"""),format.raw/*494.6*/(""");
  """),format.raw/*495.3*/("""}"""),format.raw/*495.4*/(""");*/
  
$("input[name='SearchCust']").click(function()"""),format.raw/*497.47*/("""{"""),format.raw/*497.48*/("""
	"""),format.raw/*498.2*/("""var vsaerch = $(this).val();

	if(vsaerch=='Search Customer')"""),format.raw/*500.32*/("""{"""),format.raw/*500.33*/("""
		"""),format.raw/*501.3*/("""$("#SarchDivID").show();
		$("#uploadDivID").hide();
	$(".UploadCustIdDiv").hide();
	
	"""),format.raw/*505.2*/("""}"""),format.raw/*505.3*/("""
	"""),format.raw/*506.2*/("""else if(vsaerch=='Upload Customer')"""),format.raw/*506.37*/("""{"""),format.raw/*506.38*/("""
		"""),format.raw/*507.3*/("""$("#uploadDivID").hide();
		$("#SarchDivID").hide();
		$(".SearchCustId").hide();
	
	"""),format.raw/*511.2*/("""}"""),format.raw/*511.3*/("""
	"""),format.raw/*512.2*/("""else"""),format.raw/*512.6*/("""{"""),format.raw/*512.7*/("""
	
		"""),format.raw/*514.3*/("""}"""),format.raw/*514.4*/("""
"""),format.raw/*515.1*/("""}"""),format.raw/*515.2*/(""");

$("#SearchId").click(function()"""),format.raw/*517.32*/("""{"""),format.raw/*517.33*/("""
"""),format.raw/*518.1*/("""$(".SearchCustId").show();
"""),format.raw/*519.1*/("""}"""),format.raw/*519.2*/(""");

$("#uploadcustId").click(function()"""),format.raw/*521.36*/("""{"""),format.raw/*521.37*/("""
"""),format.raw/*522.1*/("""$(".UploadCustIdDiv").show();
"""),format.raw/*523.1*/("""}"""),format.raw/*523.2*/(""");


/*$(document).ready(function() """),format.raw/*526.32*/("""{"""),format.raw/*526.33*/("""
  """),format.raw/*527.3*/("""//  $('#customerSearchTable').DataTable();
"""),format.raw/*528.1*/("""}"""),format.raw/*528.2*/(""");

$(document).ready(function() """),format.raw/*530.30*/("""{"""),format.raw/*530.31*/("""
  """),format.raw/*531.3*/("""//  $('#customerUploadTable').DataTable();
"""),format.raw/*532.1*/("""}"""),format.raw/*532.2*/(""");*/
"""),format.raw/*533.1*/("""}"""),format.raw/*533.2*/(""");
</script>"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String,smsTemplateList:List[SMSTemplate],msgParameters:List[MessageParameters],savedSearchList:List[SavedSearchResult]): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem,smsTemplateList,msgParameters,savedSearchList)

  def f:((String,String,String,String,List[SMSTemplate],List[MessageParameters],List[SavedSearchResult]) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem,smsTemplateList,msgParameters,savedSearchList) => apply(dealercode,dealerName,userName,oem,smsTemplateList,msgParameters,savedSearchList)

  def ref: this.type = this

}


}

/**/
object messageTemplateSuperAdmin extends messageTemplateSuperAdmin_Scope0.messageTemplateSuperAdmin
              /*
                  -- GENERATED --
                  DATE: Thu Jan 04 12:45:00 IST 2018
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/messageTemplateSuperAdmin.scala.html
                  HASH: 6756309109a98cc6029f8e7a214b85e309f4a1ca
                  MATRIX: 872->5|1148->185|1178->190|1240->244|1279->246|1307->248|1363->278|1377->284|1428->315|3764->2624|3818->2662|3857->2663|3919->2697|3962->2713|3987->2729|4023->2744|4053->2747|4078->2763|4115->2778|4187->2819|4217->2822|5598->4175|5627->4176|5674->4194|5704->4195|5734->4196|5764->4197|5827->4231|5857->4232|5887->4233|5917->4234|5947->4235|5977->4236|6023->4253|6053->4254|6104->4276|6134->4277|6164->4278|6194->4279|6225->4281|6255->4282|6285->4283|6315->4284|6345->4285|11587->9499|11642->9537|11682->9538|11744->9571|11788->9587|11814->9603|11847->9614|11878->9617|11904->9633|11944->9650|12016->9690|12060->9706|12362->9980|12417->10018|12457->10019|12520->10053|12564->10069|12592->10087|12636->10109|12667->10112|12696->10130|12741->10152|12814->10193|12852->10203|16534->13854|16566->13858|16644->13907|16674->13908|16704->13909|16790->13966|16820->13967|16851->13970|17122->14213|17151->14214|17212->14246|17242->14247|17273->14250|17357->14305|17387->14306|17419->14310|17575->14438|17604->14439|17741->14547|17771->14548|17804->14553|17857->14578|17886->14579|17918->14583|17947->14584|17980->14589|18028->14609|18057->14610|18092->14617|18173->14670|18202->14671|18236->14677|18320->14732|18350->14733|18381->14736|18429->14755|18459->14756|18491->14760|18544->14785|18573->14786|18604->14789|18796->14952|18826->14953|18862->14961|19091->15162|19120->15163|19155->15170|19184->15171|19214->15173|19272->15202|19302->15203|19334->15207|19555->15399|19585->15400|19623->15410|19669->15428|19698->15429|19746->15448|19776->15449|19814->15459|19872->15489|19901->15490|19933->15494|19962->15495|20006->15511|20057->15533|20087->15534|20118->15537|20428->15818|20458->15819|20492->15825|20649->15953|20679->15954|20808->16054|20838->16055|20872->16061|21105->16265|21135->16266|21180->16282|21256->16329|21286->16330|21327->16343|21357->16344|21389->16348|21418->16349|21452->16355|21510->16384|21540->16385|21570->16387|21635->16423|21665->16424|21697->16428|21905->16607|21935->16608|21971->16616|22200->16817|22229->16818|22263->16824|22292->16825|22364->16868|22394->16869|22429->16876|22671->17090|22700->17091|22882->17244|22912->17245|22944->17249|23047->17324|23076->17325|23150->17370|23180->17371|23210->17373|23335->17470|23364->17471|23428->17506|23458->17507|23489->17510|23550->17543|23579->17544|23646->17582|23676->17583|23710->17589|23809->17660|23838->17661|23922->17716|23952->17717|23983->17720|24047->17755|24077->17756|24112->17763|24199->17821|24229->17822|24269->17833|24323->17859|24352->17860|24412->17892|24441->17893|24475->17899|24504->17900|24589->17956|24619->17957|24650->17960|24742->18023|24772->18024|24804->18028|24923->18119|24952->18120|24983->18123|25047->18158|25077->18159|25109->18163|25226->18252|25255->18253|25286->18256|25318->18260|25347->18261|25382->18268|25411->18269|25441->18271|25470->18272|25536->18309|25566->18310|25596->18312|25652->18340|25681->18341|25751->18382|25781->18383|25811->18385|25870->18416|25899->18417|25967->18456|25997->18457|26029->18461|26101->18505|26130->18506|26194->18541|26224->18542|26256->18546|26328->18590|26357->18591|26391->18597|26420->18598
                  LINES: 27->3|32->3|34->5|34->5|34->5|35->6|35->6|35->6|35->6|84->55|84->55|84->55|85->56|85->56|85->56|85->56|85->56|85->56|85->56|86->57|87->58|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|248->219|248->219|248->219|249->220|249->220|249->220|249->220|249->220|249->220|249->220|250->221|253->224|263->234|263->234|263->234|264->235|264->235|264->235|264->235|264->235|264->235|264->235|265->236|267->238|357->328|359->330|361->332|361->332|361->332|363->334|363->334|364->335|370->341|370->341|372->343|372->343|373->344|373->344|373->344|374->345|375->346|375->346|378->349|378->349|379->350|381->352|381->352|381->352|381->352|382->353|383->354|383->354|385->356|387->358|387->358|390->361|390->361|390->361|391->362|391->362|391->362|392->363|393->364|393->364|394->365|396->367|396->367|397->368|405->376|405->376|407->378|407->378|408->379|408->379|408->379|409->380|414->385|414->385|415->386|416->387|416->387|416->387|416->387|417->388|418->389|418->389|419->390|419->390|427->398|427->398|427->398|428->399|436->407|436->407|437->408|441->412|441->412|447->418|447->418|448->419|453->424|453->424|454->425|455->426|455->426|457->428|457->428|458->429|458->429|461->432|461->432|461->432|462->433|462->433|462->433|463->434|466->437|466->437|467->438|475->446|475->446|476->447|476->447|482->453|482->453|483->454|489->460|489->460|494->465|494->465|495->466|497->468|497->468|500->471|500->471|501->472|505->476|505->476|506->477|506->477|507->478|508->479|508->479|509->480|509->480|510->481|511->482|511->482|517->488|517->488|518->489|518->489|518->489|519->490|519->490|519->490|520->491|521->492|521->492|523->494|523->494|524->495|524->495|526->497|526->497|527->498|529->500|529->500|530->501|534->505|534->505|535->506|535->506|535->506|536->507|540->511|540->511|541->512|541->512|541->512|543->514|543->514|544->515|544->515|546->517|546->517|547->518|548->519|548->519|550->521|550->521|551->522|552->523|552->523|555->526|555->526|556->527|557->528|557->528|559->530|559->530|560->531|561->532|561->532|562->533|562->533
                  -- GENERATED --
              */
          