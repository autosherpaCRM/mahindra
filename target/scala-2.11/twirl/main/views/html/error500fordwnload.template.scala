
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object error500fordwnload_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class error500fordwnload extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.1*/("""<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>AutoSherpa</title>
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />

    <!-- Bootstrap -->
   <link rel="stylesheet" href=""""),_display_(/*10.34*/routes/*10.40*/.Assets.at("css/bootstrap.min.css")),format.raw/*10.75*/("""">
   <link rel="stylesheet" href=""""),_display_(/*11.34*/routes/*11.40*/.Assets.at("css/AdminLTE.min.css")),format.raw/*11.74*/("""">
	<link rel="stylesheet" href=""""),_display_(/*12.32*/routes/*12.38*/.Assets.at("css/font-awesome.min.css")),format.raw/*12.76*/("""">

  </head>
  <body class="error" style="background-color:#ecf0f5">
   <!--  <div class="container">
      <div class="col-lg-8 col-lg-offset-2 text-center">
        <div class="logo">
          <h1></h1>
        </div>
        <p class="lead text-muted">File is not available to download!</p>
        <div class="clearfix"></div>
        
        <div class="clearfix"></div>
        <br>
        <div class="col-lg-6 col-lg-offset-3">
          <div class="btn-group btn-group-justified">
            <a href="/" class="btn btn-warning">Home</a>
             </div>
        </div>
      </div>
    </div>
	 -->
	<section class="content">
      <div class="">
        <h2 class="headline text-yellow"> </h2>

        <div class="col-lg-8 col-lg-offset-2 text-center">
          <h2><i class="fa fa-warning text-yellow"></i> File is not available to download! </h2>

          <p>
            We could not find the page you were looking for.
            Meanwhile, you may <br /><a href="/"><b><u>Return To Home</u></b></a> 
          </p>

          
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
	
  </body>
</html>
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object error500fordwnload extends error500fordwnload_Scope0.error500fordwnload
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:26 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/error500fordwnload.scala.html
                  HASH: f35f50198af281c749054f398cc685acc1952a15
                  MATRIX: 853->0|1187->307|1202->313|1258->348|1322->385|1337->391|1392->425|1454->460|1469->466|1528->504
                  LINES: 32->1|41->10|41->10|41->10|42->11|42->11|42->11|43->12|43->12|43->12
                  -- GENERATED --
              */
          