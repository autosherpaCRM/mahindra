
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object changeAssignmentByCREManager_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class changeAssignmentByCREManager extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[List[WyzUser],List[WyzUser],String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(fromTransfer:List[WyzUser],toTransfer:List[WyzUser],dealerName:String,userName:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.89*/("""
"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*4.56*/{_display_(Seq[Any](format.raw/*4.57*/("""
 """),_display_(/*5.3*/helper/*5.9*/.form(action = routes.CallInteractionController.reAssigningCallsofselectCRE())/*5.87*/ {_display_(Seq[Any](format.raw/*5.89*/("""
    """),format.raw/*6.5*/("""<div class="row">
<div class="col-lg-12">
  <div class="panel panel-info">
    <div class="panel-heading"><strong>Select CRE</strong> <small>|To Transfer Calls</small></div>
    <div class="panel-body">
    
    <div class="col-sm-4 selectFrom">
      <label>From :</label>
      <select id="selectedValues" name="selectedValues[]" class="selectpicker" multiple required="required">
       """),_display_(/*15.9*/for(data <- fromTransfer) yield /*15.34*/{_display_(Seq[Any](format.raw/*15.35*/("""                      	                         
                """),format.raw/*16.17*/("""<option value=""""),_display_(/*16.33*/data/*16.37*/.userName),format.raw/*16.46*/("""">"""),_display_(/*16.49*/data/*16.53*/.userName),format.raw/*16.62*/("""</option>
                """)))}),format.raw/*17.18*/("""
               
       
      """),format.raw/*20.7*/("""</select>
      </div>
      <div class="col-sm-4 selectTo" style="display:none;">
      <label>To :</label>
      <select id="data[]" name="data[]" class="selectpicker" multiple required="required">        
                            			 
                              	                         
      """),_display_(/*27.8*/for(data <- toTransfer) yield /*27.31*/{_display_(Seq[Any](format.raw/*27.32*/("""                      	                         
                """),format.raw/*28.17*/("""<option value=""""),_display_(/*28.33*/data/*28.37*/.userName),format.raw/*28.46*/("""">"""),_display_(/*28.49*/data/*28.53*/.userName),format.raw/*28.62*/("""</option>
                """)))}),format.raw/*29.18*/("""
                      				
                      		
        
        
      """),format.raw/*34.7*/("""</select>
      </div>
     <div class="col-sm-2">
          <label class="btn btn-primary"  id="viewCalls" onclick="getChangeAssignmentCalls();">View Calls</label>
      </div>
        
      <div class="col-sm-2 assignCalls"style="display:none;">
          <button type="submit" class="btn btn-primary" id="js-upload-submit1">Assign Calls</button>
      </div>
    	
   
  </div>
</div>
</div>
    </div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-success">
      <div class="panel-heading"> List of Interactions For Change Assignment </div>
      <!-- /.panel-heading -->
      <div class="panel-body">
        <div class="dataTable_wrapper">
          <table class="table table-striped table-bordered table-hover" id="dataTables-example51">
            <thead>
              <tr>
                                    <th>Agent Name</th>
                                    <th>Customer Name</th>
                                    <th>Mobile Number</th>
                                    <th>Vehicle RegNo</th>
                                    <th>Model</th>
                                    <th>SaleDate</th>                                    
                                    <th>Next Service Due </th>
                                    
              </tr>
            </thead>
           
          </table>
        </div>
      </div>
      <!-- /.panel-body --> 
    </div>
    <!-- /.panel --> 
  </div>
  <!-- /.col-lg-12 --> 
</div>
""")))}),format.raw/*80.2*/("""
""")))}),format.raw/*81.2*/("""
"""),format.raw/*82.1*/("""<script>
$('#viewCalls').click(function () """),format.raw/*83.35*/("""{"""),format.raw/*83.36*/("""
"""),format.raw/*84.1*/("""$('.selectTo').css('display', 'block');
$('.assignCalls').css('display', 'block');
$('.selectFrom').css('display', 'none');
$('#viewCalls').css('display', 'none');
"""),format.raw/*88.1*/("""}"""),format.raw/*88.2*/(""");

$('.assignCalls').click(function () """),format.raw/*90.37*/("""{"""),format.raw/*90.38*/("""

"""),format.raw/*92.1*/("""$('.selectFrom').css('display', 'block');
$('#viewCalls').css('display', 'block');
$('.selectTo').css('display', 'none');
$('.assignCalls').css('display', 'none');

"""),format.raw/*97.1*/("""}"""),format.raw/*97.2*/(""");


</script>"""))
      }
    }
  }

  def render(fromTransfer:List[WyzUser],toTransfer:List[WyzUser],dealerName:String,userName:String): play.twirl.api.HtmlFormat.Appendable = apply(fromTransfer,toTransfer,dealerName,userName)

  def f:((List[WyzUser],List[WyzUser],String,String) => play.twirl.api.HtmlFormat.Appendable) = (fromTransfer,toTransfer,dealerName,userName) => apply(fromTransfer,toTransfer,dealerName,userName)

  def ref: this.type = this

}


}

/**/
object changeAssignmentByCREManager extends changeAssignmentByCREManager_Scope0.changeAssignmentByCREManager
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:20 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/changeAssignmentByCREManager.scala.html
                  HASH: ecd1598cbedf6523dbc45334611215e9b3579674
                  MATRIX: 826->5|1008->92|1036->95|1098->149|1136->150|1165->154|1178->160|1264->238|1303->240|1335->246|1761->646|1802->671|1841->672|1935->738|1978->754|1991->758|2021->767|2051->770|2064->774|2094->783|2153->811|2214->845|2552->1157|2591->1180|2630->1181|2724->1247|2767->1263|2780->1267|2810->1276|2840->1279|2853->1283|2883->1292|2942->1320|3051->1402|4618->2939|4651->2942|4680->2944|4752->2988|4781->2989|4810->2991|5005->3159|5033->3160|5103->3202|5132->3203|5163->3207|5360->3377|5388->3378
                  LINES: 27->3|32->3|33->4|33->4|33->4|34->5|34->5|34->5|34->5|35->6|44->15|44->15|44->15|45->16|45->16|45->16|45->16|45->16|45->16|45->16|46->17|49->20|56->27|56->27|56->27|57->28|57->28|57->28|57->28|57->28|57->28|57->28|58->29|63->34|109->80|110->81|111->82|112->83|112->83|113->84|117->88|117->88|119->90|119->90|121->92|126->97|126->97
                  -- GENERATED --
              */
          