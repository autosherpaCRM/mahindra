
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object assignComplaints_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class assignComplaints extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[List[String],List[Complaint],String,String,List[Location],List[Workshop],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(cresList:List[String],complaintData:List[Complaint],dealerName:String,user:String,locationList:List[Location],workshopList:List[Workshop]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.141*/("""

"""),_display_(/*3.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*3.52*/ {_display_(Seq[Any](format.raw/*3.54*/("""
"""),_display_(/*4.2*/helper/*4.8*/.form(action=routes.CallInteractionController.assignComplaints() ,'id -> "form5")/*4.89*/{_display_(Seq[Any](format.raw/*4.90*/("""

"""),format.raw/*6.1*/("""<!-- complaint Assignment Model statrs -->
<div class="row" >
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">   
                COMPLAINT ASSIGNMENT MODULE
            </div>
            <div class="panel-body" style="overflow-x: auto;">
                <div class="table-responsive">	
                <table id="baseTable" class="table table-striped table-bordered nowrap example4321" cellspacing="0">
                    <thead>
                        <tr id="row1">
                            <th>COMPLAINT No.</th>
                            <th>ISSUE DATE</th>
                            <th>AGING (Days)</th>
                            <th>REGISTERED BY</th>
                            <th>SOURCE</th>
                            <th>FUNCTION</th>
                            <th>CATEGORY</th>
                            <th>VEHICLE No.</th>
                            <th>MOBILE No.</th>
                            <th >View</th>
                        </tr>
                    </thead>

                    <tbody>
                        """),_display_(/*32.26*/for(post <- complaintData) yield /*32.52*/{_display_(Seq[Any](format.raw/*32.53*/("""
                      
                        """),format.raw/*34.25*/("""<tr id="rowclass" class="rowclass"""),_display_(/*34.59*/post/*34.63*/.getComplaintNumber()),format.raw/*34.84*/("""">
                            <td id="compNum" >"""),_display_(/*35.48*/post/*35.52*/.getComplaintNumber()),format.raw/*35.73*/("""</td>
                            <td id="issueDate">"""),_display_(/*36.49*/post/*36.53*/.getIssueDate()),format.raw/*36.68*/("""</td>
	<td id="x">"""),_display_(/*37.14*/post/*37.18*/.getAgeOfComplaint()),format.raw/*37.38*/("""</td>
                            <td id="userName">"""),_display_(/*38.48*/post/*38.52*/.getWyzUser().getUserName()),format.raw/*38.79*/("""</td>
                            <td id="y">"""),_display_(/*39.41*/post/*39.45*/.getFunctionName()),format.raw/*39.63*/("""</td>
                            <td id="z">"""),_display_(/*40.41*/post/*40.45*/.getSourceName()),format.raw/*40.61*/("""</td>
                            <td id="a">"""),_display_(/*41.41*/post/*41.45*/.getSubcomplaintType()),format.raw/*41.67*/("""</td>
                            <td id="vREgNO">"""),_display_(/*42.46*/post/*42.50*/.getVehicleRegNo()),format.raw/*42.68*/("""</td>
                            <td id="custPhone">"""),_display_(/*43.49*/post/*43.53*/.getCustomerPhone()),format.raw/*43.72*/("""</td>
                            <td><a class="getModel" data-id=""""),_display_(/*44.63*/post/*44.67*/.getComplaintNumber()),format.raw/*44.88*/(""""   href="" data-toggle="modal" data-target="#ComplaintAsignModelPopUp" data-backdrop="static" data-keyboard="false"><i class="fa fa-arrow-circle-right"></i>&nbsp;View</a></td>
							</tr>
                        """)))}),format.raw/*46.26*/("""
                    """),format.raw/*47.21*/("""</tbody>
                </table>
            </div>
            </div>
            </div>
            </div>
            </div>
            
            <!-- Assign Complaint Model Popup -->
            <div class="modal fade" id="ComplaintAsignModelPopUp" role="dialog">
                <div class="modal-dialog modal-lg">
              
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body">


                            <div class="panel-group">

                                <div class="panel panel-primary">
                                    <div class="panel-heading" style="text-align: center;"><b>COMPLAINT ASSIGNMENT MODULE<b></div>
                                                <div id="" class="panel">

                                                    <div class="panel-body">





<h4 style="color: #E80B0B;"><b>CUSTOMER DETAILS :<b></h4>      
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-condensed table-responsive table-user-information">
                        <tbody>

                            <tr>
                                <td><strong> VEHICLE NUMBER*:</strong></td>
                                <td class="text-primary" id="vNumber"></td>
                            </tr>

                            <tr>
                                <td><strong>CUSTOMER NAME:</strong></td>
                                <td class="text-primary" id="cName"></td>
                            </tr>

                            <tr>
                                <td><strong>MOBILE No: </strong></td>
                                <td class="text-primary" id="mNumber"></td>
                            </tr>

                            <tr>
                                <td><strong>EMAIL: </strong></td>
                                <td class="text-primary" id="eMail"></td>
                            </tr> 

                            <tr>
                                <td><strong>ADDRESS: </strong></td>
                                <td class="text-primary" id="aDdress"></td>
                            </tr>

                            <tr>
                                <td><strong>CHASSIS No: </strong></td>
                                <td class="text-primary" id="chassisNumber"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-condensed table-responsive table-user-information">
                        <tbody>

                            <tr>
                                <td><strong>MODEL:</strong></td>
                                <td class="text-primary" id="mOdel"></td>
                            </tr>
                            <tr>
                                <td><strong>VARIANT: </strong></td>
                                <td class="text-primary" id="vAriant"></td>
                            </tr>
                            <tr>
                                <td><strong>SALE DATE: </strong></td>
                                <td class="text-primary" id="saleDate"></td>
                            </tr>
                            <tr>
                                <td><strong>LAST SERVICE: </strong></td>
                                <td class="text-primary" id="lastService"></td>
                            </tr>
                            <tr>
                                <td><strong>SERVICE ADVISOR : </strong></td>
                                <td class="text-primary" id="serviceAdvisor"></td>
                            </tr>
                            <tr>
                                <td><strong>WORKSHOP: </strong></td>
                                <td class="text-primary" id="workShop"></td>
                            </tr>



                        </tbody>
                    </table>
                </div>

            </div>
            <h4 style="color: #E80B0B;"><b>COMPLAINT INFORMATION:<b></h4>      
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-condensed table-responsive table-user-information">
                                    <tbody>

                                        <tr>
                                            <td><strong> FUNCTION:</strong></td>
                                            <td class="text-primary" id="funcTion"> </td>
                                        </tr>

                                        <tr>
                                            <td><strong>CATEGORY:</strong></td>
                                            <td class="text-primary" id="cateGory"></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2"><strong>DESCRIPTION:</strong></td>
                                            <td class="text-primary" id="descripTion"></td>
                                            
                                            <td class="text-primary" id="descripTion1"></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-condensed table-responsive table-user-information">
                                    <tbody>

                                        <tr>
                                            <td><strong>SOURCE:</strong></td>
                                            <td class="text-primary" id="souRce">  </td>
                                        </tr>
                                        <tr>
                                            <td><strong>SUB CATEGORY 1: </strong></td>
                                            <td class="text-primary" id="subCategory1"></td>
                                        </tr>



                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 style="text-align: right;"><b><a href="#" style="color:#E80B0B;text-decoration: underline;">Resolve It</a></b></h4>
                                <h4 style="color: #E80B0B;"><b>ASSIGN COMPLAINT :<b></h4>  
                </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">CITY </label>
                            <select id="city" class="form-control" onclick="ajaxCallToLoadWorkShopByCity();">
                                <option value="0" >--Select--</option>
                                """),_display_(/*212.34*/for(location_list<-locationList) yield /*212.66*/{_display_(Seq[Any](format.raw/*212.67*/("""
                                
                              
                                """),format.raw/*215.33*/("""<option value=""""),_display_(/*215.49*/location_list/*215.62*/.getName()),format.raw/*215.72*/("""">"""),_display_(/*215.75*/location_list/*215.88*/.getName()),format.raw/*215.98*/("""</option>
                              
                             """)))}),format.raw/*217.31*/("""
                              """),format.raw/*218.31*/("""</select>
                        </div>
                    </div>
                <div class="col-lg-3">
                        <div class="form-group">
                            <label for="workshop">LOCATION </label>
                            <select id="workshop" class="form-control">
                                <option value="select" >Select</option>
                                """),_display_(/*226.34*/for(workshoplist<-workshopList) yield /*226.65*/{_display_(Seq[Any](format.raw/*226.66*/("""
                                """),format.raw/*227.33*/("""<option value=""""),_display_(/*227.49*/workshoplist/*227.61*/.getWorkshopName()),format.raw/*227.79*/("""">"""),_display_(/*227.82*/workshoplist/*227.94*/.getWorkshopName()),format.raw/*227.112*/("""</option>
                             """)))}),format.raw/*228.31*/("""
                              """),format.raw/*229.31*/("""</select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">FUNCTION </label>
                            <select id="FUNCTION" class="form-control">
                                <option value="0">--Select--</option>
                                <option value="SALES">SALES</option>   
                                <option value="AFTERSALES">AFTERSALES</option>
                                <option value="INSURANCE">INSURANCE</option>
                                <option value="FINANCE">FINANCE</option>
                                <option value="PARTS">PARTS</option>
                                <option value="PRODUCT">PRODUCT</option>
                                <option value="USED CAR SALES">USED CAR SALES</option>                            
                              </select>
                        </div>
                    </div>
					

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">OWNERSHIP </label>
                            
                            <select id="OWNERSHIP" class="form-control">
                                
                                <option value="0">--Select--</option>
                                """),_display_(/*256.34*/for(post1 <- cresList) yield /*256.56*/{_display_(Seq[Any](format.raw/*256.57*/("""
                                """),format.raw/*257.33*/("""<option value=""""),_display_(/*257.49*/post1),format.raw/*257.54*/("""">"""),_display_(/*257.57*/post1),format.raw/*257.62*/("""</option>
                              
                              """)))}),format.raw/*259.32*/("""
                              """),format.raw/*260.31*/("""</select>
                             
                              
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">PRIORITY </label>
                          
                            <select id="PRIORITY" class="form-control">
                                <option value="0">--Select--</option>
                                <option value="2">High</option>
                                <option value="3" selected="selected">Medium</option>
                                <option value="4">Low</option>
                              </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">ESCALATION 1 </label>
                            <input type="text" class="form-control" name="ESCALATION1"  id="ESCALATION1" autocomplete="off" />
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">ESCALATION 2 </label>
                            <input type="text" class="form-control" name="ESCALATION2"  id="ESCALATION2" autocomplete="off" />
                        </div>
                    </div>
                </div>
                </div>
                </div>
                </div>
                <div class="text-right">
                    <button class="btn btn-success AddComplaintResolution" type="button" >Submit</button>
                    <button type="button" class="btn btn-danger"  data-dismiss="modal">Close</button>
                </div>


                </div>
                </div>

                </div>

                </div>
                </div>
            
          

 """)))}),format.raw/*309.3*/("""
 """)))}),format.raw/*310.3*/("""
                    
                    
                    """))
      }
    }
  }

  def render(cresList:List[String],complaintData:List[Complaint],dealerName:String,user:String,locationList:List[Location],workshopList:List[Workshop]): play.twirl.api.HtmlFormat.Appendable = apply(cresList,complaintData,dealerName,user,locationList,workshopList)

  def f:((List[String],List[Complaint],String,String,List[Location],List[Workshop]) => play.twirl.api.HtmlFormat.Appendable) = (cresList,complaintData,dealerName,user,locationList,workshopList) => apply(cresList,complaintData,dealerName,user,locationList,workshopList)

  def ref: this.type = this

}


}

/**/
object assignComplaints extends assignComplaints_Scope0.assignComplaints
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:12 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/assignComplaints.scala.html
                  HASH: 56ec567c5c29c1d81fc20c9a249e7f12bd76936e
                  MATRIX: 833->1|1068->140|1098->145|1156->195|1195->197|1223->200|1236->206|1325->287|1363->288|1393->292|2568->1440|2610->1466|2649->1467|2727->1517|2788->1551|2801->1555|2843->1576|2921->1627|2934->1631|2976->1652|3058->1707|3071->1711|3107->1726|3154->1746|3167->1750|3208->1770|3289->1824|3302->1828|3350->1855|3424->1902|3437->1906|3476->1924|3550->1971|3563->1975|3600->1991|3674->2038|3687->2042|3730->2064|3809->2116|3822->2120|3861->2138|3943->2193|3956->2197|3996->2216|4092->2285|4105->2289|4147->2310|4395->2527|4445->2549|11994->10070|12043->10102|12083->10103|12212->10203|12256->10219|12279->10232|12311->10242|12342->10245|12365->10258|12397->10268|12502->10341|12563->10373|13000->10782|13048->10813|13088->10814|13151->10848|13195->10864|13217->10876|13257->10894|13288->10897|13310->10909|13351->10927|13424->10968|13485->11000|14980->12467|15019->12489|15059->12490|15122->12524|15166->12540|15193->12545|15224->12548|15251->12553|15357->12627|15418->12659|17487->14697|17522->14701
                  LINES: 27->1|32->1|34->3|34->3|34->3|35->4|35->4|35->4|35->4|37->6|63->32|63->32|63->32|65->34|65->34|65->34|65->34|66->35|66->35|66->35|67->36|67->36|67->36|68->37|68->37|68->37|69->38|69->38|69->38|70->39|70->39|70->39|71->40|71->40|71->40|72->41|72->41|72->41|73->42|73->42|73->42|74->43|74->43|74->43|75->44|75->44|75->44|77->46|78->47|243->212|243->212|243->212|246->215|246->215|246->215|246->215|246->215|246->215|246->215|248->217|249->218|257->226|257->226|257->226|258->227|258->227|258->227|258->227|258->227|258->227|258->227|259->228|260->229|287->256|287->256|287->256|288->257|288->257|288->257|288->257|288->257|290->259|291->260|340->309|341->310
                  -- GENERATED --
              */
          