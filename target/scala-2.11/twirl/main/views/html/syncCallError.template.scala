
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object syncCallError_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class syncCallError extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(key:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.14*/("""
"""),format.raw/*2.1*/("""<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Call Sync Error</title>
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />

    <!-- Bootstrap -->
   <link rel="stylesheet" href=""""),_display_(/*11.34*/routes/*11.40*/.Assets.at("css/bootstrap.min.css")),format.raw/*11.75*/("""">

  </head>
  <body class="error">
    <div class="container">
      <div class="col-lg-8 col-lg-offset-2 text-center">
        <div class="logo">
          <h1>Call Sync Error</h1>
        </div>
        <p class="lead text-muted">Oops, Firebase Data Not able to save in dataBase! at key """),_display_(/*20.94*/key),format.raw/*20.97*/("""</p>
        <div class="clearfix"></div>
        
        <div class="clearfix"></div>
        <br>
        <div class="col-lg-6 col-lg-offset-3">
          <div class="btn-group btn-group-justified">
            <a href="/" class="btn btn-warning">Home</a>
             </div>
        </div>
      </div><!-- /.col-lg-8 col-offset-2 -->
    </div>
  </body>
</html>


"""))
      }
    }
  }

  def render(key:String): play.twirl.api.HtmlFormat.Appendable = apply(key)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (key) => apply(key)

  def ref: this.type = this

}


}

/**/
object syncCallError extends syncCallError_Scope0.syncCallError
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:38 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/syncCallError.scala.html
                  HASH: 5b353c6770de9c01d7f37b5e6a63bb5f53d97377
                  MATRIX: 761->1|868->13|896->15|1235->327|1250->333|1306->368|1634->669|1658->672
                  LINES: 27->1|32->1|33->2|42->11|42->11|42->11|51->20|51->20
                  -- GENERATED --
              */
          