
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object complaints_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class complaints extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[List[ComplaintSource],List[Workshop],String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(sourcelist:List[ComplaintSource],listwork:List[Workshop],oem:String,dealercode:String,dealerName:String,userName:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.123*/("""

"""),_display_(/*4.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*4.65*/ {_display_(Seq[Any](format.raw/*4.67*/("""
"""),_display_(/*5.2*/helper/*5.8*/.form(action = routes.CallInteractionController.postComplaints)/*5.71*/ {_display_(Seq[Any](format.raw/*5.73*/("""
"""),format.raw/*6.1*/("""<div class="row" >
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading">   
        COMPLAINT REGISTRATION FORM </div>
		<div class="panel-body">
        <h4>CUSTOMER DETAILS :</h4>
              <div class="col-lg-3">
               <div class="form-group">
                <label for="vehicleRegNo">Vehicle Number <i style="color:red">*</i></label>
               
                	<div class="input-group">
                              <input type="text" class="form-control" name="vehicleRegNo" maxlength="12"  min="8" max="12" id="vehicleRegNo" autocomplete="off"/>
                              <span class="input-group-addon" data-toggle="modal" data-target="#searchPopup" id="searchveh" onclick="ajaxsearchVehicle();"><i class="fa fa-search"></i> </span>
                      </div>
                          </div>
              </div>
          <div class="col-lg-3">
               <div class="form-group">
                <label for="model">Model <i style="color:red">*</i></label>
                <input type="text" class="form-control" name="model" maxlength="20" id="model" autocomplete="off" />
              </div>
            </div>
       <div class="col-lg-3">
               <div class="form-group">
                <label for="customerName">Customer Name <i style="color:red">*</i></label>
                <input type="text" class="form-control customerNameComplain" name="customerName"  id="customerName" value="" />
              </div>
            </div>
        
      <div class="col-lg-3">             
              <div class="form-group">
                <label for="variant">Variant <i style="color:red">*</i></label>
                <input type="text" class="form-control" name="variant" maxlength="20"  id="variant" autocomplete="off" />
              </div>
            </div>
       
            <div class="col-lg-3">
               <div class="form-group">
                <label for="customerPhone">Customer Phone <i style="color:red">*</i></label>
                <input type="text" class="form-control" name="customerPhone" maxlength="10"   id="customerPhone" autocomplete="off" />
              </div>
            </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="saledate">Sale Date <i style="color:red">*</i></label>
               <input type="text"  class="form-control datepickerPrevious" name="saleDate" id="saledate" autocomplete="off" readonly/>
              </div>
            </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="email">Email <i style="color:red">*</i></label>
                <input type="text" class="form-control customerEmail" name="emailAddress" id="email" autocomplete="off"/>
              </div>
            </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="lastservice">Last Service  <i style="color:red">*</i></label>
               <input type="text"  class="form-control rangedatepicker" name="lastServiceDate" id="lastservice" autocomplete="off" readonly/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="address">Address  <i style="color:red">*</i></label>
                <textarea type="text" class="form-control" name="addressLine1" rows="1" id="address"></textarea>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="serviceadvisor">Service Advisor  <i style="color:red">*</i></label>
                <input type="text" class="form-control" name="serviceadvisor" id="serviceadvisor" autocomplete="off"/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="chassisno">Chassis No <i style="color:red">*</i></label>
               <input type="text" class="form-control" name="chassisNo"  maxlength="40" id="chassisno" autocomplete="off"/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="workshop">Workshop <i style="color:red">*</i></label>
           <!--    <input type="text" class="form-control" name="workshop"  maxlength="30" id="workshop" autocomplete="off"/> --> 
               <select class="form-control" name="workshop"  maxlength="30" id="workshop">
               
               """),_display_(/*91.17*/for(sa <- listwork) yield /*91.36*/{_display_(Seq[Any](format.raw/*91.37*/("""
               """),format.raw/*92.16*/("""<option value=""""),_display_(/*92.32*/sa/*92.34*/.getWorkshopName()),format.raw/*92.52*/("""">"""),_display_(/*92.55*/sa/*92.57*/.getWorkshopName()),format.raw/*92.75*/("""</option>
               """)))}),format.raw/*93.17*/("""
               """),format.raw/*94.16*/("""</select>
               
              </div>
            </div>
            <hr>
              <h4>COMPLAINT INFORMATION: </h4>
                  <div class="col-lg-3">
             <div class="form-group">
                <label for="sname">Source<i style="color:red">*</i></label>
                <select class="form-control" id="sname" name="sourceName">
                    <option value="0">Select Source</option>
                    
                    """),_display_(/*106.22*/for(li <- sourcelist) yield /*106.43*/{_display_(Seq[Any](format.raw/*106.44*/("""
                    	
                    	"""),format.raw/*108.22*/("""<option value=""""),_display_(/*108.38*/li/*108.40*/.getComplaintSource()),format.raw/*108.61*/("""">"""),_display_(/*108.64*/li/*108.66*/.getComplaintSource()),format.raw/*108.87*/("""</option>
                    	
                    """)))}),format.raw/*110.22*/("""
                   
                  """),format.raw/*112.19*/("""</select><span style="color:red;"></span>
             </div>
        </div>
                <div class="col-lg-3">
               <div class="form-group">
                <label for="functame">Function  <i style="color:red">*</i></label>
               
                <select class="form-control" id="functame" name="functionName">
                	<option value="0">Select Function</option>
							<option value="finance">FINANCE</option>
							<option value="insurance">INSURANCE</option>
							<option value="parts">PARTS</option>
							<option value="product">PRODUCT</option>
							<option value="sales">SALES</option>
							<option value="usedcarsales">USED CAR SALES</option>
							                </select>
              </div>
            </div>
              
     <div class="col-lg-3 complainCategory" style="display:none">
             <div class="form-group">
                <label for="complaintType">Complaint Type CATEGORY:<i style="color:red">*</i></label>
                <select class="form-control" id="complaintType" name="complaintType">
                    <option value="0">Select Complaint Type</option>
                      <option value="Technical">Technical</option>
                      <option value="Service">Service</option>
                      <option value="Bodyshop">Bodyshop</option>
                  </select><span style="color:red;"></span>
             </div>
        </div>
              
            <div class="col-lg-3 complainSubCategory" style="display:none">
             <div class="form-group">
                <label for="subcomplaintType">Sub CATEGORY:<i style="color:red">*</i></label>
                <select class="form-control" id="subcomplaintType" name="subcomplaintType">
                    <option value="0">Select subComplaint Type</option>
                      <option value="Service">Service</option>
                      <option value="Repair">Repair</option>
                      <option value="Others">Others</option>
                  </select><span style="color:red;"></span>
             </div>
             </div>
                 <div class="col-lg-3">
              <div class="form-group">
                <label for="description">Description:</label>
                <textarea  class="form-control" name="description" rows="1" id="description" autocomplete="off"></textarea>
              </div>
            </div>
              <div class="col-lg-3">                  
            <div class="form-group">
			<label>&nbsp;</label>
                <input type="submit" class="btn btn-primary btn-block"  id="complaintSubmit" value="Submit" />
              </div>
              </div>

             </div>
         </div>
     </div>
</div>
     """)))}),format.raw/*171.7*/("""
     """)))}),format.raw/*172.7*/("""
     
     """),format.raw/*174.6*/("""<script>
   
    $(document).ready(function() """),format.raw/*176.34*/("""{"""),format.raw/*176.35*/("""
       """),format.raw/*177.8*/("""var stateObject = """),format.raw/*177.26*/("""{"""),format.raw/*177.27*/("""
    """),format.raw/*178.5*/(""""AFTERSALES": """),format.raw/*178.19*/("""{"""),format.raw/*178.20*/("""
        """),format.raw/*179.9*/(""""Technical": ["Accessories", "Body","Brake","Electrical","Engine","Hvac","Steering","Transmission","Feedback Product"],
        "Service": ["Advisor", "Quality","Sales Related","Spares","Warrantly","Initiation","PickUp","FeedBack-Service"],	
        "Bodyshop": ["Poor WorkManship-Denting,Fitment","Poor WorkManship-Painting","Delay In Delivery","High Repair Charges","Insurance"]
    """),format.raw/*182.5*/("""}"""),format.raw/*182.6*/("""
  
"""),format.raw/*184.1*/("""}"""),format.raw/*184.2*/("""
"""),format.raw/*185.1*/("""window.onload = function () """),format.raw/*185.29*/("""{"""),format.raw/*185.30*/("""
    """),format.raw/*186.5*/("""var stateSel = document.getElementById("functame"),
        countySel = document.getElementById("complaintType"),
        citySel = document.getElementById("subcomplaintType");
    for (var state in stateObject) """),format.raw/*189.36*/("""{"""),format.raw/*189.37*/("""
        """),format.raw/*190.9*/("""stateSel.options[stateSel.options.length] = new Option(state, state);
    """),format.raw/*191.5*/("""}"""),format.raw/*191.6*/("""
    """),format.raw/*192.5*/("""stateSel.onchange = function () """),format.raw/*192.37*/("""{"""),format.raw/*192.38*/("""
        """),format.raw/*193.9*/("""countySel.length = 1; // remove all options bar first
        citySel.length = 1; // remove all options bar first
        if (this.selectedIndex < 1) return; // done   
        for (var county in stateObject[this.value]) """),format.raw/*196.53*/("""{"""),format.raw/*196.54*/("""
            """),format.raw/*197.13*/("""countySel.options[countySel.options.length] = new Option(county, county);
        """),format.raw/*198.9*/("""}"""),format.raw/*198.10*/("""
    """),format.raw/*199.5*/("""}"""),format.raw/*199.6*/("""
    """),format.raw/*200.5*/("""stateSel.onchange(); // reset in case page is reloaded
    countySel.onchange = function () """),format.raw/*201.38*/("""{"""),format.raw/*201.39*/("""
        """),format.raw/*202.9*/("""citySel.length = 1; // remove all options bar first
        if (this.selectedIndex < 1) return; // done   
        var cities = stateObject[stateSel.value][this.value];
        for (var i = 0; i < cities.length; i++) """),format.raw/*205.49*/("""{"""),format.raw/*205.50*/("""
            """),format.raw/*206.13*/("""citySel.options[citySel.options.length] = new Option(cities[i], cities[i]);
        """),format.raw/*207.9*/("""}"""),format.raw/*207.10*/("""
    """),format.raw/*208.5*/("""}"""),format.raw/*208.6*/("""
    """),format.raw/*209.5*/("""}"""),format.raw/*209.6*/("""
    """),format.raw/*210.5*/("""}"""),format.raw/*210.6*/(""");
 
         
         
         
         
      </script>
      
    <script>
 	  $('#customerPhone').on('keydown',function(e)"""),format.raw/*219.49*/("""{"""),format.raw/*219.50*/("""
 	 	  """),format.raw/*220.7*/("""-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
 	 	  """),format.raw/*221.7*/("""}"""),format.raw/*221.8*/(""");
 
      
   </script>"""))
      }
    }
  }

  def render(sourcelist:List[ComplaintSource],listwork:List[Workshop],oem:String,dealercode:String,dealerName:String,userName:String): play.twirl.api.HtmlFormat.Appendable = apply(sourcelist,listwork,oem,dealercode,dealerName,userName)

  def f:((List[ComplaintSource],List[Workshop],String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (sourcelist,listwork,oem,dealercode,dealerName,userName) => apply(sourcelist,listwork,oem,dealercode,dealerName,userName)

  def ref: this.type = this

}


}

/**/
object complaints extends complaints_Scope0.complaints
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:24 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/complaints.scala.html
                  HASH: 709d9bd4653eef6b00b095a49021654915732576
                  MATRIX: 813->3|1030->124|1060->129|1131->192|1170->194|1198->197|1211->203|1282->266|1321->268|1349->270|6033->4927|6068->4946|6107->4947|6152->4964|6195->4980|6206->4982|6245->5000|6275->5003|6286->5005|6325->5023|6383->5050|6428->5067|6931->5542|6969->5563|7009->5564|7084->5610|7128->5626|7140->5628|7183->5649|7214->5652|7226->5654|7269->5675|7356->5730|7426->5771|10255->8569|10294->8577|10336->8591|10413->8639|10443->8640|10480->8649|10527->8667|10557->8668|10591->8674|10634->8688|10664->8689|10702->8699|11118->9087|11147->9088|11181->9094|11210->9095|11240->9097|11297->9125|11327->9126|11361->9132|11605->9347|11635->9348|11673->9358|11776->9433|11805->9434|11839->9440|11900->9472|11930->9473|11968->9483|12221->9707|12251->9708|12294->9722|12405->9805|12435->9806|12469->9812|12498->9813|12532->9819|12654->9912|12684->9913|12722->9923|12971->10143|13001->10144|13044->10158|13157->10243|13187->10244|13221->10250|13250->10251|13284->10257|13313->10258|13347->10264|13376->10265|13543->10403|13573->10404|13609->10412|13881->10656|13910->10657
                  LINES: 27->2|32->2|34->4|34->4|34->4|35->5|35->5|35->5|35->5|36->6|121->91|121->91|121->91|122->92|122->92|122->92|122->92|122->92|122->92|122->92|123->93|124->94|136->106|136->106|136->106|138->108|138->108|138->108|138->108|138->108|138->108|138->108|140->110|142->112|201->171|202->172|204->174|206->176|206->176|207->177|207->177|207->177|208->178|208->178|208->178|209->179|212->182|212->182|214->184|214->184|215->185|215->185|215->185|216->186|219->189|219->189|220->190|221->191|221->191|222->192|222->192|222->192|223->193|226->196|226->196|227->197|228->198|228->198|229->199|229->199|230->200|231->201|231->201|232->202|235->205|235->205|236->206|237->207|237->207|238->208|238->208|239->209|239->209|240->210|240->210|249->219|249->219|250->220|251->221|251->221
                  -- GENERATED --
              */
          