
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object addMasterDataUsersBySuperAdmin_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class addMasterDataUsersBySuperAdmin extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[String,String,String,String,List[Location],List[Workshop],List[Role],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(dealercode:String,dealerName:String,userName:String,oem:String,locationList:List[Location],workshopList:List[Workshop],roleList:List[Role]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.142*/("""
"""),_display_(/*3.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*3.56*/ {_display_(Seq[Any](format.raw/*3.58*/("""
"""),_display_(/*4.2*/helper/*4.8*/.form(action = routes.SuperAdminController.postUserBySuperAdmin)/*4.72*/ {_display_(Seq[Any](format.raw/*4.74*/("""

"""),format.raw/*6.1*/("""<div class="row" >
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading">   
        Add Location </div>
		<div class="panel-body">
            <div class="col-lg-3">
              <div class="form-group">
                <label for="firstName">First Name<i style="color:red">*</i>:</label>
                <input type="text" class="form-control txtOnly" name="firstName"  id="firstName" autocomplete="off"  required/>
              </div>
            </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="lastName">last Name</label>
                <input type="text" class="form-control txtOnly" name="lastName"  id="lastName" autocomplete="off" >
              </div>
            </div>
              <div class="col-lg-3">
              <div class="form-group">
                <label for="userName">User Name<i style="color:red">*</i></label>
                <input type="text" class="form-control txtOnly" name="userName"  id="userName" autocomplete="off"  onclick="ajaxCallToCheckUser();" required/>
              </div>
            </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="phonenum">Phone Number<i style="color:red">*</i></label>
                <input type="text" class="form-control numericOnly" name="phoneNumber" maxlength="10" id="phonenum" autocomplete="off"  required/>
              </div>
            </div>
                  <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">CITY<i style="color:red">*</i> </label>
                            <select id="city" name="name" class="form-control " onclick="ajaxCallToLoadWorkShopByCity();" required>
                                <option value="" >--Select--</option>
                                """),_display_(/*41.34*/for(location_list<-locationList) yield /*41.66*/{_display_(Seq[Any](format.raw/*41.67*/("""
                                """),format.raw/*42.33*/("""<option value=""""),_display_(/*42.49*/location_list/*42.62*/.getName()),format.raw/*42.72*/("""">"""),_display_(/*42.75*/location_list/*42.88*/.getName()),format.raw/*42.98*/("""</option>
                             """)))}),format.raw/*43.31*/("""
                              """),format.raw/*44.31*/("""</select>
                        </div>
                    </div>
                <div class="col-lg-3">
                        <div class="form-group">
                            <label for="workshop">LOCATION<i style="color:red">*</i> </label>
                            <select id="workshop" name="id" class="form-control" required>
                                <option value="" >--Select--</option>
                               
                              </select>
                        </div>
                    </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">ROLE<i style="color:red">*</i> </label>
                            <select id="role" name="role" class="form-control" required>
                                <option value="" >--Select--</option>
                                """),_display_(/*61.34*/for(role_list<-roleList) yield /*61.58*/{_display_(Seq[Any](format.raw/*61.59*/("""
                                
                                """),format.raw/*63.33*/("""<option value=""""),_display_(/*63.49*/role_list/*63.58*/.getRole()),format.raw/*63.68*/("""">"""),_display_(/*63.71*/role_list/*63.80*/.getRole()),format.raw/*63.90*/("""</option>
                              
                             """)))}),format.raw/*65.31*/("""
                              """),format.raw/*66.31*/("""</select>
                        </div>
                    </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="pincode">Pincode<i style="color:red">*</i></label>
                <input type="text" class="form-control numericOnly" name="pinCode" maxlength="6" id="pinCode" autocomplete="off"  required/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="pincode">Default Password<i style="color:red">*</i></label>
                <input type="text" class="form-control" name="password"  id="password" autocomplete="off"  required/>
              </div>
            </div>
             <div class="col-lg-3">
              <div class="form-group">
              <br />
                  <input type="submit" class="btn btn-primary"  id="UserSubmit" value="Submit" />
              </div>
            </div>
        </div>
     </div>
     </div>
     </div>
     """)))}),format.raw/*91.7*/("""
 """)))}),format.raw/*92.3*/("""

 """),format.raw/*94.2*/("""<script>
 $(".numericOnly").keypress(function (e) """),format.raw/*95.42*/("""{"""),format.raw/*95.43*/("""
	    """),format.raw/*96.6*/("""if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
	"""),format.raw/*97.2*/("""}"""),format.raw/*97.3*/(""");
 $( ".txtOnly" ).keypress(function(e) """),format.raw/*98.39*/("""{"""),format.raw/*98.40*/("""
     """),format.raw/*99.6*/("""var key = e.keyCode;
     if (key >= 48 && key <= 57) """),format.raw/*100.34*/("""{"""),format.raw/*100.35*/("""
         """),format.raw/*101.10*/("""e.preventDefault();
     """),format.raw/*102.6*/("""}"""),format.raw/*102.7*/("""
 """),format.raw/*103.2*/("""}"""),format.raw/*103.3*/(""");
 </script>
 <script>
 function ajaxCallToCheckUser()"""),format.raw/*106.32*/("""{"""),format.raw/*106.33*/("""
	 """),format.raw/*107.3*/("""var uname = $("#userName").val(); 
	 var urlLink="/checkExistingUser/" + uname + ""; 
	   $.ajax("""),format.raw/*109.12*/("""{"""),format.raw/*109.13*/("""
	       """),format.raw/*110.9*/("""url: urlLink
	   """),format.raw/*111.5*/("""}"""),format.raw/*111.6*/(""").done(function () """),format.raw/*111.25*/("""{"""),format.raw/*111.26*/("""	
		   """),format.raw/*112.6*/("""alert("User exists");
	   """),format.raw/*113.5*/("""}"""),format.raw/*113.6*/(""");
 """),format.raw/*114.2*/("""}"""),format.raw/*114.3*/("""
 """),format.raw/*115.2*/("""</script>"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String,locationList:List[Location],workshopList:List[Workshop],roleList:List[Role]): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem,locationList,workshopList,roleList)

  def f:((String,String,String,String,List[Location],List[Workshop],List[Role]) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem,locationList,workshopList,roleList) => apply(dealercode,dealerName,userName,oem,locationList,workshopList,roleList)

  def ref: this.type = this

}


}

/**/
object addMasterDataUsersBySuperAdmin extends addMasterDataUsersBySuperAdmin_Scope0.addMasterDataUsersBySuperAdmin
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:10 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/addMasterDataUsersBySuperAdmin.scala.html
                  HASH: a72c341621e95c7eac503d499ebfb0c9f98691b2
                  MATRIX: 857->3|1093->143|1121->146|1183->200|1222->202|1250->205|1263->211|1335->275|1374->277|1404->281|3378->2228|3426->2260|3465->2261|3527->2295|3570->2311|3592->2324|3623->2334|3653->2337|3675->2350|3706->2360|3778->2401|3838->2433|4803->3371|4843->3395|4882->3396|4978->3464|5021->3480|5039->3489|5070->3499|5100->3502|5118->3511|5149->3521|5253->3594|5313->3626|6382->4665|6416->4669|6448->4674|6527->4725|6556->4726|6590->4733|6686->4802|6714->4803|6784->4845|6813->4846|6847->4853|6931->4908|6961->4909|7001->4920|7055->4946|7084->4947|7115->4950|7144->4951|7231->5009|7261->5010|7293->5014|7421->5113|7451->5114|7489->5124|7535->5142|7564->5143|7612->5162|7642->5163|7678->5171|7733->5198|7762->5199|7795->5204|7824->5205|7855->5208
                  LINES: 27->2|32->2|33->3|33->3|33->3|34->4|34->4|34->4|34->4|36->6|71->41|71->41|71->41|72->42|72->42|72->42|72->42|72->42|72->42|72->42|73->43|74->44|91->61|91->61|91->61|93->63|93->63|93->63|93->63|93->63|93->63|93->63|95->65|96->66|121->91|122->92|124->94|125->95|125->95|126->96|127->97|127->97|128->98|128->98|129->99|130->100|130->100|131->101|132->102|132->102|133->103|133->103|136->106|136->106|137->107|139->109|139->109|140->110|141->111|141->111|141->111|141->111|142->112|143->113|143->113|144->114|144->114|145->115
                  -- GENERATED --
              */
          