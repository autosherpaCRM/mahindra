
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object complaintResolution_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class complaintResolution extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[String,String,String,String,List[Complaint],List[Complaint],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(oem:String,dealercode:String,dealerName:String,userName:String,complaintData:List[Complaint],compalintDataclosed:List[Complaint]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.132*/("""

"""),_display_(/*4.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*4.65*/ {_display_(Seq[Any](format.raw/*4.67*/("""
"""),format.raw/*5.1*/("""<div class="row" >
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading">   
        COMPLAINT RESOLUTION MODULE</div>
		<div class="panel-body">
		<h4 style="text-align: center;color:red;"><b>OPEN COMPLAINTS</b></h4>
<table id="baseTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>COMPLAINT No.</th>
                <th>STATUS</th>
                <th>ISSUE DATE</th>
                <th>AGING (Days)</th>
                <th>REGISTERED BY</th>
                <th>SOURCE</th>
                <th>FUNCTION</th>
                <th>CATEGORY</th>
                <th>VEHICLE No.</th>
<!--                <th>MOBILE No.</th>
                <th>ASSIGNED TO</th>-->
                <th >View</th>
            </tr>
        </thead>
       
        <tbody>
               """),_display_(/*31.17*/for(post <- complaintData) yield /*31.43*/ {_display_(Seq[Any](format.raw/*31.45*/("""
"""),format.raw/*32.1*/("""<!--            <tr class="rowclass"""),_display_(/*32.37*/post/*32.41*/.getComplaintNumber()),format.raw/*32.62*/("""" value=""""),_display_(/*32.72*/post/*32.76*/.getComplaintNumber()),format.raw/*32.97*/("""" id=""""),_display_(/*32.104*/post/*32.108*/.getComplaintNumber()),format.raw/*32.129*/("""">
                <td class="complaintNum" id="tdclass"""),_display_(/*33.54*/post/*33.58*/.getComplaintNumber()),format.raw/*33.79*/("""">"""),_display_(/*33.82*/post/*33.86*/.getComplaintNumber()),format.raw/*33.107*/("""</td>
                <td id="tdclass"""),_display_(/*34.33*/post/*34.37*/.getComplaintNumber()),format.raw/*34.58*/("""">"""),_display_(/*34.61*/post/*34.65*/.getComplaintStatus()),format.raw/*34.86*/("""</td>
                <td id="tdclass"""),_display_(/*35.33*/post/*35.37*/.getComplaintNumber()),format.raw/*35.58*/("""">"""),_display_(/*35.61*/post/*35.65*/.getIssueDate()),format.raw/*35.80*/("""</td>
                <td id="tdclass"""),_display_(/*36.33*/post/*36.37*/.getComplaintNumber()),format.raw/*36.58*/("""">"""),_display_(/*36.61*/post/*36.65*/.getAgeOfComplaint()),format.raw/*36.85*/("""</td>
                <td id="tdclass"""),_display_(/*37.33*/post/*37.37*/.getComplaintNumber()),format.raw/*37.58*/("""">"""),_display_(/*37.61*/post/*37.65*/.getWyzUser().getUserName()),format.raw/*37.92*/("""</td>
                <td id="tdclass"""),_display_(/*38.33*/post/*38.37*/.getComplaintNumber()),format.raw/*38.58*/("""">"""),_display_(/*38.61*/post/*38.65*/.getFunctionName()),format.raw/*38.83*/("""</td>
                <td id="tdclass"""),_display_(/*39.33*/post/*39.37*/.getComplaintNumber()),format.raw/*39.58*/("""">"""),_display_(/*39.61*/post/*39.65*/.getSourceName()),format.raw/*39.81*/("""</td>
                <td id="tdclass"""),_display_(/*40.33*/post/*40.37*/.getComplaintNumber()),format.raw/*40.58*/("""">"""),_display_(/*40.61*/post/*40.65*/.getSubcomplaintType()),format.raw/*40.87*/("""</td>
                <td class="veh_num" id="tdclass"""),_display_(/*41.49*/post/*41.53*/.getComplaintNumber()),format.raw/*41.74*/("""">"""),_display_(/*41.77*/post/*41.81*/.getVehicleRegNo()),format.raw/*41.99*/("""</td>
                <td id="tdclass"""),_display_(/*42.33*/post/*42.37*/.getComplaintNumber()),format.raw/*42.58*/("""">"""),_display_(/*42.61*/post/*42.65*/.getCustomerPhone()),format.raw/*42.84*/("""</td>
                <td>dfgdfg</td>
                <td><a href="" class="comp"""),_display_(/*44.44*/post/*44.48*/.getComplaintNumber()),format.raw/*44.69*/("""" value=""""),_display_(/*44.79*/post/*44.83*/.getComplaintNumber()),format.raw/*44.104*/("""" data-id=""""),_display_(/*44.116*/post/*44.120*/.getComplaintNumber()),format.raw/*44.141*/("""" data-toggle="modal"  data-target="#OpenComplaintPopup" onclick="ajaxcomplaintNumber();" ><i class="fa fa-arrow-circle-right"></i>&nbsp;View</a></td>
                
            </tr>
            -->
            <tr id="rowclass" class="rowclass"""),_display_(/*48.47*/post/*48.51*/.getComplaintNumber()),format.raw/*48.72*/("""">
                            <td id="compNum" >"""),_display_(/*49.48*/post/*49.52*/.getComplaintNumber()),format.raw/*49.73*/("""</td>
                            <td id="status">"""),_display_(/*50.46*/post/*50.50*/.getComplaintStatus()),format.raw/*50.71*/("""</td>
                            <td id="issueDate">"""),_display_(/*51.49*/post/*51.53*/.getIssueDate()),format.raw/*51.68*/("""</td>
                            <td id="x">"""),_display_(/*52.41*/post/*52.45*/.getAgeOfComplaint()),format.raw/*52.65*/("""</td>
                            <td id="userName">"""),_display_(/*53.48*/post/*53.52*/.getWyzUser().getUserName()),format.raw/*53.79*/("""</td>
                            <td id="y">"""),_display_(/*54.41*/post/*54.45*/.getFunctionName()),format.raw/*54.63*/("""</td>
                            <td id="z">"""),_display_(/*55.41*/post/*55.45*/.getSourceName()),format.raw/*55.61*/("""</td>
           
                                                        <td id="a">"""),_display_(/*57.69*/post/*57.73*/.getSubcomplaintType()),format.raw/*57.95*/("""</td>

                            <td id="vREgNO">"""),_display_(/*59.46*/post/*59.50*/.getVehicleRegNo()),format.raw/*59.68*/("""</td>

                            <td><a class="getModel" data-id=""""),_display_(/*61.63*/post/*61.67*/.getComplaintNumber()),format.raw/*61.88*/("""" href="" data-toggle="modal" data-target="#OpenComplaintPopup"  data-backdrop="static" data-keyboard="false"><i class="fa fa-arrow-circle-right"></i>&nbsp;View</a></td>

                        </tr>
            """)))}),format.raw/*64.14*/("""
            """),format.raw/*65.13*/("""</tbody>
            </table>
            
            <h4 style="text-align: center;color:red;"><b>CLOSED  COMPLAINTS</b></h4>
            <table id="complaint" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>COMPLAINT No.</th>
                <th>STATUS</th>
                <th>ISSUE DATE</th>
                <th>AGING (Days)</th>
                <th>REGISTERED BY</th>
                <th>SOURCE</th>
                <th>FUNCTION</th>
                <th>CATEGORY</th>
                <th>VEHICLE No.</th>
<!--                <th>MOBILE No.</th>
                <th>ASSIGNED TO</th>-->
                <th>View</th>
            </tr>
        </thead>
  
          <tbody>
               """),_display_(/*88.17*/for(post <- compalintDataclosed) yield /*88.49*/ {_display_(Seq[Any](format.raw/*88.51*/("""

            """),format.raw/*90.13*/("""<tr id="rowclass" class="rowclass """),_display_(/*90.48*/post/*90.52*/.getComplaintNumber()),format.raw/*90.73*/("""">
                            <td id="compNumclosed" >"""),_display_(/*91.54*/post/*91.58*/.getComplaintNumber()),format.raw/*91.79*/("""</td>
                            <td id="statusclosed">"""),_display_(/*92.52*/post/*92.56*/.getComplaintStatus()),format.raw/*92.77*/("""</td>
                            <td id="issueDateclosed">"""),_display_(/*93.55*/post/*93.59*/.getIssueDate()),format.raw/*93.74*/("""</td>
                            <td id="xclosed">"""),_display_(/*94.47*/post/*94.51*/.getAgeOfComplaint()),format.raw/*94.71*/("""</td>
                            <td id="userNameclosed">"""),_display_(/*95.54*/post/*95.58*/.getWyzUser().getUserName()),format.raw/*95.85*/("""</td>
                            <td id="yclosed">"""),_display_(/*96.47*/post/*96.51*/.getFunctionName()),format.raw/*96.69*/("""</td>
                            <td id="zclosed">"""),_display_(/*97.47*/post/*97.51*/.getSourceName()),format.raw/*97.67*/("""</td>
                            
                            <td id="aclosed">"""),_display_(/*99.47*/post/*99.51*/.getSubcomplaintType()),format.raw/*99.73*/("""</td>

                            <td id="vREgNOclosed">"""),_display_(/*101.52*/post/*101.56*/.getVehicleRegNo()),format.raw/*101.74*/("""</td>

                            <td><a class="getModelclosed" data-id=""""),_display_(/*103.69*/post/*103.73*/.getComplaintNumber()),format.raw/*103.94*/("""" href="" data-toggle="modal" data-target="#ClosedComplaintPopup"  data-backdrop="static" data-keyboard="false"><i class="fa fa-arrow-circle-right"></i>&nbsp;View</a></td>

                        </tr>

            """)))}),format.raw/*107.14*/("""
            """),format.raw/*108.13*/("""</tbody>
            </table>
            
   </div>
   </div>
   </div>
   </div>

  <!-- Open Complain Popup -->
  <div class="modal fade" id="OpenComplaintPopup" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*123.102*/routes/*123.108*/.Assets.at("images/close1.png")),format.raw/*123.139*/(""""></button>
          
        </div>
        <div class="modal-body">
          
          
<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-heading" style="text-align: center;"><b>OPEN COMPLAINT RESOLUTION FORM<b></div>
        <div id="" class="panel">
		
            <div class="panel-body">
            <h4 style="color: #E80B0B;"><b>CUSTOMER DETAILS :<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				
                    <tr>
                      <td><strong> VEHICLE NUMBER*:</strong></td>
                      <td id="veh_num_pop"class="text-primary"></td>
                    </tr>
                   
                    <tr>
                      <td><strong>CUSTOMER NAME:</strong></td>
                      <td id="cust_name_pop"class="text-primary"></td>
                    </tr>
                 
                     <tr>
                       <td><strong>MOBILE No: </strong></td>
                      <td id="cust_phone_pop" class="text-primary"></td>
                    </tr>
					 
					  <tr>
                      <td><strong>EMAIL: </strong></td>
                      <td id="cust_email"class="text-primary"></td>
                    </tr> 
				
					  <tr>
                      <td><strong>ADDRESS: </strong></td>
                      <td id="addressName"class="text-primary"></td>
                    </tr>
					
						  <tr>
                      <td><strong>CHASSIS No: </strong></td>
                      <td id="chassisno"class="text-primary"></td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				   
                  <tr>
                      <td><strong>MODEL:</strong></td>
                      <td id="model" class="text-primary"></td>
                    </tr>
					   <tr>
                      <td><strong>VARIANT: </strong></td>
                      <td id="variant"class="text-primary"></td>
                    </tr>
					 <tr>
                      <td><strong>SALE DATE: </strong></td>
                      <td id="saledate" class="text-primary"></td>
                    </tr>
						<tr>
                      <td><strong>LAST SERVICE: </strong></td>
                      <td id="lastserv"class="text-primary"></td>
                    </tr>
						  <tr>
                      <td><strong>SERVICE ADVISOR : </strong></td>
                      <td id="serviceadvisor"class="text-primary"></td>
                    </tr>
						  <tr>
                      <td><strong>WORKSHOP: </strong></td>
                      <td id="workshopOpen" class="text-primary"></td>
                    </tr>
					
					
                    
                  </tbody>
                </table>
                </div>
                           
              </div>
			   <h4 style="color: #E80B0B;"><b>COMPLAINT INFORMATION:<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                 <tbody>

                    <tr>
                      <td><strong> FUNCTION:</strong></td>
                      <td id="function" class="text-primary"> </td>
                    </tr>
                   
                    <tr>
                      <td><strong>CATEGORY:</strong></td>
                      <td id="complaintType" class="text-primary"></td>
                    </tr>
            <tr>
                      <td rowspan="2"><strong>DESCRIPTION:</strong></td>
                      <td  id="description"class="text-primary"></td>
                    </tr>
                 
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
   
                  <tr>
                      <td><strong>SOURCE:</strong></td>
                      <td id="source"class="text-primary">  </td>
                    </tr>
                    <tr>
                      <td><strong>SUB CATEGORY 1: </strong></td>
                      <td id="subcomplaintType"class="text-primary"></td>
                    </tr>


                    
                  </tbody>

                </table>
                </div>
                           
              </div>
             <h4 style="color: #E80B0B;"><b>ASSIGN COMPLAINT :<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				
                    <tr>
                      <td><strong> CITY:</strong></td>
                      <td class="text-primary" id="cityName"> </td>
                    </tr>
                   
                    <tr>
                      <td><strong>OWNERSHIP:</strong></td>
                      <td class="text-primary" id="assignOwnership"></td>
                    </tr>
					  <tr>
                      <td><strong>ESCALATION 1:</strong></td>
                      <td class="text-primary" ></td>
                    </tr>
                 
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				   
                  <tr>
                      <td><strong>FUNCTION:</strong></td>
                      <td class="text-primary" id="function">  </td>
                    </tr>
					   <tr>
                      <td><strong>PRIORITY: </strong></td>
                      <td class="text-primary" id="prorityName"></td>
                    </tr>
					 <tr>
                      <td><strong>ESCALATION 2:</strong></td>
                      <td class="text-primary"></td>
                    </tr>
                 
				
					
                    
                  </tbody>
                </table>
                </div>
                           
              </div>
              
           <div class="row">
              
              	<h4 style="color: #E80B0B;"><b>COMPLAINT RESOLUTION<b></b></b></h4>
             
           <div class="col-lg-3">
               <div class="form-group">
                <label for="complaintStatus">COMPLAINT STATUS</label>
                
                <select id="complaintStatus" class="form-control" name="complaintStatus">
                    
                    <option value="Closed">Closed</option>
                     <option value="Open">Open</option>
                    
                </select>
              </div>
            </div>
             <div class="col-lg-3">
               <div class="form-group">
                <label for="customerStatus">CUSTOMER STATUS</label>
                
                 <select id="customerStatus" class="form-control" name="customerStatus">
                    
                    <option value="Satisfied">Satisfied</option>
                     <option value="Dissatisfied">Dissatisfied</option>
                    
                </select>
              </div>
            </div>
            
             <div class="col-lg-3">
               <div class="form-group">
                <label for="actionTaken">ACTION TAKEN </label>
                <input type="text" id="actionTaken"class="form-control" name="actionTaken"   autocomplete="off" />
              </div>
            </div>
            
             <div class="col-lg-3">
               <div class="form-group">
                <label for="resolutionBy">RESOLUTION By: </label>
                <input type="text" id="resolutionBy"class="form-control" name="resolutionBy" value=""""),_display_(/*341.102*/userName),format.raw/*341.110*/("""" autocomplete="off" />
              </div>
            </div>
           
              </div>
                <div class="row">
					 	 <div class="col-md-6">
               <div class="form-group">
                <label for="reasonfor">REASON FOR </label>
                <textarea type="text" id="reasonfor"class="form-control" name="reasonfor"  autocomplete="off" ></textarea>
              </div>
            </div>
				</div>
           
		
            </div>
        </div>
    </div>
    
  <div class="text-right">
  <button type="button" id="resolve_save" class="btn btn-success resolve_save" data-dismiss="modal">Update</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

       
    </div>
        </div>
        
      </div>
      
    </div>
  </div>
  
  <!-- Closed complaint Popup -->
    <div class="modal fade" id="ClosedComplaintPopup" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*381.102*/routes/*381.108*/.Assets.at("images/close1.png")),format.raw/*381.139*/(""""></button>
          
        </div>
        <div class="modal-body">
          
          
<div class="panel-group">
  
    <div class="panel panel-primary">
        <div class="panel-heading" style="text-align: center;"><b>CLOSED COMPLAINT RESOLUTION FORM<b></div>
        <div id="" class="panel">
		
            <div class="panel-body">
            
            
            
            
            
        <h4 style="color: #E80B0B;"><b>CUSTOMER DETAILS :<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				
                    <tr>
                      <td><strong> VEHICLE NUMBER*:</strong></td>
                      <td id="veh_num_popClosed"class="text-primary"></td>
                    </tr>
                   
                    <tr>
                      <td><strong>CUSTOMER NAME:</strong></td>
                      <td id="cust_name_popClosed"class="text-primary"></td>
                    </tr>
                 
                     <tr>
                       <td><strong>MOBILE No: </strong></td>
                      <td id="cust_phone_popClosed" class="text-primary"></td>
                    </tr>
					 
					  <tr>
                      <td><strong>EMAIL: </strong></td>
                      <td id="cust_emailClosed"class="text-primary"></td>
                    </tr> 
				
					  <tr>
                      <td><strong>ADDRESS: </strong></td>
                      <td id="addressNameClosed"class="text-primary"></td>
                    </tr>
					
						  <tr>
                      <td><strong>CHASSIS No: </strong></td>
                      <td id="chassisnoClosed"class="text-primary"></td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				   
                  <tr>
                      <td><strong>MODEL:</strong></td>
                      <td id="modelClosed" class="text-primary"></td>
                    </tr>
					   <tr>
                      <td><strong>VARIANT: </strong></td>
                      <td id="variantClosed"class="text-primary"></td>
                    </tr>
					 <tr>
                      <td><strong>SALE DATE: </strong></td>
                      <td id="saledateClosed" class="text-primary"></td>
                    </tr>
						<tr>
                      <td><strong>LAST SERVICE: </strong></td>
                      <td id="lastservClosed"class="text-primary"></td>
                    </tr>
						  <tr>
                      <td><strong>SERVICE ADVISOR : </strong></td>
                      <td id="serviceadvisorClosed"class="text-primary"></td>
                    </tr>
						  <tr>
                      <td><strong>WORKSHOP: </strong></td>
                      <td id="workshopClosed" class="text-primary"></td>
                    </tr>
					
					
                    
                  </tbody>
                </table>
                </div>
                           
              </div>
            <h4 style="color: #E80B0B;"><b>COMPLAINT INFORMATION:<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				
                    <tr>
                      <td><strong> FUNCTION:</strong></td>
                      <td id="functionClosed" class="text-primary"> </td>
                    </tr>
                   
                    <tr>
                      <td><strong>CATEGORY:</strong></td>
                      <td id="complaintTypeClosed" class="text-primary"></td>
                    </tr>
					  <tr>
                      <td rowspan="2"><strong>DESCRIPTION:</strong></td>
                      <td  id="descriptionClosed"class="text-primary"></td>
                    </tr>
                 
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				   
                  <tr>
                      <td><strong>SOURCE:</strong></td>
                      <td id="sourceClosed"class="text-primary">  </td>
                    </tr>
					   <tr>
                      <td><strong>SUB CATEGORY 1: </strong></td>
                      <td id="subcomplaintTypeClosed" class="text-primary"></td>
                    </tr>
				
					
                    
                  </tbody>
                </table>
                </div>
                           
              </div>
			     <h4 style="color: #E80B0B;"><b>ASSIGN COMPLAINT :<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				
                    <tr>
                      <td><strong> CITY:</strong></td>
                      <td class="text-primary" id="cityNameClosed"> </td>
                    </tr>
                   
                    <tr>
                      <td><strong>OWNERSHIP:</strong></td>
                      <td class="text-primary" id = "ownershipClosed"></td>
                    </tr>
					  <tr>
                      <td><strong>ESCALATION 1:</strong></td>
                      <td class="text-primary"></td>
                    </tr>
                 
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				   
                  <tr>
                      <td><strong>FUNCTION:</strong></td>
                      <td class="text-primary" id="functionNameClosed">  </td>
                    </tr>
					   <tr>
                      <td><strong>PRIORITY: </strong></td>
                      <td class="text-primary" id="priorityClosed"></td>
                    </tr>
					 <tr>
                      <td><strong>ESCALATION 2:</strong></td>
                      <td class="text-primary"></td>
                    </tr>
                 
				
					
                    
                  </tbody>
                </table>
                </div>
                           
              </div>
              
           <div class="row">
              
              	<h4 style="color: #E80B0B;"><b>COMPLAINT RESOLUTION<b></b></b></h4>
            
           <div class="col-lg-3">
               <div class="form-group">
                <label for="complaintStatusClosed">COMPLAINT STATUS</label>
                <input type="text" id="complaintStatusClosed"class="form-control" name="complaintStatusClosed"   autocomplete="off" readonly/>
              </div>
            </div>
             <div class="col-lg-3">
               <div class="form-group">
                <label for="customerStatusClosed">CUSTOMER STATUS</label>
                <input type="text" id="customerStatusClosed"class="form-control" name="customerStatusClosed"   autocomplete="off" readonly/>
              </div>
            </div>
            
             <div class="col-lg-3">
               <div class="form-group">
                <label for="actionTakenClosed">ACTION TAKEN </label>
                <input type="text" id="actionTakenClosed"class="form-control" name="actionTakenClosed"   autocomplete="off" readonly />
              </div>
            </div>
            
             <div class="col-lg-3">
               <div class="form-group">
                <label for="resolutionByClosed">RESOLUTION By: </label>
                <input type="text" id="resolutionByClosed"class="form-control" name="resolutionByClosed" value=""""),_display_(/*592.114*/userName),format.raw/*592.122*/("""" autocomplete="off" readonly/>
              </div>
            </div>
           
              </div>
                <div class="row">
					  	 <div class="col-lg-6">
               <div class="form-group">
                <label for="reasonforClosed">REASON FOR </label>
                <input type="textarea" id="reasonforClosed"class="form-control" name="reasonforClosed"  autocomplete="off" ></textarea>
              </div>
            </div>
				</div>
           
		
            </div>
        </div>
    </div>
    
  <div class="text-right">
  <button type="button" id="resolve_save_closed" class="btn btn-success resolve_save_closed" data-dismiss="modal">Update</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

       
    </div>
        </div>
        
      </div>
      
    </div>
  </div>
        </div>
        
      </div>
      
    </div>
  </div>


""")))}),format.raw/*632.2*/("""

"""),format.raw/*634.1*/("""<script>
	$(".getModel").on(
			"click",
			function() """),format.raw/*637.15*/("""{"""),format.raw/*637.16*/("""

				"""),format.raw/*639.5*/("""$.blockUI();
				
				var $row = $(this).closest("tr"); // Find the row
				var complaintNum = $row.find("#compNum").text(); // Find the text
				var $text2 = $row.find("#issueDate").text();
				var $text3 = $row.find("#userName").text();
				var veh_num = $row.find("#vREgNO").text();
				var status = $row.find("#status").text();
				var $text5 = $row.find("#x").text();
				var $text6 = $row.find("#y").text();
				var $text7 = $row.find("#z").text();
				var $text8 = $row.find("#a").text();
				var $text9 = $row.find("#issueDate").text();
				var $text10 = $row.find("#custPhone").text();
				$(".resolve_save").val(complaintNum);

				var urlDisposition = "/CRE/searchcomplaintNum/" + complaintNum
						+ "/" + veh_num + "";
				$.ajax("""),format.raw/*657.12*/("""{"""),format.raw/*657.13*/("""
					"""),format.raw/*658.6*/("""url : urlDisposition

				"""),format.raw/*660.5*/("""}"""),format.raw/*660.6*/(""").done(function(data) """),format.raw/*660.28*/("""{"""),format.raw/*660.29*/("""

					
					
					"""),format.raw/*664.6*/("""if (data != null) """),format.raw/*664.24*/("""{"""),format.raw/*664.25*/("""
						"""),format.raw/*665.7*/("""console.log(data);
						//  console.log(complaintNum);

						$("#veh_num_pop").html(data.vehicleRegNo);
						$("#cust_phone_pop").html(data.customerMobileNo);
						$("#cust_name_pop").html(data.customerName);
						$("#cust_email").html(data.customerEmail);
						$("#serviceadvisor").html(data.serviceadvisor);
						$("#addressName").html(data.preferredAdress);
						$("#chassisno").html(data.chassisNo);
						$("#saledate").html(data.saleDate);
						$("#variant").html(data.variant);
						$("#model").html(data.model);
						$("#function").html(data.functionName);
						$("#serviceadvisor").html(data.serviceadvisor);
						$("#source").html(data.sourceName);
						$("#description").html(data.description);
						$("#subcomplaintType").html(data.subcomplaintType);
						$("#complaintType").html(data.complaintType);
						$("#function").html(data.functionName);
						$("#assignOwnership").html(data.assignedUser);
						$("#prorityName").html(data.priority);
						$("#cityName").html(data.location);
						$("#workshopOpen").html(data.workshop);

					"""),format.raw/*690.6*/("""}"""),format.raw/*690.7*/("""

					"""),format.raw/*692.6*/("""$.unblockUI();
				"""),format.raw/*693.5*/("""}"""),format.raw/*693.6*/(""");
			"""),format.raw/*694.4*/("""}"""),format.raw/*694.5*/(""");
</script>
  <script>
			$(".resolve_save")
					.on(
							'click',
							function() """),format.raw/*700.19*/("""{"""),format.raw/*700.20*/("""

								"""),format.raw/*702.9*/("""var complaintNum = $(".resolve_save").val();

								var reasonFor = $("#reasonfor").val();

								var complaintStatus = $("#complaintStatus")
										.val();
								var actionTaken = $("#actionTaken").val();
								var resolutionBy = $("#resolutionBy").val();
								var customerStatus = $("#customerStatus").val();

								//alert(complaintNum);

								if (reasonFor == '') """),format.raw/*714.30*/("""{"""),format.raw/*714.31*/("""
									"""),format.raw/*715.10*/("""Lobibox.notify('warning', """),format.raw/*715.36*/("""{"""),format.raw/*715.37*/("""
										"""),format.raw/*716.11*/("""continueDelayOnInactiveTab : true,
										msg : 'Reason for is Required'
									"""),format.raw/*718.10*/("""}"""),format.raw/*718.11*/(""");

									return false;

								"""),format.raw/*722.9*/("""}"""),format.raw/*722.10*/("""
								"""),format.raw/*723.9*/("""if (actionTaken == '') """),format.raw/*723.32*/("""{"""),format.raw/*723.33*/("""
									"""),format.raw/*724.10*/("""Lobibox.notify('warning', """),format.raw/*724.36*/("""{"""),format.raw/*724.37*/("""
										"""),format.raw/*725.11*/("""continueDelayOnInactiveTab : true,
										msg : 'Action Taken is Required'
									"""),format.raw/*727.10*/("""}"""),format.raw/*727.11*/(""");

									return false;
								"""),format.raw/*730.9*/("""}"""),format.raw/*730.10*/(""" """),format.raw/*730.11*/("""else """),format.raw/*730.16*/("""{"""),format.raw/*730.17*/("""
									"""),format.raw/*731.10*/("""var urlDisposition = "/CRE/updateComplaintsResolution/"
											+ complaintNum
											+ "/"
											+ reasonFor
											+ "/"
											+ complaintStatus
											+ "/"
											+ customerStatus
											+ "/"
											+ actionTaken + "/" + resolutionBy;
									$
											.ajax("""),format.raw/*742.18*/("""{"""),format.raw/*742.19*/("""
												"""),format.raw/*743.13*/("""url : urlDisposition
											"""),format.raw/*744.12*/("""}"""),format.raw/*744.13*/(""")
											.done(
													function(data) """),format.raw/*746.29*/("""{"""),format.raw/*746.30*/("""
														"""),format.raw/*747.15*/("""if (data != null) """),format.raw/*747.33*/("""{"""),format.raw/*747.34*/("""
															"""),format.raw/*748.16*/("""Lobibox
																	.alert(
																			'success',
																			"""),format.raw/*751.20*/("""{"""),format.raw/*751.21*/("""
																				"""),format.raw/*752.21*/("""msg : 'Complaint Has been Resolved for open Successfully....'
																			"""),format.raw/*753.20*/("""}"""),format.raw/*753.21*/(""");

														"""),format.raw/*755.15*/("""}"""),format.raw/*755.16*/("""
														"""),format.raw/*756.15*/("""window
																.setTimeout(
																		function() """),format.raw/*758.30*/("""{"""),format.raw/*758.31*/("""
																			"""),format.raw/*759.20*/("""location
																					.reload()
																		"""),format.raw/*761.19*/("""}"""),format.raw/*761.20*/(""", 4000)

													"""),format.raw/*763.14*/("""}"""),format.raw/*763.15*/(""");

								"""),format.raw/*765.9*/("""}"""),format.raw/*765.10*/("""

							"""),format.raw/*767.8*/("""}"""),format.raw/*767.9*/(""");
		</script>
	 <script>
			$(".resolve_save_closed")
					.on(
							'click',
							function() """),format.raw/*773.19*/("""{"""),format.raw/*773.20*/("""

								"""),format.raw/*775.9*/("""//alert("closed submit");
								var complaintNumClosed = $(
										".resolve_save_closed").val();
								var reasonForClosed = $("#reasonforClosed")
										.val();
								var complaintStatusClosed = $(
										"#complaintStatusClosed").val();
								var actionTakenClosed = $("#actionTakenClosed")
										.val();
								var resolutionByClosed = $(
										"#resolutionByClosed").val();
								var customerStatusClosed = $(
										"#customerStatusClosed").val();

								//alert(complaintNumClosed);

								if (reasonforClosed == '') """),format.raw/*791.36*/("""{"""),format.raw/*791.37*/("""
									"""),format.raw/*792.10*/("""Lobibox.notify('warning', """),format.raw/*792.36*/("""{"""),format.raw/*792.37*/("""
										"""),format.raw/*793.11*/("""continueDelayOnInactiveTab : true,
										msg : 'Please Enter the Reason..'
									"""),format.raw/*795.10*/("""}"""),format.raw/*795.11*/(""");

									return false;

								"""),format.raw/*799.9*/("""}"""),format.raw/*799.10*/(""" """),format.raw/*799.11*/("""else """),format.raw/*799.16*/("""{"""),format.raw/*799.17*/("""
									"""),format.raw/*800.10*/("""var urlDisposition = "/CRE/updateComplaintsResolutionClosed/"
											+ complaintNumClosed
											+ "/"
											+ reasonForClosed
											+ "/"
											+ complaintStatusClosed
											+ "/"
											+ customerStatusClosed
											+ "/"
											+ actionTakenClosed
											+ "/"
											+ resolutionByClosed;
									$
											.ajax("""),format.raw/*813.18*/("""{"""),format.raw/*813.19*/("""
												"""),format.raw/*814.13*/("""url : urlDisposition
											"""),format.raw/*815.12*/("""}"""),format.raw/*815.13*/(""")
											.done(
													function(data) """),format.raw/*817.29*/("""{"""),format.raw/*817.30*/("""
														"""),format.raw/*818.15*/("""if (data != null) """),format.raw/*818.33*/("""{"""),format.raw/*818.34*/("""
															"""),format.raw/*819.16*/("""Lobibox
																	.alert(
																			'success',
																			"""),format.raw/*822.20*/("""{"""),format.raw/*822.21*/("""
																				"""),format.raw/*823.21*/("""msg : 'Complaint Has been Resolved for closed Successfully....'
																			"""),format.raw/*824.20*/("""}"""),format.raw/*824.21*/(""");

														"""),format.raw/*826.15*/("""}"""),format.raw/*826.16*/("""
														"""),format.raw/*827.15*/("""window
																.setTimeout(
																		function() """),format.raw/*829.30*/("""{"""),format.raw/*829.31*/("""
																			"""),format.raw/*830.20*/("""location
																					.reload()
																		"""),format.raw/*832.19*/("""}"""),format.raw/*832.20*/(""", 4000)

													"""),format.raw/*834.14*/("""}"""),format.raw/*834.15*/(""");

								"""),format.raw/*836.9*/("""}"""),format.raw/*836.10*/("""
							"""),format.raw/*837.8*/("""}"""),format.raw/*837.9*/(""");
		</script>
    
<script>
	$(".getModelclosed").on(
			"click",
			function() """),format.raw/*843.15*/("""{"""),format.raw/*843.16*/("""

				"""),format.raw/*845.5*/("""$.blockUI();
				var $row = $(this).closest("tr"); // Find the row
				var complaintNumclosed = $row.find("#compNumclosed").text(); // Find the text
				var $text2 = $row.find("#issueDateclosed").text();
				var $text3 = $row.find("#userNameclosed").text();
				var veh_numclosed = $row.find("#vREgNOclosed").text();
				var status = $row.find("#statusclosed").text();
				var $text5 = $row.find("#xclosed").text();
				var $text6 = $row.find("#yclosed").text();
				var $text7 = $row.find("#zclosed").text();
				var $text8 = $row.find("#aclosed").text();
				var $text9 = $row.find("#issueDateclosed").text();
				//     var $text10 = $row.find("#custPhoneclosed").text();
				$(".resolve_save_closed").val(complaintNumclosed);

				var urlDisposition = "/CRE/searchcomplaintNumClosed/"
						+ complaintNumclosed + "/" + veh_numclosed + "";
				$.ajax("""),format.raw/*862.12*/("""{"""),format.raw/*862.13*/("""
					"""),format.raw/*863.6*/("""url : urlDisposition

				"""),format.raw/*865.5*/("""}"""),format.raw/*865.6*/(""")
						.done(
								function(data) """),format.raw/*867.24*/("""{"""),format.raw/*867.25*/("""

									"""),format.raw/*869.10*/("""$.unblockUI();
									
									if (data != null) """),format.raw/*871.28*/("""{"""),format.raw/*871.29*/("""
										"""),format.raw/*872.11*/("""console.log(data);
										console.log(complaintNumclosed);

										$("#veh_num_popClosed").html(
												data.vehicleRegNo);
									  $("#cust_phone_popClosed").html(
											  data.customerMobileNo);      
										$("#cust_name_popClosed").html(
												data.customerName);
										$("#cust_emailClosed").html(
												data.customerEmail);
										$("#serviceadvisorClosed").html(
												data.serviceadvisor);
										$("#addressNameClosed").html(
												data.preferredAdress);
										$("#chassisnoClosed").html(
												data.chassisNo);
										$("#saledateClosed")
												.html(data.saleDate);
										$("#variantClosed").html(data.variant);
										$("#modelClosed").html(data.model);
										$("#serviceadvisorClosed").html(
												data.serviceadvisor);
										$("#sourceClosed")
												.html(data.sourceName);
										$("#descriptionClosed").html(
												data.description);
										$("#subcomplaintTypeClosed").html(
												data.subcomplaintType);
										$("#complaintTypeClosed").html(
												data.complaintType);
										$("#functionClosed").html(
												data.functionName);
										$("#resolutionByClosed").val(
												data.resolutionBy);
										$("#functionNameClosed").html(
												data.functionName);
										$("#priorityClosed")
												.html(data.priority);
										$("#cityNameClosed")
												.html(data.location);
										$("#ownershipClosed")
												.html(data.wyzUser);									
										$("#complaintStatusClosed").val(
												data.complaintStatus);
										$("#customerStatusClosed").val(
												data.customerstatus);
										$("#actionTakenClosed").val(
												data.actionTaken);
										$("#reasonforClosed").val(
												data.reasonFor);
										$("#workshopClosed").html(data.workshop);
										//alert(data.resolutionBy);
										//            $("#address").val(data.preferredAdress);
									"""),format.raw/*926.10*/("""}"""),format.raw/*926.11*/("""
								"""),format.raw/*927.9*/("""}"""),format.raw/*927.10*/(""");
			"""),format.raw/*928.4*/("""}"""),format.raw/*928.5*/(""");
</script>    
  
   """))
      }
    }
  }

  def render(oem:String,dealercode:String,dealerName:String,userName:String,complaintData:List[Complaint],compalintDataclosed:List[Complaint]): play.twirl.api.HtmlFormat.Appendable = apply(oem,dealercode,dealerName,userName,complaintData,compalintDataclosed)

  def f:((String,String,String,String,List[Complaint],List[Complaint]) => play.twirl.api.HtmlFormat.Appendable) = (oem,dealercode,dealerName,userName,complaintData,compalintDataclosed) => apply(oem,dealercode,dealerName,userName,complaintData,compalintDataclosed)

  def ref: this.type = this

}


}

/**/
object complaintResolution extends complaintResolution_Scope0.complaintResolution
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:23 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/complaintResolution.scala.html
                  HASH: 07b53285b40cf85d80b776db23a8a13ca2bc664a
                  MATRIX: 826->3|1052->133|1082->138|1153->201|1192->203|1220->205|2169->1127|2211->1153|2251->1155|2280->1157|2343->1193|2356->1197|2398->1218|2435->1228|2448->1232|2490->1253|2525->1260|2539->1264|2582->1285|2666->1342|2679->1346|2721->1367|2751->1370|2764->1374|2807->1395|2873->1434|2886->1438|2928->1459|2958->1462|2971->1466|3013->1487|3079->1526|3092->1530|3134->1551|3164->1554|3177->1558|3213->1573|3279->1612|3292->1616|3334->1637|3364->1640|3377->1644|3418->1664|3484->1703|3497->1707|3539->1728|3569->1731|3582->1735|3630->1762|3696->1801|3709->1805|3751->1826|3781->1829|3794->1833|3833->1851|3899->1890|3912->1894|3954->1915|3984->1918|3997->1922|4034->1938|4100->1977|4113->1981|4155->2002|4185->2005|4198->2009|4241->2031|4323->2086|4336->2090|4378->2111|4408->2114|4421->2118|4460->2136|4526->2175|4539->2179|4581->2200|4611->2203|4624->2207|4664->2226|4774->2309|4787->2313|4829->2334|4866->2344|4879->2348|4922->2369|4962->2381|4976->2385|5019->2406|5298->2658|5311->2662|5353->2683|5431->2734|5444->2738|5486->2759|5565->2811|5578->2815|5620->2836|5702->2891|5715->2895|5751->2910|5825->2957|5838->2961|5879->2981|5960->3035|5973->3039|6021->3066|6095->3113|6108->3117|6147->3135|6221->3182|6234->3186|6271->3202|6386->3290|6399->3294|6442->3316|6523->3370|6536->3374|6575->3392|6673->3463|6686->3467|6728->3488|6976->3705|7018->3719|7839->4513|7887->4545|7927->4547|7971->4563|8033->4598|8046->4602|8088->4623|8172->4680|8185->4684|8227->4705|8312->4763|8325->4767|8367->4788|8455->4849|8468->4853|8504->4868|8584->4921|8597->4925|8638->4945|8725->5005|8738->5009|8786->5036|8866->5089|8879->5093|8918->5111|8998->5164|9011->5168|9048->5184|9158->5267|9171->5271|9214->5293|9302->5353|9316->5357|9356->5375|9461->5452|9475->5456|9518->5477|9771->5698|9814->5712|10277->6146|10294->6152|10348->6183|18769->14575|18800->14583|20051->15805|20068->15811|20122->15842|28461->24152|28492->24160|29505->25142|29537->25146|29624->25204|29654->25205|29690->25213|30481->25975|30511->25976|30546->25983|30602->26011|30631->26012|30682->26034|30712->26035|30763->26058|30810->26076|30840->26077|30876->26085|31997->27178|32026->27179|32063->27188|32111->27208|32140->27209|32175->27216|32204->27217|32329->27313|32359->27314|32399->27326|32827->27725|32857->27726|32897->27737|32952->27763|32982->27764|33023->27776|33139->27863|33169->27864|33237->27904|33267->27905|33305->27915|33357->27938|33387->27939|33427->27950|33482->27976|33512->27977|33553->27989|33671->28078|33701->28079|33767->28117|33797->28118|33827->28119|33861->28124|33891->28125|33931->28136|34276->28452|34306->28453|34349->28467|34411->28500|34441->28501|34520->28551|34550->28552|34595->28568|34642->28586|34672->28587|34718->28604|34832->28689|34862->28690|34913->28712|35024->28794|35054->28795|35103->28815|35133->28816|35178->28832|35274->28899|35304->28900|35354->28921|35443->28981|35473->28982|35526->29006|35556->29007|35598->29021|35628->29022|35667->29033|35696->29034|35830->29139|35860->29140|35900->29152|36504->29727|36534->29728|36574->29739|36629->29765|36659->29766|36700->29778|36819->29868|36849->29869|36917->29909|36947->29910|36977->29911|37011->29916|37041->29917|37081->29928|37492->30310|37522->30311|37565->30325|37627->30358|37657->30359|37736->30409|37766->30410|37811->30426|37858->30444|37888->30445|37934->30462|38048->30547|38078->30548|38129->30570|38242->30654|38272->30655|38321->30675|38351->30676|38396->30692|38492->30759|38522->30760|38572->30781|38661->30841|38691->30842|38744->30866|38774->30867|38816->30881|38846->30882|38883->30891|38912->30892|39028->30979|39058->30980|39094->30988|39995->31860|40025->31861|40060->31868|40116->31896|40145->31897|40214->31937|40244->31938|40286->31951|40369->32005|40399->32006|40440->32018|42512->34061|42542->34062|42580->34072|42610->34073|42645->34080|42674->34081
                  LINES: 27->2|32->2|34->4|34->4|34->4|35->5|61->31|61->31|61->31|62->32|62->32|62->32|62->32|62->32|62->32|62->32|62->32|62->32|62->32|63->33|63->33|63->33|63->33|63->33|63->33|64->34|64->34|64->34|64->34|64->34|64->34|65->35|65->35|65->35|65->35|65->35|65->35|66->36|66->36|66->36|66->36|66->36|66->36|67->37|67->37|67->37|67->37|67->37|67->37|68->38|68->38|68->38|68->38|68->38|68->38|69->39|69->39|69->39|69->39|69->39|69->39|70->40|70->40|70->40|70->40|70->40|70->40|71->41|71->41|71->41|71->41|71->41|71->41|72->42|72->42|72->42|72->42|72->42|72->42|74->44|74->44|74->44|74->44|74->44|74->44|74->44|74->44|74->44|78->48|78->48|78->48|79->49|79->49|79->49|80->50|80->50|80->50|81->51|81->51|81->51|82->52|82->52|82->52|83->53|83->53|83->53|84->54|84->54|84->54|85->55|85->55|85->55|87->57|87->57|87->57|89->59|89->59|89->59|91->61|91->61|91->61|94->64|95->65|118->88|118->88|118->88|120->90|120->90|120->90|120->90|121->91|121->91|121->91|122->92|122->92|122->92|123->93|123->93|123->93|124->94|124->94|124->94|125->95|125->95|125->95|126->96|126->96|126->96|127->97|127->97|127->97|129->99|129->99|129->99|131->101|131->101|131->101|133->103|133->103|133->103|137->107|138->108|153->123|153->123|153->123|371->341|371->341|411->381|411->381|411->381|622->592|622->592|662->632|664->634|667->637|667->637|669->639|687->657|687->657|688->658|690->660|690->660|690->660|690->660|694->664|694->664|694->664|695->665|720->690|720->690|722->692|723->693|723->693|724->694|724->694|730->700|730->700|732->702|744->714|744->714|745->715|745->715|745->715|746->716|748->718|748->718|752->722|752->722|753->723|753->723|753->723|754->724|754->724|754->724|755->725|757->727|757->727|760->730|760->730|760->730|760->730|760->730|761->731|772->742|772->742|773->743|774->744|774->744|776->746|776->746|777->747|777->747|777->747|778->748|781->751|781->751|782->752|783->753|783->753|785->755|785->755|786->756|788->758|788->758|789->759|791->761|791->761|793->763|793->763|795->765|795->765|797->767|797->767|803->773|803->773|805->775|821->791|821->791|822->792|822->792|822->792|823->793|825->795|825->795|829->799|829->799|829->799|829->799|829->799|830->800|843->813|843->813|844->814|845->815|845->815|847->817|847->817|848->818|848->818|848->818|849->819|852->822|852->822|853->823|854->824|854->824|856->826|856->826|857->827|859->829|859->829|860->830|862->832|862->832|864->834|864->834|866->836|866->836|867->837|867->837|873->843|873->843|875->845|892->862|892->862|893->863|895->865|895->865|897->867|897->867|899->869|901->871|901->871|902->872|956->926|956->926|957->927|957->927|958->928|958->928
                  -- GENERATED --
              */
          