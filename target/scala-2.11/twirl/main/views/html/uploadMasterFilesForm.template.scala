
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object uploadMasterFilesForm_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class uploadMasterFilesForm extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[List[Integer],String,String,List[Campaign],List[Location],String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(resultdata:List[Integer],uploadId:String,uploadType:String,campaignList:List[Campaign],locationList:List[Location],dealerCode:String,userName:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.152*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerCode)/*2.56*/ {_display_(Seq[Any](format.raw/*2.58*/("""

"""),_display_(/*4.2*/helper/*4.8*/.form(action = routes.UploadExcelController.uploadExcelData(), 'enctype -> "multipart/form-data")/*4.105*/ {_display_(Seq[Any](format.raw/*4.107*/("""


"""),format.raw/*7.1*/("""<input type="hidden" value=""""),_display_(/*7.30*/uploadId),format.raw/*7.38*/("""">


<div class="panel panel-info">
    <div class="panel-heading"><strong>Master Data Upload</strong></div>
    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-primary">
                    <div class="panel-heading"><b>Select file from your computer</b></div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-lg-3">
                                <label>Select File Format:</label>
                                <select name="dataType" id="upload_form_name" class="form-control">
                                    <option value= "select">--Select--</option>
                                    <option value= "sale_register">Sale Register</option>
                                    <option value= "job_card">Job Card</option>
                                    <option value= "bill">Bills</option>
                                    <option value= "SMR">SMR</option>
                                    <option value= "campaign">Campaign</option>

                                </select> 
                            </div>
                            <div class="col-md-3 campaignNameInsert" style="display:none" >
                                <div class="form-group">
                                    <label for="campaignName">Campaign Name</label>
                                    <select name="campaignName" id="campaignName" class="form-control">
                                        <option value='select'>--Select Campaign--</option>
                                        """),_display_(/*39.42*/for(campaign_list<-campaignList) yield /*39.74*/{_display_(Seq[Any](format.raw/*39.75*/("""
                                        """),format.raw/*40.41*/("""<option value=""""),_display_(/*40.57*/campaign_list/*40.70*/.getCampaignName()),format.raw/*40.88*/("""">"""),_display_(/*40.91*/campaign_list/*40.104*/.getCampaignName()),format.raw/*40.122*/("""</option>
                                        """)))}),format.raw/*41.42*/("""
                                """),format.raw/*42.33*/("""</select> 
                                    
                                </div>				 
                            </div>
                            <div class="col-lg-3" style="display:none" >  
                                <label>Sheet Name:</label>                                           
                                <input type="text" class="form-control" name="sheetName" id="sheetName" value="Sheet1">
                            </div>

                            <div class="col-lg-3">
                                <label>&nbsp;</label>  
                                <input type="file" name="uploadfiles" id="uploadfiles" >  
                                <span id="lblError" style="color: red;"></span>
                            </div>

                        </div>
                        <div class="row"> 
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="cityName">Select City</label>
                                    <select class="form-control" id="city" name="name" onchange="ajaxCallToLoadWorkShopByCity();">	
                                        <option value='select'>--Select City--</option>
                                        """),_display_(/*64.42*/for(location_list<-locationList) yield /*64.74*/{_display_(Seq[Any](format.raw/*64.75*/("""
                                        """),format.raw/*65.41*/("""<option value=""""),_display_(/*65.57*/location_list/*65.70*/.getName()),format.raw/*65.80*/("""">"""),_display_(/*65.83*/location_list/*65.96*/.getName()),format.raw/*65.106*/("""</option>
                                        """)))}),format.raw/*66.42*/("""
                                    """),format.raw/*67.37*/("""</select>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="workshopId">Select Workshop</label>
                                    <select class="form-control" id="workshop" name="id" >					  
                                        <option value=""></option>
                                    </select>
                                </div>				 
                            </div>
                            <div class="col-md-3 campaignStartdate" style="display:none" >
                                <div class="form-group">
                                    <label for="seldate">Select From Date</label>
                                    <input type="text" class="form-control datepicker"  id="campaignStartdate" name="campaignStartdate" readonly>			  
                                </div>				 
                            </div>
                            <div class="col-md-3 campaignExpiry" style="display:none">
                                <div class="form-group">
                                    <label for="seldateExp">Select Expiry Date</label>
                                    <input type="text" class="form-control datepicker" id="campaignExpiry" name="campaignExpiry" readonly>
                                </div>				 
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary pull-right upload-submitExcel" name="submit" id="uploadFileSubmit" value="Submit" >
                      
                      
                      """),_display_(/*95.24*/if(uploadId!="0")/*95.41*/{_display_(Seq[Any](format.raw/*95.42*/("""
                        
                        
                                """),format.raw/*98.33*/("""<div class="form-group">
                                    <label></label>
                                    """),_display_(/*100.38*/{if(resultdata(0)==0){
                                    
                                     <span style="color:red">Data Not Uploaded</span>
                                     }else{
                                     
                                     <span style="color:green">Data Uploaded Successfully</span>
                                      <p>Total Record are {resultdata(1)} and records uploded successfully is {resultdata(0)}</p>
                                   
                                     }}),format.raw/*108.40*/("""
                                     
                                  """),format.raw/*110.35*/("""<a href="/CREManager/uploadDataReport/"""),_display_(/*110.74*/uploadId),format.raw/*110.82*/("""/"""),_display_(/*110.84*/uploadType),format.raw/*110.94*/("""" >Click Here View Report</a>  
                                </div>				 
                           
                            """)))}),format.raw/*113.30*/("""
                            
                            
                              
                    """),format.raw/*117.21*/("""</div>
                    
                     

                </div>
                
                

                <input type="checkbox" style="display:none" name="useworker" ><br />
                <input type="checkbox" style="display:none" name="xferable" checked><br />
                <input type="checkbox" style="display:none" name="userabs" checked><br />
                <div class="hr-line-dashed"></div>
                <div class="ibox-content">			
                    <div class="table-responsive">
                        <table class="table table-bordered header-fixed table-responsive" id="tableData" style="width:80%">
                            <thead>

                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div> 
                    <br />

                </div>

            </div>	
            
           
                            

        </div>
        <!--<button type="submit" class="btn btn-primary" name="submit" onclick="ajaxUploadData()" >Submit</button>-->


    </div>
    <script>
        $(document).ready(function () """),format.raw/*154.39*/("""{"""),format.raw/*154.40*/("""
            """),format.raw/*155.13*/("""$.fn.checkFileType = function (options) """),format.raw/*155.53*/("""{"""),format.raw/*155.54*/("""
                """),format.raw/*156.17*/("""var defaults = """),format.raw/*156.32*/("""{"""),format.raw/*156.33*/("""
                    """),format.raw/*157.21*/("""allowedExtensions: [],
                    success: function () """),format.raw/*158.42*/("""{"""),format.raw/*158.43*/("""}"""),format.raw/*158.44*/(""",
                    error: function () """),format.raw/*159.40*/("""{"""),format.raw/*159.41*/("""}"""),format.raw/*159.42*/("""
                """),format.raw/*160.17*/("""}"""),format.raw/*160.18*/(""";
                options = $.extend(defaults, options);

                return this.each(function () """),format.raw/*163.46*/("""{"""),format.raw/*163.47*/("""

                    """),format.raw/*165.21*/("""$(this).on('change', function () """),format.raw/*165.54*/("""{"""),format.raw/*165.55*/("""
                        """),format.raw/*166.25*/("""var value = $(this).val(),
                                file = value.toLowerCase(),
                                extension = file.substring(file.lastIndexOf('.') + 1);

                        if ($.inArray(extension, options.allowedExtensions) == -1) """),format.raw/*170.84*/("""{"""),format.raw/*170.85*/("""
                            """),format.raw/*171.29*/("""options.error();
                            $(this).focus();
                        """),format.raw/*173.25*/("""}"""),format.raw/*173.26*/(""" """),format.raw/*173.27*/("""else """),format.raw/*173.32*/("""{"""),format.raw/*173.33*/("""
                            """),format.raw/*174.29*/("""options.success();

                        """),format.raw/*176.25*/("""}"""),format.raw/*176.26*/("""

                    """),format.raw/*178.21*/("""}"""),format.raw/*178.22*/(""");

                """),format.raw/*180.17*/("""}"""),format.raw/*180.18*/(""");
            """),format.raw/*181.13*/("""}"""),format.raw/*181.14*/(""";
            $('#uploadfiles').checkFileType("""),format.raw/*182.45*/("""{"""),format.raw/*182.46*/("""
                """),format.raw/*183.17*/("""allowedExtensions: ['xlsx'],
                error: function () """),format.raw/*184.36*/("""{"""),format.raw/*184.37*/("""
                    """),format.raw/*185.21*/("""Lobibox.alert('warning', """),format.raw/*185.46*/("""{"""),format.raw/*185.47*/("""
                        """),format.raw/*186.25*/("""msg: "Please select XLSX file only"
                    """),format.raw/*187.21*/("""}"""),format.raw/*187.22*/(""");
                """),format.raw/*188.17*/("""}"""),format.raw/*188.18*/("""
            """),format.raw/*189.13*/("""}"""),format.raw/*189.14*/(""");
            $("#uploadFileSubmit").click(function () """),format.raw/*190.54*/("""{"""),format.raw/*190.55*/("""
                """),format.raw/*191.17*/("""var uploadName = $("#upload_form_name").val();
                var sheetName = $("#sheetName").val();
                var city = $("#city").val();
                var workshop = $("#workshop").val();
                var uploadfiles = $("#uploadfiles").val();
				console.log(workshop);
				console.log(city);
                if (uploadName != 'select' && sheetName != '' && uploadfiles != '' && city != 'select' && workshop != '0') """),format.raw/*198.124*/("""{"""),format.raw/*198.125*/("""
                   
                    
                    """),format.raw/*201.21*/("""return true;
                """),format.raw/*202.17*/("""}"""),format.raw/*202.18*/(""" """),format.raw/*202.19*/("""else """),format.raw/*202.24*/("""{"""),format.raw/*202.25*/("""

                    """),format.raw/*204.21*/("""if (uploadName == 'select' || uploadName == '') """),format.raw/*204.69*/("""{"""),format.raw/*204.70*/("""

                        """),format.raw/*206.25*/("""Lobibox.notify('warning', """),format.raw/*206.51*/("""{"""),format.raw/*206.52*/("""
                            """),format.raw/*207.29*/("""msg: "Please select the type of file"
                        """),format.raw/*208.25*/("""}"""),format.raw/*208.26*/(""");
                        return false;

                    """),format.raw/*211.21*/("""}"""),format.raw/*211.22*/("""
                    """),format.raw/*212.21*/("""if (sheetName == '') """),format.raw/*212.42*/("""{"""),format.raw/*212.43*/("""

                        """),format.raw/*214.25*/("""Lobibox.notify('warning', """),format.raw/*214.51*/("""{"""),format.raw/*214.52*/("""
                            """),format.raw/*215.29*/("""msg: "Please enter the sheet name"
                        """),format.raw/*216.25*/("""}"""),format.raw/*216.26*/(""");
                        return false;

                    """),format.raw/*219.21*/("""}"""),format.raw/*219.22*/("""
                    """),format.raw/*220.21*/("""if (uploadfiles == '') """),format.raw/*220.44*/("""{"""),format.raw/*220.45*/("""

                        """),format.raw/*222.25*/("""Lobibox.notify('warning', """),format.raw/*222.51*/("""{"""),format.raw/*222.52*/("""
                            """),format.raw/*223.29*/("""msg: "Please choose the file"
                        """),format.raw/*224.25*/("""}"""),format.raw/*224.26*/(""");
                        return false;

                    """),format.raw/*227.21*/("""}"""),format.raw/*227.22*/("""
                    """),format.raw/*228.21*/("""if (city == 'select' || city == '') """),format.raw/*228.57*/("""{"""),format.raw/*228.58*/("""

                        """),format.raw/*230.25*/("""Lobibox.notify('warning', """),format.raw/*230.51*/("""{"""),format.raw/*230.52*/("""
                            """),format.raw/*231.29*/("""msg: "Please select the city"
                        """),format.raw/*232.25*/("""}"""),format.raw/*232.26*/(""");
                        return false;

                    """),format.raw/*235.21*/("""}"""),format.raw/*235.22*/("""
                    """),format.raw/*236.21*/("""if (workshop == '0' || workshop == '') """),format.raw/*236.60*/("""{"""),format.raw/*236.61*/("""

                        """),format.raw/*238.25*/("""Lobibox.notify('warning', """),format.raw/*238.51*/("""{"""),format.raw/*238.52*/("""
                            """),format.raw/*239.29*/("""msg: "Please select the workshop"
                        """),format.raw/*240.25*/("""}"""),format.raw/*240.26*/(""");
                        return false;

                    """),format.raw/*243.21*/("""}"""),format.raw/*243.22*/("""
                """),format.raw/*244.17*/("""}"""),format.raw/*244.18*/("""
            """),format.raw/*245.13*/("""}"""),format.raw/*245.14*/(""");

        """),format.raw/*247.9*/("""}"""),format.raw/*247.10*/(""");

    </script>


    """)))}),format.raw/*252.6*/("""
    """)))}))
      }
    }
  }

  def render(resultdata:List[Integer],uploadId:String,uploadType:String,campaignList:List[Campaign],locationList:List[Location],dealerCode:String,userName:String): play.twirl.api.HtmlFormat.Appendable = apply(resultdata,uploadId,uploadType,campaignList,locationList,dealerCode,userName)

  def f:((List[Integer],String,String,List[Campaign],List[Location],String,String) => play.twirl.api.HtmlFormat.Appendable) = (resultdata,uploadId,uploadType,campaignList,locationList,dealerCode,userName) => apply(resultdata,uploadId,uploadType,campaignList,locationList,dealerCode,userName)

  def ref: this.type = this

}


}

/**/
object uploadMasterFilesForm extends uploadMasterFilesForm_Scope0.uploadMasterFilesForm
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:39 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/uploadMasterFilesForm.scala.html
                  HASH: 7d7502e882cd566f3e50aca4e21bc223c9373156
                  MATRIX: 842->1|1088->151|1116->154|1178->208|1217->210|1247->215|1260->221|1366->318|1406->320|1438->326|1493->355|1521->363|3251->2066|3299->2098|3338->2099|3408->2141|3451->2157|3473->2170|3512->2188|3542->2191|3565->2204|3605->2222|3688->2274|3750->2308|5094->3625|5142->3657|5181->3658|5251->3700|5294->3716|5316->3729|5347->3739|5377->3742|5399->3755|5431->3765|5514->3817|5580->3855|7363->5611|7389->5628|7428->5629|7542->5715|7686->5831|8246->6369|8350->6444|8417->6483|8447->6491|8477->6493|8509->6503|8677->6639|8820->6753|10066->7970|10096->7971|10139->7985|10208->8025|10238->8026|10285->8044|10329->8059|10359->8060|10410->8082|10504->8147|10534->8148|10564->8149|10635->8191|10665->8192|10695->8193|10742->8211|10772->8212|10907->8318|10937->8319|10990->8343|11052->8376|11082->8377|11137->8403|11428->8665|11458->8666|11517->8696|11634->8784|11664->8785|11694->8786|11728->8791|11758->8792|11817->8822|11892->8868|11922->8869|11975->8893|12005->8894|12056->8916|12086->8917|12131->8933|12161->8934|12237->8981|12267->8982|12314->9000|12408->9065|12438->9066|12489->9088|12543->9113|12573->9114|12628->9140|12714->9197|12744->9198|12793->9218|12823->9219|12866->9233|12896->9234|12982->9291|13012->9292|13059->9310|13528->9749|13559->9750|13653->9815|13712->9845|13742->9846|13772->9847|13806->9852|13836->9853|13889->9877|13966->9925|13996->9926|14053->9954|14108->9980|14138->9981|14197->10011|14289->10074|14319->10075|14413->10140|14443->10141|14494->10163|14544->10184|14574->10185|14631->10213|14686->10239|14716->10240|14775->10270|14864->10330|14894->10331|14988->10396|15018->10397|15069->10419|15121->10442|15151->10443|15208->10471|15263->10497|15293->10498|15352->10528|15436->10583|15466->10584|15560->10649|15590->10650|15641->10672|15706->10708|15736->10709|15793->10737|15848->10763|15878->10764|15937->10794|16021->10849|16051->10850|16145->10915|16175->10916|16226->10938|16294->10977|16324->10978|16381->11006|16436->11032|16466->11033|16525->11063|16613->11122|16643->11123|16737->11188|16767->11189|16814->11207|16844->11208|16887->11222|16917->11223|16959->11237|16989->11238|17050->11268
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|35->4|35->4|35->4|38->7|38->7|38->7|70->39|70->39|70->39|71->40|71->40|71->40|71->40|71->40|71->40|71->40|72->41|73->42|95->64|95->64|95->64|96->65|96->65|96->65|96->65|96->65|96->65|96->65|97->66|98->67|126->95|126->95|126->95|129->98|131->100|139->108|141->110|141->110|141->110|141->110|141->110|144->113|148->117|185->154|185->154|186->155|186->155|186->155|187->156|187->156|187->156|188->157|189->158|189->158|189->158|190->159|190->159|190->159|191->160|191->160|194->163|194->163|196->165|196->165|196->165|197->166|201->170|201->170|202->171|204->173|204->173|204->173|204->173|204->173|205->174|207->176|207->176|209->178|209->178|211->180|211->180|212->181|212->181|213->182|213->182|214->183|215->184|215->184|216->185|216->185|216->185|217->186|218->187|218->187|219->188|219->188|220->189|220->189|221->190|221->190|222->191|229->198|229->198|232->201|233->202|233->202|233->202|233->202|233->202|235->204|235->204|235->204|237->206|237->206|237->206|238->207|239->208|239->208|242->211|242->211|243->212|243->212|243->212|245->214|245->214|245->214|246->215|247->216|247->216|250->219|250->219|251->220|251->220|251->220|253->222|253->222|253->222|254->223|255->224|255->224|258->227|258->227|259->228|259->228|259->228|261->230|261->230|261->230|262->231|263->232|263->232|266->235|266->235|267->236|267->236|267->236|269->238|269->238|269->238|270->239|271->240|271->240|274->243|274->243|275->244|275->244|276->245|276->245|278->247|278->247|283->252
                  -- GENERATED --
              */
          