
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object assignedInteractionPageForUpload_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class assignedInteractionPageForUpload extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(dealerName:String,user:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.33*/("""

"""),_display_(/*5.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*5.52*/ {_display_(Seq[Any](format.raw/*5.54*/("""

"""),_display_(/*7.2*/helper/*7.8*/.form(action = routes.CallInteractionController.toUploadInteractionsByCREManager(),'enctype -> "multipart/form-data")/*7.125*/{_display_(Seq[Any](format.raw/*7.126*/("""
  
      """),format.raw/*9.7*/("""<div class="panel panel-info">
        <div class="panel-heading"><strong>Call Interactions Upload</strong></div>
        <div class="panel-body">
        <div class="row">
            
            <div class=col-md-8>
                  <h4>Select file from your computer</h4>
          
            <div class="form-inline">
              
             <div class="form-group">
                <input type="file" name="uploadfiles" id="js-upload-files" multiple>
               
                <span id="lblError" style="color: red;"></span>
                
              </div>
             
              <button type="submit" class="btn btn-sm btn-primary" id='js-upload-submit' name="selectedFile">Add Interactions</button>
             
            </div>
            </div>
        </div>
               					
          </div>
        </div>
    """)))}),format.raw/*34.6*/("""
    
   
    
""")))}),format.raw/*38.2*/("""

"""))
      }
    }
  }

  def render(dealerName:String,user:String): play.twirl.api.HtmlFormat.Appendable = apply(dealerName,user)

  def f:((String,String) => play.twirl.api.HtmlFormat.Appendable) = (dealerName,user) => apply(dealerName,user)

  def ref: this.type = this

}


}

/**/
object assignedInteractionPageForUpload extends assignedInteractionPageForUpload_Scope0.assignedInteractionPageForUpload
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:12 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/assignedInteractionPageForUpload.scala.html
                  HASH: bf3b2ab7ac5cd8dd8577205c558ac9908fb92161
                  MATRIX: 806->5|932->36|962->41|1020->91|1059->93|1089->98|1102->104|1228->221|1267->222|1305->234|2216->1115|2266->1135
                  LINES: 27->3|32->3|34->5|34->5|34->5|36->7|36->7|36->7|36->7|38->9|63->34|67->38
                  -- GENERATED --
              */
          