
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object InsuranceCallLogPageCREManager_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class InsuranceCallLogPageCREManager extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[CallDispositionData],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long,String],campaignListInsurance :List[Campaign],serviceTypeList :List[ServiceTypes],dispoList:List[CallDispositionData]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.195*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*2.52*/ {_display_(Seq[Any](format.raw/*2.54*/("""	



"""),format.raw/*6.1*/("""<style>
    td, th """),format.raw/*7.12*/("""{"""),format.raw/*7.13*/("""
        """),format.raw/*8.9*/("""border: 1px solid black;
        overflow: hidden; 
        text-align:center!important;
    """),format.raw/*11.5*/("""}"""),format.raw/*11.6*/("""
"""),format.raw/*12.1*/("""</style>
 
  <div>

        <div class="panel panel-primary">
            <div class="panel-heading">   
                Insurance Call Log Information </div>
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab" id="Tab1" onclick="InsuranceassignedInteractionDataMR();">Scheduled</a> </li>
                    <li><a href="#profile" data-toggle="tab" id="Tab2" onclick="ajaxInsuranceCallForFollowUpRequiredServerMR(4);">Follow Up Required</a> </li>
                    <li><a href="#messages" data-toggle="tab" id="Tab3" onclick="ajaxInsuranceCallForFollowUpRequiredServerMR(25);">Book Appointments</a> </li>
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxInsuranceCallForFollowUpRequiredServerMR(26);" >Renewal Not Required</a> </li>
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxInsuranceCallForNonContactsServerMR(1);">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxInsuranceCallForNonContactsServerMR(2);">Dropped</a> </li>
                </ul>
                <div class="tab-content">
                <div class="panel panel-default" style="border-top: #fff;">
				<div class="panel-body">
                <div class="row">
		<div class="col-md-2" id="cityDiv">
		<div class="form-group">
                <label>City </label>
               
               <select class="form-control" id="city" name="cityName" onchange="ajaxCallToLoadWorkShopByCity();"> 
                <option value='--Select--'>--Select--</option>
                
				"""),_display_(/*39.6*/for(city <- listCity) yield /*39.27*/{_display_(Seq[Any](format.raw/*39.28*/("""                  	                         
                    """),format.raw/*40.21*/("""<option value=""""),_display_(/*40.37*/city/*40.41*/.getName()),format.raw/*40.51*/("""">"""),_display_(/*40.54*/city/*40.58*/.getName()),format.raw/*40.68*/("""</option>
                    """)))}),format.raw/*41.22*/("""
                """),format.raw/*42.17*/("""</select>
</div>
            </div> 
			<div class="col-md-2" id="workshopDiv">
			<div class="form-group">
                <label>WorkShop Location </label>               
                <select class="form-control" id="workshop" name="workshopId" onchange="ajaxCallToLoadCRESByWorkshop();">                 			    
                

                </select>
			</div>
            </div> 
		
            <div class="col-md-2" id="cresDiv">
			 <label>Select CRE's </label>
			<div class="form-group">
              <select class="selectpicker filter form-control" data-column-index="7" id="ddlCreIds" name="ddlCreIds" multiple>
                
                    
                </select> 
</div>


            </div>
            
                <div class="col-md-2 tab-pane" id="campaignDiv" >
                                
                                <label>Select Campaign</label>
						<select class="filter form-control" id="campaignName" data-column-index="0" name="campaignName">
							<option value="0" >--Select--</option>
							"""),_display_(/*72.9*/for(campaign_List<-campaignListInsurance) yield /*72.50*/{_display_(Seq[Any](format.raw/*72.51*/("""
		                                """),format.raw/*73.35*/("""<option value=""""),_display_(/*73.51*/campaign_List/*73.64*/.getId()),format.raw/*73.72*/("""">"""),_display_(/*73.75*/campaign_List/*73.88*/.getCampaignName()),format.raw/*73.106*/("""</option>
		                             """)))}),format.raw/*74.33*/("""
		                                
						"""),format.raw/*76.7*/("""</select>
					</div>
					
					<div class="col-md-2" id="fromDateDiv">
					<label>From Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="From Assign Date" data-column-index="1" id="fromduedaterange" name="fromduedaterange" readonly>
				  </div>
				  <div class="col-md-2" id="toDateDiv">
					<label>To Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="To Assign Date" data-column-index="2" id="toduedaterange" name="toduedaterange" readonly>
				  </div>
				  </div>
				  
				  <div class="row">
				   
							<div class="col-md-2">
				  <label for="">Renewal Type</label>
				  <select class="filter form-control"  name="renewalType" data-column-index="4">
						<option value="0">--Select--</option>
						<option value="1st Renewal">1st Renewal</option>
						<option value="2nd Renewal">2nd Renewal</option>
						<option value="3rd Renewal">3rd Renewal</option>
						<option value="4th Renewal">4th Renewal</option>
						<option value="5th Renewal">5th Renewal</option>
						<option value="6th Renewal">6th Renewal</option>
						<option value="7th Renewal">7th Renewal</option>
						<option value="8th Renewal">8th Renewal</option>
					 </select>
				</div>
					 
					<div class="col-md-3" id="serviceBookTypeDiv" style="display:none">						
					<label>Select Booked service type</label>
						<select class="filter form-control" id="serviceBookedType" data-column-index="4" name="serviceBookedType">
							<option value="0" >--Select--</option>
	
					                                
						</select>
					</div>
					<div class="col-md-3" id="lastDispoTypeDiv" style="display:none">						
					<label>Last Disposition</label>
						<select class="filter form-control" id="lastDispo" data-column-index="5" >
							<option value="0" >--Select--</option>
								"""),_display_(/*118.10*/for(dispo <- dispoList) yield /*118.33*/{_display_(Seq[Any](format.raw/*118.34*/("""
			"""),format.raw/*119.4*/("""<option value=""""),_display_(/*119.20*/dispo/*119.25*/.getDisposition()),format.raw/*119.42*/("""">"""),_display_(/*119.45*/dispo/*119.50*/.getDisposition()),format.raw/*119.67*/("""</option>
			
		""")))}),format.raw/*121.4*/("""
							
					                                
						"""),format.raw/*124.7*/("""</select>
					</div>
					<div class="col-md-3" id="droppedcountDiv" style="display:none">						
					<label>Dropped Count</label>
						<select class="filter form-control" id="droppedCount" data-column-index="6">
							<option value="0" >--Select--</option>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							
					                                
						</select>
					</div>
				</div>
                </div>
				  </div>
                    <div class="panel panel-default tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="InsuranceassignedInteractionTableMR">
                                        <thead>
                                            <tr> 

				    		<th>CRE Name</th>
				    						    		<th>Campaign Name</th>
				    		<th>Customer Name</th>
				    		<th>Mobile Number</th>
				    		<th>Vehicle Reg No</th>
				    		<th>ChassisNo</th>
				    		<th>Policy Due Month</th>
				    		<th>Policy Due Date</th>
				    		<th>Next Renewal Type</th>
				    		<th>Last Insurance Company</th>
                                            </tr>
                                        </thead>
                                        <tbody>								
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="InsurancefollowUpRequiredServerDataMR4" >
                                        <thead>
                         
                                         	<tr> 
                                          		<th>Is CallInitiated</th>
                                          		<th>CRE Name</th>
                                          		<th>Campaign Name</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>
                                                <th>Model</th>
                                                <th>FollowUp Date</th>                                    
                                                <th>FollowUp Time</th>
                                                <th>Renewal Type</th>                                               
                                                <th>Last Disposition</th>
                                            </tr>    
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="messages">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="InsurancefollowUpRequiredServerDataMR25">
                                        <thead>
                                           <tr> 
                                            	<th>Campaign Name</th>
                                          		<th>CRE Name</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>
                                                <th>Appointment Date</th>                                    
                                                <th>Appointment Time</th>
                                                <th>Appointment Status</th>
                                                <th>Renewal Type</th>
                                                <th>Insurance Agent</th>
                                                <th>Last Disposition</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="settings">
                         <div class="panel-body inf-content">
                        <div class="dataTable_wrapper">                            
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="InsurancefollowUpRequiredServerDataMR26">
                                    <thead>
                                        <tr> 
                                        	<th>Campaign Name</th>
                                          		<th>CRE Name</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>
                                                <th>Renewal Type</th>
                                                <th>Insurance Agent</th>
                                                <th>Last Disposition</th>
                                                <th>Reason</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div><!--End tab panel-->
                    <div class="tab-pane fade " id="nonContacts">
                        <div class="panel-body inf-content"> 
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="InsurancenonContactsServerDataMR1">
                                    <thead>
                                        <tr> 
                                        	<th>Campaign Name</th>
                                       	 	<th>CRE Name</th>                         
                                            <th>Customer Name</th>
                                                                                            <th>Call date</th>
                                            
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                      
                
                                            
                                          
                                                <th>Last Disposition</th>
                                                 											
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="InsurancenonContactsServerDataMR2">
                                        <thead>
                                                 <tr> 
                                        <th>Campaign Name</th>
                                       	 	<th>CRE Name</th>                         
                                            <th>Customer Name</th>
                                                                                            <th>Call date</th>
                                            
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                      
                
                                            
                                          
                                                <th>Last Disposition</th>
                                                 											
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div><!--End tab content-->
            </div><!--End panel body-->
        </div>
   
</div>
""")))}),format.raw/*308.2*/(""" 

"""),format.raw/*310.1*/("""<script type="text/javascript"> 

function InsuranceassignedInteractionDataMR()"""),format.raw/*312.46*/("""{"""),format.raw/*312.47*/("""
    """),format.raw/*313.5*/("""// alert("server side data load");
    document.getElementById("cresDiv").style.display = "block";
	//document.getElementById("workshopDiv").style.display = "block";
	//document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("campaignDiv").style.display = "block";
    //document.getElementById("serviceBookTypeDiv").style.display = "none";
    //document.getElementById("serviceTypeDiv").style.display = "block";

	//document.getElementById("lastDispoTypeDiv").style.display = "none";
	//document.getElementById("droppedcountDiv").style.display = "none";

    var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*329.40*/("""{"""),format.raw/*329.41*/("""
    """),format.raw/*330.5*/("""if(myOption.options[i].selected)"""),format.raw/*330.37*/("""{"""),format.raw/*330.38*/("""
        """),format.raw/*331.9*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*331.54*/("""{"""),format.raw/*331.55*/("""
    	 """),format.raw/*332.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
    	 console.log("Users id"+UserIds);
        """),format.raw/*334.9*/("""}"""),format.raw/*334.10*/("""
    """),format.raw/*335.5*/("""}"""),format.raw/*335.6*/("""
"""),format.raw/*336.1*/("""}"""),format.raw/*336.2*/("""
"""),format.raw/*337.1*/("""if(UserIds.length > 0)"""),format.raw/*337.23*/("""{"""),format.raw/*337.24*/("""
    	"""),format.raw/*338.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
"""),format.raw/*339.1*/("""}"""),format.raw/*339.2*/("""
"""),format.raw/*340.1*/("""else"""),format.raw/*340.5*/("""{"""),format.raw/*340.6*/("""
"""),format.raw/*341.1*/("""UserIds="Select";
"""),format.raw/*342.1*/("""}"""),format.raw/*342.2*/("""
    """),format.raw/*343.5*/("""var ajaxUrl = "/CREManager/InsuranceassignedInteractionTableDataMR/"+UserIds+"";
    
    
    var table= $('#InsuranceassignedInteractionTableMR').dataTable( """),format.raw/*346.69*/("""{"""),format.raw/*346.70*/("""
        """),format.raw/*347.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,	
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*356.59*/("""{"""),format.raw/*356.60*/("""
              """),format.raw/*357.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*359.15*/("""}"""),format.raw/*359.16*/("""
    """),format.raw/*360.5*/("""}"""),format.raw/*360.6*/(""" """),format.raw/*360.7*/(""");

    FilterOptionDataMR(table);
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*364.61*/("""{"""),format.raw/*364.62*/("""
      """),format.raw/*365.7*/("""$($.fn.dataTable.tables(true)).DataTable()
         .columns.adjust();
   """),format.raw/*367.4*/("""}"""),format.raw/*367.5*/(""");
    
    """),format.raw/*369.5*/("""}"""),format.raw/*369.6*/("""




"""),format.raw/*374.1*/("""function ajaxInsuranceCallForFollowUpRequiredServerMR(buckettype)"""),format.raw/*374.66*/("""{"""),format.raw/*374.67*/("""
	"""),format.raw/*375.2*/("""document.getElementById("cresDiv").style.display = "block";
	//document.getElementById("workshopDiv").style.display = "block";
	//document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	//document.getElementById("serviceBookTypeDiv").style.display = "none";
	//document.getElementById("serviceTypeDiv").style.display = "block";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	//document.getElementById("droppedcountDiv").style.display = "none";
	          

	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*390.41*/("""{"""),format.raw/*390.42*/("""
	    """),format.raw/*391.6*/("""if(myOption.options[i].selected)"""),format.raw/*391.38*/("""{"""),format.raw/*391.39*/("""
	        """),format.raw/*392.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*392.55*/("""{"""),format.raw/*392.56*/("""
	    	 """),format.raw/*393.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*394.10*/("""}"""),format.raw/*394.11*/("""
	    """),format.raw/*395.6*/("""}"""),format.raw/*395.7*/("""
	"""),format.raw/*396.2*/("""}"""),format.raw/*396.3*/("""
	"""),format.raw/*397.2*/("""if(UserIds.length > 0)"""),format.raw/*397.24*/("""{"""),format.raw/*397.25*/("""
	    	"""),format.raw/*398.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*399.2*/("""}"""),format.raw/*399.3*/("""
	"""),format.raw/*400.2*/("""else"""),format.raw/*400.6*/("""{"""),format.raw/*400.7*/("""
	"""),format.raw/*401.2*/("""UserIds="Select";
	"""),format.raw/*402.2*/("""}"""),format.raw/*402.3*/("""
	    """),format.raw/*403.6*/("""var ajaxUrl = "/CREManager/InsurancefollowUpCallLogTableDataMR/"+UserIds+"/"+buckettype+"";
	    
	    
	    var table= $('#InsurancefollowUpRequiredServerDataMR'+buckettype).dataTable( """),format.raw/*406.83*/("""{"""),format.raw/*406.84*/("""
	        """),format.raw/*407.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*416.60*/("""{"""),format.raw/*416.61*/("""
              """),format.raw/*417.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*419.15*/("""}"""),format.raw/*419.16*/("""
	    """),format.raw/*420.6*/("""}"""),format.raw/*420.7*/(""" """),format.raw/*420.8*/(""");

	    FilterOptionDataMR(table);
	    
	"""),format.raw/*424.2*/("""}"""),format.raw/*424.3*/("""



"""),format.raw/*428.1*/("""function ajaxInsuranceCallForNonContactsServerMR(buckettype)"""),format.raw/*428.61*/("""{"""),format.raw/*428.62*/("""
	"""),format.raw/*429.2*/("""//alert("noncontact");
	
		document.getElementById("cresDiv").style.display = "block";
		//document.getElementById("workshopDiv").style.display = "block";
		//document.getElementById("cityDiv").style.display = "block";	
		document.getElementById("campaignDiv").style.display = "block";
		//document.getElementById("serviceBookTypeDiv").style.display = "none";
		//document.getElementById("serviceTypeDiv").style.display = "none";
		document.getElementById("fromDateDiv").style.display = "block";
		document.getElementById("toDateDiv").style.display = "block";
		document.getElementById("lastDispoTypeDiv").style.display = "block";
		//document.getElementById("droppedcountDiv").style.display = "block";

	var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*445.40*/("""{"""),format.raw/*445.41*/("""
    """),format.raw/*446.5*/("""if(myOption.options[i].selected)"""),format.raw/*446.37*/("""{"""),format.raw/*446.38*/("""
        """),format.raw/*447.9*/("""if(myOption.options[i].value != "Select")"""),format.raw/*447.50*/("""{"""),format.raw/*447.51*/("""
    		"""),format.raw/*448.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*449.9*/("""}"""),format.raw/*449.10*/("""
    """),format.raw/*450.5*/("""}"""),format.raw/*450.6*/("""
"""),format.raw/*451.1*/("""}"""),format.raw/*451.2*/("""
	"""),format.raw/*452.2*/("""if(UserIds.length > 0)"""),format.raw/*452.24*/("""{"""),format.raw/*452.25*/("""
    	"""),format.raw/*453.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*454.2*/("""}"""),format.raw/*454.3*/("""
	"""),format.raw/*455.2*/("""else"""),format.raw/*455.6*/("""{"""),format.raw/*455.7*/("""
		"""),format.raw/*456.3*/("""UserIds="Select";
	"""),format.raw/*457.2*/("""}"""),format.raw/*457.3*/("""
    """),format.raw/*458.5*/("""var ajaxUrl = "/CREManager/InsurancenonContactsServerDataTableMR/"+UserIds+"/"+buckettype+"";
     
    var table = $('#InsurancenonContactsServerDataMR'+buckettype).dataTable( """),format.raw/*460.78*/("""{"""),format.raw/*460.79*/("""
        """),format.raw/*461.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*470.59*/("""{"""),format.raw/*470.60*/("""
              """),format.raw/*471.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*473.15*/("""}"""),format.raw/*473.16*/("""
    """),format.raw/*474.5*/("""}"""),format.raw/*474.6*/(""" """),format.raw/*474.7*/(""");

    FilterOptionDataMR(table);
    
    """),format.raw/*478.5*/("""}"""),format.raw/*478.6*/("""






"""),format.raw/*485.1*/("""</script>
<script type="text/javascript">  
function FilterOptionDataMR(table)"""),format.raw/*487.35*/("""{"""),format.raw/*487.36*/("""
	"""),format.raw/*488.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*489.39*/("""{"""),format.raw/*489.40*/("""
    	"""),format.raw/*490.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
	    console.log("i : "+i);
		console.log("v length "+v.length);
	    
	    if(v.length>0)"""),format.raw/*496.20*/("""{"""),format.raw/*496.21*/("""
		    
		    """),format.raw/*498.7*/("""if(i == 7)"""),format.raw/*498.17*/("""{"""),format.raw/*498.18*/("""	

		    	"""),format.raw/*500.8*/("""var UserIds="",j;
		        myOption = document.getElementById('ddlCreIds');
		        console.log("here"+myOption);		        
		    for (j=0;j<myOption.options.length;j++)"""),format.raw/*503.46*/("""{"""),format.raw/*503.47*/("""
		        """),format.raw/*504.11*/("""if(myOption.options[j].selected)"""),format.raw/*504.43*/("""{"""),format.raw/*504.44*/("""
		            """),format.raw/*505.15*/("""if(myOption.options[j].value != "Select")"""),format.raw/*505.56*/("""{"""),format.raw/*505.57*/("""
		        		"""),format.raw/*506.13*/("""UserIds = UserIds + myOption.options[j].value + ",";
		            """),format.raw/*507.15*/("""}"""),format.raw/*507.16*/("""
		        """),format.raw/*508.11*/("""}"""),format.raw/*508.12*/("""
		    """),format.raw/*509.7*/("""}"""),format.raw/*509.8*/("""
		    	"""),format.raw/*510.8*/("""if(UserIds.length > 0)"""),format.raw/*510.30*/("""{"""),format.raw/*510.31*/("""
		        	"""),format.raw/*511.12*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
		        	values[7]=UserIds;
		    	"""),format.raw/*513.8*/("""}"""),format.raw/*513.9*/("""else"""),format.raw/*513.13*/("""{"""),format.raw/*513.14*/("""
		    		"""),format.raw/*514.9*/("""UserIds="Select";
		    		values[7]=UserIds;
		    	"""),format.raw/*516.8*/("""}"""),format.raw/*516.9*/("""
		    
		    """),format.raw/*518.7*/("""}"""),format.raw/*518.8*/("""else"""),format.raw/*518.12*/("""{"""),format.raw/*518.13*/("""
		    	"""),format.raw/*519.8*/("""values[i] = v;
			    """),format.raw/*520.8*/("""}"""),format.raw/*520.9*/("""
	
			       
		  
		    """),format.raw/*524.7*/("""}"""),format.raw/*524.8*/("""		    
	    
	    	"""),format.raw/*526.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');
	 	 //var table = $('#nonContactsServerDataMR').dataTable();
	 	 if(values.length>0) """),format.raw/*531.25*/("""{"""),format.raw/*531.26*/("""
			 """),format.raw/*532.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*532.37*/("""{"""),format.raw/*532.38*/("""
				 """),format.raw/*533.6*/("""console.log("values[i] : "+values[i]);	
			    table.api().columns(i).search(values[i]);
			 """),format.raw/*535.5*/("""}"""),format.raw/*535.6*/("""
			 """),format.raw/*536.5*/("""table.api().draw(); 
		"""),format.raw/*537.3*/("""}"""),format.raw/*537.4*/("""
	 	
		 	  
	"""),format.raw/*540.2*/("""}"""),format.raw/*540.3*/(""");
	
	
"""),format.raw/*543.1*/("""}"""),format.raw/*543.2*/("""
"""),format.raw/*544.1*/("""</script>

 <script type="text/javascript">
	window.onload = InsuranceassignedInteractionDataMR();
</script>
    """))
      }
    }
  }

  def render(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long, String],campaignListInsurance:List[Campaign],serviceTypeList:List[ServiceTypes],dispoList:List[CallDispositionData]): play.twirl.api.HtmlFormat.Appendable = apply(listCity,dealerName,user,allCREHash,campaignListInsurance,serviceTypeList,dispoList)

  def f:((List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[CallDispositionData]) => play.twirl.api.HtmlFormat.Appendable) = (listCity,dealerName,user,allCREHash,campaignListInsurance,serviceTypeList,dispoList) => apply(listCity,dealerName,user,allCREHash,campaignListInsurance,serviceTypeList,dispoList)

  def ref: this.type = this

}


}

/**/
object InsuranceCallLogPageCREManager extends InsuranceCallLogPageCREManager_Scope0.InsuranceCallLogPageCREManager
              /*
                  -- GENERATED --
                  DATE: Thu Jan 04 13:05:49 IST 2018
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/InsuranceCallLogPageCREManager.scala.html
                  HASH: 3aba70ef703bd8f487774a9ab82452376e7bdf0b
                  MATRIX: 895->1|1184->194|1212->197|1270->247|1309->249|1344->258|1391->278|1419->279|1455->289|1578->385|1606->386|1635->388|3379->2106|3416->2127|3455->2128|3549->2194|3592->2210|3605->2214|3636->2224|3666->2227|3679->2231|3710->2241|3773->2273|3819->2291|4931->3377|4988->3418|5027->3419|5091->3455|5134->3471|5156->3484|5185->3492|5215->3495|5237->3508|5277->3526|5351->3569|5422->3613|7378->5541|7418->5564|7458->5565|7491->5570|7535->5586|7550->5591|7589->5608|7620->5611|7635->5616|7674->5633|7724->5652|7808->5708|17803->15672|17836->15677|17946->15758|17976->15759|18010->15765|18889->16615|18919->16616|18953->16622|19014->16654|19044->16655|19082->16665|19156->16710|19186->16711|19222->16719|19352->16821|19382->16822|19416->16828|19445->16829|19475->16831|19504->16832|19534->16834|19585->16856|19615->16857|19650->16864|19731->16917|19760->16918|19790->16920|19822->16924|19851->16925|19881->16927|19928->16946|19957->16947|19991->16953|20182->17115|20212->17116|20250->17126|20566->17413|20596->17414|20641->17430|20748->17508|20778->17509|20812->17515|20841->17516|20870->17517|21003->17621|21033->17622|21069->17630|21173->17706|21202->17707|21244->17721|21273->17722|21311->17732|21405->17797|21435->17798|21466->17801|22312->18618|22342->18619|22377->18626|22438->18658|22468->18659|22508->18670|22582->18715|22612->18716|22649->18725|22741->18788|22771->18789|22806->18796|22835->18797|22866->18800|22895->18801|22926->18804|22977->18826|23007->18827|23043->18835|23125->18889|23154->18890|23185->18893|23217->18897|23246->18898|23277->18901|23325->18921|23354->18922|23389->18929|23607->19118|23637->19119|23677->19130|24002->19426|24032->19427|24077->19443|24184->19521|24214->19522|24249->19529|24278->19530|24307->19531|24382->19578|24411->19579|24447->19587|24536->19647|24566->19648|24597->19651|25462->20487|25492->20488|25526->20494|25587->20526|25617->20527|25655->20537|25725->20578|25755->20579|25791->20587|25881->20649|25911->20650|25945->20656|25974->20657|26004->20659|26033->20660|26064->20663|26115->20685|26145->20686|26180->20693|26262->20747|26291->20748|26322->20751|26354->20755|26383->20756|26415->20760|26463->20780|26492->20781|26526->20787|26734->20966|26764->20967|26802->20977|27120->21266|27150->21267|27195->21283|27302->21361|27332->21362|27366->21368|27395->21369|27424->21370|27500->21418|27529->21419|27571->21433|27680->21513|27710->21514|27741->21517|27826->21573|27856->21574|27891->21581|28131->21792|28161->21793|28205->21809|28244->21819|28274->21820|28314->21832|28518->22007|28548->22008|28589->22020|28650->22052|28680->22053|28725->22069|28795->22110|28825->22111|28868->22125|28965->22193|28995->22194|29036->22206|29066->22207|29102->22215|29131->22216|29168->22225|29219->22247|29249->22248|29291->22261|29410->22352|29439->22353|29472->22357|29502->22358|29540->22368|29622->22422|29651->22423|29695->22439|29724->22440|29757->22444|29787->22445|29824->22454|29875->22477|29904->22478|29961->22507|29990->22508|30039->22529|30300->22761|30330->22762|30364->22768|30425->22800|30455->22801|30490->22808|30613->22903|30642->22904|30676->22910|30728->22934|30757->22935|30801->22951|30830->22952|30868->22962|30897->22963|30927->22965
                  LINES: 27->1|32->1|33->2|33->2|33->2|37->6|38->7|38->7|39->8|42->11|42->11|43->12|70->39|70->39|70->39|71->40|71->40|71->40|71->40|71->40|71->40|71->40|72->41|73->42|103->72|103->72|103->72|104->73|104->73|104->73|104->73|104->73|104->73|104->73|105->74|107->76|149->118|149->118|149->118|150->119|150->119|150->119|150->119|150->119|150->119|150->119|152->121|155->124|339->308|341->310|343->312|343->312|344->313|360->329|360->329|361->330|361->330|361->330|362->331|362->331|362->331|363->332|365->334|365->334|366->335|366->335|367->336|367->336|368->337|368->337|368->337|369->338|370->339|370->339|371->340|371->340|371->340|372->341|373->342|373->342|374->343|377->346|377->346|378->347|387->356|387->356|388->357|390->359|390->359|391->360|391->360|391->360|395->364|395->364|396->365|398->367|398->367|400->369|400->369|405->374|405->374|405->374|406->375|421->390|421->390|422->391|422->391|422->391|423->392|423->392|423->392|424->393|425->394|425->394|426->395|426->395|427->396|427->396|428->397|428->397|428->397|429->398|430->399|430->399|431->400|431->400|431->400|432->401|433->402|433->402|434->403|437->406|437->406|438->407|447->416|447->416|448->417|450->419|450->419|451->420|451->420|451->420|455->424|455->424|459->428|459->428|459->428|460->429|476->445|476->445|477->446|477->446|477->446|478->447|478->447|478->447|479->448|480->449|480->449|481->450|481->450|482->451|482->451|483->452|483->452|483->452|484->453|485->454|485->454|486->455|486->455|486->455|487->456|488->457|488->457|489->458|491->460|491->460|492->461|501->470|501->470|502->471|504->473|504->473|505->474|505->474|505->474|509->478|509->478|516->485|518->487|518->487|519->488|520->489|520->489|521->490|527->496|527->496|529->498|529->498|529->498|531->500|534->503|534->503|535->504|535->504|535->504|536->505|536->505|536->505|537->506|538->507|538->507|539->508|539->508|540->509|540->509|541->510|541->510|541->510|542->511|544->513|544->513|544->513|544->513|545->514|547->516|547->516|549->518|549->518|549->518|549->518|550->519|551->520|551->520|555->524|555->524|557->526|562->531|562->531|563->532|563->532|563->532|564->533|566->535|566->535|567->536|568->537|568->537|571->540|571->540|574->543|574->543|575->544
                  -- GENERATED --
              */
          