
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object main_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String,userName : String,dealerName:String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.68*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>

<html>
<head>
<title>"""),_display_(/*7.9*/title),format.raw/*7.14*/("""</title>
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<meta http-equiv="cache-control" content="max-age=0" >
<meta http-equiv="expires" content="0" >
<meta http-equiv="pragma" content="no-cache" >
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="shortcut icon" type="image/png" href=""""),_display_(/*13.51*/routes/*13.57*/.Assets.at("images/favicon.ico")),format.raw/*13.89*/("""">
<script src=""""),_display_(/*14.15*/routes/*14.21*/.Assets.at("javascripts/jquery-1.7.1.min.js")),format.raw/*14.66*/("""" type="text/javascript"></script>
<link rel="stylesheet" href=""""),_display_(/*15.31*/routes/*15.37*/.Assets.at("css/bootstrap.min.css")),format.raw/*15.72*/("""">
<link rel="stylesheet" href=""""),_display_(/*16.31*/routes/*16.37*/.Assets.at("css/jquery-ui.min.css")),format.raw/*16.72*/("""">
<link rel="stylesheet" href=""""),_display_(/*17.31*/routes/*17.37*/.Assets.at("css/skin-red.css")),format.raw/*17.67*/("""">
<link rel="stylesheet" href=""""),_display_(/*18.31*/routes/*18.37*/.Assets.at("css/AdminLTE.min.css")),format.raw/*18.71*/("""">
<link rel="stylesheet" href=""""),_display_(/*19.31*/routes/*19.37*/.Assets.at("css/dataTables.bootstrap.css")),format.raw/*19.79*/("""">
<link rel="stylesheet" href=""""),_display_(/*20.31*/routes/*20.37*/.Assets.at("css/dataTables.responsive.css")),format.raw/*20.80*/("""">
<link rel="stylesheet" href=""""),_display_(/*21.31*/routes/*21.37*/.Assets.at("css/font-awesome.min.css")),format.raw/*21.75*/("""">
<link rel="stylesheet" href=""""),_display_(/*22.31*/routes/*22.37*/.Assets.at("css/ionicons.min.css")),format.raw/*22.71*/("""">
<link rel="stylesheet" href=""""),_display_(/*23.31*/routes/*23.37*/.Assets.at("css/bootstrap-select.min.css")),format.raw/*23.79*/("""">
<link rel="stylesheet" href=""""),_display_(/*24.31*/routes/*24.37*/.Assets.at("css/Wyz_css.css")),format.raw/*24.66*/("""">
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper"> 
  
  <!-- Main Header -->
  <header class="main-header"> 
    
    <!-- Logo --> 
    <a href="/superAdmin/index" class="logo"> <span class="logo-mini"><b>A</b>DCC</span> <span> <img src=""""),_display_(/*34.108*/routes/*34.114*/.Assets.at("images/dealercustomerconnectlogoblack2.png")),format.raw/*34.170*/("""" width="200" height="44" style="margin-left: -20px;"/></span> </a> 
    
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation"> 
      <!-- Sidebar toggle button--> 
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          		 <li><a href="/superAdmin"><i class="fa fa-user fa-fw"></i>"""),_display_(/*42.73*/dealerName),format.raw/*42.83*/("""</a></li>
             	 <li><a href="#"><i class="fa fa-user fa-fw"></i>"""),_display_(/*43.65*/userName),format.raw/*43.73*/("""</a></li>  
             	  <li><a href="/superAdmin/changepasswordsuperAdmin"><i class="fa fa-sign-out fa-fw"></i>Change Password</a></li>    
                    
                    <li><a href="/logoutUser" onclick="ClearHistory()"><i class="fa fa-sign-out fa-fw"></i>LogOut</a></li>              
            
             
            </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#"><i class="fa fa-sitemap"></i> <span>SUPER-Admin<span class="fa arrow"></span></span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li><a href="/superAdmin/addDealer"><i class="fa fa-check-square-o"></i>Dealer Registration</a></li>
            <li><a href="/superAdmin/dealerList"><i class="fa fa-th-list"></i>Dealer List</a></li>
            <li><a href="/superAdmin/addUser"><i class="fa fa-pencil-square-o"></i>User Registration</a></li>
            <li><a href="/superAdmin/userList"><i class="fa fa-list"></i>User List</a></li>
            <li><a href="/superAdmin/scheduledcallLog"><i class="fa fa-phone"></i>Service Scheduled Calls</a></li>
            <li><a href="/superAdmin/salesScheduledCallLog"><i class="fa fa-phone-square"></i>Sales Scheduled Calls</a></li>
            <li><a href="/liabilities"><i class="fa fa-download"></i>log file display</a></li>
          	
          </ul>
        
        
      </ul>
      <!-- /.sidebar-menu --> 
    </section>
    <!-- /.sidebar --> 
  </aside>
  <div class="content-wrapper">
    <section class="content">
      <div class="row">
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>
              <p>Scheduled Calls</p>
            </div>
            <div class="icon"> <i class="ion-android-call"></i> </div>
            
            <!--<span class="pull-left" data-toggle="modal" data-target=".myModal1">View Details</span>--> 
          </div>
        </div>
        <!-- ./col -->
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53<sup style="font-size: 20px">%</sup></h3>
              <p>% of Call achieved</p>
            </div>
            <div class="icon"> <i class="ion-android-checkbox-outline"></i> </div>
             </div>
        </div>
        <!-- ./col -->
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>44</h3>
              <p>VAS</p>
            </div>
            <div class="icon"> <i class="ion-person-add"></i> </div>
             </div>
        </div>
        <!-- ./col -->
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>
              <p>Pending calls</p>
            </div>
            <div class="icon"> <i class="ion-android-download"></i> </div>
            </div>
        </div>
        <!-- ./col --> 
      </div>
      
      """),_display_(/*125.8*/content),format.raw/*125.15*/(""" 
     """),format.raw/*126.6*/("""</section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs"> ADCC </div>
    <strong>WyzMindz &copy; 2016 <a href="http://www.wyzmindz.com/">Company</a>.</strong> All rights reserved. </footer>
</div>
<script src=""""),_display_(/*132.15*/routes/*132.21*/.Assets.at("javascripts/jquery.js")),format.raw/*132.56*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*133.15*/routes/*133.21*/.Assets.at("javascripts/jquery-ui.min.js")),format.raw/*133.63*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*134.15*/routes/*134.21*/.Assets.at("javascripts/bootstrap.min.js")),format.raw/*134.63*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*135.15*/routes/*135.21*/.Assets.at("javascripts/app.js")),format.raw/*135.53*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*136.15*/routes/*136.21*/.Assets.at("javascripts/jquery.dataTables.min.js")),format.raw/*136.71*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*137.15*/routes/*137.21*/.Assets.at("javascripts/dataTables.bootstrap.min.js")),format.raw/*137.74*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*138.15*/routes/*138.21*/.Assets.at("javascripts/Chart.min.js")),format.raw/*138.59*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*139.15*/routes/*139.21*/.Assets.at("javascripts/fastclick.min.js")),format.raw/*139.63*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*140.15*/routes/*140.21*/.Assets.at("javascripts/Wyz_Scripts.js")),format.raw/*140.61*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*141.15*/routes/*141.21*/.Assets.at("javascripts/jquery.blockUI.js")),format.raw/*141.64*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*142.15*/routes/*142.21*/.Assets.at("javascripts/bootstrap-select.min.js")),format.raw/*142.70*/("""" type="text/javascript"></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDh_ehAgNa_D8qb-DcBg-mSYytYZ0KXDEg&callback=initMap"
  type="text/javascript"></script>
</body>
</html>

"""))
      }
    }
  }

  def render(title:String,userName:String,dealerName:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title,userName,dealerName)(content)

  def f:((String,String,String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title,userName,dealerName) => (content) => apply(title,userName,dealerName)(content)

  def ref: this.type = this

}


}

/**/
object main extends main_Scope0.main
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:31 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/main.scala.html
                  HASH: 8ccbf0c573aae0ec40aeef012eaaa6a7329daf6f
                  MATRIX: 762->1|923->67|953->71|1021->114|1046->119|1438->484|1453->490|1506->522|1551->540|1566->546|1632->591|1725->657|1740->663|1796->698|1857->732|1872->738|1928->773|1989->807|2004->813|2055->843|2116->877|2131->883|2186->917|2247->951|2262->957|2325->999|2386->1033|2401->1039|2465->1082|2526->1116|2541->1122|2600->1160|2661->1194|2676->1200|2731->1234|2792->1268|2807->1274|2870->1316|2931->1350|2946->1356|2996->1385|3311->1672|3327->1678|3405->1734|3919->2221|3950->2231|4052->2306|4081->2314|7423->5629|7452->5636|7488->5644|7766->5894|7782->5900|7839->5935|7918->5986|7934->5992|7998->6034|8077->6085|8093->6091|8157->6133|8236->6184|8252->6190|8306->6222|8385->6273|8401->6279|8473->6329|8552->6380|8568->6386|8643->6439|8722->6490|8738->6496|8798->6534|8877->6585|8893->6591|8957->6633|9036->6684|9052->6690|9114->6730|9193->6781|9209->6787|9274->6830|9352->6880|9368->6886|9439->6935
                  LINES: 27->1|32->1|34->3|38->7|38->7|44->13|44->13|44->13|45->14|45->14|45->14|46->15|46->15|46->15|47->16|47->16|47->16|48->17|48->17|48->17|49->18|49->18|49->18|50->19|50->19|50->19|51->20|51->20|51->20|52->21|52->21|52->21|53->22|53->22|53->22|54->23|54->23|54->23|55->24|55->24|55->24|65->34|65->34|65->34|73->42|73->42|74->43|74->43|156->125|156->125|157->126|163->132|163->132|163->132|164->133|164->133|164->133|165->134|165->134|165->134|166->135|166->135|166->135|167->136|167->136|167->136|168->137|168->137|168->137|169->138|169->138|169->138|170->139|170->139|170->139|171->140|171->140|171->140|172->141|172->141|172->141|173->142|173->142|173->142
                  -- GENERATED --
              */
          