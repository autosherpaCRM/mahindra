
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object rosterOfUnAvailibilityByUserSelectedRange_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class rosterOfUnAvailibilityByUserSelectedRange extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[String,String,List[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(dealerName:String,user:String,usersData:List[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.57*/("""

"""),_display_(/*5.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*5.52*/ {_display_(Seq[Any](format.raw/*5.54*/("""

"""),format.raw/*7.1*/("""<div id="domMessage" style="display:none;"> 
    <h1>We are processing your request.  Please be patient.</h1> 
</div>
<div class="row"> 
<div class="col-lg-12">
        <div class="panel panel-primary">
      <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
            <h4 class="panel-title accordion-toggle"></h4>
            <span style="width:45px;"/>&nbsp;</span> </div>
      <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
          <ul class="nav nav-tabs nav-justified">
                <li class="active"><a data-toggle="tab" href="#home"><b class="tabFonts">CRE Roster</b></a></li>
                <li><a data-toggle="tab" href="#menu1"><b class="tabFonts">Service Advisor Roster</b></a></li>
                <li><a data-toggle="tab" href="#menu2"><b class="tabFonts">Driver Roster</b></a></li>
              </ul>
          <div class="tab-content scrollable-menu">
                <div id="home" class="tab-pane fade in active"> <br>
                
                      <div class="col-md-12"> 
                          <div class=col-md-4>
                <label> Select CRE's </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
                
               
                <select id='selectedCREis'  class="selectpicker" onchange='rosterUnavialbiltyByUser();'>
                    
                    <option value="select">SELECT</option>
                    
                    """),_display_(/*36.22*/for(cres <- usersData) yield /*36.44*/{_display_(Seq[Any](format.raw/*36.45*/("""
                    
                     """),_display_(/*38.23*/if(cres.getRole()==("CRE"))/*38.50*/{_display_(Seq[Any](format.raw/*38.51*/("""
                   
                   """),format.raw/*40.20*/("""<option value=""""),_display_(/*40.36*/cres/*40.40*/.userName),format.raw/*40.49*/("""">"""),_display_(/*40.52*/cres/*40.56*/.userName),format.raw/*40.65*/("""</option>
                    
                    """)))}),format.raw/*42.22*/("""
                    
                    """)))}),format.raw/*44.22*/("""
                    
"""),format.raw/*46.1*/("""<br/>
                    <br/>
                    <br/>
                </select>

            </div> 
                          </div> 
                    
                     <div class="col-md-12">                          
                     <table class="table table-striped table-bordered table-hover" id="creTableRoster">
              <thead>
                
              </thead>
           <tbody>
                  
             </tbody>      
            </table>   
        
                  </div>
				  
				  
				<div class="col-md-12">                          
                     <table class="table table-striped table-bordered table-hover" id="creTableRosters">
					
					</table>   
        
                </div>
				<div class="col-md-12">                          
                     <table class="table table-striped table-bordered table-hover" id="loadRoaster">
             
					</table>   
        
                </div>
     
       </div>
    <div id="menu1" class="tab-pane fade scrollable-menu1"> <br>
                    
                    <div class="col-md-12"> 
                          <div class=col-md-4>
                <label> Select Service Advisor's </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
               <select  id='selectedServiceAdvisoris' class="selectpicker" onchange='rosterUnavialbiltyByServiceAdvisor();'>
                    
                    <option value="select">SELECT</option>
                    
                    """),_display_(/*91.22*/for(cres <- usersData) yield /*91.44*/{_display_(Seq[Any](format.raw/*91.45*/("""
                    
                     """),_display_(/*93.23*/if(cres.getRole()==("Service Advisor"))/*93.62*/{_display_(Seq[Any](format.raw/*93.63*/("""
                     
                   
                   """),format.raw/*96.20*/("""<option value=""""),_display_(/*96.36*/cres/*96.40*/.userName),format.raw/*96.49*/("""">"""),_display_(/*96.52*/cres/*96.56*/.userName),format.raw/*96.65*/("""</option>
                    
                    """)))}),format.raw/*98.22*/("""
                    
                    """)))}),format.raw/*100.22*/("""
                    

                """),format.raw/*103.17*/("""</select>

            </div> 
                          </div> 
                    <br/>
                    <br/>
                    <br/>
              <div class="col-md-12"> 
              
                     <table class="table table-striped table-bordered table-hover" id="creTableRosterByService" >
              <thead>
                
              </thead>
           <tbody>
                
                  
             </tbody>      
            </table> 
                  
                
                  
                  </div>
                       <div class="col-md-12"> 
              
                     <table class="table table-striped table-bordered table-hover" id="creTableRosterByServiceData" >
              <thead>
                
              </thead>
           <tbody>
                
                  
             </tbody>      
            </table> 
                  
                
                  
                  </div>
                    <div class="col-md-12">                          
                     <table class="table table-striped table-bordered table-hover" id="loadRoaster1">
             
					</table>   
        
                </div>
            </div>
                <div id="menu2" class="tab-pane fade scrollable-menu1"> <br>
                    
                    <div class="col-md-12"> 
                          <div class=col-md-4>
                <label> Select Driver Roaster</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
               <select  id='selectedDriver' class="selectpicker" onchange='rosterUnavialbiltyByDriver();'>
                    
                    <option value="select">SELECT</option>
                    
                    """),_display_(/*157.22*/for(cres <- usersData) yield /*157.44*/{_display_(Seq[Any](format.raw/*157.45*/("""
                    
                     """),_display_(/*159.23*/if(cres.getRole()==("Driver"))/*159.53*/{_display_(Seq[Any](format.raw/*159.54*/("""
                     
                   
                   """),format.raw/*162.20*/("""<option value=""""),_display_(/*162.36*/cres/*162.40*/.userName),format.raw/*162.49*/("""">"""),_display_(/*162.52*/cres/*162.56*/.userName),format.raw/*162.65*/("""</option>
                    
                    """)))}),format.raw/*164.22*/("""
                    
                    """)))}),format.raw/*166.22*/("""
                    

                """),format.raw/*169.17*/("""</select>

            </div> 
                          </div> 
                    <br/>
                    <br/>
                    <br/>
              <div class="col-md-12"> 
              
            <table class="table table-striped table-bordered table-hover" id="creTableRosterByDriver" >
              <thead>
                
              </thead>
           <tbody>
                
                  
             </tbody>      
            </table> 
                  
                
                  
                  </div>
				   <div class="col-md-12"> 
              
            <table class="table table-striped table-bordered table-hover" id="creTableRosterByDriverData" >
              <thead>
                
              </thead>
           <tbody>
                
                  
             </tbody>      
            </table> 
                  
                
                  
                  </div>
				   <div class="col-md-12"> 
              
            <table class="table table-striped table-bordered table-hover" id="loadRoaster2" >
              <thead>
                
              </thead>
           <tbody>
                
                  
             </tbody>      
            </table> 
                  
                
                  
                  </div>
            </div>
            
              </div>
        </div>
          </div>
    </div>
      </div>
    </div>
  
""")))}),format.raw/*230.2*/("""
 """))
      }
    }
  }

  def render(dealerName:String,user:String,usersData:List[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(dealerName,user,usersData)

  def f:((String,String,List[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (dealerName,user,usersData) => apply(dealerName,user,usersData)

  def ref: this.type = this

}


}

/**/
object rosterOfUnAvailibilityByUserSelectedRange extends rosterOfUnAvailibilityByUserSelectedRange_Scope0.rosterOfUnAvailibilityByUserSelectedRange
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:38 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/rosterOfUnAvailibilityByUserSelectedRange.scala.html
                  HASH: 70c28dca7c2c3e7d1808eeb54d1874d61567fcd3
                  MATRIX: 838->5|988->60|1018->65|1076->115|1115->117|1145->121|2714->1663|2752->1685|2791->1686|2864->1732|2900->1759|2939->1760|3009->1802|3052->1818|3065->1822|3095->1831|3125->1834|3138->1838|3168->1847|3253->1901|3329->1946|3380->1970|4968->3531|5006->3553|5045->3554|5118->3600|5166->3639|5205->3640|5298->3705|5341->3721|5354->3725|5384->3734|5414->3737|5427->3741|5457->3750|5542->3804|5619->3849|5690->3891|7531->5704|7570->5726|7610->5727|7684->5773|7724->5803|7764->5804|7858->5869|7902->5885|7916->5889|7947->5898|7978->5901|7992->5905|8023->5914|8109->5968|8186->6013|8257->6055|9811->7578
                  LINES: 27->3|32->3|34->5|34->5|34->5|36->7|65->36|65->36|65->36|67->38|67->38|67->38|69->40|69->40|69->40|69->40|69->40|69->40|69->40|71->42|73->44|75->46|120->91|120->91|120->91|122->93|122->93|122->93|125->96|125->96|125->96|125->96|125->96|125->96|125->96|127->98|129->100|132->103|186->157|186->157|186->157|188->159|188->159|188->159|191->162|191->162|191->162|191->162|191->162|191->162|191->162|193->164|195->166|198->169|259->230
                  -- GENERATED --
              */
          