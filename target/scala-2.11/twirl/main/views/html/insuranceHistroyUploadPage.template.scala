
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object insuranceHistroyUploadPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class insuranceHistroyUploadPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[List[Location],String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(locationList:List[Location],dealerCode:String,userName:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.65*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerCode)/*2.56*/ {_display_(Seq[Any](format.raw/*2.58*/("""

"""),_display_(/*4.2*/helper/*4.8*/.form(action = routes.InsuranceHistoryController.uploadExcelDataHistory(), 'enctype -> "multipart/form-data")/*4.117*/ {_display_(Seq[Any](format.raw/*4.119*/("""




"""),format.raw/*9.1*/("""<div class="panel panel-info">
    <div class="panel-heading"><strong>Insurance Master Data Upload</strong></div>
    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-primary">
                    <div class="panel-heading"><b>Select file from your computer</b></div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-lg-3">
                                <label>Select File Format:</label>
                                <select name="dataType" id="upload_form_name" class="form-control">
                                     <option value= "InsuranceHistory">Insurance</option>

                                </select> 
                            </div>
                            
                            <div class="col-lg-3" style="display:none" >  
                                <label>Sheet Name:</label>                                           
                                <input type="text" class="form-control" name="sheetName" id="sheetName" value="Sheet1" required>
                            </div>

                            <div class="col-lg-3">
                                <label>&nbsp;</label>  
                                <input type="file" name="uploadfiles" id="uploadfiles" >  
                                <span id="lblError" style="color: red;"></span>
                            </div>

                        </div>
                        <div class="row"> 
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="cityName">Select City</label>
                                    <select class="form-control" id="city" name="name" onchange="ajaxCallToLoadWorkShopByCity();" required>	
                                        <option value='select'>--Select City--</option>
                                        """),_display_(/*47.42*/for(location_list<-locationList) yield /*47.74*/{_display_(Seq[Any](format.raw/*47.75*/("""
                                        """),format.raw/*48.41*/("""<option value=""""),_display_(/*48.57*/location_list/*48.70*/.getName()),format.raw/*48.80*/("""">"""),_display_(/*48.83*/location_list/*48.96*/.getName()),format.raw/*48.106*/("""</option>
                                        """)))}),format.raw/*49.42*/("""
                                    """),format.raw/*50.37*/("""</select>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="workshopId">Select Location</label>
                                    <select class="form-control" id="workshop" name="id" required>					  
                                        <option value=""></option>
                                    </select>
                                </div>				 
                            </div>
                           
                            
                        </div>
                        <input type="submit" class="btn btn-primary pull-right" name="submit" id="uploadFileSubmit" value="Submit" >
                      
                               
                    </div>
                    
                     

                </div>
                
                
            </div>	
            
           
                            

        </div>
       


    </div>
    <script>
        $(document).ready(function () """),format.raw/*86.39*/("""{"""),format.raw/*86.40*/("""
            """),format.raw/*87.13*/("""$.fn.checkFileType = function (options) """),format.raw/*87.53*/("""{"""),format.raw/*87.54*/("""
                """),format.raw/*88.17*/("""var defaults = """),format.raw/*88.32*/("""{"""),format.raw/*88.33*/("""
                    """),format.raw/*89.21*/("""allowedExtensions: [],
                    success: function () """),format.raw/*90.42*/("""{"""),format.raw/*90.43*/("""}"""),format.raw/*90.44*/(""",
                    error: function () """),format.raw/*91.40*/("""{"""),format.raw/*91.41*/("""}"""),format.raw/*91.42*/("""
                """),format.raw/*92.17*/("""}"""),format.raw/*92.18*/(""";
                options = $.extend(defaults, options);

                return this.each(function () """),format.raw/*95.46*/("""{"""),format.raw/*95.47*/("""

                    """),format.raw/*97.21*/("""$(this).on('change', function () """),format.raw/*97.54*/("""{"""),format.raw/*97.55*/("""
                        """),format.raw/*98.25*/("""var value = $(this).val(),
                                file = value.toLowerCase(),
                                extension = file.substring(file.lastIndexOf('.') + 1);

                        if ($.inArray(extension, options.allowedExtensions) == -1) """),format.raw/*102.84*/("""{"""),format.raw/*102.85*/("""
                            """),format.raw/*103.29*/("""options.error();
                            $(this).focus();
                        """),format.raw/*105.25*/("""}"""),format.raw/*105.26*/(""" """),format.raw/*105.27*/("""else """),format.raw/*105.32*/("""{"""),format.raw/*105.33*/("""
                            """),format.raw/*106.29*/("""options.success();

                        """),format.raw/*108.25*/("""}"""),format.raw/*108.26*/("""

                    """),format.raw/*110.21*/("""}"""),format.raw/*110.22*/(""");

                """),format.raw/*112.17*/("""}"""),format.raw/*112.18*/(""");
            """),format.raw/*113.13*/("""}"""),format.raw/*113.14*/(""";
            $('#uploadfiles').checkFileType("""),format.raw/*114.45*/("""{"""),format.raw/*114.46*/("""
                """),format.raw/*115.17*/("""allowedExtensions: ['xlsx'],
                error: function () """),format.raw/*116.36*/("""{"""),format.raw/*116.37*/("""
                    """),format.raw/*117.21*/("""Lobibox.alert('warning', """),format.raw/*117.46*/("""{"""),format.raw/*117.47*/("""
                        """),format.raw/*118.25*/("""msg: "Please select XLSX file only"
                    """),format.raw/*119.21*/("""}"""),format.raw/*119.22*/(""");
                """),format.raw/*120.17*/("""}"""),format.raw/*120.18*/("""
            """),format.raw/*121.13*/("""}"""),format.raw/*121.14*/(""");
           


            $("#uploadFileSubmit").click(function () """),format.raw/*125.54*/("""{"""),format.raw/*125.55*/("""
                """),format.raw/*126.17*/("""var uploadName = $("#upload_form_name").val();
                var sheetName = $("#sheetName").val();
                var city = $("#city").val();
                var workshop = $("#workshop").val();
                var uploadfiles = $("#uploadfiles").val();

                if (uploadName != 'select' && sheetName != '' && uploadfiles != '' && city != 'select' && workshop != 'select') """),format.raw/*132.129*/("""{"""),format.raw/*132.130*/("""
                    """),format.raw/*133.21*/("""Lobibox.notify('success', """),format.raw/*133.47*/("""{"""),format.raw/*133.48*/("""
                        """),format.raw/*134.25*/("""msg: "Uploaded Successfully"
                    """),format.raw/*135.21*/("""}"""),format.raw/*135.22*/(""");
                    setTimeout(function () """),format.raw/*136.44*/("""{"""),format.raw/*136.45*/("""
                        """),format.raw/*137.25*/("""location.reload();
                    """),format.raw/*138.21*/("""}"""),format.raw/*138.22*/(""", 3000);
                    return true;
                """),format.raw/*140.17*/("""}"""),format.raw/*140.18*/(""" """),format.raw/*140.19*/("""else """),format.raw/*140.24*/("""{"""),format.raw/*140.25*/("""

                    """),format.raw/*142.21*/("""if (uploadName == 'select' || uploadName == '') """),format.raw/*142.69*/("""{"""),format.raw/*142.70*/("""

                        """),format.raw/*144.25*/("""Lobibox.notify('warning', """),format.raw/*144.51*/("""{"""),format.raw/*144.52*/("""
                            """),format.raw/*145.29*/("""msg: "Please select the type of file"
                        """),format.raw/*146.25*/("""}"""),format.raw/*146.26*/(""");
                        return false;

                    """),format.raw/*149.21*/("""}"""),format.raw/*149.22*/("""
                    """),format.raw/*150.21*/("""if (sheetName == '') """),format.raw/*150.42*/("""{"""),format.raw/*150.43*/("""

                        """),format.raw/*152.25*/("""Lobibox.notify('warning', """),format.raw/*152.51*/("""{"""),format.raw/*152.52*/("""
                            """),format.raw/*153.29*/("""msg: "Please enter the sheet name"
                        """),format.raw/*154.25*/("""}"""),format.raw/*154.26*/(""");
                        return false;

                    """),format.raw/*157.21*/("""}"""),format.raw/*157.22*/("""
                    """),format.raw/*158.21*/("""if (uploadfiles == '') """),format.raw/*158.44*/("""{"""),format.raw/*158.45*/("""

                        """),format.raw/*160.25*/("""Lobibox.notify('warning', """),format.raw/*160.51*/("""{"""),format.raw/*160.52*/("""
                            """),format.raw/*161.29*/("""msg: "Please choose the file"
                        """),format.raw/*162.25*/("""}"""),format.raw/*162.26*/(""");
                        return false;

                    """),format.raw/*165.21*/("""}"""),format.raw/*165.22*/("""
                    """),format.raw/*166.21*/("""if (city == 'select' || city == '') """),format.raw/*166.57*/("""{"""),format.raw/*166.58*/("""

                        """),format.raw/*168.25*/("""Lobibox.notify('warning', """),format.raw/*168.51*/("""{"""),format.raw/*168.52*/("""
                            """),format.raw/*169.29*/("""msg: "Please select the city"
                        """),format.raw/*170.25*/("""}"""),format.raw/*170.26*/(""");
                        return false;

                    """),format.raw/*173.21*/("""}"""),format.raw/*173.22*/("""
                    """),format.raw/*174.21*/("""if (workshop == 'select' || workshop == '') """),format.raw/*174.65*/("""{"""),format.raw/*174.66*/("""

                        """),format.raw/*176.25*/("""Lobibox.notify('warning', """),format.raw/*176.51*/("""{"""),format.raw/*176.52*/("""
                            """),format.raw/*177.29*/("""msg: "Please select the workshop"
                        """),format.raw/*178.25*/("""}"""),format.raw/*178.26*/(""");
                        return false;

                    """),format.raw/*181.21*/("""}"""),format.raw/*181.22*/("""
                """),format.raw/*182.17*/("""}"""),format.raw/*182.18*/("""
            """),format.raw/*183.13*/("""}"""),format.raw/*183.14*/(""");

            
        """),format.raw/*186.9*/("""}"""),format.raw/*186.10*/(""");

    </script>


    """)))}),format.raw/*191.6*/("""
    """)))}))
      }
    }
  }

  def render(locationList:List[Location],dealerCode:String,userName:String): play.twirl.api.HtmlFormat.Appendable = apply(locationList,dealerCode,userName)

  def f:((List[Location],String,String) => play.twirl.api.HtmlFormat.Appendable) = (locationList,dealerCode,userName) => apply(locationList,dealerCode,userName)

  def ref: this.type = this

}


}

/**/
object insuranceHistroyUploadPage extends insuranceHistroyUploadPage_Scope0.insuranceHistroyUploadPage
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:29 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/insuranceHistroyUploadPage.scala.html
                  HASH: de343cc32036c4092846c40c387b782afa7b39a8
                  MATRIX: 809->1|967->64|995->67|1057->121|1096->123|1126->128|1139->134|1257->243|1297->245|1333->255|3436->2331|3484->2363|3523->2364|3593->2406|3636->2422|3658->2435|3689->2445|3719->2448|3741->2461|3773->2471|3856->2523|3922->2561|5127->3738|5156->3739|5198->3753|5266->3793|5295->3794|5341->3812|5384->3827|5413->3828|5463->3850|5556->3915|5585->3916|5614->3917|5684->3959|5713->3960|5742->3961|5788->3979|5817->3980|5951->4086|5980->4087|6032->4111|6093->4144|6122->4145|6176->4171|6467->4433|6497->4434|6556->4464|6673->4552|6703->4553|6733->4554|6767->4559|6797->4560|6856->4590|6931->4636|6961->4637|7014->4661|7044->4662|7095->4684|7125->4685|7170->4701|7200->4702|7276->4749|7306->4750|7353->4768|7447->4833|7477->4834|7528->4856|7582->4881|7612->4882|7667->4908|7753->4965|7783->4966|7832->4986|7862->4987|7905->5001|7935->5002|8038->5076|8068->5077|8115->5095|8539->5489|8570->5490|8621->5512|8676->5538|8706->5539|8761->5565|8840->5615|8870->5616|8946->5663|8976->5664|9031->5690|9100->5730|9130->5731|9219->5791|9249->5792|9279->5793|9313->5798|9343->5799|9396->5823|9473->5871|9503->5872|9560->5900|9615->5926|9645->5927|9704->5957|9796->6020|9826->6021|9920->6086|9950->6087|10001->6109|10051->6130|10081->6131|10138->6159|10193->6185|10223->6186|10282->6216|10371->6276|10401->6277|10495->6342|10525->6343|10576->6365|10628->6388|10658->6389|10715->6417|10770->6443|10800->6444|10859->6474|10943->6529|10973->6530|11067->6595|11097->6596|11148->6618|11213->6654|11243->6655|11300->6683|11355->6709|11385->6710|11444->6740|11528->6795|11558->6796|11652->6861|11682->6862|11733->6884|11806->6928|11836->6929|11893->6957|11948->6983|11978->6984|12037->7014|12125->7073|12155->7074|12249->7139|12279->7140|12326->7158|12356->7159|12399->7173|12429->7174|12485->7202|12515->7203|12576->7233
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|35->4|35->4|35->4|40->9|78->47|78->47|78->47|79->48|79->48|79->48|79->48|79->48|79->48|79->48|80->49|81->50|117->86|117->86|118->87|118->87|118->87|119->88|119->88|119->88|120->89|121->90|121->90|121->90|122->91|122->91|122->91|123->92|123->92|126->95|126->95|128->97|128->97|128->97|129->98|133->102|133->102|134->103|136->105|136->105|136->105|136->105|136->105|137->106|139->108|139->108|141->110|141->110|143->112|143->112|144->113|144->113|145->114|145->114|146->115|147->116|147->116|148->117|148->117|148->117|149->118|150->119|150->119|151->120|151->120|152->121|152->121|156->125|156->125|157->126|163->132|163->132|164->133|164->133|164->133|165->134|166->135|166->135|167->136|167->136|168->137|169->138|169->138|171->140|171->140|171->140|171->140|171->140|173->142|173->142|173->142|175->144|175->144|175->144|176->145|177->146|177->146|180->149|180->149|181->150|181->150|181->150|183->152|183->152|183->152|184->153|185->154|185->154|188->157|188->157|189->158|189->158|189->158|191->160|191->160|191->160|192->161|193->162|193->162|196->165|196->165|197->166|197->166|197->166|199->168|199->168|199->168|200->169|201->170|201->170|204->173|204->173|205->174|205->174|205->174|207->176|207->176|207->176|208->177|209->178|209->178|212->181|212->181|213->182|213->182|214->183|214->183|217->186|217->186|222->191
                  -- GENERATED --
              */
          