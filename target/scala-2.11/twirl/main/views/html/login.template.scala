
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object login_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class login extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(url: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.15*/("""

"""),format.raw/*3.1*/("""<link rel="stylesheet" media="screen" href=""""),_display_(/*3.46*/routes/*3.52*/.Assets.at("css/Wyz_Login.css")),format.raw/*3.83*/(""""/> 
<link rel="stylesheet" href=""""),_display_(/*4.31*/routes/*4.37*/.Assets.at("css/bootstrap.min.css")),format.raw/*4.72*/(""""> 
<link rel="stylesheet" href=""""),_display_(/*5.31*/routes/*5.37*/.Assets.at("css/font-awesome.min.css")),format.raw/*5.75*/("""">  
<link rel="stylesheet" href=""""),_display_(/*6.31*/routes/*6.37*/.Assets.at("css/style.css")),format.raw/*6.64*/("""">  
<style>
.footer """),format.raw/*8.9*/("""{"""),format.raw/*8.10*/("""
  """),format.raw/*9.3*/("""position: absolute;
  bottom: 0;
  width: 100%;
  height: 60px;
  background-color: #e6e6e6;
  line-height: 60px;
  box-shadow: 5px -5px 5px #b6b6b6;
  text-align: -webkit-right;
  """),format.raw/*17.3*/("""}"""),format.raw/*17.4*/("""
"""),format.raw/*18.1*/("""</style>


  <div class="row" style="background-color:#EEEEEE; box-shadow: 10px 10px 5px #888888; margin-right: 0px;">
    <div class="logo"> <a href="#"><img src=""""),_display_(/*22.47*/routes/*22.53*/.Assets.at("images/dealercustomerconnectlogoblack2.png")),format.raw/*22.109*/(""""  alt="AutoSherpa" data-toggle="tooltip" title="AutoSherpa"></a> </div>
  </div>

<form action=""""),_display_(/*25.16*/url),format.raw/*25.19*/("""" method="POST">

  <div class="group">
    <input type="text" name="username" title="Enter UserName" placeholder="UserName" required>
    <span class="highlight"></span><span class="bar"></span>
  </div>
  <div class="group">
    <input type="password" name="password" title="Enter Password" placeholder="Password" required>
    <span class="highlight"></span><span class="bar"></span>
  </div>
  <label style="color:red" id="lblError"></label>
  <button type="submit" name="submit" class="button buttonBlue" data-toggle="tooltip" title="Login">Login
  <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
  </button>
  
  <a href="/" style="margin-left: 135px;> <button type="submit" data-toggle="tooltip" title="Back" class="btn btn-success btn-xs"><i class="fa fa-arrow-circle-left"></i>&nbsp;Back</button> </a>

</form>

   <br>
   <footer class="footer">
           Powered by:<img src=""""),_display_(/*46.34*/routes/*46.40*/.Assets.at("images/AUTOBIZ.png")),format.raw/*46.72*/(""""  alt="" data-toggle="tooltip" title="AutoSherpa" style="width: 200px;">
       </footer>
      
   </br>

<script src=""""),_display_(/*51.15*/routes/*51.21*/.Assets.at("javascripts/jquery.js")),format.raw/*51.56*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*52.15*/routes/*52.21*/.Assets.at("javascripts/Wyz_Scripts.js")),format.raw/*52.61*/("""" type="text/javascript"></script>
 <script type="text/javascript">
    
    var query_string = """),format.raw/*55.24*/("""{"""),format.raw/*55.25*/("""}"""),format.raw/*55.26*/(""";
    var query = window.location.search.substring(1);
    
  // alert(query);
    var vars = query.split("&");
    var pair='';
    for (var i=0;i<vars.length;i++) """),format.raw/*61.37*/("""{"""),format.raw/*61.38*/("""
        """),format.raw/*62.9*/("""pair = vars[i].split("=");
        if(pair[i] === "CredentialsException")"""),format.raw/*63.47*/("""{"""),format.raw/*63.48*/("""
            """),format.raw/*64.13*/("""document.getElementById('lblError').innerText="Invalid Username/Password !";
        """),format.raw/*65.9*/("""}"""),format.raw/*65.10*/("""
        """),format.raw/*66.9*/("""}"""),format.raw/*66.10*/(""" 
   
"""),format.raw/*68.1*/("""</script>"""))
      }
    }
  }

  def render(url:String): play.twirl.api.HtmlFormat.Appendable = apply(url)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (url) => apply(url)

  def ref: this.type = this

}


}

/**/
object login extends login_Scope0.login
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:30 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/login.scala.html
                  HASH: 32513d84894cd7bdc13a2f6f9f3a4a9a69edb16e
                  MATRIX: 745->1|853->14|883->18|954->63|968->69|1019->100|1081->136|1095->142|1150->177|1211->212|1225->218|1283->256|1345->292|1359->298|1406->325|1455->348|1483->349|1513->353|1729->542|1757->543|1786->545|1982->714|1997->720|2075->776|2203->877|2227->880|3193->1819|3208->1825|3261->1857|3415->1984|3430->1990|3486->2025|3564->2076|3579->2082|3640->2122|3767->2221|3796->2222|3825->2223|4024->2394|4053->2395|4090->2405|4192->2479|4221->2480|4263->2494|4376->2580|4405->2581|4442->2591|4471->2592|4506->2600
                  LINES: 27->1|32->1|34->3|34->3|34->3|34->3|35->4|35->4|35->4|36->5|36->5|36->5|37->6|37->6|37->6|39->8|39->8|40->9|48->17|48->17|49->18|53->22|53->22|53->22|56->25|56->25|77->46|77->46|77->46|82->51|82->51|82->51|83->52|83->52|83->52|86->55|86->55|86->55|92->61|92->61|93->62|94->63|94->63|95->64|96->65|96->65|97->66|97->66|99->68
                  -- GENERATED --
              */
          