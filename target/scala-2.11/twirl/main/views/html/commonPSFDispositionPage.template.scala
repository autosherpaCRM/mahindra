
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object commonPSFDispositionPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class commonPSFDispositionPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template16[String,Long,Long,Long,Long,String,String,String,Customer,Vehicle,WyzUser,Service,List[SMSTemplate],List[String],List[String],Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(oem:String,typeOfPSF:Long,dispositionHistory:Long,interactionid:Long,uniqueid:Long,dealercode:String,dealerName:String,userName:String,customerData:Customer,vehicleData:Vehicle,userData:WyzUser,latestService:Service,smsTemplates:List[SMSTemplate],complaintOFCust:List[String],statesList:List[String],psfDayPage:Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.319*/("""

"""),_display_(/*3.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*3.65*/ {_display_(Seq[Any](format.raw/*3.67*/("""
	
	"""),format.raw/*5.2*/("""<style>
	.ColorGreen"""),format.raw/*6.13*/("""{"""),format.raw/*6.14*/("""
		"""),format.raw/*7.3*/("""background-color:green;
	"""),format.raw/*8.2*/("""}"""),format.raw/*8.3*/("""
	
	"""),format.raw/*10.2*/(""".ColorRed"""),format.raw/*10.11*/("""{"""),format.raw/*10.12*/("""
		"""),format.raw/*11.3*/("""background-color:red;
	"""),format.raw/*12.2*/("""}"""),format.raw/*12.3*/("""
"""),format.raw/*13.1*/("""</style>

"""),_display_(/*15.2*/helper/*15.8*/.form(action=routes.PSFController.postCommonPSFDispositionPage())/*15.73*/{_display_(Seq[Any](format.raw/*15.74*/("""
"""),format.raw/*16.1*/("""<link rel="stylesheet" href=""""),_display_(/*16.31*/routes/*16.37*/.Assets.at("css/cre.css")),format.raw/*16.62*/("""">
<input type="hidden" id="pFlag" name="pFlag" value=""""),_display_(/*17.54*/{if(customerData.getPreferredAdress()!=null){customerData.getPreferredAdress().getAddressType()}}),format.raw/*17.151*/("""">
<input type="hidden" name="customer_Id" id="customer_Id" value=""""),_display_(/*18.66*/{customerData.getId()}),format.raw/*18.88*/("""">
<input type="hidden" id="wyzUser_Id" name="wyzUser_Id" value=""""),_display_(/*19.64*/{userData.getId()}),format.raw/*19.82*/("""">
<input type="hidden" id="vehical_Id" name="vehicalId" value=""""),_display_(/*20.63*/{vehicleData.getVehicle_id()}),format.raw/*20.92*/("""">
<input type="hidden" name="uniqueidForCallSync" value=""""),_display_(/*21.57*/uniqueid),format.raw/*21.65*/("""">
<input type="hidden" id="workshopIdis" value=""""),_display_(/*22.48*/{if(latestService.getWorkshop()!=null){latestService.getWorkshop().getId()}}),format.raw/*22.124*/("""">
<input type="hidden" id="location_Id" value=""""),_display_(/*23.47*/{userData.getLocation().getCityId()}),format.raw/*23.83*/("""">
<input type="hidden" name="interactionid" value='"""),_display_(/*24.51*/interactionid),format.raw/*24.64*/("""'>
<input type="hidden" name="dispositionHistory" value='"""),_display_(/*25.56*/dispositionHistory),format.raw/*25.74*/("""'>
<input type="hidden" name="typeOfPSF" value='"""),_display_(/*26.47*/typeOfPSF),format.raw/*26.56*/("""'>
<input type="hidden" name="oem" value='"""),_display_(/*27.41*/oem),format.raw/*27.44*/("""'>



<div class="row" style="text-transform:uppercase;">
<!-- 4User div Starts -->
	<div class="col-md-4">
			  <div id="panelDemo10" class="panel panel-info custinfoData">
      <div class="panel-heading" style="text-align:center; padding: 5px;"> <b><i class="fa fa-user"></i>&nbsp;
        
        """),_display_(/*37.10*/{if(customerData.getCustomerName()!=null){
        
        customerData.getCustomerName();
        
        }else{
        
        customerData.getConcatinatedName();							
        
        }}),format.raw/*45.11*/(""" """),format.raw/*45.12*/("""</b><br />
        <b><i class="fa fa-user-circle">
		<input type="text" id="driverIdUpdate" style="border:none;background-color:#1797be;" value=""""),_display_(/*47.96*/{if(customerData.getUserDriver()!=null){
        
        customerData.getUserDriver();
        
        }}),format.raw/*51.11*/("""" readonly></i>
         </b></div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-6 phoneselect">
            <div class="col-xs-3">
             <!--  <input type="button"  class="btn btn-primary" value=""""),_display_(/*57.74*/{customerData.getPreferredPhone().getPhoneNumber()}),format.raw/*57.125*/("""" id="ddl_phone_no" name="ddl_phone_no" style="cursor:default ;width: 150px;margin-left: -137px;font-size: medium;"> -->
			 <select class="btn btn-primary" id="ddl_phone_no" name="ddl_phone_no" style="cursor:default ;width: 150px;margin-left: -137px;font-size: medium;">
									"""),_display_(/*59.11*/for( phoneList <- customerData.getPhones()) yield /*59.54*/{_display_(Seq[Any](format.raw/*59.55*/("""
											
									"""),_display_(/*61.11*/{if(phoneList.isIsPreferredPhone ==true){
									
									if(phoneList.getPhoneNumber()!=null){
									<option value={phoneList.getPhoneNumber()} selected="selected">{phoneList.getPhoneNumber()}</option>
									}}else{ 
									
										if(phoneList.getPhoneNumber()!=null){
										<option value={phoneList.getPhoneNumber()} >{phoneList.getPhoneNumber()}</option>
										}}}),format.raw/*69.14*/("""
									""")))}),format.raw/*70.11*/("""						
								 """),format.raw/*71.10*/("""</select>
            </div>
            <button type="button" class="btn btn-success phoneinfobtn"  data-target="#addBtn" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil-square-o fa-lg"></i></button>
            
<button type="button" style="Display:none" id="DNDbtn" class="btn btn-danger">DND</button>			
            <!--<button type="button" class="btn btn-success test" id="btnDnd" data-target="#DNDConfirm" data-toggle="modal" data-backdrop="static" data-keyboard="false">DND</button>--> 
            
          </div>
          <div class="col-sm-12" style="margin-top: 10px;">
            <div class="col-xs-4">
              <input type="hidden" id="isCallinitaited" name="isCallinitaited" value="NotDailed">
              <input type="hidden" id="pref_number_callini" value=""""),_display_(/*82.69*/{customerData.getPreferredPhone().getPhone_Id()}),format.raw/*82.117*/("""">
              <button type="button" id="callIdBtn" class="btn btn-primary btn-lg" onclick ="callfunctionFromPage('"""),_display_(/*83.116*/{customerData.getPreferredPhone().getPhone_Id()}),format.raw/*83.164*/("""','"""),_display_(/*83.168*/uniqueid),format.raw/*83.176*/("""','"""),_display_(/*83.180*/customerData/*83.192*/.getId()),format.raw/*83.200*/("""');"  style="border-radius: 29px;" ><i class="fa fa-phone-square fa-2x" aria-hidden="true" title="Make Call" ></i></button>
            </div>
            <div class="col-xs-4">
              <button type="button" class="btn btn-primary btn-lg " style="border-radius: 29px;"><i class="fa fa-envelope-square fa-2x" aria-hidden="true"></i></button>
            </div>
            <div class="col-xs-4">
              <button type="button" data-toggle="modal" data-target="#smsPopuId" data-backdrop="static" data-keyboard="false" class="btn btn-primary  btn-lg" style="border-radius: 29px;"><i class="fa fa-comment fa-2x" title="SMS" ></i></button>
            </div>
          </div>
          <div class="col-sm-12" style="margin-top: 6px;">
            <div class="row">
              <div class="col-xs-12" > <span><b><u>Email:</u>&nbsp; </b></span> <span style="font-size:15px" id="ddl_email"> """),_display_(/*94.127*/{if(customerData.getPreferredEmail()!=null){
                customerData.getPreferredEmail().getEmailAddress()
                }}),format.raw/*96.19*/(""" """),format.raw/*96.20*/("""</span> </div>
            </div>
            <div class="row">
              <div class="col-xs-6"> <span><b><u>DOA:</u>&nbsp; </b></span> <span>"""),_display_(/*99.84*/{customerData.getAnniversaryDateStr()}),format.raw/*99.122*/("""</span> </div>
              <div class="col-xs-6" > <span><b><u>DOB:</u>&nbsp; </b></span> <span>"""),_display_(/*100.85*/{customerData.getDobStr()}),format.raw/*100.111*/("""</span> </div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="row">
              <div class="col-xs-12"> <span><b><u>Address:</u>&nbsp; </b></span> <span class="addressBlog" id="prefAdressUpdate"> """),_display_(/*105.132*/{if(customerData.getPreferredAdress()!=null){
                if(customerData.getPreferredAdress().getConcatenatedAdress()!=null){
                customerData.getPreferredAdress().getConcatenatedAdress();
                }else{
                if(customerData.getConcatinatedPrefAddree()!=null){
                customerData.getConcatinatedPrefAddree();
                }    
                }
				}}),format.raw/*113.7*/(""" """),format.raw/*113.8*/("""</span> </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-6"> <b><u>Pref Time:</u>&nbsp;</b><span>"""),_display_(/*118.75*/{customerData.getPreferred_time_start()}),format.raw/*118.115*/(""" """),format.raw/*118.116*/("""To """),_display_(/*118.120*/{customerData.getPreferred_time_end()}),format.raw/*118.158*/("""</span> </div>
              <div class="col-xs-6"> <b><u>Pref Contact:</u>&nbsp;</b> <span>"""),_display_(/*119.79*/{customerData.getMode_of_contact()}),format.raw/*119.114*/("""</span> </div>
            </div>
          </div>
		  <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-12"> <b><u>Pref Day:</u>&nbsp;</b> <span>"""),_display_(/*124.76*/{customerData.getPreferred_day()}),format.raw/*124.109*/("""</span> </div>
              
            </div>
          </div>
          <!--button-->
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-6"> """),_display_(/*131.39*/if(customerData.getSegment()!=null)/*131.74*/{_display_(Seq[Any](format.raw/*131.75*/("""
                
                """),_display_(/*133.18*/for(segmentIterate <- customerData.getSegment()) yield /*133.66*/{_display_(Seq[Any](format.raw/*133.67*/("""
                """),format.raw/*134.17*/("""<button type="button" class="btn btn-info btn-flat btn-block loya" style="cursor:default;"><b style="font-size:15px"> """),_display_(/*134.136*/if(segmentIterate.getName()!="" && segmentIterate.getName()!=null)/*134.202*/{_display_(Seq[Any](format.raw/*134.203*/("""
                """),_display_(/*135.18*/segmentIterate/*135.32*/.getName()),format.raw/*135.42*/("""
                """)))}/*136.18*/else/*136.22*/{_display_(Seq[Any](format.raw/*136.23*/("""
                """),format.raw/*137.17*/("""NA
                """)))}),format.raw/*138.18*/(""" """),format.raw/*138.19*/("""</b></button>
                """)))}),format.raw/*139.18*/("""
                """)))}),format.raw/*140.18*/("""
				"""),format.raw/*141.5*/("""</div>
            </div>
          </div>
        </div>
      </div>
    </div>
			   </div>
		  <div class="col-md-8" style="padding: 0px;">
							<div class="col-sm-4" style="padding: 0px;">
			<!-- START panel-->
					<div id="panelDemo10" class="panel panel-info">
					<div class="panel-heading"><b>VEHICLE&nbsp;INFO</b><img src=""""),_display_(/*152.68*/routes/*152.74*/.Assets.at("images/car.png")),format.raw/*152.102*/("""" class="pull-right" style="width:28px;"></img></div>
						<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
						"""),_display_(/*154.8*/if(vehicleData!=null)/*154.29*/{_display_(Seq[Any](format.raw/*154.30*/("""
		
						
						
						"""),format.raw/*158.7*/("""<span><b>"""),_display_(/*158.17*/{vehicleData.getVehicleRegNo()}),format.raw/*158.48*/("""</b></span>&nbsp;|&nbsp;<span><b>"""),_display_(/*158.82*/{vehicleData.getModel()}),format.raw/*158.106*/("""</b></span></br>
						<span><b>"""),_display_(/*159.17*/{vehicleData.getVariant()}),format.raw/*159.43*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*159.64*/{vehicleData.getColor()}),format.raw/*159.88*/("""</b></span></br>
						<span>Mileage(Kms):&nbsp;<b>"""),_display_(/*160.36*/{latestService.getServiceOdometerReading()}),format.raw/*160.79*/("""</b></span></br>
						<span>Sale&nbsp;Date:&nbsp;<b class="servi" style="background-color: #f05050; color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;">"""),_display_(/*161.164*/{vehicleData.getSaleDateStr()}),format.raw/*161.194*/("""</b></b></span>
										
						
				""")))}),format.raw/*164.6*/("""
						"""),format.raw/*165.7*/("""</div>
									</div>
									<!-- END panel-->
							</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading specialoffer"><b>NEXT&nbsp;SERVICE&nbsp;DUE</b><img src=""""),_display_(/*172.85*/routes/*172.91*/.Assets.at("images/services1.png")),format.raw/*172.125*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow-x:hidden;padding: 5px;">
<span><b class="servi" style="background-color: #f05050; color: #fff;border-radius: 3px;font-size: 13px;border-color: transparent;"></b>&nbsp;|&nbsp;<b></b></span><br>
<span>FORECAST&nbsp;LOGIC: &nbsp;<b>"""),_display_(/*175.38*/{vehicleData.getForecastLogic()}),format.raw/*175.70*/("""</b></span><br>
<span>AVG&nbsp;RUNNING/MONTH(Kms):&nbsp;<b>"""),_display_(/*176.45*/{vehicleData.getAverageRunning()}),format.raw/*176.78*/("""</b> </span></br>
<span>NO&nbsp;SHOW(Months):&nbsp;<b>"""),_display_(/*177.38*/{vehicleData.getRunningBetweenvisits()}),format.raw/*177.77*/("""</b> </span>

</div>
</div>
</div>
							
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>LAST&nbsp;SERVICE&nbsp;STATUS</b><img src=""""),_display_(/*186.75*/routes/*186.81*/.Assets.at("images/car-insurance.png")),format.raw/*186.119*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span><b>"""),_display_(/*188.11*/latestService/*188.24*/.getLastServiceDateStr()),format.raw/*188.48*/("""</b> :&nbsp;|&nbsp;<b>"""),_display_(/*188.71*/latestService/*188.84*/.getLastServiceType()),format.raw/*188.105*/("""</b></span><br>
<span>Workshop:&nbsp;<b>
"""),_display_(/*190.2*/{if(latestService.getWorkshop()!=null){
latestService.getWorkshop().getWorkshopName()
}
}),format.raw/*193.2*/("""

"""),format.raw/*195.1*/("""</b></span><br>
<span>PSF Status :&nbsp;<b>"""),_display_(/*196.29*/{latestService.getLastPSFStatus()}),format.raw/*196.63*/("""</b></span><br>
<span>PSF DATE :&nbsp;<b>
"""),_display_(/*198.2*/{if(typeOfPSF==5){
latestService.getPsf2Str()
}else if(typeOfPSF==4){
latestService.getPsf1Str()
}else if(typeOfPSF==6){
latestService.getPsf3Str()
}else if(typeOfPSF==7){
latestService.getPsf4Str()
}
}),format.raw/*207.2*/("""
"""),format.raw/*208.1*/("""</b></span>
</div>

</div>
<!-- END panel-->
</div>

<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>INSURANCE</b><img src=""""),_display_(/*218.55*/routes/*218.61*/.Assets.at("images/truck.png")),format.raw/*218.91*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">

		

<span>Expires&nbsp;On:&nbsp;<b class="servi" style="background-color: #f05050;color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;"></b></b></span><br>
<span style="font-size:13px">New&nbsp;India&nbsp;Assurance</span><br>
<span style="font-size:13px">Policy&nbsp;No:&nbsp;<b></b></span><br>
<span style="font-size:13px">Premium(Rs.):&nbsp;<b></b></span>


</div>

</div>
<!-- END panel-->
</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>OTHER&nbsp;UPSELL</b><img src=""""),_display_(/*237.63*/routes/*237.69*/.Assets.at("images/warranty-certificate.png")),format.raw/*237.114*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span>EW&nbsp;Due:&nbsp;<b class="servi" style="background-color: #f05050;color: #fff;border-radius: 3px; font-size: 13px;
border-color: transparent;">"""),_display_(/*240.30*/{vehicleData.getExtendedWarentyDue()}),format.raw/*240.67*/("""</b></span><br>
<span>Warranty&nbsp;Due:&nbsp;<b class="servi" style="font-size: 13px;"></b></span><br>
<span style="font-size: 13px;">EXCHANGE:&nbsp;<b>"""),_display_(/*242.51*/{vehicleData.getExchange()}),format.raw/*242.78*/("""</b></span></br>
<span>&nbsp;</span>


</div>

</div>
<!-- END panel-->
</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>COMPLAINTS</b><img src=""""),_display_(/*254.56*/routes/*254.62*/.Assets.at("images/businessman.png")),format.raw/*254.98*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span>OPEN:<b class="servi" style="background-color: #f05050; margin-left: 15px; color: #fff;border-radius: 3px; font-size: 17px;    border-color: transparent;">"""),_display_(/*256.163*/complaintOFCust/*256.178*/.get(0)),format.raw/*256.185*/("""</b></span><br>
<span> CLOSED:<b class="servi" style="background-color: #27c24c; color: #fff;border-radius: 3px;     font-size: 13px;
border-color: transparent;">"""),_display_(/*258.30*/complaintOFCust/*258.45*/.get(1)),format.raw/*258.52*/("""</b></span><br>
<span style="font-size: 14PX;">Last&nbsp;Raised&nbsp;On:&nbsp;<b>"""),_display_(/*259.67*/complaintOFCust/*259.82*/.get(2)),format.raw/*259.89*/("""</b></span><br>
<span style="font-size: 13px;">RESOLVED&nbsp;BY:&nbsp;<b>"""),_display_(/*260.59*/complaintOFCust/*260.74*/.get(3)),format.raw/*260.81*/("""</b>&nbsp;</span>

</div>

</div>
<!-- END panel-->
</div>
			</div>
  <div class="modal fade" id="addBtn" role="dialog">
								<div class="modal-dialog modal-lg">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header" style="padding:1px;text-align:center;color:red;">
											<button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*274.103*/routes/*274.109*/.Assets.at("images/close1.png")),format.raw/*274.140*/(""""></button>
											<h4 class="modal-title"><b>UPDATE CUSTOMER INFORMATION</b></h4>
										</div>
										
								 <div class="modal-body">
					 <div class="row">
						  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <lable><b>Owner:</b></lable>
								  <input type="text" class="form-control" id="customerNameEdit" value=""""),_display_(/*283.81*/customerData/*283.93*/.getCustomerName()),format.raw/*283.111*/("""" name="" readonly>
								  <br/>
								</div>
								
								<div class="col-sm-3">
								  <lable><b>User/Driver:</b></lable>
								  <input type="text" class="form-control textOnlyAccepted" id="driverNameEdit" value=""""),_display_(/*289.96*/customerData/*289.108*/.getUserDriver()),format.raw/*289.124*/("""" name="">
								  <br/>
								</div>
								
								
							  </div>
							  </br>
							  <div class="col-sm-12">
								<div class="col-sm-3">
								  <lable><b>Permanent Address:</b></lable>
								  <textarea class="form-control permanentAddress" rows="2" id="permanentAddress" name="permanentAddress" readonly >"""),_display_(/*299.125*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine1();
	  
	  }}),format.raw/*303.6*/("""_
	  """),_display_(/*304.5*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine2();
	  
	  }}),format.raw/*308.6*/("""_
	  """),_display_(/*309.5*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine3();
	  
	  }}),format.raw/*313.6*/("""</textarea>
								  <br/>
								</div>
								<div class="col-sm-1">
								  <button type="button" class="btn btn-success peditAddressmodal" data-toggle="modal" data-target="#driver_modal1" style="margin-top: 25px;">Add</button>
								</div>
								<div class="col-md-3">
								  <lable><b>Residence Address:</b></lable>
								  <textarea class="form-control" rows="2" id="residenceAddress" name="residenceAddress" readonly>"""),_display_(/*321.107*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine1();
	  
	  }}),format.raw/*325.6*/("""_
	  """),_display_(/*326.5*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine2();
	  
	  }}),format.raw/*330.6*/("""_
	  """),_display_(/*331.5*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine3();
	  
	  }}),format.raw/*335.6*/("""</textarea>
								</div>
								<div class="col-md-1">
								  <button type="button" class="btn btn-success reditAddressmodal"  data-toggle="modal" data-target="#driver_modal2" style="margin-top: 25px;">Add</button>
								</div>
								<div class="col-md-3">
								  <lable><b>Office Address:</b></lable>
								  <textarea class="form-control" rows="2" id="officeAddress" name="officeAddress" readonly >"""),_display_(/*342.102*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine1();
	  
	  }}),format.raw/*346.6*/("""_
	  """),_display_(/*347.5*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine2();
	  
	  }}),format.raw/*351.6*/("""_
	  """),_display_(/*352.5*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine3();
	  
	  }}),format.raw/*356.6*/("""</textarea>
								</div>
								<div class="col-md-1">
								  <button type="button" class="btn btn-success oeditAddressmodal" data-toggle="modal" data-target="#driver_modal3" style="margin-top: 25px;">Add</button>
								</div>
							  </div>
							  <hr>
							  <div class="col-sm-12">
								<p style="color:Red;"><b>PREFERRED COMMUNICATION ADDRESS</b></p>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox1" name="ADDRESS" value="0">
								  Permanent Address</div>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox2" name="ADDRESS" value="2" >
								  Residence Address</div>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox3" name="ADDRESS" value="1" >
								  Office Address</div>
							  </div>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <lable><b>Preferred Email:</b></lable>
								  <select class="form-control" id="email" name="email">
									"""),_display_(/*380.11*/for( emailList <- customerData.getEmails()) yield /*380.54*/{_display_(Seq[Any](format.raw/*380.55*/("""
											
									"""),_display_(/*382.11*/{if(emailList.isIsPreferredEmail() ==true)
									
									<option value={emailList.getEmailAddress()} selected="selected">{emailList.getEmailAddress()}</option>
									else 
										<option value={emailList.getEmailAddress()} >{emailList.getEmailAddress()}</option>
										}),format.raw/*387.12*/("""
									""")))}),format.raw/*388.11*/("""						
								 """),format.raw/*389.10*/("""</select>
								</div>
								<div class="col-xs-1"><br />  
								  <button type="button" class="btn btn-success oeditEmailmodal" data-toggle="modal" data-target="#driver_modal4">Add</button>
								</div>
								<div class="col-sm-3">
								  <lable><b>Date OF Birth:</b></lable>
								  <input type="text" class="datepicMYDropDown form-control" id="dob" value=""""),_display_(/*396.86*/{customerData.getDob()}),format.raw/*396.109*/("""" name="dob" readonly>
								</div>
								<div class="col-md-3">
								  <lable><b>Date of Anniversary</b></lable>
								  <input type="text" class="datepicMYDropDown form-control" id="anniversary_date" value=""""),_display_(/*400.99*/{customerData.getAnniversary_date()}),format.raw/*400.135*/("""" name="anniversary_date" readonly>
								</div>
							  </div>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<h5 style="color:Red;"><b>MANAGE PREFERENCES</b></h5>
								<div class="col-sm-3">
								  <lable><b>Preferred Contact #:</b></lable>
								   <select class="form-control" id="preffered_contact_num" name="preffered_contact_num">
									"""),_display_(/*409.11*/for( phoneList <- customerData.getPhones()) yield /*409.54*/{_display_(Seq[Any](format.raw/*409.55*/("""
											
									"""),_display_(/*411.11*/{if(phoneList.isIsPreferredPhone ==true)
									
									<option value={phoneList.phone_Id.toString()} selected="selected">{phoneList.getPhoneNumber()}</option>
									else 
										<option value={phoneList.phone_Id.toString()} >{phoneList.getPhoneNumber()}</option>
										}),format.raw/*416.12*/("""
									""")))}),format.raw/*417.11*/("""						
								 """),format.raw/*418.10*/("""</select>
								  
								</div>
								<div class="col-sm-1">
								 <input type="button"  class="btn btn-success"  id="tanniversary_edit" data-toggle="modal" data-target="#myModalAddPhone"   style="margin-top: 20px;"  value="Add">
										   </div>
								<div class="col-md-3">
								  <lable><b>Preferred Mode of Contact:</b></lable>
								  <div class="col-sm-12" id="modeOfCon"  style="display:none">
									<select class="form-control" id="preffered_mode_contact" name="preffered_mode_contact" multiple="multiple" >
									  <option value="Email">Email</option>
									  <option value="Phone">Phone</option>
									  <option value="SMS">SMS</option>
									</select>
								  </div>
								  <input type="text" class="form-control" id="tpreffered_mode_contact"  value=""""),_display_(/*433.89*/{customerData.getMode_of_contact()}),format.raw/*433.124*/("""" readonly>
								</div>
								<div class="col-md-1">
								  <input type="button"  class="btn btn-success"  id="tpreffered_mode_contact_edit" style="margin-top: 20px;" value="Add">
								  
								</div>
								<div class="col-md-3">
								  <lable><b>Preferred Day to Contact:</b></lable>
								  <div class="col-sm-12" id="daysWeek" style="display:none">
									<select class="form-control" id="preffered_day_contact" name="preffered_day_contact" multiple="multiple">
									  <option value="Sunday">Sunday</option>
									  <option value="Monday">Monday</option>
									  <option value="Tuesday">Tuesday</option>
									  <option value="Wednesday">Wednesday</option>
									  <option value="Thursday">Thursday</option>
									  <option value="Friday">Friday</option>
									  <option value="Saturday">Saturday</option>
									</select>
								  </div>
								  <input type="text" class="form-control" id="tpreffered_day_contact"  value=""""),_display_(/*452.88*/{customerData.getPreferred_day()}),format.raw/*452.121*/("""" readonly >
								</div>
								<div class="col-md-1">
								  <input type="button"  class="btn btn-success"  id="tpreffered_day_contact_edit" style="margin-top: 20px;" value="Add">
								</div>
							  </div>
							  <br>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <lable><b>Preferred Time Of Contact(Start Time):</b></lable>
								  <input type="text" class="form-control single-input" id="dateStarttime" name="dateStarttime" value=""""),_display_(/*463.112*/{customerData.getPreferred_time_start()}),format.raw/*463.152*/("""" readonly>
								</div>
								<div class="col-md-3">
								  <lable><b>Preferred Time Of Contact(End Time):</b></lable>
								  <input type="text" class="form-control single-input" id="dateEndtime" name="dateEndtime" value=""""),_display_(/*467.108*/{customerData.getPreferred_time_end()}),format.raw/*467.146*/("""" readonly>
								</div>
								<div class="col-sm-6">
								  <lable><b>Comments:(250 characters only)</b></lable>
								  <textarea type="text" class="form-control" id="custEditComment" maxlength="250" rows="2" name="custEditComment" value=""""),_display_(/*471.131*/{customerData.getComments()}),format.raw/*471.159*/("""">"""),_display_(/*471.162*/{customerData.getComments()}),format.raw/*471.190*/("""</textarea>
								</div>
							  </div>
							 
							</div>
				</div>

										<div class="modal-footer">
											<button type="button" id="pn_save" class="btn btn-success" onClick="ajaxCallForAddcustomerinfo();" data-dismiss="modal">Save</button>

										</div>
									</div>

								</div>
							</div>
		  
			  
		  <!-- 4User Div End -->
   


</div>



<!--sashi model-->
<div class="modal fade" id="driver_modal1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*501.94*/routes/*501.100*/.Assets.at("images/close1.png")),format.raw/*501.131*/(""""></button>
		
		<p class="modal-title" style="tex-align:center"><b>Permanent Address</b></p>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control paddr_line1" rows="1"   placeholder="Please enter Company Name/Flat No & Building name." ></textarea>
			  <br/></td>
		 
			<td>Address Line2:</td>
			<td><textarea class="form-control paddr_line2" rows="1"   placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
			  <td>Address Line3:</td>
			<td><textarea class="form-control paddr_line3" rows="1"  placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
		  </tr>
		  <tr>
			<td>City</td>
			<td><input type="text" class="form-control paddr_line4 textOnlyAccepted" rows="1" value=""""),_display_(/*521.94*/{if( customerData.getPermanentAddressData()!=null){ customerData.getPermanentAddressData().getCity()} }),format.raw/*521.197*/(""""  placeholder="Please enter City" /><br/></td>
		  
			<td>State</td>
			<td><input type="text"  class="form-control paddr_line5 textOnlyAccepted" rows="1"  value=""""),_display_(/*524.96*/{if( customerData.getPermanentAddressData()!=null){ customerData.getPermanentAddressData().getState()} }),format.raw/*524.200*/("""" placeholder="Please enter State"/><br/></td>
		  </tr>

		  <tr>
			<td>PinCode</td>			
			<td><input type="text"   maxlength="6" class="form-control paddr_line6 numberOnly" rows="1" value=""""),_display_(/*529.104*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getPincode()}else{0} }),format.raw/*529.217*/(""""  placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			
			<td><input type="text"  class="form-control paddr_line7" rows="1" value="India"  readonly><br/></td>
		  </tr>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary paddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="driver_modal2" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*547.94*/routes/*547.100*/.Assets.at("images/close1.png")),format.raw/*547.131*/(""""></button>
		
		<p class="modal-title" style="tex-align:center"><b>Residence Address</b></p>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control raddr_line1" rows="1"  name="raddressline1" placeholder="Please enter Company Name/Flat No & Building name." ></textarea>
			  <br/></td>
		
			<td>Address Line2:</td>
			<td><textarea class="form-control raddr_line2" rows="1"  name="raddressline2" placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
			  
			 <td>Address Line3:</td>
			<td><textarea class="form-control raddr_line3" rows="1"  name="raddressline3" placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td> 
		  </tr>
		   <tr>
			<td>City</td>
			<td><input type="text" class="form-control raddr_line4 textOnlyAccepted" rows="1"   value=""""),_display_(/*568.96*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getCity()} }),format.raw/*568.199*/("""" placeholder="Please enter City" /><br/></td>
		  
			<td>State</td>
			<td><input type="text"  class="form-control raddr_line5 textOnlyAccepted" rows="1"  value=""""),_display_(/*571.96*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getState()} }),format.raw/*571.200*/("""" placeholder="Please enter State"/><br/></td>
		  </tr>

		  <tr>
			<td>PinCode</td>
			<td><input type="text" maxlength="6" class="form-control raddr_line6 numberOnly" rows="1"  value=""""),_display_(/*576.103*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getPincode()}else{0} }),format.raw/*576.216*/("""" placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control raddr_line7" rows="1" value="India" readonly><br/></td>
		  </tr>
		
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary raddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="driver_modal3" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*594.94*/routes/*594.100*/.Assets.at("images/close1.png")),format.raw/*594.131*/(""""></button>
		<p class="modal-title" style="tex-align:center"><b>Office Address</b></p>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control oaddr_line1" rows="2"  placeholder="Please enter Company Name/Flat No & Building name." name="oaddressline1">"""),_display_(/*601.148*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine1()} }),format.raw/*601.253*/("""</textarea>
			  <br/></td>
		  
			<td>Address Line2:</td>
			<td><textarea class="form-control oaddr_line2" rows="2" placeholder="Please enter Road number/ Road name." name="oaddressline2">"""),_display_(/*605.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine2()} }),format.raw/*605.238*/("""</textarea>
			  <br/></td>
			<td>Address Line3:</td>
			<td><textarea class="form-control oaddr_line3" rows="2" placeholder="Please enter Road number/ Road name." name="oaddressline3">"""),_display_(/*608.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine3()} }),format.raw/*608.238*/("""</textarea>
			  <br/></td>
		  </tr>
		  <tr>
			<td>City</td>
			<td><input type="text" class="form-control oaddr_line7 textOnlyAccepted" id="paddr_line7" rows="1"  name="paddressline3" value=""""),_display_(/*613.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getCity()} }),format.raw/*613.230*/(""""placeholder="Please enter City" /><br/></td>
		  
			<td>State</td>
			<td><input type="text"  class="form-control oaddr_line4 textOnlyAccepted" id="paddr_line4" rows="1"  name="paddressline4" value=""""),_display_(/*616.134*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getState()} }),format.raw/*616.232*/("""" placeholder="Please enter State" /><br/></td>
		  </tr>

		  <tr>
			<td>PinCode</td>
			<td><input type="text" maxlength="6"  class="form-control oaddr_line5 numberOnly" id="paddr_line5" rows="1" value=""""),_display_(/*621.120*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getPincode()}else{0} }),format.raw/*621.233*/("""" name="paddressline5" placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control oaddr_line6" rows="1" value="India" readonly><br/></td>
			
		  </tr>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary oaddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="driver_modal4" role="dialog">
    <div class="modal-dialog modal-xs">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*642.102*/routes/*642.108*/.Assets.at("images/close1.png")),format.raw/*642.139*/(""""></button>
          <h4 class="modal-title">ADD EMAIL</h4>
        </div>
		
		
        <div class="modal-body">
		<div class="row">
        <div class="col-xs-12">
        <div class="col-xs-4">
        <input type="text" name="myEmailNum" class="form-control email"  maxlength="30" min="5" max="30" id="myEmailNum" >
      </div>
        </div>
        </div>
		</div>
        <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal" id="saveEmailCust">SAVE</button>
        </div>
      </div>
      
    </div>
  </div>
<div id="smallModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h4 class="modal-title">Message</h4>
	  </div>
	  <div class="modal-body">
		<p>This feature is not enabled for """),_display_(/*671.39*/userName),format.raw/*671.47*/(""".<br>
		  Contact Administrator.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>
  </div>
</div>
<div id="smallModal1" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h4 class="modal-title">E-Mail</h4>
	  </div>
	  <div class="modal-body">
		<p>This feature is not enabled for """),_display_(/*688.39*/userName),format.raw/*688.47*/(""" """),format.raw/*688.48*/(""".<br>
		  Contact Administrator.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>
  </div>
</div>


<!---bottom fixed popup----->
<div class="modal fade" id="bottonFixedId" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*706.95*/routes/*706.101*/.Assets.at("images/close1.png")),format.raw/*706.132*/(""""></button>
	  <h4 class="modal-title">Call Script</h4>
	</div>
	<div class="modal-body">
	  <p>Hi Good Morning!. </p>
	</div>
	<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
  </div>
  
</div>
</div>

  
  
  <div class="modal fade" id="DNDConfirm" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*728.102*/routes/*728.108*/.Assets.at("images/close1.png")),format.raw/*728.139*/(""""></button>
          <h4 class="modal-title" style="text-align:center;">Do Not Disturb(DND) Tagging</h4>
        </div>
        <div class="modal-body">
          <b>Do you want to tag the customer """),_display_(/*732.47*/customerData/*732.59*/.getCustomerName()),format.raw/*732.77*/("""  """),format.raw/*732.79*/("""as �Do Not Disturb /Call� ?<br> Click �YES� to confirm</b><br><br>
		  
  <div class="radio-inline">
      <label><input type="radio" name="RadioDND" id="RadioDNDYes" value="makeDND">YES</label>
    </div>
    <div class="radio-inline">
      <label><input type="radio" name="RadioDND" id="RadioDNDNo" value="removeDND">NO</label>
    </div>
        </div>
        <div class="modal-footer" style="text-align:center;" >
         
<button type="button" class="btn btn-success" id="submitDndBtn" data-dismiss="modal">submit</button>		  
        </div>
      </div>
      
    </div>
  </div>
  
  
   <div class="modal fade" id="smsPopuId" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="text-align:center;background-color: #106A86;color: #fff;">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*757.102*/routes/*757.108*/.Assets.at("images/close1.png")),format.raw/*757.139*/(""""></button>
          <h4 class="modal-title" ><b>SMS OPTIONS<b></h4>
        </div>
        <div class="">
   <ul class="nav nav-pills nav-stacked col-md-3">
   """),_display_(/*762.5*/for(smsTemplate<-smsTemplates) yield /*762.35*/{_display_(Seq[Any](format.raw/*762.36*/("""
   		"""),format.raw/*763.6*/("""<li><a class="clssTemplType" id="typAnch_"""),_display_(/*763.48*/{smsTemplate.getSmsId()}),format.raw/*763.72*/("""" href="#tab_"""),_display_(/*763.86*/{smsTemplate.getSmsId()}),format.raw/*763.110*/("""" data-toggle="pill">"""),_display_(/*763.132*/{smsTemplate.getSmsType()}),format.raw/*763.158*/("""</a></li>
   	""")))}),format.raw/*764.6*/("""
 
"""),format.raw/*766.1*/("""</ul>
<div class="tab-content col-md-9 smstabclass">
<input type="hidden" id="smsrequestReference" value=""""),_display_(/*768.55*/{vehicleData.getVehicle_id()}),format.raw/*768.84*/("""" />
  """),_display_(/*769.4*/for(smsTemplate<-smsTemplates) yield /*769.34*/{_display_(Seq[Any](format.raw/*769.35*/("""
  	

        """),format.raw/*772.9*/("""<div class="tab-pane """),_display_(/*772.31*/if(smsTemplate.getSmsId()==1)/*772.60*/{_display_(Seq[Any](format.raw/*772.61*/("""active""")))}),format.raw/*772.68*/("""" id="tab_"""),_display_(/*772.79*/{smsTemplate.getSmsId()}),format.raw/*772.103*/("""">
        <input type="hidden" id="typ_"""),_display_(/*773.39*/{smsTemplate.getSmsId()}),format.raw/*773.63*/(""""  />
             <h4><u>"""),_display_(/*774.22*/{smsTemplate.getSmsType()}),format.raw/*774.48*/("""</u></h4>
			 <textarea name="text" rows="5" cols="45" id="txt_"""),_display_(/*775.55*/{smsTemplate.getSmsId()}),format.raw/*775.79*/("""" class="cl_"""),_display_(/*775.92*/{smsTemplate.getSmsId()}),format.raw/*775.116*/("""" disabled="disabled" style="border:none; background-color:#fff;">"""),_display_(/*775.183*/{smsTemplate.getSmsTemplate()}),format.raw/*775.213*/("""</textarea>
<br/>
		<input type="button" name="set_Value" id="ed_"""),_display_(/*777.49*/{smsTemplate.getSmsId()}),format.raw/*777.73*/("""" class="btn btn-info cmmnbtnclass"  value="Edit"  >
     

        </div>
 
 """)))}),format.raw/*782.3*/("""
 """),format.raw/*783.2*/("""</div>	
<!-- tab content -->
        </div>
        <div class="modal-footer">
          <button type="button" id="smsSendbtn" class="btn btn-success" data-dismiss="modal">SEND SMS</button>
        </div>
      </div>
    </div>
  </div>
 
  <!-- Trigger the modal with a button -->

  <!-- Modal -->
  <div class="modal fade" id="myModalForInfo" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color:#1797BE;text-align:center;">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*802.102*/routes/*802.108*/.Assets.at("images/close1.png")),format.raw/*802.139*/(""""></button>
          <h4 class="modal-title heading" style="color:#fff"><b>SERVICE HISTORY -<<VEH No.>> - <<SERVICE DATE>><b> </h4>
        </div>
        <div class="modal-body">
         

   <div class="row" style="border:2px solid #1797BE;margin-right: -8px;margin-left: -6px;">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>CUSTOMER ID</strong></td>
                      <td class="text-primary"> 1311935638 </td>
                    </tr>
					
					<tr>
                      <td><strong>S/ADVISOR</strong></td>
                      <td class="text-primary"> </td>
                    </tr>
					<tr>
                      <td><strong>MILEAGE (Kms)</strong></td>
                      <td class="text-primary">34233</td>
                    </tr>
					  
					 <tr>
                      <td><strong>PART DISC</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                   <tr>
                      <td><strong>PARTS BILL</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                 
					<tr>
                      <td><strong> JOBCARD No.</strong></td>
                      <td class="text-primary">JC13010045</td>
                    </tr>
				
					<tr>
                      <td><strong>TECHNICIAN</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					<tr>
                      <td><strong>Bill No.</strong></td>
                      <td class="text-primary">BR13010474</td>
                    </tr>
					 
					
					<tr>
                      <td><strong>LAB DISC</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					 <tr>
                      <td><strong>LAB BILL</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                   
                  </tbody>
                </table>
                </div>
                 <div class="col-md-4">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                
					<tr>
                      <td><strong>W/SHOP</strong></td>
                      <td class="text-primary">UNIV RD</td>
                    </tr>
					
					
					<tr>
                      <td><strong>S/TYPE</strong></td>
                      <td class="text-primary">FR3</td>
                    </tr>
					<tr>
                      <td><strong>Bill Date</strong></td>
                      <td class="text-primary">10/27/2015</td>
                    </tr>
					
					
					 <tr>
                      <td><strong>DISCOUNT</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                  
					 <tr>
                      <td><strong>BILL AMOUNT</strong></td>
                      <td class="text-primary">656577</td>
                    </tr>
                  </tbody>
                </table>
                </div>
	</div>
			<div class="row" style="border:2px solid #1797BE;margin-top: 5px;margin-right: -8px;margin-left: -6px;">
	   
		
			<h4 style="text-align:center;"><b>REMARKS<b></h4>
    			<div class="col-lg-4" style="text-align: center;">
  			       <div class="form-group">
     			    <label for="">DEFECT DETAILS</label>
     			     <textarea type="" class="form-control" rows="3" id=""  readonly></textarea>
 			     </div>
		 </div> 
  		    <div class="col-lg-4" style="    text-align: center;">
  			<div class="form-group">
     			 <label for="">LABOUR DETAILS</label>
     			 <textarea type="" class="form-control" id=""  rows="3" readonly></textarea>
 			 </div>
		 </div>
       
  		<div class="col-lg-4" style="    text-align: center;">
  			<div class="form-group">
     			 <label for="">JC REMARKS</label>
     			  <textarea type="" class="form-control" id=""  rows="3" readonly></textarea>
 			 </div>
		 </div>
		 
 

</div>

<div class="row" style="border:2px solid #1797BE;    margin-right: -8px;
    margin-left: -6px;    margin-top: 5px;">
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>3rd DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4" >
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
				
				
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>6th DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
				
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>30th DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
</div>
        </div>
      
      </div>
      
    </div>
  </div>
  
  <!-- PSF Disposition Form --> 
  <div class="row">
  <div class="col-lg-12"> 
		
					
					"""),_display_(/*1071.7*/psfDayPage),format.raw/*1071.17*/("""


				
	"""),format.raw/*1075.2*/("""</div>
	</div>
	
<!-----------INTERACTIONH HISTORY--------->
<div class="row">
<div class="col-lg-12">
	<div class="panel panel-primary">
  <div class="panel-heading" align="center"; style="background-color:#1797BE;" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne3">
		<h4 class="panel-title accordion-toggle"></h4>
		<img src=""""),_display_(/*1084.14*/routes/*1084.20*/.Assets.at("images/INTERACTION HISTORY.png")),format.raw/*1084.64*/("""" style="width:17px;"/>&nbsp;<b>INTERACTION HISTORY</b> </div>
  <div id="collapseOne3" class="panel-collapse collapse in">
		<div class="panel-body">
	  <div class="panel with-nav-tabs">
			<div class="panel-heading clearfix">
		  <div>
				<ul class="nav nav-tabs">
			  <li class="expand"><a href="#tab1primary" data-toggle="tab" onclick="callHistoryByVehicle();" >PREVIOUS COMMENTS</a></li>
			  <li class="expand"><a href="#tab2primary" data-toggle="tab" onclick="callHistoryByVehicle();">DISPOSITIONS</a></li>
			  <li class="expand"><a href="#tab3primary" data-toggle="tab" onclick="smsHistoryOfUser();">SMS</a></li>
			  <li class="expand"><a href="#tab4primary" data-toggle="tab">E-Mail</a></li>
			  <li class="expand"><a href="#tab5primary" data-toggle="tab" onclick="complaintsOFVehicle();">COMPLAINTS</a></li>
			</ul>
			  </div>
		</div>
			<div class="panel-body">
		  <div class="tab-content">
				<div class="tab-pane fade in active" id="tab1primary">
			  <div class="row">
					<div class="col-sm-6">
				  <div class="form-group">
						<table class="table table-condensed table-responsive table-user-information" id="previousCommentTable">
						<thead>
							<tr>
						  <th></th>
						  <th></th>						  
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
				<div class="tab-pane fade" id="tab2primary">
			  <div class="row">
					<div class="col-sm-6">
				  <div class="form-group">
						<table class="table" id="dispositionHistory">
					  <thead>
							<tr>
						  <th>Date</th>
						  <th>Time</th>
						  <th>Project</th>
						  <th>Disposition</th>
						  <th>Next FollowUpDate</th>
						  <th>CRE ID</th>
						 
						</tr>
						  </thead>
					  <tbody>
						  
					 
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			<div class="tab-pane fade" id="tab3primary">
			  <div class="row">
				<div class="col-sm-6">
				
						<table class="table table-condensed table-responsive" id="sms_history">
						 <thead>
							<tr>
						  <th>DATE</th>
						  <th>TIME</th>
						  <th>SOURCE AUTO/CRE ID</th>
						  <th>SCENARIO</th>
						  <th>MESSAGE</th>
						  <th>STATUS</th>
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					
				</div>
				
				</div>
			</div>
			<div class="tab-pane fade" id="tab4primary">
			  <div class="row">
					<div class="col-sm-6">
				  <div class="form-group">
						<table class="table table-condensed table-responsive table-user-information">
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			<div class="tab-pane fade" id="tab5primary">
			  <div class="row">
					<div class="col-sm-6">
				  <div class="form-group">
						<table class="table table-condensed table-responsive table-user-information" id="complaint_history">
					  <thead>
							<tr>
						  <th>COMPLAINT No.</th>
						  <th>ISSUE DATE</th>
						  <th>SOURCE</th>
						  <th>FUNCTION</th>
						  <th>CATEGORY</th>
						  <th>STATUS</th>
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			  </div>
		</div>
		  </div>
	</div>
	  </div>
</div>
  </div>
</div>

    <div class="modal fade" id="myModalAddPhone" role="dialog">
    <div class="modal-dialog modal-xs">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*1228.102*/routes/*1228.108*/.Assets.at("images/close1.png")),format.raw/*1228.139*/(""""></button>
          <h4 class="modal-title">ADD PHONE NUMBER</h4>
        </div>
		
		
        <div class="modal-body">
		<div class="row">
        <div class="col-xs-12">
        <div class="col-xs-4">
        <input type="text" name="myPhoneNum" class="form-control numberOnly" maxlength="10" min="10" max="10" id="myPhoneNum" >
      </div>
        </div>
        </div>
		</div>
        <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal" id="savePhoneCust">SAVE</button>
        </div>
      </div>
      
    </div>
  </div>
   <!--------------------------------Side bar popup start------------------------------------------>
  <div id="fixedsocial">
    <div class="facebookflat" data-toggle="modal" data-target="#myModal1" >
	<!-- <img src='"""),_display_(/*1252.18*/routes/*1252.24*/.Assets.at("images/car.png")),format.raw/*1252.52*/("""' style="width: 50px;" /> -->
	<img src='"""),_display_(/*1253.13*/routes/*1253.19*/.Assets.at("images/sports-car.jpg")),format.raw/*1253.54*/("""' style="width: 50px; border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Vehicle" />
	</div>
    <div class="twitterflat" data-toggle="modal" data-target="#myModal2" >
	<img src='"""),_display_(/*1256.13*/routes/*1256.19*/.Assets.at("images/service.jpg")),format.raw/*1256.51*/("""' style="width: 50px;border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Service" onclick="ajaxCallServiceLoadOfCustomer();"/>
	<!-- <img src='"""),_display_(/*1257.18*/routes/*1257.24*/.Assets.at("images/car-insurance.png")),format.raw/*1257.62*/("""' style="width: 50px;" /> -->
	</div> 
     <div class="twitterflat1" data-toggle="modal" data-target="#myModal3" >
	 <img src='"""),_display_(/*1260.14*/routes/*1260.20*/.Assets.at("images/car-insu.jpg")),format.raw/*1260.53*/("""' style="width: 50px; border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Insurance"/> 
	 <!-- <img src='"""),_display_(/*1261.19*/routes/*1261.25*/.Assets.at("images/INSURANCE1.png")),format.raw/*1261.60*/("""' style="width: 50px;" /> --> 
	 </div> 
      
</div>
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*1271.102*/routes/*1271.108*/.Assets.at("images/close1.png")),format.raw/*1271.139*/(""""></button>
          <h4 class="modal-title" style="text-align: center;"><b>VEHICLE INFORMATION<b /></h4>
        </div>
        <div class="modal-body">
		<div class="table table-responsive">
          <table id="example700" class = "table table-bordered">
 
  <thead class="makefixed">
				   <tr>
				  <th style="width: 11%;">Model</th>
			   <th style="width: 11%;">Variant</th>
				<th style="width: 11%;">Color</th>
			   <th style="width: 12%;">Registration&nbsp;No</th>
				<th style="width: 11%;">Engine&nbsp;No</th>
				<th style="width: 11%;">Chassis&nbsp;No</th>
				  <th style="width: 11%;">Sale&nbsp;Date</th>
				<th style="width: 11%;">Fuel&nbsp;Type</th>
				</tr>
			  </thead>
			  <tbody>
			   
			"""),_display_(/*1292.5*/for(post <- customerData.getVehicles()) yield /*1292.44*/ {_display_(Seq[Any](format.raw/*1292.46*/("""
			"""),format.raw/*1293.4*/("""<tr>
			  <td style="text-align:center"><input type="text-primary" id="model" value=""""),_display_(/*1294.82*/post/*1294.86*/.getModel()),format.raw/*1294.97*/(""""  style="border:none;text-align:center" readonly></td>
			  <td style="text-align:center"><input type="text-primary" id="variant" value=""""),_display_(/*1295.84*/post/*1295.88*/.getVariant()),format.raw/*1295.101*/("""" style="border:none;text-align:center" readonly></td>
			  <td style="text-align:center"><input type="text-primary" id="color" value=""""),_display_(/*1296.82*/post/*1296.86*/.getColor()),format.raw/*1296.97*/("""" style="border:none;text-align:center" readonly></td>
			  <td style="text-align:center">
			  <div class="col-sm-12">
			  <div class="col-sm-8">
			  <input type="text-primary" class="vehicalRegNo" id="vehicalRegNo" value=""""),_display_(/*1300.80*/post/*1300.84*/.getVehicleRegNo()),format.raw/*1300.102*/("""" style="border:none;margin-right: 15px;" readonly>
				</p>
				</div>
				<div class="col-sm-2">
				  <input type="button" value="+"  id="editRegistrationno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				 </div>
				 <div class="col-sm-2">
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateRegistrationno" onmouseenter="ajaxAddRegistrationno();" style="display:none;">
				</div>
			  </div></td>
			  <td style="text-align:center"> 
			  <div class="col-sm-12">
			   <div class="col-sm-8">
			  <input type="text-primary" id="engineNo" value=""""),_display_(/*1313.55*/post/*1313.59*/.getEngineNo()),format.raw/*1313.73*/("""" style="border:none;margin-right: 15px;" readonly>
				</p>
				  </div>
			<!-- <div class="col-sm-2">
				  <input type="button" value="+" id="editEngineno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				</div>  <div class="col-sm-2">
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateEngineno" onmouseenter="ajaxAddEngineno();" style="display:none;">
				</div>-->
				</div></td>
			  <td style="text-align:center"><div class="col-sm-12 ">
			   <div class="col-sm-8">
			  <input type="text-primary" id="chassisNo" value=""""),_display_(/*1324.56*/post/*1324.60*/.getChassisNo()),format.raw/*1324.75*/("""" style="border:none;margin-right: 15px;" readonly>
				 </div>
				<!--<div class="col-sm-2">
				  <input type="button" value="+" id="editChassisno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				  </div>  
				  <div class="col-sm-2">  
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateChassisno" onmouseenter="ajaxAddChassisno();" style="display:none;">
				</div>-->
				</div></td>
			
			  <td style="text-align:center"><input type="text-primary" id="saleDate" value=""""),_display_(/*1334.85*/post/*1334.89*/.getSaleDate()),format.raw/*1334.103*/("""" style="border:none;text-align:center" readonly></td>
			  <td style="text-align:center"><input type="text-primary" id="fuelType" value=""""),_display_(/*1335.85*/post/*1335.89*/.getFuelType()),format.raw/*1335.103*/("""" style="border:none;text-align:center" readonly></td>
			</tr>			""")))}),format.raw/*1336.13*/("""
				"""),format.raw/*1337.5*/("""</tbody>
</table>
</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1356.80*/routes/*1356.86*/.Assets.at("images/close1.png")),format.raw/*1356.117*/(""""></button>
          <h4 class="modal-title">SERVICE INFORMATION</h4>
        </div>
        <div class="modal-body"  style="padding: 5px;">
		<div class="table table-responsive">
          <table id="example800" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
 
   <thead class="makefixed">
                <tr>
						<th>Repair Order&nbsp;Date</th>
						<th>Repair Order&nbsp;No.</th>
						<th>Bill&nbsp;Date</th>
						<th>Kilometer</th>
						<th>Model</th>
						<th>Registration No.</th>
						<th>Service Type</th>
						<th>Menu Code Description</th>
						<th>Service Advisor Name</th>
						<th>Customer Name</th>
						<th>Group (Category)</th>
						<th>Part Amount</th>
						<th>Labour Amount</th>
						<th>Total Amount</th> 
					</tr>
                  </thead>
				   <tbody>
                                   
                    </tbody>
			  
</table>

</div>
        </div>
     
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog modal-lg" >
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*1402.102*/routes/*1402.108*/.Assets.at("images/close1.png")),format.raw/*1402.139*/(""""></button>
          <h4 class="modal-title" style="tex-align:center;"><b>INSURANCE INFORMATION</b></h4>
        </div>
        <div class="modal-body">
		<div class="table table-responsive">
          <table id="" class = "table table-bordered">
	 
	   <thead class="makefixed">
					  <tr>
					<th>Policy Issue Date</th>
					<th>Policy Number</th>
					<th>Policy type</th>
					<th>Class</th>
					<th>Add-On</th>
					<th>Due Date</th>
					<th>Gross Premium</th>
					<th>IDV</th>
					<th>OD(%)</th>
					<th>Basic OD</th>
					<th>NCB(%)</th>
					<th>NCB Value</th>
					<th>Discount</th>
					<th>OD Premium</th>
					<th>TP Premium</th>
					<th>PA Premium</th>
					<th>Legal Liability</th>
					<th>Net Liability</th>
					<th>Add-On Premium</th>
					<th>Other Premium</th>
					<th>Net Premium</th>
					<th>Tax</th>
					<!--<th>Gross Premium</th>
					<th>Coverage&nbsp;Period</th>
					<th>Policy&nbsp;Due&nbsp;Date</th>
					<th>Insurance&nbsp;Company</th>
					<th>NCB(%age)</th>
					<th>NCB&nbsp;Amount(Rs.)</th>
					<th>PREMIUM</th>
					<th>Service&nbsp;Tax</th>
					<th>Total&nbsp;Premium</th>
					<th>Last&nbsp;Renewal&nbsp;By</th>-->
					
					</tr>
				  </thead>
				<tbody>
				"""),_display_(/*1447.6*/for(insuranceData <-customerData.getInsurances()) yield /*1447.55*/{_display_(Seq[Any](format.raw/*1447.56*/("""
				 
				  """),format.raw/*1449.7*/("""<tr>
				  <td><a data-toggle="modal" data-target="#otherdetails" ><i class="fa fa-info-circle" data-toggel="tooltip" title="Other Details" style="font-size:30px;color:red;"></i></a></td>
					<td>"""),_display_(/*1451.11*/insuranceData/*1451.24*/.getPolicyDueDateStr()),format.raw/*1451.46*/("""</td>
					<td>"""),_display_(/*1452.11*/insuranceData/*1452.24*/.getPolicyNo()),format.raw/*1452.38*/("""</td>
					<td>"""),_display_(/*1453.11*/insuranceData/*1453.24*/.getInsuranceCompanyName()),format.raw/*1453.50*/("""</td>
					<td>"""),_display_(/*1454.11*/insuranceData/*1454.24*/.getPolicyType()),format.raw/*1454.40*/("""</td>
					<td>"""),_display_(/*1455.11*/insuranceData/*1455.24*/.getClassType()),format.raw/*1455.39*/("""</td>
					<td>"""),_display_(/*1456.11*/insuranceData/*1456.24*/.getAddOn()),format.raw/*1456.35*/("""</td>
					<td>"""),_display_(/*1457.11*/insuranceData/*1457.24*/.getPolicyDueDate()),format.raw/*1457.43*/("""</td>
					<td>"""),_display_(/*1458.11*/insuranceData/*1458.24*/.getGrossPremium()),format.raw/*1458.42*/("""</td>
					<td>"""),_display_(/*1459.11*/insuranceData/*1459.24*/.getIdv()),format.raw/*1459.33*/("""</td>
					<td>"""),_display_(/*1460.11*/insuranceData/*1460.24*/.getOdPercentage()),format.raw/*1460.42*/("""</td>
					 <td>"""),_display_(/*1461.12*/insuranceData/*1461.25*/.getNcBPercentage()),format.raw/*1461.44*/("""</td>
					<td>"""),_display_(/*1462.11*/insuranceData/*1462.24*/.getNcBAmountStr()),format.raw/*1462.42*/("""</td>
					<td>"""),_display_(/*1463.11*/insuranceData/*1463.24*/.getDiscountPercentage()),format.raw/*1463.48*/("""</td>
					<td>"""),_display_(/*1464.11*/insuranceData/*1464.24*/.getODpremium()),format.raw/*1464.39*/("""</td>
					<td>"""),_display_(/*1465.11*/insuranceData/*1465.24*/.getaPPremium()),format.raw/*1465.39*/("""</td>
					<td>"""),_display_(/*1466.11*/insuranceData/*1466.24*/.getpAPremium()),format.raw/*1466.39*/("""</td>
					<td>"""),_display_(/*1467.11*/insuranceData/*1467.24*/.getLegalLiability()),format.raw/*1467.44*/("""</td>
					<td>"""),_display_(/*1468.11*/insuranceData/*1468.24*/.getNetLiability()),format.raw/*1468.42*/("""</td>
					<td>"""),_display_(/*1469.11*/insuranceData/*1469.24*/.getAdd_ON_Premium()),format.raw/*1469.44*/("""</td>
					<td>"""),_display_(/*1470.11*/insuranceData/*1470.24*/.getOtherPremium()),format.raw/*1470.42*/("""</td>
					<td>"""),_display_(/*1471.11*/insuranceData/*1471.24*/.getPremiumAmountStr()),format.raw/*1471.46*/("""</td>
					<td>"""),_display_(/*1472.11*/insuranceData/*1472.24*/.getServiceTax()),format.raw/*1472.40*/("""</td>
				  </tr>
				  """)))}),format.raw/*1474.8*/("""
					"""),format.raw/*1475.6*/("""</tbody> 
	</table>
        </div>
		</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <!--------------------------------Side bar popup end------------------------------------------>
  
  <!--Popup For Driver Allocation-->
<div class="modal fade" id="DriverAllcationPOPUp" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
	   <div class="modal-header" style="text-align:center">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Driver Allocation</h4>																													
        </div>
             <div class="modal-body" style="padding:4px;">
  <div class="panel panel-primary" >
	<div class="panel-heading" style="padding:0px;"> </div>
	<div class="panel-body">
			<div class="col-md-12">
			
			<div class="form-inline">
  <div class="form-group">
<label for="comments"><b> BOOKING DATE :<b></label>
<input type="text" class="form-control datepicMYDropDown" id="changeserviceBookingDate" name="pickupDate" onchange="AssignBtnBkreviewScript();" readonly>
  </div>
  </div>
<br />
</div>

                <div class="col-md-12">
                <table class="table table-responsive">
                  <tbody>
                    <tr>
                      <td><strong> New Booking Date: </strong></td>
                      <td class="text-primary" id="newbookingdate"></td>
                    </tr>
                     <tr>
                      <td><strong> New Driver Name:</strong></td>
                      <td class="text-primary" id="newDriver"></td>
                    </tr>
					<tr>
                      <td><strong>New Time Slot:</strong></td>
                      <td class="text-primary" id="newTimeSlot"></td>
                    </tr>
                  </tbody>
                </table>
                </div>
              
	 <div>
        <input type="hidden" id="tempValue" value="0">
        <input type="hidden" id="tempIncreValue" value="0">
        <input type="hidden" name="time_From" id="startValue" value="0">
        <input type="hidden" name="time_To" id="endValue" value="0">
        <input type="hidden" name="driverId" id="driverValue" value="0">
      </div>
  <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="tableID"  style="cursor: pointer;">
    <thead>  
        
    </thead>
    <tbody> 
           
    </tbody>
  </table>
  <button type="button" id="allcateSubmit" class="btn btn-primary pull-right" data-dismiss="modal">Submit</button>
</div>
</div>
 </div>
       </div>
      
    </div>
  </div>
  
  <!-- Adress pop up is -->
  <div class="modal fade" id="AddNewAddressPopup" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1563.78*/routes/*1563.84*/.Assets.at("images/close1.png")),format.raw/*1563.115*/(""""></button>
        <h4 class="modal-title"><b style="text-align:center; color:Red;">Add New Address</b></h4>
      </div>
      <div class="modal-body">
	 
		  <div class="row" id="AddAddressDiv">
		   <div class="col-md-12">
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>Add Address1<b></label>
				<textarea class="form-control" rows="1" id="AddAddress1MMS" name="address1New" ></textarea>
			  </div>
			</div>
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>Add Address2<b></label>
				<textarea class="form-control" rows="1" id="AddAddress2MMS" name="address2New" ></textarea>
			  </div>
			</div>
			
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>State<b></label>
				<select class="form-control" name="stateNew" id="AddAddrMMSState" onchange="getCityByStateSelection('AddAddrMMSState','cityInputPopup3');">
				  <option value="0">--SELECT--</option>
		"""),_display_(/*1588.4*/for(states <- statesList) yield /*1588.29*/{_display_(Seq[Any](format.raw/*1588.30*/("""
           
          """),format.raw/*1590.11*/("""<option value=""""),_display_(/*1590.27*/states),format.raw/*1590.33*/("""">"""),_display_(/*1590.36*/states),format.raw/*1590.42*/("""</option>
            """)))}),format.raw/*1591.14*/("""
		
				"""),format.raw/*1593.5*/("""</select>
			  </div>
			</div>
			
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>City<b></label>
				<select class="form-control" name="cityNew" placeholder="City" id="cityInputPopup3">	
                                
                              
        
                                    </select>
			  </div>
			</div>
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>PinCode<b></label>
				<input type="text" class="form-control numberOnly" name="pincodeNew" value="0" id="PinCode1MMS" maxlength="6" />
			  </div>
			</div>
			
			</div>
		   
		  </div>
  

<div class="modal-footer">
  <button type="button" class="btn btn-success" id="AddAddrMMSSave" data-dismiss="modal">Save</button>
</div>
</div>
    </div>
  </div>
</div>


<!-- Incomplete Survey -->

<div class="modal fade" id="IncompleteSurvey" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Incomplete Survey</h4>
        </div>
        <div class="modal-body">
         <div class="row">
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpDate">Follow Up Date</label>
					  <input type="text" name="psfAppointmentDate" class="showOnlyFutureDate form-control" id="incompleteDate" readonly>
					</div>
				  </div>
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpTime">Follow Up Time</label>
					  <input type="text" name="psfAppointmentTime" class="single-input form-control" id="" readonly>
					</div>
				  </div>
				   <div class="col-md-6">
					<div class="form-group">
					  <label for="followUpTime">Remarks</label>
					  <textarea type="text" name="remarksList[0]" class="form-control" id=""></textarea>
					</div>
				  </div>
				
        </div>
		
        </div>
<div class="modal-footer">
          <button type="submit" class="btn btn-success" name="" data-dismiss="modal">Submit</button>
        </div>      
      </div>
      
    </div>
  </div>
  
  
""")))}),format.raw/*1672.2*/("""


"""),format.raw/*1675.1*/("""<script src=""""),_display_(/*1675.15*/routes/*1675.21*/.Assets.at("javascripts/disposition.js")),format.raw/*1675.61*/("""" type="text/javascript"></script>


""")))}),format.raw/*1678.2*/("""
"""))
      }
    }
  }

  def render(oem:String,typeOfPSF:Long,dispositionHistory:Long,interactionid:Long,uniqueid:Long,dealercode:String,dealerName:String,userName:String,customerData:Customer,vehicleData:Vehicle,userData:WyzUser,latestService:Service,smsTemplates:List[SMSTemplate],complaintOFCust:List[String],statesList:List[String],psfDayPage:Html): play.twirl.api.HtmlFormat.Appendable = apply(oem,typeOfPSF,dispositionHistory,interactionid,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,userData,latestService,smsTemplates,complaintOFCust,statesList,psfDayPage)

  def f:((String,Long,Long,Long,Long,String,String,String,Customer,Vehicle,WyzUser,Service,List[SMSTemplate],List[String],List[String],Html) => play.twirl.api.HtmlFormat.Appendable) = (oem,typeOfPSF,dispositionHistory,interactionid,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,userData,latestService,smsTemplates,complaintOFCust,statesList,psfDayPage) => apply(oem,typeOfPSF,dispositionHistory,interactionid,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,userData,latestService,smsTemplates,complaintOFCust,statesList,psfDayPage)

  def ref: this.type = this

}


}

/**/
object commonPSFDispositionPage extends commonPSFDispositionPage_Scope0.commonPSFDispositionPage
              /*
                  -- GENERATED --
                  DATE: Wed Jan 10 12:18:14 IST 2018
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/commonPSFDispositionPage.scala.html
                  HASH: c77d50f2ea750bafc97a4d64c6c59c9fc48d7ab3
                  MATRIX: 907->1|1320->318|1348->321|1419->384|1458->386|1488->390|1535->410|1563->411|1592->414|1643->439|1670->440|1701->444|1738->453|1767->454|1797->457|1847->480|1875->481|1903->482|1940->493|1954->499|2028->564|2067->565|2095->566|2152->596|2167->602|2213->627|2296->683|2415->780|2510->848|2553->870|2646->936|2685->954|2777->1019|2827->1048|2913->1107|2942->1115|3019->1165|3117->1241|3193->1290|3250->1326|3330->1379|3364->1392|3449->1450|3488->1468|3564->1517|3594->1526|3664->1569|3688->1572|4018->1875|4234->2070|4263->2071|4437->2218|4565->2325|4838->2571|4911->2622|5220->2904|5279->2947|5318->2948|5368->2971|5779->3361|5821->3372|5865->3388|6718->4214|6788->4262|6934->4380|7004->4428|7036->4432|7066->4440|7098->4444|7120->4456|7150->4464|8075->5361|8226->5491|8255->5492|8429->5639|8489->5677|8616->5776|8665->5802|8940->6048|9362->6449|9391->6450|9608->6639|9671->6679|9702->6680|9735->6684|9796->6722|9917->6815|9975->6850|10187->7034|10243->7067|10463->7259|10508->7294|10548->7295|10611->7330|10676->7378|10716->7379|10762->7396|10910->7515|10987->7581|11028->7582|11074->7600|11098->7614|11130->7624|11168->7642|11182->7646|11222->7647|11268->7664|11320->7684|11350->7685|11413->7716|11463->7734|11496->7739|11863->8078|11879->8084|11930->8112|12100->8255|12131->8276|12171->8277|12223->8301|12261->8311|12314->8342|12376->8376|12423->8400|12484->8433|12532->8459|12581->8480|12627->8504|12707->8556|12772->8599|12981->8779|13034->8809|13104->8848|13139->8855|13428->9116|13444->9122|13501->9156|13867->9494|13921->9526|14009->9586|14064->9619|14147->9674|14208->9713|14466->9943|14482->9949|14543->9987|14711->10127|14734->10140|14780->10164|14831->10187|14854->10200|14898->10221|14967->10263|15077->10352|15107->10354|15179->10398|15235->10432|15305->10475|15528->10677|15557->10678|15805->10898|15821->10904|15873->10934|16631->11664|16647->11670|16715->11715|17025->11997|17084->12034|17266->12188|17315->12215|17590->12462|17606->12468|17664->12504|17985->12796|18011->12811|18041->12818|18232->12981|18257->12996|18286->13003|18396->13085|18421->13100|18450->13107|18552->13181|18577->13196|18606->13203|19058->13626|19075->13632|19129->13663|19537->14043|19559->14055|19600->14073|19859->14304|19882->14316|19921->14332|20280->14662|20426->14787|20459->14793|20605->14918|20638->14924|20784->15049|21252->15488|21398->15613|21431->15619|21577->15744|21610->15750|21756->15875|22201->16291|22341->16410|22374->16416|22514->16535|22547->16541|22687->16660|23739->17684|23799->17727|23839->17728|23890->17751|24197->18036|24240->18047|24285->18063|24689->18439|24735->18462|24982->18681|25041->18717|25462->19110|25522->19153|25562->19154|25613->19177|25920->19462|25963->19473|26008->19489|26838->20291|26896->20326|27900->21302|27956->21335|28504->21854|28567->21894|28830->22128|28891->22166|29171->22417|29222->22445|29254->22448|29305->22476|29978->23121|29995->23127|30049->23158|30878->23959|31004->24062|31198->24228|31325->24332|31547->24525|31683->24638|32335->25262|32352->25268|32406->25299|33304->26169|33430->26272|33623->26437|33750->26541|33968->26730|34104->26843|34753->27464|34770->27470|34824->27501|35172->27820|35300->27925|35521->28117|35649->28222|35865->28409|35993->28514|36218->28710|36338->28807|36569->29009|36690->29107|36926->29314|37062->29427|37788->30124|37805->30130|37859->30161|38823->31097|38853->31105|39443->31667|39473->31675|39503->31676|40017->32162|40034->32168|40088->32199|40689->32771|40706->32777|40760->32808|40988->33008|41010->33020|41050->33038|41081->33040|42062->33992|42079->33998|42133->34029|42323->34192|42370->34222|42410->34223|42444->34229|42514->34271|42560->34295|42602->34309|42649->34333|42700->34355|42749->34381|42795->34396|42826->34399|42961->34506|43012->34535|43047->34543|43094->34573|43134->34574|43176->34588|43226->34610|43265->34639|43305->34640|43344->34647|43383->34658|43430->34682|43499->34723|43545->34747|43600->34774|43648->34800|43740->34864|43786->34888|43827->34901|43874->34925|43970->34992|44023->35022|44117->35088|44163->35112|44273->35191|44303->35193|44989->35850|45006->35856|45060->35887|53592->44391|53625->44401|53663->44410|54048->44766|54065->44772|54132->44816|57833->48487|57851->48493|57906->48524|58732->49321|58749->49327|58800->49355|58871->49397|58888->49403|58946->49438|59176->49639|59193->49645|59248->49677|59441->49841|59458->49847|59519->49885|59677->50014|59694->50020|59750->50053|59905->50179|59922->50185|59980->50220|60403->50613|60421->50619|60476->50650|61227->51373|61284->51412|61326->51414|61359->51418|61474->51504|61489->51508|61523->51519|61691->51658|61706->51662|61743->51675|61908->51811|61923->51815|61957->51826|62213->52053|62228->52057|62270->52075|62917->52693|62932->52697|62969->52711|63595->53308|63610->53312|63648->53327|64222->53872|64237->53876|64275->53890|64443->54029|64458->54033|64496->54047|64596->54114|64630->54119|65217->54677|65234->54683|65289->54714|66630->56025|66648->56031|66703->56062|67946->57277|68013->57326|68054->57327|68096->57340|68323->57538|68347->57551|68392->57573|68437->57589|68461->57602|68498->57616|68543->57632|68567->57645|68616->57671|68661->57687|68685->57700|68724->57716|68769->57732|68793->57745|68831->57760|68876->57776|68900->57789|68934->57800|68979->57816|69003->57829|69045->57848|69090->57864|69114->57877|69155->57895|69200->57911|69224->57924|69256->57933|69301->57949|69325->57962|69366->57980|69412->57997|69436->58010|69478->58029|69523->58045|69547->58058|69588->58076|69633->58092|69657->58105|69704->58129|69749->58145|69773->58158|69811->58173|69856->58189|69880->58202|69918->58217|69963->58233|69987->58246|70025->58261|70070->58277|70094->58290|70137->58310|70182->58326|70206->58339|70247->58357|70292->58373|70316->58386|70359->58406|70404->58422|70428->58435|70469->58453|70514->58469|70538->58482|70583->58504|70628->58520|70652->58533|70691->58549|70748->58574|70783->58580|73908->61676|73925->61682|73980->61713|74986->62691|75029->62716|75070->62717|75123->62740|75168->62756|75197->62762|75229->62765|75258->62771|75314->62794|75351->62802|77602->65021|77634->65024|77677->65038|77694->65044|77757->65084|77827->65122
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|37->6|37->6|38->7|39->8|39->8|41->10|41->10|41->10|42->11|43->12|43->12|44->13|46->15|46->15|46->15|46->15|47->16|47->16|47->16|47->16|48->17|48->17|49->18|49->18|50->19|50->19|51->20|51->20|52->21|52->21|53->22|53->22|54->23|54->23|55->24|55->24|56->25|56->25|57->26|57->26|58->27|58->27|68->37|76->45|76->45|78->47|82->51|88->57|88->57|90->59|90->59|90->59|92->61|100->69|101->70|102->71|113->82|113->82|114->83|114->83|114->83|114->83|114->83|114->83|114->83|125->94|127->96|127->96|130->99|130->99|131->100|131->100|136->105|144->113|144->113|149->118|149->118|149->118|149->118|149->118|150->119|150->119|155->124|155->124|162->131|162->131|162->131|164->133|164->133|164->133|165->134|165->134|165->134|165->134|166->135|166->135|166->135|167->136|167->136|167->136|168->137|169->138|169->138|170->139|171->140|172->141|183->152|183->152|183->152|185->154|185->154|185->154|189->158|189->158|189->158|189->158|189->158|190->159|190->159|190->159|190->159|191->160|191->160|192->161|192->161|195->164|196->165|203->172|203->172|203->172|206->175|206->175|207->176|207->176|208->177|208->177|217->186|217->186|217->186|219->188|219->188|219->188|219->188|219->188|219->188|221->190|224->193|226->195|227->196|227->196|229->198|238->207|239->208|249->218|249->218|249->218|268->237|268->237|268->237|271->240|271->240|273->242|273->242|285->254|285->254|285->254|287->256|287->256|287->256|289->258|289->258|289->258|290->259|290->259|290->259|291->260|291->260|291->260|305->274|305->274|305->274|314->283|314->283|314->283|320->289|320->289|320->289|330->299|334->303|335->304|339->308|340->309|344->313|352->321|356->325|357->326|361->330|362->331|366->335|373->342|377->346|378->347|382->351|383->352|387->356|411->380|411->380|411->380|413->382|418->387|419->388|420->389|427->396|427->396|431->400|431->400|440->409|440->409|440->409|442->411|447->416|448->417|449->418|464->433|464->433|483->452|483->452|494->463|494->463|498->467|498->467|502->471|502->471|502->471|502->471|532->501|532->501|532->501|552->521|552->521|555->524|555->524|560->529|560->529|578->547|578->547|578->547|599->568|599->568|602->571|602->571|607->576|607->576|625->594|625->594|625->594|632->601|632->601|636->605|636->605|639->608|639->608|644->613|644->613|647->616|647->616|652->621|652->621|673->642|673->642|673->642|702->671|702->671|719->688|719->688|719->688|737->706|737->706|737->706|759->728|759->728|759->728|763->732|763->732|763->732|763->732|788->757|788->757|788->757|793->762|793->762|793->762|794->763|794->763|794->763|794->763|794->763|794->763|794->763|795->764|797->766|799->768|799->768|800->769|800->769|800->769|803->772|803->772|803->772|803->772|803->772|803->772|803->772|804->773|804->773|805->774|805->774|806->775|806->775|806->775|806->775|806->775|806->775|808->777|808->777|813->782|814->783|833->802|833->802|833->802|1102->1071|1102->1071|1106->1075|1115->1084|1115->1084|1115->1084|1259->1228|1259->1228|1259->1228|1283->1252|1283->1252|1283->1252|1284->1253|1284->1253|1284->1253|1287->1256|1287->1256|1287->1256|1288->1257|1288->1257|1288->1257|1291->1260|1291->1260|1291->1260|1292->1261|1292->1261|1292->1261|1302->1271|1302->1271|1302->1271|1323->1292|1323->1292|1323->1292|1324->1293|1325->1294|1325->1294|1325->1294|1326->1295|1326->1295|1326->1295|1327->1296|1327->1296|1327->1296|1331->1300|1331->1300|1331->1300|1344->1313|1344->1313|1344->1313|1355->1324|1355->1324|1355->1324|1365->1334|1365->1334|1365->1334|1366->1335|1366->1335|1366->1335|1367->1336|1368->1337|1387->1356|1387->1356|1387->1356|1433->1402|1433->1402|1433->1402|1478->1447|1478->1447|1478->1447|1480->1449|1482->1451|1482->1451|1482->1451|1483->1452|1483->1452|1483->1452|1484->1453|1484->1453|1484->1453|1485->1454|1485->1454|1485->1454|1486->1455|1486->1455|1486->1455|1487->1456|1487->1456|1487->1456|1488->1457|1488->1457|1488->1457|1489->1458|1489->1458|1489->1458|1490->1459|1490->1459|1490->1459|1491->1460|1491->1460|1491->1460|1492->1461|1492->1461|1492->1461|1493->1462|1493->1462|1493->1462|1494->1463|1494->1463|1494->1463|1495->1464|1495->1464|1495->1464|1496->1465|1496->1465|1496->1465|1497->1466|1497->1466|1497->1466|1498->1467|1498->1467|1498->1467|1499->1468|1499->1468|1499->1468|1500->1469|1500->1469|1500->1469|1501->1470|1501->1470|1501->1470|1502->1471|1502->1471|1502->1471|1503->1472|1503->1472|1503->1472|1505->1474|1506->1475|1594->1563|1594->1563|1594->1563|1619->1588|1619->1588|1619->1588|1621->1590|1621->1590|1621->1590|1621->1590|1621->1590|1622->1591|1624->1593|1703->1672|1706->1675|1706->1675|1706->1675|1706->1675|1709->1678
                  -- GENERATED --
              */
          