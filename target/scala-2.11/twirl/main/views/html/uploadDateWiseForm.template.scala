
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object uploadDateWiseForm_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class uploadDateWiseForm extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[HashMap[String, String],String,String,Date,Date,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(alltypeOfReport:HashMap[String,String],uploadId:String,uploadType:String,minDate:Date,maxDate:Date,dealercode:String,dealerName:String,userName:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.154*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*2.56*/ {_display_(Seq[Any](format.raw/*2.58*/("""
	
	"""),format.raw/*4.2*/("""<input type="hidden" id="reportTypeId" value=""""),_display_(/*4.49*/uploadType),format.raw/*4.59*/("""">
	<input type="hidden" id="reportUploadId" value=""""),_display_(/*5.51*/uploadId),format.raw/*5.59*/("""">
	
	<div class="panel panel-info">
    <div class="panel-heading"></div>
    <div class="panel-body"> 
	
	 <div class="row">
	<div class="col-md-12" id="" style="">   
							<div class="col-lg-3">
							<div class="form-group">
                                <label>Select File Type :</label>
                                <select id="dataFileType" class="form-control selectpicker" value=""""),_display_(/*16.101*/uploadType),format.raw/*16.111*/("""" multiple>                                                                                                        
                                   <!--  <option value= "job_card">Job Card Status</option>
                                    <option value= "bill">Work Bills</option>
                                    <option value= "SMR" selected="selected">SMR</option>
                                    <option value= "campaign">Campaign</option>-->
                                    
                                    """),_display_(/*22.38*/for(key <- alltypeOfReport.keySet()) yield /*22.74*/{_display_(Seq[Any](format.raw/*22.75*/(""" 
                                    
                                    """),_display_(/*24.38*/{if(key == uploadType){
									
									<option value={key} selected="selected">{alltypeOfReport.get(key)}</option>
									}
									else {
										<option value={key} >{alltypeOfReport.get(key)}</option>
										}}),format.raw/*30.13*/("""
					  
                    
                    """)))}),format.raw/*33.22*/("""

                                """),format.raw/*35.33*/("""</select> 
                            </div>
                            </div>
                            
                            <div class="col-lg-3">
                            <div class="form-group">
                                <label>From Date :</label>
                                <input type='text' id="fromDate" class="form-control datepicker" value=""""),_display_(/*42.106*/minDate),format.raw/*42.113*/("""" required/>                               
                            </div>
                            </div>
                            
                             <div class="col-lg-3">
                             <div class="form-group">
                                <label>To Date :</label>
                                <input type='text' id="toDate" class="form-control range1datepicker" value=""""),_display_(/*49.110*/maxDate),format.raw/*49.117*/("""" required/>                               
                            </div>
                            </div>
                             
				            <div class="col-lg-3">
				            <div class="form-group">
				            <lable>&nbsp;</lable><br/>
				              <button type='button' class="btn btn-info" onclick="ajaxCallForUploadExcelReport();" >View</button>
				              </div>
				            </div>
            
                            
                            
                            </div>
                            </div>
                            </div>
                            </div>
	
	

<div class="panel panel-info">
    <div class="panel-heading"><strong>Upload Summary Table</strong></div>
    <div class="panel-body"> 
 


	  <div class="col-md-12" id="" style="">
	  <div class="box box-info">
      
            <!-- /.box-header -->
			
            <div class="box-body">
              
                <table id="uploadReport" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Report Name</th>
                <th>Recorded Date</th>
               	<th>City</th>
				<th>Location</th>				
				<th>Total Records</th>
				<th>Records Approved</th>
				<th>Records Rejected</th>
				<th>Uploaded File Name</th>
				<th>Upload by User</th>
				<th>Upload Date</th>
				<th>Upload Time</th>
				<th>Upload ID</th>
				
            </tr>
        </thead>
 
        <tbody>                   
                  </tbody>
                </table>
             
              <!-- /.table-responsive -->
            </div>
           
          </div>
		  </div>

        


    </div>
 </div>


    
	
    """)))}),format.raw/*120.6*/("""
    """),format.raw/*121.5*/("""<script src=""""),_display_(/*121.19*/routes/*121.25*/.Assets.at("javascripts/uploadReportAjax.js")),format.raw/*121.70*/("""" type="text/javascript"></script>    
    
	<script>
		   var reportFromUpload=document.getElementById("reportTypeId").value;

			if(reportFromUpload!="0")"""),format.raw/*126.29*/("""{"""),format.raw/*126.30*/("""
		   
		  """),format.raw/*128.5*/("""// alert("reportFromUpload not  zero: "+reportFromUpload);
		   $("#dataFileType").val(reportFromUpload);
		   window.onload = ajaxCallForUploadExcelReport;		   
			"""),format.raw/*131.4*/("""}"""),format.raw/*131.5*/("""

	"""),format.raw/*133.2*/("""</script>
    
 
	"""))
      }
    }
  }

  def render(alltypeOfReport:HashMap[String, String],uploadId:String,uploadType:String,minDate:Date,maxDate:Date,dealercode:String,dealerName:String,userName:String): play.twirl.api.HtmlFormat.Appendable = apply(alltypeOfReport,uploadId,uploadType,minDate,maxDate,dealercode,dealerName,userName)

  def f:((HashMap[String, String],String,String,Date,Date,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (alltypeOfReport,uploadId,uploadType,minDate,maxDate,dealercode,dealerName,userName) => apply(alltypeOfReport,uploadId,uploadType,minDate,maxDate,dealercode,dealerName,userName)

  def ref: this.type = this

}


}

/**/
object uploadDateWiseForm extends uploadDateWiseForm_Scope0.uploadDateWiseForm
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:38 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/uploadDateWiseForm.scala.html
                  HASH: 8fa4070f084f44cb36535f85aa6f1a51fc828554
                  MATRIX: 833->1|1081->153|1109->156|1171->210|1210->212|1242->218|1315->265|1345->275|1425->329|1453->337|1890->746|1922->756|2488->1295|2540->1331|2579->1332|2684->1410|2936->1641|3021->1695|3085->1731|3498->2116|3527->2123|3977->2545|4006->2552|5835->4350|5869->4356|5911->4370|5927->4376|5994->4421|6184->4582|6214->4583|6255->4596|6451->4764|6480->4765|6513->4770
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|35->4|35->4|36->5|36->5|47->16|47->16|53->22|53->22|53->22|55->24|61->30|64->33|66->35|73->42|73->42|80->49|80->49|151->120|152->121|152->121|152->121|152->121|157->126|157->126|159->128|162->131|162->131|164->133
                  -- GENERATED --
              */
          