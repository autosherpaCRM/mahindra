
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object mainPageSalesExecutive_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class mainPageSalesExecutive extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String,userName : String,dealerName:String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.68*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>
<html>
<head>
<title>"""),_display_(/*6.9*/title),format.raw/*6.14*/("""</title>
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<meta http-equiv="cache-control" content="max-age=0" >
<meta http-equiv="expires" content="0" >
<meta http-equiv="pragma" content="no-cache" >
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<link rel="shortcut icon" type="image/png" href=""""),_display_(/*13.51*/routes/*13.57*/.Assets.at("images/favicon.ico")),format.raw/*13.89*/("""">
<script src=""""),_display_(/*14.15*/routes/*14.21*/.Assets.at("javascripts/jquery-1.7.1.min.js")),format.raw/*14.66*/("""" type="text/javascript"></script>
<link rel="stylesheet" href=""""),_display_(/*15.31*/routes/*15.37*/.Assets.at("css/bootstrap.min.css")),format.raw/*15.72*/("""">
<link rel="stylesheet" href=""""),_display_(/*16.31*/routes/*16.37*/.Assets.at("css/jquery-ui.min.css")),format.raw/*16.72*/("""">
<link rel="stylesheet" href=""""),_display_(/*17.31*/routes/*17.37*/.Assets.at("css/skin-red.css")),format.raw/*17.67*/("""">
<link rel="stylesheet" href=""""),_display_(/*18.31*/routes/*18.37*/.Assets.at("css/AdminLTE.min.css")),format.raw/*18.71*/("""">
<link rel="stylesheet" href=""""),_display_(/*19.31*/routes/*19.37*/.Assets.at("css/dataTables.bootstrap.css")),format.raw/*19.79*/("""">
<link rel="stylesheet" href=""""),_display_(/*20.31*/routes/*20.37*/.Assets.at("css/dataTables.responsive.css")),format.raw/*20.80*/("""">
<link rel="stylesheet" href=""""),_display_(/*21.31*/routes/*21.37*/.Assets.at("css/font-awesome.min.css")),format.raw/*21.75*/("""">
<link rel="stylesheet" href=""""),_display_(/*22.31*/routes/*22.37*/.Assets.at("css/ionicons.min.css")),format.raw/*22.71*/("""">
<link rel="stylesheet" href=""""),_display_(/*23.31*/routes/*23.37*/.Assets.at("css/Wyz_css.css")),format.raw/*23.66*/("""">
<link rel="stylesheet" href=""""),_display_(/*24.31*/routes/*24.37*/.Assets.at("css/bootstrap-clockpicker.min.css")),format.raw/*24.84*/("""">
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper" id="SalesExecutivePage"> 
  
  <!-- Main Header -->
  <header class="main-header"> 
    
    <!-- Logo --> 
    <a href="/SalesExecutive" class="logo"> <span class="logo-mini"><b>A</b>DCC</span> <span> <img src=""""),_display_(/*34.106*/routes/*34.112*/.Assets.at("images/dealercustomerconnectlogoblack2.png")),format.raw/*34.168*/("""" width="200" height="44" style="margin-left: -20px;"/></span> </a> 
    
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation"> 
      <!-- Sidebar toggle button--> 
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a>
      
      <div class="navbar-custom-menu">
         <ul class="nav navbar-nav">
         <li><a href="#"><i class="fa fa-user fa-fw"></i>"""),_display_(/*43.59*/dealerName),format.raw/*43.69*/("""</a></li>
     	 <li><a href="#"><i class="fa fa-user fa-fw"></i>"""),_display_(/*44.57*/userName),format.raw/*44.65*/("""</a></li> 
     	  <li><a href="/SalesExecutive/changepassword"><i class="fa fa-sign-out fa-fw"></i>Change Password</a></li>      
                    
                    <li><a href="/logoutUser" onclick="ClearHistory()"><i class="fa fa-sign-out fa-fw"></i>LogOut</a></li>              
            
             
            </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#"><i class="fa fa-sitemap"></i> <span>Sales Executive<span class="fa arrow"></span></span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
           
             <li><a href="/SalesExecutive/viewSalesCallList"><i class="fa fa-phone"></i>View Sales Call Log</a></li>
            <li><a href="/SalesExecutive/viewSalesassignedCall"><i class="fa fa-phone-square"></i>Sales Scheduled Calls</a></li>
             <li><a href="/SalesExecutive/viewReportOfSE"><i class="fa fa-tachometer"></i>DashBoard</a></li>
          </ul>
        </li>
      </ul>      <!-- /.sidebar-menu --> 
    </section>   <!-- /.sidebar --> 
  </aside>
  <div class="content-wrapper">
    <section class="content">
      <div class="row">
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><div id="salesSchCountSE">..</div></h3>
              <p>Sales Scheduled Calls</p>
            </div>
            <div class="icon"> <i class="ion-android-call"></i> </div>
             
            <!--<span class="pull-left" data-toggle="modal" data-target=".myModal1">View Details</span>--> 
          </div>
        </div> <!-- ./col3 -->
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><div id="salesSEConversionTD">..</div></h3>
              <p>% Conversion Rate</p>
            </div>
            <div class="icon"> <i class="ion-android-checkbox-outline"></i> </div>
            </div>
        </div> <!-- ./col -->
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>44</h3>
              <p>VAS</p>
            </div>
            <div class="icon"> <i class="ion-person-add"></i> </div>
             </div>
        </div> <!-- ./col -->
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><div id="salesSchSEPending">..</div></h3>
              <p>Pending calls</p>
            </div>
            <div class="icon"> <i class="ion-android-download"></i> </div>
             </div>
        </div> <!-- ./col --> 
      </div>  <!-- /.row --> 
      
      """),_display_(/*115.8*/content),format.raw/*115.15*/("""
      """),format.raw/*116.7*/("""</section>
    </div>
     <footer class="main-footer">
        <div class="pull-right hidden-xs"> ADCC </div>
        <strong>WyzMindz &copy; 2016 <a href="http://www.wyzmindz.com/">Company</a>.</strong> All rights reserved.
     </footer>
</div>
<script src=""""),_display_(/*123.15*/routes/*123.21*/.Assets.at("javascripts/jquery.js")),format.raw/*123.56*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*124.15*/routes/*124.21*/.Assets.at("javascripts/jquery-ui.min.js")),format.raw/*124.63*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*125.15*/routes/*125.21*/.Assets.at("javascripts/bootstrap.min.js")),format.raw/*125.63*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*126.15*/routes/*126.21*/.Assets.at("javascripts/app.js")),format.raw/*126.53*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*127.15*/routes/*127.21*/.Assets.at("javascripts/jquery.dataTables.min.js")),format.raw/*127.71*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*128.15*/routes/*128.21*/.Assets.at("javascripts/dataTables.bootstrap.min.js")),format.raw/*128.74*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*129.15*/routes/*129.21*/.Assets.at("javascripts/Chart.min.js")),format.raw/*129.59*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*130.15*/routes/*130.21*/.Assets.at("javascripts/fastclick.min.js")),format.raw/*130.63*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*131.15*/routes/*131.21*/.Assets.at("javascripts/Wyz_Scripts.js")),format.raw/*131.61*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*132.15*/routes/*132.21*/.Assets.at("javascripts/jquery.blockUI.js")),format.raw/*132.64*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*133.15*/routes/*133.21*/.Assets.at("javascripts/bootstrap-clockpicker.min.js")),format.raw/*133.75*/("""" type="text/javascript"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDh_ehAgNa_D8qb-DcBg-mSYytYZ0KXDEg&callback=initMap"
  type="text/javascript"></script>
</body>
</html>
"""))
      }
    }
  }

  def render(title:String,userName:String,dealerName:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title,userName,dealerName)(content)

  def f:((String,String,String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title,userName,dealerName) => (content) => apply(title,userName,dealerName)(content)

  def ref: this.type = this

}


}

/**/
object mainPageSalesExecutive extends mainPageSalesExecutive_Scope0.mainPageSalesExecutive
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:32 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/mainPageSalesExecutive.scala.html
                  HASH: 77297df1a18366591189014974738c61679ed02e
                  MATRIX: 798->1|959->67|989->71|1055->112|1080->117|1474->484|1489->490|1542->522|1587->540|1602->546|1668->591|1761->657|1776->663|1832->698|1893->732|1908->738|1964->773|2025->807|2040->813|2091->843|2152->877|2167->883|2222->917|2283->951|2298->957|2361->999|2422->1033|2437->1039|2501->1082|2562->1116|2577->1122|2636->1160|2697->1194|2712->1200|2767->1234|2828->1268|2843->1274|2893->1303|2954->1337|2969->1343|3037->1390|3374->1699|3390->1705|3468->1761|3977->2243|4008->2253|4102->2320|4131->2328|7109->5279|7138->5286|7174->5294|7471->5563|7487->5569|7544->5604|7623->5655|7639->5661|7703->5703|7782->5754|7798->5760|7862->5802|7941->5853|7957->5859|8011->5891|8090->5942|8106->5948|8178->5998|8257->6049|8273->6055|8348->6108|8427->6159|8443->6165|8503->6203|8582->6254|8598->6260|8662->6302|8741->6353|8757->6359|8819->6399|8898->6450|8914->6456|8979->6499|9057->6549|9073->6555|9149->6609
                  LINES: 27->1|32->1|34->3|37->6|37->6|44->13|44->13|44->13|45->14|45->14|45->14|46->15|46->15|46->15|47->16|47->16|47->16|48->17|48->17|48->17|49->18|49->18|49->18|50->19|50->19|50->19|51->20|51->20|51->20|52->21|52->21|52->21|53->22|53->22|53->22|54->23|54->23|54->23|55->24|55->24|55->24|65->34|65->34|65->34|74->43|74->43|75->44|75->44|146->115|146->115|147->116|154->123|154->123|154->123|155->124|155->124|155->124|156->125|156->125|156->125|157->126|157->126|157->126|158->127|158->127|158->127|159->128|159->128|159->128|160->129|160->129|160->129|161->130|161->130|161->130|162->131|162->131|162->131|163->132|163->132|163->132|164->133|164->133|164->133
                  -- GENERATED --
              */
          