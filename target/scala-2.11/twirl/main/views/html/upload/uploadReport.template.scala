
package views.html.upload

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object uploadReport_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class uploadReport extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[List[String],String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(uploadTypes:List[String],user:String,dealer:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.54*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealer)/*2.48*/ {_display_(Seq[Any](format.raw/*2.50*/("""

"""),format.raw/*4.1*/("""<div class="panel panel-info">
  <div class="panel-heading"><b>Upload Reports</b></div>
  <div class="panel-body">

    <div class="col-md-3">
        <select name="uploadType" id="uploadType" class="form-control m-b">
                              """),_display_(/*10.32*/for(uploadType <- uploadTypes) yield /*10.62*/ {_display_(Seq[Any](format.raw/*10.64*/("""
                    """),format.raw/*11.21*/("""<option value=""""),_display_(/*11.37*/uploadType),format.raw/*11.47*/("""">"""),_display_(/*11.50*/uploadType),format.raw/*11.60*/("""</option>
                """)))}),format.raw/*12.18*/("""
                               """),format.raw/*13.32*/("""</select>
    </div>
           <br>
                              
           <div class="row">
  <div class="col-lg-12">
    
    <br>
    <div class="panel panel-info">
      <div class="panel-heading"><b>Search</b></div>
      <div class="panel-body">
        <div class="table table-responsive">
      <table class="table table-striped table-bordered table-hover" id="uploadlist">
                                    <thead>
                                 <tr> 
                                    <th>File Name</th>
                                    <th>filePath</th>
                                    <th>Uploaded By</th>
                                    <th>Status</th>                                   
                                    <th>Uploaded Date</th>
                                    <th>Number Accepted</th>                                    
                                    <th>Processing Errors</th>
                                    <th>Number Discarded</th>
                                    <th>Error</th>
                                   	<th>isError</th>
                                    <th>Processing Start</th>
                                    <th>Processing End</th>
                                 </tr>
                              </thead>
                                <tbody>
                   </tbody>
           </table>
        </div>
      </div>
    </div>
  </div>
</div>
    
</div>
 </div>

""")))}),format.raw/*54.2*/("""
"""),format.raw/*55.1*/("""<script type="text/javascript">
  $(document).ready(function() """),format.raw/*56.32*/("""{"""),format.raw/*56.33*/("""
  	"""),format.raw/*57.4*/("""var selectedType = $('#uploadType :selected').text();
  
    var table = $('#uploadlist').dataTable( """),format.raw/*59.45*/("""{"""),format.raw/*59.46*/("""
        """),format.raw/*60.9*/(""""processing": false,
        "serverSide": false,
        "scrollY": 300,
        "paging": true,
        "searching":false,
        "ordering":false,
        "ajax": "/uploadList/"+selectedType,

        "columnDefs": [
                       """),format.raw/*69.24*/("""{"""),format.raw/*69.25*/("""
                           """),format.raw/*70.28*/(""""render": function ( data, type, row ) """),format.raw/*70.67*/("""{"""),format.raw/*70.68*/("""
                                """),format.raw/*71.33*/("""if(row[9] == true)
                               		return '<a href="'+data+'">error link</a>';
                               		else
                                   		return "";
                           """),format.raw/*75.28*/("""}"""),format.raw/*75.29*/(""",
                           "targets": 8
                       """),format.raw/*77.24*/("""}"""),format.raw/*77.25*/(""","""),format.raw/*77.26*/("""{"""),format.raw/*77.27*/("""
                           """),format.raw/*78.28*/(""""render": function ( data, type, row ) """),format.raw/*78.67*/("""{"""),format.raw/*78.68*/("""
                              		"""),format.raw/*79.33*/("""return '<a href="'+row[1]+'">'+data+'</a>';
                          """),format.raw/*80.27*/("""}"""),format.raw/*80.28*/(""",
                          "targets": 0
                          
                           """),format.raw/*83.28*/("""}"""),format.raw/*83.29*/(""",
                       """),format.raw/*84.24*/("""{"""),format.raw/*84.25*/(""" """),format.raw/*84.26*/(""""visible": false,  "targets": [ 1,9 ] """),format.raw/*84.64*/("""}"""),format.raw/*84.65*/("""
                   """),format.raw/*85.20*/("""]
        
    """),format.raw/*87.5*/("""}"""),format.raw/*87.6*/(""" """),format.raw/*87.7*/(""");
    
    $('#uploadType').on('change' ,function(e)"""),format.raw/*89.46*/("""{"""),format.raw/*89.47*/("""
		
		"""),format.raw/*91.3*/("""var selectedType = $('#uploadType :selected').text();
		var urlstr = "/uploadList/"+selectedType;
		//table.ajax.url(urlstr).load();
    	tableApi = table.api();
    	tableApi.ajax.url(urlstr).load();
    """),format.raw/*96.5*/("""}"""),format.raw/*96.6*/(""" """),format.raw/*96.7*/(""");
		
"""),format.raw/*98.1*/("""}"""),format.raw/*98.2*/(""");
    

</script>	
"""))
      }
    }
  }

  def render(uploadTypes:List[String],user:String,dealer:String): play.twirl.api.HtmlFormat.Appendable = apply(uploadTypes,user,dealer)

  def f:((List[String],String,String) => play.twirl.api.HtmlFormat.Appendable) = (uploadTypes,user,dealer) => apply(uploadTypes,user,dealer)

  def ref: this.type = this

}


}

/**/
object uploadReport extends uploadReport_Scope0.uploadReport
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:43 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/upload/uploadReport.scala.html
                  HASH: ba745ab56b91635715f26e8f9608457814e4aeff
                  MATRIX: 786->1|933->53|961->56|1015->102|1054->104|1084->108|1367->364|1413->394|1453->396|1503->418|1546->434|1577->444|1607->447|1638->457|1697->485|1758->518|3301->2031|3330->2033|3422->2097|3451->2098|3483->2103|3614->2206|3643->2207|3680->2217|3961->2470|3990->2471|4047->2500|4114->2539|4143->2540|4205->2574|4446->2787|4475->2788|4570->2855|4599->2856|4628->2857|4657->2858|4714->2887|4781->2926|4810->2927|4872->2961|4971->3032|5000->3033|5126->3131|5155->3132|5209->3158|5238->3159|5267->3160|5333->3198|5362->3199|5411->3220|5455->3237|5483->3238|5511->3239|5594->3294|5623->3295|5658->3303|5895->3513|5923->3514|5951->3515|5986->3523|6014->3524
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|41->10|41->10|41->10|42->11|42->11|42->11|42->11|42->11|43->12|44->13|85->54|86->55|87->56|87->56|88->57|90->59|90->59|91->60|100->69|100->69|101->70|101->70|101->70|102->71|106->75|106->75|108->77|108->77|108->77|108->77|109->78|109->78|109->78|110->79|111->80|111->80|114->83|114->83|115->84|115->84|115->84|115->84|115->84|116->85|118->87|118->87|118->87|120->89|120->89|122->91|127->96|127->96|127->96|129->98|129->98
                  -- GENERATED --
              */
          