
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object assignmentFilterListPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class assignmentFilterListPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[List[String],String,String,List[Campaign],List[Location],List[Campaign],List[Campaign],List[ServiceTypes],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(allCRE:List[String],dealerName:String,user:String,campaign_list:List[Campaign],locationList:List[Location],campaignList:List[Campaign],campaignListPSF:List[Campaign],servicetypes:List[ServiceTypes]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.201*/("""

"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*4.52*/ {_display_(Seq[Any](format.raw/*4.54*/("""


"""),format.raw/*7.1*/("""<div class="panel panel-primary">
	<div class="panel-heading">
		<strong>Call Allocation | To Assign Calls</strong>
	</div>
	<div class="panel-body">
		<div class="row">


			<div class="col-sm-3 campaignTypeDiv">
				<div class="form-group">
					<label>Assignment Type :</label> <select class="form-control"
						name="campaignType" id="campaignTypeData"> 
						<option value='0'>--Select--</option>
						"""),_display_(/*20.8*/for(list_camp	<- campaign_list) yield /*20.39*/{_display_(Seq[Any](format.raw/*20.40*/("""
						"""),format.raw/*21.7*/("""<option value=""""),_display_(/*21.23*/list_camp/*21.32*/.id),format.raw/*21.35*/("""">"""),_display_(/*21.38*/list_camp/*21.47*/.campaignType),format.raw/*21.60*/("""</option> """)))}),format.raw/*21.71*/("""
					"""),format.raw/*22.6*/("""</select>
				</div>
			</div>


			<div class="col-md-3 campaignTypePSF" style="display: none;">
				<label>Select PSF Type :</label> <select id="campaignNamePSF"
					name="campaignNamePSF" class="form-control">
					<option value='0'>--Select--</option> """),_display_(/*30.45*/for(listcamp <-
					campaignListPSF) yield /*31.22*/{_display_(Seq[Any](format.raw/*31.23*/("""
					"""),format.raw/*32.6*/("""<option value=""""),_display_(/*32.22*/listcamp/*32.30*/.id),format.raw/*32.33*/("""">"""),_display_(/*32.36*/listcamp/*32.44*/.campaignName),format.raw/*32.57*/("""</option> """)))}),format.raw/*32.68*/("""
				"""),format.raw/*33.5*/("""</select>
			</div>
			<div class="col-md-3 selectcampaign" style="display: none">
				<div class="form-group">
					<label for="">Select Campaign Name</label> <select
						class="form-control" id="campaignName" name="campaignName">
						<option value='0'>--Select--</option> """),_display_(/*39.46*/for(list_camp <-
						campaignList) yield /*40.20*/{_display_(Seq[Any](format.raw/*40.21*/("""
						"""),format.raw/*41.7*/("""<option value=""""),_display_(/*41.23*/list_camp/*41.32*/.id),format.raw/*41.35*/("""">"""),_display_(/*41.38*/list_camp/*41.47*/.campaignName),format.raw/*41.60*/("""</option> """)))}),format.raw/*41.71*/("""


					"""),format.raw/*44.6*/("""</select>

				</div>
			</div>
			
			<div class="col-sm-3 fromDiv" >
				<div class="form-group">
					<label>From Date :</label> <input type="text-primary"
						class="datepicker form-control" placeholder="Enter From Date" id="singleData" name="singleData"
						readonly>
				</div>
			</div>


			<div class="col-sm-3 toDiv" >
				<div class="form-group">
					<label>To Date :</label> <input type="text-primary"
						class="datepicker form-control" placeholder="Enter To Date" id="tranferData"
						name="tranferData" readonly>
				</div>
			</div>		
			
			

	
		
		<div class="col-sm-3 model" style="display: none;">
				<div class="form-group">
					<label>Model:</label>
					
					<select	class="form-control" id="model"
						name="model">
					<option value="">SELECT</option>
					<option value="1">Commercial</option>
					<option value="0">Non-Commercial</option>
				</select>
				
				</div>
			</div>

			<div class="col-sm-3 custDiv" >
				<div class="form-group">
					<label>Customer Category :</label> <select class="form-control"
						id="cust_category" name="cust_category">
						<option value="All">All</option>
						<option value="A+">A+</option>
						<option value="B+">B+</option>

					</select>
				</div>
			</div>

			<div class="col-sm-3 servDiv" >
				<div class="form-group" >
					<label>Service Type :</label> <select class="form-control"
						id="service_type" name="service_type">
						<option value="All">All</option> """),_display_(/*100.41*/for(servTyp <- servicetypes) yield /*100.69*/{_display_(Seq[Any](format.raw/*100.70*/("""

						"""),format.raw/*102.7*/("""<option value=""""),_display_(/*102.23*/servTyp/*102.30*/.getServiceTypeName()),format.raw/*102.51*/("""">"""),_display_(/*102.54*/servTyp/*102.61*/.getServiceTypeName()),format.raw/*102.82*/("""</option>

						""")))}),format.raw/*104.8*/("""
					"""),format.raw/*105.6*/("""</select>
				</div>
			</div>


			<div class="col-md-3 locationSelect">

				<label for="cityName">Select City</label> <select
					class="form-control" id="city" name="cityName"
					onchange="ajaxCallToLoadWorkShopByCity();">
					<option value='select'>--Select City--</option>
					"""),_display_(/*116.7*/for(location_list<-locationList) yield /*116.39*/{_display_(Seq[Any](format.raw/*116.40*/("""
					"""),format.raw/*117.6*/("""<option value=""""),_display_(/*117.22*/location_list/*117.35*/.getName()),format.raw/*117.45*/("""">"""),_display_(/*117.48*/location_list/*117.61*/.getName()),format.raw/*117.71*/("""</option>
					""")))}),format.raw/*118.7*/("""
				"""),format.raw/*119.5*/("""</select>


			</div>
			
			</div>
			<div class="row">
			
			<div class="col-md-3 workshopSel">

				<label for="workshopId">Select Workshop</label> 
				<select	class="form-control" id="workshop" name="workshopId">
					<option value=""></option>
				</select>

			</div>

			<div class="col-sm-3 viewAssDiv">
				<label></label> 
					<button type="submit" id="" name="selectedFile" class="btn btn-sm btn-primary viewAssignCalls btn-block" value="viewAssignCalls">View
						Calls</button>

				
			</div>
			
			<div class="col-md-3 selectcre" style="display: none;">
			<label for="data">Select CRE's :</label> 
			<div class="form-group">
				
				<select multiple="multiple" id="data[]"
					name="data[]" class="selectpicker form-control" required="required">
					"""),_display_(/*150.7*/for(data1 <- allCRE) yield /*150.27*/{_display_(Seq[Any](format.raw/*150.28*/("""
					"""),format.raw/*151.6*/("""<option value=""""),_display_(/*151.22*/data1),format.raw/*151.27*/("""">"""),_display_(/*151.30*/data1),format.raw/*151.35*/("""</option> 
					""")))}),format.raw/*152.7*/("""

				"""),format.raw/*154.5*/("""</select>
				</div>
		
			</div>


			<div class="col-sm-3 assgnBtn" style="display: none;">
				
					<label></label>
					<button type="submit" class="btn btn-sm btn-primary assignCall btn-block"
						id="" name="selectedFile"
						value="assignCalls">Assign Calls</button>

				
			</div>

		</div>
	</div>


</div>


<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-info">
			<div class="panel-heading">List Of Calls To Assign</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover"
						id="assignedTable">
						<thead>
							<tr>

								<th>Customer Name</th>
								<th>Mobile Number</th>
								<th>Vehicle RegNo.</th>
								<th>Model</th>
								<th>Variant</th>
								<th>Next Service Date</th>


							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

""")))}),format.raw/*212.2*/("""


"""),format.raw/*215.1*/("""<script>
        $(document).ready(function () """),format.raw/*216.39*/("""{"""),format.raw/*216.40*/("""


        	"""),format.raw/*219.10*/("""$('.viewAssignCalls').click(function()"""),format.raw/*219.48*/("""{"""),format.raw/*219.49*/("""
            	
                """),format.raw/*221.17*/("""var city = $("#city").val();
                var workshop = $("#workshop").val();
                var fromDate = $("#singleData").val();
                var toDate = $("#tranferData").val();
                var servicetype = $("#service_type").val();
                var customerCatgy = $("#cust_category").val();
                var campaignTypeIs = $("#campaignTypeData").val();

                var campName = $("#campaignName").val();
                var psfName = $("#campaignNamePSF").val();
                var modelname = $("#model").val();
               

                if (fromDate != ''  && toDate != '' && city != 'select' && workshop != 'select') """),format.raw/*234.98*/("""{"""),format.raw/*234.99*/("""

                	"""),format.raw/*236.18*/("""$.blockUI();

                     var urlLink = "/CREManager/assignedCallsList"
                    	    
                    	    $('#assignedTable').dataTable( """),format.raw/*240.57*/("""{"""),format.raw/*240.58*/("""
                    	     """),format.raw/*241.27*/(""""bDestroy": true,
                    	     "processing": true,
                    	     "serverSide": true,
                    	     "scrollY": 300,
                    	     "paging": true,
                    	     "searching": false,
                    	     "ordering":false,
                    	     "ajax": """),format.raw/*248.35*/("""{"""),format.raw/*248.36*/("""
                    		        """),format.raw/*249.31*/("""'type': 'POST',
                    		        'url': urlLink,
                    		        'data': """),format.raw/*251.39*/("""{"""),format.raw/*251.40*/("""
                    		        	"""),format.raw/*252.32*/("""city: ''+city,
                    		        	workshop:''+workshop,
                    		        	fromDate:''+fromDate,
                    		        	toDate:''+toDate,
                    		        	customerCatgy:''+customerCatgy,
                    		        	servicetype:''+servicetype,
                    		        	campaignTypeIs:''+campaignTypeIs,
                    		        	campName:''+campName,
                    		        	psfName:''+psfName,
                    		        	modelname:''+modelname
                    		        	
                    		        	
                    		        """),format.raw/*264.31*/("""}"""),format.raw/*264.32*/("""
                        	        
                    	     """),format.raw/*266.27*/("""}"""),format.raw/*266.28*/("""
                    	 """),format.raw/*267.23*/("""}"""),format.raw/*267.24*/(""" """),format.raw/*267.25*/(""");
                     $.unblockUI();
                     $('.assgnBtn').css("display","block");
                     $('.selectcre').css("display","block");
                     
                    
                    return true;
                """),format.raw/*274.17*/("""}"""),format.raw/*274.18*/(""" """),format.raw/*274.19*/("""else """),format.raw/*274.24*/("""{"""),format.raw/*274.25*/("""
                    
                       """),format.raw/*276.24*/("""if (city == 'select' || city == '') """),format.raw/*276.60*/("""{"""),format.raw/*276.61*/("""

                        """),format.raw/*278.25*/("""Lobibox.notify('warning', """),format.raw/*278.51*/("""{"""),format.raw/*278.52*/("""
                            """),format.raw/*279.29*/("""msg: "Please select the city"
                        """),format.raw/*280.25*/("""}"""),format.raw/*280.26*/(""");
                        return false;

                    """),format.raw/*283.21*/("""}"""),format.raw/*283.22*/("""
                    """),format.raw/*284.21*/("""if (workshop == 'select' || workshop == '') """),format.raw/*284.65*/("""{"""),format.raw/*284.66*/("""

                        """),format.raw/*286.25*/("""Lobibox.notify('warning', """),format.raw/*286.51*/("""{"""),format.raw/*286.52*/("""
                            """),format.raw/*287.29*/("""msg: "Please select the workshop"
                        """),format.raw/*288.25*/("""}"""),format.raw/*288.26*/(""");
                        return false;

                    """),format.raw/*291.21*/("""}"""),format.raw/*291.22*/("""


                    """),format.raw/*294.21*/("""if (fromDate == '') """),format.raw/*294.41*/("""{"""),format.raw/*294.42*/("""

                        """),format.raw/*296.25*/("""Lobibox.notify('warning', """),format.raw/*296.51*/("""{"""),format.raw/*296.52*/("""
                            """),format.raw/*297.29*/("""msg: "Please enter the start date"
                        """),format.raw/*298.25*/("""}"""),format.raw/*298.26*/(""");
                        return false;

                    """),format.raw/*301.21*/("""}"""),format.raw/*301.22*/("""
                    """),format.raw/*302.21*/("""if (toDate == '') """),format.raw/*302.39*/("""{"""),format.raw/*302.40*/("""

                        """),format.raw/*304.25*/("""Lobibox.notify('warning', """),format.raw/*304.51*/("""{"""),format.raw/*304.52*/("""
                            """),format.raw/*305.29*/("""msg: "Please enter the End date"
                        """),format.raw/*306.25*/("""}"""),format.raw/*306.26*/(""");
                        return false;

                    """),format.raw/*309.21*/("""}"""),format.raw/*309.22*/("""                    
                 
                """),format.raw/*311.17*/("""}"""),format.raw/*311.18*/("""
            """),format.raw/*312.13*/("""}"""),format.raw/*312.14*/(""");


        	$('.assignCall').click(function()"""),format.raw/*315.43*/("""{"""),format.raw/*315.44*/("""

        		 """),format.raw/*317.12*/("""$.blockUI();
            	

        		 var city = $("#city").val();
                 var workshop = $("#workshop").val();
                 var fromDate = $("#singleData").val();
                 var toDate = $("#tranferData").val();
                 var servicetype = $("#service_type").val();
                 var customerCatgy = $("#cust_category").val();
                 var campaignTypeIs = $("#campaignTypeData").val();

                 var campName = $("#campaignName").val();
                 var psfName = $("#campaignNamePSF").val();
                 var modelname =$("#model").val();
                 var selected_cres;
             	var x = document.getElementById("data[]");
             	var j = 0;
             	for (var i = 0; i < x.options.length; i++) """),format.raw/*334.58*/("""{"""),format.raw/*334.59*/("""
             		"""),format.raw/*335.16*/("""if (x.options[i].selected == true) """),format.raw/*335.51*/("""{"""),format.raw/*335.52*/("""
             			"""),format.raw/*336.17*/("""if (j == 0)
             				selected_cres = x.options[i].value;
             			else
             				selected_cres = selected_cres + ","
             						+ x.options[i].value;

             			//console.log(selected_cres);
             			j++;
             		"""),format.raw/*344.16*/("""}"""),format.raw/*344.17*/("""
             	"""),format.raw/*345.15*/("""}"""),format.raw/*345.16*/("""


             	"""),format.raw/*348.15*/("""var urlLink = "/CREManager/assignedCallsListToUser"
            	    
             		$.ajax("""),format.raw/*350.23*/("""{"""),format.raw/*350.24*/("""
            	        """),format.raw/*351.22*/("""type: 'POST',	       
            	        url: urlLink,
            	        data: """),format.raw/*353.28*/("""{"""),format.raw/*353.29*/("""

            	        	"""),format.raw/*355.23*/("""city: ''+city,
        		        	workshop:''+workshop,
        		        	fromDate:''+fromDate,
        		        	toDate:''+toDate,
        		        	customerCatgy:''+customerCatgy,
        		        	servicetype:''+servicetype,
        		        	campaignTypeIs:''+campaignTypeIs,
        		        	campName:''+campName,
        		        	psfName:''+psfName,            	        	
            	        	selected_cres:''+selected_cres,            	        	
            	        	modelname:''+modelname
            	        	
            	        """),format.raw/*367.22*/("""}"""),format.raw/*367.23*/(""",
            	        success: function (json) """),format.raw/*368.47*/("""{"""),format.raw/*368.48*/("""
            	        	   """),format.raw/*369.26*/("""$.unblockUI();
            	        	window.location='assignmentFilterPage';
            	        """),format.raw/*371.22*/("""}"""),format.raw/*371.23*/(""",
            	        error: function () """),format.raw/*372.41*/("""{"""),format.raw/*372.42*/("""
            	        	
            	        	
            	        """),format.raw/*375.22*/("""}"""),format.raw/*375.23*/("""
            	       
            	    """),format.raw/*377.18*/("""}"""),format.raw/*377.19*/(""");
             	

        """),format.raw/*380.9*/("""}"""),format.raw/*380.10*/(""");

        """),format.raw/*382.9*/("""}"""),format.raw/*382.10*/(""");

    </script>
"""))
      }
    }
  }

  def render(allCRE:List[String],dealerName:String,user:String,campaign_list:List[Campaign],locationList:List[Location],campaignList:List[Campaign],campaignListPSF:List[Campaign],servicetypes:List[ServiceTypes]): play.twirl.api.HtmlFormat.Appendable = apply(allCRE,dealerName,user,campaign_list,locationList,campaignList,campaignListPSF,servicetypes)

  def f:((List[String],String,String,List[Campaign],List[Location],List[Campaign],List[Campaign],List[ServiceTypes]) => play.twirl.api.HtmlFormat.Appendable) = (allCRE,dealerName,user,campaign_list,locationList,campaignList,campaignListPSF,servicetypes) => apply(allCRE,dealerName,user,campaign_list,locationList,campaignList,campaignListPSF,servicetypes)

  def ref: this.type = this

}


}

/**/
object assignmentFilterListPage extends assignmentFilterListPage_Scope0.assignmentFilterListPage
              /*
                  -- GENERATED --
                  DATE: Thu Jan 04 13:05:46 IST 2018
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/assignmentFilterListPage.scala.html
                  HASH: 959d41508e6314f0d2e89fa913de6244ad109788
                  MATRIX: 882->2|1177->201|1205->204|1263->254|1302->256|1331->259|1768->670|1815->701|1854->702|1888->709|1931->725|1949->734|1973->737|2003->740|2021->749|2055->762|2097->773|2130->779|2415->1037|2468->1074|2507->1075|2540->1081|2583->1097|2600->1105|2624->1108|2654->1111|2671->1119|2705->1132|2747->1143|2779->1148|3085->1427|3137->1463|3176->1464|3210->1471|3253->1487|3271->1496|3295->1499|3325->1502|3343->1511|3377->1524|3419->1535|3454->1543|4951->3012|4996->3040|5036->3041|5072->3049|5116->3065|5133->3072|5176->3093|5207->3096|5224->3103|5267->3124|5316->3142|5350->3148|5666->3437|5715->3469|5755->3470|5789->3476|5833->3492|5856->3505|5888->3515|5919->3518|5942->3531|5974->3541|6021->3557|6054->3562|6857->4338|6894->4358|6934->4359|6968->4365|7012->4381|7039->4386|7070->4389|7097->4394|7145->4411|7179->4417|8246->5453|8277->5456|8353->5503|8383->5504|8424->5516|8491->5554|8521->5555|8581->5586|9273->6249|9303->6250|9351->6269|9543->6432|9573->6433|9629->6460|9976->6778|10006->6779|10066->6810|10195->6910|10225->6911|10286->6943|10924->7552|10954->7553|11044->7614|11074->7615|11126->7638|11156->7639|11186->7640|11467->7892|11497->7893|11527->7894|11561->7899|11591->7900|11665->7945|11730->7981|11760->7982|11815->8008|11870->8034|11900->8035|11958->8064|12041->8118|12071->8119|12162->8181|12192->8182|12242->8203|12315->8247|12345->8248|12400->8274|12455->8300|12485->8301|12543->8330|12630->8388|12660->8389|12751->8451|12781->8452|12833->8475|12882->8495|12912->8496|12967->8522|13022->8548|13052->8549|13110->8578|13198->8637|13228->8638|13319->8700|13349->8701|13399->8722|13446->8740|13476->8741|13531->8767|13586->8793|13616->8794|13674->8823|13760->8880|13790->8881|13881->8943|13911->8944|13995->8999|14025->9000|14067->9013|14097->9014|14173->9061|14203->9062|14245->9075|15045->9846|15075->9847|15120->9863|15184->9898|15214->9899|15260->9916|15552->10179|15582->10180|15626->10195|15656->10196|15702->10213|15823->10305|15853->10306|15904->10328|16017->10412|16047->10413|16100->10437|16681->10989|16711->10990|16788->11038|16818->11039|16873->11065|17000->11163|17030->11164|17101->11206|17131->11207|17228->11275|17258->11276|17326->11315|17356->11316|17411->11343|17441->11344|17481->11356|17511->11357
                  LINES: 27->2|32->2|34->4|34->4|34->4|37->7|50->20|50->20|50->20|51->21|51->21|51->21|51->21|51->21|51->21|51->21|51->21|52->22|60->30|61->31|61->31|62->32|62->32|62->32|62->32|62->32|62->32|62->32|62->32|63->33|69->39|70->40|70->40|71->41|71->41|71->41|71->41|71->41|71->41|71->41|71->41|74->44|130->100|130->100|130->100|132->102|132->102|132->102|132->102|132->102|132->102|132->102|134->104|135->105|146->116|146->116|146->116|147->117|147->117|147->117|147->117|147->117|147->117|147->117|148->118|149->119|180->150|180->150|180->150|181->151|181->151|181->151|181->151|181->151|182->152|184->154|242->212|245->215|246->216|246->216|249->219|249->219|249->219|251->221|264->234|264->234|266->236|270->240|270->240|271->241|278->248|278->248|279->249|281->251|281->251|282->252|294->264|294->264|296->266|296->266|297->267|297->267|297->267|304->274|304->274|304->274|304->274|304->274|306->276|306->276|306->276|308->278|308->278|308->278|309->279|310->280|310->280|313->283|313->283|314->284|314->284|314->284|316->286|316->286|316->286|317->287|318->288|318->288|321->291|321->291|324->294|324->294|324->294|326->296|326->296|326->296|327->297|328->298|328->298|331->301|331->301|332->302|332->302|332->302|334->304|334->304|334->304|335->305|336->306|336->306|339->309|339->309|341->311|341->311|342->312|342->312|345->315|345->315|347->317|364->334|364->334|365->335|365->335|365->335|366->336|374->344|374->344|375->345|375->345|378->348|380->350|380->350|381->351|383->353|383->353|385->355|397->367|397->367|398->368|398->368|399->369|401->371|401->371|402->372|402->372|405->375|405->375|407->377|407->377|410->380|410->380|412->382|412->382
                  -- GENERATED --
              */
          