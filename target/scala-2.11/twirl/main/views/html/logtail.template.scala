
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object logtail_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class logtail extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.1*/("""<!DOCTYPE html>
<!-- Copyright (c) 2012 Daniel Richman; GNU GPL 3 -->
<html>
    <head>
        <title>habitat parser log viewer</title>
        
          <script src=""""),_display_(/*7.25*/routes/*7.31*/.Assets.at("javascripts/jquery.min.js")),format.raw/*7.70*/("""" type="text/javascript"></script> 
      
       <script src=""""),_display_(/*9.22*/routes/*9.28*/.Assets.at("javascripts/logtail.js")),format.raw/*9.64*/("""" type="text/javascript"></script> 
        <link rel="stylesheet" href=""""),_display_(/*10.39*/routes/*10.45*/.Assets.at("css/logtail.css")),format.raw/*10.74*/("""">
    </head>
    <body>
        <div id="header">
            js-logtail.
            <a href="./">Reversed</a> or
            <a href="./?noreverse">chronological</a> view.
            <a id="pause" href='#'>Pause</a>.
        </div>
        <pre id="data">Loading...</pre>
    </body>
</html>
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object logtail extends logtail_Scope0.logtail
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:30 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/logtail.scala.html
                  HASH: 514cabc71d264d805347e25dd2c8dcf3b9abd845
                  MATRIX: 831->0|1033->176|1047->182|1106->221|1198->287|1212->293|1268->329|1370->404|1385->410|1435->439
                  LINES: 32->1|38->7|38->7|38->7|40->9|40->9|40->9|41->10|41->10|41->10
                  -- GENERATED --
              */
          