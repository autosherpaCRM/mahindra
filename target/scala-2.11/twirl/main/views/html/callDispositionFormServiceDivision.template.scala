
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object callDispositionFormServiceDivision_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class callDispositionFormServiceDivision extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template13[String,String,String,String,String,String,String,String,String,List[Campaign],List[ServiceTypes],List[String],List[CallDispositionData],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(toActivateTab1:String,toActivateTab2:String,toActivateTab3:String,toActivateTab4:String,toActivateTab5:String,dealercode:String,dealerName:String,userName:String,oem:String,campaignList :List[Campaign],serviceTypeList :List[ServiceTypes],bookedServiceTypeList:List[String],dispoList:List[CallDispositionData]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.312*/("""
"""),_display_(/*2.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*2.65*/ {_display_(Seq[Any](format.raw/*2.67*/("""

"""),format.raw/*4.1*/("""<input type="hidden" id="user" value=""""),_display_(/*4.40*/userName),format.raw/*4.48*/("""">

<input type="hidden" id="toActivateTab1" value=""""),_display_(/*6.50*/toActivateTab1),format.raw/*6.64*/("""">
<input type="hidden" id="toActivateTab2" value=""""),_display_(/*7.50*/toActivateTab2),format.raw/*7.64*/("""">
<input type="hidden" id="toActivateTab3" value=""""),_display_(/*8.50*/toActivateTab3),format.raw/*8.64*/("""">
<input type="hidden" id="toActivateTab4" value=""""),_display_(/*9.50*/toActivateTab4),format.raw/*9.64*/("""">
<input type="hidden" id="toActivateTab5" value=""""),_display_(/*10.50*/toActivateTab5),format.raw/*10.64*/("""">

<style>
    td, th """),format.raw/*13.12*/("""{"""),format.raw/*13.13*/("""
                
        """),format.raw/*15.9*/("""border: 1px solid black;
        overflow: hidden; 
        text-align:center!important;
    """),format.raw/*18.5*/("""}"""),format.raw/*18.6*/("""
"""),format.raw/*19.1*/("""</style>

<div class="row" >

        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><div id="schCRECount"><i class="fa fa-spinner fa-pulse fa-fw"></i></div></h3>
              <p>Scheduled Calls</p>
            </div>
            <div class="icon"> <i class="ion-android-call"></i> </div>
             
            <!--<span class="pull-left" data-toggle="modal" data-target=".myModal1">View Details</span>--> 
          </div>
        </div>      <!-- ./col -->
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><div id="serviceCREBookd"><i class="fa fa-spinner fa-pulse fa-fw"></i></div></h3>
              <p>Service Booked</p>
            </div>
            <div class="icon"> <i class="ion-android-checkbox-outline"></i> </div>
            </div>
        </div>        <!-- ./col -->
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><div id="serviceBookdCREPer"><i class="fa fa-spinner fa-pulse fa-fw"></i></div></h3>
              <p> Conversion </p>
            </div>
            <div class="icon"> <i class="ion-person-add"></i> </div>
             </div>
        </div>        <!-- ./col -->
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><div id="schCREPending"><i class="fa fa-spinner fa-pulse fa-fw"></i></div></h3>
              <p>Pending calls</p>
            </div>
            <div class="icon"> <i class="ion-android-download"></i> </div>
             </div>
        </div>        <!-- ./col --> 
    
</div>
<div class="row">



    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading"> <b>Call Log Information</b> </div>
            <div class="panel-body">
			<div class="panel panel-default">
                <ul class="nav nav-tabs">
                    <li><a href="#home" data-toggle="tab" id="Tab1" onclick="assignedInteractionData();">Scheduled</a> </li>
                    <li><a href="#profile" data-toggle="tab" id="Tab2" onclick="ajaxCallForFollowUpRequiredServer();">Follow Up Required</a> </li>
                    <li><a href="#messages" data-toggle="tab" id="Tab3" onclick="ajaxCallForServiceBookedServer();">Service Booked</a> </li>
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxCallForServiceNotRequiredServer();" >Service Not Required</a> </li>
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxCallForNonContactsServer();">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxCallForDroppedCallsServer();">Dropped</a> </li>
                </ul>
                <div class="tab-content">
				<div class="panel panel-default" style="border-top: #fff;">
				<div class="panel-body">
                 <div class="row">
                <div class="col-md-3 tab-pane" id="campaignDiv">
                                
                                <label>Select Campaign</label>
						<select class="filter form-control" id="campaignName" data-column-index="0" name="campaignName">
							<option value="0" >--Select--</option>
							"""),_display_(/*93.9*/for(campaign_List<-campaignList) yield /*93.41*/{_display_(Seq[Any](format.raw/*93.42*/("""
		                                """),format.raw/*94.35*/("""<option value=""""),_display_(/*94.51*/campaign_List/*94.64*/.getCampaignName()),format.raw/*94.82*/("""">"""),_display_(/*94.85*/campaign_List/*94.98*/.getCampaignName()),format.raw/*94.116*/("""</option>
		                             """)))}),format.raw/*95.33*/("""
		                                
						"""),format.raw/*97.7*/("""</select>
					</div>
					
					<div class="col-md-3" id="fromDateDiv">
					<label>From Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" placeholder="Enter From Date" id="fromduedaterange" name="fromduedaterange" readonly>
				  </div>
				   <div class="col-md-3" id="toDateDiv">
					<label>To Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="2" placeholder="Enter To Date" id="toduedaterange" name="toduedaterange" readonly>
				  </div>
						<div class="col-md-3" id="serviceTypeDiv">
					
			
						<label>Select service type</label>
						<select class="filter form-control" id="ServiceTypeName" data-column-index="3" name="ServiceTypeName">
							<option value="0" >--Select--</option>
							"""),_display_(/*114.9*/for(serviceType_List<-serviceTypeList) yield /*114.47*/{_display_(Seq[Any](format.raw/*114.48*/("""
			                                """),format.raw/*115.36*/("""<option value=""""),_display_(/*115.52*/serviceType_List/*115.68*/.getServiceTypeName()),format.raw/*115.89*/("""">"""),_display_(/*115.92*/serviceType_List/*115.108*/.getServiceTypeName()),format.raw/*115.129*/("""</option>
			                             """)))}),format.raw/*116.34*/("""
			                                
						"""),format.raw/*118.7*/("""</select>
					</div>
					<div class="col-md-3" id="serviceBookTypeDiv" style="display:none">						
					<label>Select Booked service type</label>
						<select class="filter form-control" id="serviceBookedType" data-column-index="4" name="serviceBookedType">
							<option value="0" >--Select--</option>
							"""),_display_(/*124.9*/for(bookedServiceType_List<-bookedServiceTypeList) yield /*124.59*/{_display_(Seq[Any](format.raw/*124.60*/("""
 """),format.raw/*125.2*/("""<option value=""""),_display_(/*125.18*/bookedServiceType_List),format.raw/*125.40*/("""">"""),_display_(/*125.43*/bookedServiceType_List),format.raw/*125.65*/("""</option>					                   
           """)))}),format.raw/*126.13*/("""
					                                
						"""),format.raw/*128.7*/("""</select>
					</div>
					<div class="col-md-3" id="lastDispoTypeDiv" style="display:none">						
					<label>Last Disposition</label>
						<select class="filter form-control" id="lastDispo" data-column-index="5" >
							<option value="0" >--Select--</option>
								"""),_display_(/*134.10*/for(dispo <- dispoList) yield /*134.33*/{_display_(Seq[Any](format.raw/*134.34*/("""
			"""),format.raw/*135.4*/("""<option value=""""),_display_(/*135.20*/dispo/*135.25*/.getDisposition()),format.raw/*135.42*/("""">"""),_display_(/*135.45*/dispo/*135.50*/.getDisposition()),format.raw/*135.67*/("""</option>
			
		""")))}),format.raw/*137.4*/("""
							
					                                
						"""),format.raw/*140.7*/("""</select>
					</div>
					<div class="col-md-3" id="droppedcountDiv" style="display:none">						
					<label>Dropped Count</label>
						<select class="filter form-control" id="droppedCount" data-column-index="6">
							<option value="0" >--Select--</option>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							
					                                
						</select>
					</div>
				</div>
				</div>
				</div>
                                
                            
                    <div class="panel panel-default tab-pane fade """),_display_(/*159.68*/toActivateTab1),format.raw/*159.82*/("""" id="home">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                
                                        <table class="table table-striped table-bordered table-hover" id="assignedInteractionTable" width="100%">
                                        <thead>
                                            <tr> 
											<th>Campaign</th>	
                                             <th>Customer&nbsp;Name</th>
                                                <th>RegNo.</th>
                                                <th>Model</th> 
                                                <th>Category</th>                                                
                                                <th>DueDate</th>
                                                <th>Type</th>                                                
                                                <th>PSFStatus</th>
                                                <th>DND</th>
                                                <th>Complaint</th>
						<th class="no-sort"></th>





                                            </tr>
                                        </thead>
                                        <tbody>								




                                        </tbody>

                                    </table>




                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade """),_display_(/*204.68*/toActivateTab2),format.raw/*204.82*/("""" id="profile">
                        <div class="panel-body inf-content">

                            <div class="dataTable_wrapper">                            
                                
                                    <table class="table table-striped table-bordered table-hover" id="followUpRequiredServerData">
                                        <thead>
                                         <tr> 
                                                <th>Campaign</th> 
                                                <th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>                                                                   
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>FollowUp Date</th>                                    
                                                <th>FollowUp Time</th>
                                                <th>Last Disposition</th>
                                                <th class="no-sort"></th>



                                            </tr>   
                                        </thead>
                                        <tbody>

                                        </tbody>

                                    </table>

                            </div>

                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade """),_display_(/*237.68*/toActivateTab3),format.raw/*237.82*/("""" id="messages">
                        <div class="panel-body inf-content">


                            <div class="dataTable_wrapper">                            
                                
                                    <table class="table table-striped table-bordered table-hover" id="serviceBookedServerData">
                                        <thead>
                                            <tr> 
												<th>Booking ID</th>												
                                                <th>Campaign</th> 
                                                <th>Call Date</th>                           	
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>Service ScheduledDate</th>  
                                                <th>Service ScheduledTime</th> 
                                                <th>Service Advisor</th>  
                                                <th>Booking Status</th>
                                                <th>Last Disposition</th>
                                                <th class="no-sort"></th>



                                            </tr>
                                        </thead>
                                        <tbody>




                                        </tbody>

                                    </table>


                            </div>

                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade """),_display_(/*278.68*/toActivateTab4),format.raw/*278.82*/("""" id="settings">
                         <div class="panel-body inf-content">

                        <div class="dataTable_wrapper">                            
                            <div style="overflow-x: auto">
                                <table class="table table-striped table-bordered table-hover" id="serviceNotRequiredServerData">
                                    <thead>
                                        <tr> 
                                            <th>Campaign</th>
                                            <th>Call Date</th>                           	
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Service Due</th>
                                            <th>Last Disposition</th>
                                            <th>Reason</th>
                                            <th class="no-sort"></th>



                                        </tr>
                                    </thead>
                                    <tbody>




                                    </tbody>

                                </table>




                            </div>

                        </div>
                    </div>

                    </div><!--End tab panel-->
                    <div class="panel panel-default tab-pane fade """),_display_(/*318.68*/toActivateTab5),format.raw/*318.82*/("""" id="nonContacts">
                        
                        <div class="panel-body inf-content"> 
                           
                            <div style="overflow-x: auto">
                                <table class="table table-striped table-bordered table-hover" id="nonContactsServerData" width="100%">
                                    <thead>
                                        <tr> 
                                           
                                           <th>Campaign</th> 
                                           <th>Call Date</th>
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Service Due</th>
                                            <th>Last Disposition</th>
                                            <th class="no-sort"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>


                    <div class="panel panel-default tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">


                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="droppedCallsServerData" width="100%">
                                        <thead>
                                            <tr> 
												   <th>Campaign</th>
												   <th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>Last Disposition</th>





                                            </tr>
                                        </thead>
                                        <tbody>



                                        </tbody>

                                    </table>




                                </div>

                            </div>

                        </div>
                    </div>
                    </div>




                </div><!--End tab content-->
            </div><!--End panel body-->
        </div>
    </div>
</div>

""")))}),format.raw/*397.2*/("""
"""),format.raw/*398.1*/("""<script type="text/javascript">
    window.onload = ajaxRequestDataOFCREIndexPageWithTab;
</script>
<script>

    function getvalMain(sel) """),format.raw/*403.30*/("""{"""),format.raw/*403.31*/("""
        """),format.raw/*404.9*/("""var selVal = sel.value;
        var selValuess = 'Already Serviced';

        if (selVal == selValuess) """),format.raw/*407.35*/("""{"""),format.raw/*407.36*/("""
            """),format.raw/*408.13*/("""$('.show1').css('display', 'block');
        """),format.raw/*409.9*/("""}"""),format.raw/*409.10*/(""" """),format.raw/*409.11*/("""else """),format.raw/*409.16*/("""{"""),format.raw/*409.17*/("""
            """),format.raw/*410.13*/("""$('.show1').css('display', 'none');

        """),format.raw/*412.9*/("""}"""),format.raw/*412.10*/("""
    """),format.raw/*413.5*/("""}"""),format.raw/*413.6*/("""

    """),format.raw/*415.5*/("""function getval(sel) """),format.raw/*415.26*/("""{"""),format.raw/*415.27*/("""
        """),format.raw/*416.9*/("""var onSelVal = sel.value;
        var selValuesTab2 = 'Serviced At Other Dealer';

        if (onSelVal == selValuesTab2) """),format.raw/*419.40*/("""{"""),format.raw/*419.41*/("""
            """),format.raw/*420.13*/("""$('.show2').css('display', 'block');
        """),format.raw/*421.9*/("""}"""),format.raw/*421.10*/(""" """),format.raw/*421.11*/("""else """),format.raw/*421.16*/("""{"""),format.raw/*421.17*/("""
            """),format.raw/*422.13*/("""$('.show2').css('display', 'none');

        """),format.raw/*424.9*/("""}"""),format.raw/*424.10*/("""
    """),format.raw/*425.5*/("""}"""),format.raw/*425.6*/("""

"""),format.raw/*427.1*/("""</script>

<script type="text/javascript">
      
 function assignedInteractionData()"""),format.raw/*431.36*/("""{"""),format.raw/*431.37*/("""
    """),format.raw/*432.5*/("""// alert("server side data load");
    document.getElementById("campaignDiv").style.display = "block";
    document.getElementById("serviceBookTypeDiv").style.display = "none";
    document.getElementById("serviceTypeDiv").style.display = "block";
    document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";
     
    var table= $('#assignedInteractionTable').dataTable( """),format.raw/*441.58*/("""{"""),format.raw/*441.59*/("""
        """),format.raw/*442.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": "/assignedInteractionTableData",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*451.59*/("""{"""),format.raw/*451.60*/("""
              """),format.raw/*452.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*454.15*/("""}"""),format.raw/*454.16*/("""
    """),format.raw/*455.5*/("""}"""),format.raw/*455.6*/(""" """),format.raw/*455.7*/(""");

    FilterOptionData(table);
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*459.61*/("""{"""),format.raw/*459.62*/("""
      """),format.raw/*460.7*/("""$($.fn.dataTable.tables(true)).DataTable()
         .columns.adjust();
   """),format.raw/*462.4*/("""}"""),format.raw/*462.5*/(""");
    
    """),format.raw/*464.5*/("""}"""),format.raw/*464.6*/("""
    
    
    """),format.raw/*467.5*/("""function ajaxCallForFollowUpRequiredServer()"""),format.raw/*467.49*/("""{"""),format.raw/*467.50*/("""
    	"""),format.raw/*468.6*/("""document.getElementById("campaignDiv").style.display = "block";
    	document.getElementById("serviceBookTypeDiv").style.display = "none";
    	document.getElementById("serviceTypeDiv").style.display = "block";
    	document.getElementById("fromDateDiv").style.display = "block";
    	document.getElementById("toDateDiv").style.display = "block";
    	document.getElementById("lastDispoTypeDiv").style.display = "none";
    	document.getElementById("droppedcountDiv").style.display = "none";

    	
              
        //alert("follow up server side data");
        var table= $('#followUpRequiredServerData').dataTable( """),format.raw/*479.64*/("""{"""),format.raw/*479.65*/("""
        """),format.raw/*480.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching": true,
        "ordering":false,	
        "ajax": "/followUpCallLogTableData",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*489.59*/("""{"""),format.raw/*489.60*/("""
              """),format.raw/*490.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*492.15*/("""}"""),format.raw/*492.16*/("""
    """),format.raw/*493.5*/("""}"""),format.raw/*493.6*/(""" """),format.raw/*493.7*/(""");

        FilterOptionData(table);
        
    """),format.raw/*497.5*/("""}"""),format.raw/*497.6*/("""
    
    
    
    """),format.raw/*501.5*/("""function ajaxCallForServiceBookedServer()"""),format.raw/*501.46*/("""{"""),format.raw/*501.47*/("""

    	"""),format.raw/*503.6*/("""document.getElementById("campaignDiv").style.display = "block";
    	document.getElementById("serviceBookTypeDiv").style.display = "block";
    	document.getElementById("serviceTypeDiv").style.display = "none";
    	document.getElementById("fromDateDiv").style.display = "block";
    	document.getElementById("toDateDiv").style.display = "block";
    	document.getElementById("lastDispoTypeDiv").style.display = "none";
    	document.getElementById("droppedcountDiv").style.display = "none";
        
    	 var table= $('#serviceBookedServerData').dataTable( """),format.raw/*511.59*/("""{"""),format.raw/*511.60*/("""
        """),format.raw/*512.9*/(""""bDestroy": true,  
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching": true,
        "ordering":false,		
        "ajax": "/serviceBookedServerDataTable",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*521.59*/("""{"""),format.raw/*521.60*/("""
              """),format.raw/*522.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*524.15*/("""}"""),format.raw/*524.16*/("""
    """),format.raw/*525.5*/("""}"""),format.raw/*525.6*/(""" """),format.raw/*525.7*/(""");  
    	 FilterOptionData(table);
        
    """),format.raw/*528.5*/("""}"""),format.raw/*528.6*/("""
    
    """),format.raw/*530.5*/("""function ajaxCallForServiceNotRequiredServer()"""),format.raw/*530.51*/("""{"""),format.raw/*530.52*/("""
    	"""),format.raw/*531.6*/("""document.getElementById("campaignDiv").style.display = "block";
    	document.getElementById("serviceBookTypeDiv").style.display = "none";
    	document.getElementById("serviceTypeDiv").style.display = "none";
    	document.getElementById("fromDateDiv").style.display = "none";
    	document.getElementById("toDateDiv").style.display = "none";
    	document.getElementById("lastDispoTypeDiv").style.display = "none";
    	document.getElementById("droppedcountDiv").style.display = "none";
        
    	 var table= $('#serviceNotRequiredServerData').dataTable( """),format.raw/*539.64*/("""{"""),format.raw/*539.65*/("""
        
        """),format.raw/*541.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching": true,
        "ordering":false,		
        "ajax": "/serviceNotRequiredServerDataTable",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*550.59*/("""{"""),format.raw/*550.60*/("""
              """),format.raw/*551.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*553.15*/("""}"""),format.raw/*553.16*/("""
    """),format.raw/*554.5*/("""}"""),format.raw/*554.6*/(""" """),format.raw/*554.7*/(""");    

    	 FilterOptionData(table);  
        
    """),format.raw/*558.5*/("""}"""),format.raw/*558.6*/("""
    
    
    """),format.raw/*561.5*/("""function ajaxCallForNonContactsServer()"""),format.raw/*561.44*/("""{"""),format.raw/*561.45*/("""

    	"""),format.raw/*563.6*/("""document.getElementById("campaignDiv").style.display = "block";
    	document.getElementById("serviceBookTypeDiv").style.display = "none";
    	document.getElementById("serviceTypeDiv").style.display = "none";
    	document.getElementById("fromDateDiv").style.display = "block";
    	document.getElementById("toDateDiv").style.display = "block";
    	document.getElementById("lastDispoTypeDiv").style.display = "block";
    	document.getElementById("droppedcountDiv").style.display = "block";
      
    	var table= $('#nonContactsServerData').dataTable( """),format.raw/*571.56*/("""{"""),format.raw/*571.57*/("""
        """),format.raw/*572.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching": true,
        "ordering":false,				
        "ajax": "/nonContactsServerDataTable",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*581.59*/("""{"""),format.raw/*581.60*/("""
              """),format.raw/*582.15*/("""$('td', nRow).attr('nowrap','nowrap');
              return nRow;
              """),format.raw/*584.15*/("""}"""),format.raw/*584.16*/("""
    """),format.raw/*585.5*/("""}"""),format.raw/*585.6*/(""" """),format.raw/*585.7*/(""");
        
         FilterOptionData(table);  
        
    """),format.raw/*589.5*/("""}"""),format.raw/*589.6*/("""
    
    
    """),format.raw/*592.5*/("""function ajaxCallForDroppedCallsServer()"""),format.raw/*592.45*/("""{"""),format.raw/*592.46*/("""
        
    	"""),format.raw/*594.6*/("""document.getElementById("campaignDiv").style.display = "none";
    	document.getElementById("serviceBookTypeDiv").style.display = "none";
    	document.getElementById("serviceTypeDiv").style.display = "none";
    	document.getElementById("fromDateDiv").style.display = "none";
    	document.getElementById("toDateDiv").style.display = "none";
    	document.getElementById("lastDispoTypeDiv").style.display = "none";
    	document.getElementById("droppedcountDiv").style.display = "none";
        
       $('#droppedCallsServerData').dataTable( """),format.raw/*602.48*/("""{"""),format.raw/*602.49*/("""
        """),format.raw/*603.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching": true,
        "ordering":false,        
        "ajax": "/droppedCallsServerDataTable",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*612.59*/("""{"""),format.raw/*612.60*/("""
              """),format.raw/*613.15*/("""$('td', nRow).attr('nowrap','nowrap');
              return nRow;
              """),format.raw/*615.15*/("""}"""),format.raw/*615.16*/("""
    """),format.raw/*616.5*/("""}"""),format.raw/*616.6*/(""" """),format.raw/*616.7*/("""); 
        
    """),format.raw/*618.5*/("""}"""),format.raw/*618.6*/("""
    
"""),format.raw/*620.1*/("""</script>
<script>
	 
	var urlDisposition = "/CRE/dashboardCounts";
    $.ajax("""),format.raw/*624.12*/("""{"""),format.raw/*624.13*/("""
        """),format.raw/*625.9*/("""url: urlDisposition

    """),format.raw/*627.5*/("""}"""),format.raw/*627.6*/(""").done(function (dashboardCount) """),format.raw/*627.39*/("""{"""),format.raw/*627.40*/("""

        """),format.raw/*629.9*/("""//console.log(dashboardCount);

  	  var dashCount = dashboardCount;
  		 $("#schCRECount").html(dashCount[0]);
  		 $("#serviceCREBookd").html(dashCount[1]);
  	     $("#serviceBookdCREPer").html(dashCount[2]);
  	     $("#schCREPending").html(dashCount[3]);
        
    """),format.raw/*637.5*/("""}"""),format.raw/*637.6*/(""");



</script>
<script>

function FilterOptionData(table)"""),format.raw/*644.33*/("""{"""),format.raw/*644.34*/("""
	"""),format.raw/*645.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*646.39*/("""{"""),format.raw/*646.40*/("""
    	"""),format.raw/*647.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
		console.log(v);
		console.log(i);
	    
	    if(v.length>0)"""),format.raw/*653.20*/("""{"""),format.raw/*653.21*/("""
		    	   
		   """),format.raw/*655.6*/("""values[i] = v;
	    
	    """),format.raw/*657.6*/("""}"""),format.raw/*657.7*/("""
	    
	    	"""),format.raw/*659.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');

	 	 if(values.length>0) """),format.raw/*664.25*/("""{"""),format.raw/*664.26*/("""
			 """),format.raw/*665.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*665.37*/("""{"""),format.raw/*665.38*/("""	
			    """),format.raw/*666.8*/("""table.api().columns(i).search(values[i]);
			 """),format.raw/*667.5*/("""}"""),format.raw/*667.6*/("""
			 """),format.raw/*668.5*/("""table.api().draw();
			 """),format.raw/*669.5*/("""}"""),format.raw/*669.6*/(""" 
		 	  
	"""),format.raw/*671.2*/("""}"""),format.raw/*671.3*/(""");
	
	
"""),format.raw/*674.1*/("""}"""),format.raw/*674.2*/("""
"""),format.raw/*675.1*/("""</script>"""))
      }
    }
  }

  def render(toActivateTab1:String,toActivateTab2:String,toActivateTab3:String,toActivateTab4:String,toActivateTab5:String,dealercode:String,dealerName:String,userName:String,oem:String,campaignList:List[Campaign],serviceTypeList:List[ServiceTypes],bookedServiceTypeList:List[String],dispoList:List[CallDispositionData]): play.twirl.api.HtmlFormat.Appendable = apply(toActivateTab1,toActivateTab2,toActivateTab3,toActivateTab4,toActivateTab5,dealercode,dealerName,userName,oem,campaignList,serviceTypeList,bookedServiceTypeList,dispoList)

  def f:((String,String,String,String,String,String,String,String,String,List[Campaign],List[ServiceTypes],List[String],List[CallDispositionData]) => play.twirl.api.HtmlFormat.Appendable) = (toActivateTab1,toActivateTab2,toActivateTab3,toActivateTab4,toActivateTab5,dealercode,dealerName,userName,oem,campaignList,serviceTypeList,bookedServiceTypeList,dispoList) => apply(toActivateTab1,toActivateTab2,toActivateTab3,toActivateTab4,toActivateTab5,dealercode,dealerName,userName,oem,campaignList,serviceTypeList,bookedServiceTypeList,dispoList)

  def ref: this.type = this

}


}

/**/
object callDispositionFormServiceDivision extends callDispositionFormServiceDivision_Scope0.callDispositionFormServiceDivision
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:18 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/callDispositionFormServiceDivision.scala.html
                  HASH: 102c9f4904f6c55ff7105f483b9278a8cf84ace9
                  MATRIX: 933->1|1339->311|1367->314|1438->377|1477->379|1507->383|1572->422|1600->430|1681->485|1715->499|1794->552|1828->566|1907->619|1941->633|2020->686|2054->700|2134->753|2169->767|2223->793|2252->794|2307->822|2430->918|2458->919|2487->921|6062->4470|6110->4502|6149->4503|6213->4539|6256->4555|6278->4568|6317->4586|6347->4589|6369->4602|6409->4620|6483->4663|6554->4707|7417->5543|7472->5581|7512->5582|7578->5619|7622->5635|7648->5651|7691->5672|7722->5675|7749->5691|7793->5712|7869->5756|7942->5801|8290->6122|8357->6172|8397->6173|8428->6176|8472->6192|8516->6214|8547->6217|8591->6239|8670->6286|8745->6333|9051->6611|9091->6634|9131->6635|9164->6640|9208->6656|9223->6661|9262->6678|9293->6681|9308->6686|9347->6703|9397->6722|9481->6778|10173->7442|10209->7456|12010->9229|12046->9243|13753->10922|13789->10936|15651->12770|15687->12784|17268->14337|17304->14351|20242->17258|20272->17260|20445->17404|20475->17405|20513->17415|20649->17522|20679->17523|20722->17537|20796->17583|20826->17584|20856->17585|20890->17590|20920->17591|20963->17605|21038->17652|21068->17653|21102->17659|21131->17660|21167->17668|21217->17689|21247->17690|21285->17700|21439->17825|21469->17826|21512->17840|21586->17886|21616->17887|21646->17888|21680->17893|21710->17894|21753->17908|21828->17955|21858->17956|21892->17962|21921->17963|21953->17967|22071->18056|22101->18057|22135->18063|22752->18651|22782->18652|22820->18662|23161->18974|23191->18975|23236->18991|23343->19069|23373->19070|23407->19076|23436->19077|23465->19078|23596->19180|23626->19181|23662->19189|23766->19265|23795->19266|23837->19280|23866->19281|23912->19299|23985->19343|24015->19344|24050->19351|24714->19986|24744->19987|24782->19997|25118->20304|25148->20305|25193->20321|25300->20399|25330->20400|25364->20406|25393->20407|25422->20408|25504->20462|25533->20463|25585->20487|25655->20528|25685->20529|25722->20538|26318->21105|26348->21106|26386->21116|26729->21430|26759->21431|26804->21447|26911->21525|26941->21526|26975->21532|27004->21533|27033->21534|27113->21586|27142->21587|27182->21599|27257->21645|27287->21646|27322->21653|27920->22222|27950->22223|27998->22243|28344->22560|28374->22561|28419->22577|28526->22655|28556->22656|28590->22662|28619->22663|28648->22664|28734->22722|28763->22723|28809->22741|28877->22780|28907->22781|28944->22790|29536->23353|29566->23354|29604->23364|29945->23676|29975->23677|30020->23693|30131->23775|30161->23776|30195->23782|30224->23783|30253->23784|30346->23849|30375->23850|30421->23868|30490->23908|30520->23909|30565->23926|31146->24478|31176->24479|31214->24489|31560->24806|31590->24807|31635->24823|31746->24905|31776->24906|31810->24912|31839->24913|31868->24914|31915->24933|31944->24934|31980->24942|32092->25025|32122->25026|32160->25036|32215->25063|32244->25064|32306->25097|32336->25098|32376->25110|32685->25391|32714->25392|32808->25457|32838->25458|32869->25461|32954->25517|32984->25518|33019->25525|33230->25707|33260->25708|33307->25727|33363->25755|33392->25756|33435->25771|33636->25943|33666->25944|33700->25950|33761->25982|33791->25983|33829->25993|33904->26040|33933->26041|33967->26047|34020->26072|34049->26073|34089->26085|34118->26086|34156->26096|34185->26097|34215->26099
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|35->4|35->4|37->6|37->6|38->7|38->7|39->8|39->8|40->9|40->9|41->10|41->10|44->13|44->13|46->15|49->18|49->18|50->19|124->93|124->93|124->93|125->94|125->94|125->94|125->94|125->94|125->94|125->94|126->95|128->97|145->114|145->114|145->114|146->115|146->115|146->115|146->115|146->115|146->115|146->115|147->116|149->118|155->124|155->124|155->124|156->125|156->125|156->125|156->125|156->125|157->126|159->128|165->134|165->134|165->134|166->135|166->135|166->135|166->135|166->135|166->135|166->135|168->137|171->140|190->159|190->159|235->204|235->204|268->237|268->237|309->278|309->278|349->318|349->318|428->397|429->398|434->403|434->403|435->404|438->407|438->407|439->408|440->409|440->409|440->409|440->409|440->409|441->410|443->412|443->412|444->413|444->413|446->415|446->415|446->415|447->416|450->419|450->419|451->420|452->421|452->421|452->421|452->421|452->421|453->422|455->424|455->424|456->425|456->425|458->427|462->431|462->431|463->432|472->441|472->441|473->442|482->451|482->451|483->452|485->454|485->454|486->455|486->455|486->455|490->459|490->459|491->460|493->462|493->462|495->464|495->464|498->467|498->467|498->467|499->468|510->479|510->479|511->480|520->489|520->489|521->490|523->492|523->492|524->493|524->493|524->493|528->497|528->497|532->501|532->501|532->501|534->503|542->511|542->511|543->512|552->521|552->521|553->522|555->524|555->524|556->525|556->525|556->525|559->528|559->528|561->530|561->530|561->530|562->531|570->539|570->539|572->541|581->550|581->550|582->551|584->553|584->553|585->554|585->554|585->554|589->558|589->558|592->561|592->561|592->561|594->563|602->571|602->571|603->572|612->581|612->581|613->582|615->584|615->584|616->585|616->585|616->585|620->589|620->589|623->592|623->592|623->592|625->594|633->602|633->602|634->603|643->612|643->612|644->613|646->615|646->615|647->616|647->616|647->616|649->618|649->618|651->620|655->624|655->624|656->625|658->627|658->627|658->627|658->627|660->629|668->637|668->637|675->644|675->644|676->645|677->646|677->646|678->647|684->653|684->653|686->655|688->657|688->657|690->659|695->664|695->664|696->665|696->665|696->665|697->666|698->667|698->667|699->668|700->669|700->669|702->671|702->671|705->674|705->674|706->675
                  -- GENERATED --
              */
          