
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object addCampaign_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class addCampaign extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,String,String,List[Location],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealercode:String,dealerName:String,userName:String,locationList:List[Location],campaignTypedata:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.107*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*2.56*/ {_display_(Seq[Any](format.raw/*2.58*/("""
"""),_display_(/*3.2*/helper/*3.8*/.form(action = routes.CampaignController.postCampaign)/*3.62*/ {_display_(Seq[Any](format.raw/*3.64*/("""

"""),format.raw/*5.1*/("""<!--
<script type="text/javascript">
  $(document).ready(function() """),format.raw/*7.32*/("""{"""),format.raw/*7.33*/("""
    """),format.raw/*8.5*/("""$('#customerSearchTable').dataTable( """),format.raw/*8.42*/("""{"""),format.raw/*8.43*/("""
        """),format.raw/*9.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
        "paging": true,
        "searching":true,
        "ordering":false,
        "ajax": "/searchByCustomerv2"
        
    """),format.raw/*18.5*/("""}"""),format.raw/*18.6*/(""" """),format.raw/*18.7*/(""");
"""),format.raw/*19.1*/("""}"""),format.raw/*19.2*/(""" """),format.raw/*19.3*/(""");
</script>	-->
	


<div class="panel panel-primary">
    <div class="panel-heading"><strong>Add Campaign</strong></div>
    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-info">
                    <div class="panel-heading"><b>Add Information Of Campaign</b></div>
                    <div class="panel-body">
	                   <div class="row">
                            <div class="col-lg-2">
                                <label>Select File Format:</label>
                                <input type="text" class="form-control"  id="campaignType" name="campaignType" value=""""),_display_(/*37.120*/campaignTypedata),format.raw/*37.136*/("""" readonly>
                            </div>
                            <div class="col-md-2 campaignNameInsert" >
                                <div class="form-group">
                                    <label for="campaignName">Campaign Name</label>
                                    <input type="text" class="form-control"  id="campaignName" name="campaignName">			  
                                </div>				 
                            </div>
                          
                             <div class="col-md-2">
                                <div class="form-group">
                                    <label for="cityName">Select City</label>
                                    <select class="form-control" id="city" name="name" onchange="ajaxCallToLoadWorkShopByCity();">	
                                        <option value='select'>--Select City--</option>
                                        """),_display_(/*51.42*/for(location_list<-locationList) yield /*51.74*/{_display_(Seq[Any](format.raw/*51.75*/("""
                                        """),format.raw/*52.41*/("""<option value=""""),_display_(/*52.57*/location_list/*52.70*/.getName()),format.raw/*52.80*/("""">"""),_display_(/*52.83*/location_list/*52.96*/.getName()),format.raw/*52.106*/("""</option>
                                        """)))}),format.raw/*53.42*/("""
                                    """),format.raw/*54.37*/("""</select>

                                </div>
                            </div>

                  
                              
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="workshopId">Select """),_display_(/*63.69*/if(campaignTypedata == "Insurance")/*63.104*/{_display_(Seq[Any](format.raw/*63.105*/("""Location""")))}/*63.114*/else/*63.118*/{_display_(Seq[Any](format.raw/*63.119*/("""Workshop""")))}),format.raw/*63.128*/("""</label>
                                    <select class="form-control" id="workshop" name="id" >					  
                                        <option value=""></option>
                                    </select>
                                </div>				 
                            </div>
                            <div class="col-md-2 campaignStartdate"  >
                                <div class="form-group">
                                    <label for="seldate">Select From Date</label>
                                    <input type="text" class="form-control datepicker"  id="campaignStart" name="createdDate" readonly>			  
                                </div>				 
                            </div>
                            <div class="col-md-2 campaignExpiry">
                                <div class="form-group">
                                    <label for="seldateExp">Select Expiry Date</label>
                                    <input type="text" class="form-control datepicker" id="campaignExpiryNew" name="expiryDate" readonly>
                                </div>				 
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary pull-right" name="submit" id="uploadFileCampaign" value="Submit" >
                    </div>

                </div>

                <div class="hr-line-dashed"></div>
<!--                <div class="ibox-content">			
                    <div class="table-responsive">
                        <table class="table table-bordered header-fixed table-responsive" id="tableData" style="width:80%">
                            <thead>

                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div> 
                    <br />

                </div>-->

            </div>	

        </div>
        <!--<button type="submit" class="btn btn-primary" name="submit" onclick="ajaxUploadData()" >Submit</button>-->


    </div>

""")))}),format.raw/*110.2*/("""
""")))}),format.raw/*111.2*/("""
"""),format.raw/*112.1*/("""<script>
        $(document).ready(function () """),format.raw/*113.39*/("""{"""),format.raw/*113.40*/("""
            
            """),format.raw/*115.13*/("""$("#uploadFileCampaign").click(function () """),format.raw/*115.56*/("""{"""),format.raw/*115.57*/("""
                
                """),format.raw/*117.17*/("""var campaignType = $("#campaignType").val();
                var campaignName = $("#campaignName").val();
                var city = $("#city").val();
                var workshop = $("#workshop").val();
                var campaignStart = $("#campaignStart").val();
                var campaignExpiryNew = $("#campaignExpiryNew").val();

                if (campaignType != ''  && campaignName != '' && city != 'select' && workshop != 'select') """),format.raw/*124.108*/("""{"""),format.raw/*124.109*/("""
                    """),format.raw/*125.21*/("""Lobibox.notify('success', """),format.raw/*125.47*/("""{"""),format.raw/*125.48*/("""
                        """),format.raw/*126.25*/("""msg: "Uploaded Successfully"
                    """),format.raw/*127.21*/("""}"""),format.raw/*127.22*/(""");
                    setTimeout(function () """),format.raw/*128.44*/("""{"""),format.raw/*128.45*/("""
                        """),format.raw/*129.25*/("""location.reload();
                    """),format.raw/*130.21*/("""}"""),format.raw/*130.22*/(""", 5000);
                    return true;
                """),format.raw/*132.17*/("""}"""),format.raw/*132.18*/(""" """),format.raw/*132.19*/("""else """),format.raw/*132.24*/("""{"""),format.raw/*132.25*/("""
                            
                    """),format.raw/*134.21*/("""if (campaignType == '' || campaignType!='Campaign') """),format.raw/*134.73*/("""{"""),format.raw/*134.74*/("""

                        """),format.raw/*136.25*/("""return false;

                    """),format.raw/*138.21*/("""}"""),format.raw/*138.22*/("""
                    """),format.raw/*139.21*/("""if (campaignName == '') """),format.raw/*139.45*/("""{"""),format.raw/*139.46*/("""

                        """),format.raw/*141.25*/("""Lobibox.notify('warning', """),format.raw/*141.51*/("""{"""),format.raw/*141.52*/("""
                            """),format.raw/*142.29*/("""msg: "Please enter the name of the Campaign"
                        """),format.raw/*143.25*/("""}"""),format.raw/*143.26*/(""");
                        return false;

                    """),format.raw/*146.21*/("""}"""),format.raw/*146.22*/("""
                       """),format.raw/*147.24*/("""if (city == 'select' || city == '') """),format.raw/*147.60*/("""{"""),format.raw/*147.61*/("""

                        """),format.raw/*149.25*/("""Lobibox.notify('warning', """),format.raw/*149.51*/("""{"""),format.raw/*149.52*/("""
                            """),format.raw/*150.29*/("""msg: "Please select the city"
                        """),format.raw/*151.25*/("""}"""),format.raw/*151.26*/(""");
                        return false;

                    """),format.raw/*154.21*/("""}"""),format.raw/*154.22*/("""
                    """),format.raw/*155.21*/("""if (workshop == 'select' || workshop == '') """),format.raw/*155.65*/("""{"""),format.raw/*155.66*/("""

                        """),format.raw/*157.25*/("""Lobibox.notify('warning', """),format.raw/*157.51*/("""{"""),format.raw/*157.52*/("""
                            """),format.raw/*158.29*/("""msg: "Please select the workshop"
                        """),format.raw/*159.25*/("""}"""),format.raw/*159.26*/(""");
                        return false;

                    """),format.raw/*162.21*/("""}"""),format.raw/*162.22*/("""
                    """),format.raw/*163.21*/("""if (campaignStart == '') """),format.raw/*163.46*/("""{"""),format.raw/*163.47*/("""

                        """),format.raw/*165.25*/("""Lobibox.notify('warning', """),format.raw/*165.51*/("""{"""),format.raw/*165.52*/("""
                            """),format.raw/*166.29*/("""msg: "Please enter the start date of the Campaign"
                        """),format.raw/*167.25*/("""}"""),format.raw/*167.26*/(""");
                        return false;

                    """),format.raw/*170.21*/("""}"""),format.raw/*170.22*/("""
                    """),format.raw/*171.21*/("""if (campaignExpiryNew == '') """),format.raw/*171.50*/("""{"""),format.raw/*171.51*/("""

                        """),format.raw/*173.25*/("""Lobibox.notify('warning', """),format.raw/*173.51*/("""{"""),format.raw/*173.52*/("""
                            """),format.raw/*174.29*/("""msg: "Please enter the Expiry date of the Campaign"
                        """),format.raw/*175.25*/("""}"""),format.raw/*175.26*/(""");
                        return false;

                    """),format.raw/*178.21*/("""}"""),format.raw/*178.22*/("""
                 
                """),format.raw/*180.17*/("""}"""),format.raw/*180.18*/("""
            """),format.raw/*181.13*/("""}"""),format.raw/*181.14*/(""");

        """),format.raw/*183.9*/("""}"""),format.raw/*183.10*/(""");

    </script>"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,locationList:List[Location],campaignTypedata:String): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,locationList,campaignTypedata)

  def f:((String,String,String,List[Location],String) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,locationList,campaignTypedata) => apply(dealercode,dealerName,userName,locationList,campaignTypedata)

  def ref: this.type = this

}


}

/**/
object addCampaign extends addCampaign_Scope0.addCampaign
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:09 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/addCampaign.scala.html
                  HASH: f9280be1f116c0c485a36853d8acfbbfff62f83d
                  MATRIX: 793->1|994->106|1022->109|1084->163|1123->165|1151->168|1164->174|1226->228|1265->230|1295->234|1392->304|1420->305|1452->311|1516->348|1544->349|1580->359|1841->593|1869->594|1897->595|1928->599|1956->600|1984->601|2700->1289|2738->1305|3713->2253|3761->2285|3800->2286|3870->2328|3913->2344|3935->2357|3966->2367|3996->2370|4018->2383|4050->2393|4133->2445|4199->2483|4547->2804|4592->2839|4632->2840|4661->2849|4675->2853|4715->2854|4756->2863|6917->4993|6951->4996|6981->4998|7058->5046|7088->5047|7145->5075|7217->5118|7247->5119|7312->5155|7795->5608|7826->5609|7877->5631|7932->5657|7962->5658|8017->5684|8096->5734|8126->5735|8202->5782|8232->5783|8287->5809|8356->5849|8386->5850|8475->5910|8505->5911|8535->5912|8569->5917|8599->5918|8680->5970|8761->6022|8791->6023|8848->6051|8914->6088|8944->6089|8995->6111|9048->6135|9078->6136|9135->6164|9190->6190|9220->6191|9279->6221|9378->6291|9408->6292|9502->6357|9532->6358|9586->6383|9651->6419|9681->6420|9738->6448|9793->6474|9823->6475|9882->6505|9966->6560|9996->6561|10090->6626|10120->6627|10171->6649|10244->6693|10274->6694|10331->6722|10386->6748|10416->6749|10475->6779|10563->6838|10593->6839|10687->6904|10717->6905|10768->6927|10822->6952|10852->6953|10909->6981|10964->7007|10994->7008|11053->7038|11158->7114|11188->7115|11282->7180|11312->7181|11363->7203|11421->7232|11451->7233|11508->7261|11563->7287|11593->7288|11652->7318|11758->7395|11788->7396|11882->7461|11912->7462|11978->7499|12008->7500|12051->7514|12081->7515|12123->7529|12153->7530
                  LINES: 27->1|32->1|33->2|33->2|33->2|34->3|34->3|34->3|34->3|36->5|38->7|38->7|39->8|39->8|39->8|40->9|49->18|49->18|49->18|50->19|50->19|50->19|68->37|68->37|82->51|82->51|82->51|83->52|83->52|83->52|83->52|83->52|83->52|83->52|84->53|85->54|94->63|94->63|94->63|94->63|94->63|94->63|94->63|141->110|142->111|143->112|144->113|144->113|146->115|146->115|146->115|148->117|155->124|155->124|156->125|156->125|156->125|157->126|158->127|158->127|159->128|159->128|160->129|161->130|161->130|163->132|163->132|163->132|163->132|163->132|165->134|165->134|165->134|167->136|169->138|169->138|170->139|170->139|170->139|172->141|172->141|172->141|173->142|174->143|174->143|177->146|177->146|178->147|178->147|178->147|180->149|180->149|180->149|181->150|182->151|182->151|185->154|185->154|186->155|186->155|186->155|188->157|188->157|188->157|189->158|190->159|190->159|193->162|193->162|194->163|194->163|194->163|196->165|196->165|196->165|197->166|198->167|198->167|201->170|201->170|202->171|202->171|202->171|204->173|204->173|204->173|205->174|206->175|206->175|209->178|209->178|211->180|211->180|212->181|212->181|214->183|214->183
                  -- GENERATED --
              */
          