
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object dealerInfo_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class dealerInfo extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,List[models.Dealer],Form[Dealer],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealerName:String,userName:String,dealersData:List[models.Dealer],dealerform:Form[Dealer]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import models._
import helper._
import scala._

Seq[Any](format.raw/*1.93*/("""
"""),_display_(/*5.2*/main("AutoSherpaCRM",userName,dealerName)/*5.43*/ {_display_(Seq[Any](format.raw/*5.45*/("""
"""),_display_(/*6.2*/helper/*6.8*/.form(action = routes.DealerController.dealerInformation)/*6.65*/ {_display_(Seq[Any](format.raw/*6.67*/("""

"""),format.raw/*8.1*/("""<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">List of Dealer Information</h1>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"> List of Dealers </div>
      <div class="panel-body">
        <div class="dataTable_wrapper">
          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th colspan="2">Actions</th>
                <th>Dealer Id</th>
                <th>Dealer Name</th>
                <th>Dealer Address</th>
                <th>Dealer Code</th>
              </tr>
            </thead>
            <tbody>
            
            """),_display_(/*31.14*/for(post <- dealersData) yield /*31.38*/ {_display_(Seq[Any](format.raw/*31.40*/("""
            """),format.raw/*32.13*/("""<tr>
              <td><a href="/superAdmin/dealer/delete/"""),_display_(/*33.55*/post/*33.59*/.id),format.raw/*33.62*/(""""><i class="fa fa-trash-o"></i></a></td>
              <td><a href="/superAdmin/dealer/edit/"""),_display_(/*34.53*/post/*34.57*/.id),format.raw/*34.60*/(""""><i class="fa fa-pencil-square-o"></i></a></td>
              <td> """),_display_(/*35.21*/post/*35.25*/.dealerId),format.raw/*35.34*/("""</td>
              <td> """),_display_(/*36.21*/post/*36.25*/.dealerName),format.raw/*36.36*/("""</td>
              <td>"""),_display_(/*37.20*/post/*37.24*/.dealerAddress),format.raw/*37.38*/("""</td>
              <td>"""),_display_(/*38.20*/post/*38.24*/.dealerCode),format.raw/*38.35*/("""</td>
            </tr>
            """)))}),format.raw/*40.14*/("""
              """),format.raw/*41.15*/("""</tbody>
            
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
        """)))}),format.raw/*49.10*/("""

""")))}))
      }
    }
  }

  def render(dealerName:String,userName:String,dealersData:List[models.Dealer],dealerform:Form[Dealer]): play.twirl.api.HtmlFormat.Appendable = apply(dealerName,userName,dealersData,dealerform)

  def f:((String,String,List[models.Dealer],Form[Dealer]) => play.twirl.api.HtmlFormat.Appendable) = (dealerName,userName,dealersData,dealerform) => apply(dealerName,userName,dealersData,dealerform)

  def ref: this.type = this

}


}

/**/
object dealerInfo extends dealerInfo_Scope0.dealerInfo
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:25 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/dealerInfo.scala.html
                  HASH: 8e942f31e3d9181ac3ab35f797c277042ec9ae99
                  MATRIX: 795->1|1027->92|1055->148|1104->189|1143->191|1171->194|1184->200|1249->257|1288->259|1318->263|2115->1033|2155->1057|2195->1059|2237->1073|2324->1133|2337->1137|2361->1140|2482->1234|2495->1238|2519->1241|2616->1311|2629->1315|2659->1324|2713->1351|2726->1355|2758->1366|2811->1392|2824->1396|2859->1410|2912->1436|2925->1440|2957->1451|3027->1490|3071->1506|3215->1619
                  LINES: 27->1|34->1|35->5|35->5|35->5|36->6|36->6|36->6|36->6|38->8|61->31|61->31|61->31|62->32|63->33|63->33|63->33|64->34|64->34|64->34|65->35|65->35|65->35|66->36|66->36|66->36|67->37|67->37|67->37|68->38|68->38|68->38|70->40|71->41|79->49
                  -- GENERATED --
              */
          