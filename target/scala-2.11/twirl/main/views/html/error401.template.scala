
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object error401_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class error401 extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>AutoSherpa</title>
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />
<link rel="stylesheet" href=""""),_display_(/*9.31*/routes/*9.37*/.Assets.at("css/bootstrap.min.css")),format.raw/*9.72*/("""">
<link rel="stylesheet" href=""""),_display_(/*10.31*/routes/*10.37*/.Assets.at("css/AdminLTE.min.css")),format.raw/*10.71*/("""">
<link rel="stylesheet" href=""""),_display_(/*11.31*/routes/*11.37*/.Assets.at("css/font-awesome.min.css")),format.raw/*11.75*/("""">
  </head>
  <body class="error" style="background-color:#ecf0f5">
    <!-- <div class="container">
      <div class="col-lg-8 col-lg-offset-2 text-center">
        <div class="logo">
          <h1>401</h1>
        </div>
        <p class="lead text-muted">Oops, an error has occurred. unauthorized!</p>
        <div class="clearfix"></div>
        
        <div class="clearfix"></div>
        <br>
        <div class="col-lg-6 col-lg-offset-3">
          <div class="btn-group btn-group-justified">
            <a href="/" class="btn btn-warning">Home</a>
             </div>
        </div>
      </div>
    </div> -->
	<section class="content">
      <div class="">
        <h2 class="headline text-yellow"> </h2>

        <div class="col-lg-8 col-lg-offset-2 text-center">
          <h2><i class="fa fa-warning text-yellow"></i> Unauthorized access! </h2>

          <p>
            We could not find the page you were looking for.
            Meanwhile, you may <br /><a href="/"><b><u>Return To Home</u></b></a> 
          </p>

          
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
  </body>
</html>"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object error401 extends error401_Scope0.error401
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:26 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/error401.scala.html
                  HASH: 2f5aeb506880c72c7e8729b7c6a99f644b892bee
                  MATRIX: 833->2|1137->280|1151->286|1206->321|1267->355|1282->361|1337->395|1398->429|1413->435|1472->473
                  LINES: 32->2|39->9|39->9|39->9|40->10|40->10|40->10|41->11|41->11|41->11
                  -- GENERATED --
              */
          