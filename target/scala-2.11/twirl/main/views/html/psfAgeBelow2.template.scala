
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object psfAgeBelow2_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class psfAgeBelow2 extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.4*/():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.6*/("""
	
	"""),format.raw/*4.2*/("""<div style="display:none;" class="animated  bounceInRight" id="MahiYesLastQAging" >
	
			<p>Thank You Sir/Madam, Request you to rate the following questions on a scale of 1-10.Where 10 would be considered as Excellent. 9 would be considered as Good.</p>
			<p>8 and less than 8 would be considered as unacceptable and CCCF will be opened for the same.</p>
			<div class="row">
				<table class=" table table-responsive table-striped table-bordered">
        <thead>
            <tr>
                <th>Sl No.</th>
                <th>Questions</th>
                <th>Rating</th>
                
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>How would you rate on Overall  Appointment Process ?</td>
                <td > <select class="form-control input-sm " id="1stQSelect" name="qM1RatingOverAllAppointProcess">
        <option value="0">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
               
            </tr>
			
				<tr>
					<td>2</td>
					<td>How would you rate on  Time taken for  vehicle handover to SA/RM?</td>
					<td><select class="form-control input-sm" id="2ndQSelect" name="qM2RatingTimeTakenVehiclehandOver">
					<option value="0">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
				</tr>
				<tr>
					<td>3</td>
					<td>How would you rate on Service Advisor Courtesy & Response?</td>
					<td ><select class="form-control input-sm " id="3rdQSelect" name="qM3RatingServicAdvCourtesyResponse">
					<option value="0">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
				</tr>
				<tr>
					<td>4</td>
					<td>How would you Rate Service Advisor on Job  explanation , Time & Cost Estimate?</td>
					<td ><select class="form-control input-sm " id="4thQSelect" name="qM4RatingServiceAdvJobExpl">
					<option value="0">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
				</tr>
				<tr>
					<td>5</td>
					<td>How would you rate Cleanliness of Dealership & Comfort of Waiting Area (includes, seating, ameniites, refreshments) ?</td>
					<td ><select class="form-control input-sm " id="6thQSelect" name="qM5RatingCleanlinessOfDealer">
					<option value="0">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
				</tr>
				<tr>
					<td>6</td>
					<td>How would you rate Timeliness of Vehicle Delivery process after service?</td>
					<td ><select class="form-control input-sm" id="7thQSelect" name="qM6RatingTimelinessVehicleDelivary">
					<option value="0">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
				</tr>
				<tr>
					<td>7</td>
					<td>How would you rate on Fairness of charges?</td>
					<td ><select class="form-control input-sm" id="8thQSelect" name="qM7RatingFairnessOfCharge" >
					<option value="0">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
				</tr><tr>
					<td>8</td>
					<td>How would you rate helpfulness of staff  during  vehicle  delivery  process ?</td>
					<td ><select class="form-control input-sm" id="8thQSelect" name="qM8RatingHelpFulnessOfStaff">
					<option value="0">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
				</tr><tr>
					<td>9</td>
					<td>How would  you Rate on total time required to service your vehicle?</td>
					<td><select class="form-control input-sm" id="9thQSelect" name="qM9RatingTotalTimeRequired">
					<option value="0">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
				</tr>
				<tr>
					<td>10</td>
					<td>How would you rate the quality of maintenance/repair work performed?</td>
					<td ><select class="form-control input-sm" id="10thQSelect" name="qM10RatingQualityOfMaintenance">
					<option value="0">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
				</tr>
				<tr>
					<td>11</td>
					<td>How would you rate on the condition/cleanliness of your vehicle at the time of delivery?</td>
					<td><select class="form-control input-sm" id="11thQSelect" name="qM11RatingCondAndCleanlinessOfVeh">
					<option value="0">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
				</tr>
				<tr>
					<td>12</td>
					<td>How would you rate on  overall service experience?</td>
					<td><select class="form-control input-sm" id="12thQSelect" name="qM12RatingOverAllServExp">
					<option value="0">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
				</tr>
			</tbody>
			</table>
			 </div> 
             <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Remarks</label>
                      <textarea type="text" name="remarksList[2]" class="form-control" rows="2" id=""></textarea>
                    </div>
                  </div>
            
			 
					<div class="pull-right">
                        <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#IncompleteSurvey">Incomplete Survey</button>                      
					<button type="button" class="btn btn-info" id="BackTo3rdQuesMaAgeing">Back</button>	
                    <button type="button" class="btn btn-primary" id="nextToupsellBelow2">Next</button> 					
					<!-- <button type="submit" class="btn btn-info" id="ratingSubmit3rdDayM">Submit</button> -->
			</div>
			</div>



			
			"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object psfAgeBelow2 extends psfAgeBelow2_Scope0.psfAgeBelow2
              /*
                  -- GENERATED --
                  DATE: Sat Dec 16 16:03:44 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/psfAgeBelow2.scala.html
                  HASH: 92354260f7f806d05837fe14e6578d45887675e5
                  MATRIX: 752->8|848->10|880->16
                  LINES: 27->2|32->2|34->4
                  -- GENERATED --
              */
          