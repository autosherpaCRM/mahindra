
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object viewAllComplaints1_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class viewAllComplaints1 extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[String,String,String,List[String],List[Location],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(dealercode:String,dealerName:String,userName:String,cresList:List[String],locationList:List[Location],distinctFunctions:List[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.136*/("""

"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*4.56*/ {_display_(Seq[Any](format.raw/*4.58*/("""

"""),format.raw/*6.1*/("""<style>
 .accordion-toggle:after """),format.raw/*7.26*/("""{"""),format.raw/*7.27*/("""
  
	"""),format.raw/*9.2*/("""font-family:'FontAwesome';
	content:"\f077";
	float: right;
	color: inherit;
"""),format.raw/*13.1*/("""}"""),format.raw/*13.2*/("""
"""),format.raw/*14.1*/(""".panel-heading.collapsed .accordion-toggle:after """),format.raw/*14.50*/("""{"""),format.raw/*14.51*/("""
 
	"""),format.raw/*16.2*/("""content:"\f078";
"""),format.raw/*17.1*/("""}"""),format.raw/*17.2*/("""
"""),format.raw/*18.1*/(""".panel-primary>.panel-heading """),format.raw/*18.31*/("""{"""),format.raw/*18.32*/("""
	"""),format.raw/*19.2*/("""color: #fff;
	background-color: #0288D1;
	border-color: #0288D1;
"""),format.raw/*22.1*/("""}"""),format.raw/*22.2*/("""
"""),format.raw/*23.1*/(""".btn-info """),format.raw/*23.11*/("""{"""),format.raw/*23.12*/("""
	"""),format.raw/*24.2*/("""background-color: #03A9F4;
	border-color: #5B987C!important;
	"""),format.raw/*26.2*/("""}"""),format.raw/*26.3*/("""
	
"""),format.raw/*28.1*/(""".panel-group .panel-collapse """),format.raw/*28.30*/("""{"""),format.raw/*28.31*/("""
    """),format.raw/*29.5*/("""margin-top: 0px !important;
"""),format.raw/*30.1*/("""}"""),format.raw/*30.2*/("""
"""),format.raw/*31.1*/(""".panel-group .panel-heading """),format.raw/*31.29*/("""{"""),format.raw/*31.30*/("""
    """),format.raw/*32.5*/("""padding: 5px;
"""),format.raw/*33.1*/("""}"""),format.raw/*33.2*/("""
"""),format.raw/*34.1*/("""</style>

"""),_display_(/*36.2*/helper/*36.8*/.form(action = routes.CallInteractionController.downloadExcelFile)/*36.74*/ {_display_(Seq[Any](format.raw/*36.76*/("""
"""),format.raw/*37.1*/("""<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading" style="text-align:center;"><b>COMPLAINT HISTORY</b></div>
			<div class="panel-body">
			
				<div class="col-md-2">
				<label>Location</label>
				<select class="form-control" id="selLocId" name="location">
					"""),_display_(/*46.7*/for(loc <- locationList) yield /*46.31*/{_display_(Seq[Any](format.raw/*46.32*/("""						
						"""),format.raw/*47.7*/("""<option value=""""),_display_(/*47.23*/loc/*47.26*/.getName()),format.raw/*47.36*/("""">"""),_display_(/*47.39*/loc/*47.42*/.getName()),format.raw/*47.52*/("""</option>
					""")))}),format.raw/*48.7*/("""
				"""),format.raw/*49.5*/("""</select>
				</div>
				<div class="col-md-2">
				<label>Function</label>
				<select class="form-control" id="selFuncId" name="functionName">
							<option value="0">Select Function</option>
							"""),_display_(/*55.9*/for(func <- distinctFunctions) yield /*55.39*/{_display_(Seq[Any](format.raw/*55.40*/("""						
						"""),format.raw/*56.7*/("""<option value=""""),_display_(/*56.23*/func),format.raw/*56.27*/("""">"""),_display_(/*56.30*/func),format.raw/*56.34*/("""</option>
					""")))}),format.raw/*57.7*/("""	
				"""),format.raw/*58.5*/("""</select>
				</div>
				<div class="col-md-2">
				<label>Start Date</label>
				<input type="text" class="datepicker form-control" id="raisedDateId" name="raisedDate" readonly/>
					
				
				</div>
				<div class="col-md-2">
				<label>End Date</label>
				<input type="text" class="datepicker form-control" id="endDateId" name="endDate" readonly/>
					
				
				</div>
				<div class="col-md-2">
				<label>Status</label>
				<select class="form-control" id="complainHistoryID" name="complaintStatus">
					<option value="0">--Select--</option>
					<option value="New">NEW COMPLAINT</option>
					<option value="Assigned">ASSIGNED COMPLAINT</option>
					<option value="Closed">CLOSED COMPLAINT</option>
				</select>
				</div>
				<div class="col-md-1">
				<div class="form-group">
				<label></label>
				<button type="button" class="btn btn-success form-control" onclick="viewComplaintData();">SUBMIT</button>
				</div>
			</div>
			
			<div class="col-md-1">
				<div class="form-group">
				<label></label>
				<button type="submit" class="btn btn-success form-control">DOWNLOAD</button>
				</div>
			</div> 
		</div>
	</div>
</div>
</div>



<div class="row" id="openComplaintID">
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading nameappend" style="text-align:center;"> <b>COMPLAINTS LIST</b> </div>
		<div class="panel-body">
		<div class="dataTable_wrapper">  
	
<table  class="table table-striped table-bordered table-hover" style="width:100%" cellspacing="0" id="ComplaintID">
        <thead>
            <tr>

            <th>COMPLAINT No</th>
            <th>ISSUE DATE</th>             
               <th>AGING (Days)</th>
                 <th>REGISTERED BY</th>
                  <th>SOURCE</th>
                 <th>FUNCTION</th>               
                     <th>VEHICLE No</th>
                      <th>VIEW</th>  
            </tr>
        </thead>
       
         <tbody>
               
             </tbody>
            </table>
            </div>
               
    </div>
  </div>
</div>
</div>
  <!-- Open Complain Popup -->
  <div class="modal fade" id="OpenComplaintPopup" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
          
          
<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-heading" style="text-align: center;"><b>OPEN COMPLAINT RESOLUTION FORM<b></div>
        <div id="" class="panel">
		
            <div class="panel-body">
            <h4 style="color: #E80B0B;"><b>CUSTOMER DETAILS :<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				
                    <tr>
                      <td><strong> VEHICLE NUMBER*:</strong></td>
                      <td id="veh_num_pop" class="text-primary"></td>
                    </tr>
                   
                    <tr>
                      <td><strong>CUSTOMER NAME:</strong></td>
                      <td id="cust_name_pop" class="text-primary"></td>
                    </tr>
                 
                     <tr>
                       <td><strong>MOBILE No: </strong></td>
                      <td id="cust_phone_pop" class="text-primary"></td>
                    </tr>
					 
					  <tr>
                      <td><strong>EMAIL: </strong></td>
                      <td id="cust_email" class="text-primary"></td>
                    </tr> 
				
					  <tr>
                      <td><strong>ADDRESS: </strong></td>
                      <td id="addressName" class="text-primary"></td>
                    </tr>
					
						  <tr>
                      <td><strong>CHASSIS No: </strong></td>
                      <td id="chassisno" class="text-primary"></td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				   
                  <tr>
                      <td><strong>MODEL:</strong></td>
                      <td id="model" class="text-primary"></td>
                    </tr>
					   <tr>
                      <td><strong>VARIANT: </strong></td>
                      <td id="variant" class="text-primary"></td>
                    </tr>
					 <tr>
                      <td><strong>SALE DATE: </strong></td>
                      <td id="saledate" class="text-primary"></td>
                    </tr>
						<tr>
                      <td><strong>LAST SERVICE: </strong></td>
                      <td id="lastserv" class="text-primary"></td>
                    </tr>
						  <tr>
                      <td><strong>SERVICE ADVISOR : </strong></td>
                      <td id="serviceadvisor" class="text-primary"></td>
                    </tr>
						  <tr>
                      <td><strong>WORKSHOP: </strong></td>
                      <td id="workshop" class="text-primary"></td>
                    </tr>
					
					
                    
                  </tbody>
                </table>
                </div>
                           
              </div>
                <h4 style="color: #E80B0B;"><b>COMPLAINT INFORMATION:<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                 <tbody>
 <tr>
                      <td><strong> COMPLAINT NUMBER</strong></td>
                      <td id="compnum" class="text-primary"> </td>
                    </tr>
                     <tr>
                      <td><strong> COMPLAINT STATUS</strong></td>
                      <td id="compstatus" class="text-primary"> </td>
                    </tr>
                    <tr>
                      <td><strong> FUNCTION:</strong></td>
                      <td id="function" class="text-primary"> </td>
                    </tr>
                   
                    <tr>
                      <td><strong>CATEGORY:</strong></td>
                      <td id="complaintType" class="text-primary"></td>
                    </tr>
            <tr>
                      <td rowspan="2"><strong>DESCRIPTION:</strong></td>
                      <td  id="description" class="text-primary"></td>
                    </tr>
                 
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
   
                  <tr>
                      <td><strong>SOURCE:</strong></td>
                      <td id="source"class="text-primary">  </td>
                    </tr>
                    <tr>
                      <td><strong>SUB CATEGORY 1: </strong></td>
                      <td id="subcomplaintType"class="text-primary"></td>
                    </tr>


                    
                  </tbody>

                </table>
                </div>
                           
              </div>
						<div class="row">
<div class="col-lg-12">
	<div class="panel panel-primary">
  <div class="panel-heading" align="center"  onclick="trys();" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne4" style="cursor:pointer;">
		<h4 class="panel-title accordion-toggle"></h4>
		<img src=""""),_display_(/*282.14*/routes/*282.20*/.Assets.at("images/INTERACTION HISTORY.png")),format.raw/*282.64*/("""" style="width:17px;"/>&nbsp;<b>COMPLAINTS  HISTORY</b>
		 </div>
		 <div id="collapseOne4"  class="panel-collapse collapse"
		<div class="panel-body">
		<div class="row">
                    <table class="table table-border table-responsive" id="ComplaintHistory" style="display:none">
		
		
				</table>
		
                        </div>
		</div>
		</div>
		</div>
		</div>
		</br>
						
						
                        <div class="row">
                            <div class="col-lg-12" style="">

<input type="button" class="btn btn-info pull-right Resolve_It" id="Resolve_It" value="Resolve It">
<input type="button" class="btn btn-info" id="Reassign_It" value="Assign It">
 
                             
                </div>
                </div>
              
           <div class="row" style="display:none" id="Resolve_compID">
              
                                <h4 class="modal-title" style="text-align:center; color:red;"><b> COMPLAINT RESOLUTION</b></h4>  
              	 <div class="col-lg-3">
               <div class="form-group">
                <label for="reasonfor">REASON FOR </label>
                <input type="text" id="reasonfor"class="form-control" name="reasonfor"  autocomplete="off" />
              </div>
            </div>
           <div class="col-lg-3">
               <div class="form-group">
                <label for="complaintStatus">COMPLAINT STATUS</label>
                
                <select id="complaintStatus" class="form-control" name="complaintStatus">
                    
                    <option value="Closed">Closed</option>
                     <option value="Open">Open</option>
                    
                </select>
              </div>
            </div>
             <div class="col-lg-3">
               <div class="form-group">
                <label for="customerStatus">CUSTOMER STATUS</label>
                
                 <select id="customerStatus" class="form-control" name="customerStatus">
                    
                    <option value="Satisfied">Satisfied</option>
                     <option value="Dissatisfied">Dissatisfied</option>
                    
                </select>
              </div>
            </div>
            
             <div class="col-lg-3">
               <div class="form-group">
                <label for="actionTaken">ACTION TAKEN </label>
                <input type="text" id="actionTaken"class="form-control" name="actionTaken"   autocomplete="off" />
              </div>
            </div>
            
             <div class="col-lg-3">
               <div class="form-group">
                <label for="resolutionBy">RESOLUTION By: </label>
                <input type="text" id="resolutionBy"class="form-control" name="resolutionBy" value=""""),_display_(/*354.102*/userName),format.raw/*354.110*/("""" autocomplete="off" />
              </div>
            </div>
                  <div class="text-right">
  <button type="button" id="resolve_savebymanager" class="btn btn-success resolve_savebymanager" data-dismiss="modal">Update</button>

          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
           
              </div>
                
           		<div class="row" style="display:none;" id="reassign_compID">
                                <h4 class="modal-title" style="text-align:center; color:red;"><b>ASSIGNMENT COMPLAINT</b></h4>  

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">CITY </label>
                            <select id="city" class="form-control" onchange="ajaxCallToLoadWorkShopByCityComplaints(); ajaxCallToLoadFunctionsByLocation();">
                                <option value="0" >--Select--</option>
                                """),_display_(/*373.34*/for(location_list<-locationList) yield /*373.66*/{_display_(Seq[Any](format.raw/*373.67*/("""
                                
                              
                                """),format.raw/*376.33*/("""<option value=""""),_display_(/*376.49*/location_list/*376.62*/.getName()),format.raw/*376.72*/("""">"""),_display_(/*376.75*/location_list/*376.88*/.getName()),format.raw/*376.98*/("""</option>
                              
                             """)))}),format.raw/*378.31*/("""
                              """),format.raw/*379.31*/("""</select>
                        </div>
                    </div>
                <div class="col-lg-3">
                        <div class="form-group">
                            <label for="workshop">LOCATION </label>
                            <select id="workshopSelected" class="form-control">
                                <option value="select" >Select</option>
                                
                              </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">FUNCTION </label>
                            <select id="FUNCTION" class="form-control" onchange="ajaxLoadUserByFunc();">
                                <option value="0">--Select--</option>
                                                         
                              </select>
                        </div>
                    </div>
					

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">OWNERSHIP </label>
                            
                            <select id="OWNERSHIP" class="form-control">
                                
                                <option value="0">--Select--</option>
                               
                              </select>
                             
                              
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">PRIORITY </label>
                          
                            <select id="PRIORITY" class="form-control">
                                <option value="0">--Select--</option>
                                <option value="2">High</option>
                                <option value="3" selected="selected">Medium</option>
                                <option value="4">Low</option>
                              </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">ESCALATION 1 </label>
                            <input type="text" class="form-control" name="ESCALATION1"  id="ESCALATION1" autocomplete="off" />
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">ESCALATION 2 </label>
                            <input type="text" class="form-control" name="ESCALATION2"  id="ESCALATION2" autocomplete="off" />
                        </div>
                    </div>
                    
                        <div class="text-right">
                    <button class="btn btn-success AddComplaintResolution" type="button" >Submit</button>
                    <button type="button" class="btn btn-danger"  data-dismiss="modal">Close</button>
                </div>
                </div>
		
            </div>
        </div>
    </div>
    


       
    </div>
        </div>
        
      </div>
      
    </div>
  </div>
  
  """)))}),format.raw/*461.4*/("""

 """),format.raw/*463.2*/("""<script>


function  getcomplaintnum(complaintNum,veh_num)"""),format.raw/*466.48*/("""{"""),format.raw/*466.49*/("""
	"""),format.raw/*467.2*/("""//alert();
		
		 $.blockUI();
        var urlDisposition = "/CRE/searchcomplaintNum/" + complaintNum + "/" + veh_num +"";
		
        $.ajax("""),format.raw/*472.16*/("""{"""),format.raw/*472.17*/("""
               """),format.raw/*473.16*/("""url: urlDisposition

    """),format.raw/*475.5*/("""}"""),format.raw/*475.6*/(""").done(function (data) """),format.raw/*475.29*/("""{"""),format.raw/*475.30*/("""
        """),format.raw/*476.9*/("""if (data != null) """),format.raw/*476.27*/("""{"""),format.raw/*476.28*/("""
        """),format.raw/*477.9*/("""console.log(data);
      console.log(complaintNum);
           
      $.unblockUI();   
        $("#veh_num_pop").html(data.vehicleRegNo);           
           $("#cust_phone_pop").html(data.customerMobileNo);      
           $("#cust_name_pop").html(data.customerName);
           $("#cust_email").html(data.customerEmail);      
            $("#serviceadvisor").html(data.serviceadvisor);
            $("#addressName").html(data.preferredAdress);
           $("#chassisno").html(data.chassisNo);
          $("#saledate").html(data.saleDate);
           $("#variant").html(data.variant);
           $("#model").html(data.model);
             $("#function").html(data.functionName);
           $("#compnum").html(data.complaintNumber);
           $("#compstatus").html(data.complaintStatus);
         $("#serviceadvisor").html(data.serviceadvisor);
          $("#source").html(data.sourceName);
        $("#description").html(data.description);
         $("#subcomplaintType").html(data.subcomplaintType);
          $("#complaintType").html(data.complaintType);
           $("#function").html(data.functionName);
            $("#assignOwnership").html(data.assignedUser);
             $("#prorityName").html(data.priority);
              $("#cityName").html(data.location);
              $("#lastserv").html(data.lastServiceDate);
              $("#workshop").html(data.workshop);
             
             // alert(data.chassisNo);

 """),format.raw/*508.2*/("""}"""),format.raw/*508.3*/("""
          
"""),format.raw/*510.1*/("""}"""),format.raw/*510.2*/(""");
          $('#OpenComplaintPopup').modal("""),format.raw/*511.42*/("""{"""),format.raw/*511.43*/("""
        """),format.raw/*512.9*/("""show: 'true'
    """),format.raw/*513.5*/("""}"""),format.raw/*513.6*/("""); 
"""),format.raw/*514.1*/("""}"""),format.raw/*514.2*/("""
 """),format.raw/*515.2*/("""function trys() """),format.raw/*515.18*/("""{"""),format.raw/*515.19*/("""
	 """),format.raw/*516.3*/("""$.blockUI();
$("#ComplaintHistory").toggle();
     var complaintNumber = $('#compnum').text();
     var vehregnumber = $('#veh_num_pop').text();
            var urlDisposition="/CREManager/getComplaintHistoryAll/"+complaintNumber+"/"+vehregnumber+"";
	$.ajax("""),format.raw/*521.9*/("""{"""),format.raw/*521.10*/("""
		"""),format.raw/*522.3*/("""url:urlDisposition 
	"""),format.raw/*523.2*/("""}"""),format.raw/*523.3*/(""").done(function(data)"""),format.raw/*523.24*/("""{"""),format.raw/*523.25*/("""
            """),format.raw/*524.13*/("""//console.log("test");
            //console.log(data);
	$.unblockUI();
           var tableHeaderRowCount = 1;
            var table = document.getElementById('ComplaintHistory');
                          	$("#ComplaintHistory").find("tr").remove();
                        $('#ComplaintHistory tbody tr').empty();

           th = $('<tr>');
                th.append('<th>COMPLAINT No</th>');
                th.append('<th>COMPLAINT STATUS</th>');
                th.append('<th>ISSUE DATE</th>');                
                th.append('<th>AGING (Days)</th>');
                th.append('<th>REGISTERED BY</th>');
                th.append('<th>SOURCE</th>');
                th.append('<th>FUNCTION</th>');
                th.append('<th>VEHICLE No</th>');
               // th.append('<th>RESOLVED BY</th>');
                $('#ComplaintHistory').append(th);
  
            for (i = 0; i < data.length; i++) """),format.raw/*544.47*/("""{"""),format.raw/*544.48*/("""


           """),format.raw/*547.12*/("""var     complaintNum =      data[i].complaintNumber;
               var     complaintStatus =      data[i].complaintStatus;
              var     issueDate =       data[i].issueDate;
             var     ageOfComplaint =        data[i].ageOfComplaint;
             var     wyzUser =       data[i].wyzUser;
               var     sourceName =      data[i].sourceName;
             var     functionName =        data[i].functionName;
           var vehregno = data[i].vehicleRegNo;
			var resolBy=data[i].resolutionBy;
            
                tr = $('<tr/>');
                    tr.append('<td id="complaintNum">'+complaintNum+'</td>');
                      tr.append('<td id="compstatus">'+complaintStatus+'</td>');
                 tr.append('<td id="issueDateS">'+issueDate+'</td>');
                  tr.append('<td id="ageOfComplaint">'+ageOfComplaint+'</td>');
                   tr.append('<td id="wyzUser">'+wyzUser+'</td>');
                   tr.append('<td id="sourceName">'+sourceName+'</td>'); 
                   tr.append('<td id="functionName">'+functionName+'</td>');
                   tr.append('<td id="vehregno">'+vehregno+'</td>');
                  // tr.append('<td id="resolBy">'+resolBy+'</td>');
                
                $('#ComplaintHistory').append(tr); 
            """),format.raw/*569.13*/("""}"""),format.raw/*569.14*/("""
            
    """),format.raw/*571.5*/("""}"""),format.raw/*571.6*/(""");
            
    """),format.raw/*573.5*/("""}"""),format.raw/*573.6*/("""
"""),format.raw/*574.1*/("""function viewComplaintData() """),format.raw/*574.30*/("""{"""),format.raw/*574.31*/("""
	"""),format.raw/*575.2*/("""// $.blockUI();
     var filterData = document.getElementById('complainHistoryID').value;

    var varLoc=document.getElementById('selLocId').value;
    var varfunc=document.getElementById('selFuncId').value;
    var varraisedDate=document.getElementById('raisedDateId').value;
    var varendDate = document.getElementById('endDateId').value;
    var excelFileName=varLoc+"_"+filterData;

    if(varraisedDate=="")"""),format.raw/*584.26*/("""{"""),format.raw/*584.27*/("""
    	"""),format.raw/*585.6*/("""varraisedDate="0";
        """),format.raw/*586.9*/("""}"""),format.raw/*586.10*/("""

    """),format.raw/*588.5*/("""if(varendDate=="")"""),format.raw/*588.23*/("""{"""),format.raw/*588.24*/("""
    	"""),format.raw/*589.6*/("""varendDate="0";
        """),format.raw/*590.9*/("""}"""),format.raw/*590.10*/("""
   """),format.raw/*591.4*/("""// alert("varfunc : "+varfunc+" varraisedDate : "+varraisedDate+" varLoc : "+varLoc );
    var complainHistoryID = $("#complainHistoryID").text();
    var urlDisposition="/CREManager/getComplaintsDataByFilter/"+filterData+"/"+varLoc+"/"+varfunc+"/"+varraisedDate+"/"+varendDate;

    if ($.fn.DataTable.isDataTable( '#ComplaintID' ) ) """),format.raw/*595.56*/("""{"""),format.raw/*595.57*/("""
    	"""),format.raw/*596.6*/("""var table = $('#ComplaintID').DataTable();
    	table.clear();table.destroy();
    	"""),format.raw/*598.6*/("""}"""),format.raw/*598.7*/("""

    	"""),format.raw/*600.6*/("""$("#ComplaintID thead").find("tr").remove();

    	th=$('<tr />');
    	th.append('<th>COMPLAINT NO</th>');
    	th.append('<th>ISSUE DATE</th>');
    	th.append('<th>AGING (Days)</th>');
    	th.append('<th>REGISTERED BY</th>');
    	th.append('<th>SOURCE</th>');
    	th.append('<th>FUNCTION</th>');
    	th.append('<th>VEHICLE NO</th>');
    	th.append('<th>VIEW</th>');
    	
    	$('#ComplaintID thead').append(th);

    	$('#ComplaintID').dataTable( """),format.raw/*614.35*/("""{"""),format.raw/*614.36*/("""
    		
    		  """),format.raw/*616.9*/(""""ajax": """),format.raw/*616.17*/("""{"""),format.raw/*616.18*/("""
    		    """),format.raw/*617.11*/(""""url": urlDisposition,
    		    "dataSrc": function ( json ) """),format.raw/*618.40*/("""{"""),format.raw/*618.41*/("""
    		      """),format.raw/*619.13*/("""for ( var i=0,ien=json.length ; i<ien ; i++ ) """),format.raw/*619.59*/("""{"""),format.raw/*619.60*/("""
    		    	 

    		
    		"""),format.raw/*623.7*/("""json[i][0] =  json[i].complaintNumber;
    		json[i][1] =  json[i].issueDate;
    		json[i][2] =  json[i].ageOfComplaint;
    		json[i][3] =  json[i].wyzUser;
    		json[i][4] =  json[i].sourceName;
    		json[i][5] =  json[i].functionName;
    		json[i][6] =  json[i].vehicleRegNo;
    		json[i][7] = '<span style="color:blue" onclick=\"getcomplaintnum(\''+json[i].complaintNumber+'\',\''+json[i].vehicleRegNo+'\') \" class=\"getModel getModal viewComplaints viewComplaintsresolve\"> view</span>';
    		
    		

    		      """),format.raw/*634.13*/("""}"""),format.raw/*634.14*/("""
    		     
    		      """),format.raw/*636.13*/("""return json;
    		    """),format.raw/*637.11*/("""}"""),format.raw/*637.12*/("""
    		  """),format.raw/*638.9*/("""}"""),format.raw/*638.10*/(""",
    		  			"searching":false,
    		        	"scrollY": "350px",
    		        	"scrollX":"auto",
    		            dom: '<"html5buttons"B>lTfgitp',
    		             buttons: [
    		                """),format.raw/*644.23*/("""{"""),format.raw/*644.24*/(""" """),format.raw/*644.25*/("""extend: 'copy' """),format.raw/*644.40*/("""}"""),format.raw/*644.41*/(""",
    		                """),format.raw/*645.23*/("""{"""),format.raw/*645.24*/(""" """),format.raw/*645.25*/("""extend: 'csv' ,title:excelFileName"""),format.raw/*645.59*/("""}"""),format.raw/*645.60*/(""",
    		                """),format.raw/*646.23*/("""{"""),format.raw/*646.24*/(""" """),format.raw/*646.25*/("""extend: 'excel', title: excelFileName """),format.raw/*646.63*/("""}"""),format.raw/*646.64*/(""",
    		               ],
    		               "fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*648.78*/("""{"""),format.raw/*648.79*/("""
    		                   """),format.raw/*649.26*/("""$('td', nRow).attr('nowrap','nowrap');
    		                   return nRow;
    		                   """),format.raw/*651.26*/("""}"""),format.raw/*651.27*/("""              
    		"""),format.raw/*652.7*/("""}"""),format.raw/*652.8*/(""" """),format.raw/*652.9*/(""");



	    $('#Reassign_It').click(function()"""),format.raw/*656.40*/("""{"""),format.raw/*656.41*/("""
	            """),format.raw/*657.14*/("""$("#reassign_compID").show();
	            $("#Resolve_compID").hide();
	    """),format.raw/*659.6*/("""}"""),format.raw/*659.7*/(""");
	    $('#Resolve_It').click(function()"""),format.raw/*660.39*/("""{"""),format.raw/*660.40*/(""" 
	          """),format.raw/*661.12*/("""$("#Resolve_compID").show();
	            $("#reassign_compID").hide();
	    """),format.raw/*663.6*/("""}"""),format.raw/*663.7*/(""");
		
    
	
    """),format.raw/*667.5*/("""}"""),format.raw/*667.6*/("""
	
"""),format.raw/*669.1*/("""</script>
""")))}),format.raw/*670.2*/("""


"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,cresList:List[String],locationList:List[Location],distinctFunctions:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,cresList,locationList,distinctFunctions)

  def f:((String,String,String,List[String],List[Location],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,cresList,locationList,distinctFunctions) => apply(dealercode,dealerName,userName,cresList,locationList,distinctFunctions)

  def ref: this.type = this

}


}

/**/
object viewAllComplaints1 extends viewAllComplaints1_Scope0.viewAllComplaints1
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:41 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/viewAllComplaints1.scala.html
                  HASH: ee9772e1e82dcf851c7a91a87a3604bc2221b087
                  MATRIX: 826->3|1056->137|1086->142|1148->196|1187->198|1217->202|1278->236|1306->237|1339->244|1447->325|1475->326|1504->328|1581->377|1610->378|1643->384|1688->402|1716->403|1745->405|1803->435|1832->436|1862->439|1957->507|1985->508|2014->510|2052->520|2081->521|2111->524|2202->588|2230->589|2262->594|2319->623|2348->624|2381->630|2437->659|2465->660|2494->662|2550->690|2579->691|2612->697|2654->712|2682->713|2711->715|2750->728|2764->734|2839->800|2879->802|2908->804|3267->1137|3307->1161|3346->1162|3387->1176|3430->1192|3442->1195|3473->1205|3503->1208|3515->1211|3546->1221|3593->1238|3626->1244|3861->1453|3907->1483|3946->1484|3987->1498|4030->1514|4055->1518|4085->1521|4110->1525|4157->1542|4191->1549|12251->9581|12267->9587|12333->9631|15242->12511|15273->12519|16344->13562|16393->13594|16433->13595|16562->13695|16606->13711|16629->13724|16661->13734|16692->13737|16715->13750|16747->13760|16852->13833|16913->13865|20414->17335|20447->17340|20537->17401|20567->17402|20598->17405|20772->17550|20802->17551|20848->17568|20903->17595|20932->17596|20984->17619|21014->17620|21052->17630|21099->17648|21129->17649|21167->17659|22664->19128|22693->19129|22735->19143|22764->19144|22838->19189|22868->19190|22906->19200|22952->19218|22981->19219|23014->19224|23043->19225|23074->19228|23119->19244|23149->19245|23181->19249|23473->19513|23503->19514|23535->19518|23585->19540|23614->19541|23664->19562|23694->19563|23737->19577|24707->20518|24737->20519|24783->20536|26143->21867|26173->21868|26221->21888|26250->21889|26300->21911|26329->21912|26359->21914|26417->21943|26447->21944|26478->21947|26930->22370|26960->22371|26995->22378|27051->22406|27081->22407|27117->22415|27164->22433|27194->22434|27229->22441|27282->22466|27312->22467|27345->22472|27713->22811|27743->22812|27778->22819|27892->22905|27921->22906|27958->22915|28457->23385|28487->23386|28533->23404|28570->23412|28600->23413|28641->23425|28733->23488|28763->23489|28806->23503|28881->23549|28911->23550|28971->23582|29537->24119|29567->24120|29623->24147|29676->24171|29706->24172|29744->24182|29774->24183|30012->24392|30042->24393|30072->24394|30116->24409|30146->24410|30200->24435|30230->24436|30260->24437|30323->24471|30353->24472|30407->24497|30437->24498|30467->24499|30534->24537|30564->24538|30698->24643|30728->24644|30784->24671|30917->24775|30947->24776|30997->24798|31026->24799|31055->24800|31133->24849|31163->24850|31207->24865|31314->24944|31343->24945|31414->24987|31444->24988|31487->25002|31594->25081|31623->25082|31672->25103|31701->25104|31734->25109|31777->25121
                  LINES: 27->2|32->2|34->4|34->4|34->4|36->6|37->7|37->7|39->9|43->13|43->13|44->14|44->14|44->14|46->16|47->17|47->17|48->18|48->18|48->18|49->19|52->22|52->22|53->23|53->23|53->23|54->24|56->26|56->26|58->28|58->28|58->28|59->29|60->30|60->30|61->31|61->31|61->31|62->32|63->33|63->33|64->34|66->36|66->36|66->36|66->36|67->37|76->46|76->46|76->46|77->47|77->47|77->47|77->47|77->47|77->47|77->47|78->48|79->49|85->55|85->55|85->55|86->56|86->56|86->56|86->56|86->56|87->57|88->58|312->282|312->282|312->282|384->354|384->354|403->373|403->373|403->373|406->376|406->376|406->376|406->376|406->376|406->376|406->376|408->378|409->379|491->461|493->463|496->466|496->466|497->467|502->472|502->472|503->473|505->475|505->475|505->475|505->475|506->476|506->476|506->476|507->477|538->508|538->508|540->510|540->510|541->511|541->511|542->512|543->513|543->513|544->514|544->514|545->515|545->515|545->515|546->516|551->521|551->521|552->522|553->523|553->523|553->523|553->523|554->524|574->544|574->544|577->547|599->569|599->569|601->571|601->571|603->573|603->573|604->574|604->574|604->574|605->575|614->584|614->584|615->585|616->586|616->586|618->588|618->588|618->588|619->589|620->590|620->590|621->591|625->595|625->595|626->596|628->598|628->598|630->600|644->614|644->614|646->616|646->616|646->616|647->617|648->618|648->618|649->619|649->619|649->619|653->623|664->634|664->634|666->636|667->637|667->637|668->638|668->638|674->644|674->644|674->644|674->644|674->644|675->645|675->645|675->645|675->645|675->645|676->646|676->646|676->646|676->646|676->646|678->648|678->648|679->649|681->651|681->651|682->652|682->652|682->652|686->656|686->656|687->657|689->659|689->659|690->660|690->660|691->661|693->663|693->663|697->667|697->667|699->669|700->670
                  -- GENERATED --
              */
          