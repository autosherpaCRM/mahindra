
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object insuranceOutBoundPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class insuranceOutBoundPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template10[List[String],List[InsuranceAgent],List[Workshop],Customer,Vehicle,List[Location],CallInteraction,List[ServiceTypes],WyzUser,Service,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.4*/(statesList:List[String],insurAgent:List[InsuranceAgent],workshopList:List[Workshop],customerData:Customer,vehicleData:Vehicle,locationList:List[Location],interOfCall:CallInteraction,servicetypeList:List[ServiceTypes],userData:WyzUser,latestService:Service):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.261*/("""
	 
	 """),format.raw/*3.3*/("""<input type="hidden" name="selectedFile" value=""""),_display_(/*3.52*/{if(interOfCall.getInsuranceDisposition()!=null){interOfCall.getInsuranceDisposition().getCallDispositionData().getDisposition()}}),format.raw/*3.182*/("""">  
		  <div style="display:none;" class="animated  bounceInRight" id="serviceBookDiv">
		  
	  
<!-- BOOK MY SERVICE -->		
	  <div class="col-md-12">
			  <div class="row">
				<div class="col-md-3">
				  <div class="form-group">
					<label for="">Select Vehicle</label>
					<select class="form-control" id="vehicle" name="vehicleId_SB">
					  
						"""),_display_(/*15.8*/for(post <- customerData.getVehicles()) yield /*15.47*/{_display_(Seq[Any](format.raw/*15.48*/("""
                              
                              """),format.raw/*17.31*/("""<option value=""""),_display_(/*17.47*/post/*17.51*/.vehicle_id),format.raw/*17.62*/("""">"""),_display_(/*17.65*/post/*17.69*/.vehicleRegNo),format.raw/*17.82*/("""</option>
                              
									""")))}),format.raw/*19.11*/("""
					  
					  
					  
						
					"""),format.raw/*24.6*/("""</select>
				  </div>
				  </div>
				  <div class="col-md-3">
				  <div class="form-group">
					<label for="">Select City</label>
					<select class="form-control" id="city" name="cityName">					  
							
                              
                              <option value=""""),_display_(/*33.47*/{userData.getLocation().getName()}),format.raw/*33.81*/("""">"""),_display_(/*33.84*/{userData.getLocation().getName()}),format.raw/*33.118*/("""</option>
                              
									
					</select>
				  </div>
				</div>
				
					<div class="col-md-3">
				  <label for="">Renewal Type</label>
				  <select class="form-control"  name="renewalType" onchange="">
						<option value="0">--Select--</option>
						<option value="1st Renewal">1st Renewal</option>
						<option value="2nd Renewal">2nd Renewal</option>
						<option value="3rd Renewal">3rd Renewal</option>
						<option value="4th Renewal">4th Renewal</option>
						<option value="5th Renewal">5th Renewal</option>
						<option value="6th Renewal">6th Renewal</option>
						<option value="7th Renewal">7th Renewal</option>
						<option value="8th Renewal">8th Renewal</option>
						
						
						
						

				 </select>
				</div>
				<div class="col-md-3">
				  <label for="">Renewal Mode</label>
				  <select class="form-control" id="" name="renewalMode">
					<option value='0'>--Select--</option>
					<option value='PI'>Preferred INS Partner</option>
					<option value='NPI'>Non-Preferred Partner</option>
					
					
				
				  </select>
				</div>
				
				
				</div>
				 </div>
			  
			
		   <div class="col-md-12">
			  <div class="row">
			  <div class="col-md-3">	  
			  
				  <div class="form-group">
					<label class="control-label" for="inputGroup">Appointment Date<i class="lblStar">*</i></label>
					
					<input type="text" name="appointmentDate" class="form-control" id="insdate12345" readonly>					 
					 
					 
				  </div>
				</div>
				<div class="col-md-3">
					<div class="from-group">
						<label class="control-label" for="inputGroup">Appointment Time<i class="lblStar">*</i></label>
				<input type="text" name="appointmentFromTime" class="timepicker_7 form-control" id="appointmentTime" readonly>

					
					</div>
				</div>
			
		 <div class="col-md-3">
				  <div class="form-group">
				  <label>&nbsp;</label><br/>
					<button type="button" class="btn btn-primary btn-block" onClick="return ajaxAutoSASelectionIn('insdate12345','serviceAdvisor')">Recommend Agent</button>
					  
				  </div>
				</div>
				<div class="col-md-3">
				  <div class="form-group">
					<label for="">Assigned Insurance Agent</label>
			
					<select class="form-control insuranceAgent" name="insuranceAgentData" id="serviceAdvisor" onChange="ajaxAutoSAManualchangeIns('insdate12345','serviceAdvisor');">
												
					</select>
					
					</select>
				  </div>
				</div>
				<!--
				<div class="col-md-3">
				  <div class="form-group">
					<button type="button" class="btn btn-primary" onClick="return ajaxAutoSASelectionList('workshop','date12345','serviceAdvisor');">Change SA</button>
				  </div>
				</div>	
				-->
				
			  </div>
			
			
			<div class="pull-right">
			<button type="button" class="btn btn-primary" id="backtoMain">Back</button>
			<button type="button" class="btn btn-primary" id="nextToCustomerDrive">Next</button>
			</div>
			</div>
			</div>
			<div style="display:none;" class="animated  bounceInRight" id="CustomerDriveInDiv">
			<div class="col-md-12">
			<label>Choose one of the below option:</label>
		<br /> 
		<div class="radio-inline">
		  <label>
			<input type="radio" name="typeOfPickup" value="Home Visit" id="homeVisitId" >
			Home Visit</label>
		</div>
		<div class="radio-inline">
	  
		  <label>
			<input type="radio" name="typeOfPickup" value="Showroom Visit" id="showroomVisitId" onclick="ajaxAddShowRoom();">
			Showroom Visit</label>
	</div>
		
	  
	  <div style="display:none;" id="ShowroomListDiv">
		  <div class="animated  bounceInRight">
			  <div class="col-md-3">
				   <label for="">List Of Showrooms:</label>
					<div class="form-group">
					 
					  <select class="form-control" id="ShowroomsSelectId" name="showRoom_id">
						<option value="0">--Select--</option>
						
					  </select>
					</div>
				  </div>
				  <!-- <div class="col-md-3">
					<div class="form-group">
					  <label for="time_From">Appointment From</label>
					  <input type="text" name="appointmentFromTime" class="timePickRange7to19 form-control" id="appoFmRoomID" readonly>
					</div>
				  </div>
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="time_To">Appointment To:</label>
					  <input type="text" name="appointmentToTime" class="timePickRange7to19 form-control" id="appoToRoomID" readonly>
					</div>
				  </div> -->
				  <div class="col-md-3">
					<div class="form-group">
					<br />
				  
				
				</div>
				  </div>
				  
				 
			   
		  </div>
		  		
	  </div>
	  <div style="display:none;" id="pickupDivInsurance">
		  <div class="animated  bounceInRight">
			<div class="col-md-3">
			  <div class="form-group">
				<label for="addressOfVisit"><b>Confirm Address</b></label>
				<select class="form-control" name="addressOfVisit" id="AddressMSSId">
				  """),_display_(/*192.8*/for(addre_list <- customerData.getAddresses()) yield /*192.54*/{_display_(Seq[Any](format.raw/*192.55*/("""
				  	
				  	"""),_display_(/*194.9*/if(addre_list.getConcatenatedAdress() == null)/*194.55*/{_display_(Seq[Any](format.raw/*194.56*/("""
                          """),format.raw/*195.27*/("""<option value=""""),_display_(/*195.43*/addre_list/*195.53*/.getAddressLine1()),format.raw/*195.71*/(""","""),_display_(/*195.73*/addre_list/*195.83*/.getAddressLine2()),format.raw/*195.101*/(""","""),_display_(/*195.103*/addre_list/*195.113*/.getAddressLine3()),format.raw/*195.131*/(""","""),_display_(/*195.133*/addre_list/*195.143*/.getCity()),format.raw/*195.153*/(""","""),_display_(/*195.155*/addre_list/*195.165*/.getPincode()),format.raw/*195.178*/("""">"""),_display_(/*195.181*/addre_list/*195.191*/.getAddressLine1()),format.raw/*195.209*/(""","""),_display_(/*195.211*/addre_list/*195.221*/.getAddressLine2()),format.raw/*195.239*/(""","""),_display_(/*195.241*/addre_list/*195.251*/.getAddressLine3()),format.raw/*195.269*/(""","""),_display_(/*195.271*/addre_list/*195.281*/.getCity()),format.raw/*195.291*/(""","""),_display_(/*195.293*/addre_list/*195.303*/.getPincode()),format.raw/*195.316*/("""</option>
                         """)))}/*196.27*/else/*196.31*/{_display_(Seq[Any](format.raw/*196.32*/("""
                         
                          """),format.raw/*198.27*/("""<option value=""""),_display_(/*198.43*/addre_list/*198.53*/.getConcatenatedAdress()),format.raw/*198.77*/(""","""),_display_(/*198.79*/addre_list/*198.89*/.getCity()),format.raw/*198.99*/(""","""),_display_(/*198.101*/addre_list/*198.111*/.getPincode()),format.raw/*198.124*/("""">"""),_display_(/*198.127*/addre_list/*198.137*/.getConcatenatedAdress()),format.raw/*198.161*/(""","""),_display_(/*198.163*/addre_list/*198.173*/.getCity()),format.raw/*198.183*/(""","""),_display_(/*198.185*/addre_list/*198.195*/.getPincode()),format.raw/*198.208*/("""</option>
                         """)))}),format.raw/*199.27*/("""
						  """)))}),format.raw/*200.10*/("""
				"""),format.raw/*201.5*/("""</select>
			  </div>
			</div>
			  
			 <!--<div class="col-md-3">
					<div class="form-group">
					  <label for="time_From">Appointment From</label>
					  <input type="text" name="appointmentFromTimeHomevisit" class="timePickRange7to19 form-control" id="appoFmHMID1" readonly>
					</div>
				  </div>
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="time_To">Appointment To:</label>
					  <input type="text" name="appointmentToTimeHomeVisit" class="timePickRange7to19 form-control" id="appoToHMID2" readonly>
					</div>
				  </div> --> 
				   <div class="col-md-3">
					<div class="form-group">
					<br />
				   <button type="button" class="btn btn-success" data-toggle="modal" data-target="#AddNewAddressPopup">Add New Address</button>
				
				</div>
				  </div>
			   
		  </div>
		  
		  
	 
		
	  </div>
	  <div class="pull-right">
	  <button type="button" class="btn btn-primary" id="BackToCunstomerDriveIndu">Back</button>
	   <button type="button" class="btn btn-primary" id="NextToLeadInsurance">Next</button>
	  </div>
	</div>
		  </div>
		  
		  <div style="display:none;" class="animated  bounceInRight" id="Add_OnsIndurace1">
		
		 <label>Any Add-Ons covers prefered by customer? (Details will be shared by the Insurance Executive) </label>
              <br>
			  <div class="radio-inline">
		  <label>
			<input type="radio" name="AddOnsYes" value="Yes" id="AddOnsYesRId" >
			Yes</label>
		</div>
		<div class="radio-inline">
		  <label>
			<input type="radio" name="AddOnsYes" value="No" id="AddOnsNoRId" checked>
			No</label>
		</div>
		
		<div style="display:none;" id="AllAdd_onsDiv" class="animated  bounceInRight">
		<div class="row">
              <div class="col-md-12">
              <p><b>Popular Options :</b></p>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="ComprehensivePolicy" name="addOnsPrefered_PopularOptions[0]" class="Add_Onschk">Comprehensive Policy
    </label>
           </div>
           </div>
            <div class="col-xs-3">
            <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="NCB Protect Cover" name="addOnsPrefered_PopularOptions[1]" class="Add_Onschk">NCB Protect Cover
    </label>
    </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Zero Dep Cover" name="addOnsPrefered_PopularOptions[2]" class="Add_Onschk">Zero Dep Cover
    </label>
    </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Invoice Cover" name="addOnsPrefered_PopularOptions[3]" class="Add_Onschk">Invoice Cover
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Engine Protect" name="addOnsPrefered_PopularOptions[4]" class="Add_Onschk">Engine Protect
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Electronic Circuit Cover" name="addOnsPrefered_PopularOptions[5]" class="Add_Onschk"> Electronic Circuit Cover
    </label>
           </div>
           </div>
          
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Passenger Cover" name="addOnsPrefered_PopularOptions[6]" class="Add_Onschk">Passenger Cover
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Quick Assistance Cover" name="addOnsPrefered_PopularOptions[7]"  class="Add_Onschk">Quick Assistance Cover
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Voluntary Deductible Cover" name="addOnsPrefered_PopularOptions[8]" class="Add_Onschk">Voluntary Deductible Cover
    </label>
           </div>
           </div>
           <br />

              </div>
            </div>
		<div class="row">
              <div class="col-md-12">

              <p><b>Other Options:</b></p>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Loss Of Personal Belongings" name="addOnsPrefered_OtherOptions[0]" class="Add_Onschk">Loss Of Personal Belongings
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Windshield Glass Cover" name="addOnsPrefered_OtherOptions[1]" class="Add_Onschk">Windshield Glass Cover
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Consumable Expenses" name="addOnsPrefered_OtherOptions[2]" class="Add_Onschk">Consumable Expenses
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Hospital Daily Cash Allowance" name="addOnsPrefered_OtherOptions[3]" class="Add_Onschk">Hospital Daily Cash Allowance
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Key Protector Cover" name="addOnsPrefered_OtherOptions[4]" class="Add_Onschk">Key Protector Cover
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Consumable Expenses" name="addOnsPrefered_OtherOptions[5]" class="Add_Onschk">Consumable Expenses
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Additional Expense Coverage" name="addOnsPrefered_OtherOptions[6]" class="Add_Onschk">Additional Expense Coverage 
    </label>
           </div>
           </div>
            <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Loss of DL / RC" name="addOnsPrefered_OtherOptions[7]" class="Add_Onschk">Loss of DL / RC
    </label>
           </div>
           </div>



           </div>
           </div>
		</div>
				
	  
	 
	  <div class="pull-right">
	  <button type="button" class="btn btn-primary" id="BackToNewInsu1">Back</button>
	   <button type="button" class="btn btn-primary" id="NextToNewInsu1">Next</button>
	  </div>
	</div>
		  
		   <div style="display:none;" class="animated  bounceInRight" id="PremiumInsu2">
			<div class="col-md-12">
			<label>Have you shared the tentative 'Insurance Premium' with customer ?:</label>
		<br /> 
		<div class="radio-inline">
		  <label>
			<input type="radio" name="PremiumYes" value="Yes" id="PremiumYesID" >
			Yes</label>
		</div>
		<div class="radio-inline">
	  
		  <label>
			<input type="radio" name="PremiumYes" value="No" id="PremiumNoID" >
			No</label>
	</div>
			
 <div  style="display:none;" id="InsurancePremiumDiv" class="animated  bounceInRight">
        <div class="row">
      <div class="col-xs-3">
       <div class="form-group">
        <label for="InsuranceCompany">Insurance Company</label>
          <select class="form-control input-sm" name="insuranceCompany" id="InsuCompSelectID">
          <option value="0">--Select--</option>
          <option value="Agriculture Insurance Company of India">Agriculture Insurance Company of India</option>
			<option value="Apollo Munich Insurance">Apollo Munich Insurance</option>
			<option value="Bajaj Allianz General Insurance">Bajaj Allianz General Insurance</option>
			<option value="Bharti AXA General Insurance">Bharti AXA General Insurance</option>
			<option value="Cholamandalam MS General Insurance">Cholamandalam MS General Insurance</option>
			<option value="Cigna TTK Health Insurance Company Ltd - Cigna TTK">Cigna TTK Health Insurance Company Ltd - Cigna TTK</option>
			<option value="ECGC (Export Credit Guarantee Corporation of India)">ECGC (Export Credit Guarantee Corporation of India)</option>
			<option value="Future Generali India Insurance Company Ltd">Future Generali India Insurance Company Ltd</option>
			<option value="HDFC ERGO General Insurance">HDFC ERGO General Insurance</option>
			<option value="ICICI Lombard General Insurance Company Limited">ICICI Lombard General Insurance Company Limited</option>
			<option value="IFFCO Tokio General Insurance Company Limited">IFFCO Tokio General Insurance Company Limited</option>
			<option value="L&T General Insurance">L&T General Insurance</option>
			<option value="Liberty Videocon General Insurance">Liberty Videocon General Insurance</option>
			<option value="Magma HDI General Insurance">Magma HDI General Insurance</option>
			<option value="Max Bupa General Insurance">Max Bupa General Insurance</option>
			<option value="National Insurance Company Limited">National Insurance Company Limited</option>
			<option value="New India Assurance">New India Assurance</option>
			<option value="Oriental Insurance Company">Oriental Insurance Company</option>
			<option value="Raheja QBE General Insurance Company Ltd - Raheja QBE">Raheja QBE General Insurance Company Ltd - Raheja QBE</option>
			<option value="Reliance General Insurance">Reliance General Insurance</option>
			<option value="Religare Health Insurance">Religare Health Insurance</option>
			<option value="Royal Sundaram General Insurance Co. Limited">Royal Sundaram General Insurance Co. Limited</option>
			<option value="SBI General Insurance Company Ltd">SBI General Insurance Company Ltd</option>
			<option value="Shriram General Insurance Company Ltd">Shriram General Insurance Company Ltd</option>
			<option value="Star Health and Allied Insurance">Star Health and Allied Insurance</option>
			<option value="Tata AIG General Insurance">Tata AIG General Insurance</option>
			<option value="United India Insurance">United India Insurance</option>
			<option value="Universal Sompo General Insurance">Universal Sompo General Insurance</option>
			<option value="Others">Others</option>
        </select>
        </div>
      </div>
      <div class="col-xs-3">
       <div class="form-group">
        <label for="InsuDSAID">DSA (if any)</label>
        <input class="form-control input-sm" id="InsuDSAID" name="dsa" type="text">
      </div>
      </div>
      
    </div>	
	 <div class="row">
	  <div class="col-md-3">
                <div class="form-group">
                  <label>Premium (with Tax)</label>
                  <div class="input-group">
                    <input type="text" class="form-control numberOnly" value="0" name="premiumwithTax" id="PremiumwithTax" readonly>
                     <span class="input-group-addon" id="editFinalPremium" style="cursor:pointer" data-toggle="tooltip" title="Reset"><i class="fa fa-pencil"></i> </span></div> 
                </div>
              </div>
			  
     
	  <div class="col-xs-3">
       <div class="form-group">
       <br />
        <button type="button" class="btn btn-primary" id="" data-target="#InsuPremiumPopup" data-toggle="modal" data-backdrop="static" data-keyboard="false" onclick="ajaxODPercentage();"><i class="fa fa-calculator fa-lg"></i>&nbsp;Calculate Premium</button>
      </div>
      </div>
     
    </div>
	 </div>
	 
	  <div class="pull-right">
	  <button type="button" class="btn btn-primary" id="BackToNewInsu2">Back</button>
	   <button type="button" class="btn btn-primary" id="NextToNewInsu2">Next</button>
	  </div>
	
		  </div>
		  </div>
		   <div style="display:none;" class="animated  bounceInRight" id="nomineeDetails3">
			<div class="col-md-12">
			<label>Customer's nominee details available ?:</label>
		<br /> 
		<div class="radio-inline">
		  <label>
			<input type="radio" name="nomineeYes" value="Yes" id="nomineeYesID" >
			Yes</label>
		</div>
		<div class="radio-inline">
		  <label>
			<input type="radio" name="nomineeYes" value="No" id="nomineeNoID" >
			No</label>
		</div>
		<div class="row animated  bounceInRight" style="display:none;" id="nomineeDiv">
			<div class="col-md-3">
				<div class="form-group">
					<label>Nominee Name</label>
					<input type="text" class="form-control textOnlyAccepted" name="nomineeName" id="NomineeNameID" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Nominee Age</label>
					<input type="text" class="form-control numberOnly" name="nomineeAge" maxlength="2" id="NomineeAgeID"  />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Nominee's Relation with Owner</label>
					<input type="text" class="form-control textOnlyAccepted" name="nomineeRelationWithOwner" id="NomineeRelationID" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Appointee Name</label>
					<input type="text" class="form-control textOnlyAccepted" name="appointeeName" id="AppointeeNameID"  />
				</div>
			</div>
		</div>
		
				
	  
	 
	  <div class="pull-right">
	  <button type="button" class="btn btn-primary" id="BackToNewInsu3">Back</button>
	   <button type="button" class="btn btn-primary" id="NextToNewInsu3">Next</button>
	  </div>
	</div>
		  </div>
		  
		   <div class="col-md-12 animated  bounceInRight" style="display:none;" id="finalDiv1">
			<label>Capture Lead : Is there an Upsell Opportunity.</label>
		   <br>
		   
			<div class="radio-inline">
			  <label>
				<input type="radio" name="LeadYes" value="Capture Lead Yes" id="LeadYesID" >
				Yes</label>
			</div>
			<div class="radio-inline">
			  <label>
				<input type="radio" name="LeadYes" value="Capture Lead No" id="LeadNoID" checked>
				No</label>
			</div>
			<div class="row animated  bounceInRight" style="display:none;" id="LeadDiv">
			 <div class="col-md-12">
			  <div class="col-md-6">
			  
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[0].upsellId" value="1" id="InsuranceIDCheck" onClick="loadLeadBasedOnUserLocation('InsuranceIDCheck','insuranceLead1')">
					Insurance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="InsuranceSelect">
				 
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[0].taggedTo" id="insuranceLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments1" name="upsellLead[0].upsellComments"></textarea>
					</div>
				 
				</div>
				
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[1].upsellId" value="2" id="MaxicareIDCheck" onClick="loadLeadBasedOnUserLocation('MaxicareIDCheck','maxicareLead')">
					Warranty / EW</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="MaxicareSelect">
				 
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[1].taggedTo" id="maxicareLead">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments1" name="upsellLead[1].upsellComments"></textarea>
					</div>
				 
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[2].upsellId" value="4" id="ShieldID" onClick="loadLeadBasedOnUserLocation('ShieldID','warrantyLead1')">
					Re-Finance / New Car Finance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="ShieldSelect">
					
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[2].taggedTo" id="warrantyLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments2" name="upsellLead[2].upsellComments"></textarea>
					</div>
				 
				</div>
				<!-- <div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[1].upSellType" value="Warranty / EW" id="WARRANTYID" >
					Warranty / EW</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="WARRANTYSelect">
					
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[1].taggedTo" id="warrantyLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments2" name="upsellLead[1].upsellComments"></textarea>
					</div>
				 
				</div> -->
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="VAS myOutCheckbox" name="upsellLead[3].upsellId" value="3" id="VASID" onClick="loadLeadBasedOnUserLocation('VASID','vASLead1')"/>
					VAS</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="VASTagToSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[3].taggedTo" id="vASLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments3" name="upsellLead[3].upsellComments"></textarea>
					</div>
				</div>
				</div>
				<!-- <div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[3].upSellType" value="Re-Finance / New Car Finance" id="ReFinanceIDCheck" />
					Re-Finance / New Car Finance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="ReFinanceSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[3].taggedTo" id="reFinanceLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments4" name="upsellLead[3].upsellComments"></textarea>
					</div>
				 </div>
				</div> -->
				<div class="col-md-6">
				<!-- <div class="checkbox">
				  <label>
					<input type="checkbox" class="Loan myOutCheckbox" name="upsellLead[4].upSe"""),
format.raw("""llType" value="Sell Old Car" id="LoanID" />
					Sell Old Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="LoanSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[4].taggedTo" id="sellOldCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments5" name="upsellLead[4].upsellComments"></textarea>
					</div>
				  </div> -->
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="EXCHANGE myOutCheckbox" name="upsellLead[4].upsellId" value="6" id="EXCHANGEID" onClick="loadLeadBasedOnUserLocation('EXCHANGEID','buyNewCarLead1')"/>
					Buy New Car / Exchange</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="EXCHANGEIDSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[4].taggedTo" id="buyNewCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments6" name="upsellLead[4].upsellComments"></textarea>
					</div>
				 	</div>
				  <div class="checkbox">
				  <label>
					<input type="checkbox" class="UsedCar myOutCheckbox" name="upsellLead[5].upsellId" value="9" id="UsedCarID" onClick="loadLeadBasedOnUserLocation('UsedCarID','usedCarLead1')"/>
					Used Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="UsedCarSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[5].taggedTo" id="usedCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments7" name="upsellLead[5].upsellComments"></textarea>
					</div>
				 
				</div>
				 <div class="checkbox">
				  <label>
					<input type="checkbox" class="UsedCar myOutCheckbox" name="upsellLead[6].upsellId" value="5" id="RoadSideAsstID" onClick="loadLeadBasedOnUserLocation('RoadSideAsstID','RoadSideAssiLead1')"/>
					Sell Old Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="RoadSideAssiSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[6].taggedTo" id="RoadSideAssiLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments7" name="upsellLead[6].upsellComments"></textarea>
					</div>
				 
				</div>
		   
			  </div>
			</div>
			 
			</div>
			  <div class="pull-right" >
			<button type="button" class="btn btn-primary" id="BackToInsuNomine">Back</button>
			  <button type="button" class="btn btn-primary" value="" id="NextToCustFeedBack"/>Next</button>
			</div>
			</div>
	
				
			<div class="row animated  bounceInRight" id="CustFeedBack" style="display:none;">
				  <div class="col-md-12">
				  
					<label>Does Customer has a feedback / compliant ?</label>
					<br>
					<div class="radio-inline">
					  <label for="feedbackYes">
						<input type="radio" name="userfeedback" onclick="loadLeadBasedOnLocationDepartment();" value="feedback Yes" id="feedbackYes" >
						Yes</label>
					</div>
					<div class="radio-inline">
					  <label for="feedbackNo">
						<input type="radio" name="userfeedback" value="feedback No" id="feedbackNo" checked>
						No</label> 
					</div>
				
			   
				<div class="row animated  bounceInRight" style="display:none;" id="feedbackDIV"><br>
				 <div class="col-md-12">
				  <div class="col-md-3">
					<label for="">Select Department<i class="lblStar">*</i></label>
					<select class="form-control selected_department" onchange="ajaxLeadTagByDepartment();" id="selected_department1" name="departmentForFB">
					 
					</select>
				  </div>
				  <div class="col-md-3">
					<label for="">Tag to</label>
					<select class="form-control" name="complaintOrFB_TagTo" id="LeadTagsByLocation1" name="complaintOrFB_TagTo">
					
					"""),_display_(/*791.7*/{if(interOfCall.getSrdisposition()!=null){
					
					<option value={interOfCall.getSrdisposition().getComplaintOrFB_TagTo()} selected="selected">{interOfCall.getSrdisposition().getComplaintOrFB_TagTo()}</option>
					
					}}),format.raw/*795.8*/("""	  
					"""),format.raw/*796.6*/("""</select>
				  </div>
				
				  <div class="col-md-3">
					<label for="comments">Feedback/Comment</label>
					<textarea class="form-control" rows="1" id="commentsOfFB" name="remarksOfFB"></textarea>
				  </div>
				  <br>
				</div>
				  </div>
				  <div class="pull-right" >
			<button type="button" class="btn btn-primary" id="BackToUpsell">Back</button>
			  <button type="button" class="btn btn-primary" value="" id="NextToLastQuestion"/>Next</button>
			</div>
		  </div>
		  </div>

<div class="col-md-12 animated  bounceInRight" id="LastQuestion" style="display:none;">
				
					<label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
					<br>
					<div class="col-md-2">
					<div class="radio-inline">
					  <label for="feedbackYes">
						<input type="radio" name="CustomerFeedBackYes" value="Customer Yes" id="CustomerYes" >
						Yes</label>
					</div>
					<div class="radio-inline">
					  <label for="feedbackNo">
						<input type="radio" name="CustomerFeedBackYes" value="Customer No" id="CustomerNo" checked>
						No</label> 
					</div>
					</div>
					<div class="col-md-4">
					<label>Remarks</label>
					<textarea class="form-control" rows="2" name="remarksList[0]"></textarea>
				</div>
				  <div class="pull-right" >
			<button type="button" class="btn btn-primary" id="BackToCustomerFeedback">Back</button>
			  <button type="submit" class="btn btn-primary" value="bookMyAppointment" name="typeOfsubmit" id="bookMyserviceSubmit"/>Submit</button>
			</div>
				  </div>
				  
				  
				  
				  <!-- call me later -->
				  
				   <div class="row animated  bounceInRight" style="display:none;" id="callMeLattteDiv">
		   <div class="col-md-12">
			<div class="col-md-3">
			  <div class="form-group">
				<label for="followUpDate">Follow Up Date<i class="lblStar">*</i></label>
				<input type="text" name="followUpDate" class="form-control" id="FollowUpDate" readonly>
			  </div>
			</div>
			<div class="col-md-3">
			  <div class="form-group">
				<label for="followUpTime">Follow Up Time<i class="lblStar">*</i></label>
				<input type="text" name="followUpTime" class="timepicker_7 form-control" id="FollowUpTime" readonly>
			  </div>
			</div>
			<div class="col-md-3">
			  <div class="form-group">
				<label for="">Remarks</label>
				<textarea class="form-control" rows="2" name="remarksList[1]"></textarea>
			  </div>
			</div>
			<br>
			  <div class="pull-right">
			  <button type="button" class="btn btn-primary" value="" id="CallaterBack">Back</button>
				<button type="submit" class="btn btn-primary" value="callMeLater" id="callMeLaterSubmit" name="typeOfsubmit">Submit</button>
			</div>
		  </div>
		
		  </div>
				  
				  <!-- Service Not Required -->
				   <div class="row animated  bounceInRight" style="display:none;" id="alreadyserviceDIV" > 
				<!--No Service Required-->
				<div class="col-md-12" >
					<div class="col-md-12">
					  <div class="form-group">
					 
						<div class="checkbox AlreadyServiced">
						  <label>
							<input type="checkbox" class="NoService" name="renewalNotRequiredReason" value="Already Renewed" id="AlreadyServiced" >
							Already Renewed</label>
						</div>
						<div class="animated  bounceInRight alreadyservicedDiv1" style="display:none;" id="alreadyservicedDiv1">
						  <div class="col-md-12">
							<div class="radio ServicedMyDealer">
							  <label>
								<input type="radio" name="renewalDoneBy" onclick="workshopNameList();" value="Renewed At My Dealer" id="ServicedMyDealer" >
								Renewed At My Dealer</label>
							</div>
							<div class="row animated  bounceInRight" style="display:none;" id="ServicedMyDealerDiv">
							  <div class="col-md-12">
								<div class="col-md-3">
								  <label for=""><b>Last Renewal Date<b></label>
								 <input type="text" name="lastRenewalDate" value=""""),_display_(/*895.60*/{vehicleData.getLastServiceDate()}),format.raw/*895.94*/("""" class="datepicker form-control"/>
								</div>
								<div class="col-md-3">
								  <label for=""><b>Last Renewed Location<b></label>
									<select class="form-control" name="lastRenewedLocation" id="lastServiceWorkshopList">
								  
								 	<option value=0>--Select--</option>				  
								 
								 	
								  </select>
								  
								</div>
								 <div class="col-md-2">
								  <label for=""><b>Insurance Provided By<b></label>
								  <select class="form-control" name="insuranceProvidedBy">
											<option value="0">--Select--</option>
											<option value="Agriculture Insurance Company of India">Agriculture Insurance Company of India</option>
											<option value="Apollo Munich Insurance">Apollo Munich Insurance</option>
											<option value="Bajaj Allianz General Insurance">Bajaj Allianz General Insurance</option>
											<option value="Bharti AXA General Insurance">Bharti AXA General Insurance</option>
											<option value="Cholamandalam MS General Insurance">Cholamandalam MS General Insurance</option>
											<option value="Cigna TTK Health Insurance Company Ltd - Cigna TTK">Cigna TTK Health Insurance Company Ltd - Cigna TTK</option>
											<option value="ECGC (Export Credit Guarantee Corporation of India)">ECGC (Export Credit Guarantee Corporation of India)</option>
											<option value="Future Generali India Insurance Company Ltd">Future Generali India Insurance Company Ltd</option>
											<option value="HDFC ERGO General Insurance">HDFC ERGO General Insurance</option>
											<option value="ICICI Lombard General Insurance Company Limited">ICICI Lombard General Insurance Company Limited</option>
											<option value="IFFCO Tokio General Insurance Company Limited">IFFCO Tokio General Insurance Company Limited</option>
											<option value="L&T General Insurance">L&T General Insurance</option>
											<option value="Liberty Videocon General Insurance">Liberty Videocon General Insurance</option>
											<option value="Magma HDI General Insurance">Magma HDI General Insurance</option>
											<option value="Max Bupa General Insurance">Max Bupa General Insurance</option>
											<option value="National Insurance Company Limited">National Insurance Company Limited</option>
											<option value="New India Assurance">New India Assurance</option>
											<option value="Oriental Insurance Company">Oriental Insurance Company</option>
											<option value="Raheja QBE General Insurance Company Ltd - Raheja QBE">Raheja QBE General Insurance Company Ltd - Raheja QBE</option>
											<option value="Reliance General Insurance">Reliance General Insurance</option>
											<option value="Religare Health Insurance">Religare Health Insurance</option>
											<option value="Royal Sundaram General Insurance Co. Limited">Royal Sundaram General Insurance Co. Limited</option>
											<option value="SBI General Insurance Company Ltd">SBI General Insurance Company Ltd</option>
											<option value="Shriram General Insurance Company Ltd">Shriram General Insurance Company Ltd</option>
											<option value="Star Health and Allied Insurance">Star Health and Allied Insurance</option>
											<option value="Tata AIG General Insurance">Tata AIG General Insurance</option>
											<option value="United India Insurance">United India Insurance</option>
											<option value="Universal Sompo General Insurance">Universal Sompo General Insurance</option>
											<option value="Others">Others</option>
											
										</select>
								</div>
								 <div class="col-md-2">
								  <label for=""><b>Premium (Rs.)<b></label>
								  <input type="text" name="premimum" value="" class="form-control numberOnly"/>
								</div>
								 <div class="col-md-2">
								  <label for=""><b>Cover Note (No.)<b></label>
								  <input type="text" name="coverNoteNo" value="" class="form-control"/>
								</div>
							  </div>
							</div>
							<div class="radio ServicedOtherDealer">
							  <label>
								<input type="radio" name="renewalDoneBy" value="Renewed At Other Dealer" id="renewedOtherDealer" >
								Renewed At Other Dealer</label>
							</div>
							<div class="row animated  bounceInRight" style="display:none;" id="ServicedAtOtherDealerDiv" >
							  <div class="col-md-12">
								
								 <div class="col-md-12">
									<div class="radio">
									  <label>
										<input type="radio" name="typeOfAutherization" value="OEM Authorized Dealer" id="Autorizedworkshopid" >
										OEM Authorized Dealer</label>
									</div>
								  </div>
							   
								<div class="animated  bounceInRight" style="display:none;" id="AutorizedworkshopDIV">
								  <div class="col-md-12">
								   
									  <div class="col-md-3">
										<label for="dealerName"><b>Dealer Name<b></label>
										<input type="text" name="dealerName" class="form-control textOnlyAccepted" />
									  </div>
								  
									  <div class="col-md-3">
										<label for="dateOfService"><b>Date of Renewal<b></label>
										<input type="text" name=dateOfRenewal class="datepicker form-control" />
									  </div>
								 
									  <div class="col-md-3">
										<label for="mileageAtService"><b>Insurance Provider<b></label>
										<select class="form-control" name="insuranceProvidedByOEM">
											<option value="0">--Select--</option>
											<option value="Agriculture Insurance Company of India">Agriculture Insurance Company of India</option>
											<option value="Apollo Munich Insurance">Apollo Munich Insurance</option>
											<option value="Bajaj Allianz General Insurance">Bajaj Allianz General Insurance</option>
											<option value="Bharti AXA General Insurance">Bharti AXA General Insurance</option>
											<option value="Cholamandalam MS General Insurance">Cholamandalam MS General Insurance</option>
											<option value="Cigna TTK Health Insurance Company Ltd - Cigna TTK">Cigna TTK Health Insurance Company Ltd - Cigna TTK</option>
											<option value="ECGC (Export Credit Guarantee Corporation of India)">ECGC (Export Credit Guarantee Corporation of India)</option>
											<option value="Future Generali India Insurance Company Ltd">Future Generali India Insurance Company Ltd</option>
											<option value="HDFC ERGO General Insurance">HDFC ERGO General Insurance</option>
											<option value="ICICI Lombard General Insurance Company Limited">ICICI Lombard General Insurance Company Limited</option>
											<option value="IFFCO Tokio General Insurance Company Limited">IFFCO Tokio General Insurance Company Limited</option>
											<option value="L&T General Insurance">L&T General Insurance</option>
											<option value="Liberty Videocon General Insurance">Liberty Videocon General Insurance</option>
											<option value="Magma HDI General Insurance">Magma HDI General Insurance</option>
											<option value="Max Bupa General Insurance">Max Bupa General Insurance</option>
											<option value="National Insurance Company Limited">National Insurance Company Limited</option>
											<option value="New India Assurance">New India Assurance</option>
											<option value="Oriental Insurance Company">Oriental Insurance Company</option>
											<option value="Raheja QBE General Insurance Company Ltd - Raheja QBE">Raheja QBE General Insurance Company Ltd - Raheja QBE</option>
											<option value="Reliance General Insurance">Reliance General Insurance</option>
											<option value="Religare Health Insurance">Religare Health Insurance</option>
											<option value="Royal Sundaram General Insurance Co. Limited">Royal Sundaram General Insurance Co. Limited</option>
											<option value="SBI General Insurance Company Ltd">SBI General Insurance Company Ltd</option>
											<option value="Shriram General Insurance Company Ltd">Shriram General Insurance Company Ltd</option>
											<option value="Star Health and Allied Insurance">Star Health and Allied Insurance</option>
											<option value="Tata AIG General Insurance">Tata AIG General Insurance</option>
											<option value="United India Insurance">United India Insurance</option>
											<option value="Universal Sompo General Insurance">Universal Sompo General Insurance</option>
											<option value="Others">Others</option>
											
										</select>
									  </div>
									
									  <div class="col-md-3">
										<label for="serviceType"><b>Type of Insurance <b></label>
										
										<select class="form-control" id="" name="typeOfInsurance">
										  <option value="0">--SELECT--</option>
										  <option value="PI">PI</option>
										  <option value="NPI">NPI</option>
										  
									
									</select>
									  </div>
								   
								  </div>
								</div>
							   
								  <div class="col-md-12">
									<div class="radio">
									  <label>
										<input type="radio" name="typeOfAutherization" value="Unauthorized Dealer" id="NonAutorizedworkshopid" >
										Unauthorized Dealer</label>
									</div>
								  </div>
							  
								<div class="animated  bounceInRight" style="display:none;" id="NonAutorizedworkshopDiv">
								  <div class="col-md-12">
									<div class="col-md-3">
									  <label for="dateOfRenewalNonAuth"><b>Date of Renewal<b></label>
									  <input type="text" name="dateOfRenewalNonAuth" class="datepicker form-control" />
									</div>
									<div class="col-md-3">
									  <label for="mileageAsOnDate"><b> Insurance Provider<b></label>
									 <select class="form-control" name="insuranceProvidedUnAuth">
											<option value="0">--Select--</option>
											<option value="Agriculture Insurance Company of India">Agriculture Insurance Company of India</option>
											<option value="Apollo Munich Insurance">Apollo Munich Insurance</option>
											<option value="Bajaj Allianz General Insurance">Bajaj Allianz General Insurance</option>
											<option value="Bharti AXA General Insurance">Bharti AXA General Insurance</option>
											<option value="Cholamandalam MS General Insurance">Cholamandalam MS General Insurance</option>
											<option value="Cigna TTK Health Insurance Company Ltd - Cigna TTK">Cigna TTK Health Insurance Company Ltd - Cigna TTK</option>
											<option value="ECGC (Export Credit Guarantee Corporation of India)">ECGC (Export Credit Guarantee Corporation of India)</option>
											<option value="Future Generali India Insurance Company Ltd">Future Generali India Insurance Company Ltd</option>
											<option value="HDFC ERGO General Insurance">HDFC ERGO General Insurance</option>
											<option value="ICICI Lombard General Insurance Company Limited">ICICI Lombard General Insurance Company Limited</option>
											<option value="IFFCO Tokio General Insurance Company Limited">IFFCO Tokio General Insurance Company Limited</option>
											<option value="L&T General Insurance">L&T General Insurance</option>
											<option value="Liberty Videocon General Insurance">Liberty Videocon General Insurance</option>
											<option value="Magma HDI General Insurance">Magma HDI General Insurance</option>
											<option value="Max Bupa General Insurance">Max Bupa General Insurance</option>
											<option value="National Insurance Company Limited">National Insurance Company Limited</option>
											<option value="New India Assurance">New India Assurance</option>
											<option value="Oriental Insurance Company">Oriental Insurance Company</option>
											<option value="Raheja QBE General Insurance Company Ltd - Raheja QBE">Raheja QBE General Insurance Company Ltd - Raheja QBE</option>
											<option value="Reliance General Insurance">Reliance General Insurance</option>
											<option value="Religare Health Insurance">Religare Health Insurance</option>
											<option value="Royal Sundaram General Insurance Co. Limited">Royal Sundaram General Insurance Co. Limited</option>
											<option value="SBI General Insurance Company Ltd">SBI General Insurance Company Ltd</option>
											<option value="Shriram General Insurance Company Ltd">Shriram General Insurance Company Ltd</option>
											<option value="Star Health and Allied Insurance">Star Health and Allied Insurance</option>
											<option value="Tata AIG General Insurance">Tata AIG General Insurance</option>
											<option value="United India Insurance">United India Insurance</option>
											<option value="Universal Sompo General Insurance">Universal Sompo General Insurance</option>
											<option value="Others">Others</option>
											
										</select>
									</div>
								  </div>
								</div>
							  </div>
							</div>
							
						  </div>
						  
						  <div class="pull-right" >
					
					  <button type="button" class="btn btn-primary" value="" id="nextToAlreadySrviceUpsell"/>Next</button>
					</div>
				  </div>
		<div class="row animated  bounceInRight" id="AlreadyServiceUpsellOpp" style="display:none;">
						  <div class="col-md-12">
				   
					<label>Capture Lead : Is there an Upsell Opportunity.</label>
				   <br>
				   
					<div class="radio-inline">
					  <label>
						<input type="radio" name="LeadYesAlradyService" value="Capture Lead Yes AlreadyService" id="" >
						Yes</label>
					</div>
					<div class="radio-inline">
					  <label>
						<input type="radio" name="LeadYesAlradyService" value="Capture Lead No AlreadyService" id="LeadNoUpselOpp" checked>
						No</label>
					</div>
					<div class="row animated  bounceInRight" style="display:none;" id="LeadDivAlreadyService">
					  <div class="col-md-6">
					 
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="" name="upsellLead[7].upsellId" value="1" id="InsuranceIDCheckAlreadyServiced" onClick="loadLeadBasedOnUserLocation('InsuranceIDCheckAlreadyServiced','insuranceLead')">
							Insurance</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="InsuranceSelectAlreadyService">
						  <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[7].taggedTo" id="insuranceLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[7].upsellComments"></textarea>
							</div>
						   </div>
						</div>
						
						 <div class="checkbox">
						  <label>
							<input type="checkbox" class="" name="upsellLead[8].upsellId" value="2" id="MaxicareIDCheckAlreadyServiced" onClick="loadLeadBasedOnUserLocation('MaxicareIDCheckAlreadyServiced','MaxicareLeadAlreadyServed')">
							Warranty / EW</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="MaxicareSelectAlreadyService">
						  <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[8].taggedTo" id="MaxicareLeadAlreadyServed">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[8].upsellComments"></textarea>
							</div>
						   </div>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="" name="upsellLead[9].upsellId" value="4" id="ShieldIDAlreadyService" onClick="loadLeadBasedOnUserLocation('ShieldIDAlreadyService','ShieldLead')">
							Re-Finance / New Car Finance</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="ShieldSelectAlreadyService">
							<div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[9].taggedTo" id="ShieldLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[9].upsellComments"></textarea>
							</div>
						   </div>
						<!-- <div class="checkbox">
						  <label>
							<input type="checkbox" class="" name="upsellLead[8].upSellType" value="Warranty / EW" id="WARRANTYIDAlreadyService" >
							Warranty / EW</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="WARRANTYSelectAlreadyService">
							<div class="col-md-12">
							<div class="col-md-3">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[8].taggedTo" id="warrantyLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-3">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[8].upsellComments"></textarea>
							</div>
						   </div> -->
						   
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="VAS" name="upsellLead[10].upsellId" value="3" id="VASIDAlreadyService" onClick="loadLeadBasedOnUserLocation('VASIDAlreadyService','vASLead')"/>
							VAS</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="VASTagToSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[10].taggedTo" id="vASLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[10].upsellComments"></textarea>
							</div>
						   </div>
						  
						</div>
						<!-- <div class="checkbox">
						  <label>
							<input type="checkbox" class="LeadClass" name="upsellLead[10].upSellType" value="Re-Finance / New Car Finance" id="ReFinanceIDCheckAlreadyService" />
							Re-Finance / New Car Finance</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="ReFinanceSelectAlreadyService">
						  <div class="col-md-12">
							<div class="col-md-3">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[10].taggedTo" id="reFinanceLead">
								<option >Select</option>	
							  </select>
							   </div>
							   <div class="col-md-3">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[10].upsellComments"></textarea>
							</div>
						   </div>
						 
						</div> -->
						</div>
						 <div class="col-md-6">
						<!-- <div class="checkbox">
						  <label>
							<input type="checkbox" class="Loan" name="upsellLead[11].upSellType" value="Sell Old Car" id="LoanIDAlreadyService" />
							Sell Old Car</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="LoanSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-3">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[11].taggedTo" id="sellOldCarLead">
								<option >Select</option>
							  </select>
							   </div>
	"""),
format.raw("""						   <div class="col-md-3">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[11].upsellComments"></textarea>
							</div>
						   </div>
					
						</div> -->
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="EXCHANGE" name="upsellLead[11].upsellId" value="6" id="EXCHANGEIDAlreadyService" onClick="loadLeadBasedOnUserLocation('EXCHANGEIDAlreadyService','buyNewCarLead')"/>
							Buy New Car / Exchange</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="EXCHANGEIDSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[11].taggedTo" id="buyNewCarLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[11].upsellComments"></textarea>
							</div>
						   </div>
						
						</div>
						  <div class="checkbox">
						  <label>
							<input type="checkbox" class="UsedCar" name="upsellLead[12].upsellId" value="9" id="UsedCarIDAlreadyService" onClick="loadLeadBasedOnUserLocation('UsedCarIDAlreadyService','usedCarLead')"/>
							Used Car</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="UsedCarSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[12].taggedTo" id="usedCarLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[12].upsellComments"></textarea>
							</div>
						   </div>
						  
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="UsedCar" name="upsellLead[13].upsellId" value="5" id="RoadSideIDAlreadyService" onClick="loadLeadBasedOnUserLocation('RoadSideIDAlreadyService','roadCarLead')"/>
							Sell Old Car</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="RoadSideSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[13].taggedTo" id="roadCarLead">
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[13].upsellComments"></textarea>
							</div>
						   </div>
						  
						</div>
				   
					  </div>
					</div>
					  <div class="pull-right" >
					<button type="button" class="btn btn-primary" id="BackToAlreadySerUpsel">Back</button>
					  <button type="button" class="btn btn-primary" value="" id="NextAlreadySerFeedBack"/>Next</button>
					</div>
					
						  </div>
				
							</div>
							<div class="row animated  bounceInRight" id="AlreadyServiceFeedBAck" style="display:none;">
						  <div class="col-md-12">
						 
							<label>Does Customer has a feedback / compliant ?</label>
							<br>
							<div class="radio-inline">
							  <label for="feedbackYes">
								<input type="radio" name="userfeedbackAlreadyService" onclick="loadLeadBasedOnLocationDepartment();" value="feedback Yes AlreadyService" id="userfeedbackAlreadyService" >
								Yes</label>
							</div>
							<div class="radio-inline">
							  <label for="feedbackNo">
								<input type="radio" name="userfeedbackAlreadyService" value="feedback No AlreadyService" id="" >
								No</label> 
							</div>
						
					   
						<div class="row animated  bounceInRight" style="display:none;" id="feedbackDIVAlreadyService"><br>
						 <div class="col-md-12">
						  <div class="col-md-3">
							<label for="">Select Department<i class="lblStar">*</i></label>
						<select class="form-control selected_department" id="selected_department" name="departmentForFB1" onchange="ajaxLeadTagByDepartment();">
						 <option>select</option>
						 
							</select>
						  </div>
						  <div class="col-md-3">
							<label for="">Tag to</label>
							<select class="form-control" id="LeadTagsByLocation" name="complaintOrFB_TagTo1">
							  
							</select>
						  </div>
						
						  <div class="col-md-3">
							<label for="comments">Feedback/Comment</label>
							<textarea class="form-control" rows="1" id="comments" name="remarksOfFB1"></textarea>
						  </div>
						  <br>
						</div>
						  </div>
						  <div class="pull-right" >
					<button type="button" class="btn btn-primary" id="BackToAlreadyServUpsell">Back</button>
					  <button type="button" class="btn btn-primary" value="" id="NextToLastAlreadySerQuestion"/>Next</button>
					</div>
				  </div>
				    </div>
					
					<div class="row animated  bounceInRight" id="LastQuestionAlreadyServ" style="display:none;">
						  <div class="col-md-12">
						  <div class="col-md-12">
							<label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
							<br>
							<div class="col-md-2">
							<div class="radio-inline">
							  <label for="">
								<input type="radio" name="radio7" value="" id=""  data-toggle="modal" data-target="#addBtn" >
								Yes</label>
							</div>
							<div class="radio-inline">
							  <label for="feedbackNo">
								<input type="radio" name="radio7" value="" id="" checked>
								No</label> 
							</div>
							</div>
							<div class="col-md-4">
							<label>Remarks</label>
							<textarea class="form-control" rows="2" name="remarksList[2]"></textarea>
							</div>
						  </div>
						  </div>
						  <div class="pull-right" >
					<button type="button" class="btn btn-primary " id="BackToAlreadyServiceFeedBack">Back</button>
					 <button type="submit" class="btn btn-primary" value="alreadyRenewed" name="typeOfsubmit"id="alreadyServiced"/>Submit</button>
					</div>
						  </div>
						
						
						<div class="checkbox VehicleSold"> 
						  <label>
							<input type="checkbox" class="NoService" name="renewalNotRequiredReason" value="Vehicle Sold" id="VehicleSold" >
							Vehicle Sold</label>
						</div>
						
				
						<div class="row animated  bounceInRight" style="display:none;" id="VehicelSoldYesRNo">
						<div class="col-md-12">
                        <label>	Do you have the contact details of the new owner ?</label>
                        <br>
                        <div class="radio-inline">
                          <label for="">
                            <input type="radio" name="VehicleSoldYes" value="VehicleSold Yes" id="VehicleSoldYesbtn" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="">
                            <input type="radio" name="VehicleSoldYes" value="VehicleSoldYes No" id="VehicleSoldNobtn" >
                            No</label> 
                        </div>
                      </div>
                     <div class="pull-right backToAllSNR">
				    <button type="button" class="btn btn-primary" value="" id="backToAlreadyServicediv">Back</button>
					<button type="button" class="btn btn-primary" value="" id="NextToPurchaseNewcarNO">Next</button>
                </div>
						
                      
						</div>
						
					
						<div class="row animated  bounceInRight" style="display:none;"  id="VehicleSoldClickYes">
							 
							
				<div class="row">
			 <div class="col-md-12">
 <label>Please confirm the name of the new owner. </label><br>			 
               <div class="form-group">
      <div class="col-xs-1">
       
       <select class="form-control">
<option>Mr.</option>
<option>Ms.</option>
<option>Mrs.</option>

</select>
      </div>
      <div class="col-xs-2" style="margin-left: -28px;">
       
        <input class="form-control onlyAlphabetOnly" id="customerFNameConfirm" name="customerFName" placeholder="First Name" type="text">
      </div>
      <div class="col-xs-2">
       
        <input class="form-control onlyAlphabetOnly" name="customerLName" placeholder="Last Name" id="customerLNameConfirm" type="text">
      </div>
	  </div>
	  </div>
	  </div>
	  </br>
	  <div class="row">
			 <div class="col-md-12">
       <div class="col-xs-2">
              <input class="form-control numberOnly" maxlength="10" name="phoneList[0]" placeholder="Mobile1" id="Mobile1" type="text">
      </div>
      <div class="col-xs-2">
     
        <input class="form-control numberOnly" maxlength="10" name="phoneList[1]" placeholder="Mobile2" id="Mobile2" type="text">
      </div>
	  
      <div class="col-xs-1">
     
        <input class="form-control numberOnly" maxlength="4" name="phoneList[2]" placeholder="STD" id="STDCodeInput" type="text">
      </div>
      <div class="col-xs-2">
        
        <input class="form-control numberOnly" maxlength="8" name="phoneList[3]" placeholder="Landline No." id="LandlineInput" type="text">
      </div>
    </div>
        </div>
			</br>
            					
				
           <div class="row">
             <div class="col-md-12">
							
							  <div class="form-group">
							 
							 <div class="col-xs-2">
        <textarea rows="2" class="form-control" name="addressLine1" placeholder="Address Line1"></textarea>
        
		
      </div>
      <div class="col-xs-2">
       
         <textarea class="form-control" placeholder="Address Line2" name="addressLine2" id="" rows="2" ></textarea>
      </div>
	        </div>
			</div>
			</div>
			</br>
			   <div class="row">
             <div class="col-md-12">
             
             <div class="col-xs-2">
				<select class="form-control" name="state" placeholder="State" id="stateInput" onchange="getCityByStateSelection('stateInput','cityInput');">	
					<option value="0">--SELECT--</option>
			"""),_display_(/*1507.5*/for(states <- statesList) yield /*1507.30*/{_display_(Seq[Any](format.raw/*1507.31*/("""
           
          """),format.raw/*1509.11*/("""<option value=""""),_display_(/*1509.27*/states),format.raw/*1509.33*/("""">"""),_display_(/*1509.36*/states),format.raw/*1509.42*/("""</option>
                              
	  """)))}),format.raw/*1511.5*/("""        """),format.raw/*1511.13*/("""</select>
        
      </div>
       <div class="col-xs-2">

       
		 <select class="form-control" name="city" placeholder="City" id="cityInput">	
                                        
                                    </select>
      </div>
      
       <div class="col-xs-2">
     
        <input class="form-control numberOnly" maxlength="6" name="pincode" value="0"  placeholder="Pin" id="PinInput" type="text">
		<input type="hidden" name="country" value="India">
		
      </div>
      </div>
							 </div>
            
								 <div class="pull-right backToYesSNR ">
				    <button type="button" class="btn btn-primary" value="" id="backToSNR">Back</button>
					<button type="button" class="btn btn-primary" value="" id="NextToPurchaseCar">Next</button>
                </div>
						   
						  </div>
						</div>
								<div class="row animated  bounceInRight" style="display:none;"  id="VehicleSoldClickNo">
						  <div class="col-md-12">
						  <label for=""><b>	Did you purchase another car?<b></label><br>
						     <div class="radio-inline">
                          <label for="">
                            <input type="radio" name="PurchaseYes" value="Purchase Yes" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="">
                            <input type="radio" name="PurchaseYes" value="Purchase No" id="" >
                            No</label> 
                        </div>
						   
						
						
						
							<div class="row animated  bounceInRight" style="display:none;"  id="PurchaseClickYes">
						  <div class="col-md-12">
						 
						   <div class="col-xs-2">
					        <label for=""> New Car Registration No</label>
					        <input class="form-control" id="" name="vehicleRegNo" type="text">
					      </div>
					         <div class="col-xs-2">
					        <label for=""> Brand</label>
					        <input class="form-control" id="" name="variant" type="text">
					      </div>
					         <div class="col-xs-2">
					        <label for=""> Model</label>
					        <input class="form-control" id="" name="model" type="text">
					      </div>
					      <div class="col-xs-2">
					        <label for=""> Dealership Name</label>
					        <input class="form-control" id="" name="dealershipName" type="text">
					      </div>
					      <div class="col-xs-3">
					        <label for=""> Purchase Date</label>
					        <input class="form-control datepicker" name="saleDate" id="" type="text" readonly>
					      </div>
						  </div>
					
						   
						  </div>
							  </div>  
						  <div class="pull-right">
				 <button type="button" class="btn btn-primary" value="" id="BackToPleaseComfNewOwnre">Back</button>
					<button type="button" class="btn btn-primary" value="" id="nextToFinalEditInfoVS">next</button>
                </div>
						</div>
						
 	<div class="row animated  bounceInRight" id="VehicelSoldQuestion" style="display:none;">
				      <div class="col-md-12">
                      <div class="col-md-12">
                        <label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
                        <br>
						<div class="col-md-2">
                        <div class="radio-inline">
                          <label for="feedbackYes">
                            <input type="radio" name="radio3" value="" data-toggle="modal" data-target="#addBtn" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="feedbackNo">
                            <input type="radio" name="radio3" value="" id="" checked>
                            No</label> 
                        </div>
						</div>
						 <div class="col-md-4">
							<label>Remarks</label>
							<textarea class="form-control" rows="2" name="remarksList[3]"></textarea>
						 </div>
                      </div>
                      </div>
                      <div class="pull-right" >
				<button type="button" class="btn btn-primary" id="BackToVSHVNewCar">Back</button>
                 <button type="submit" class="btn btn-primary" value="vehicleSold" name="typeOfsubmit" id="vehicleSold" />Submit</button>
                </div>
                      </div>
                      
					  
					
						
						<div class="checkbox Dissatisfiedwithpreviousservice">
						  <label>
							<input type="checkbox" class="NoService" name="renewalNotRequiredReason" onclick="advisorBasedOnWorkshop();" value="Dissatisfied with previous service" id="Dissatisfiedwithpreviousservice" >
							Dissatisfied with previous service</label>
						</div>

						<div class="row animated  bounceInRight" style="display:none;"  id="txtDissatisfiedwithpreviousservice">
						  <div class="col-md-12">
							
							  <div class="col-md-3">
								<div class="form-group">
								  <label for="lastServiceDateOfDWPS"><b>Last Service Date<b></label>
								 <input type="text" name="lastServiceDateOfDWPS" value=""""),_display_(/*1633.66*/{vehicleData.getLastServiceDate()}),format.raw/*1633.100*/("""" placeholder="Enter From Date" class="form-control datepicker" />
								</div>
							  </div>
							   <div class="col-md-3">
								<div class="form-group">
								  <label for=""><b>Service Advisor ID<b></label>
								  <input type="text" name="serviceAdvisorID" value=""""),_display_(/*1639.62*/{if(latestService.getServiceAdvisor()!=null){latestService.getServiceAdvisor().getAdvisorName()}}),format.raw/*1639.159*/("""" class="form-control"/> 
								</div>
							  </div>
							  
							  <div class="col-md-3">
								<div class="form-group">
								  <label for=""><b>Service Type<b></label>
								  <input type="text" name="lastServiceType" class="form-control"/> 
								</div>
							  </div>
							  <div class="col-md-3">
								<div class="form-group">
								  <label for=""><b>Assigned To<b></label>								  
								  <select class="form-control" name="assignedToSA">
								  <option value=1>"""),_display_(/*1653.28*/{userData.getCreManager()}),format.raw/*1653.54*/("""</option>					  
						
								  </select>
								</div>
							  </div>
							
						   
						  </div>
						  <div class="pull-right" >
				
                  <button type="button" class="btn btn-primary" value="" id="NextDisatisfiedPrPopup"/>Next</button>
                </div>
						</div>
							<div class="row animated  bounceInRight" id="DisatisfiedPreQuestion" style="display:none;">
				      <div class="col-md-12">
                      <div class="col-md-12">
                        <label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
                        <br>
						<div class="col-md-2">
                        <div class="radio-inline">
                          <label for="feedbackYes">
                            <input type="radio" name="radio1" value="" data-toggle="modal" data-target="#addBtn" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="feedbackNo">
                            <input type="radio" name="radio1" value="" id="" checked>
                            No</label> 
                        </div>
						</div>
						 <div class="col-md-4">
							<label>Remarks</label>
							<textarea class="form-control" rows="2" name="remarksList[4]"></textarea>
						 </div>
                      </div>
                      </div>
                      <div class="pull-right" >
				<button type="button" class="btn btn-primary " id="BackToDissatisfiedPreviosInc">Back</button>
                  <button type="submit" class="btn btn-primary" value="dissatifiedwithPreviousService" name="typeOfsubmit" id="dissatifiedwithPreviousService"/>Submit</button>
                </div>
                      </div>
                      
					  
						<div class="checkbox Distancefrom">
						  <label>
							<input type="checkbox" class="NoService" name="renewalNotRequiredReason" value="Distance from Dealer Location" id="Distancefrom" >
							Distance from Dealer Location</label>
						</div>
						<div style="display:none;" class="animated  bounceInRight" id="DistancefromDealerLocationDIV">
						 <div class="col-md-12">
						  <div class="radio">
							<label>
							  <input type="radio" name="DistancefromDealerLocationRadio" value="Transfer to other city" id="TransfertoothercityID" >
							  Transfer to other city</label>
						  </div>
						  </div>
						  <div class="animated  bounceInRight" style="display:none;" id="txtTransfertoothercity">
						  <div class="col-md-12">
							<div class="col-md-3">
							<label for="transferedCity"><b>Enter name of the city<b></label>							
									<!-- <select class="form-control" name="transferedCity">
									"""),_display_(/*1714.11*/for(location_list<-locationList) yield /*1714.43*/{_display_(Seq[Any](format.raw/*1714.44*/("""
                                        """),format.raw/*1715.41*/("""<option value=""""),_display_(/*1715.57*/location_list/*1715.70*/.getName()),format.raw/*1715.80*/("""">"""),_display_(/*1715.83*/location_list/*1715.96*/.getName()),format.raw/*1715.106*/("""</option>
                                        """)))}),format.raw/*1716.42*/("""									
									"""),format.raw/*1717.10*/("""<option value="Others">Others</option>									
									</select> -->
									
									<input type="text" class="form-control" name="transferedCity">
						   
						  </div>
						  </div>
						  </div>
						   <div class="col-md-12">
						  <div class="radio">
							<label>
							  <input type="radio" name="DistancefromDealerLocationRadio" value="Too far" id="ToofarID" onclick="ajaxgetDealerOEM();">
							  Too far(Same city)</label>
						  </div>
						  </div>
						  <div class="animated  bounceInRight" style="display:none;"  id="txtToofar">
							<div class="col-md-12">
							 
								<div class="col-md-3">
								  <label for=""><b>Enter PIN of the city<b></label>
								  <input type="text" name="pinCodeOfCity" class="form-control numberOnly" maxlength="6" />
								</div>
							  
							  
								<div class="col-md-9">
								  <label for=""><b>Would you like to opt for <input type="text" id="oemvalue" style="border:none" readonly/><b></label>
								  <br>
								  <div class="radio-inline">
									<label>
									  <input type="radio" name="optforMMS" value="yes" id="EnterPINthecityID" >
									  Yes</label>
								  </div>
								  <div class="radio-inline">
									<label>
									  <input type="radio" name="optforMMS" value="No" id="EnterPINofthecityID" >
									  NO</label>
								  </div>
								</div>
							
							</div>
						  </div>
				 <div class="pull-right" >
				    <button type="button" class="btn btn-primary" value="" id="NextDistanceForPopup"/>Next</button>
                </div>
						</div>
						<div class="row animated  bounceInRight" id="DistanceFoRRQuestion" style="display:none;">
				      <div class="col-md-12">
                      <div class="col-md-12">
                        <label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
                        <br>
						<div class="col-md-2">
                        <div class="radio-inline">
                          <label for="feedbackYes">
                            <input type="radio" name="radio4" value="" data-toggle="modal" data-target="#addBtn" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="feedbackNo">
                            <input type="radio" name="radio4" value="" id="" checked>
                            No</label> 
                        </div>
						</div>
						 <div class="col-md-4">
							<label>Remarks</label>
							<textarea class="form-control" rows="2" name="remarksList[5]"></textarea>
						 </div>
                      </div>
                      </div>
                      <div class="pull-right" >
				  <button type="button" class="btn btn-primary " id="BackToDissatisfiedDelaerLoc">Back</button>
                  <button type="submit" class="btn btn-primary" value="dissatifiedwithPreviousService" name="typeOfsubmit" id="dissatifiedwithPreviousServices"/>Submit</button>
                </div>
                      </div>
                      
						
						
						<div class="checkbox DissatisfiedwithSalesID">
						  <label>
							<input type="checkbox" class="NoService" name="renewalNotRequiredReason" onclick="ajaxLeadOnDissatisfiedWithSales(7);" value="Dissatisfied with Sales" id="DissatisfiedwithSalesID" >
							Dissatisfaction with Sales</label>
 						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="DissatisfactionwithSalesREmarksDiv">
						  <div class="col-md-12">
							
							  <div class="col-md-3">
								<label for="">Tag to</label>
								<select class="form-control" id="noServiceReasonTaggedTo" name="noServiceReasonTaggedTo">
								  <option value="select">select</option>
								  
								</select>
							  </div>
							
							  <div class="col-md-3">
								<label for="">Remarks</label>
								<textarea type="text" rows="1" name="noServiceReasonTaggedToComments" class="form-control"></textarea>
							  </div>
							
						  </div>
						   <div class="pull-right" >
				    <button type="button" class="btn btn-primary" value="" id="NextDisSatisSalePopup"/>Next</button>
                </div>
						</div>
						<div class="row animated  bounceInRight" id="DisstisFiedSaleRQuestion" style="display:none;">
				      <div class="col-md-12">
                      <div class="col-md-12">
                        <label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
                        <br>
						<div class="col-md-2">
                        <div class="radio-inline">
                          <label for="feedbackYes">
                            <input type="radio" name="radio5" value="" data-toggle="modal" data-target="#addBtn" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="feedbackNo">
                            <input type="radio" name="radio5" value="" id="" checked>
                            No</label> 
                        </div>
						</div>
						 <div class="col-md-4">
							<label>Remarks</label>
							<textarea class="form-control" rows="2" name="remarksList[6]"></textarea>
						 </div>
                      </div>
                      </div>
                      <div class="pull-right" >
				
				<button type="button" class="btn btn-primary" id="backToDissSatiSalse"/>Back</button>
                  <button type="submit" class="btn btn-primary" value="dissatifiedwithPreviousService" name="typeOfsubmit" id="dissatifiedwithPService"/>Submit</button>
                </div>
                      </div>
					  
						<div class="checkbox DissatisfiedwithInsuranceId">
						  <label>
							<input type="checkbox"  class="NoService" name="renewalNotRequiredReason" onclick="ajaxLeadOnDissatisfiedWithSales(1);" value="Dissatisfied with Insurance" id="DissatisfiedwithInsuranceId" >
							Dissatisfaction with Insurance</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="DissatisfactionwithInsuranceREmarksDiv">
						  <div class="col-md-12">
							
							  <div class="col-md-3">
								<label for="">Tag the case to</label>
								<select class="form-control" id="noServiceReasonTaggedTo1" name="noServiceReasonTaggedTo1">
								  <option value="select">select</option>
								</select>
							  </div>
							  <div class="col-md-3">
								<label for="">Remarks</label>
								<textarea type="text" rows="1" name="noServiceReasonTaggedToComments1" class="form-control"></textarea>
							  </div>
							
						   
						  </div>
						  <div class="pull-right" >
				    <button type="button" class="btn btn-primary" value="" id="NextDisSatInsurancPopup"/>Next</button>
                </div>
						</div>
								<div class="row animated  bounceInRight" id="DisstisInsurancQuestion" style="display:none;">
				      <div class="col-md-12">
                      <div class="col-md-12">
                        <label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
                        <br>
						<div class="col-md-2">
                        <div class="radio-inline">
                          <label for="feedbackYes">
                            <input type="radio" name="radio6" value="" data-toggle="modal" data-target="#addBtn" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="feedbackNo">
                            <input type="radio" name="radio6" value="" id="" checked>
                            No</label> 
                        </div>
						</div>
						 <div class="col-md-4">
							<label>Remarks</label>
							<textarea class="form-control" rows="2" name="remarksList[7]"></textarea>
						 </div>
                      </div>
                      </div>
                      <div class="pull-right" >
				<button type="button" class="btn btn-primary" value="" id="bsckTodisInsu"/>Back</button>
                  <button type="submit" class="btn btn-primary" value="dissatifiedwithPreviousService" name="typeOfsubmit" id="dissatifiedwithService"/>Submit</button>
                </div>
                      </div>
					  
					  <div class="checkbox ExcBillingId">
						  <label>
							<input type="checkbox"  class="NoService" name="renewalNotRequiredReason" value="Excess Billing" id="ExcBillingId">
							Excess Billing</label>
							
						</div>
						 <div class="checkbox Stolen">
						  <label>
							<input type="checkbox"  class="NoService" name="renewalNotRequiredReason" value="Stolen" id="Stolen" >
							Stolen</label>
							
						</div>
						
						 <div class="checkbox Totalloss">
						  <label>
							<input type="checkbox"  class="NoService" name="renewalNotRequiredReason" value="Total loss" id="Totalloss" >
							Total loss/Damaged</label>
							
						</div>
						<div class="pull-right animated  bounceInRight" style="display:none;" id="stolenHideShowSubmit">
				    <button type="submit" class="btn btn-primary" value="dissatifiedwithPreviousService" name="typeOfsubmit" id="stolenDamageSubmit"/>Submit</button>
                </div>
						
						
						<div class="checkbox OtherLast">
						  <label>
							<input type="checkbox"  class="NoService" name="renewalNotRequiredReason" value="Other Service" id="Other" >
							Other</label>
						</div>
						<div class="animated  bounceInRight" style="display:none;" id="OtherSeriveRemarks">
						  
						   <div class="col-md-3">
							  <label for="comments">Enter other reason for no service</label>
							  <textarea class="form-control" rows="1" id="commentsOtherRemarks" name="reason" ></textarea>
						   </div>
						 
						 	  <div class="pull-right" >
				    <button type="button" class="btn btn-primary" value="" id="NextOthersPopup"/>Next</button>
                </div>
						</div>
					<div class="row animated  bounceInRight" id="OthersLastQuestion" style="display:none;">
				      <div class="col-md-12">
                      <div class="col-md-12">
                        <label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
                        <br>
						 <div class="col-md-2">
                        <div class="radio-inline">
                          <label for="feedbackYes">
                            <input type="radio" name="radio8" value="" data-toggle="modal" data-target="#addBtn" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="feedbackNo">
                            <input type="radio" name="radio8" value="" id="" checked>
                            No</label> 
                        </div>
                        </div>
						 <div class="col-md-4">
							<label>Remarks</label>
							<textarea class="form-control" rows="2" name="remarksList[8]"></textarea>
						 </div>
                      </div>
                      </div>
                      <div class="pull-right" >
				<button type="button" class="btn btn-primary" id="BacktoOtherInsu">Back</button>
                  <button type="submit" class="btn btn-primary" value="dissatifiedwithPreviousService" name="typeOfsubmit" id="vehicleSoldStolen"/>Submit</button>
                </div>
                      </div>
					
					</div>
				</div>
						</div>
<!--Out Bound Upsel Opportunity---------->
			
		
	 
			
		  <div class="col-md-4" id="NoComments" style="display:none;">
			<div class="row">
			  <label for="comments"><b>Remarks<b></label>
			  <textarea class="form-control" rows="2" id="comments" name="remarksList[9]"></textarea>
			</div>
		  </div>
		 
		
<!--  Inbound Call-->



				  
				  """))
      }
    }
  }

  def render(statesList:List[String],insurAgent:List[InsuranceAgent],workshopList:List[Workshop],customerData:Customer,vehicleData:Vehicle,locationList:List[Location],interOfCall:CallInteraction,servicetypeList:List[ServiceTypes],userData:WyzUser,latestService:Service): play.twirl.api.HtmlFormat.Appendable = apply(statesList,insurAgent,workshopList,customerData,vehicleData,locationList,interOfCall,servicetypeList,userData,latestService)

  def f:((List[String],List[InsuranceAgent],List[Workshop],Customer,Vehicle,List[Location],CallInteraction,List[ServiceTypes],WyzUser,Service) => play.twirl.api.HtmlFormat.Appendable) = (statesList,insurAgent,workshopList,customerData,vehicleData,locationList,interOfCall,servicetypeList,userData,latestService) => apply(statesList,insurAgent,workshopList,customerData,vehicleData,locationList,interOfCall,servicetypeList,userData,latestService)

  def ref: this.type = this

}


}

/**/
object insuranceOutBoundPage extends insuranceOutBoundPage_Scope0.insuranceOutBoundPage
              /*
                  -- GENERATED --
                  DATE: Wed Jan 10 12:18:16 IST 2018
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/insuranceOutBoundPage.scala.html
                  HASH: e207705f8fa62ee4aa0f39c003af11b9cb78e0fe
                  MATRIX: 903->3|1256->260|1290->268|1365->317|1516->447|1912->817|1967->856|2006->857|2098->921|2141->937|2154->941|2186->952|2216->955|2229->959|2263->972|2347->1025|2416->1067|2740->1364|2795->1398|2825->1401|2881->1435|7903->6430|7966->6476|8006->6477|8052->6496|8108->6542|8148->6543|8205->6571|8249->6587|8269->6597|8309->6615|8339->6617|8359->6627|8400->6645|8431->6647|8452->6657|8493->6675|8524->6677|8545->6687|8578->6697|8609->6699|8630->6709|8666->6722|8698->6725|8719->6735|8760->6753|8791->6755|8812->6765|8853->6783|8884->6785|8905->6795|8946->6813|8977->6815|8998->6825|9031->6835|9062->6837|9083->6847|9119->6860|9176->6897|9190->6901|9230->6902|9314->6957|9358->6973|9378->6983|9424->7007|9454->7009|9474->7019|9506->7029|9537->7031|9558->7041|9594->7054|9626->7057|9647->7067|9694->7091|9725->7093|9746->7103|9779->7113|9810->7115|9831->7125|9867->7138|9936->7175|9979->7186|10013->7192|34642->31774|34893->32004|34931->32014|38967->36022|39023->36056|69559->66544|69602->66569|69643->66570|69698->66595|69743->66611|69772->66617|69804->66620|69833->66626|69912->66673|69950->66681|75287->71989|75345->72023|75662->72311|75783->72408|76328->72924|76377->72950|79277->75821|79327->75853|79368->75854|79440->75896|79485->75912|79509->75925|79542->75935|79574->75938|79598->75951|79632->75961|79717->76013|79767->76033
                  LINES: 27->1|32->1|34->3|34->3|34->3|46->15|46->15|46->15|48->17|48->17|48->17|48->17|48->17|48->17|48->17|50->19|55->24|64->33|64->33|64->33|64->33|223->192|223->192|223->192|225->194|225->194|225->194|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|226->195|227->196|227->196|227->196|229->198|229->198|229->198|229->198|229->198|229->198|229->198|229->198|229->198|229->198|229->198|229->198|229->198|229->198|229->198|229->198|229->198|229->198|229->198|230->199|231->200|232->201|823->791|827->795|828->796|927->895|927->895|1540->1507|1540->1507|1540->1507|1542->1509|1542->1509|1542->1509|1542->1509|1542->1509|1544->1511|1544->1511|1666->1633|1666->1633|1672->1639|1672->1639|1686->1653|1686->1653|1747->1714|1747->1714|1747->1714|1748->1715|1748->1715|1748->1715|1748->1715|1748->1715|1748->1715|1748->1715|1749->1716|1750->1717
                  -- GENERATED --
              */
          