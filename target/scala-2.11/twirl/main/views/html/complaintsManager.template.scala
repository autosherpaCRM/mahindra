
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object complaintsManager_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class complaintsManager extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[List[ComplaintSource],List[Workshop],String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(sourcelist:List[ComplaintSource],listwork:List[Workshop],dealercode:String,dealerName:String,userName:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.112*/("""

"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*4.56*/ {_display_(Seq[Any](format.raw/*4.58*/("""
"""),_display_(/*5.2*/helper/*5.8*/.form(action = routes.CallInteractionController.postComplaintsByManager)/*5.80*/ {_display_(Seq[Any](format.raw/*5.82*/("""
"""),format.raw/*6.1*/("""<div class="row" >
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading">   
        COMPLAINT REGISTRATION FORM </div>
		<div class="panel-body">
        <h4>CUSTOMER DETAILS :</h4>
              <div class="col-lg-3">
               <div class="form-group">
                <label for="vehicleRegNo">Vehicle Number <i style="color:red">*</i></label>
               
                	<div class="input-group">
                              <input type="text" class="form-control" name="vehicleRegNo" maxlength="12"  min="8" max="12" id="vehicleRegNo" autocomplete="off"/>
                              <span class="input-group-addon" data-toggle="modal" data-target="#searchPopup" id="searchveh" onclick="ajaxsearchVehicle();"><i class="fa fa-search"></i> </span>
                      </div>
                          </div>
              </div>
          <div class="col-lg-3">
               <div class="form-group">
                <label for="model">Model <i style="color:red">*</i></label>
                <input type="text" class="form-control" name="model" maxlength="20" id="model" autocomplete="off" />
              </div>
            </div>
       <div class="col-lg-3">
               <div class="form-group">
                <label for="customerName">Customer Name <i style="color:red">*</i></label>
                <input type="text" class="form-control customerNameComplain" name="customerName"  id="customerName" value="" />
              </div>
            </div>
        
      <div class="col-lg-3">             
              <div class="form-group">
                <label for="variant">Variant <i style="color:red">*</i></label>
                <input type="text" class="form-control" name="variant" maxlength="20"  id="variant" autocomplete="off" />
              </div>
            </div>
       
            <div class="col-lg-3">
               <div class="form-group">
                <label for="customerPhone">Customer Phone <i style="color:red">*</i></label>
                <input type="text" class="form-control number-only" name="customerPhone" maxlength="10"   id="customerPhone" autocomplete="off" />
              </div>
            </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="saledate">Sale Date <i style="color:red">*</i></label>
               <input type="text"  class="form-control datepickerPrevious" name="saleDate" id="saledate" autocomplete="off" readonly/>
              </div>
            </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="email">Email <i style="color:red">*</i></label>
                <input type="text" class="form-control customerEmail" name="emailAddress" id="email" autocomplete="off"/>
              </div>
            </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="lastservice">Last Service Date<i style="color:red">*</i></label>
               <input type="text"  class="form-control rangedatepicker" name="lastServiceDate" id="lastservice" autocomplete="off" readonly/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="address">Address <i style="color:red">*</i></label>
                <textarea type="text" class="form-control" name="addressLine1" rows="1" id="address"></textarea>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="serviceadvisor">Service Advisor <i style="color:red">*</i></label>
                <input type="text" class="form-control" name="serviceadvisor" id="serviceadvisor" autocomplete="off"/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="chassisno">Chassis No <i style="color:red">*</i></label>
               <input type="text" class="form-control" name="chassisNo"  maxlength="40" id="chassisno" autocomplete="off"/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="workshop">Workshop <i style="color:red">*</i></label>
              <!--  <input type="text" class="form-control" name="workshop"  maxlength="30" id="workshop" autocomplete="off"/> -->
              <select class="form-control" name="workshop"  maxlength="30" id="workshop">
               
               """),_display_(/*91.17*/for(sa <- listwork) yield /*91.36*/{_display_(Seq[Any](format.raw/*91.37*/("""
               """),format.raw/*92.16*/("""<option value=""""),_display_(/*92.32*/sa/*92.34*/.getWorkshopName()),format.raw/*92.52*/("""">"""),_display_(/*92.55*/sa/*92.57*/.getWorkshopName()),format.raw/*92.75*/("""</option>
               """)))}),format.raw/*93.17*/("""
               """),format.raw/*94.16*/("""</select>
              </div>
            </div>
            <hr>
              <h4>COMPLAINT INFORMATION:</h4>
                  <div class="col-lg-3">
             <div class="form-group">
                <label for="sname">Source<i style="color:red">*</i></label>
                <select class="form-control" id="sname" name="sourceName">
                    <option value="0">Select Source</option>
                   """),_display_(/*104.21*/for(li <- sourcelist) yield /*104.42*/{_display_(Seq[Any](format.raw/*104.43*/("""
                    	
                    	"""),format.raw/*106.22*/("""<option value=""""),_display_(/*106.38*/li/*106.40*/.getComplaintSource()),format.raw/*106.61*/("""">"""),_display_(/*106.64*/li/*106.66*/.getComplaintSource()),format.raw/*106.87*/("""</option>
                    	
                    """)))}),format.raw/*108.22*/("""
                   
                  """),format.raw/*110.19*/("""</select><span style="color:red;"></span>
             </div>
        </div>
                <div class="col-lg-3">
               <div class="form-group">
                <label for="functame">Function <i style="color:red">*</i></label>
               
                <select class="form-control" id="functame" name="functionName">
                	<option value="0">Select Function</option>                                       
                                        <option value="finance">FINANCE</option>
                                        <option value="insurance">INSURANCE</option>
                                        <option value="parts">PARTS</option>
                                        <option value="product">PRODUCT</option>
                                        <option value="sales">SALES</option>
                                        <option value="usedcarsales">USED CAR SALES</option>
	 </select>
              </div>
            </div>
              
     <div class="col-lg-3 complainCategory" style="display:none">
             <div class="form-group">
                <label for="complaintType">Complaint Type CATEGORY:<i style="color:red">*</i></label>
                <select class="form-control" id="complaintType" name="complaintType">
                    <option value="0">Select Complaint Type</option>
                      <option value="Technical">Technical</option>
                      <option value="Service">Service</option>
                      <option value="Bodyshop">Bodyshop</option>
                  </select><span style="color:red;"></span>
             </div>
        </div>
              
            <div class="col-lg-3 complainSubCategory" style="display:none">
             <div class="form-group">
                <label for="subcomplaintType">Sub CATEGORY:<i style="color:red">*</i></label>
                <select class="form-control" id="subcomplaintType" name="subcomplaintType">
                    <option value="0">Select subComplaint Type</option>
                      <option value="Service">Service</option>
                      <option value="Repair">Repair</option>
                      <option value="Others">Others</option>
                  </select><span style="color:red;"></span>
             </div>
             </div>
                 <div class="col-lg-3">
              <div class="form-group">
                <label for="description">Description:</label>
                <textarea  class="form-control" name="description" id="description" autocomplete="off"></textarea>
              </div>
            </div>
              <div class="col-lg-12">                  
             <div class='text-right'>
                <input type="submit" class="btn btn-primary"  id="complaintSubmit" value="Submit" />
              </div>
              </div>

             </div>
         </div>
     </div>
</div>
     """)))}),format.raw/*168.7*/("""
     """)))}),format.raw/*169.7*/("""
     
     """),format.raw/*171.6*/("""<script>
   
    $(document).ready(function() """),format.raw/*173.34*/("""{"""),format.raw/*173.35*/("""
       """),format.raw/*174.8*/("""var stateObject = """),format.raw/*174.26*/("""{"""),format.raw/*174.27*/("""
    """),format.raw/*175.5*/(""""AFTERSALES": """),format.raw/*175.19*/("""{"""),format.raw/*175.20*/("""
        """),format.raw/*176.9*/(""""Technical": ["Accessories", "Body","Brake","Electrical","Engine","Hvac","Steering","Transmission","Feedback Product"],
        "Service": ["Advisor", "Quality","Sales Related","Spares","Warrantly","Initiation","PickUp","FeedBack-Service"],	
        "Bodyshop": ["Poor WorkManship-Denting,Fitment","Poor WorkManship-Painting","Delay In Delivery","High Repair Charges","Insurance"]
    """),format.raw/*179.5*/("""}"""),format.raw/*179.6*/("""
  
"""),format.raw/*181.1*/("""}"""),format.raw/*181.2*/("""
"""),format.raw/*182.1*/("""window.onload = function () """),format.raw/*182.29*/("""{"""),format.raw/*182.30*/("""
    """),format.raw/*183.5*/("""var stateSel = document.getElementById("functame"),
        countySel = document.getElementById("complaintType"),
        citySel = document.getElementById("subcomplaintType");
    for (var state in stateObject) """),format.raw/*186.36*/("""{"""),format.raw/*186.37*/("""
        """),format.raw/*187.9*/("""stateSel.options[stateSel.options.length] = new Option(state, state);
    """),format.raw/*188.5*/("""}"""),format.raw/*188.6*/("""
    """),format.raw/*189.5*/("""stateSel.onchange = function () """),format.raw/*189.37*/("""{"""),format.raw/*189.38*/("""
        """),format.raw/*190.9*/("""countySel.length = 1; // remove all options bar first
        citySel.length = 1; // remove all options bar first
        if (this.selectedIndex < 1) return; // done   
        for (var county in stateObject[this.value]) """),format.raw/*193.53*/("""{"""),format.raw/*193.54*/("""
            """),format.raw/*194.13*/("""countySel.options[countySel.options.length] = new Option(county, county);
        """),format.raw/*195.9*/("""}"""),format.raw/*195.10*/("""
    """),format.raw/*196.5*/("""}"""),format.raw/*196.6*/("""
    """),format.raw/*197.5*/("""stateSel.onchange(); // reset in case page is reloaded
    countySel.onchange = function () """),format.raw/*198.38*/("""{"""),format.raw/*198.39*/("""
        """),format.raw/*199.9*/("""citySel.length = 1; // remove all options bar first
        if (this.selectedIndex < 1) return; // done   
        var cities = stateObject[stateSel.value][this.value];
        for (var i = 0; i < cities.length; i++) """),format.raw/*202.49*/("""{"""),format.raw/*202.50*/("""
            """),format.raw/*203.13*/("""citySel.options[citySel.options.length] = new Option(cities[i], cities[i]);
        """),format.raw/*204.9*/("""}"""),format.raw/*204.10*/("""
    """),format.raw/*205.5*/("""}"""),format.raw/*205.6*/("""
    """),format.raw/*206.5*/("""}"""),format.raw/*206.6*/("""
    """),format.raw/*207.5*/("""}"""),format.raw/*207.6*/(""");
 </script>
      
   """))
      }
    }
  }

  def render(sourcelist:List[ComplaintSource],listwork:List[Workshop],dealercode:String,dealerName:String,userName:String): play.twirl.api.HtmlFormat.Appendable = apply(sourcelist,listwork,dealercode,dealerName,userName)

  def f:((List[ComplaintSource],List[Workshop],String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (sourcelist,listwork,dealercode,dealerName,userName) => apply(sourcelist,listwork,dealercode,dealerName,userName)

  def ref: this.type = this

}


}

/**/
object complaintsManager extends complaintsManager_Scope0.complaintsManager
              /*
                  -- GENERATED --
                  DATE: Thu Dec 14 12:08:24 IST 2017
                  SOURCE: D:/CRM_AutosherpaMahindra/mahindra/app/views/complaintsManager.scala.html
                  HASH: 22d498eee3fe6817dd0ace316c3226cf9f3321a3
                  MATRIX: 820->3|1026->113|1056->118|1118->172|1157->174|1185->177|1198->183|1278->255|1317->257|1345->259|6041->4928|6076->4947|6115->4948|6160->4965|6203->4981|6214->4983|6253->5001|6283->5004|6294->5006|6333->5024|6391->5051|6436->5068|6898->5502|6936->5523|6976->5524|7051->5570|7095->5586|7107->5588|7150->5609|7181->5612|7193->5614|7236->5635|7323->5690|7393->5731|10394->8701|10433->8709|10475->8723|10552->8771|10582->8772|10619->8781|10666->8799|10696->8800|10730->8806|10773->8820|10803->8821|10841->8831|11257->9219|11286->9220|11320->9226|11349->9227|11379->9229|11436->9257|11466->9258|11500->9264|11744->9479|11774->9480|11812->9490|11915->9565|11944->9566|11978->9572|12039->9604|12069->9605|12107->9615|12360->9839|12390->9840|12433->9854|12544->9937|12574->9938|12608->9944|12637->9945|12671->9951|12793->10044|12823->10045|12861->10055|13110->10275|13140->10276|13183->10290|13296->10375|13326->10376|13360->10382|13389->10383|13423->10389|13452->10390|13486->10396|13515->10397
                  LINES: 27->2|32->2|34->4|34->4|34->4|35->5|35->5|35->5|35->5|36->6|121->91|121->91|121->91|122->92|122->92|122->92|122->92|122->92|122->92|122->92|123->93|124->94|134->104|134->104|134->104|136->106|136->106|136->106|136->106|136->106|136->106|136->106|138->108|140->110|198->168|199->169|201->171|203->173|203->173|204->174|204->174|204->174|205->175|205->175|205->175|206->176|209->179|209->179|211->181|211->181|212->182|212->182|212->182|213->183|216->186|216->186|217->187|218->188|218->188|219->189|219->189|219->189|220->190|223->193|223->193|224->194|225->195|225->195|226->196|226->196|227->197|228->198|228->198|229->199|232->202|232->202|233->203|234->204|234->204|235->205|235->205|236->206|236->206|237->207|237->207
                  -- GENERATED --
              */
          