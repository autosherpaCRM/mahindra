
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/CRM_AutosherpaMahindra/mahindra/conf/routes
// @DATE:Thu Jan 04 13:05:46 IST 2018

package org.pac4j.play;

import router.RoutesPrefix;

public class routes {
  
  public static final org.pac4j.play.ReverseCallbackController CallbackController = new org.pac4j.play.ReverseCallbackController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final org.pac4j.play.javascript.ReverseCallbackController CallbackController = new org.pac4j.play.javascript.ReverseCallbackController(RoutesPrefix.byNamePrefix());
  }

}
