
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/CRM_AutosherpaMahindra/mahindra/conf/routes
// @DATE:Thu Jan 04 13:05:46 IST 2018

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:13
package org.pac4j.play.javascript {
  import ReverseRouteContext.empty

  // @LINE:13
  class ReverseCallbackController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:13
    def callback: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "org.pac4j.play.CallbackController.callback",
      """
        function() {
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "callback"})
          }
        
        }
      """
    )
  
  }


}
