
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/CRM_AutosherpaMahindra/mahindra/conf/routes
// @DATE:Thu Jan 04 13:05:46 IST 2018

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseDealerController DealerController = new controllers.ReverseDealerController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseCallRecordingHistoryController CallRecordingHistoryController = new controllers.ReverseCallRecordingHistoryController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAudioStreamController AudioStreamController = new controllers.ReverseAudioStreamController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseCustomerScheduledController CustomerScheduledController = new controllers.ReverseCustomerScheduledController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseWyzUserController WyzUserController = new controllers.ReverseWyzUserController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseSMSTemplateController SMSTemplateController = new controllers.ReverseSMSTemplateController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAllAudioConverter AllAudioConverter = new controllers.ReverseAllAudioConverter(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseServiceAdvisorController ServiceAdvisorController = new controllers.ReverseServiceAdvisorController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseSuperAdminController SuperAdminController = new controllers.ReverseSuperAdminController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReversePSFController PSFController = new controllers.ReversePSFController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseSMSandEmailController SMSandEmailController = new controllers.ReverseSMSandEmailController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseFileUploadController FileUploadController = new controllers.ReverseFileUploadController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAutoSelInsuranceAgentController AutoSelInsuranceAgentController = new controllers.ReverseAutoSelInsuranceAgentController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseCallInteractionController CallInteractionController = new controllers.ReverseCallInteractionController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseUploadExcelController UploadExcelController = new controllers.ReverseUploadExcelController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseCallInfoController CallInfoController = new controllers.ReverseCallInfoController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseInsuranceController InsuranceController = new controllers.ReverseInsuranceController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseServiceBookController ServiceBookController = new controllers.ReverseServiceBookController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAllCallInteractionController AllCallInteractionController = new controllers.ReverseAllCallInteractionController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseSearchControllerMR SearchControllerMR = new controllers.ReverseSearchControllerMR(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAutoSelectionSAController AutoSelectionSAController = new controllers.ReverseAutoSelectionSAController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseApplication Application = new controllers.ReverseApplication(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseChangeAssignmentController ChangeAssignmentController = new controllers.ReverseChangeAssignmentController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseInsuranceHistoryController InsuranceHistoryController = new controllers.ReverseInsuranceHistoryController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseProcessUploadedFiles ProcessUploadedFiles = new controllers.ReverseProcessUploadedFiles(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseSearchController SearchController = new controllers.ReverseSearchController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseCampaignController CampaignController = new controllers.ReverseCampaignController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseReportsController ReportsController = new controllers.ReverseReportsController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseUserAuthenticator UserAuthenticator = new controllers.ReverseUserAuthenticator(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseFirebaseSyncController FirebaseSyncController = new controllers.ReverseFirebaseSyncController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseScheduledCallController ScheduledCallController = new controllers.ReverseScheduledCallController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseDealerController DealerController = new controllers.javascript.ReverseDealerController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseCallRecordingHistoryController CallRecordingHistoryController = new controllers.javascript.ReverseCallRecordingHistoryController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAudioStreamController AudioStreamController = new controllers.javascript.ReverseAudioStreamController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseCustomerScheduledController CustomerScheduledController = new controllers.javascript.ReverseCustomerScheduledController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseWyzUserController WyzUserController = new controllers.javascript.ReverseWyzUserController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseSMSTemplateController SMSTemplateController = new controllers.javascript.ReverseSMSTemplateController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAllAudioConverter AllAudioConverter = new controllers.javascript.ReverseAllAudioConverter(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseServiceAdvisorController ServiceAdvisorController = new controllers.javascript.ReverseServiceAdvisorController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseSuperAdminController SuperAdminController = new controllers.javascript.ReverseSuperAdminController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReversePSFController PSFController = new controllers.javascript.ReversePSFController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseSMSandEmailController SMSandEmailController = new controllers.javascript.ReverseSMSandEmailController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseFileUploadController FileUploadController = new controllers.javascript.ReverseFileUploadController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAutoSelInsuranceAgentController AutoSelInsuranceAgentController = new controllers.javascript.ReverseAutoSelInsuranceAgentController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseCallInteractionController CallInteractionController = new controllers.javascript.ReverseCallInteractionController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseUploadExcelController UploadExcelController = new controllers.javascript.ReverseUploadExcelController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseCallInfoController CallInfoController = new controllers.javascript.ReverseCallInfoController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseInsuranceController InsuranceController = new controllers.javascript.ReverseInsuranceController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseServiceBookController ServiceBookController = new controllers.javascript.ReverseServiceBookController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAllCallInteractionController AllCallInteractionController = new controllers.javascript.ReverseAllCallInteractionController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseSearchControllerMR SearchControllerMR = new controllers.javascript.ReverseSearchControllerMR(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAutoSelectionSAController AutoSelectionSAController = new controllers.javascript.ReverseAutoSelectionSAController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseChangeAssignmentController ChangeAssignmentController = new controllers.javascript.ReverseChangeAssignmentController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseInsuranceHistoryController InsuranceHistoryController = new controllers.javascript.ReverseInsuranceHistoryController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseProcessUploadedFiles ProcessUploadedFiles = new controllers.javascript.ReverseProcessUploadedFiles(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseSearchController SearchController = new controllers.javascript.ReverseSearchController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseCampaignController CampaignController = new controllers.javascript.ReverseCampaignController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseReportsController ReportsController = new controllers.javascript.ReverseReportsController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseUserAuthenticator UserAuthenticator = new controllers.javascript.ReverseUserAuthenticator(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseFirebaseSyncController FirebaseSyncController = new controllers.javascript.ReverseFirebaseSyncController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseScheduledCallController ScheduledCallController = new controllers.javascript.ReverseScheduledCallController(RoutesPrefix.byNamePrefix());
  }

}
