
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/CRM_AutosherpaMahindra/mahindra/conf/routes
// @DATE:Thu Jan 04 13:05:46 IST 2018

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:7
package controllers.javascript {
  import ReverseRouteContext.empty

  // @LINE:168
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:168
    def at: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.at",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:35
  class ReverseDealerController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:35
    def adddel: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DealerController.adddel",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/addDealer"})
        }
      """
    )
  
    // @LINE:529
    def getOEMOfDealer: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DealerController.getOEMOfDealer",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getOEMOfDealer"})
        }
      """
    )
  
    // @LINE:41
    def geteditDealer: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DealerController.geteditDealer",
      """
        function(did0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/dealer/edit/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("did", did0)})
        }
      """
    )
  
    // @LINE:39
    def deleteDealerData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DealerController.deleteDealerData",
      """
        function(did0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/dealer/delete/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("did", did0)})
        }
      """
    )
  
    // @LINE:42
    def postEditDealer: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DealerController.postEditDealer",
      """
        function(did0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/edit/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("did", did0)})
        }
      """
    )
  
    // @LINE:38
    def dealerInformation: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DealerController.dealerInformation",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/dealerList"})
        }
      """
    )
  
    // @LINE:36
    def adddealerForm: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DealerController.adddealerForm",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/addDealer"})
        }
      """
    )
  
  }

  // @LINE:631
  class ReverseCallRecordingHistoryController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:635
    def callRecordingDataInsurance: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallRecordingHistoryController.callRecordingDataInsurance",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/recordingDataListInsurance"})
        }
      """
    )
  
    // @LINE:640
    def otherCallRecordingView: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallRecordingHistoryController.otherCallRecordingView",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/otherCallRecording"})
        }
      """
    )
  
    // @LINE:634
    def callRecordingViewInsurance: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallRecordingHistoryController.callRecordingViewInsurance",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/callRecordingInsurance"})
        }
      """
    )
  
    // @LINE:631
    def callRecordingView: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallRecordingHistoryController.callRecordingView",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/callRecording"})
        }
      """
    )
  
    // @LINE:632
    def callRecordingData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallRecordingHistoryController.callRecordingData",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/recordingDataList"})
        }
      """
    )
  
    // @LINE:638
    def callRecordingDataPSF: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallRecordingHistoryController.callRecordingDataPSF",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/recordingDataListPSF"})
        }
      """
    )
  
    // @LINE:637
    def callRecordingViewPSF: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallRecordingHistoryController.callRecordingViewPSF",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/callRecordingPSF"})
        }
      """
    )
  
    // @LINE:641
    def otherCallRecordingData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallRecordingHistoryController.otherCallRecordingData",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/otherRecordingDataList"})
        }
      """
    )
  
  }

  // @LINE:622
  class ReverseAudioStreamController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:622
    def header: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AudioStreamController.header",
      """
        function(interactionId0) {
          return _wA({method:"HEAD", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/audiostream/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("interactionId", encodeURIComponent(interactionId0))})
        }
      """
    )
  
    // @LINE:623
    def streamAudio: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AudioStreamController.streamAudio",
      """
        function(interactionId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/audiostream/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("interactionId", encodeURIComponent(interactionId0))})
        }
      """
    )
  
  }

  // @LINE:186
  class ReverseCustomerScheduledController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:186
    def readCustomersFromCSV: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CustomerScheduledController.readCustomersFromCSV",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "customers/readCustomersFromCSV"})
        }
      """
    )
  
    // @LINE:188
    def addcustomerInfo: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CustomerScheduledController.addcustomerInfo",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/addcustomer"})
        }
      """
    )
  
  }

  // @LINE:10
  class ReverseWyzUserController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:63
    def viewReportCREManager: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.viewReportCREManager",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/viewReport"})
        }
      """
    )
  
    // @LINE:28
    def userInformation: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.userInformation",
      """
        function() {
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/userList"})
          }
        
        }
      """
    )
  
    // @LINE:161
    def indexPageSalesManager: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.indexPageSalesManager",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "SalesManager"})
        }
      """
    )
  
    // @LINE:58
    def loginfo: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.loginfo",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/loginformation"})
        }
      """
    )
  
    // @LINE:396
    def changepasswordServiceAdvisor: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.changepasswordServiceAdvisor",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ServiceAdvisor/changepassword"})
        }
      """
    )
  
    // @LINE:57
    def changepasswordeditingsuperAdmin: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.changepasswordeditingsuperAdmin",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/passwordchangesuperAdmin"})
        }
      """
    )
  
    // @LINE:26
    def addApplicationUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.addApplicationUser",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/addUser"})
        }
      """
    )
  
    // @LINE:56
    def changepasswordsuperAdmin: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.changepasswordsuperAdmin",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/changepasswordsuperAdmin"})
        }
      """
    )
  
    // @LINE:104
    def indexPageCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.indexPageCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE"})
        }
      """
    )
  
    // @LINE:110
    def changepasswordCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.changepasswordCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/changepasswordcre"})
        }
      """
    )
  
    // @LINE:31
    def geteditUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.geteditUser",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/user/edit/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:25
    def addapp: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.addapp",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/addUser"})
        }
      """
    )
  
    // @LINE:18
    def logoutParticularUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.logoutParticularUser",
      """
        function(userIs0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logoutParticularUser/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("userIs", encodeURIComponent(userIs0))})
        }
      """
    )
  
    // @LINE:33
    def deleteUserData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.deleteUserData",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/user/delete/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:397
    def changepasswordeditingServiceAdvisor: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.changepasswordeditingServiceAdvisor",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "ServiceAdvisor/passwordchange"})
        }
      """
    )
  
    // @LINE:11
    def logout: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.logout",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logoutUser"})
        }
      """
    )
  
    // @LINE:401
    def indexPageOthers: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.indexPageOthers",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "OthersLogins"})
        }
      """
    )
  
    // @LINE:394
    def indexPageServiceAdvisor: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.indexPageServiceAdvisor",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ServiceAdvisor"})
        }
      """
    )
  
    // @LINE:163
    def viewReport: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.viewReport",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "SalesManager/viewReport"})
        }
      """
    )
  
    // @LINE:10
    def changePassword: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.changePassword",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "changePassword"})
        }
      """
    )
  
    // @LINE:66
    def changepassword: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.changepassword",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/changepassword"})
        }
      """
    )
  
    // @LINE:67
    def changepasswordediting: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.changepasswordediting",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/passwordchange"})
        }
      """
    )
  
    // @LINE:111
    def changepasswordeditingCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.changepasswordeditingCRE",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/passwordchangecre"})
        }
      """
    )
  
    // @LINE:62
    def indexPageCREManager: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.indexPageCREManager",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager"})
        }
      """
    )
  
    // @LINE:22
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/index"})
        }
      """
    )
  
    // @LINE:32
    def postEditUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WyzUserController.postEditUser",
      """
        function(id0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "superAdmin/user/edit/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
  }

  // @LINE:216
  class ReverseSMSTemplateController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:601
    def postSMSTemplate: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SMSTemplateController.postSMSTemplate",
      """
        function(messageTemplate0,msgAPI1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "postSMSTemplate/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("messageTemplate", encodeURIComponent(messageTemplate0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("msgAPI", encodeURIComponent(msgAPI1))})
        }
      """
    )
  
    // @LINE:600
    def SMSTemplateBySuperAdmin: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SMSTemplateController.SMSTemplateBySuperAdmin",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "SMSTemplateBySuperAdmin"})
        }
      """
    )
  
    // @LINE:604
    def getSelectedUserListforDatatable: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SMSTemplateController.getSelectedUserListforDatatable",
      """
        function(selectedUsers0,setFlag1,savedsearchname2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getSelectedUserListforDatatable/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectedUsers", encodeURIComponent(selectedUsers0)) + "/" + (""" + implicitly[PathBindable[Boolean]].javascriptUnbind + """)("setFlag", setFlag1) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("savedsearchname", encodeURIComponent(savedsearchname2))})
        }
      """
    )
  
    // @LINE:216
    def checkIfSearchNameExists: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SMSTemplateController.checkIfSearchNameExists",
      """
        function(searchnamevalue0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "checkIfSearchNameExists/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("searchnamevalue", encodeURIComponent(searchnamevalue0))})
        }
      """
    )
  
    // @LINE:603
    def postSMSBulk: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SMSTemplateController.postSMSBulk",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "postSMSBulk"})
        }
      """
    )
  
    // @LINE:602
    def getCustomersListBySavedName: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SMSTemplateController.getCustomersListBySavedName",
      """
        function(savedsearchname0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getCustomersListBySavedName/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("savedsearchname", encodeURIComponent(savedsearchname0))})
        }
      """
    )
  
  }

  // @LINE:626
  class ReverseAllAudioConverter(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:626
    def convertAllFiles: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AllAudioConverter.convertAllFiles",
      """
        function(startId0,endId1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/audioconverter/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("startId", startId0) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("endId", endId1)})
        }
      """
    )
  
  }

  // @LINE:465
  class ReverseServiceAdvisorController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:465
    def getServiceAdvisorComplaints: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ServiceAdvisorController.getServiceAdvisorComplaints",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ServiceAdvisor/resolveComplaint"})
        }
      """
    )
  
  }

  // @LINE:590
  class ReverseSuperAdminController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:590
    def LocationBySuperAdmin: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SuperAdminController.LocationBySuperAdmin",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "LocationBySuperAdmins"})
        }
      """
    )
  
    // @LINE:591
    def postLocationBySuperAdmin: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SuperAdminController.postLocationBySuperAdmin",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "postLocationBySuperAdmins"})
        }
      """
    )
  
    // @LINE:595
    def postUserBySuperAdmin: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SuperAdminController.postUserBySuperAdmin",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "postUserBySuperAdmins"})
        }
      """
    )
  
    // @LINE:594
    def UsersBySuperAdmin: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SuperAdminController.UsersBySuperAdmin",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "UsersBySuperAdmins"})
        }
      """
    )
  
    // @LINE:596
    def checkIfUserExists: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SuperAdminController.checkIfUserExists",
      """
        function(uname0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "checkExistingUser/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("uname", encodeURIComponent(uname0))})
        }
      """
    )
  
  }

  // @LINE:149
  class ReversePSFController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:434
    def ajaxCallForAppointmentPSFData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.ajaxCallForAppointmentPSFData",
      """
        function(psfDay0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ajaxCallForAppointmentPSFData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("psfDay", encodeURIComponent(psfDay0))})
        }
      """
    )
  
    // @LINE:429
    def ajaxCallForIncompletedSurveyPSFData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.ajaxCallForIncompletedSurveyPSFData",
      """
        function(psfDay0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ajaxCallForIncompletedSurveyPSFData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("psfDay", encodeURIComponent(psfDay0))})
        }
      """
    )
  
    // @LINE:430
    def ajaxCallForCompletedSurveyPSFData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.ajaxCallForCompletedSurveyPSFData",
      """
        function(psfDay0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ajaxCallForCompletedSurveyPSFData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("psfDay", encodeURIComponent(psfDay0))})
        }
      """
    )
  
    // @LINE:417
    def getPSF30List: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.getPSF30List",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/PSF30List"})
        }
      """
    )
  
    // @LINE:421
    def getPSFNextDayList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.getPSFNextDayList",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/PSFNextDayList"})
        }
      """
    )
  
    // @LINE:419
    def getPSF3rdDayList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.getPSF3rdDayList",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/PSF3RDList"})
        }
      """
    )
  
    // @LINE:433
    def ajaxCallForDroppedCallsPSFData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.ajaxCallForDroppedCallsPSFData",
      """
        function(psfDay0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ajaxCallForDroppedCallsPSFData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("psfDay", encodeURIComponent(psfDay0))})
        }
      """
    )
  
    // @LINE:432
    def ajaxCallForNonContactsPSFData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.ajaxCallForNonContactsPSFData",
      """
        function(psfDay0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ajaxCallForNonContactsPSFData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("psfDay", encodeURIComponent(psfDay0))})
        }
      """
    )
  
    // @LINE:431
    def ajaxCallForDissatisfiedPSFData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.ajaxCallForDissatisfiedPSFData",
      """
        function(psfDay0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ajaxCallForDissatisfiedPSFData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("psfDay", encodeURIComponent(psfDay0))})
        }
      """
    )
  
    // @LINE:497
    def postCommonPSFDispositionPage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.postCommonPSFDispositionPage",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/psfDispo"})
        }
      """
    )
  
    // @LINE:428
    def ajaxCallForFollowUpRequiredPSFData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.ajaxCallForFollowUpRequiredPSFData",
      """
        function(psfDay0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ajaxCallForFollowUpRequiredPSFData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("psfDay", encodeURIComponent(psfDay0))})
        }
      """
    )
  
    // @LINE:149
    def getPSFFollowUpNotificationToday: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.getPSFFollowUpNotificationToday",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/getPSFFollowupNotificationOfToday"})
        }
      """
    )
  
    // @LINE:415
    def getPSF15List: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.getPSF15List",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/PSF15List"})
        }
      """
    )
  
    // @LINE:427
    def assignedInteractionPSFData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.assignedInteractionPSFData",
      """
        function(name0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assignedInteractionTablePSFData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name0))})
        }
      """
    )
  
    // @LINE:496
    def getCommonPSFDispositionPage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.getCommonPSFDispositionPage",
      """
        function(cid0,vehicle_id1,interactionid2,dispositionHistory3,typeOfPSF4) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/psfDispo/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("cid", cid0) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("vehicle_id", vehicle_id1) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("interactionid", interactionid2) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("dispositionHistory", dispositionHistory3) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("typeOfPSF", typeOfPSF4)})
        }
      """
    )
  
    // @LINE:423
    def getPSF4thDayList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.getPSF4thDayList",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/PSF4thDayList"})
        }
      """
    )
  
    // @LINE:413
    def getPSFList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PSFController.getPSFList",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/PSFList"})
        }
      """
    )
  
  }

  // @LINE:550
  class ReverseSMSandEmailController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:550
    def startTriggerSMS: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SMSandEmailController.startTriggerSMS",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "SMSTriggger"})
        }
      """
    )
  
  }

  // @LINE:533
  class ReverseFileUploadController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:533
    def uploadPage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FileUploadController.uploadPage",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "fileuploadPage"})
        }
      """
    )
  
    // @LINE:548
    def uploadReportPage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FileUploadController.uploadReportPage",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "uploadReportPage"})
        }
      """
    )
  
    // @LINE:535
    def headRequest: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FileUploadController.headRequest",
      """
        function() {
          return _wA({method:"HEAD", url:"""" + _prefix + { _defaultPrefix } + """" + "postfile"})
        }
      """
    )
  
    // @LINE:538
    def downloadFile: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FileUploadController.downloadFile",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "downloadFile/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id0))})
        }
      """
    )
  
    // @LINE:534
    def startUpload: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FileUploadController.startUpload",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "postfile"})
        }
      """
    )
  
    // @LINE:543
    def getUploadsList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FileUploadController.getUploadsList",
      """
        function(utype0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "uploadList/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("utype", encodeURIComponent(utype0))})
        }
      """
    )
  
    // @LINE:539
    def downloadErrors: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FileUploadController.downloadErrors",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "downloadErrors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id0))})
        }
      """
    )
  
    // @LINE:546
    def getExistingFiles: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FileUploadController.getExistingFiles",
      """
        function(utype0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getExistingFiles/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("utype", encodeURIComponent(utype0))})
        }
      """
    )
  
    // @LINE:544
    def getUploadsListById: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FileUploadController.getUploadsListById",
      """
        function(upId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "uploadListById/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("upId", upId0)})
        }
      """
    )
  
    // @LINE:537
    def deleteFile: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FileUploadController.deleteFile",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "deletefile/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id0))})
        }
      """
    )
  
  }

  // @LINE:475
  class ReverseAutoSelInsuranceAgentController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:477
    def ajaxupdateInsuAgentChange: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AutoSelInsuranceAgentController.ajaxupdateInsuAgentChange",
      """
        function(date0,preSaDetails1,newSaDetails2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/callDispositionPageIns/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("date", encodeURIComponent(date0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("preSaDetails", encodeURIComponent(preSaDetails1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("newSaDetails", encodeURIComponent(newSaDetails2))})
        }
      """
    )
  
    // @LINE:475
    def ajaxAutoInsurAgentSelection: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AutoSelInsuranceAgentController.ajaxAutoInsurAgentSelection",
      """
        function(date0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/callDispositionPageIns/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("date", encodeURIComponent(date0))})
        }
      """
    )
  
    // @LINE:476
    def ajaxAutoInsurAgentSelectionList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AutoSelInsuranceAgentController.ajaxAutoInsurAgentSelectionList",
      """
        function(saDetails0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/callDispositionPageIns/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("saDetails", encodeURIComponent(saDetails0))})
        }
      """
    )
  
  }

  // @LINE:95
  class ReverseCallInteractionController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:278
    def ajaxCallForAddcustomerinfo: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.ajaxCallForAddcustomerinfo",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/saveaddcustomerinfo"})
        }
      """
    )
  
    // @LINE:239
    def get_required_fields: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.get_required_fields",
      """
        function(selected_part0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "uploadFormat/MapMasterFormat/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selected_part", encodeURIComponent(selected_part0))})
        }
      """
    )
  
    // @LINE:363
    def getServiceDataOfCustomer: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getServiceDataOfCustomer",
      """
        function(customerId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/serviceDataOfCustomer/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("customerId", customerId0)})
        }
      """
    )
  
    // @LINE:350
    def postUpdateRangeOfUnavailabiltyOfUsers: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.postUpdateRangeOfUnavailabiltyOfUsers",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/updateRangeOfUnavailabilty"})
        }
      """
    )
  
    // @LINE:370
    def getComplaintHistoryVeh: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getComplaintHistoryVeh",
      """
        function(vehicalId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getComplaintHistoryOfvehicalId/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("vehicalId", vehicalId0)})
        }
      """
    )
  
    // @LINE:352
    def getRoasterTable: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getRoasterTable",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/todaysRoaster"})
        }
      """
    )
  
    // @LINE:345
    def getComplaintHistoryAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getComplaintHistoryAll",
      """
        function(complaintNumber0,vehregnumber1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/getComplaintHistoryAll/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("complaintNumber", encodeURIComponent(complaintNumber0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("vehregnumber", encodeURIComponent(vehregnumber1))})
        }
      """
    )
  
    // @LINE:226
    def getSelectedAssignCalls: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getSelectedAssignCalls",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/assignCall"})
        }
      """
    )
  
    // @LINE:207
    def assigningCallsToCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.assigningCallsToCRE",
      """
        function(selectAgent0,fromData1,toDate2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/assignCalls/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectAgent", encodeURIComponent(selectAgent0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("fromData", encodeURIComponent(fromData1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("toDate", encodeURIComponent(toDate2))})
        }
      """
    )
  
    // @LINE:565
    def getFunctionsListByLoc: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getFunctionsListByLoc",
      """
        function(selectedCity0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/listFunctionsByLocation/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectedCity", encodeURIComponent(selectedCity0))})
        }
      """
    )
  
    // @LINE:283
    def ajaxAddRegistrationno: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.ajaxAddRegistrationno",
      """
        function(vehicalRegNo0,customer_id1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/saveaddcustomervregistrationno/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("vehicalRegNo", encodeURIComponent(vehicalRegNo0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("customer_id", customer_id1)})
        }
      """
    )
  
    // @LINE:221
    def postAddCustomer: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.postAddCustomer",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "postAddCustomer"})
        }
      """
    )
  
    // @LINE:554
    def getCRESByWorkshops: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getCRESByWorkshops",
      """
        function(workshops0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getCRESByWorkshop/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("workshops", encodeURIComponent(workshops0))})
        }
      """
    )
  
    // @LINE:97
    def getServiceBookedDataCREMan: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getServiceBookedDataCREMan",
      """
        function(selectedAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/getServiceBookedDataCREMan/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectedAgent", encodeURIComponent(selectedAgent0))})
        }
      """
    )
  
    // @LINE:286
    def saveaddcustomermobno: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.saveaddcustomermobno",
      """
        function(custMobNo0,wyzUser_id1,customer_Id2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/saveaddcustomermobno/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("custMobNo", encodeURIComponent(custMobNo0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("wyzUser_id", wyzUser_id1) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("customer_Id", customer_Id2)})
        }
      """
    )
  
    // @LINE:367
    def getLeadByUserLocation: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getLeadByUserLocation",
      """
        function(userLocation0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/leadBasedOnLocation/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("userLocation", userLocation0)})
        }
      """
    )
  
    // @LINE:330
    def updateComplaintsResolution: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.updateComplaintsResolution",
      """
        function(complaintNum0,reasonFor1,complaintStatus2,customerStatus3,actionTaken4,resolutionBy5) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/updateComplaintsResolution/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("complaintNum", encodeURIComponent(complaintNum0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("reasonFor", encodeURIComponent(reasonFor1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("complaintStatus", encodeURIComponent(complaintStatus2)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("customerStatus", encodeURIComponent(customerStatus3)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("actionTaken", encodeURIComponent(actionTaken4)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("resolutionBy", encodeURIComponent(resolutionBy5))})
        }
      """
    )
  
    // @LINE:371
    def getSMSHistoryOfCustomer: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getSMSHistoryOfCustomer",
      """
        function(customerId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getSMSHistoryOfCustomerId/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("customerId", customerId0)})
        }
      """
    )
  
    // @LINE:96
    def getFollowUpRequiredDataCREMan: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getFollowUpRequiredDataCREMan",
      """
        function(selectedAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/getFollowUpRequiredDataCREMan/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectedAgent", encodeURIComponent(selectedAgent0))})
        }
      """
    )
  
    // @LINE:95
    def getAssignedCallsOfCREManager: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getAssignedCallsOfCREManager",
      """
        function(selectedAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/getAssignedCallsOfCREManager/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectedAgent", encodeURIComponent(selectedAgent0))})
        }
      """
    )
  
    // @LINE:460
    def getCRESListBasedOnWorkshop: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getCRESListBasedOnWorkshop",
      """
        function(workshopId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/listCREsByWorkshop/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("workshopId", workshopId0)})
        }
      """
    )
  
    // @LINE:353
    def toUpdateRoasterUnAvailablity: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.toUpdateRoasterUnAvailablity",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/todaysRoaster"})
        }
      """
    )
  
    // @LINE:311
    def getDriverServiceBooked: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getDriverServiceBooked",
      """
        function(driverId0,userId1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getDriverServices/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("driverId", driverId0) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("userId", userId1)})
        }
      """
    )
  
    // @LINE:368
    def getLeadTagByDepartment: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getLeadTagByDepartment",
      """
        function(userLocation0,departmentName1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/leadBasedOnDepartment/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("userLocation", userLocation0) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("departmentName", departmentName1)})
        }
      """
    )
  
    // @LINE:277
    def ajaxAddPhoneNumber: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.ajaxAddPhoneNumber",
      """
        function(phone_no0,customer_id1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/addphone/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("phone_no", encodeURIComponent(phone_no0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("customer_id", customer_id1)})
        }
      """
    )
  
    // @LINE:230
    def getReAssignmentCalls: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getReAssignmentCalls",
      """
        function(selectAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/getReAssignmentCalls/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectAgent", encodeURIComponent(selectAgent0))})
        }
      """
    )
  
    // @LINE:238
    def upload_Excel_Sheet: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.upload_Excel_Sheet",
      """
        function(uploadId0,uploadType1,resultData2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/uploadExcelSheet/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("uploadId", encodeURIComponent(uploadId0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("uploadType", encodeURIComponent(uploadType1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("resultData", encodeURIComponent(resultData2))})
        }
      """
    )
  
    // @LINE:566
    def getusersByFuncandLocation: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getusersByFuncandLocation",
      """
        function(city0,function1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/listUsersByFuncandLoc/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("city", encodeURIComponent(city0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("function", encodeURIComponent(function1))})
        }
      """
    )
  
    // @LINE:213
    def searchByCustomerManager: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.searchByCustomerManager",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchByCustomerManager"})
        }
      """
    )
  
    // @LINE:316
    def getListWorkshopByLocation: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getListWorkshopByLocation",
      """
        function(selectedCity0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/listWorkshops/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectedCity", encodeURIComponent(selectedCity0))})
        }
      """
    )
  
    // @LINE:214
    def addCustomerCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.addCustomerCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addCustomer"})
        }
      """
    )
  
    // @LINE:98
    def getServiceNotRequiredDataCREMan: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getServiceNotRequiredDataCREMan",
      """
        function(selectedAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/getServiceNotRequiredDataCREMan/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectedAgent", encodeURIComponent(selectedAgent0))})
        }
      """
    )
  
    // @LINE:358
    def getRosterDataByUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getRosterDataByUser",
      """
        function(selectAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/loadRosterData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectAgent", encodeURIComponent(selectAgent0))})
        }
      """
    )
  
    // @LINE:254
    def postComplaints: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.postComplaints",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "postComplaints"})
        }
      """
    )
  
    // @LINE:562
    def viewAllComplaintsReadOnlyAccess: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.viewAllComplaintsReadOnlyAccess",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "viewAllComplaints"})
        }
      """
    )
  
    // @LINE:360
    def deleteUnavialbility: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.deleteUnavialbility",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/deleteUnavialbility/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:231
    def reAssigningCallsofselectCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.reAssigningCallsofselectCRE",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/changeAssignment"})
        }
      """
    )
  
    // @LINE:344
    def getComplaintsDataByFilter: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getComplaintsDataByFilter",
      """
        function(filterData0,varLoc1,varfunc2,varraisedDate3,varendDate4) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/getComplaintsDataByFilter/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("filterData", encodeURIComponent(filterData0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("varLoc", encodeURIComponent(varLoc1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("varfunc", encodeURIComponent(varfunc2)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("varraisedDate", encodeURIComponent(varraisedDate3)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("varendDate", encodeURIComponent(varendDate4))})
        }
      """
    )
  
    // @LINE:452
    def getExistingVehicleRegCount: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getExistingVehicleRegCount",
      """
        function(vehicleReg0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/vehicleRegNoExist/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("vehicleReg", encodeURIComponent(vehicleReg0))})
        }
      """
    )
  
    // @LINE:259
    def postComplaintsByManager: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.postComplaintsByManager",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "postComplaintsByManager"})
        }
      """
    )
  
    // @LINE:552
    def getWorkshopsByLocations: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getWorkshopsByLocations",
      """
        function(locations0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getworkshopByLocation/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("locations", encodeURIComponent(locations0))})
        }
      """
    )
  
    // @LINE:99
    def getNonContactsDataCREMan: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getNonContactsDataCREMan",
      """
        function(selectedAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/getNonContactsDataCREMan/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectedAgent", encodeURIComponent(selectedAgent0))})
        }
      """
    )
  
    // @LINE:282
    def ajaxAddChassisno: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.ajaxAddChassisno",
      """
        function(chassisNo0,customer_id1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/saveaddcustomerchassisno/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("chassisNo", encodeURIComponent(chassisNo0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("customer_id", customer_id1)})
        }
      """
    )
  
    // @LINE:327
    def ajaxcomplaintNumber: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.ajaxcomplaintNumber",
      """
        function(complaintNum0,veh_num1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/searchcomplaintNum/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("complaintNum", encodeURIComponent(complaintNum0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("veh_num", encodeURIComponent(veh_num1))})
        }
      """
    )
  
    // @LINE:355
    def rosterOfUnavailabiltyByUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.rosterOfUnavailabiltyByUser",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/rosterOfUnavailabiltyByUser"})
        }
      """
    )
  
    // @LINE:459
    def getDriverListBasedOnworkshop: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getDriverListBasedOnworkshop",
      """
        function(workshopId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/listDrivers/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("workshopId", workshopId0)})
        }
      """
    )
  
    // @LINE:364
    def getCallHistoryOfvehicalId: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getCallHistoryOfvehicalId",
      """
        function(vehicalId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getCallHistoryOfvehicalId/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("vehicalId", vehicalId0)})
        }
      """
    )
  
    // @LINE:201
    def getCustomerByInteractionId: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getCustomerByInteractionId",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getCustomerByInteractionId"})
        }
      """
    )
  
    // @LINE:284
    def ajaxAddEngineno: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.ajaxAddEngineno",
      """
        function(engineNo0,customer_id1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/saveaddcustomerengineno/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("engineNo", encodeURIComponent(engineNo0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("customer_id", customer_id1)})
        }
      """
    )
  
    // @LINE:334
    def complaintAssignment: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.complaintAssignment",
      """
        function(complaintNum0,veh_num1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/complaintAssignment/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("complaintNum", encodeURIComponent(complaintNum0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("veh_num", encodeURIComponent(veh_num1))})
        }
      """
    )
  
    // @LINE:236
    def upload_file_Format_submit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.upload_file_Format_submit",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/uploadExcelData"})
        }
      """
    )
  
    // @LINE:240
    def ajax_transaction_data_upload: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.ajax_transaction_data_upload",
      """
        function(selected_format0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "master/excelUpload/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selected_format", encodeURIComponent(selected_format0))})
        }
      """
    )
  
    // @LINE:229
    def toChangeAssigment: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.toChangeAssigment",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/changeAssignment"})
        }
      """
    )
  
    // @LINE:228
    def getAssignedList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getAssignedList",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/getAssignList"})
        }
      """
    )
  
    // @LINE:356
    def addRosterOfUserByAjax: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.addRosterOfUserByAjax",
      """
        function(selectAgent0,fromDate1,toDate2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/addRosterOfUserByAjax/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectAgent", encodeURIComponent(selectAgent0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("fromDate", encodeURIComponent(fromDate1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("toDate", encodeURIComponent(toDate2))})
        }
      """
    )
  
    // @LINE:557
    def getCitiesByState: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getCitiesByState",
      """
        function(selState0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getCityByStates/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selState", encodeURIComponent(selState0))})
        }
      """
    )
  
    // @LINE:287
    def saveaddcustomerEmail: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.saveaddcustomerEmail",
      """
        function(custEmail0,wyzUser_id1,customer_Id2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/saveaddcustomerEmail/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("custEmail", encodeURIComponent(custEmail0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("wyzUser_id", wyzUser_id1) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("customer_Id", customer_Id2)})
        }
      """
    )
  
    // @LINE:138
    def getServiceBookedData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getServiceBookedData",
      """
        function(selectAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getServiceBookedData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectAgent", encodeURIComponent(selectAgent0))})
        }
      """
    )
  
    // @LINE:137
    def getFollowUpRequiredData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getFollowUpRequiredData",
      """
        function(selectAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getFollowUpRequiredData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectAgent", encodeURIComponent(selectAgent0))})
        }
      """
    )
  
    // @LINE:205
    def getAssignedInteraction: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getAssignedInteraction",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/UploadAllCalls"})
        }
      """
    )
  
    // @LINE:303
    def getWorkShopServiceBooked: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getWorkShopServiceBooked",
      """
        function(workId0,userId1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getWorkshopServices/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("workId", workId0) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("userId", userId1)})
        }
      """
    )
  
    // @LINE:341
    def downloadExcelFile: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.downloadExcelFile",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/viewComplaints1"})
        }
      """
    )
  
    // @LINE:458
    def sendCustomSMSAjax: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.sendCustomSMSAjax",
      """
        function(rerenceNumber0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/sendSMS/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("rerenceNumber", rerenceNumber0)})
        }
      """
    )
  
    // @LINE:225
    def getPageForAssigningCalls: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getPageForAssigningCalls",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/assignCall"})
        }
      """
    )
  
    // @LINE:332
    def updateComplaintsResolutionClosed: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.updateComplaintsResolutionClosed",
      """
        function(complaintNumClosed0,reasonForClosed1,complaintStatusClosed2,customerStatusClosed3,actionTakenClosed4,resolutionByClosed5) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/updateComplaintsResolutionClosed/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("complaintNumClosed", encodeURIComponent(complaintNumClosed0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("reasonForClosed", encodeURIComponent(reasonForClosed1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("complaintStatusClosed", encodeURIComponent(complaintStatusClosed2)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("customerStatusClosed", encodeURIComponent(customerStatusClosed3)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("actionTakenClosed", encodeURIComponent(actionTakenClosed4)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("resolutionByClosed", encodeURIComponent(resolutionByClosed5))})
        }
      """
    )
  
    // @LINE:256
    def assignComplaints: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.assignComplaints",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/assignComplaints"})
        }
      """
    )
  
    // @LINE:208
    def getAssignedCallsOfUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getAssignedCallsOfUser",
      """
        function(selectAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getAssignedCalls/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectAgent", encodeURIComponent(selectAgent0))})
        }
      """
    )
  
    // @LINE:139
    def getNonContactsData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getNonContactsData",
      """
        function(selectAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getNonContactsData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectAgent", encodeURIComponent(selectAgent0))})
        }
      """
    )
  
    // @LINE:365
    def getServiceAdvisorOfWorkshop: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getServiceAdvisorOfWorkshop",
      """
        function(workshopId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/listServiceAdvisors/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("workshopId", workshopId0)})
        }
      """
    )
  
    // @LINE:237
    def ajax_master_data_upload_fileFormat: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.ajax_master_data_upload_fileFormat",
      """
        function(selected_part0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "uploadFormat/getFormElement/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selected_part", encodeURIComponent(selected_part0))})
        }
      """
    )
  
    // @LINE:253
    def addComplaints: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.addComplaints",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "complaints"})
        }
      """
    )
  
    // @LINE:312
    def reAssignAgentUpdate: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.reAssignAgentUpdate",
      """
        function(rowId0,wyzUserId1,post_id2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/reAssignAgent/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("rowId", rowId0) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("wyzUserId", wyzUserId1) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("post_id", post_id2)})
        }
      """
    )
  
    // @LINE:245
    def getExcelColumnsOFUploadFormat: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getExcelColumnsOFUploadFormat",
      """
        function(upload_format0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getExcelColumns/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("upload_format", encodeURIComponent(upload_format0))})
        }
      """
    )
  
    // @LINE:307
    def getSerAdvServiceBooked: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getSerAdvServiceBooked",
      """
        function(serviceAdvisorId0,userId1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getServiceAdvisorServices/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("serviceAdvisorId", serviceAdvisorId0) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("userId", userId1)})
        }
      """
    )
  
    // @LINE:331
    def updateComplaintsResolutionByManager: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.updateComplaintsResolutionByManager",
      """
        function(complaintNum0,reasonFor1,complaintStatus2,customerStatus3,actionTaken4,resolutionBy5) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/updateComplaintsResolutionByManager/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("complaintNum", encodeURIComponent(complaintNum0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("reasonFor", encodeURIComponent(reasonFor1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("complaintStatus", encodeURIComponent(complaintStatus2)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("customerStatus", encodeURIComponent(customerStatus3)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("actionTaken", encodeURIComponent(actionTaken4)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("resolutionBy", encodeURIComponent(resolutionBy5))})
        }
      """
    )
  
    // @LINE:206
    def toUploadInteractionsByCREManager: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.toUploadInteractionsByCREManager",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/UploadAllCalls"})
        }
      """
    )
  
    // @LINE:366
    def getWorkshopListName: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getWorkshopListName",
      """
        function(workshopId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/listWorkshopsIfExisting/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("workshopId", workshopId0)})
        }
      """
    )
  
    // @LINE:326
    def complaintsResolution: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.complaintsResolution",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "complaintResolution"})
        }
      """
    )
  
    // @LINE:258
    def addComplaintsByManager: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.addComplaintsByManager",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "complaintsByManager"})
        }
      """
    )
  
    // @LINE:100
    def getDroppedCallsDataCREMan: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getDroppedCallsDataCREMan",
      """
        function(selectedAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/getDroppedCallsDataCREMan/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectedAgent", encodeURIComponent(selectedAgent0))})
        }
      """
    )
  
    // @LINE:257
    def updateComplaints: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.updateComplaints",
      """
        function(id0,comments1,selected_value2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/updateComplaints/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("comments", encodeURIComponent(comments1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selected_value", encodeURIComponent(selected_value2))})
        }
      """
    )
  
    // @LINE:337
    def saveaddComplaintAssignModile: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.saveaddComplaintAssignModile",
      """
        function(complaintNum0,city1,workshop2,functions3,ownership4,priority5,esclation16,esclation27) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/saveaddComplaintAssignModile/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("complaintNum", encodeURIComponent(complaintNum0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("city", encodeURIComponent(city1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("workshop", encodeURIComponent(workshop2)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("functions", encodeURIComponent(functions3)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("ownership", encodeURIComponent(ownership4)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("priority", encodeURIComponent(priority5)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("esclation1", encodeURIComponent(esclation16)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("esclation2", encodeURIComponent(esclation27))})
        }
      """
    )
  
    // @LINE:349
    def updateRangeOfUnavailabiltyOfUsers: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.updateRangeOfUnavailabiltyOfUsers",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/updateRangeOfUnavailabilty"})
        }
      """
    )
  
    // @LINE:338
    def viewAllComplaints1: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.viewAllComplaints1",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/viewComplaints1"})
        }
      """
    )
  
    // @LINE:235
    def upload_file_Format_show: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.upload_file_Format_show",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/uploadExcelData"})
        }
      """
    )
  
    // @LINE:369
    def getTagNameByUpselLeadType: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getTagNameByUpselLeadType",
      """
        function(userLocation0,upselIDTag1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/upselLeadIdTag/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("userLocation", userLocation0) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("upselIDTag", upselIDTag1)})
        }
      """
    )
  
    // @LINE:461
    def getCREListBasedOnWorkshopCallHistory: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getCREListBasedOnWorkshopCallHistory",
      """
        function(workshopId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/listCREsByWorkshopcallhistory/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("workshopId", workshopId0)})
        }
      """
    )
  
    // @LINE:212
    def searchByCustomer: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.searchByCustomer",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchByCustomer"})
        }
      """
    )
  
    // @LINE:564
    def getListWorkshopByLocationById: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getListWorkshopByLocationById",
      """
        function(selectedCity0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/listWorkshopsByID/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("selectedCity", selectedCity0)})
        }
      """
    )
  
    // @LINE:328
    def ajaxcomplaintNumberClosed: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.ajaxcomplaintNumberClosed",
      """
        function(complaintNumClosed0,veh_numclosed1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/searchcomplaintNumClosed/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("complaintNumClosed", encodeURIComponent(complaintNumClosed0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("veh_numclosed", encodeURIComponent(veh_numclosed1))})
        }
      """
    )
  
    // @LINE:223
    def getMediaFileCallInteractions: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getMediaFileCallInteractions",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getMediaFile/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:244
    def getDownloadExcelFormat: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getDownloadExcelFormat",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/downloadExcelFormat"})
        }
      """
    )
  
    // @LINE:321
    def getWorkshopSummaryDetails: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getWorkshopSummaryDetails",
      """
        function(selectedWorkshop0,schDate1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/listWorkshopSummary/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("selectedWorkshop", selectedWorkshop0) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("schDate", encodeURIComponent(schDate1))})
        }
      """
    )
  
    // @LINE:140
    def getDroppedCallsData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getDroppedCallsData",
      """
        function(selectAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getDroppedCallsData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectAgent", encodeURIComponent(selectAgent0))})
        }
      """
    )
  
    // @LINE:141
    def getServiceNotRequiredData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.getServiceNotRequiredData",
      """
        function(selectAgent0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getServiceNotRequiredData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectAgent", encodeURIComponent(selectAgent0))})
        }
      """
    )
  
    // @LINE:325
    def ajaxsearchVehicle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.ajaxsearchVehicle",
      """
        function(veh_number0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/searchvehRegNo/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("veh_number", encodeURIComponent(veh_number0))})
        }
      """
    )
  
    // @LINE:359
    def updateRosterOfUserByAjaxVal: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInteractionController.updateRosterOfUserByAjaxVal",
      """
        function(selectAgent0,From_Date1,To_Date2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/updateRosterOfUserByAjaxData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("selectAgent", encodeURIComponent(selectAgent0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("From_Date", encodeURIComponent(From_Date1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("To_Date", encodeURIComponent(To_Date2))})
        }
      """
    )
  
  }

  // @LINE:388
  class ReverseUploadExcelController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:388
    def uploadExcelData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UploadExcelController.uploadExcelData",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/uploadExcelPOST"})
        }
      """
    )
  
  }

  // @LINE:88
  class ReverseCallInfoController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:147
    def getFollowUpRemainderOfMissedSchedules: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.getFollowUpRemainderOfMissedSchedules",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/getFollowUpRemainderOfMissedSchedules"})
        }
      """
    )
  
    // @LINE:89
    def getPSFCallLogViewPage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.getPSFCallLogViewPage",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/PSFCallLogPageCREManager"})
        }
      """
    )
  
    // @LINE:88
    def getCallDispositionBucketForCREMan: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.getCallDispositionBucketForCREMan",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/CallDispositionBucketForCREMan"})
        }
      """
    )
  
    // @LINE:106
    def getcallLogEditForCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.getcallLogEditForCRE",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/toeditcallLog/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:146
    def getFollowUpNotifyBeforeTime: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.getFollowUpNotifyBeforeTime",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/getFollowUpNotificationBeforeTime"})
        }
      """
    )
  
    // @LINE:483
    def getUpsellLeadsSeletedInLastSB: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.getUpsellLeadsSeletedInLastSB",
      """
        function(sr_int_id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/upsellSelectedLastSB/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("sr_int_id", sr_int_id0)})
        }
      """
    )
  
    // @LINE:123
    def startInitiatingOfCall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.startInitiatingOfCall",
      """
        function(phonenumber0,uniqueid1,customerId2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/initiateCall/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("phonenumber", phonenumber0) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("uniqueid", uniqueid1) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("customerId", customerId2)})
        }
      """
    )
  
    // @LINE:177
    def startSyncOperation: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.startSyncOperation",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "callInfo/startSync"})
        }
      """
    )
  
    // @LINE:156
    def deleteCallLog: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.deleteCallLog",
      """
        function(dealercode0,id1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/deleteCall/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id1) + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("dealercode", dealercode0)])})
        }
      """
    )
  
    // @LINE:148
    def getFollowUpNotificationToday: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.getFollowUpNotificationToday",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/getFollowUpNotificationOfToday"})
        }
      """
    )
  
    // @LINE:178
    def stopSyncOperation: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.stopSyncOperation",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "callInfo/stopSync"})
        }
      """
    )
  
    // @LINE:585
    def getLeadNamesbyLeadId: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.getLeadNamesbyLeadId",
      """
        function(leadId0,userId1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getLeadNamesbyLeadId/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("leadId", leadId0) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("userId", userId1)})
        }
      """
    )
  
    // @LINE:157
    def deleteSchCalllog: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.deleteSchCalllog",
      """
        function(dealercode0,id1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/deleteSchCall/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id1) + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("dealercode", dealercode0)])})
        }
      """
    )
  
    // @LINE:587
    def getCheckVehicleRegExist: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.getCheckVehicleRegExist",
      """
        function(vehRegId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/checkingVehicleRegNo/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("vehRegId", encodeURIComponent(vehRegId0))})
        }
      """
    )
  
    // @LINE:107
    def postcallLogeditForCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.postcallLogeditForCRE",
      """
        function(id0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/toeditcallLog/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:90
    def getInsuranceCallLogViewPage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.getInsuranceCallLogViewPage",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/InsuranceCallLogPageCREManager"})
        }
      """
    )
  
    // @LINE:145
    def getFollowUpTableDataOfCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.getFollowUpTableDataOfCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/listTodaysFollowUp"})
        }
      """
    )
  
    // @LINE:182
    def todeleteFilesFromDirectory: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CallInfoController.todeleteFilesFromDirectory",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "delete/allFilesFromTmp"})
        }
      """
    )
  
  }

  // @LINE:487
  class ReverseInsuranceController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:521
    def insuranceFollowUpNotificationToday: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.insuranceFollowUpNotificationToday",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/getInsuranceFollowupNotificationOfToday"})
        }
      """
    )
  
    // @LINE:514
    def showRoomList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.showRoomList",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/showRoomList"})
        }
      """
    )
  
    // @LINE:501
    def getPageForAssigningCallsInsurance: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.getPageForAssigningCallsInsurance",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/assignCallInsurance"})
        }
      """
    )
  
    // @LINE:490
    def getAssignedInsuInteraction: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.getAssignedInsuInteraction",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "insurance/assignedInsuInteraction"})
        }
      """
    )
  
    // @LINE:528
    def insuranceHistoryOfCustomerId: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.insuranceHistoryOfCustomerId",
      """
        function(customerId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getInsuranceHistoryOfCustomerId/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("customerId", customerId0)})
        }
      """
    )
  
    // @LINE:508
    def getNCBValueByBasicValue: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.getNCBValueByBasicValue",
      """
        function(ncbPercenVal0,basicODValue1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getNCBBYBasicODValue/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("ncbPercenVal", ncbPercenVal0) + "/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("basicODValue", basicODValue1)})
        }
      """
    )
  
    // @LINE:507
    def getBasicODVaue: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.getBasicODVaue",
      """
        function(odvValue0,idvValue1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getBasicODValue/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("odvValue", odvValue0) + "/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("idvValue", idvValue1)})
        }
      """
    )
  
    // @LINE:487
    def getCommonCallDispositionForm: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.getCommonCallDispositionForm",
      """
        function(cid0,vehicle_id1,typeDispo2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getDispositionFormPage/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("cid", cid0) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("vehicle_id", vehicle_id1) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("typeDispo", encodeURIComponent(typeDispo2))})
        }
      """
    )
  
    // @LINE:516
    def downloadInsuranceErrorData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.downloadInsuranceErrorData",
      """
        function(uploadId0,uploadType1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/downloadExcelSheet/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("uploadId", encodeURIComponent(uploadId0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("uploadType", encodeURIComponent(uploadType1))})
        }
      """
    )
  
    // @LINE:492
    def getNonContactDroppedInsuranceDispoData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.getNonContactDroppedInsuranceDispoData",
      """
        function(typeOfdispo0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "insurance/nonContactedDispoInterac/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("typeOfdispo", typeOfdispo0)})
        }
      """
    )
  
    // @LINE:489
    def getAllInsuranceAssignedList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.getAllInsuranceAssignedList",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/insuranceAssignedList"})
        }
      """
    )
  
    // @LINE:510
    def gettotalPremiumForAddOn: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.gettotalPremiumForAddOn",
      """
        function(addOnPremiumValuePer0,odPremiumVaue1,commercialDiscPerceValue2,idvVal3,thirdPartPremiumValue4) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/gettotalPremiumOdAddOn/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("addOnPremiumValuePer", addOnPremiumValuePer0) + "/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("odPremiumVaue", odPremiumVaue1) + "/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("commercialDiscPerceValue", commercialDiscPerceValue2) + "/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("idvVal", idvVal3) + "/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("thirdPartPremiumValue", thirdPartPremiumValue4)})
        }
      """
    )
  
    // @LINE:506
    def getODPercentage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.getODPercentage",
      """
        function(cubicCap0,vehAge1,zoneid2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getODPercentage/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("cubicCap", encodeURIComponent(cubicCap0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("vehAge", encodeURIComponent(vehAge1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("zoneid", encodeURIComponent(zoneid2))})
        }
      """
    )
  
    // @LINE:488
    def postCommonCallDispositionForm: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.postCommonCallDispositionForm",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getDispositionPageOfTab"})
        }
      """
    )
  
    // @LINE:499
    def upload_Excel_Sheet_insurance: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.upload_Excel_Sheet_insurance",
      """
        function(uploadId0,uploadType1,resultData2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/uploadExcelSheetInsurance/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("uploadId", encodeURIComponent(uploadId0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("uploadType", encodeURIComponent(uploadType1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("resultData", encodeURIComponent(resultData2))})
        }
      """
    )
  
    // @LINE:502
    def postAssignCallInsurance: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.postAssignCallInsurance",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/assignCallInsurance"})
        }
      """
    )
  
    // @LINE:491
    def getContactedDispoFormData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.getContactedDispoFormData",
      """
        function(typeOfdispo0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "insurance/contactedDispoInteractions/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("typeOfdispo", typeOfdispo0)})
        }
      """
    )
  
    // @LINE:509
    def gettotalPremiumAndDiscValue: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceController.gettotalPremiumAndDiscValue",
      """
        function(odPremiumVaue0,commercialDiscPerceValue1,thirdPartPremiumValue2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getDiscValueVyODPremium/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("odPremiumVaue", odPremiumVaue0) + "/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("commercialDiscPerceValue", commercialDiscPerceValue1) + "/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("thirdPartPremiumValue", thirdPartPremiumValue2)})
        }
      """
    )
  
  }

  // @LINE:608
  class ReverseServiceBookController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:619
    def driverListScheduleByWorkshopId: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ServiceBookController.driverListScheduleByWorkshopId",
      """
        function(workshopId0,scheduleDate1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "driverListSchedule/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("workshopId", workshopId0) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("scheduleDate", encodeURIComponent(scheduleDate1))})
        }
      """
    )
  
    // @LINE:648
    def downloadServiceBooking: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ServiceBookController.downloadServiceBooking",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/downloadBooking"})
        }
      """
    )
  
    // @LINE:616
    def reviewScheduleBooking: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ServiceBookController.reviewScheduleBooking",
      """
        function(servicebookedId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/reviewResheduleBooking/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("servicebookedId", servicebookedId0)})
        }
      """
    )
  
    // @LINE:610
    def getServiceAdvisorListByWorkshop: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ServiceBookController.getServiceAdvisorListByWorkshop",
      """
        function(workshopid0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "serviceAdvisorListByWorkshop/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("workshopid", workshopid0)})
        }
      """
    )
  
    // @LINE:614
    def cancelBookingOrPickup: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ServiceBookController.cancelBookingOrPickup",
      """
        function(servicebookedId0,cancelId1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/cancelBooking/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("servicebookedId", servicebookedId0) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("cancelId", cancelId1)})
        }
      """
    )
  
    // @LINE:611
    def getsearchServiceBoookedList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ServiceBookController.getsearchServiceBoookedList",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchServiceBookedList"})
        }
      """
    )
  
    // @LINE:608
    def getReviewBookingPage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ServiceBookController.getReviewBookingPage",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/getReviewBookingPage"})
        }
      """
    )
  
    // @LINE:617
    def postRescheduleBooking: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ServiceBookController.postRescheduleBooking",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/postRescheduleBooking"})
        }
      """
    )
  
  }

  // @LINE:295
  class ReverseAllCallInteractionController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:300
    def downloadCallHistoryReport: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AllCallInteractionController.downloadCallHistoryReport",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/downloadCallHistoryReport"})
        }
      """
    )
  
    // @LINE:299
    def getAllCallInteractions: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AllCallInteractionController.getAllCallInteractions",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/getAllCallHistoryData"})
        }
      """
    )
  
    // @LINE:295
    def viewAllCallIntearctions: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AllCallInteractionController.viewAllCallIntearctions",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/viewCallInteractions"})
        }
      """
    )
  
  }

  // @LINE:71
  class ReverseSearchControllerMR(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:71
    def getPSFassignedInteractionTableDataMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.getPSFassignedInteractionTableDataMR",
      """
        function(CREIds0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/PSFassignedInteractionTableDataMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0))})
        }
      """
    )
  
    // @LINE:443
    def missedCallInteractionDataMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.missedCallInteractionDataMR",
      """
        function(CREIds0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "missedCallsServerDataTableMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0))})
        }
      """
    )
  
    // @LINE:83
    def getInsurancenonContactsServerDataTableMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.getInsurancenonContactsServerDataTableMR",
      """
        function(CREIds0,buckettype1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/InsurancenonContactsServerDataTableMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("buckettype", buckettype1)})
        }
      """
    )
  
    // @LINE:447
    def getMediaFileMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.getMediaFileMR",
      """
        function(callId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/downloadMediaFile/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("callId", callId0)})
        }
      """
    )
  
    // @LINE:439
    def serviceBookedInteractionDataMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.serviceBookedInteractionDataMR",
      """
        function(CREIds0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "serviceBookedServerDataTableMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0))})
        }
      """
    )
  
    // @LINE:72
    def getPSFfollowUpCallLogTableDataMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.getPSFfollowUpCallLogTableDataMR",
      """
        function(CREIds0,buckettype1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/PSFfollowUpCallLogTableDataMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("buckettype", buckettype1)})
        }
      """
    )
  
    // @LINE:75
    def getPSFnonContactsServerDataTableMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.getPSFnonContactsServerDataTableMR",
      """
        function(CREIds0,buckettype1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/PSFnonContactsServerDataTableMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("buckettype", buckettype1)})
        }
      """
    )
  
    // @LINE:444
    def incomingCallInteractionDataMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.incomingCallInteractionDataMR",
      """
        function(CREIds0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "incomingCallsServerDataTableMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0))})
        }
      """
    )
  
    // @LINE:438
    def followUpRequiredInteractionDataMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.followUpRequiredInteractionDataMR",
      """
        function(CREIds0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "followUpCallLogTableDataMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0))})
        }
      """
    )
  
    // @LINE:453
    def getDashboardCount: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.getDashboardCount",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/dashboardCounts"})
        }
      """
    )
  
    // @LINE:440
    def serviceNotRequiredInteractionDataMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.serviceNotRequiredInteractionDataMR",
      """
        function(CREIds0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "serviceNotRequiredServerDataTableMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0))})
        }
      """
    )
  
    // @LINE:79
    def getInsuranceassignedInteractionTableDataMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.getInsuranceassignedInteractionTableDataMR",
      """
        function(CREIds0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/InsuranceassignedInteractionTableDataMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0))})
        }
      """
    )
  
    // @LINE:437
    def assignedInteractionDataMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.assignedInteractionDataMR",
      """
        function(CREIds0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assignedInteractionTableDataMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0))})
        }
      """
    )
  
    // @LINE:445
    def outgoingCallsServerDataTableMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.outgoingCallsServerDataTableMR",
      """
        function(CREIds0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "outgoingCallsServerDataTableMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0))})
        }
      """
    )
  
    // @LINE:80
    def getInsurancefollowUpCallLogTableDataMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.getInsurancefollowUpCallLogTableDataMR",
      """
        function(CREIds0,buckettype1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/InsurancefollowUpCallLogTableDataMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("buckettype", buckettype1)})
        }
      """
    )
  
    // @LINE:442
    def droppedCallInteractionDataMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.droppedCallInteractionDataMR",
      """
        function(CREIds0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "droppedCallsServerDataTableMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0))})
        }
      """
    )
  
    // @LINE:441
    def nonContactsInteractionDataMR: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchControllerMR.nonContactsInteractionDataMR",
      """
        function(CREIds0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "nonContactsServerDataTableMR/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("CREIds", encodeURIComponent(CREIds0))})
        }
      """
    )
  
  }

  // @LINE:469
  class ReverseAutoSelectionSAController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:471
    def ajaxupdateSaChange: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AutoSelectionSAController.ajaxupdateSaChange",
      """
        function(workshopId0,date1,preSaDetails2,newSaDetails3) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/callDispositionPage/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("workshopId", workshopId0) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("date", encodeURIComponent(date1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("preSaDetails", encodeURIComponent(preSaDetails2)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("newSaDetails", encodeURIComponent(newSaDetails3))})
        }
      """
    )
  
    // @LINE:470
    def ajaxAutoSASelectionList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AutoSelectionSAController.ajaxAutoSASelectionList",
      """
        function(saDetails0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/callDispositionPage/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("saDetails", encodeURIComponent(saDetails0))})
        }
      """
    )
  
    // @LINE:469
    def ajaxAutoSASelection: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AutoSelectionSAController.ajaxAutoSASelection",
      """
        function(workshopId0,date1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/callDispositionPage/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("workshopId", workshopId0) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("date", encodeURIComponent(date1))})
        }
      """
    )
  
  }

  // @LINE:7
  class ReverseApplication(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:7
    def landingPage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.landingPage",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
    // @LINE:16
    def formIndex: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.formIndex",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "form/index.html"})
        }
      """
    )
  
    // @LINE:9
    def login: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.login",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
        }
      """
    )
  
  }

  // @LINE:568
  class ReverseChangeAssignmentController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:581
    def assignListToUserSelected: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChangeAssignmentController.assignListToUserSelected",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/assignedCallsListToUser"})
        }
      """
    )
  
    // @LINE:571
    def changeAssignedCallsToAgents: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChangeAssignmentController.changeAssignedCallsToAgents",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/assignCallToAgents"})
        }
      """
    )
  
    // @LINE:568
    def changeassignedCalls: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChangeAssignmentController.changeassignedCalls",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/changeAssignedCalls"})
        }
      """
    )
  
    // @LINE:569
    def postchangeassignedCalls: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChangeAssignmentController.postchangeassignedCalls",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/changeAssignedCalls"})
        }
      """
    )
  
    // @LINE:579
    def assignmentFilterList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChangeAssignmentController.assignmentFilterList",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/assignmentFilterPage"})
        }
      """
    )
  
    // @LINE:580
    def getAssignmentFilterListAjax: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChangeAssignmentController.getAssignmentFilterListAjax",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/assignedCallsList"})
        }
      """
    )
  
  }

  // @LINE:525
  class ReverseInsuranceHistoryController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:525
    def uploadHistoryViewPage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceHistoryController.uploadHistoryViewPage",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "insuranceHistoryUpload"})
        }
      """
    )
  
    // @LINE:526
    def uploadExcelDataHistory: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InsuranceHistoryController.uploadExcelDataHistory",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "insuranceHistoryUpload"})
        }
      """
    )
  
  }

  // @LINE:536
  class ReverseProcessUploadedFiles(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:536
    def process: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProcessUploadedFiles.process",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "processFile"})
        }
      """
    )
  
    // @LINE:541
    def processFile: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProcessUploadedFiles.processFile",
      """
        function(id0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "wyzprocessFile/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id0))})
        }
      """
    )
  
  }

  // @LINE:181
  class ReverseSearchController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:181
    def startSyncOfReports: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.startSyncOfReports",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "service/startSyncReports"})
        }
      """
    )
  
    // @LINE:454
    def getDashboardCountCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.getDashboardCountCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/dashboardCounts"})
        }
      """
    )
  
    // @LINE:379
    def assignedInteractionData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.assignedInteractionData",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assignedInteractionTableData"})
        }
      """
    )
  
    // @LINE:249
    def uploadHistoryReport: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.uploadHistoryReport",
      """
        function(uploadId0,uploadType1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/uploadDataReport/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("uploadId", encodeURIComponent(uploadId0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("uploadType", encodeURIComponent(uploadType1))})
        }
      """
    )
  
    // @LINE:381
    def serviceBookedInteractionData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.serviceBookedInteractionData",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "serviceBookedServerDataTable"})
        }
      """
    )
  
    // @LINE:384
    def droppedCallInteractionData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.droppedCallInteractionData",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "droppedCallsServerDataTable"})
        }
      """
    )
  
    // @LINE:383
    def nonContactsInteractionData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.nonContactsInteractionData",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "nonContactsServerDataTable"})
        }
      """
    )
  
    // @LINE:215
    def searchCustomer: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.searchCustomer",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchByCustomerv2"})
        }
      """
    )
  
    // @LINE:250
    def uploadHistoryViewData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.uploadHistoryViewData",
      """
        function(typeIds0,fromDate1,toDate2,uploadReportId3) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "uploadReport/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("typeIds", encodeURIComponent(typeIds0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("fromDate", encodeURIComponent(fromDate1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("toDate", encodeURIComponent(toDate2)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("uploadReportId", encodeURIComponent(uploadReportId3))})
        }
      """
    )
  
    // @LINE:382
    def serviceNotRequiredInteractionData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.serviceNotRequiredInteractionData",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "serviceNotRequiredServerDataTable"})
        }
      """
    )
  
    // @LINE:380
    def followUpRequiredInteractionData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.followUpRequiredInteractionData",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "followUpCallLogTableData"})
        }
      """
    )
  
  }

  // @LINE:404
  class ReverseCampaignController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:574
    def getCampaignNamesByUpload: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CampaignController.getCampaignNamesByUpload",
      """
        function(uploadType0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getCampaignList/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("uploadType", encodeURIComponent(uploadType0))})
        }
      """
    )
  
    // @LINE:405
    def postCampaign: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CampaignController.postCampaign",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "postCampaign"})
        }
      """
    )
  
    // @LINE:404
    def addCampaign: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CampaignController.addCampaign",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addcampaign"})
        }
      """
    )
  
    // @LINE:519
    def addCampaignInsurance: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CampaignController.addCampaignInsurance",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/addInsuranceCampaign"})
        }
      """
    )
  
  }

  // @LINE:105
  class ReverseReportsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:126
    def getAllAjaxRequestForCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReportsController.getAllAjaxRequestForCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/getAjaxDataCompleteData"})
        }
      """
    )
  
    // @LINE:133
    def getBookedListByTimeForCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReportsController.getBookedListByTimeForCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/getBookedListByTime"})
        }
      """
    )
  
    // @LINE:195
    def getBookedListByTime: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReportsController.getBookedListByTime",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ajax/getBookedListByTime"})
        }
      """
    )
  
    // @LINE:193
    def getScheduledCallsCountCREMan: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReportsController.getScheduledCallsCountCREMan",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ajax/getTtlSchCallsCREManager"})
        }
      """
    )
  
    // @LINE:131
    def getServiceBookedPercentageForCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReportsController.getServiceBookedPercentageForCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/getPercServiceBookedForsCRE"})
        }
      """
    )
  
    // @LINE:128
    def getScheduledCallsCountOfCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReportsController.getScheduledCallsCountOfCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/getTtlSchCallsCRE"})
        }
      """
    )
  
    // @LINE:132
    def getCallTypePieForCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReportsController.getCallTypePieForCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/getCallTypePieForCRE"})
        }
      """
    )
  
    // @LINE:130
    def getServiceBookedForCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReportsController.getServiceBookedForCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/getTtlServiceBookedForCRE"})
        }
      """
    )
  
    // @LINE:129
    def getScheduledCallsPendingCountForCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReportsController.getScheduledCallsPendingCountForCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/ajax/getTtlSchCallsPendingCRE"})
        }
      """
    )
  
    // @LINE:197
    def getCallTypePie: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReportsController.getCallTypePie",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ajax/getCallTypePie"})
        }
      """
    )
  
    // @LINE:105
    def viewReportCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReportsController.viewReportCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/viewReport"})
        }
      """
    )
  
  }

  // @LINE:176
  class ReverseUserAuthenticator(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:179
    def updateUserAuthentication: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UserAuthenticator.updateUserAuthentication",
      """
        function(phoneIMEINo0,registrationId1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "updateUserAuthentication" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("phoneIMEINo", phoneIMEINo0), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("registrationId", registrationId1)])})
        }
      """
    )
  
    // @LINE:176
    def authenticateUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UserAuthenticator.authenticateUser",
      """
        function(phoneNumber0,phoneIMEINo1,registrationId2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users/authenticate/" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("phoneNumber", phoneNumber0), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("phoneIMEINo", phoneIMEINo1), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("registrationId", registrationId2)])})
        }
      """
    )
  
  }

  // @LINE:262
  class ReverseFirebaseSyncController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:646
    def startSyncOperationInsuranceAgentHistory: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.startSyncOperationInsuranceAgentHistory",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/startSyncInsuranceHistory"})
        }
      """
    )
  
    // @LINE:272
    def stopSyncOperationPSFCallServiceAgent: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.stopSyncOperationPSFCallServiceAgent",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/stopSyncPSFCall"})
        }
      """
    )
  
    // @LINE:645
    def startSyncOperationInsuranceAgent: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.startSyncOperationInsuranceAgent",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/startSyncInsuranceAgent"})
        }
      """
    )
  
    // @LINE:274
    def startServiceAdvisorPSFHistorySync: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.startServiceAdvisorPSFHistorySync",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/startSyncPSFHistory"})
        }
      """
    )
  
    // @LINE:266
    def stopSyncOperationServiceAgent: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.stopSyncOperationServiceAgent",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/stopSyncServiceAgent"})
        }
      """
    )
  
    // @LINE:268
    def stopServiceAdvisorSummaryDetailSync: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.stopServiceAdvisorSummaryDetailSync",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/stopSyncSummarySync"})
        }
      """
    )
  
    // @LINE:262
    def startSyncOperationServiceAgent: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.startSyncOperationServiceAgent",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/startSyncServiceAgent"})
        }
      """
    )
  
    // @LINE:263
    def startServiceAdvisorHistorySync: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.startServiceAdvisorHistorySync",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/startSyncHistorySync"})
        }
      """
    )
  
    // @LINE:390
    def startSyncOperationDriverPickupDropList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.startSyncOperationDriverPickupDropList",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "driverData"})
        }
      """
    )
  
    // @LINE:271
    def startSyncOperationPSFCallServiceAgent: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.startSyncOperationPSFCallServiceAgent",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/startSyncPSFCall"})
        }
      """
    )
  
    // @LINE:264
    def startServiceAdvisorSummaryDetailSync: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.startServiceAdvisorSummaryDetailSync",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/startSyncSummarySync"})
        }
      """
    )
  
    // @LINE:267
    def stopServiceAdvisorHistorySync: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.stopServiceAdvisorHistorySync",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CREManager/stopSyncHistorySync"})
        }
      """
    )
  
    // @LINE:652
    def startSyncOperationAllEvents: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FirebaseSyncController.startSyncOperationAllEvents",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "startAllEvents"})
        }
      """
    )
  
  }

  // @LINE:44
  class ReverseScheduledCallController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:44
    def startSyncForTesting: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ScheduledCallController.startSyncForTesting",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "deleteRecordFireBase"})
        }
      """
    )
  
    // @LINE:118
    def getCallDispositionTabPAgeCRE: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ScheduledCallController.getCallDispositionTabPAgeCRE",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "CRE/getDispositionPageOfTab"})
        }
      """
    )
  
  }


}
