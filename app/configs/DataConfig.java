package configs;

import java.beans.PropertyVetoException;
import java.util.HashMap;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.MultiTenancyStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import controllers.multitenant.MyTenantIdentifier;

@Configuration
@EnableTransactionManagement
public class DataConfig {

	@Autowired
	private AnnotationConfigApplicationContext applicationContext;

	@Bean
	public ApplicationContext applicationContext() {
		return applicationContext;
	}

	@Bean
	public EntityManagerFactory entityManagerFactory() throws PropertyVetoException {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabase(Database.MYSQL);
		vendorAdapter.setShowSql(false);
		vendorAdapter.setGenerateDdl(true);
		LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactory.setPackagesToScan("models","models.upload","models.sales");
		entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
		//entityManagerFactory.setDataSource(dataSource());
		HashMap<String, Object> propertiesMap = new HashMap<String, Object>();
		propertiesMap.put("hibernate.hbm2ddl.auto", "update");
		propertiesMap.put("hibernate.id.new_generator_mappings", "true");
		propertiesMap.put("hibernate.format_sql", "true");

		propertiesMap.put(org.hibernate.cfg.Environment.MULTI_TENANT_IDENTIFIER_RESOLVER, myTenantIdentifier());
		propertiesMap.put(org.hibernate.cfg.Environment.MULTI_TENANT_CONNECTION_PROVIDER,
				wyzAppMultiTenantConnProvider());
		propertiesMap.put(org.hibernate.cfg.Environment.MULTI_TENANT, MultiTenancyStrategy.DATABASE);

		entityManagerFactory.setJpaPropertyMap(propertiesMap);
		entityManagerFactory.afterPropertiesSet();
		return entityManagerFactory.getObject();
	}

	@Bean
	public PlatformTransactionManager transactionManager() throws PropertyVetoException {
		return new JpaTransactionManager(entityManagerFactory());
	}

/*	@Bean
	public DataSource dataSource() throws PropertyVetoException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		Config configuration = ConfigFactory.load();
		dataSource.setDriverClass(configuration.getString("db.spring.driver"));
		dataSource.setJdbcUrl(configuration.getString("db.spring.url"));
		dataSource.setUser(configuration.getString("db.spring.user"));
		dataSource.setPassword(configuration.getString("db.spring.password"));

		// the settings below are optional -- c3p0 can work with defaults
		dataSource.setMinPoolSize(20);
		dataSource.setAcquireIncrement(10);
		dataSource.setMaxPoolSize(50);

		// The DataSource cpds is now a fully configured and usable pooled
		// DataSource

		return dataSource;
	}
*/
	@Bean
	public JinqSource jinqSource() {
		return new JinqSource();
	}

	@Bean(destroyMethod = "")
	public WyzAppMultiTenantConnProvider wyzAppMultiTenantConnProvider() {
		return new WyzAppMultiTenantConnProvider();
	}

	@Bean
	public MyTenantIdentifier myTenantIdentifier() {
		MyTenantIdentifier myTenantIdentifier = new MyTenantIdentifier();
		return myTenantIdentifier;

	}

}