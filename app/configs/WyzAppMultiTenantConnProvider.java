package configs;

import java.util.Arrays;
import java.util.HashMap;

import org.hibernate.engine.jdbc.connections.spi.AbstractMultiTenantConnectionProvider;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import play.Logger.ALogger;

public class WyzAppMultiTenantConnProvider extends AbstractMultiTenantConnectionProvider {

	ALogger logger = play.Logger.of("application");

	private static final long serialVersionUID = 2L;
	private HashMap<String, ConnectionProvider> connProviderMap = new HashMap<>();

	public WyzAppMultiTenantConnProvider() {

		// for more practical implementation we can read those databases from a
		// database table.
		for (String providerName : Arrays.asList(new String[] { "cauveryford" })) {

			logger.info("Adding the connection provider : "+providerName);

			connProviderMap.put(providerName, new WyzAppConnectionProviderImpl(providerName));
		}

	}

	@Override
	protected ConnectionProvider getAnyConnectionProvider() {
    	Config configuration = ConfigFactory.load();
		String defaultDataBaseName = configuration.getString("app.defaultdatabase");
		logger.debug("the selected database is : "+defaultDataBaseName);
		ConnectionProvider provider = connProviderMap.get(defaultDataBaseName);
		if(provider == null){
			logger.info("Provider is null");
			return null;
		}else{
			logger.info("Provider is not null:"+provider);
			return provider;
		}
 	
	}

	@Override
	protected ConnectionProvider selectConnectionProvider(String s) {
		
    	logger.debug("Obtaining connection for:"+s);
    	logger.debug("connProviderMap connection for:"+ connProviderMap.get(s));
    	if(connProviderMap.get(s) != null){
    		return connProviderMap.get(s);
    		
    	}else{
    		logger.debug("New Connection is created for : "+s);	    		
    		WyzAppConnectionProviderImpl newconnection=new WyzAppConnectionProviderImpl(s);
    		connProviderMap.put(s, newconnection);
    		return connProviderMap.get(s);
    		
    	}

	
	}

}
