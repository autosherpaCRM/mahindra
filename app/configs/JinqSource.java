package configs;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import models.Address;
import models.AppointmentBooked;
import models.AssignedInteraction;
import models.BookingDateTime;
import models.CallDispositionData;
import models.CallHistoryCube;
import models.CallInteraction;
import models.Campaign;
import models.CityStates;
import models.Complaint;
import models.Customer;
import models.CustomerSearchResult;
import models.Dealer;
import models.DeletedStatusReport;
import models.Driver;
import models.Email;
import models.IRDA_OD_PREMIUM;
import models.Insurance;
import models.InsuranceAgent;
import models.InsuranceAssignedInteraction;
import models.InsuranceCallHistoryCube;
import models.InsuranceExcelError;
import models.Location;
import models.MessageParameters;
import models.MessageTemplete;
import models.Phone;
import models.PickupDrop;
import models.Role;
import models.SMSInteraction;
import models.SMSParameters;
import models.SRDisposition;
import models.SavedSearchResult;
import models.Segment;
import models.Service;
import models.ServiceAdvisor;
import models.ServiceBooked;
import models.ServiceBookedSynced;
import models.ServiceDoneSchCall;
import models.ServiceTypes;
import models.ShowRooms;
import models.SpecialOfferMaster;
import models.SynchedKey;
import models.SynchedKeySchCalls;
import models.TaggingUsers;
import models.Tenant;
import models.Vehicle;
import models.VehicleSummaryCube;
import models.Workshop;
import models.SMSTemplate;
import models.WorkshopSummary;
import models.WyzUser;
import models.UnAvailability;
import models.Upload;
import models.UploadData;
import models.UploadMasterFormat;
import models.UploadReportFiles;
import models.UploadType;
import models.UpsellLead;
import models.UserSession;
import models.Outlet;
import models.PSFAssignedInteraction;
import models.ComplaintAssignedInteraction;
import models.ComplaintInteraction;
import models.ComplaintSource;
import models.ComplaintTypes;
import models.FormData;

import org.jinq.jpa.JPAJinqStream;
import org.jinq.jpa.JinqJPAStreamProvider;
import org.springframework.stereotype.Component;

/**
 * Created by enda on 31/08/15.
 */
@Component
public class JinqSource {
	private JinqJPAStreamProvider streams;

	@PersistenceUnit
	public void setEntityManagerFactory(EntityManagerFactory emf) throws Exception {
		streams = new JinqJPAStreamProvider(emf);
	}

	// Wrapper that passes through Jinq requests to Jinq
	public <U> JPAJinqStream<U> streamAll(EntityManager em, Class<U> entity) {
		return streams.streamAll(em, entity);
	}

	public JPAJinqStream<WyzUser> wyzUsers(EntityManager em) {
		return streams.streamAll(em, WyzUser.class);
	}
	public JPAJinqStream<DeletedStatusReport> deletedStatusReports(EntityManager em) {
		return streams.streamAll(em, DeletedStatusReport.class);
	}
	
	public JPAJinqStream<VehicleSummaryCube> vehicleSummaryCubes(EntityManager em) {
		return streams.streamAll(em, VehicleSummaryCube.class);
	}
	public JPAJinqStream<Role> roles(EntityManager em) {
		return streams.streamAll(em, Role.class);
	}

	public JPAJinqStream<Complaint> complaint(EntityManager em) {
		return streams.streamAll(em, Complaint.class);
	}

	public JPAJinqStream<FormData> formdata(EntityManager em) {
		return streams.streamAll(em, FormData.class);
	}
	public JPAJinqStream<Customer> customers(EntityManager em) {
		return streams.streamAll(em, Customer.class);
	}

	public JPAJinqStream<ComplaintInteraction> complaintInteractions(EntityManager em) {
		return streams.streamAll(em, ComplaintInteraction.class);
	}

	public JPAJinqStream<ComplaintAssignedInteraction> complaintAssignedInteractions(EntityManager em) {
		return streams.streamAll(em, ComplaintAssignedInteraction.class);
	}

	public JPAJinqStream<Dealer> dealers(EntityManager em) {
		return streams.streamAll(em, Dealer.class);
	}

	public JPAJinqStream<CallDispositionData> callDispositionDatas(EntityManager em) {
		return streams.streamAll(em, CallDispositionData.class);
	}

	public JPAJinqStream<AssignedInteraction> assignedInteractions(EntityManager em) {
		return streams.streamAll(em, AssignedInteraction.class);

	}

	public JPAJinqStream<CallInteraction> callinteractions(EntityManager em) {
		return streams.streamAll(em, CallInteraction.class);

	}

	public JPAJinqStream<Segment> segments(EntityManager em) {
		return streams.streamAll(em, Segment.class);

	}

	public JPAJinqStream<Phone> phones(EntityManager em) {
		return streams.streamAll(em, Phone.class);

	}

	public JPAJinqStream<Address> address(EntityManager em) {
		return streams.streamAll(em, Address.class);
	}

	public JPAJinqStream<Vehicle> vehicle(EntityManager em) {
		return streams.streamAll(em, Vehicle.class);

	}

	public JPAJinqStream<Service> services(EntityManager em) {
		return streams.streamAll(em, Service.class);

	}

	public JPAJinqStream<Outlet> outlets(EntityManager em) {
		return streams.streamAll(em, Outlet.class);

	}

	public JPAJinqStream<ServiceAdvisor> serviceadvisor(EntityManager em) {
		return streams.streamAll(em, ServiceAdvisor.class);

	}

	public JPAJinqStream<Driver> driver(EntityManager em) {
		return streams.streamAll(em, Driver.class);

	}

	public JPAJinqStream<Workshop> workshop(EntityManager em) {
		return streams.streamAll(em, Workshop.class);

	}

	public JPAJinqStream<WorkshopSummary> workshopSummarys(EntityManager em) {
		return streams.streamAll(em, WorkshopSummary.class);

	}

	public JPAJinqStream<Location> locations(EntityManager em) {
		return streams.streamAll(em, Location.class);

	}

	public JPAJinqStream<SynchedKey> synchedKey(EntityManager em) {
		return streams.streamAll(em, SynchedKey.class);
	}

	public JPAJinqStream<PickupDrop> pickupDrop(EntityManager em) {
		return streams.streamAll(em, PickupDrop.class);
	}

	public JPAJinqStream<SynchedKeySchCalls> sychedKeySchCalls(EntityManager em) {
		return streams.streamAll(em, SynchedKeySchCalls.class);
	}

	public JPAJinqStream<ServiceDoneSchCall> serviceDoneSchCalls(EntityManager em) {
		return streams.streamAll(em, ServiceDoneSchCall.class);
	}

	public JPAJinqStream<MessageTemplete> messageTempletes(EntityManager em) {
		return streams.streamAll(em, MessageTemplete.class);
	}

	public JPAJinqStream<ServiceBookedSynced> serviceBookedSynced(EntityManager em) {
		return streams.streamAll(em, ServiceBookedSynced.class);
	}

	public JPAJinqStream<SRDisposition> srDisposition(EntityManager em) {
		return streams.streamAll(em, SRDisposition.class);
	}

	public JPAJinqStream<ServiceBooked> serviceBooked(EntityManager em) {
		return streams.streamAll(em, ServiceBooked.class);

	}

	public JPAJinqStream<UnAvailability> unAvailabilitys(EntityManager em) {
		return streams.streamAll(em, UnAvailability.class);

	}

	public JPAJinqStream<TaggingUsers> taggingUsers(EntityManager em) {
		return streams.streamAll(em, TaggingUsers.class);

	}

	public JPAJinqStream<Campaign> campaigns(EntityManager em) {
		return streams.streamAll(em, Campaign.class);

	}
	
	public JPAJinqStream<Email> emails(EntityManager em) {
		return streams.streamAll(em, Email.class);

	}

	public JPAJinqStream<PSFAssignedInteraction> psfAssignedInteraction(EntityManager em) {
		return streams.streamAll(em, PSFAssignedInteraction.class);

	}

	public JPAJinqStream<SMSTemplate> smsTemplates(EntityManager em) {
		return streams.streamAll(em, SMSTemplate.class);
	}

	public JPAJinqStream<SMSInteraction> smsInteractions(EntityManager em) {
		return streams.streamAll(em, SMSInteraction.class);
	}

	public JPAJinqStream<UploadMasterFormat> uploadMasterFormat(EntityManager em) {
		return streams.streamAll(em, UploadMasterFormat.class);
	}

	public JPAJinqStream<UploadData> uploadDatas(EntityManager em) {
		return streams.streamAll(em, UploadData.class);
	}

	public JPAJinqStream<ServiceTypes> serviceTypes(EntityManager em) {
		return streams.streamAll(em, ServiceTypes.class);
	}

	public JPAJinqStream<UpsellLead> upsellLeads(EntityManager em) {
		return streams.streamAll(em, UpsellLead.class);
	}

	public JPAJinqStream<SpecialOfferMaster> specialOfferMasters(EntityManager em) {
		return streams.streamAll(em, SpecialOfferMaster.class);
	}

	public JPAJinqStream<UserSession> userSessions(EntityManager em) {
		return streams.streamAll(em, UserSession.class);
	}

	public JPAJinqStream<InsuranceAssignedInteraction> insuranceAssignedInteraction(EntityManager em) {
		return streams.streamAll(em, InsuranceAssignedInteraction.class);
	}

	public JPAJinqStream<IRDA_OD_PREMIUM> iRDA_OD_PREMIUMS(EntityManager em) {
		return streams.streamAll(em, IRDA_OD_PREMIUM.class);
	}

	public JPAJinqStream<ShowRooms> showRooms(EntityManager em) {
		return streams.streamAll(em, ShowRooms.class);
	}

	public JPAJinqStream<Insurance> insurances(EntityManager em) {
		return streams.streamAll(em, Insurance.class);
	}

	public JPAJinqStream<InsuranceExcelError> insuranceExcelErrors(EntityManager em) {
		return streams.streamAll(em, InsuranceExcelError.class);
	}

	public JPAJinqStream<AppointmentBooked> appointmentsBooked(EntityManager em) {
		return streams.streamAll(em, AppointmentBooked.class);
	}

	public JPAJinqStream<InsuranceAgent> insuranceAgents(EntityManager em) {
		return streams.streamAll(em, InsuranceAgent.class);
	}

	public JPAJinqStream<Upload> uploads(EntityManager em) {
		return streams.streamAll(em, Upload.class);
	}

	public JPAJinqStream<UploadType> uploadTypes(EntityManager em) {
		return streams.streamAll(em, UploadType.class);
	}

	public JPAJinqStream<CityStates> citystates(EntityManager em) {
		return streams.streamAll(em, CityStates.class);
	}

	public JPAJinqStream<Tenant> tenant(EntityManager em) {
		return streams.streamAll(em, Tenant.class);
	}

	public JPAJinqStream<MessageParameters> messageParameters(EntityManager em) {
		return streams.streamAll(em, MessageParameters.class);
	}

	public JPAJinqStream<SavedSearchResult> savedSearchs(EntityManager em) {
		return streams.streamAll(em, SavedSearchResult.class);
	}

	public JPAJinqStream<CustomerSearchResult> customerSearchResult(EntityManager em) {
		return streams.streamAll(em, CustomerSearchResult.class);
	}
	
	public JPAJinqStream<ComplaintSource> complaintSource(EntityManager em) {
		return streams.streamAll(em, ComplaintSource.class);
	}
	
	public JPAJinqStream<BookingDateTime> bookingDateTime(EntityManager em) {
		return streams.streamAll(em, BookingDateTime.class);
	}
	
	public JPAJinqStream<ComplaintTypes> complaintTypes(EntityManager em) {
		return streams.streamAll(em, ComplaintTypes.class);
	}
	
	public JPAJinqStream<SMSParameters> smsParameters(EntityManager em) {
		return streams.streamAll(em, SMSParameters.class);
	}
	
	public JPAJinqStream<CallHistoryCube> callHistoryCube(EntityManager em) {
		return streams.streamAll(em, CallHistoryCube.class);
	}
	
	public JPAJinqStream<UploadReportFiles> uploadReportFiles(EntityManager em) {
		return streams.streamAll(em, UploadReportFiles.class);
	}
	
	public JPAJinqStream<InsuranceCallHistoryCube> insuranceCallHistoryCube(EntityManager em) {
		return streams.streamAll(em, InsuranceCallHistoryCube.class);
	}

}
