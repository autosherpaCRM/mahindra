package models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class SRDisposition {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@Column(length = 30)
	public String InOutCallName;

	@Column(length = 30)
	public String typeOfDisposition;

	@OneToOne
	public CallDispositionData callDispositionData;

	@Column(length = 70)
	public String noServiceReason;

	@Column(length = 70)
	public String reasonForHTML;

	@Temporal(TemporalType.DATE)
	public Date followUpDate;

	public String followUpTime;
	public String isFollowupRequired;
	public String isFollowUpDone;
	public String cityName;
	public String reason;
	public String serviceLocation;
	public String mileageAtService;
	public String mileageAsOnDate;
	public String serviceType;
	public String dateOfService;
	public String dealerName;
	public String comments;

	public String transferedCity;

	public String serviceAdvisorID;
	public long assignedToSA;
	public String ServicedAtOtherDealerRadio;
	public String checkedwithDMS;
	@Temporal(TemporalType.DATE)
	public Date lastServiceDate;
	@Temporal(TemporalType.DATE)
	public Date lastServiceDateOfDWPS;
	public String DistancefromDealerLocationRadio;
	public String pinCodeOfCity;
	public String optforMMS;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "callInteraction_id", nullable = true)
	public CallInteraction callInteraction;
	

	public String other;

	@Column(length = 100)
	public String departmentForFB;
	@Column(length = 100)
	public String complaintOrFB_TagTo;
	@Column(length = 250)
	public String remarksOfFB;

	@Column(length = 70)
	public String noServiceReasonTaggedTo;

	@Column(length = 70)
	public String noServiceReasonTaggedToComments;

	// IN BOUND REQUIRED FIELDS
	
	public long upsellCount;
	
	@Column(length = 100)
	public String inBoundLeadSource;
	

	@OneToMany(mappedBy = "srDisposition")
	public List<UpsellLead> upsellLead;

	//Kms not required filelds
	
	public long currentMileage;
	
	@Temporal(TemporalType.DATE)
	public Date expectedVisitDate;
	
	//Remarks fields
	
	@Column(length = 300)
	public String remarks;
	
	
	public String getTransferedCity() {
		return transferedCity;
	}

	public void setTransferedCity(String transferedCity) {
		this.transferedCity = transferedCity;
	}

	public String getNoServiceReasonTaggedTo() {
		return noServiceReasonTaggedTo;
	}

	public void setNoServiceReasonTaggedTo(String noServiceReasonTaggedTo) {
		this.noServiceReasonTaggedTo = noServiceReasonTaggedTo;
	}

	public String getNoServiceReasonTaggedToComments() {
		return noServiceReasonTaggedToComments;
	}

	public void setNoServiceReasonTaggedToComments(String noServiceReasonTaggedToComments) {
		this.noServiceReasonTaggedToComments = noServiceReasonTaggedToComments;
	}

	public String getInBoundLeadSource() {
		return inBoundLeadSource;
	}

	public void setInBoundLeadSource(String inBoundLeadSource) {
		this.inBoundLeadSource = inBoundLeadSource;
	}

	public String getComplaintOrFB_TagTo() {
		return complaintOrFB_TagTo;
	}

	public void setComplaintOrFB_TagTo(String complaintOrFB_TagTo) {
		this.complaintOrFB_TagTo = complaintOrFB_TagTo;
	}

	public String getDepartmentForFB() {
		return departmentForFB;
	}

	public void setDepartmentForFB(String departmentForFB) {
		this.departmentForFB = departmentForFB;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTypeOfDisposition() {
		return typeOfDisposition;
	}

	public void setTypeOfDisposition(String typeOfDisposition) {
		this.typeOfDisposition = typeOfDisposition;
	}

	public CallDispositionData getCallDispositionData() {
		return callDispositionData;
	}

	public void setCallDispositionData(CallDispositionData callDisposition1) {
		this.callDispositionData = callDisposition1;
	}

	public void setNoServiceReason(String noServiceReason) {
		this.noServiceReason = noServiceReason;
	}

	public String getNoServiceReason() {
		return noServiceReason;
	}

	public String getReasonForHTML() {
		return reasonForHTML;
	}

	public void setReasonForHTML(String reasonForHTML) {
		this.reasonForHTML = reasonForHTML;
	}

	public Date getFollowUpDate() {
		return followUpDate;
	}

	public void setFollowUpDate(Date followUpDate) {
		this.followUpDate = followUpDate;
	}

	public String getFollowUpTime() {
		return followUpTime;
	}

	public void setFollowUpTime(String followUpTime) {
		this.followUpTime = followUpTime;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getServiceLocation() {
		return serviceLocation;
	}

	public void setServiceLocation(String serviceLocation) {
		this.serviceLocation = serviceLocation;
	}

	public String getMileageAtService() {
		return mileageAtService;
	}

	public void setMileageAtService(String mileageAtService) {
		this.mileageAtService = mileageAtService;
	}

	public String getMileageAsOnDate() {
		return mileageAsOnDate;
	}

	public void setMileageAsOnDate(String mileageAsOnDate) {
		this.mileageAsOnDate = mileageAsOnDate;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getDateOfService() {
		return dateOfService;
	}

	public void setDateOfService(String dateOfService) {
		this.dateOfService = dateOfService;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public CallInteraction getCallInteraction() {
		return callInteraction;
	}

	public void setCallInteraction(CallInteraction callInteraction) {
		this.callInteraction = callInteraction;
	}

	public String getIsFollowupRequired() {
		return isFollowupRequired;
	}

	public void setIsFollowupRequired(String isFollowupRequired) {
		this.isFollowupRequired = isFollowupRequired;
	}

	public String getIsFollowUpDone() {
		return isFollowUpDone;
	}

	public void setIsFollowUpDone(String isFollowUpDone) {
		this.isFollowUpDone = isFollowUpDone;
	}

	public String getServiceAdvisorID() {
		return serviceAdvisorID;
	}

	public void setServiceAdvisorID(String serviceAdvisorID) {
		this.serviceAdvisorID = serviceAdvisorID;
	}

	public long getAssignedToSA() {
		return assignedToSA;
	}

	public void setAssignedToSA(long assignedToSA) {
		this.assignedToSA = assignedToSA;
	}

	public String getServicedAtOtherDealerRadio() {
		return ServicedAtOtherDealerRadio;
	}

	public void setServicedAtOtherDealerRadio(String ServicedAtOtherDealerRadio) {
		this.ServicedAtOtherDealerRadio = ServicedAtOtherDealerRadio;
	}

	public String getCheckedwithDMS() {
		return checkedwithDMS;
	}

	public void setCheckedwithDMS(String checkedwithDMS) {
		this.checkedwithDMS = checkedwithDMS;
	}

	public Date getLastServiceDate() {
		return lastServiceDate;
	}

	public void setLastServiceDate(Date lastServiceDate) {
		this.lastServiceDate = lastServiceDate;
	}

	public Date getLastServiceDateOfDWPS() {
		return lastServiceDateOfDWPS;
	}

	public void setLastServiceDateOfDWPS(Date lastServiceDateOfDWPS) {
		this.lastServiceDateOfDWPS = lastServiceDateOfDWPS;
	}

	public String getDistancefromDealerLocationRadio() {
		return DistancefromDealerLocationRadio;
	}

	public void setDistancefromDealerLocationRadio(String DistancefromDealerLocationRadio) {
		this.DistancefromDealerLocationRadio = DistancefromDealerLocationRadio;
	}

	public String getPinCodeOfCity() {
		return pinCodeOfCity;
	}

	public void setPinCodeOfCity(String pinCodeOfCity) {
		this.pinCodeOfCity = pinCodeOfCity;
	}

	public String getOptforMMS() {
		return optforMMS;
	}

	public void setOptforMMS(String optforMMS) {
		this.optforMMS = optforMMS;
	}

	public List<UpsellLead> getUpsellLead() {
		return upsellLead;
	}

	public void setUpsellLead(List<UpsellLead> upsellLead) {
		this.upsellLead = upsellLead;
	}

	public String getInOutCallName() {
		return InOutCallName;
	}

	public void setInOutCallName(String InOutCallName) {
		this.InOutCallName = InOutCallName;
	}

	public String getRemarksOfFB() {
		return remarksOfFB;
	}

	public void setRemarksOfFB(String remarksOfFB) {
		this.remarksOfFB = remarksOfFB;
	}

	
	public long getUpsellCount() {
		return upsellCount;
	}

	public void setUpsellCount(long upsellCount) {
		this.upsellCount = upsellCount;
	}

	public long getCurrentMileage() {
		return currentMileage;
	}

	public void setCurrentMileage(long currentMileage) {
		this.currentMileage = currentMileage;
	}

	public Date getExpectedVisitDate() {
		return expectedVisitDate;
	}

	public void setExpectedVisitDate(Date expectedVisitDate) {
		this.expectedVisitDate = expectedVisitDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	private String cityOfDistanceFromDealerLocation;


	public String getCityOfDistanceFromDealerLocation() {
		return cityOfDistanceFromDealerLocation;
	}

	public void setCityOfDistanceFromDealerLocation(String cityOfDistanceFromDealerLocation) {
		this.cityOfDistanceFromDealerLocation = cityOfDistanceFromDealerLocation;
	}

}
