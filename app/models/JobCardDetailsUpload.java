package models;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class JobCardDetailsUpload {

	public long id;
	public String jobCardNumber;
	public String customerName;
	public String customerPhone;
	public String customerCat;
	public String psfStatus;
	public String vehicleRegnumber;
	public String model;
	public String serviceAdvisor;
	public String technician;
	public String servicetype;
	public String repeat_revisit;
	public String chassisNo;
	public String mi;
	public String groupData;
	public String circularNo;
	public String mileage;
	public double estLabAmt;
	public double estPartAmt;
	public String status;
	public String billNo;
	public double labAmt;
	public double partsAmt;
	public String pickuprequired;
	public String pickupLocation;
	public double billAmt;
	public String addressline1;
	public String addressline2;
	public String addressline3;
	public String city;
	public long pincode;
	public String engineNo;
	public String variant;
	public String color;
	public String mobileNo;
	public long year;
	@Temporal(TemporalType.DATE)
	public Date promiseDate;
	@Temporal(TemporalType.DATE)
	public Date revisedPromiseDate;
	@Temporal(TemporalType.DATE)
	public Date readyDate;
	public Date jobCardDate;
	@Temporal(TemporalType.DATE)
	public Date saleDate;
	@Temporal(TemporalType.DATE)
	public Date pickupDate;
	@Temporal(TemporalType.DATE)
	public Date billDate;
	public String dob;
	public String doa;

	public String getRepeat_revisit() {
		return repeat_revisit;
	}

	public void setRepeat_revisit(String repeat_revisit) {
		this.repeat_revisit = repeat_revisit;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public String getMi() {
		return mi;
	}

	public void setMi(String mi) {
		this.mi = mi;
	}

	public String getGroupData() {
		return groupData;
	}

	public void setGroupData(String groupData) {
		this.groupData = groupData;
	}

	public String getCircularNo() {
		return circularNo;
	}

	public void setCircularNo(String circularNo) {
		this.circularNo = circularNo;
	}

	public String getMileage() {
		return mileage;
	}

	public void setMileage(String mileage) {
		this.mileage = mileage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getPickuprequired() {
		return pickuprequired;
	}

	public void setPickuprequired(String pickuprequired) {
		this.pickuprequired = pickuprequired;
	}

	public String getPickupLocation() {
		return pickupLocation;
	}

	public void setPickupLocation(String pickupLocation) {
		this.pickupLocation = pickupLocation;
	}

	public String getAddressline1() {
		return addressline1;
	}

	public void setAddressline1(String addressline1) {
		this.addressline1 = addressline1;
	}

	public String getAddressline2() {
		return addressline2;
	}

	public void setAddressline2(String addressline2) {
		this.addressline2 = addressline2;
	}

	public String getAddressline3() {
		return addressline3;
	}

	public void setAddressline3(String addressline3) {
		this.addressline3 = addressline3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public long getPincode() {
		return pincode;
	}

	public void setPincode(long pincode) {
		this.pincode = pincode;
	}

	public String getEngineNo() {
		return engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public long getYear() {
		return year;
	}

	public void setYear(long year) {
		this.year = year;
	}

	public Date getJobCardDate() {
		return jobCardDate;
	}

	public void setJobCardDate(Date jobCardDate) {
		this.jobCardDate = jobCardDate;
	}

	public Date getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getDoa() {
		return doa;
	}

	public void setDoa(String doa) {
		this.doa = doa;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerCat() {
		return customerCat;
	}

	public void setCustomerCat(String customerCat) {
		this.customerCat = customerCat;
	}

	public String getPsfStatus() {
		return psfStatus;
	}

	public void setPsfStatus(String psfStatus) {
		this.psfStatus = psfStatus;
	}

	public String getVehicleRegnumber() {
		return vehicleRegnumber;
	}

	public void setVehicleRegnumber(String vehicleRegnumber) {
		this.vehicleRegnumber = vehicleRegnumber;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getServiceAdvisor() {
		return serviceAdvisor;
	}

	public void setServiceAdvisor(String serviceAdvisor) {
		this.serviceAdvisor = serviceAdvisor;
	}

	public String getTechnician() {
		return technician;
	}

	public void setTechnician(String technician) {
		this.technician = technician;
	}

	public String getServicetype() {
		return servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}

	public Date getPromiseDate() {
		return promiseDate;
	}

	public void setPromiseDate(Date promiseDate) {
		this.promiseDate = promiseDate;
	}

	public Date getRevisedPromiseDate() {
		return revisedPromiseDate;
	}

	public void setRevisedPromiseDate(Date revisedPromiseDate) {
		this.revisedPromiseDate = revisedPromiseDate;
	}

	public Date getReadyDate() {
		return readyDate;
	}

	public void setReadyDate(Date readyDate) {
		this.readyDate = readyDate;
	}

	public long getId() {
		return id;
	}

	public double getEstLabAmt() {
		return estLabAmt;
	}

	public void setEstLabAmt(double estLabAmt) {
		this.estLabAmt = estLabAmt;
	}

	public double getEstPartAmt() {
		return estPartAmt;
	}

	public void setEstPartAmt(double estPartAmt) {
		this.estPartAmt = estPartAmt;
	}

	public double getLabAmt() {
		return labAmt;
	}

	public void setLabAmt(double labAmt) {
		this.labAmt = labAmt;
	}

	public double getPartsAmt() {
		return partsAmt;
	}

	public void setPartsAmt(double partsAmt) {
		this.partsAmt = partsAmt;
	}

	public double getBillAmt() {
		return billAmt;
	}

	public void setBillAmt(double billAmt) {
		this.billAmt = billAmt;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getJobCardNumber() {
		return jobCardNumber;
	}

	public void setJobCardNumber(String jobCardNumber) {
		this.jobCardNumber = jobCardNumber;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

}
