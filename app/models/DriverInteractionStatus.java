package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class DriverInteractionStatus {

	public static int STATUS_STARTED = 1;
	public static int STATUS_REACHED_POINT = 2;
	public static int STATUS_PICKEDUP = 3;
	public static int STATUS_DROPPED = 4;
	public static int STATUS_CANCELLED = 5;

	public static String STATUS_1 = "Started";

	public static String STATUS_2 = "Picked Up";

	public static String STATUS_3 = "Dropped";

	public static String STATUS_4 = "Cancelled";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	public int interactionKey;

	@Column(length = 30)
	public String interactionStatus;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getInteractionKey() {
		return interactionKey;
	}

	public void setInteractionKey(int interactionKey) {
		this.interactionKey = interactionKey;
	}

	public String getInteractionStatus() {
		return interactionStatus;
	}

	public void setInteractionStatus(String interactionStatus) {
		this.interactionStatus = interactionStatus;
	}

}
