package models;

import javax.persistence.*;

@Entity
public class Address {

	public static int ADDRESS_TYPE_OFFICE = 1;
	public static int ADDRESS_TYPE_RESIDENCE = 2;
	public static int ADDRESS_TYPE_OTHERS = 3;
	public static int ADDRESS_TYPE_PICKUP = 4;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	@Column(length = 5000)
	public String addressLine1;
	@Column(length = 5000)
	public String addressLine2;
	@Column(length = 5000)
	public String addressLine3;
	@Column(length = 10)
	public long pincode;
	@Column(length = 30)
	public String city;
	@Column(length = 30)
	public String state;
	@Column(length = 30)
	public String country;
	@Column(length = 100)
	public String district;

	@Column(length = 100)
	public String tehsil;

	public int addressType;

	@Column(length = 5000)
	public String concatenatedAdress;

	public boolean isPreferred;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_Id", nullable = true)
	public Customer customer;

	public String upload_id;

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public long getPincode() {
		return pincode;
	}

	public void setPincode(long pincode) {
		this.pincode = pincode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getAddressType() {
		return addressType;
	}

	public void setAddressType(int addressType) {
		this.addressType = addressType;
	}

	 public String getConcatenatedAddressWith() {
	 StringBuffer buff = new StringBuffer();
	 buff.append(addressLine1).append("\n");
	 buff.append(addressLine2).append("\n");
	 buff.append(addressLine3).append("\n");
	 buff.append(city).append("\n");
	 buff.append(state).append("\n");
	 buff.append(pincode).append("\n");
	 return buff.toString();
	 }
	
	// @Override
	// public String toString(){
	// return getConcatenatedAdress();
	// }

	public void setConcatenatedAdress(String concatenatedAdress) {
		this.concatenatedAdress = concatenatedAdress;
	}

	public boolean isPreferred() {
		return isPreferred;
	}

	public void setPreferred(boolean isPreferred) {
		this.isPreferred = isPreferred;
	}

	public String getConcatenatedAdress() {
		return concatenatedAdress;
	}

	public boolean isIsPreferred() {
		return isPreferred;
	}

	public void setIsPreferred(boolean isPreferred) {
		this.isPreferred = isPreferred;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getTehsil() {
		return tehsil;
	}

	public void setTehsil(String tehsil) {
		this.tehsil = tehsil;
	}
	
	

}
