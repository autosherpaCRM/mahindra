package models;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class ServiceBooked {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long serviceBookedId;	
	public boolean isPickupRequired;
	public String typeOfPickup;
	public String statusOfSB;
	
	public String serviceBookedType;
	
	public String serviceBookingAddress;


	@ManyToOne
	public Customer customer;
	
	@ManyToOne
	public Vehicle vehicle;

	@OneToOne
	public PickupDrop pickupDrop;

	@ManyToOne
	public Workshop workshop;

	@ManyToOne
	public ServiceAdvisor serviceAdvisor;
	
	@ManyToOne
	public Driver driver;
	
	@ManyToOne
	public ServiceTypes serviceBookType;
	
	
	@ManyToOne
	public SpecialOfferMaster specialOfferMaster;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "serviceBooked")
	public List<CallInteraction>  callInteraction;
	
	
	@OneToOne
	public CallDispositionData serviceBookStatus;
	
	
	@ManyToOne
	public WyzUser wyzUser;
	
	
	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	public CallDispositionData getServiceBookStatus() {
		return serviceBookStatus;
	}

	public void setServiceBookStatus(CallDispositionData serviceBookStatus) {
		this.serviceBookStatus = serviceBookStatus;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public java.util.Date scheduledDateTime;
	
	
	public String getServiceScheduledDateStr(){
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringDate = formatter.format(scheduledDateTime);
		return stringDate;
	}
	
	public String getServiceScheduledDateStrSqlFor(){
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringDate = formatter.format(scheduledDateTime);
		return stringDate;
	}
	
	public String getServiceScheduledTimeStr(){
		String pattern = "HH:mm:ss";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringTime = formatter.format(scheduledDateTime);
		return stringTime;

	}
	
	public String getServiceScheduledTimeStrTime(){
		String pattern = "HH:mm";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringTime = formatter.format(scheduledDateTime);
		return stringTime;

	}
	
	public String getServiceScheduledTimeStrSqlFor(){
		String pattern = "HH:mm";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringTime = formatter.format(scheduledDateTime);
		return stringTime;

	}
	
	
	public String getTypeOfPickup() {
		return typeOfPickup;
	}

	public void setTypeOfPickup(String typeOfPickup) {
		this.typeOfPickup = typeOfPickup;
	}

	public String getStatusOfSB() {
		return statusOfSB;
	}

	public void setStatusOfSB(String statusOfSB) {
		this.statusOfSB = statusOfSB;
	}

	public long getServiceBookedId() {
		return serviceBookedId;
	}

	public void setServiceBookedId(long serviceBookedId) {
		this.serviceBookedId = serviceBookedId;
	}

/*	public String getLastServiceDate() {
		return lastServiceDate;
	}

	public void setLastServiceDate(String lastServiceDate) {
		this.lastServiceDate = lastServiceDate;
	}

	public long getNumberOfServices() {
		return numberOfServices;
	}

	public void setNumberOfServices(long numberOfServices) {
		this.numberOfServices = numberOfServices;
	}

	public String getServiceScheduledDate() {
		return serviceScheduledDate;
	}

	public void setServiceScheduledDate(String serviceScheduledDate) {
		this.serviceScheduledDate = serviceScheduledDate;
	}

	public String getServiceScheduledTime() {
		return serviceScheduledTime;
	}

	public void setServiceScheduledTime(String serviceScheduledTime) {
		this.serviceScheduledTime = serviceScheduledTime;
	}

	public String getServiceBookedAddress() {
		return serviceBookedAddress;
	}

	public void setServiceBookedAddress(String serviceBookedAddress) {
		this.serviceBookedAddress = serviceBookedAddress;
	}

	public String getServiceLocation() {
		return serviceLocation;
	}

	public void setServiceLocation(String serviceLocation) {
		this.serviceLocation = serviceLocation;
	}

	public String getServiceAdvisor() {
		return serviceAdvisor;
	}

	public void setServiceAdvisor(String serviceAdvisor) {
		this.serviceAdvisor = serviceAdvisor;
	}
*/
	public boolean isIsPickupRequired() {
		return isPickupRequired;
	}

	public void setIsPickupRequired(boolean isPickupRequired) {
		this.isPickupRequired = isPickupRequired;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public PickupDrop getPickupDrop() {
		return pickupDrop;
	}

	public void setPickupDrop(PickupDrop pickupDrop) {
		this.pickupDrop = pickupDrop;
	}

	

	public List<CallInteraction> getCallInteraction() {
		return callInteraction;
	}

	public void setCallInteraction(List<CallInteraction> callInteraction) {
		this.callInteraction = callInteraction;
	}

	public Workshop getWorkshop() {
		return workshop;
	}

	public void setWorkshop(Workshop workshop) {
		this.workshop = workshop;
	}

	public ServiceAdvisor getServiceAdvisor() {
		return serviceAdvisor;
	}

	public void setServiceAdvisor(ServiceAdvisor serviceadvisor) {
		this.serviceAdvisor = serviceadvisor;
	}

	public boolean isPickupRequired() {
		return isPickupRequired;
	}

	public void setPickupRequired(boolean isPickupRequired) {
		this.isPickupRequired = isPickupRequired;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public java.util.Date getScheduledDateTime() {
		return scheduledDateTime;
	}

	public void setScheduledDateTime(java.util.Date scheduledDateTime) {
		this.scheduledDateTime = scheduledDateTime;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public String getServiceBookedType() {
		return serviceBookedType;
	}

	public void setServiceBookedType(String serviceBookedType) {
		this.serviceBookedType = serviceBookedType;
	}

	public SpecialOfferMaster getSpecialOfferMaster() {
		return specialOfferMaster;
	}

	public void setSpecialOfferMaster(SpecialOfferMaster specialOfferMaster) {
		this.specialOfferMaster = specialOfferMaster;
	}

	public String getServiceBookingAddress() {
		return serviceBookingAddress;
	}

	public void setServiceBookingAddress(String serviceBookingAddress) {
		this.serviceBookingAddress = serviceBookingAddress;
	}

	public ServiceTypes getServiceBookType() {
		return serviceBookType;
	}

	public void setServiceBookType(ServiceTypes serviceBookType) {
		this.serviceBookType = serviceBookType;
	}

}
