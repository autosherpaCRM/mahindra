package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CampaignExcelError {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	public String dataUploadLocation;
	public String source;
	public String veh_reg_no;
	public String customerName;
	public String vehicle_types;
	public String varaintDesc;
	public String colorDesc;
	public String chassis;
	public String engine;
	public String salutation;
	public String address1;
	public String address2;
	public String address3;
	public String city;
	public String pincode;
	public String mobile1;
	public String mobile2;
	public String std_code;
	public String land_line;
	public String email_id;
	public String startDate;	
	public String endDate;
	
	public int rowNumber;
	public boolean isError;

	@Column(length = 700)
	public String errorInformation;
	
	public String upload_id;
	
	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDataUploadLocation() {
		return dataUploadLocation;
	}
	public void setDataUploadLocation(String dataUploadLocation) {
		this.dataUploadLocation = dataUploadLocation;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getVeh_reg_no() {
		return veh_reg_no;
	}
	public void setVeh_reg_no(String veh_reg_no) {
		this.veh_reg_no = veh_reg_no;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getVehicle_types() {
		return vehicle_types;
	}
	public void setVehicle_types(String vehicle_types) {
		this.vehicle_types = vehicle_types;
	}
	public String getVaraintDesc() {
		return varaintDesc;
	}
	public void setVaraintDesc(String varaintDesc) {
		this.varaintDesc = varaintDesc;
	}
	public String getColorDesc() {
		return colorDesc;
	}
	public void setColorDesc(String colorDesc) {
		this.colorDesc = colorDesc;
	}
	public String getChassis() {
		return chassis;
	}
	public void setChassis(String chassis) {
		this.chassis = chassis;
	}
	public String getEngine() {
		return engine;
	}
	public void setEngine(String engine) {
		this.engine = engine;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getMobile1() {
		return mobile1;
	}
	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}
	public String getMobile2() {
		return mobile2;
	}
	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}
	public String getStd_code() {
		return std_code;
	}
	public void setStd_code(String std_code) {
		this.std_code = std_code;
	}
	public String getLand_line() {
		return land_line;
	}
	public void setLand_line(String land_line) {
		this.land_line = land_line;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}
	public boolean isError() {
		return isError;
	}
	public void setError(boolean isError) {
		this.isError = isError;
	}
	public String getErrorInformation() {
		return errorInformation;
	}
	public void setErrorInformation(String errorInformation) {
		this.errorInformation = errorInformation;
	}
	
	
}
