package models;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class InsuranceDisposition {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@Column(length = 30)
	public String InOutCallName;

	@Column(length = 30)
	public String typeOfDisposition;

	@OneToOne
	public CallDispositionData callDispositionData;

	@Temporal(TemporalType.DATE)
	public Date followUpDate;

	@Column(length = 30)
	public String followUpTime;

	@Column(length = 30)
	public String isFollowupRequired;

	@Column(length = 30)
	public String isFollowUpDone;

	@Column(length = 70)
	public String renewalNotRequiredReason;

	@Column(length = 70)
	public String renewalDoneBy;

	@Temporal(TemporalType.DATE)
	public Date lastRenewalDate;

	@Column(length = 70)
	public String lastRenewedLocation;

	@Column(length = 200)
	public String insuranceProvidedBy;

	@Column(length = 70)
	public String premimum;

	@Column(length = 70)
	public String coverNoteNo;

	@Column(length = 70)
	public String typeOfAutherization;

	@Column(length = 70)
	public String dealerName;

	@Temporal(TemporalType.DATE)
	public Date dateOfRenewal;

	@Column(length = 70)
	public String typeOfInsurance;

	@Column(length = 70)
	public String serviceAdvisorID;

	public long assignedToSA;

	@Temporal(TemporalType.DATE)
	public Date lastServiceDate;

	@Column(length = 70)
	public String DistancefromDealerLocationRadio;

	@Column(length = 70)
	public String transferedCity;

	@Column(length = 70)
	public String pinCodeOfCity;

	@Column(length = 100)
	public String departmentForFB;
	@Column(length = 100)
	public String complaintOrFB_TagTo;
	@Column(length = 250)
	public String remarksOfFB;

	@Column(length = 70)
	public String noServiceReasonTaggedTo;

	@Column(length = 70)
	public String noServiceReasonTaggedToComments;

	@Column(length = 200)
	public String reason;

	@Column(length = 70)
	public String cityName;

	@Column(length = 30)
	public String optforMMS;

	public long upsellCount;

	@Column(length = 100)
	public String inBoundLeadSource;

	@Column(length = 600)
	public String addOnsPrefered_PopularOptionsData;

	@Column(length = 600)
	public String addOnsPrefered_OtherOptionsData;

	@Column(length = 300)
	public String other;

	@Column(length = 300)
	public String comments;
	
	
	@Column(length = 150)
	public String paymentType;
	
	@Column(length = 100)
	public String paymentReference;
	

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "callInteraction_id", nullable = true)
	public CallInteraction callInteraction;

	@OneToMany(mappedBy = "insuranceDisposition")
	public List<UpsellLead> upsellLead;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getInOutCallName() {
		return InOutCallName;
	}

	public void setInOutCallName(String inOutCallName) {
		InOutCallName = inOutCallName;
	}

	public String getTypeOfDisposition() {
		return typeOfDisposition;
	}

	public void setTypeOfDisposition(String typeOfDisposition) {
		this.typeOfDisposition = typeOfDisposition;
	}

	public CallDispositionData getCallDispositionData() {
		return callDispositionData;
	}

	public void setCallDispositionData(CallDispositionData callDispositionData) {
		this.callDispositionData = callDispositionData;
	}

	public Date getFollowUpDate() {
		return followUpDate;
	}

	public void setFollowUpDate(Date followUpDate) {
		this.followUpDate = followUpDate;
	}

	public String getFollowUpTime() {
		return followUpTime;
	}

	public void setFollowUpTime(String followUpTime) {
		this.followUpTime = followUpTime;
	}

	public String getIsFollowupRequired() {
		return isFollowupRequired;
	}

	public void setIsFollowupRequired(String isFollowupRequired) {
		this.isFollowupRequired = isFollowupRequired;
	}

	public String getIsFollowUpDone() {
		return isFollowUpDone;
	}

	public void setIsFollowUpDone(String isFollowUpDone) {
		this.isFollowUpDone = isFollowUpDone;
	}

	public String getRenewalNotRequiredReason() {
		return renewalNotRequiredReason;
	}

	public void setRenewalNotRequiredReason(String renewalNotRequiredReason) {
		this.renewalNotRequiredReason = renewalNotRequiredReason;
	}

	public String getRenewalDoneBy() {
		return renewalDoneBy;
	}

	public void setRenewalDoneBy(String renewalDoneBy) {
		this.renewalDoneBy = renewalDoneBy;
	}

	public Date getLastRenewalDate() {
		return lastRenewalDate;
	}

	public void setLastRenewalDate(Date lastRenewalDate) {
		this.lastRenewalDate = lastRenewalDate;
	}

	public String getLastRenewedLocation() {
		return lastRenewedLocation;
	}

	public void setLastRenewedLocation(String lastRenewedLocation) {
		this.lastRenewedLocation = lastRenewedLocation;
	}

	public String getInsuranceProvidedBy() {
		return insuranceProvidedBy;
	}

	public void setInsuranceProvidedBy(String insuranceProvidedBy) {
		this.insuranceProvidedBy = insuranceProvidedBy;
	}

	public String getPremimum() {
		return premimum;
	}

	public void setPremimum(String premimum) {
		this.premimum = premimum;
	}

	public String getCoverNoteNo() {
		return coverNoteNo;
	}

	public void setCoverNoteNo(String coverNoteNo) {
		this.coverNoteNo = coverNoteNo;
	}

	public String getTypeOfAutherization() {
		return typeOfAutherization;
	}

	public void setTypeOfAutherization(String typeOfAutherization) {
		this.typeOfAutherization = typeOfAutherization;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public Date getDateOfRenewal() {
		return dateOfRenewal;
	}

	public void setDateOfRenewal(Date dateOfRenewal) {
		this.dateOfRenewal = dateOfRenewal;
	}

	public String getTypeOfInsurance() {
		return typeOfInsurance;
	}

	public void setTypeOfInsurance(String typeOfInsurance) {
		this.typeOfInsurance = typeOfInsurance;
	}

	public String getServiceAdvisorID() {
		return serviceAdvisorID;
	}

	public void setServiceAdvisorID(String serviceAdvisorID) {
		this.serviceAdvisorID = serviceAdvisorID;
	}

	public long getAssignedToSA() {
		return assignedToSA;
	}

	public void setAssignedToSA(long assignedToSA) {
		this.assignedToSA = assignedToSA;
	}

	public Date getLastServiceDate() {
		return lastServiceDate;
	}

	public void setLastServiceDate(Date lastServiceDate) {
		this.lastServiceDate = lastServiceDate;
	}

	public String getDistancefromDealerLocationRadio() {
		return DistancefromDealerLocationRadio;
	}

	public void setDistancefromDealerLocationRadio(String distancefromDealerLocationRadio) {
		DistancefromDealerLocationRadio = distancefromDealerLocationRadio;
	}

	public String getTransferedCity() {
		return transferedCity;
	}

	public void setTransferedCity(String transferedCity) {
		this.transferedCity = transferedCity;
	}

	public String getPinCodeOfCity() {
		return pinCodeOfCity;
	}

	public void setPinCodeOfCity(String pinCodeOfCity) {
		this.pinCodeOfCity = pinCodeOfCity;
	}

	public String getDepartmentForFB() {
		return departmentForFB;
	}

	public void setDepartmentForFB(String departmentForFB) {
		this.departmentForFB = departmentForFB;
	}

	public String getComplaintOrFB_TagTo() {
		return complaintOrFB_TagTo;
	}

	public void setComplaintOrFB_TagTo(String complaintOrFB_TagTo) {
		this.complaintOrFB_TagTo = complaintOrFB_TagTo;
	}

	public String getRemarksOfFB() {
		return remarksOfFB;
	}

	public void setRemarksOfFB(String remarksOfFB) {
		this.remarksOfFB = remarksOfFB;
	}

	public String getNoServiceReasonTaggedTo() {
		return noServiceReasonTaggedTo;
	}

	public void setNoServiceReasonTaggedTo(String noServiceReasonTaggedTo) {
		this.noServiceReasonTaggedTo = noServiceReasonTaggedTo;
	}

	public String getNoServiceReasonTaggedToComments() {
		return noServiceReasonTaggedToComments;
	}

	public void setNoServiceReasonTaggedToComments(String noServiceReasonTaggedToComments) {
		this.noServiceReasonTaggedToComments = noServiceReasonTaggedToComments;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public long getUpsellCount() {
		return upsellCount;
	}

	public void setUpsellCount(long upsellCount) {
		this.upsellCount = upsellCount;
	}

	public String getInBoundLeadSource() {
		return inBoundLeadSource;
	}

	public void setInBoundLeadSource(String inBoundLeadSource) {
		this.inBoundLeadSource = inBoundLeadSource;
	}

	public CallInteraction getCallInteraction() {
		return callInteraction;
	}

	public void setCallInteraction(CallInteraction callInteraction) {
		this.callInteraction = callInteraction;
	}

	public String getOptforMMS() {
		return optforMMS;
	}

	public void setOptforMMS(String optforMMS) {
		this.optforMMS = optforMMS;
	}

	public List<UpsellLead> getUpsellLead() {
		return upsellLead;
	}

	public void setUpsellLead(List<UpsellLead> upsellLead) {
		this.upsellLead = upsellLead;
	}

	public String getAddOnsPrefered_PopularOptionsData() {
		return addOnsPrefered_PopularOptionsData;
	}

	public void setAddOnsPrefered_PopularOptionsData(String addOnsPrefered_PopularOptionsData) {
		this.addOnsPrefered_PopularOptionsData = addOnsPrefered_PopularOptionsData;
	}

	public String getAddOnsPrefered_OtherOptionsData() {
		return addOnsPrefered_OtherOptionsData;
	}

	public void setAddOnsPrefered_OtherOptionsData(String addOnsPrefered_OtherOptionsData) {
		this.addOnsPrefered_OtherOptionsData = addOnsPrefered_OtherOptionsData;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentReference() {
		return paymentReference;
	}

	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}
	
	
}
