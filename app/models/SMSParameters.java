/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 *
 * @author wct-09
 */

@Entity
public class SMSParameters {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    public String phone;
    public String message;
    public String senderid;
    public String sucessStatus;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getSenderid() {
		return senderid;
	}
	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	public String getSucessStatus() {
		return sucessStatus;
	}
	public void setSucessStatus(String sucessStatus) {
		this.sucessStatus = sucessStatus;
	}
    
    


}
