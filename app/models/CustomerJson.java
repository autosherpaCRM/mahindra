/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author w-948
 */
public class CustomerJson {
	public String customerNameEdit;
	public String driverNameEdit;
	public String dob;
	public String anniversary_date;
	public long customerId;
	public long wyzuser_id;
	public String preferred_mobile_no;
	public String mode_of_contact;
	public String preferred_day;
	public String preferred_time_start;
	public String preferred_time_end;
	public String permanentAddress;
	public String officeAddress;
	public String residenceAddress;
	public String preferredcomAddress;
	public long preffed_flag;
	public String email;
	public String customerComments;
	public boolean dnd_flag;
	
	

	public boolean isDnd_flag() {
		return dnd_flag;
	}

	public void setDnd_flag(boolean dnd_flag) {
		this.dnd_flag = dnd_flag;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCustomerNameEdit() {
		return customerNameEdit;
	}

	public void setCustomerNameEdit(String customerNameEdit) {
		this.customerNameEdit = customerNameEdit;
	}

	public String getDriverNameEdit() {
		return driverNameEdit;
	}

	public void setDriverNameEdit(String driverNameEdit) {
		this.driverNameEdit = driverNameEdit;
	}

	public long getPreffed_flag() {
		return preffed_flag;
	}

	public void setPreffed_flag(long preffed_flag) {
		this.preffed_flag = preffed_flag;
	}

	public String getPreferredcomAddress() {
		return preferredcomAddress;
	}

	public void setPreferredcomAddress(String preferredcomAddress) {
		this.preferredcomAddress = preferredcomAddress;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAnniversary_date() {
		return anniversary_date;
	}

	public void setAnniversary_date(String anniversary_date) {
		this.anniversary_date = anniversary_date;
	}

	public String getPreferred_mobile_no() {
		return preferred_mobile_no;
	}

	public void setPreferred_mobile_no(String preferred_mobile_no) {
		this.preferred_mobile_no = preferred_mobile_no;
	}

	public String getMode_of_contact() {
		return mode_of_contact;
	}

	public void setMode_of_contact(String mode_of_contact) {
		this.mode_of_contact = mode_of_contact;
	}

	public String getPreferred_day() {
		return preferred_day;
	}

	public void setPreferred_day(String preferred_day) {
		this.preferred_day = preferred_day;
	}

	public String getPreferred_time_start() {
		return preferred_time_start;
	}

	public void setPreferred_time_start(String preferred_time_start) {
		this.preferred_time_start = preferred_time_start;
	}

	public String getPreferred_time_end() {
		return preferred_time_end;
	}

	public void setPreferred_time_end(String preferred_time_end) {
		this.preferred_time_end = preferred_time_end;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}

	public String getResidenceAddress() {
		return residenceAddress;
	}

	public void setResidenceAddress(String residenceAddress) {
		this.residenceAddress = residenceAddress;
	}

	public long getWyzuser_id() {
		return wyzuser_id;
	}

	public void setWyzuser_id(long wyzuser_id) {
		this.wyzuser_id = wyzuser_id;
	}

	public String getCustomerComments() {
		return customerComments;
	}

	public void setCustomerComments(String customerComments) {
		this.customerComments = customerComments;
	}
	
	

}
