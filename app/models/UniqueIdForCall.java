package models;
import javax.persistence.*;



@Entity
public class UniqueIdForCall {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;
	
	public int callinitiating_id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getCallinitiating_id() {
		return callinitiating_id;
	}

	public void setCallinitiating_id(int callinitiating_id) {
		this.callinitiating_id = callinitiating_id;
	}

	
	
}
