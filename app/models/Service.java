/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author W-885
 */
@Component
@Scope("prototype")
@Entity
public class Service {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	@Temporal(TemporalType.DATE)
	public java.util.Date nextServiceDue;
	@Column(length = 30)
	public String nextServiceType;

	public int serviceNoShowPeriod;

	public double partBasic;
	public double partDiscount;
	public double partCharges;
	public double labourBasic;
	public double labourDiscount;
	public double labourCharges;
	
	public double roundOffamt;
	public double billAmt;
	
	public String uploadDataFiletype;

	@Temporal(TemporalType.DATE)
	public Date psf1;

	@Temporal(TemporalType.DATE)
	public Date psf2;
	@Temporal(TemporalType.DATE)
	public Date psf3;

	@Temporal(TemporalType.DATE)
	public Date psf4;

	public String getUploadDataFiletype() {
		return uploadDataFiletype;
	}

	public void setUploadDataFiletype(String uploadDataFiletype) {
		this.uploadDataFiletype = uploadDataFiletype;
	}

	@Column(length = 30)
	public String lastServiceMeterReading;
	@Column(length = 30)
	public String lastServiceType;
	@Column(length = 30)
	public String messageSentStatus;
	@Column(length = 30)
	public String lastPSFStatus;
	@Column(length = 30)
	public String lastVisitType;
	@Column(length = 30)
	public String lastVisitMileage;
	@Column(length = 30)
	public String visitSuccessRate;

	@Column(length = 30)
	public String serviceType;

	@Column(length = 30)
	public String jobCardNumber;
	@Column(length = 30)
	public String jobCardStatus;

	@Column(length = 30)
	public String isAssigned;
	@Column(length = 30)
	public String groupData;
	@Column(length = 30)
	public String selling;
	@Column(length = 30)
	public String billNumber;
	@Column(length = 30)
	public String mcpNo;
	@Column(length = 30)
	public String technician;
	@Column(length = 30)
	public String createdBy;
	@Column(length = 30)
	public String repeat_revisit;
	@Column(length = 100)
	public String customerName;
	@Column(length = 30)
	public String custCat;
	@Column(length = 30)
	public String psfStatus;
	@Column(length = 30)
	public String circularNo;
	public String serviceOdometerReading;
	public double estLabAmt;
	public double estPartAmt;
	public String status;
	public String billNo;
	public double labAmt;
	public double partsAmt;
	public Date jobCardDate;

	@Temporal(TemporalType.DATE)
	public Date billDate;

	@Temporal(TemporalType.DATE)
	public Date readyDate;

	@Temporal(TemporalType.DATE)
	public Date promiseDate;

	@Temporal(TemporalType.DATE)
	public Date revisedPromiseDate;

	@Temporal(TemporalType.DATE)
	public Date lastServiceDate;

	@Temporal(TemporalType.DATE)
	public Date nextServiceDate;

	@Temporal(TemporalType.DATE)
	public Date lastVisitDate;

	public String lastVisitMeterReading;

	@Temporal(TemporalType.DATE)
	public Date dateOfOdometerReading;

	@Temporal(TemporalType.DATE)
	public Date saleDate;

	@Temporal(TemporalType.DATE)
	public Date createdDate;

	@Temporal(TemporalType.DATE)
	public Date updatedDate;
	
	@Temporal(TemporalType.DATE)
	public Date rsaExpiryDate;
	
	@Temporal(TemporalType.DATE)
	public Date shieldExpiryDate;
	
	@Column(length = 50)
	public String billStatus;
	
	@Column(length = 50)
	public String taxiNonTaxi;
	
	@Column(length = 50)
	public String OEMprivilageCust;
	
	@Column(length = 50)
	public String loyaltyCardNo;
	
	@Column(length = 50)
	public String campaignCode;
	
	@Column(length = 50)
	public String RoSource;
	
	@Column(length = 50)
	public String udayNo;
	
	@Column(length = 50)
	public String rsaSchemeCode;
	
	@Column(length = 50)
	public String shieldschemeCode;
	
	@Column(length = 50)
	public String expressService;
	
	@Column(length = 50)
	public String originalWarrantystartDate;
	
	@Column(length = 50)
	public String purpleCouponNo;
	
	@Column(length = 100)
	public String menuCodeDesc;
	
	@Column(length = 50)
	public String purpleCouponYesNo;
	
	@Column(length = 50)
	public String pickUpType;
	
	@Column(length = 50)
	public String dropType;

	@ManyToOne(cascade = CascadeType.ALL)
	public Vehicle vehicle;

	@ManyToOne(cascade = CascadeType.ALL)
	public ServiceAdvisor serviceAdvisor;

	@ManyToOne(cascade = CascadeType.ALL)
	public Workshop workshop;

	public String upload_id;
	
	public boolean ftps;

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "service")
	public List<PSFAssignedInteraction> psfAssignedInteraction;

	public List<PSFAssignedInteraction> getPsfAssignedInteraction() {
		return psfAssignedInteraction;
	}

	public void setPsfAssignedInteraction(List<PSFAssignedInteraction> psfAssignedInteraction) {
		this.psfAssignedInteraction = psfAssignedInteraction;
	}

	@Temporal(TemporalType.DATE)
	public Date pickupDate;

	public String getLastVisitMeterReading() {
		return lastVisitMeterReading;
	}

	public void setLastVisitMeterReading(String lastVisitMeterReading) {
		this.lastVisitMeterReading = lastVisitMeterReading;
	}

	public Workshop getWorkshop() {
		return workshop;
	}

	public void setWorkshop(Workshop workshop) {
		this.workshop = workshop;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getRepeat_revisit() {
		return repeat_revisit;
	}

	public void setRepeat_revisit(String repeat_revisit) {
		this.repeat_revisit = repeat_revisit;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustCat() {
		return custCat;
	}

	public void setCustCat(String custCat) {
		this.custCat = custCat;
	}

	public String getPsfStatus() {
		return psfStatus;
	}

	public void setPsfStatus(String psfStatus) {
		this.psfStatus = psfStatus;
	}

	public String getCircularNo() {
		return circularNo;
	}

	public void setCircularNo(String circularNo) {
		this.circularNo = circularNo;
	}

	public String getServiceOdometerReading() {
		return serviceOdometerReading;
	}

	public void setServiceOdometerReading(String mileage) {
		this.serviceOdometerReading = mileage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public Date getPromiseDate() {
		return promiseDate;
	}

	public void setPromiseDate(Date promiseDate) {
		this.promiseDate = promiseDate;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getMcpNo() {
		return mcpNo;
	}

	public void setMcpNo(String mcpNo) {
		this.mcpNo = mcpNo;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	public double getPartBasic() {
		return partBasic;
	}

	public void setPartBasic(double partBasic) {
		this.partBasic = partBasic;
	}

	public double getPartDiscount() {
		return partDiscount;
	}

	public void setPartDiscount(double partDiscount) {
		this.partDiscount = partDiscount;
	}

	public double getPartCharges() {
		return partCharges;
	}

	public void setPartCharges(double partCharges) {
		this.partCharges = partCharges;
	}

	public double getLabourBasic() {
		return labourBasic;
	}

	public void setLabourBasic(double labourBasic) {
		this.labourBasic = labourBasic;
	}

	public double getLabourDiscount() {
		return labourDiscount;
	}

	public void setLabourDiscount(double labourDiscount) {
		this.labourDiscount = labourDiscount;
	}

	public double getLabourCharges() {
		return labourCharges;
	}

	public void setLabourCharges(double labourCharges) {
		this.labourCharges = labourCharges;
	}

	public double getRoundOffamt() {
		return roundOffamt;
	}

	public void setRoundOffamt(double roundOffamt) {
		this.roundOffamt = roundOffamt;
	}

	public double getBillAmt() {
		return billAmt;
	}

	public void setBillAmt(double billAmt) {
		this.billAmt = billAmt;
	}

	public Date getRevisedPromiseDate() {
		return revisedPromiseDate;
	}

	public void setRevisedPromiseDate(Date revisedPromiseDate) {
		this.revisedPromiseDate = revisedPromiseDate;
	}

	public Date getReadyDate() {
		return readyDate;
	}

	public void setReadyDate(Date readyDate) {
		this.readyDate = readyDate;
	}

	public String getGroupData() {
		return groupData;
	}

	public void setGroupData(String groupData) {
		this.groupData = groupData;
	}

	public String getSelling() {
		return selling;
	}

	public void setSelling(String selling) {
		this.selling = selling;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getJobCardDate() {
		return jobCardDate;
	}

	public void setJobCardDate(Date jobCardDate) {
		this.jobCardDate = jobCardDate;
	}

	public String getJobCardNumber() {
		return jobCardNumber;
	}

	public void setJobCardNumber(String jobCardNumber) {
		this.jobCardNumber = jobCardNumber;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public ServiceAdvisor getServiceAdvisor() {
		return serviceAdvisor;
	}

	public void setServiceAdvisor(ServiceAdvisor serviceadvisor) {
		this.serviceAdvisor = serviceadvisor;
	}

	public String getMessageSentStatus() {
		return messageSentStatus;
	}

	public void setMessageSentStatus(String messageSentStatus) {
		this.messageSentStatus = messageSentStatus;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public java.util.Date getNextServiceDue() {
		return nextServiceDue;
	}

	public void setNextServiceDue(java.util.Date nextServiceDue) {
		this.nextServiceDue = nextServiceDue;
	}

	public Date getNextServiceDate() {
		return nextServiceDate;
	}

	public void setNextServiceDate(Date nextServiceDate) {
		this.nextServiceDate = nextServiceDate;
	}

	public String getNextServiceType() {
		return nextServiceType;
	}

	public void setNextServiceType(String nextServiceType) {
		this.nextServiceType = nextServiceType;
	}

	public int getServiceNoShowPeriod() {
		return serviceNoShowPeriod;
	}

	public void setServiceNoShowPeriod(int serviceNoShowPeriod) {
		this.serviceNoShowPeriod = serviceNoShowPeriod;
	}

	public Date getLastServiceDate() {
		return lastServiceDate;
	}
	
	public String getLastServiceDateStr() {
		if (lastServiceDate != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate = formatter.format(lastServiceDate);
			return stringDate;
		} else {

			return "";

		}
	}
	public String getNextServicedateStr() {
		if (nextServiceDate != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate = formatter.format(nextServiceDate);
			return stringDate;
		} else {

			return "";

		}
	}
	
	public String getJobCardDateStr() {
		if (jobCardDate != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate = formatter.format(jobCardDate);
			return stringDate;
		} else {

			return "";

		}
	}

	public void setLastServiceDate(Date lastServiceDate) {
		this.lastServiceDate = lastServiceDate;
	}

	public String getLastServiceMeterReading() {
		return lastServiceMeterReading;
	}

	public void setLastServiceMeterReading(String lastServiceMeterReading) {
		this.lastServiceMeterReading = lastServiceMeterReading;
	}

	public Date getDateOfOdometerReading() {
		return dateOfOdometerReading;
	}

	public void setDateOfOdometerReading(Date dateOfOdometerReading) {
		this.dateOfOdometerReading = dateOfOdometerReading;
	}

	public String getJobCardStatus() {
		return jobCardStatus;
	}

	public void setJobCardStatus(String jobCardStatus) {
		this.jobCardStatus = jobCardStatus;
	}

	public String getTechnician() {
		return technician;
	}

	public void setTechnician(String technician) {
		this.technician = technician;
	}

	public String getLastServiceType() {
		return lastServiceType;
	}

	public void setLastServiceType(String lastServiceType) {
		this.lastServiceType = lastServiceType;
	}

	public String getLastPSFStatus() {
		return lastPSFStatus;
	}

	public void setLastPSFStatus(String lastPSFStatus) {
		this.lastPSFStatus = lastPSFStatus;
	}

	public Date getLastVisitDate() {
		return lastVisitDate;
	}

	public void setLastVisitDate(Date lastVisitDate) {
		this.lastVisitDate = lastVisitDate;
	}

	public String getLastVisitType() {
		return lastVisitType;
	}

	public void setLastVisitType(String lastVisitType) {
		this.lastVisitType = lastVisitType;
	}

	public String getLastVisitMileage() {
		return lastVisitMileage;
	}

	public void setLastVisitMileage(String lastVisitMileage) {
		this.lastVisitMileage = lastVisitMileage;
	}

	public String getVisitSuccessRate() {
		return visitSuccessRate;
	}

	public void setVisitSuccessRate(String visitSuccessRate) {
		this.visitSuccessRate = visitSuccessRate;
	}

	public Date getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getIsAssigned() {
		return isAssigned;
	}

	public void setIsAssigned(String isAssigned) {
		this.isAssigned = isAssigned;
	}

	public double getEstLabAmt() {
		return estLabAmt;
	}

	public void setEstLabAmt(double estLabAmt) {
		this.estLabAmt = estLabAmt;
	}

	public double getEstPartAmt() {
		return estPartAmt;
	}

	public void setEstPartAmt(double estPartAmt) {
		this.estPartAmt = estPartAmt;
	}

	public double getLabAmt() {
		return labAmt;
	}

	public void setLabAmt(double labAmt) {
		this.labAmt = labAmt;
	}

	public double getPartsAmt() {
		return partsAmt;
	}

	public void setPartsAmt(double partsAmt) {
		this.partsAmt = partsAmt;
	}

	public Date getPsf1() {
		return psf1;
	}
	
	public String getPsf1Str() {
		if (psf1 != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate = formatter.format(psf1);
			return stringDate;
		} else {

			return "";

		}
	}

	public void setPsf1(Date psf1) {
		this.psf1 = psf1;
	}

	public Date getPsf2() {
		return psf2;
	}
	
	public String getPsf2Str() {
		if (psf2 != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate = formatter.format(psf2);
			return stringDate;
		} else {

			return "";

		}
	}

	public void setPsf2(Date psf2) {
		this.psf2 = psf2;
	}

	public Date getPsf3() {
		return psf3;
	}
	
	
	public String getPsf3Str() {
		if (psf3 != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate = formatter.format(psf3);
			return stringDate;
		} else {

			return "";

		}
	}

	public void setPsf3(Date psf3) {
		this.psf3 = psf3;
	}

	public Date getPsf4() {
		return psf4;
	}
	
	public String getPsf4Str() {
		if (psf4 != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate = formatter.format(psf4);
			return stringDate;
		} else {

			return "";

		}
	}

	public void setPsf4(Date psf4) {
		this.psf4 = psf4;
	}

	public boolean isFtps() {
		return ftps;
	}

	public void setFtps(boolean ftps) {
		this.ftps = ftps;
	}
	
	public double mfgPartsBasic;
	public double mfgPartsDiscount;
	public double mfgPartsTax;
	public double mfgPartsTotal;
	public double localPartsBasic;
	public double localPartsDiscount;
	public double localPartsTax;
	public double localPartsTotal;
	public double oilConsumablesBasic;
	public double oilConsumablesDiscount;
	public double oilConsumablesTax;
	public double oilConsumablesTotal;
	public double maxiCareBasic;
	public double maxiCareDiscount;
	public double maxiCareTax;
	public double maxiCareTotal;
	public double mfgAccessoriesBasic;
	public double mfgAccessoriesDiscount;
	public double mfgAccessoriesTax;
	public double mfgAccessoriesTotal;
	public double localAccessoriesBasic;
	public double localAccessoriesDiscount;
	public double localAccessoriesTax;
	public double localAccessoriesTotal;
	public double maximileBasic;
	public double maximileDiscount;
	public double maximileTax;
	public double maximileTotal;
	public double inhouseLabourBasic;
	public double inhouseLabourDiscount;
	public double inhouseLabourTax;
	public double inhouseLabourTotal;
	public double outLabourBasic;
	public double outLabourDiscount;
	public double outLabourTax;
	public double outLabourTotal;
	
	@Column(length = 100)
	public String serviceCategory;
	
	@Column(length = 100)
	public String billType;
	
	@Column(length = 100)
	public String jobcardLocation;
	
	@Column(length = 100)
	public String saName;
	
	@Column(length = 100)
	public String suprevisor;	
	
	@Column(length = 100)
	public String billCreatedBy;
	
	

	public Service() {
		super();
		this.mfgPartsBasic = (double)0;
		this.mfgPartsDiscount = (double)0;
		this.mfgPartsTax = (double)0;
		this.mfgPartsTotal = (double)0;
		this.localPartsBasic = (double)0;
		this.localPartsDiscount = (double)0;
		this.localPartsTax = (double)0;
		this.localPartsTotal = (double)0;
		this.oilConsumablesBasic = (double)0;
		this.oilConsumablesDiscount = (double)0;
		this.oilConsumablesTax = (double)0;
		this.oilConsumablesTotal = (double)0;
		this.maxiCareBasic = (double)0;
		this.maxiCareDiscount = (double)0;
		this.maxiCareTax = (double)0;
		this.maxiCareTotal = (double)0;
		this.mfgAccessoriesBasic = (double)0;
		this.mfgAccessoriesDiscount = (double)0;
		this.mfgAccessoriesTax = (double)0;
		this.mfgAccessoriesTotal = (double)0;
		this.localAccessoriesBasic = (double)0;
		this.localAccessoriesDiscount = (double)0;
		this.localAccessoriesTax = (double)0;
		this.localAccessoriesTotal = (double)0;
		this.maximileBasic = (double)0;
		this.maximileDiscount = (double)0;
		this.maximileTax = (double)0;
		this.maximileTotal = (double)0;
		this.inhouseLabourBasic = (double)0;
		this.inhouseLabourDiscount = (double)0;
		this.inhouseLabourTax = (double)0;
		this.inhouseLabourTotal = (double)0;
		this.outLabourBasic = (double)0;
		this.outLabourDiscount = (double)0;
		this.outLabourTax = (double)0;
		this.outLabourTotal = (double)0;
	}

	public double getMfgPartsBasic() {
		return mfgPartsBasic;
	}

	public void setMfgPartsBasic(double mfgPartsBasic) {
		this.mfgPartsBasic = mfgPartsBasic;
	}

	public double getMfgPartsDiscount() {
		return mfgPartsDiscount;
	}

	public void setMfgPartsDiscount(double mfgPartsDiscount) {
		this.mfgPartsDiscount = mfgPartsDiscount;
	}

	public double getMfgPartsTax() {
		return mfgPartsTax;
	}

	public void setMfgPartsTax(double mfgPartsTax) {
		this.mfgPartsTax = mfgPartsTax;
	}

	public double getMfgPartsTotal() {
		return mfgPartsTotal;
	}

	public void setMfgPartsTotal(double mfgPartsTotal) {
		this.mfgPartsTotal = mfgPartsTotal;
	}

	public double getLocalPartsBasic() {
		return localPartsBasic;
	}

	public void setLocalPartsBasic(double localPartsBasic) {
		this.localPartsBasic = localPartsBasic;
	}

	public double getLocalPartsDiscount() {
		return localPartsDiscount;
	}

	public void setLocalPartsDiscount(double localPartsDiscount) {
		this.localPartsDiscount = localPartsDiscount;
	}

	public double getLocalPartsTax() {
		return localPartsTax;
	}

	public void setLocalPartsTax(double localPartsTax) {
		this.localPartsTax = localPartsTax;
	}

	public double getLocalPartsTotal() {
		return localPartsTotal;
	}

	public void setLocalPartsTotal(double localPartsTotal) {
		this.localPartsTotal = localPartsTotal;
	}

	public double getOilConsumablesBasic() {
		return oilConsumablesBasic;
	}

	public void setOilConsumablesBasic(double oilConsumablesBasic) {
		this.oilConsumablesBasic = oilConsumablesBasic;
	}

	public double getOilConsumablesDiscount() {
		return oilConsumablesDiscount;
	}

	public void setOilConsumablesDiscount(double oilConsumablesDiscount) {
		this.oilConsumablesDiscount = oilConsumablesDiscount;
	}

	public double getOilConsumablesTax() {
		return oilConsumablesTax;
	}

	public void setOilConsumablesTax(double oilConsumablesTax) {
		this.oilConsumablesTax = oilConsumablesTax;
	}

	public double getOilConsumablesTotal() {
		return oilConsumablesTotal;
	}

	public void setOilConsumablesTotal(double oilConsumablesTotal) {
		this.oilConsumablesTotal = oilConsumablesTotal;
	}

	public double getMaxiCareBasic() {
		return maxiCareBasic;
	}

	public void setMaxiCareBasic(double maxiCareBasic) {
		this.maxiCareBasic = maxiCareBasic;
	}

	public double getMaxiCareDiscount() {
		return maxiCareDiscount;
	}

	public void setMaxiCareDiscount(double maxiCareDiscount) {
		this.maxiCareDiscount = maxiCareDiscount;
	}

	public double getMaxiCareTax() {
		return maxiCareTax;
	}

	public void setMaxiCareTax(double maxiCareTax) {
		this.maxiCareTax = maxiCareTax;
	}

	public double getMaxiCareTotal() {
		return maxiCareTotal;
	}

	public void setMaxiCareTotal(double maxiCareTotal) {
		this.maxiCareTotal = maxiCareTotal;
	}

	public double getMfgAccessoriesBasic() {
		return mfgAccessoriesBasic;
	}

	public void setMfgAccessoriesBasic(double mfgAccessoriesBasic) {
		this.mfgAccessoriesBasic = mfgAccessoriesBasic;
	}

	public double getMfgAccessoriesDiscount() {
		return mfgAccessoriesDiscount;
	}

	public void setMfgAccessoriesDiscount(double mfgAccessoriesDiscount) {
		this.mfgAccessoriesDiscount = mfgAccessoriesDiscount;
	}

	public double getMfgAccessoriesTax() {
		return mfgAccessoriesTax;
	}

	public void setMfgAccessoriesTax(double mfgAccessoriesTax) {
		this.mfgAccessoriesTax = mfgAccessoriesTax;
	}

	public double getMfgAccessoriesTotal() {
		return mfgAccessoriesTotal;
	}

	public void setMfgAccessoriesTotal(double mfgAccessoriesTotal) {
		this.mfgAccessoriesTotal = mfgAccessoriesTotal;
	}

	public double getLocalAccessoriesBasic() {
		return localAccessoriesBasic;
	}

	public void setLocalAccessoriesBasic(double localAccessoriesBasic) {
		this.localAccessoriesBasic = localAccessoriesBasic;
	}

	public double getLocalAccessoriesDiscount() {
		return localAccessoriesDiscount;
	}

	public void setLocalAccessoriesDiscount(double localAccessoriesDiscount) {
		this.localAccessoriesDiscount = localAccessoriesDiscount;
	}

	public double getLocalAccessoriesTax() {
		return localAccessoriesTax;
	}

	public void setLocalAccessoriesTax(double localAccessoriesTax) {
		this.localAccessoriesTax = localAccessoriesTax;
	}

	public double getLocalAccessoriesTotal() {
		return localAccessoriesTotal;
	}

	public void setLocalAccessoriesTotal(double localAccessoriesTotal) {
		this.localAccessoriesTotal = localAccessoriesTotal;
	}

	public double getMaximileBasic() {
		return maximileBasic;
	}

	public void setMaximileBasic(double maximileBasic) {
		this.maximileBasic = maximileBasic;
	}

	public double getMaximileDiscount() {
		return maximileDiscount;
	}

	public void setMaximileDiscount(double maximileDiscount) {
		this.maximileDiscount = maximileDiscount;
	}

	public double getMaximileTax() {
		return maximileTax;
	}

	public void setMaximileTax(double maximileTax) {
		this.maximileTax = maximileTax;
	}

	public double getMaximileTotal() {
		return maximileTotal;
	}

	public void setMaximileTotal(double maximileTotal) {
		this.maximileTotal = maximileTotal;
	}

	public double getInhouseLabourBasic() {
		return inhouseLabourBasic;
	}

	public void setInhouseLabourBasic(double inhouseLabourBasic) {
		this.inhouseLabourBasic = inhouseLabourBasic;
	}

	public double getInhouseLabourDiscount() {
		return inhouseLabourDiscount;
	}

	public void setInhouseLabourDiscount(double inhouseLabourDiscount) {
		this.inhouseLabourDiscount = inhouseLabourDiscount;
	}

	public double getInhouseLabourTax() {
		return inhouseLabourTax;
	}

	public void setInhouseLabourTax(double inhouseLabourTax) {
		this.inhouseLabourTax = inhouseLabourTax;
	}

	public double getInhouseLabourTotal() {
		return inhouseLabourTotal;
	}

	public void setInhouseLabourTotal(double inhouseLabourTotal) {
		this.inhouseLabourTotal = inhouseLabourTotal;
	}

	public double getOutLabourBasic() {
		return outLabourBasic;
	}

	public void setOutLabourBasic(double outLabourBasic) {
		this.outLabourBasic = outLabourBasic;
	}

	public double getOutLabourDiscount() {
		return outLabourDiscount;
	}

	public void setOutLabourDiscount(double outLabourDiscount) {
		this.outLabourDiscount = outLabourDiscount;
	}

	public double getOutLabourTax() {
		return outLabourTax;
	}

	public void setOutLabourTax(double outLabourTax) {
		this.outLabourTax = outLabourTax;
	}

	public double getOutLabourTotal() {
		return outLabourTotal;
	}

	public void setOutLabourTotal(double outLabourTotal) {
		this.outLabourTotal = outLabourTotal;
	}

	public String getServiceCategory() {
		return serviceCategory;
	}

	public void setServiceCategory(String serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public String getJobcardLocation() {
		return jobcardLocation;
	}

	public void setJobcardLocation(String jobcardLocation) {
		this.jobcardLocation = jobcardLocation;
	}

	public String getSaName() {
		return saName;
	}

	public void setSaName(String saName) {
		this.saName = saName;
	}

	public String getSuprevisor() {
		return suprevisor;
	}

	public void setSuprevisor(String suprevisor) {
		this.suprevisor = suprevisor;
	}

	public String getBillCreatedBy() {
		return billCreatedBy;
	}

	public void setBillCreatedBy(String billCreatedBy) {
		this.billCreatedBy = billCreatedBy;
	}

	public Date getRsaExpiryDate() {
		return rsaExpiryDate;
	}
	
	public String getRsaExpiryDateStr() {
		if (rsaExpiryDate != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate = formatter.format(rsaExpiryDate);
			return stringDate;
		} else {

			return "";

		}
	}

	public void setRsaExpiryDate(Date rsaExpiryDate) {
		this.rsaExpiryDate = rsaExpiryDate;
	}

	public Date getShieldExpiryDate() {
		return shieldExpiryDate;
	}
	
	public String getShieldExpiryDateStr() {
		if (shieldExpiryDate != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate = formatter.format(shieldExpiryDate);
			return stringDate;
		} else {

			return "";

		}
	}

	public void setShieldExpiryDate(Date shieldExpiryDate) {
		this.shieldExpiryDate = shieldExpiryDate;
	}

	public String getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

	public String getTaxiNonTaxi() {
		return taxiNonTaxi;
	}

	public void setTaxiNonTaxi(String taxiNonTaxi) {
		this.taxiNonTaxi = taxiNonTaxi;
	}

	public String getOEMprivilageCust() {
		return OEMprivilageCust;
	}

	public void setOEMprivilageCust(String oEMprivilageCust) {
		OEMprivilageCust = oEMprivilageCust;
	}

	public String getLoyaltyCardNo() {
		return loyaltyCardNo;
	}

	public void setLoyaltyCardNo(String loyaltyCardNo) {
		this.loyaltyCardNo = loyaltyCardNo;
	}

	public String getCampaignCode() {
		return campaignCode;
	}

	public void setCampaignCode(String campaignCode) {
		this.campaignCode = campaignCode;
	}

	public String getRoSource() {
		return RoSource;
	}

	public void setRoSource(String roSource) {
		RoSource = roSource;
	}

	public String getUdayNo() {
		return udayNo;
	}

	public void setUdayNo(String udayNo) {
		this.udayNo = udayNo;
	}

	public String getRsaSchemeCode() {
		return rsaSchemeCode;
	}

	public void setRsaSchemeCode(String rsaSchemeCode) {
		this.rsaSchemeCode = rsaSchemeCode;
	}

	public String getShieldschemeCode() {
		return shieldschemeCode;
	}

	public void setShieldschemeCode(String shieldschemeCode) {
		this.shieldschemeCode = shieldschemeCode;
	}

	public String getExpressService() {
		return expressService;
	}

	public void setExpressService(String expressService) {
		this.expressService = expressService;
	}

	public String getOriginalWarrantystartDate() {
		return originalWarrantystartDate;
	}

	public void setOriginalWarrantystartDate(String originalWarrantystartDate) {
		this.originalWarrantystartDate = originalWarrantystartDate;
	}

	public String getPurpleCouponNo() {
		return purpleCouponNo;
	}

	public void setPurpleCouponNo(String purpleCouponNo) {
		this.purpleCouponNo = purpleCouponNo;
	}

	public String getMenuCodeDesc() {
		return menuCodeDesc;
	}

	public void setMenuCodeDesc(String menuCodeDesc) {
		this.menuCodeDesc = menuCodeDesc;
	}

	public String getPurpleCouponYesNo() {
		return purpleCouponYesNo;
	}

	public void setPurpleCouponYesNo(String purpleCouponYesNo) {
		this.purpleCouponYesNo = purpleCouponYesNo;
	}

	public String getPickUpType() {
		return pickUpType;
	}

	public void setPickUpType(String pickUpType) {
		this.pickUpType = pickUpType;
	}

	public String getDropType() {
		return dropType;
	}

	public void setDropType(String dropType) {
		this.dropType = dropType;
	}
	
	

}
