/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;

import controllers.webmodels.AssignedIntreactionNoCall;

@SqlResultSetMappings({ @SqlResultSetMapping(name = "AssignedIntreactionNoCallProcedure", classes = {
		@ConstructorResult(targetClass = AssignedIntreactionNoCall.class, columns = {
				@ColumnResult(name = "wyzUserName", type = String.class),
				@ColumnResult(name = "locations", type = String.class),
				@ColumnResult(name = "campaignName", type = String.class),
				@ColumnResult(name = "customerName", type = String.class),
				@ColumnResult(name = "vehicleId", type = Long.class),
				@ColumnResult(name = "chassisNo", type = String.class),
				@ColumnResult(name = "model", type = String.class),
				@ColumnResult(name = "vehicleRegNo", type = String.class),
				@ColumnResult(name = "uploadDate", type = Date.class),
				@ColumnResult(name = "noCallAssignId", type = Long.class)
		})

		})})

@Entity
public class AssignedInteraction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public String callMade;
	public String interactionType;
	
	
	@Column(length = 30)
	public Date uplodedCurrentDate;

	@ManyToOne(cascade = CascadeType.ALL)
	public Customer customer;

	@ManyToOne(cascade = CascadeType.ALL)
	public WyzUser wyzUser;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "vehical_Id", nullable = true)
	public Vehicle vehicle;
	
	@OneToOne
	public CallDispositionData finalDisposition;
	
	public String lastDisposition;
	
	public String getLastDisposition() {
		return lastDisposition;
	}

	public void setLastDisposition(String lastDisposition) {
		this.lastDisposition = lastDisposition;
	}
	
	@ManyToOne
	public CallAssignmentStatus assignmentStatus;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "assignedInteraction")
	public List<CallInteraction> interactions;
	
	@ManyToOne
	public Campaign campaign;

	public String getInteractionType() {
        return interactionType;
    }
	/* @ManyToOne
	public Complaint complaint;
	 */
	
	public boolean displayFlag;
	
	
	public boolean isDisplayFlag() {
		return displayFlag;
	}

	public void setDisplayFlag(boolean displayFlag) {
		this.displayFlag = displayFlag;
	}

	/* public Complaint getComplaint() {
        return complaint;
    }

    public void setComplaint(Complaint complaint) {
        this.complaint = complaint;
    } */
    public void setInteractionType(String interactionType) {
        this.interactionType = interactionType;
    }
	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getUplodedCurrentDate() {
		return uplodedCurrentDate;
	}

	public void setUplodedCurrentDate(Date uplodedCurrentDate) {
		this.uplodedCurrentDate = uplodedCurrentDate;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	public String getCallMade() {
		return callMade;
	}

	public void setCallMade(String callMade) {
		this.callMade = callMade;
	}

	public CallDispositionData getFinalDisposition() {
		return finalDisposition;
	}

	public void setFinalDisposition(CallDispositionData finalDisposition) {
		this.finalDisposition = finalDisposition;
	}

	public CallAssignmentStatus getAssignmentStatus() {
		return assignmentStatus;
	}

	public void setAssignmentStatus(CallAssignmentStatus assignmentStatus) {
		this.assignmentStatus = assignmentStatus;
	}

	public List<CallInteraction> getInteractions() {
		return interactions;
	}

	public void setInteractions(List<CallInteraction> interactions) {
		this.interactions = interactions;
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	
	

}
