package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AssignedCallsReport {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	public long wyzuserId;
	
	public String uploadId;
	
	public Date assignedDate;
	
	public long assignInteractionID;
	
	public String assignmentType;
	
	public long vehicleId;
	
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getWyzuserId() {
		return wyzuserId;
	}

	public void setWyzuserId(long wyzuserId) {
		this.wyzuserId = wyzuserId;
	}

	public String getUploadId() {
		return uploadId;
	}

	public void setUploadId(String uploadId) {
		this.uploadId = uploadId;
	}

	public Date getAssignedDate() {
		return assignedDate;
	}

	public void setAssignedDate(Date assignedDate) {
		this.assignedDate = assignedDate;
	}

	public long getAssignInteractionID() {
		return assignInteractionID;
	}

	public void setAssignInteractionID(long assignInteractionID) {
		this.assignInteractionID = assignInteractionID;
	}

	public String getAssignmentType() {
		return assignmentType;
	}

	public void setAssignmentType(String assignmentType) {
		this.assignmentType = assignmentType;
	}

	public long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}
	
	
	
	

}
