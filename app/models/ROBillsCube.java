package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class ROBillsCube {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	public long customer_id;

	public long location_cityId;

	public long workshop_id;

	public long vehicle_id;

	@Column(length = 50)
	public String chassisNo;

	@Column(length = 100)
	public String color;

	@Column(length = 100)
	public String engineNo;

	@Column(length = 100)
	public String model;

	@Column(length = 100)
	public String variant;

	@Temporal(TemporalType.DATE)
	public Date saleDate;

	@Column(length = 30)
	public String vehicleRegNo;

	@Column(length = 100)
	public String modelGroup;

	@Column(length = 255)
	public String customerName;

	@Column(length = 30)
	public String customerPhone;
	
	@Temporal(TemporalType.DATE)
	public Date billDate;

	public double billAmt;

	@Column(length = 100)
	public String billNumber;

	@Column(length = 255)
	public String billStatus;

	@Temporal(TemporalType.DATE)
	public Date lastServiceDate;

	@Column(length = 100)
	public String lastServiceMeterReading;

	@Column(length = 100)
	public String lastServiceType;

	@Column(length = 100)
	public String technician;

	@Temporal(TemporalType.DATE)
	public Date jobCardDate;

	@Column(length = 100)
	public String jobCardNumber;

	@Column(length = 255)
	public String jobCardStatus;

	@Column(length = 150)
	public String jobcardLocation;

	@Column(length = 100)
	public String saName;

	@Column(length = 120)
	public String serviceCategory;

	@Column(length = 255)
	public String menuCodeDesc;

	@Temporal(TemporalType.DATE)
	public Date originalWarrantystartDate;

	@Column(length = 150)
	public String taxiNonTaxi;

	@Column(length = 500)
	public String address;

	@Column(length = 100)
	public String workshopName;

	@Column(length = 100)
	public String location_name;

	@Column(length = 100)
	public String upload_id;

	@Temporal(TemporalType.DATE)
	public Date uploadeddate;

	public boolean isUploadedToday;

	@Column(length = 100)
	public String warrantyyn;

	@Column(length = 100)
	public String shieldexpiryDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}

	public long getLocation_cityId() {
		return location_cityId;
	}

	public void setLocation_cityId(long location_cityId) {
		this.location_cityId = location_cityId;
	}

	public long getWorkshop_id() {
		return workshop_id;
	}

	public void setWorkshop_id(long workshop_id) {
		this.workshop_id = workshop_id;
	}

	public long getVehicle_id() {
		return vehicle_id;
	}

	public void setVehicle_id(long vehicle_id) {
		this.vehicle_id = vehicle_id;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getEngineNo() {
		return engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}

	public Date getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public String getModelGroup() {
		return modelGroup;
	}

	public void setModelGroup(String modelGroup) {
		this.modelGroup = modelGroup;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	public double getBillAmt() {
		return billAmt;
	}

	public void setBillAmt(double billAmt) {
		this.billAmt = billAmt;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

	public Date getLastServiceDate() {
		return lastServiceDate;
	}

	public void setLastServiceDate(Date lastServiceDate) {
		this.lastServiceDate = lastServiceDate;
	}

	public String getLastServiceMeterReading() {
		return lastServiceMeterReading;
	}

	public void setLastServiceMeterReading(String lastServiceMeterReading) {
		this.lastServiceMeterReading = lastServiceMeterReading;
	}

	public String getLastServiceType() {
		return lastServiceType;
	}

	public void setLastServiceType(String lastServiceType) {
		this.lastServiceType = lastServiceType;
	}

	public String getTechnician() {
		return technician;
	}

	public void setTechnician(String technician) {
		this.technician = technician;
	}

	public Date getJobCardDate() {
		return jobCardDate;
	}

	public void setJobCardDate(Date jobCardDate) {
		this.jobCardDate = jobCardDate;
	}

	public String getJobCardNumber() {
		return jobCardNumber;
	}

	public void setJobCardNumber(String jobCardNumber) {
		this.jobCardNumber = jobCardNumber;
	}

	public String getJobCardStatus() {
		return jobCardStatus;
	}

	public void setJobCardStatus(String jobCardStatus) {
		this.jobCardStatus = jobCardStatus;
	}

	public String getJobcardLocation() {
		return jobcardLocation;
	}

	public void setJobcardLocation(String jobcardLocation) {
		this.jobcardLocation = jobcardLocation;
	}

	public String getSaName() {
		return saName;
	}

	public void setSaName(String saName) {
		this.saName = saName;
	}

	public String getServiceCategory() {
		return serviceCategory;
	}

	public void setServiceCategory(String serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

	public String getMenuCodeDesc() {
		return menuCodeDesc;
	}

	public void setMenuCodeDesc(String menuCodeDesc) {
		this.menuCodeDesc = menuCodeDesc;
	}

	public Date getOriginalWarrantystartDate() {
		return originalWarrantystartDate;
	}

	public void setOriginalWarrantystartDate(Date originalWarrantystartDate) {
		this.originalWarrantystartDate = originalWarrantystartDate;
	}

	public String getTaxiNonTaxi() {
		return taxiNonTaxi;
	}

	public void setTaxiNonTaxi(String taxiNonTaxi) {
		this.taxiNonTaxi = taxiNonTaxi;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getWorkshopName() {
		return workshopName;
	}

	public void setWorkshopName(String workshopName) {
		this.workshopName = workshopName;
	}

	public String getLocation_name() {
		return location_name;
	}

	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	public Date getUploadeddate() {
		return uploadeddate;
	}

	public void setUploadeddate(Date uploadeddate) {
		this.uploadeddate = uploadeddate;
	}

	public boolean isUploadedToday() {
		return isUploadedToday;
	}

	public void setUploadedToday(boolean isUploadedToday) {
		this.isUploadedToday = isUploadedToday;
	}

	public String getWarrantyyn() {
		return warrantyyn;
	}

	public void setWarrantyyn(String warrantyyn) {
		this.warrantyyn = warrantyyn;
	}

	public String getShieldexpiryDate() {
		return shieldexpiryDate;
	}

	public void setShieldexpiryDate(String shieldexpiryDate) {
		this.shieldexpiryDate = shieldexpiryDate;
	}
	
	

}
