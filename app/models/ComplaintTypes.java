package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class ComplaintTypes {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(length = 50)
	private String departmentName;
	@Column(length = 50)
	private String taggedUserName;
	@Column(length = 50)
	private String taggedUserNumber;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getTaggedUserName() {
		return taggedUserName;
	}
	public void setTaggedUserName(String taggedUserName) {
		this.taggedUserName = taggedUserName;
	}
	public String getTaggedUserNumber() {
		return taggedUserNumber;
	}
	public void setTaggedUserNumber(String taggedUserNumber) {
		this.taggedUserNumber = taggedUserNumber;
	}
	
	
	

}
