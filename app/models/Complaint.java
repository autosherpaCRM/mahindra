/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 *
 * @author wct-09
 */

@Entity
public class Complaint {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	// C1000001
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public String complaintNumber;

	public String callMade;
	public String CallType;
	// public String assignedto;
	public long ageOfComplaint;

	// public long assignedUserTo;
	@Column(length = 100)
	public String complaintType;
	@Temporal(TemporalType.DATE)
	public Date issueDate;
	@Column(length = 200)
	public String sourceName;
	@Column(length = 200)
	public String subcomplaintType;

	@Column(length = 30)
	public String complaintStatus;

	@Column(length = 20)
	public String isAssigned;

	@Column(length = 100)
	public String functionName;
	public String priority;

	@Column(length = 100)
	public String complaintSource;

	@Column(length = 2000)
	public String description;

	@ManyToOne
	public WyzUser wyzUser;

	@ManyToOne
	public WyzUser assignedUserTo;

	@ManyToOne
	public Vehicle vehicle;
	
	@ManyToOne
	public Customer customer;
	
	@ManyToOne
	public Campaign campaign;

	@ManyToOne
	public Location location;

	@Column(length = 20)
	public String vehicleRegNo;

	@Column(length = 40)
	public String customerName;

	@Column(length = 20)
	public String customerPhone;

	@Temporal(TemporalType.DATE)
	public Date RaisedDate;
	public String customerstatus;

	@Column(length = 100)
	public String serviceadvisor;

	@Column(length = 100)
	public String workshop;

	@Temporal(TemporalType.DATE)
	public Date lastServiceDate;

	
	public String getComplaintSource() {
		return complaintSource;
	}

	public void setComplaintSource(String complaintSource) {
		this.complaintSource = complaintSource;
	}

	public WyzUser getAssignedUserTo() {
		return assignedUserTo;
	}

	public void setAssignedUserTo(WyzUser assignedUserTo) {
		this.assignedUserTo = assignedUserTo;
	}

	@OneToOne
	public CallInteraction callInteraction;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "complaint")
	public List<ComplaintInteraction> complaintInteractions;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "complaint")
	public List<ComplaintAssignedInteraction> complaintAssignedInteractions;

	// public WyzUser getAssignedUser() {
	// return assignedUser;
	// }
	//
	// public void setAssignedUser(WyzUser assignedUser) {
	// this.assignedUser = assignedUser;
	// }

	public List<ComplaintAssignedInteraction> getComplaintAssignedInteractions() {
		return complaintAssignedInteractions;
	}

	public void setComplaintAssignedInteractions(List<ComplaintAssignedInteraction> complaintAssignedInteractions) {
		this.complaintAssignedInteractions = complaintAssignedInteractions;
	}

	public List<ComplaintInteraction> getComplaintInteractions() {
		return complaintInteractions;
	}

	public void setComplaintInteractions(List<ComplaintInteraction> complaintInteractions) {
		this.complaintInteractions = complaintInteractions;
	}

	public CallInteraction getCallInteraction() {
		return callInteraction;
	}

	public void setCallInteraction(CallInteraction callInteraction) {
		this.callInteraction = callInteraction;
	}

	public String getIsAssigned() {
		return isAssigned;
	}

	public void setIsAssigned(String isAssigned) {
		this.isAssigned = isAssigned;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getSubcomplaintType() {
		return subcomplaintType;
	}

	public void setSubcomplaintType(String subcomplaintType) {
		this.subcomplaintType = subcomplaintType;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getCustomerstatus() {
		return customerstatus;
	}

	public void setCustomerstatus(String customerstatus) {
		this.customerstatus = customerstatus;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getComplaintNumber() {
		return complaintNumber;
	}

	public void setComplaintNumber(String complaintNumber) {
		this.complaintNumber = complaintNumber;
	}

	public String getCallMade() {
		return callMade;
	}

	public void setCallMade(String callMade) {
		this.callMade = callMade;
	}

	public String getCallType() {
		return CallType;
	}

	public void setCallType(String CallType) {
		this.CallType = CallType;
	}

	// public String getAssignedto() {
	// return assignedto;
	// }
	//
	// public void setAssignedto(String assignedto) {
	// this.assignedto = assignedto;
	// }

	public long getAgeOfComplaint() {
		return ageOfComplaint;
	}

	public void setAgeOfComplaint(long ageOfComplaint) {
		this.ageOfComplaint = ageOfComplaint;
	}

	public String getComplaintType() {
		return complaintType;
	}

	public void setComplaintType(String complaintType) {
		this.complaintType = complaintType;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public String getComplaintStatus() {
		return complaintStatus;
	}

	public void setComplaintStatus(String complaintStatus) {
		this.complaintStatus = complaintStatus;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public Date getRaisedDate() {
		return RaisedDate;
	}

	public void setRaisedDate(Date RaisedDate) {
		this.RaisedDate = RaisedDate;
	}

	public String getServiceadvisor() {
		return serviceadvisor;
	}

	public void setServiceadvisor(String serviceadvisor) {
		this.serviceadvisor = serviceadvisor;
	}

	public String getWorkshop() {
		return workshop;
	}

	public void setWorkshop(String workshop) {
		this.workshop = workshop;
	}

	public Date getLastServiceDate() {
		return lastServiceDate;
	}

	public void setLastServiceDate(Date lastServiceDate) {
		this.lastServiceDate = lastServiceDate;
	}

}
