package models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Vehicle {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long vehicle_id;
	@Column(length = 30)
	public String vehicleRegNo;
	@Column(length = 30)
	public String vehicleNumber;
	@Column(length = 30)
	public String model;
	@Column(length = 60)
	public String variant;

	@Column(length = 60)
	public String variantCode;
	
	@Column(length = 60)
	public String averageRunning;

	@Column(length = 60)
	public String colorCode;

	@Column(length = 60)
	public String customerId;

	@Column(length = 60)
	public String enquiryNo;

	@Column(length = 30)
	public String jobCardNumber;
	@Column(length = 30)
	public String color;
	@Column(length = 30)
	public String engineNo;
	@Column(length = 30)
	public String chassisNo;

	@Column(length = 30)
	public String dealershipName;

	@Temporal(TemporalType.DATE)
	public Date saleDate;
	
	@Temporal(TemporalType.DATE)
	public Date purchaseDate;
	
	//Insurance parameters
	
	@Temporal(TemporalType.DATE)
	public Date policyDueDate;
	
	
	@Column(length = 100)
	public String nextRenewalType;
	
	
	@Temporal(TemporalType.DATE)
	public Date policyFollowUpDue;
	
	
	@Column (length =100)
	public String insuranceCompanyName;

	
	
	@Temporal(TemporalType.DATE)
	public Date forecasted_duedate;
	
	@Column(length = 10)
	public String fuelType;
	@Column(length = 10)
	public String isAssigned;

	@Column(length = 40)
	public String extendedWarentyDue;

	@Column(nullable = true)
	public long age_of_vehicle;

	@Temporal(TemporalType.DATE)
	public Date nextServicedate;

	@Temporal(TemporalType.DATE)
	public Date nextServisedateBasedon_Tenure;

	@Temporal(TemporalType.DATE)
	public Date nextServisedueBasedon_Tenure;

	@Temporal(TemporalType.DATE)
	public Date nextServiseDateBasedon_Running;

	@Column(length = 20)
	public String nextServicetype;

	@Column(length = 30)
	public String odometerReading;

	@Temporal(TemporalType.DATE)
	public Date odometerReadingDate;

	
	public double averageRunningPerDay;
	@Column(nullable = true)
	public double runningBetweenvisits;

	@Column(nullable = true)
	public long daysBetweenVisit;

	@Temporal(TemporalType.DATE)
	public Date createdDate;

	@Temporal(TemporalType.DATE)
	public Date fs1ExpiryDate;

	@Temporal(TemporalType.DATE)
	public Date fs2ExpiryDate;

	@Temporal(TemporalType.DATE)
	public Date fs3ExpiryDate;

	@Temporal(TemporalType.DATE)
	public Date invDate;

	@Column(length = 30)
	public String forecastLogic;

	@Column(length = 30)
	public String salesChannel;

	@Column(length = 30)
	public String exchange;

	@Temporal(TemporalType.DATE)
	public Date lastServiceDate;

	@Temporal(TemporalType.DATE)
	public Date followUpDateSMR;

	@Temporal(TemporalType.DATE)
	public Date deliveryDate_Sale;

	@Column(length = 100)
	public String lastServiceType;

	@Column(length = 50)
	public String villageDesc;

	@Column(length = 50)
	public String tehsilDesc;

	@Column(length = 50)
	public String district;

	@Column(length = 50)
	public String stateDesc;

	@Temporal(TemporalType.DATE)
	public Date miDate;

	@Column(length = 50)
	public String miFlag;

	@Column(length = 50)
	public String miName;

	@Column(length = 50)
	public String ref_MobileNo;

	@Column(length = 50)
	public String refVehicle_regnNo;

	@Column(length = 50)
	public String refferenceNo;

	@Column(length = 50)
	public String referredBy;

	@Column(length = 50)
	public String referenceType;

	@Column(length = 50)
	public String exch_Canc_Reason;

	@Temporal(TemporalType.DATE)
	public Date exch_Canc_Date;

	@Column(length = 50)
	public String old_Car_Relation;

	@Column(length = 50)
	public String oldcar_Owner;

	@Column(length = 50)
	public String oldcar_Status;

	@Column(length = 50)
	public String oldcar_modelCode;

	@Column(length = 50)
	public String oldcar_Manufacturer;

	@Column(length = 50)
	public String oldCar_regNum;

	@Column(length = 50)
	public String evaluator_name_MSPIN;

	// @Column(length = 50)
	// public double mga_soldAmt;

	public Double valueadded_Tax;
	public String enquirySource;

	public String getEnquirySource() {
		return enquirySource;
	}

	public void setEnquirySource(String enquirySource) {
		this.enquirySource = enquirySource;
	}

	public Double loyalty_bonusDiscount;
	public String extWarrantyType;
	public Double csdEx_showroomDiscount;
	public Double discount;
	public Double institutionalCustomer;
	public Double schemeof_GOI;
	public Double addtional_Tax;
	public Double basic_Price;
	public String email_Id;
	public String mobile2;
	public String mobile1;
	public String off_Phone;
	public String stdCode_comp;
	public String res_Phone;
	public String std_code;
	public String ewType;
	public Double hyp_Amount;
	public String hyp_Address;
	public String hypo;
	public String dsa;
	public String dse;
	public String teamLead;
	public String pin_Desc;
	public String pincode;
	public String city;
	public String clr_Desc;
	public String mul_Inv_No;
	public Date mul_Inv_Dt;
	public String Variant_Desc;
	public Date cancel_Date;
	public String cancel_No;
	public String status;
	
	@Column(length = 100)
	public String forecasted_duetype;
	
	@Column(length = 100)
	public String system_forecast_logic;
	
	
	@Column(length = 100)
	public String modelGroup;
	
	@Column(length = 100)
	public String delivaryNoteNumber;
	
	@Column(length = 100)
	public String kitNo;
	
	
	public long seating;
	
	
	@Column(length = 100)
	public String oemInvoiceNo;
	

	public Date oemInvoiceDate;
	
	@Column(length = 100)
	public String initialPriority;
	
	@Column(length = 100)
	public String currentPriority;
	
	@Column(length = 100)
	public String oTFNo;
	
	public Date oTFDate;
	
	@Column(length = 100)
	public String financier;
	
	@Column(length = 100)
	public String finanaceBranch;

	public double finanaceAmount;
	
	@Column(length = 100)
	public String priceType;
	
	public double sellingPrice;
	
	public double dealerDisount;
	
	public double oemSchemaDiscount;
	
	public double totalCashRecieved;
	
	public double invoiceAmount;
	
	@Column(length = 100)
	public String modelFamilyCode;
	
	public boolean commercialVehicle;	
	
	
	public boolean isCommercialVehicle() {
		return commercialVehicle;
	}

	public void setCommercialVehicle(boolean commercialVehicle) {
		this.commercialVehicle = commercialVehicle;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "vehicle")
	public List<InsuranceAssignedInteraction> insuranceAssignedInteraction;


	@OneToMany(cascade = CascadeType.ALL, mappedBy = "vehicle")
	public List<PSFAssignedInteraction> psfAssignedInteraction;

	public List<PSFAssignedInteraction> getPsfAssignedInteraction() {
		return psfAssignedInteraction;
	}

	public void setPsfAssignedInteraction(List<PSFAssignedInteraction> psfAssignedInteraction) {
		this.psfAssignedInteraction = psfAssignedInteraction;
	}

	public Double getValueadded_Tax() {
		return valueadded_Tax;
	}

	public void setValueadded_Tax(Double valueadded_Tax) {
		this.valueadded_Tax = valueadded_Tax;
	}

	public Double getLoyalty_bonusDiscount() {
		return loyalty_bonusDiscount;
	}

	public void setLoyalty_bonusDiscount(Double loyalty_bonusDiscount) {
		this.loyalty_bonusDiscount = loyalty_bonusDiscount;
	}

	public String getExtWarrantyType() {
		return extWarrantyType;
	}

	public void setExtWarrantyType(String extWarrantyType) {
		this.extWarrantyType = extWarrantyType;
	}

	public Double getCsdEx_showroomDiscount() {
		return csdEx_showroomDiscount;
	}

	public void setCsdEx_showroomDiscount(Double csdEx_showroomDiscount) {
		this.csdEx_showroomDiscount = csdEx_showroomDiscount;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getInstitutionalCustomer() {
		return institutionalCustomer;
	}

	public void setInstitutionalCustomer(Double institutionalCustomer) {
		this.institutionalCustomer = institutionalCustomer;
	}

	public Double getSchemeof_GOI() {
		return schemeof_GOI;
	}

	public void setSchemeof_GOI(Double schemeof_GOI) {
		this.schemeof_GOI = schemeof_GOI;
	}

	public Double getAddtional_Tax() {
		return addtional_Tax;
	}

	public void setAddtional_Tax(Double addtional_Tax) {
		this.addtional_Tax = addtional_Tax;
	}

	public Double getBasic_Price() {
		return basic_Price;
	}

	public void setBasic_Price(Double basic_Price) {
		this.basic_Price = basic_Price;
	}

	public String getEmail_Id() {
		return email_Id;
	}

	public void setEmail_Id(String email_Id) {
		this.email_Id = email_Id;
	}

	public String getMobile2() {
		return mobile2;
	}

	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}

	public String getMobile1() {
		return mobile1;
	}

	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}

	public String getOff_Phone() {
		return off_Phone;
	}

	public void setOff_Phone(String off_Phone) {
		this.off_Phone = off_Phone;
	}

	public String getStdCode_comp() {
		return stdCode_comp;
	}

	public void setStdCode_comp(String stdCode_comp) {
		this.stdCode_comp = stdCode_comp;
	}

	public String getRes_Phone() {
		return res_Phone;
	}

	public void setRes_Phone(String res_Phone) {
		this.res_Phone = res_Phone;
	}

	public String getStd_code() {
		return std_code;
	}

	public void setStd_code(String std_code) {
		this.std_code = std_code;
	}

	public String getEwType() {
		return ewType;
	}

	public void setEwType(String ewType) {
		this.ewType = ewType;
	}

	public Double getHyp_Amount() {
		return hyp_Amount;
	}

	public void setHyp_Amount(Double hyp_Amount) {
		this.hyp_Amount = hyp_Amount;
	}

	public String getHyp_Address() {
		return hyp_Address;
	}

	public void setHyp_Address(String hyp_Address) {
		this.hyp_Address = hyp_Address;
	}

	public String getHypo() {
		return hypo;
	}

	public void setHypo(String hypo) {
		this.hypo = hypo;
	}

	public String getDsa() {
		return dsa;
	}

	public void setDsa(String dsa) {
		this.dsa = dsa;
	}

	public String getDse() {
		return dse;
	}

	public void setDse(String dse) {
		this.dse = dse;
	}

	public String getTeamLead() {
		return teamLead;
	}

	public void setTeamLead(String teamLead) {
		this.teamLead = teamLead;
	}

	public String getPin_Desc() {
		return pin_Desc;
	}

	public void setPin_Desc(String pin_Desc) {
		this.pin_Desc = pin_Desc;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getClr_Desc() {
		return clr_Desc;
	}

	public void setClr_Desc(String clr_Desc) {
		this.clr_Desc = clr_Desc;
	}

	public String getMul_Inv_No() {
		return mul_Inv_No;
	}

	public void setMul_Inv_No(String mul_Inv_No) {
		this.mul_Inv_No = mul_Inv_No;
	}

	public Date getMul_Inv_Dt() {
		return mul_Inv_Dt;
	}

	public void setMul_Inv_Dt(Date mul_Inv_Dt) {
		this.mul_Inv_Dt = mul_Inv_Dt;
	}

	public String getVariant_Desc() {
		return Variant_Desc;
	}

	public void setVariant_Desc(String Variant_Desc) {
		this.Variant_Desc = Variant_Desc;
	}

	public Date getCancel_Date() {
		return cancel_Date;
	}

	public void setCancel_Date(Date cancel_Date) {
		this.cancel_Date = cancel_Date;
	}

	public String getCancel_No() {
		return cancel_No;
	}

	public void setCancel_No(String cancel_No) {
		this.cancel_No = cancel_No;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	// public double getMga_soldAmt() {
	// return mga_soldAmt;
	// }
	//
	// public void setMga_soldAmt(double mga_soldAmt) {
	// this.mga_soldAmt = mga_soldAmt;
	// }

	public String getVillageDesc() {
		return villageDesc;
	}

	public void setVillageDesc(String villageDesc) {
		this.villageDesc = villageDesc;
	}

	public String getTehsilDesc() {
		return tehsilDesc;
	}

	public void setTehsilDesc(String tehsilDesc) {
		this.tehsilDesc = tehsilDesc;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public Date getMiDate() {
		return miDate;
	}

	public void setMiDate(Date miDate) {
		this.miDate = miDate;
	}

	public String getMiFlag() {
		return miFlag;
	}

	public void setMiFlag(String miFlag) {
		this.miFlag = miFlag;
	}

	public String getMiName() {
		return miName;
	}

	public void setMiName(String miName) {
		this.miName = miName;
	}

	public String getRef_MobileNo() {
		return ref_MobileNo;
	}

	public void setRef_MobileNo(String ref_MobileNo) {
		this.ref_MobileNo = ref_MobileNo;
	}

	public String getRefVehicle_regnNo() {
		return refVehicle_regnNo;
	}

	public void setRefVehicle_regnNo(String refVehicle_regnNo) {
		this.refVehicle_regnNo = refVehicle_regnNo;
	}

	public String getRefferenceNo() {
		return refferenceNo;
	}

	public void setRefferenceNo(String refferenceNo) {
		this.refferenceNo = refferenceNo;
	}

	public String getReferredBy() {
		return referredBy;
	}

	public void setReferredBy(String referredBy) {
		this.referredBy = referredBy;
	}

	public String getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(String referenceType) {
		this.referenceType = referenceType;
	}

	public String getExch_Canc_Reason() {
		return exch_Canc_Reason;
	}

	public void setExch_Canc_Reason(String exch_Canc_Reason) {
		this.exch_Canc_Reason = exch_Canc_Reason;
	}

	public Date getExch_Canc_Date() {
		return exch_Canc_Date;
	}

	public void setExch_Canc_Date(Date exch_Canc_Date) {
		this.exch_Canc_Date = exch_Canc_Date;
	}

	public String getOld_Car_Relation() {
		return old_Car_Relation;
	}

	public void setOld_Car_Relation(String old_Car_Relation) {
		this.old_Car_Relation = old_Car_Relation;
	}

	public String getOldcar_Owner() {
		return oldcar_Owner;
	}

	public void setOldcar_Owner(String oldcar_Owner) {
		this.oldcar_Owner = oldcar_Owner;
	}

	public String getOldcar_Status() {
		return oldcar_Status;
	}

	public void setOldcar_Status(String oldcar_Status) {
		this.oldcar_Status = oldcar_Status;
	}

	public String getOldcar_modelCode() {
		return oldcar_modelCode;
	}

	public void setOldcar_modelCode(String oldcar_modelCode) {
		this.oldcar_modelCode = oldcar_modelCode;
	}

	public String getOldcar_Manufacturer() {
		return oldcar_Manufacturer;
	}

	public void setOldcar_Manufacturer(String oldcar_Manufacturer) {
		this.oldcar_Manufacturer = oldcar_Manufacturer;
	}

	public String getOldCar_regNum() {
		return oldCar_regNum;
	}

	public void setOldCar_regNum(String oldCar_regNum) {
		this.oldCar_regNum = oldCar_regNum;
	}

	public String getEvaluator_name_MSPIN() {
		return evaluator_name_MSPIN;
	}

	public void setEvaluator_name_MSPIN(String evaluator_name_MSPIN) {
		this.evaluator_name_MSPIN = evaluator_name_MSPIN;
	}

	@ManyToOne
	public Outlet outlets;

	@ManyToOne
	public Customer customer;

	@ManyToOne
	public Location location;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "vehicle")
	public List<ServiceBooked> serviceBooked;

	@OneToMany(mappedBy = "vehicle")
	public List<Complaint> complaints;

	@OneToMany(mappedBy = "vehicle")
	public List<CallInteraction> callInteraction;

	@OneToMany(mappedBy = "vehicle")
	public List<Insurance> insurances;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "vehicle")
	public List<AssignedInteraction> assignedInteractions;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "vehicle")
	public List<Service> services;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "vehicle")
	public List<UpsellLead> upsellLead;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "vehicle")
	public List<SMSInteraction> sentSMSes;

	public String upload_id;

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	public List<Service> getServices() {
		return services;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	public List<Complaint> getComplaints() {
		return complaints;
	}

	public void setComplaints(List<Complaint> complaint) {
		this.complaints = complaint;
	}

	public Date getInvDate() {
		return invDate;
	}

	public void setInvDate(Date invDate) {
		this.invDate = invDate;
	}

	public Outlet getOutlets() {
		return outlets;
	}

	public void setOutlets(Outlet outlets) {
		this.outlets = outlets;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<AssignedInteraction> getAssignedInteractions() {
		return assignedInteractions;
	}

	public void setAssignedInteractions(List<AssignedInteraction> assignedInteraction) {
		this.assignedInteractions = assignedInteraction;
	}

	public String getJobCardNumber() {
		return jobCardNumber;
	}

	public void setJobCardNumber(String jobCardNumber) {
		this.jobCardNumber = jobCardNumber;
	}

	public String getDealershipName() {
		return dealershipName;
	}

	public void setDealershipName(String dealershipName) {
		this.dealershipName = dealershipName;
	}

	public String getIsAssigned() {
		return isAssigned;
	}

	public void setIsAssigned(String isAssigned) {
		this.isAssigned = isAssigned;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public long getVehicle_id() {
		return vehicle_id;
	}

	public void setVehicle_id(long vehicle_id) {
		this.vehicle_id = vehicle_id;
	}

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicalRegNo) {
		this.vehicleRegNo = vehicalRegNo;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}

	public List<CallInteraction> getCallInteraction() {
		return callInteraction;
	}

	public void setCallInteraction(List<CallInteraction> callInteraction) {
		this.callInteraction = callInteraction;
	}

	public List<Insurance> getInsurances() {
		return insurances;
	}

	public Insurance getCurrentInsurance() {
		
		
		Insurance ins = null;
		for (Insurance insurance : insurances) {
			if (insurance.isCurrent()) {
				ins = insurance;
			}
		}
		return ins;
	}

	public void setInsurances(List<Insurance> insurances) {
		this.insurances = insurances;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getSaleDateStr() {

		if (saleDate != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate = formatter.format(saleDate);

			return stringDate;
		} else {

			return "";
		}
	}

	public void setSaleDate(java.util.Date saleDate) {
		this.saleDate = saleDate;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getEngineNo() {
		return engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public long getAge_of_vehicle() {
		return age_of_vehicle;
	}

	public void setAge_of_vehicle(long age_of_vehicle) {
		this.age_of_vehicle = age_of_vehicle;
	}

	public Date getNextServicedate() {
		return nextServicedate;
	}

	public String getNextServicedateStr() {
		if (nextServicedate != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate = formatter.format(nextServicedate);
			return stringDate;
		} else {

			return "";

		}
	}

	public void setNextServicedate(Date nextServicedate) {
		this.nextServicedate = nextServicedate;
	}

	public Date getNextServisedateBasedon_Tenure() {
		return nextServisedateBasedon_Tenure;
	}

	public String getNextServicdateBasedon_TenureStr() {
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringDate = formatter.format(nextServisedateBasedon_Tenure);
		return stringDate;
	}
	
	

	public void setNextServisedateBasedon_Tenure(Date nextServisedateBasedon_Tenure) {
		this.nextServisedateBasedon_Tenure = nextServisedateBasedon_Tenure;
	}

	public Date getNextServisedueBasedon_Tenure() {
		return nextServisedueBasedon_Tenure;
	}

	public void setNextServisedueBasedon_Tenure(Date nextServisedueBasedon_Tenure) {
		this.nextServisedueBasedon_Tenure = nextServisedueBasedon_Tenure;
	}

	public Date getNextServiseDateBasedon_Running() {
		return nextServiseDateBasedon_Running;
	}

	public String getNextServiceDateBasedon_RunningStr() {
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringDate = formatter.format(nextServiseDateBasedon_Running);
		return stringDate;
	}

	public void setNextServiseDateBasedon_Running(Date nextServiseDateBasedon_Running) {
		this.nextServiseDateBasedon_Running = nextServiseDateBasedon_Running;
	}

	public String getNextServicetype() {
		return nextServicetype;
	}

	public void setNextServicetype(String nextServicetype) {
		this.nextServicetype = nextServicetype;
	}

	public double getAverageRunningPerDay() {
		return averageRunningPerDay;
	}

	public void setAverageRunningPerDay(double averageRunningPerDay) {
		this.averageRunningPerDay = averageRunningPerDay;
	}

	public double getRunningBetweenvisits() {
		return runningBetweenvisits;
	}

	public void setRunningBetweenvisits(double runningBetweenvisits) {
		this.runningBetweenvisits = runningBetweenvisits;
	}

	public List<ServiceBooked> getServiceBooked() {
		return serviceBooked;
	}

	public void setServiceBooked(List<ServiceBooked> serviceBooked) {
		this.serviceBooked = serviceBooked;
	}

	public java.util.Date getSaleDate() {
		return saleDate;
	}

	public String getOdometerReading() {
		return odometerReading;
	}

	public void setOdometerReading(String odometerReading) {
		this.odometerReading = odometerReading;
	}

	public long getDaysBetweenVisit() {
		return daysBetweenVisit;
	}

	public void setDaysBetweenVisit(long daysBetweenVisit) {
		this.daysBetweenVisit = daysBetweenVisit;
	}

	public Date getOdometerReadingDate() {
		return odometerReadingDate;
	}

	public void setOdometerReadingDate(Date odometerReadingDate) {
		this.odometerReadingDate = odometerReadingDate;
	}

	public Date getFs1ExpiryDate() {
		return fs1ExpiryDate;
	}

	public String getFs1ExpiryDateStr() {
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringDate = formatter.format(fs1ExpiryDate);
		return stringDate;
	}

	public void setFs1ExpiryDate(Date fs1ExpiryDate) {
		this.fs1ExpiryDate = fs1ExpiryDate;
	}

	public Date getFs2ExpiryDate() {
		return fs2ExpiryDate;
	}

	public void setFs2ExpiryDate(Date fs2ExpiryDate) {
		this.fs2ExpiryDate = fs2ExpiryDate;
	}

	public Date getFs3ExpiryDate() {
		return fs3ExpiryDate;
	}

	public void setFs3ExpiryDate(Date fs3ExpiryDate) {
		this.fs3ExpiryDate = fs3ExpiryDate;
	}

	public String getForecastLogic() {
		return forecastLogic;
	}

	public void setForecastLogic(String forecastLogic) {
		this.forecastLogic = forecastLogic;
	}

	public String getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

	public String getExtendedWarentyDue() {
		return extendedWarentyDue;
	}

	public void setExtendedWarentyDue(String extendedWarentyDue) {
		this.extendedWarentyDue = extendedWarentyDue;
	}

	public List<UpsellLead> getUpsellLead() {
		return upsellLead;
	}

	public void setUpsellLead(List<UpsellLead> upsellLead) {
		this.upsellLead = upsellLead;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public Date getLastServiceDate() {
		return lastServiceDate;
	}

	public void setLastServiceDate(Date lastServiceDate) {
		this.lastServiceDate = lastServiceDate;
	}

	public String getLastServiceType() {
		return lastServiceType;
	}

	public void setLastServiceType(String lastServiceType) {
		this.lastServiceType = lastServiceType;
	}

	public Date getFollowUpDateSMR() {
		return followUpDateSMR;
	}

	public void setFollowUpDateSMR(Date followUpDateSMR) {
		this.followUpDateSMR = followUpDateSMR;
	}

	public Date getDeliveryDate_Sale() {
		return deliveryDate_Sale;
	}

	public void setDeliveryDate_Sale(Date deliveryDate_Sale) {
		this.deliveryDate_Sale = deliveryDate_Sale;
	}

	public String getEnquiryNo() {
		return enquiryNo;
	}

	public void setEnquiryNo(String enquiryNo) {
		this.enquiryNo = enquiryNo;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public String getVariantCode() {
		return variantCode;
	}

	public void setVariantCode(String variantCode) {
		this.variantCode = variantCode;
	}

	public List<SMSInteraction> getSentSMSes() {
		return sentSMSes;
	}

	public void setSentSMSes(List<SMSInteraction> sentSMSes) {
		this.sentSMSes = sentSMSes;
	}

	public List<InsuranceAssignedInteraction> getInsuranceAssignedInteraction() {
		return insuranceAssignedInteraction;
	}

	public void setInsuranceAssignedInteraction(List<InsuranceAssignedInteraction> insuranceAssignedInteraction) {
		this.insuranceAssignedInteraction = insuranceAssignedInteraction;
	}

	public Date getForecasted_duedate() {
		return forecasted_duedate;
	}

	public void setForecasted_duedate(Date forecasted_duedate) {
		this.forecasted_duedate = forecasted_duedate;
	}

	public String getForecasted_duetype() {
		return forecasted_duetype;
	}

	public void setForecasted_duetype(String forecasted_duetype) {
		this.forecasted_duetype = forecasted_duetype;
	}

	public String getSystem_forecast_logic() {
		return system_forecast_logic;
	}

	public void setSystem_forecast_logic(String system_forecast_logic) {
		this.system_forecast_logic = system_forecast_logic;
	}

	public String getModelGroup() {
		return modelGroup;
	}

	public void setModelGroup(String modelGroup) {
		this.modelGroup = modelGroup;
	}

	public String getDelivaryNoteNumber() {
		return delivaryNoteNumber;
	}

	public void setDelivaryNoteNumber(String delivaryNoteNumber) {
		this.delivaryNoteNumber = delivaryNoteNumber;
	}

	public String getKitNo() {
		return kitNo;
	}

	public void setKitNo(String kitNo) {
		this.kitNo = kitNo;
	}

	public long getSeating() {
		return seating;
	}

	public void setSeating(long seating) {
		this.seating = seating;
	}

	public String getOemInvoiceNo() {
		return oemInvoiceNo;
	}

	public void setOemInvoiceNo(String oemInvoiceNo) {
		this.oemInvoiceNo = oemInvoiceNo;
	}

	public Date getOemInvoiceDate() {
		return oemInvoiceDate;
	}

	public void setOemInvoiceDate(Date oemInvoiceDate) {
		this.oemInvoiceDate = oemInvoiceDate;
	}

	public String getInitialPriority() {
		return initialPriority;
	}

	public void setInitialPriority(String initialPriority) {
		this.initialPriority = initialPriority;
	}

	public String getCurrentPriority() {
		return currentPriority;
	}

	public void setCurrentPriority(String currentPriority) {
		this.currentPriority = currentPriority;
	}

	public String getoTFNo() {
		return oTFNo;
	}

	public void setoTFNo(String oTFNo) {
		this.oTFNo = oTFNo;
	}

	public Date getoTFDate() {
		return oTFDate;
	}

	public void setoTFDate(Date oTFDate) {
		this.oTFDate = oTFDate;
	}

	public String getFinancier() {
		return financier;
	}

	public void setFinancier(String financier) {
		this.financier = financier;
	}

	public String getFinanaceBranch() {
		return finanaceBranch;
	}

	public void setFinanaceBranch(String finanaceBranch) {
		this.finanaceBranch = finanaceBranch;
	}

	public double getFinanaceAmount() {
		return finanaceAmount;
	}

	public void setFinanaceAmount(double finanaceAmount) {
		this.finanaceAmount = finanaceAmount;
	}

	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	public double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public double getDealerDisount() {
		return dealerDisount;
	}

	public void setDealerDisount(double dealerDisount) {
		this.dealerDisount = dealerDisount;
	}

	public double getOemSchemaDiscount() {
		return oemSchemaDiscount;
	}

	public void setOemSchemaDiscount(double oemSchemaDiscount) {
		this.oemSchemaDiscount = oemSchemaDiscount;
	}

	public double getTotalCashRecieved() {
		return totalCashRecieved;
	}

	public void setTotalCashRecieved(double totalCashRecieved) {
		this.totalCashRecieved = totalCashRecieved;
	}

	public double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public String getModelFamilyCode() {
		return modelFamilyCode;
	}

	public void setModelFamilyCode(String modelFamilyCode) {
		this.modelFamilyCode = modelFamilyCode;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
	
	//Added fields
	
	@Column (length =100) 
	public String typeOfProgram;
	
	@Column (length =100) 
	public String corporateType;
	
	@Column (length =100) 
	public String branchName;
	
	
	public String getTypeOfProgram() {
		return typeOfProgram;
	}

	public void setTypeOfProgram(String typeOfProgram) {
		this.typeOfProgram = typeOfProgram;
	}

	public String getCorporateType() {
		return corporateType;
	}

	public void setCorporateType(String corporateType) {
		this.corporateType = corporateType;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getPolicyDueDate() {
		return policyDueDate;
	}

	public void setPolicyDueDate(Date policyDueDate) {
		this.policyDueDate = policyDueDate;
	}

	public String getNextRenewalType() {
		return nextRenewalType;
	}

	public void setNextRenewalType(String nextRenewalType) {
		this.nextRenewalType = nextRenewalType;
	}

	public Date getPolicyFollowUpDue() {
		return policyFollowUpDue;
	}

	public void setPolicyFollowUpDue(Date policyFollowUpDue) {
		this.policyFollowUpDue = policyFollowUpDue;
	}

	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}

	public void setInsuranceCompanyName(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}

	public String getAverageRunning() {
		return averageRunning;
	}

	public void setAverageRunning(String averageRunning) {
		this.averageRunning = averageRunning;
	}
	
	
	
	
}
