package models;

import javax.persistence.*;

/**
 * @author w-1004
 *
 */
@Entity
public class MessageParameters {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;
	public String messageParameter;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getMessageParameter() {
		return messageParameter;
	}
	public void setMessageParameter(String messageParameter) {
		this.messageParameter = messageParameter;
	}
	
	
}
