package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Tenant {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(length = 40, unique=true)
	private String tenantCode;

	@Column(length = 150)
	private String tenentAddress;

	@Column(length = 30)
	private String tenantPhone;

	@OneToMany(mappedBy = "tenant",cascade = CascadeType.ALL, orphanRemoval = true)
	private List<WyzUser> users;

	private int totalUsers;

	private boolean isDeactivated;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getTenentAddress() {
		return tenentAddress;
	}

	public void setTenentAddress(String tenentAddress) {
		this.tenentAddress = tenentAddress;
	}

	public String getTenantPhone() {
		return tenantPhone;
	}

	public void setTenantPhone(String tenantPhone) {
		this.tenantPhone = tenantPhone;
	}

	public List<WyzUser> getUsers() {
		return users;
	}

	public void setUsers(List<WyzUser> users) {
		this.users = users;
	}

	public int getTotalUsers() {
		return totalUsers;
	}

	public void setTotalUsers(int totalUsers) {
		this.totalUsers = totalUsers;
	}

	public boolean isDeactivated() {
		return isDeactivated;
	}

	public void setDeactivated(boolean isDeactivated) {
		this.isDeactivated = isDeactivated;
	}

}
