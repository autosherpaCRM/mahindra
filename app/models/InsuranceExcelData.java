package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class InsuranceExcelData {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;	
	public String customerName;
	public String contactNo;
	public String email;
	public String doa;
	public java.sql.Date dob;
	public String address;
	public String city;
	public String state;
	public String vehicleRegNo;
	public java.sql.Date saleDate;
	public String engineNo;
	public String chassisNo;
	public String model;
	public String variant;
	public String color;
	public String lastMileage;
	public String policyNo;	
	public String coveragePeriod;
	public java.sql.Date policyDueDate;
	public String insuranceCompanyName;
	public double idv;
	public String odPercentage;	
	public double odAmount;
	public String ncBPercentage;
	public float ncBAmount;
	public String discountPercentage;
	public double ODpremium;
	public double liabilityPremium;
	public String add_ON_Premium;
	public double premiumAmountBeforeTax;
	public String serviceTax;
	public double premiumAmountAfterTax;
	public String lastRenewalBy;
	public String renewalType;
	public String upload_id;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDoa() {
		return doa;
	}
	public void setDoa(String doa) {
		this.doa = doa;
	}
	
	public java.sql.Date getDob() {
		return dob;
	}
	public void setDob(java.sql.Date dob) {
		this.dob = dob;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getVehicleRegNo() {
		return vehicleRegNo;
	}
	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}
	public java.sql.Date getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(java.sql.Date saleDate) {
		this.saleDate = saleDate;
	}
	public String getEngineNo() {
		return engineNo;
	}
	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}
	public String getChassisNo() {
		return chassisNo;
	}
	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getVariant() {
		return variant;
	}
	public void setVariant(String variant) {
		this.variant = variant;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getLastMileage() {
		return lastMileage;
	}
	public void setLastMileage(String lastMileage) {
		this.lastMileage = lastMileage;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getCoveragePeriod() {
		return coveragePeriod;
	}
	public void setCoveragePeriod(String coveragePeriod) {
		this.coveragePeriod = coveragePeriod;
	}
	public java.sql.Date getPolicyDueDate() {
		return policyDueDate;
	}
	public void setPolicyDueDate(java.sql.Date policyDueDate) {
		this.policyDueDate = policyDueDate;
	}
	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}
	public void setInsuranceCompanyName(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}
	public double getIdv() {
		return idv;
	}
	public void setIdv(double idv) {
		this.idv = idv;
	}
	public String getOdPercentage() {
		return odPercentage;
	}
	public void setOdPercentage(String odPercentage) {
		this.odPercentage = odPercentage;
	}
	public double getOdAmount() {
		return odAmount;
	}
	public void setOdAmount(double odAmount) {
		this.odAmount = odAmount;
	}
	public String getNcBPercentage() {
		return ncBPercentage;
	}
	public void setNcBPercentage(String ncBPercentage) {
		this.ncBPercentage = ncBPercentage;
	}
	public float getNcBAmount() {
		return ncBAmount;
	}
	public void setNcBAmount(float ncBAmount) {
		this.ncBAmount = ncBAmount;
	}
	public String getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(String discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	public double getODpremium() {
		return ODpremium;
	}
	public void setODpremium(double oDpremium) {
		ODpremium = oDpremium;
	}
	public double getLiabilityPremium() {
		return liabilityPremium;
	}
	public void setLiabilityPremium(double liabilityPremium) {
		this.liabilityPremium = liabilityPremium;
	}
	public String getAdd_ON_Premium() {
		return add_ON_Premium;
	}
	public void setAdd_ON_Premium(String add_ON_Premium) {
		this.add_ON_Premium = add_ON_Premium;
	}
	public double getPremiumAmountBeforeTax() {
		return premiumAmountBeforeTax;
	}
	public void setPremiumAmountBeforeTax(double premiumAmountBeforeTax) {
		this.premiumAmountBeforeTax = premiumAmountBeforeTax;
	}
	public String getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}
	public double getPremiumAmountAfterTax() {
		return premiumAmountAfterTax;
	}
	public void setPremiumAmountAfterTax(double premiumAmountAfterTax) {
		this.premiumAmountAfterTax = premiumAmountAfterTax;
	}
	public String getLastRenewalBy() {
		return lastRenewalBy;
	}
	public void setLastRenewalBy(String lastRenewalBy) {
		this.lastRenewalBy = lastRenewalBy;
	}
	public String getRenewalType() {
		return renewalType;
	}
	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}
	public String getUpload_id() {
		return upload_id;
	}
	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}
	
	
	


}
