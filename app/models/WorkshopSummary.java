/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.persistence.*;
import java.util.*;

/**
 *
 * @author W-885
 */
@Entity
public class WorkshopSummary {
    
    
        @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
        
        
        @Temporal(TemporalType.DATE)
        public Date serviceBookedDate;
        
        public int noOfServicesBooked;
        
        @ManyToOne
	public Workshop workshop;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getServiceBookedDate() {
        return serviceBookedDate;
    }

    public void setServiceBookedDate(Date serviceBookedDate) {
        this.serviceBookedDate = serviceBookedDate;
    }

    public int getNoOfServicesBooked() {
        return noOfServicesBooked;
    }

    public void setNoOfServicesBooked(int noOfServicesBooked) {
        this.noOfServicesBooked = noOfServicesBooked;
    }

    public Workshop getWorkshop() {
        return workshop;
    }

    public void setWorkshop(Workshop workshop) {
        this.workshop = workshop;
    }
        
        
            
    
}
