package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ColumnDefinition {

	public static String VALUE_TYPE_Continous = "CONTINUOUS";
	public static String VALUE_TYPE_Discrete = "DISCRETE";

	public static String DATA_CAT_Categorical = "CATEGORICAL";
	public static String DATA_CAT_Ordinal = "ORDINAL";
	public static String DATA_CAT_Interval = "INTERVAL";
	public static String DATA_CAT_NOMINAL = "NOMINAL";

	public static String DATA_TYPE_CHAR = "CHAR";
	public static String DATA_TYPE_VARCHAR = "VARCHAR";
	public static String DATA_TYPE_TINYINT = "TINYINT ";
	public static String DATA_TYPE_INTEGER = "INTEGER";
	public static String DATA_TYPE_BIGINT = "BIGINT";
	public static String DATA_TYPE_FLOAT = "FLOAT";
	public static String DATA_TYPE_DOUBLE = "DOUBLE";
	public static String DATA_TYPE_DATE = "DATE";
	public static String DATA_TYPE_TIME = "TIME";
	public static String DATA_TYPE_TIMESTAMP = "TIMESTAMP";
	public static String DATA_TYPE_BOOLEAN = "BOOLEAN";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(length = 40)
	private String metaColumnName;

	@Column(length = 40)
	private String sourceColumnName;

	@Column(length = 40)
	private String destinationColumnName;

	@Column(length = 40)
	private String columnDataType;

	private boolean factorable;

	private boolean isMeasure;

	@Column(length = 40)
	private String columnCategory;

	@Column(length = 40)
	private String measureType;

	private boolean isPrimary;
	
	@Column(length = 50)
	private String dataPattern;
	
	
	private boolean additionalFormData;
	
	

	private int size;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMetaColumnName() {
		return metaColumnName;
	}

	public void setMetaColumnName(String metaColumnName) {
		this.metaColumnName = metaColumnName;
	}

	public String getSourceColumnName() {
		return sourceColumnName;
	}

	public void setSourceColumnName(String sourceColumnName) {
		this.sourceColumnName = sourceColumnName;
	}

	public String getDestinationColumnName() {
		return destinationColumnName;
	}

	public void setDestinationColumnName(String destinationColumnName) {
		this.destinationColumnName = destinationColumnName;
	}

	public String getColumnDataType() {
		return columnDataType;
	}

	public void setColumnDataType(String columnDataType) {
		this.columnDataType = columnDataType;
	}

	public boolean isFactorable() {
		return factorable;
	}

	public void setFactorable(boolean factorable) {
		this.factorable = factorable;
	}

	public boolean isMeasure() {
		return isMeasure;
	}

	public void setMeasure(boolean isMeasure) {
		this.isMeasure = isMeasure;
	}

	public String getColumnCategory() {
		return columnCategory;
	}

	public void setColumnCategory(String columnCategory) {
		this.columnCategory = columnCategory;
	}

	public String getMeasureType() {
		return measureType;
	}

	public void setMeasureType(String measureType) {
		this.measureType = measureType;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isPrimary() {
		return isPrimary;
	}

	public void setPrimary(boolean primary) {
		this.isPrimary = primary;
	}

	public String getDataPattern() {
		return dataPattern;
	}

	public void setDataPattern(String dataPattern) {
		this.dataPattern = dataPattern;
	}

	public boolean isAdditionalFormData() {
		return additionalFormData;
	}

	public void setAdditionalFormData(boolean additionalFormData) {
		this.additionalFormData = additionalFormData;
	}

	
	

}
