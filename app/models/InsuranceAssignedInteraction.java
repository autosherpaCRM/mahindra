package models;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
@Entity
public class InsuranceAssignedInteraction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	public String callMade;
	
	public String interactionType;
	
	public Date uplodedCurrentDate;	
	
	@Column(length = 30)
	public String lastDisposition;

	@ManyToOne
	public Customer customer;
	
	public boolean displayFlag;

	@ManyToOne
	public WyzUser wyzUser;

	@ManyToOne	
	public Vehicle vehicle;
	
	@ManyToOne	
	public Insurance insurance;
	
	@OneToOne
	public CallDispositionData finalDisposition;	
	
	@ManyToOne
	public Campaign campaign;
	
	
	@OneToMany(mappedBy = "insuranceAssignedInteraction")
	public List<CallInteraction> callInteraction;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCallMade() {
		return callMade;
	}

	public void setCallMade(String callMade) {
		this.callMade = callMade;
	}

	public String getInteractionType() {
		return interactionType;
	}

	public void setInteractionType(String interactionType) {
		this.interactionType = interactionType;
	}

	public Date getUplodedCurrentDate() {
		return uplodedCurrentDate;
	}

	public void setUplodedCurrentDate(Date uplodedCurrentDate) {
		this.uplodedCurrentDate = uplodedCurrentDate;
	}

	public String getLastDisposition() {
		return lastDisposition;
	}

	public void setLastDisposition(String lastDisposition) {
		this.lastDisposition = lastDisposition;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public CallDispositionData getFinalDisposition() {
		return finalDisposition;
	}

	public void setFinalDisposition(CallDispositionData finalDisposition) {
		this.finalDisposition = finalDisposition;
	}

	public List<CallInteraction> getCallInteraction() {
		return callInteraction;
	}

	public void setCallInteraction(List<CallInteraction> callInteraction) {
		this.callInteraction = callInteraction;
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	public Insurance getInsurance() {
		return insurance;
	}

	public void setInsurance(Insurance insurance) {
		this.insurance = insurance;
	}

	public boolean isDisplayFlag() {
		return displayFlag;
	}

	public void setDisplayFlag(boolean displayFlag) {
		this.displayFlag = displayFlag;
	}
	
	

}
