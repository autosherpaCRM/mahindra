package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CallAssignmentStatus {

	
	public static int SERVICE_REMINDER = 1;
	public static int SERVICE_BOOKED = 2;
	public static int SERVICE_FOLLOW_UP = 3;
	public static int SERVICE_NON_CONTACT = 4;
	public static int SERVICE_DROPPED = 5;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	public int satusId;
	
	public String status;

	public int getSatusId() {
		return satusId;
	}

	public void setSatusId(int satusId) {
		this.satusId = satusId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
}
