package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SaleRegisterExcelError {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@Column(length = 100)
	public String outlet;
	@Column(length = 100)
	public String locCode;
	@Column(length = 100)
	public String saleType;
	@Column(length = 100)
	public String status;
	@Column(length = 100)
	public String invNo;
	@Column(length = 100)
	public String invCancelNo;
	@Column(length = 100)
	public String model;
	@Column(length = 100)
	public String fuelType;
	@Column(length = 100)
	public String variantCode;
	@Column(length = 100)
	public String varaintDesc;
	@Column(length = 100)
	public String colorcode;
	@Column(length = 100)
	public String colorDesc;
	@Column(length = 100)
	public String chassis;
	@Column(length = 100)
	public String engine;
	@Column(length = 100)
	public String mulInvNo;
	@Column(length = 100)
	public String customerId;
	@Column(length = 100)
	public String salutation;
	@Column(length = 100)
	public String customerName;
	@Column(length = 100)
	public String address1;
	@Column(length = 100)
	public String address2;
	@Column(length = 100)
	public String address3;
	@Column(length = 100)
	public String city;
	@Column(length = 100)
	public String picDesc;
	@Column(length = 100)
	public String teamLead;
	@Column(length = 100)
	public String dse;
	@Column(length = 100)
	public String dsa;
	@Column(length = 100)
	public String hypthecation;
	@Column(length = 100)
	public String hyp_address;
	@Column(length = 100)
	public String regNo;
	@Column(length = 100)
	public String exWarrantyType;
	@Column(length = 100)
	public String stdCode;
	@Column(length = 100)
	public String resCode;
	@Column(length = 100)
	public String stdCodeComp;
	@Column(length = 100)
	public String offPhone;
	@Column(length = 100)
	public String mobile1;
	@Column(length = 100)
	public String mobile2;
	@Column(length = 100)
	public String emailId;

	@Column(length = 100)
	public String invDateStr;
	@Column(length = 100)
	public String invCancelDateStr;
	@Column(length = 100)
	public String delDateStr;
	@Column(length = 100)
	public String promisedDelvDateStr;
	@Column(length = 100)
	public String mulInvDateStr;
	@Column(length = 100)
	public String pincodeStr;
	@Column(length = 100)
	public String hypAmtStr;
	@Column(length = 100)
	public String basicPriceStr;
	@Column(length = 100)
	public String additionalTaxStr;
	@Column(length = 100)
	public String schemeOfGoiStr;
	@Column(length = 100)
	public String instiCustStr;
	@Column(length = 100)
	public String csdExShowRoomStr;
	@Column(length = 100)
	public String loyaltyBonusDiscStr;
	@Column(length = 100)
	public String discountStr;
	@Column(length = 100)
	public String vatStr;
	@Column(length = 100)
	public String roadTaxStr;
	@Column(length = 100)
	public String insuranceStr;
	@Column(length = 100)
	public String extWarrantyStr;
	@Column(length = 100)
	public String sumStr;
	@Column(length = 100)
	public String roundOffStr;
	@Column(length = 100)
	public String ivnAmtStr;
	@Column(length = 100)
	public String purchasePriceStr;
	@Column(length = 100)
	public String panNoStr;
	@Column(length = 100)
	public String orderDateStr;
	@Column(length = 100)
	public String allotmentStr;
	@Column(length = 100)
	public String mgaSoldAmtStr;
	@Column(length = 100)
	public String exchangeCancDateStr;
	@Column(length = 100)
	public String miDateStr;

	@Column(length = 100)
	public String enquiryNo;
	@Column(length = 100)
	public String enquirySources;
	@Column(length = 100)
	public String enquirSubSource;
	@Column(length = 100)
	public String buyerType;
	@Column(length = 100)
	public String tradeIn;
	@Column(length = 100)
	public String indivScheme;
	@Column(length = 100)
	public String corporate;
	@Column(length = 100)
	public String orderNum;
	@Column(length = 100)
	public String area;
	@Column(length = 100)
	public String areaType;
	@Column(length = 100)
	public String evaluatorMSPIN;
	@Column(length = 100)
	public String oldCarRegNo;
	@Column(length = 100)
	public String oldCarMfg;
	@Column(length = 100)
	public String oldCarModelCode;
	@Column(length = 100)
	public String oldCarStatus;
	@Column(length = 100)
	public String oldCarOwner;
	@Column(length = 100)
	public String oldCarRelation;
	@Column(length = 100)
	public String exchCancReason;
	@Column(length = 100)
	public String refType;
	@Column(length = 100)
	public String refby;
	@Column(length = 100)
	public String refNo;
	@Column(length = 100)
	public String refRegNo;
	@Column(length = 100)
	public String refMobileNo;
	@Column(length = 100)
	public String miname;
	@Column(length = 100)
	public String miFlag;
	@Column(length = 100)
	public String stateDesc;
	@Column(length = 100)
	public String district;
	@Column(length = 100)
	public String tehsilDesc;
	@Column(length = 100)
	public String villageDesc;
	@Column(length = 100)
	public String ceName;
	@Column(length = 100)
	public String year;
	@Column(length = 100)
	public String upload_id;

	public int rowNumber;
	public boolean isError;

	@Column(length = 700)
	public String errorInformation;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOutlet() {
		return outlet;
	}

	public void setOutlet(String outlet) {
		this.outlet = outlet;
	}

	public String getLocCode() {
		return locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public String getSaleType() {
		return saleType;
	}

	public void setSaleType(String saleType) {
		this.saleType = saleType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInvNo() {
		return invNo;
	}

	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}

	public String getInvCancelNo() {
		return invCancelNo;
	}

	public void setInvCancelNo(String invCancelNo) {
		this.invCancelNo = invCancelNo;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getVariantCode() {
		return variantCode;
	}

	public void setVariantCode(String variantCode) {
		this.variantCode = variantCode;
	}

	public String getVaraintDesc() {
		return varaintDesc;
	}

	public void setVaraintDesc(String varaintDesc) {
		this.varaintDesc = varaintDesc;
	}

	public String getColorcode() {
		return colorcode;
	}

	public void setColorcode(String colorcode) {
		this.colorcode = colorcode;
	}

	public String getColorDesc() {
		return colorDesc;
	}

	public void setColorDesc(String colorDesc) {
		this.colorDesc = colorDesc;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public String getMulInvNo() {
		return mulInvNo;
	}

	public void setMulInvNo(String mulInvNo) {
		this.mulInvNo = mulInvNo;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPicDesc() {
		return picDesc;
	}

	public void setPicDesc(String picDesc) {
		this.picDesc = picDesc;
	}

	public String getTeamLead() {
		return teamLead;
	}

	public void setTeamLead(String teamLead) {
		this.teamLead = teamLead;
	}

	public String getDse() {
		return dse;
	}

	public void setDse(String dse) {
		this.dse = dse;
	}

	public String getDsa() {
		return dsa;
	}

	public void setDsa(String dsa) {
		this.dsa = dsa;
	}

	public String getHypthecation() {
		return hypthecation;
	}

	public void setHypthecation(String hypthecation) {
		this.hypthecation = hypthecation;
	}

	public String getHyp_address() {
		return hyp_address;
	}

	public void setHyp_address(String hyp_address) {
		this.hyp_address = hyp_address;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getExWarrantyType() {
		return exWarrantyType;
	}

	public void setExWarrantyType(String exWarrantyType) {
		this.exWarrantyType = exWarrantyType;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getStdCodeComp() {
		return stdCodeComp;
	}

	public void setStdCodeComp(String stdCodeComp) {
		this.stdCodeComp = stdCodeComp;
	}

	public String getOffPhone() {
		return offPhone;
	}

	public void setOffPhone(String offPhone) {
		this.offPhone = offPhone;
	}

	public String getMobile1() {
		return mobile1;
	}

	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}

	public String getMobile2() {
		return mobile2;
	}

	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getEnquiryNo() {
		return enquiryNo;
	}

	public void setEnquiryNo(String enquiryNo) {
		this.enquiryNo = enquiryNo;
	}

	public String getEnquirySources() {
		return enquirySources;
	}

	public void setEnquirySources(String enquirySources) {
		this.enquirySources = enquirySources;
	}

	public String getEnquirSubSource() {
		return enquirSubSource;
	}

	public void setEnquirSubSource(String enquirSubSource) {
		this.enquirSubSource = enquirSubSource;
	}

	public String getBuyerType() {
		return buyerType;
	}

	public void setBuyerType(String buyerType) {
		this.buyerType = buyerType;
	}

	public String getTradeIn() {
		return tradeIn;
	}

	public void setTradeIn(String tradeIn) {
		this.tradeIn = tradeIn;
	}

	public String getIndivScheme() {
		return indivScheme;
	}

	public void setIndivScheme(String indivScheme) {
		this.indivScheme = indivScheme;
	}

	public String getCorporate() {
		return corporate;
	}

	public void setCorporate(String corporate) {
		this.corporate = corporate;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAreaType() {
		return areaType;
	}

	public void setAreaType(String areaType) {
		this.areaType = areaType;
	}

	public String getEvaluatorMSPIN() {
		return evaluatorMSPIN;
	}

	public void setEvaluatorMSPIN(String evaluatorMSPIN) {
		this.evaluatorMSPIN = evaluatorMSPIN;
	}

	public String getOldCarRegNo() {
		return oldCarRegNo;
	}

	public void setOldCarRegNo(String oldCarRegNo) {
		this.oldCarRegNo = oldCarRegNo;
	}

	public String getOldCarMfg() {
		return oldCarMfg;
	}

	public void setOldCarMfg(String oldCarMfg) {
		this.oldCarMfg = oldCarMfg;
	}

	public String getOldCarModelCode() {
		return oldCarModelCode;
	}

	public void setOldCarModelCode(String oldCarModelCode) {
		this.oldCarModelCode = oldCarModelCode;
	}

	public String getOldCarStatus() {
		return oldCarStatus;
	}

	public void setOldCarStatus(String oldCarStatus) {
		this.oldCarStatus = oldCarStatus;
	}

	public String getOldCarOwner() {
		return oldCarOwner;
	}

	public void setOldCarOwner(String oldCarOwner) {
		this.oldCarOwner = oldCarOwner;
	}

	public String getOldCarRelation() {
		return oldCarRelation;
	}

	public void setOldCarRelation(String oldCarRelation) {
		this.oldCarRelation = oldCarRelation;
	}

	public String getExchCancReason() {
		return exchCancReason;
	}

	public void setExchCancReason(String exchCancReason) {
		this.exchCancReason = exchCancReason;
	}

	public String getRefType() {
		return refType;
	}

	public void setRefType(String refType) {
		this.refType = refType;
	}

	public String getRefby() {
		return refby;
	}

	public void setRefby(String refby) {
		this.refby = refby;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getRefRegNo() {
		return refRegNo;
	}

	public void setRefRegNo(String refRegNo) {
		this.refRegNo = refRegNo;
	}

	public String getRefMobileNo() {
		return refMobileNo;
	}

	public void setRefMobileNo(String refMobileNo) {
		this.refMobileNo = refMobileNo;
	}

	public String getMiname() {
		return miname;
	}

	public void setMiname(String miname) {
		this.miname = miname;
	}

	public String getMiFlag() {
		return miFlag;
	}

	public void setMiFlag(String miFlag) {
		this.miFlag = miFlag;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getTehsilDesc() {
		return tehsilDesc;
	}

	public void setTehsilDesc(String tehsilDesc) {
		this.tehsilDesc = tehsilDesc;
	}

	public String getVillageDesc() {
		return villageDesc;
	}

	public void setVillageDesc(String villageDesc) {
		this.villageDesc = villageDesc;
	}

	public String getCeName() {
		return ceName;
	}

	public void setCeName(String ceName) {
		this.ceName = ceName;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public boolean isError() {
		return isError;
	}

	public void setError(boolean isError) {
		this.isError = isError;
	}

	public String getErrorInformation() {
		return errorInformation;
	}

	public void setErrorInformation(String errorInformation) {
		this.errorInformation = errorInformation;
	}

	public String getInvDateStr() {
		return invDateStr;
	}

	public void setInvDateStr(String invDateStr) {
		this.invDateStr = invDateStr;
	}

	public String getInvCancelDateStr() {
		return invCancelDateStr;
	}

	public void setInvCancelDateStr(String invCancelDateStr) {
		this.invCancelDateStr = invCancelDateStr;
	}

	public String getDelDateStr() {
		return delDateStr;
	}

	public void setDelDateStr(String delDateStr) {
		this.delDateStr = delDateStr;
	}

	public String getPromisedDelvDateStr() {
		return promisedDelvDateStr;
	}

	public void setPromisedDelvDateStr(String promisedDelvDateStr) {
		this.promisedDelvDateStr = promisedDelvDateStr;
	}

	public String getMulInvDateStr() {
		return mulInvDateStr;
	}

	public void setMulInvDateStr(String mulInvDateStr) {
		this.mulInvDateStr = mulInvDateStr;
	}

	public String getPincodeStr() {
		return pincodeStr;
	}

	public void setPincodeStr(String pincodeStr) {
		this.pincodeStr = pincodeStr;
	}

	public String getHypAmtStr() {
		return hypAmtStr;
	}

	public void setHypAmtStr(String hypAmtStr) {
		this.hypAmtStr = hypAmtStr;
	}

	public String getBasicPriceStr() {
		return basicPriceStr;
	}

	public void setBasicPriceStr(String basicPriceStr) {
		this.basicPriceStr = basicPriceStr;
	}

	public String getAdditionalTaxStr() {
		return additionalTaxStr;
	}

	public void setAdditionalTaxStr(String additionalTaxStr) {
		this.additionalTaxStr = additionalTaxStr;
	}

	public String getSchemeOfGoiStr() {
		return schemeOfGoiStr;
	}

	public void setSchemeOfGoiStr(String schemeOfGoiStr) {
		this.schemeOfGoiStr = schemeOfGoiStr;
	}

	public String getInstiCustStr() {
		return instiCustStr;
	}

	public void setInstiCustStr(String instiCustStr) {
		this.instiCustStr = instiCustStr;
	}

	public String getCsdExShowRoomStr() {
		return csdExShowRoomStr;
	}

	public void setCsdExShowRoomStr(String csdExShowRoomStr) {
		this.csdExShowRoomStr = csdExShowRoomStr;
	}

	public String getLoyaltyBonusDiscStr() {
		return loyaltyBonusDiscStr;
	}

	public void setLoyaltyBonusDiscStr(String loyaltyBonusDiscStr) {
		this.loyaltyBonusDiscStr = loyaltyBonusDiscStr;
	}

	public String getDiscountStr() {
		return discountStr;
	}

	public void setDiscountStr(String discountStr) {
		this.discountStr = discountStr;
	}

	public String getVatStr() {
		return vatStr;
	}

	public void setVatStr(String vatStr) {
		this.vatStr = vatStr;
	}

	public String getRoadTaxStr() {
		return roadTaxStr;
	}

	public void setRoadTaxStr(String roadTaxStr) {
		this.roadTaxStr = roadTaxStr;
	}

	public String getInsuranceStr() {
		return insuranceStr;
	}

	public void setInsuranceStr(String insuranceStr) {
		this.insuranceStr = insuranceStr;
	}

	public String getExtWarrantyStr() {
		return extWarrantyStr;
	}

	public void setExtWarrantyStr(String extWarrantyStr) {
		this.extWarrantyStr = extWarrantyStr;
	}

	public String getSumStr() {
		return sumStr;
	}

	public void setSumStr(String sumStr) {
		this.sumStr = sumStr;
	}

	public String getRoundOffStr() {
		return roundOffStr;
	}

	public void setRoundOffStr(String roundOffStr) {
		this.roundOffStr = roundOffStr;
	}

	public String getIvnAmtStr() {
		return ivnAmtStr;
	}

	public void setIvnAmtStr(String ivnAmtStr) {
		this.ivnAmtStr = ivnAmtStr;
	}

	public String getPurchasePriceStr() {
		return purchasePriceStr;
	}

	public void setPurchasePriceStr(String purchasePriceStr) {
		this.purchasePriceStr = purchasePriceStr;
	}

	public String getPanNoStr() {
		return panNoStr;
	}

	public void setPanNoStr(String panNoStr) {
		this.panNoStr = panNoStr;
	}

	public String getOrderDateStr() {
		return orderDateStr;
	}

	public void setOrderDateStr(String orderDateStr) {
		this.orderDateStr = orderDateStr;
	}

	public String getAllotmentStr() {
		return allotmentStr;
	}

	public void setAllotmentStr(String allotmentStr) {
		this.allotmentStr = allotmentStr;
	}

	public String getMgaSoldAmtStr() {
		return mgaSoldAmtStr;
	}

	public void setMgaSoldAmtStr(String mgaSoldAmtStr) {
		this.mgaSoldAmtStr = mgaSoldAmtStr;
	}

	public String getExchangeCancDateStr() {
		return exchangeCancDateStr;
	}

	public void setExchangeCancDateStr(String exchangeCancDateStr) {
		this.exchangeCancDateStr = exchangeCancDateStr;
	}

	public String getMiDateStr() {
		return miDateStr;
	}

	public void setMiDateStr(String miDateStr) {
		this.miDateStr = miDateStr;
	}

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

}
