package models;

import controllers.webmodels.CallInteractionHistory;
import controllers.webmodels.CallLogAjaxLoad;
import controllers.webmodels.CallLogAjaxLoadMR;
import controllers.webmodels.CallLogDispositionLoad;
import controllers.webmodels.CallLogDispositionLoadMR;
import controllers.webmodels.DailyAssignmentStatusData;
import controllers.webmodels.DailyAvailabilityData;
import controllers.webmodels.InsuranceAgentIdName;
import controllers.webmodels.ServiceAdvisorIdName;
import controllers.webmodels.ServiceBookedResultList;
import controllers.webmodels.InsuranceHistoryReport;
import controllers.webmodels.CallInteractionHistoryCubeReport;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SqlResultSetMappings({ @SqlResultSetMapping(name = "CallLogAjaxLoad", classes = {
		@ConstructorResult(targetClass = CallLogAjaxLoad.class, columns = { @ColumnResult(name = "customer_name"),
				@ColumnResult(name = "vehicle_RegNo"), @ColumnResult(name = "veh_model"),
				@ColumnResult(name = "category"), @ColumnResult(name = "Loyalty"), @ColumnResult(name = "duedate"),
				@ColumnResult(name = "due_type"), @ColumnResult(name = "forecast_type"),
				@ColumnResult(name = "last_psfstatus"), @ColumnResult(name = "DND_status"),
				@ColumnResult(name = "campaignName"), @ColumnResult(name = "complaints_count"),
				@ColumnResult(name = "vehical_Id"), @ColumnResult(name = "wyzUser_id"),
				@ColumnResult(name = "customer_id"), @ColumnResult(name = "assigned_intercation_id")

		})
		}),
	@SqlResultSetMapping(name = "SMSInteractionHistory", classes = { 
				@ConstructorResult(targetClass = CallInteractionHistory.class, columns = { 
				@ColumnResult(name = "interactionDate"), @ColumnResult(name = "interactionTime"), 
				@ColumnResult(name = "smsMessage"), @ColumnResult(name = "smsStatus" ,type=Boolean.class ),
				@ColumnResult(name = "mobileNumber"),
				@ColumnResult(name = "CustomerName"), @ColumnResult(name = "VehicleRegNo"),
				@ColumnResult(name = "WyzuserName")
									
			})
		}),
	@SqlResultSetMapping(name = "DailyAvailabilityData", classes = { 
			@ConstructorResult(targetClass = DailyAvailabilityData.class, columns = { 
			@ColumnResult(name = "userName"), 
			@ColumnResult(name = "campaignName"), 
			@ColumnResult(name = "freshCalls",type = Long.class), 
			@ColumnResult(name = "followUps",type = Long.class),
			@ColumnResult(name = "overDueBookings",type = Long.class),
			@ColumnResult(name = "nonContacts",type = Long.class),
			@ColumnResult(name = "assignedDate",type = java.sql.Date.class)						
		})
	}),
	@SqlResultSetMapping(name = "DailyAssignmentStatusData", classes = { 
			@ConstructorResult(targetClass = DailyAssignmentStatusData.class, columns = { 
			@ColumnResult(name = "module"),
			@ColumnResult(name = "CRE"), 
			@ColumnResult(name = "campaignName"), 
			@ColumnResult(name = "assignedDate",type = java.sql.Date.class),
			@ColumnResult(name = "TotalAssigned",type = Long.class), 
		})
	}),
	@SqlResultSetMapping(name = "CallLogDispositionLoadMRInsuranceContacts", classes = {
			@ConstructorResult(targetClass = CallLogDispositionLoadMR.class,columns = {
			@ColumnResult(name = "isCallinitaited"), 
			@ColumnResult(name = "creName"),
			@ColumnResult(name = "campaignName"),
			@ColumnResult(name = "call_date",type = java.sql.Date.class),
			@ColumnResult(name = "customerName"),
			@ColumnResult(name = "mobileNumber"),
			@ColumnResult(name = "vehicle_RegNo"),
			@ColumnResult(name = "appointmentDate",type = java.sql.Date.class), 
			@ColumnResult(name = "appointmentTime"),
			@ColumnResult(name = "renewalType"),
			@ColumnResult(name = "appointmentstatus"),
			@ColumnResult(name = "insurance_agent"),
			@ColumnResult(name = "Last_disposition"),
			@ColumnResult(name = "reason"),
			@ColumnResult(name = "followUpDate"),
			@ColumnResult(name = "followUpTime"),
			@ColumnResult(name = "policyDueDate", type = java.sql.Date.class),
			@ColumnResult(name = "vehicle_id"),
			@ColumnResult(name = "customer_id")
	

	})

	}),
	@SqlResultSetMapping(name = "CallLogDispositionLoadMRInsuranceNonContacts", classes = {
			@ConstructorResult(targetClass = CallLogDispositionLoadMR.class,columns = {
			@ColumnResult(name = "isCallinitaited"), 
			@ColumnResult(name = "creName"),
			@ColumnResult(name = "droppedCount",type = Long.class),
			@ColumnResult(name = "campaignName"),
			@ColumnResult(name = "call_date",type = java.sql.Date.class),
			@ColumnResult(name = "customerName"),
			@ColumnResult(name = "mobileNumber"),
			@ColumnResult(name = "vehicle_RegNo"),
			@ColumnResult(name = "appointmentDate",type = java.sql.Date.class), 
			@ColumnResult(name = "appointmentTime"),
			@ColumnResult(name = "renewalType"),
			@ColumnResult(name = "appointmentstatus"),
			@ColumnResult(name = "insurance_agent"),
			@ColumnResult(name = "Last_disposition"),
			@ColumnResult(name = "reason"),
			@ColumnResult(name = "followUpDate"),
			@ColumnResult(name = "followUpTime"),
			@ColumnResult(name = "policyDueDate", type = java.sql.Date.class),
			@ColumnResult(name = "vehicle_id"),
			@ColumnResult(name = "customer_id")
	

	})

	}),
	@SqlResultSetMapping(name = "CallLogAjaxLoadMRInsuranceAssigned", classes = { 
			@ConstructorResult(targetClass = CallLogAjaxLoadMR.class, columns = { 
					@ColumnResult(name = "creName"), 
					@ColumnResult(name = "campaign"), 
					@ColumnResult(name = "customerName"), 
					@ColumnResult(name = "phone"), 
					@ColumnResult(name = "regNo"), 
					@ColumnResult(name = "chassisNo"), 
					@ColumnResult(name = "policyDueMonth"), 
					@ColumnResult(name = "policyDueDate", type = java.sql.Date.class), 
					@ColumnResult(name = "nextRenewalType"), 
					@ColumnResult(name = "lastInsuranceCompany"),
					@ColumnResult(name = "insuranceassigned_id",type = Long.class),
					@ColumnResult(name = "customer_id"),
					@ColumnResult(name = "vehicle_vehicle_id",type = Long.class)
				

	})

	}),

	@SqlResultSetMapping(name = "CallLogDispositionLoadMRPSFContacts", classes = {
			@ConstructorResult(targetClass = CallLogDispositionLoadMR.class,columns = {
			@ColumnResult(name = "campaignName"), 
			@ColumnResult(name = "creName"),
			@ColumnResult(name = "callinteraction_id"),
			@ColumnResult(name = "customer_name"),
			@ColumnResult(name = "Mobile_number"),
			@ColumnResult(name = "vehicle_RegNo"),
			@ColumnResult(name = "followUpDate"), 
			@ColumnResult(name = "followUpTime"),
			@ColumnResult(name = "call_date", type = java.sql.Date.class),
			@ColumnResult(name = "RONumber"),
			@ColumnResult(name = "ROdate", type = java.sql.Date.class),
			@ColumnResult(name = "BillDate", type = java.sql.Date.class),
			@ColumnResult(name = "Servey_date", type = java.sql.Date.class),
			@ColumnResult(name = "vehicle_id"),
			@ColumnResult(name = "model"),
			@ColumnResult(name = "customer_id"),
			@ColumnResult(name = "psfAppointmentDate", type = java.sql.Date.class),
			@ColumnResult(name = "psfAppointmentTime")
	

	})

	}),
	@SqlResultSetMapping(name = "CallLogDispositionLoadMRPSFNONContacts", classes = {
			@ConstructorResult(targetClass = CallLogDispositionLoadMR.class,columns = {
			@ColumnResult(name = "campaignName"), 
			@ColumnResult(name = "CREName"),
			@ColumnResult(name = "callinteraction_id"),
			@ColumnResult(name = "customer_name"),
			@ColumnResult(name = "Mobile_number"),
			@ColumnResult(name = "vehicle_RegNo"),
			@ColumnResult(name = "followUpDate"), 
			@ColumnResult(name = "followUpTime"),
			@ColumnResult(name = "call_date", type = java.sql.Date.class),
			@ColumnResult(name = "RONumber"),
			@ColumnResult(name = "ROdate", type = java.sql.Date.class),
			@ColumnResult(name = "BillDate", type = java.sql.Date.class),
			@ColumnResult(name = "Servey_date", type = java.sql.Date.class),
			@ColumnResult(name = "vehicle_id"),
			@ColumnResult(name = "model"),
			@ColumnResult(name = "customer_id"),
			@ColumnResult(name = "Last_disposition")	
		})

	}),
	@SqlResultSetMapping(name = "CallHistory", classes = {
			@ConstructorResult(targetClass = CallInteractionHistory.class, columns = {
					@ColumnResult(name = "Location",type=String.class), 
					@ColumnResult(name = "wyzUser_id",type=String.class),
					@ColumnResult(name = "Cre_Name",type=String.class), 
					@ColumnResult(name = "callMadeDateAndTime",type=String.class),
					@ColumnResult(name = "callTime",type=String.class),
					@ColumnResult(name = "callDate",type=String.class),
					@ColumnResult(name = "preffered_Contact_number",type=String.class),
					@ColumnResult(name = "callType",type=String.class),
					@ColumnResult(name = "Customer_name",type=String.class),
					@ColumnResult(name = "DOb",type=String.class),
					@ColumnResult(name = "office_address",type=String.class),
					@ColumnResult(name = "residential_address",type=String.class),
					@ColumnResult(name = "permenant_address",type=String.class),
					@ColumnResult(name = "Email_id",type=String.class),
					@ColumnResult(name = "customer_category",type=String.class),
					@ColumnResult(name = "customer_city",type=String.class),
					@ColumnResult(name = "callDuration",type=String.class),
					@ColumnResult(name = "customer_remarks",type=String.class),
					@ColumnResult(name = "Veh_Reg_no",type=String.class),
					@ColumnResult(name = "chassisNo",type=String.class),
					@ColumnResult(name = "Model",type=String.class),
					@ColumnResult(name = "fueltype",type=String.class),
					@ColumnResult(name = "Variant",type=String.class),
					@ColumnResult(name = "last_serviceDate",type=String.class),
					@ColumnResult(name = "lastServiceType",type=String.class),
					@ColumnResult(name = "nextServicedate",type=String.class),
					@ColumnResult(name = "nextServicetype",type=String.class),
					@ColumnResult(name = "forecastLogic",type=String.class),
					@ColumnResult(name = "previous_disposition",type=String.class),
					@ColumnResult(name = "primary_disposition",type=String.class),
					@ColumnResult(name = "secondary_dispostion", type=String.class),
					@ColumnResult(name = "Tertiary_disposition",type=String.class),
					@ColumnResult(name = "serviceType",type=String.class),
					@ColumnResult(name = "psf_status",type=String.class),
					@ColumnResult(name = "Call_interaction_id",type = Long.class),
					@ColumnResult(name = "sr_disposition_id",type = Long.class),
					@ColumnResult(name = "calling_data_type",type=String.class),
					@ColumnResult(name = "typeofpickup",type=String.class),
					@ColumnResult(name = "fromtimeofpick",type=String.class),
					@ColumnResult(name = "upto",type=String.class),
					@ColumnResult(name = "driver",type=String.class),
					@ColumnResult(name = "service_advisor",type=String.class),
					@ColumnResult(name = "upselltype",type=String.class),
					@ColumnResult(name = "assigned_date",type = java.sql.Date.class),
					@ColumnResult(name = "isCallinitaited",type=String.class)
			})

	}),
	@SqlResultSetMapping(name = "HistoryCubeReport", classes = {
			@ConstructorResult(targetClass = CallInteractionHistoryCubeReport.class, columns = {
					@ColumnResult(name = "assigned_date",type=String.class), 
					@ColumnResult(name = "call_campaign",type=String.class),
					@ColumnResult(name = "call_type",type=String.class), 
					@ColumnResult(name = "call_date",type=java.sql.Date.class),
					@ColumnResult(name = "call_Time",type=String.class),
					@ColumnResult(name = "CRE_Name",type=String.class),
					@ColumnResult(name = "chassis_No",type=String.class),
					@ColumnResult(name = "Veh_Reg_No",type=String.class),
					@ColumnResult(name = "Model",type=String.class),
					@ColumnResult(name = "Customer_Name",type=String.class),
					@ColumnResult(name = "Email_ID",type=String.class),
					@ColumnResult(name = "Preffered_Contact_Number",type=String.class),
					@ColumnResult(name = "Location_DB",type=String.class),
					@ColumnResult(name = "Address_office",type=String.class),
					@ColumnResult(name = "Address_Permanenent",type=String.class),
					@ColumnResult(name = "Address_residential",type=String.class),
					@ColumnResult(name = "forecast_Logic",type=String.class),
					@ColumnResult(name = "ForecastService_Date",type=String.class),
					@ColumnResult(name = "ForecastService_Type",type=String.class),
					@ColumnResult(name = "IsCall_Initiated",type=String.class),
					@ColumnResult(name = "Ring_Time",type=String.class),
					@ColumnResult(name = "call_duration",type=String.class),
					@ColumnResult(name = "Primary_Disposition",type=String.class),
					@ColumnResult(name = "Secondary_Dispostion",type=String.class),
					@ColumnResult(name = "Tertiary_Disposition",type=String.class),
					@ColumnResult(name = "followUp_Date",type=String.class),
					@ColumnResult(name = "followUp_Time",type=String.class),
					@ColumnResult(name = "No_Service_Reason",type=String.class),
					@ColumnResult(name = "fourth_Dispoition",type=String.class),
					@ColumnResult(name = "Already_Serviced_Date",type=String.class),
					@ColumnResult(name = "Verified_with_DMS", type=String.class),
					@ColumnResult(name = "Authorised_workshop_or_Not",type=String.class),
					@ColumnResult(name = "Already_ServicedType",type=String.class),
					@ColumnResult(name = "Already_Serviced_Dealer_Name",type=String.class),
					@ColumnResult(name = "Already_Serviced_Mileage_recorded",type = String.class),
					@ColumnResult(name = "Transferred_City",type = String.class),
					@ColumnResult(name = "pinCode_City",type=String.class),
					@ColumnResult(name = "Lead_Source_For_incoming_call",type=String.class),
					@ColumnResult(name = "current_Mileage",type=String.class),
					@ColumnResult(name = "expected_Visit_Date",type=String.class),
					@ColumnResult(name = "Remarks_1",type=String.class),
					@ColumnResult(name = "Remarks_2",type=String.class),
					@ColumnResult(name = "Remarks_3",type=String.class),
					@ColumnResult(name = "Remarks_nonContact_Others",type = String.class),
					@ColumnResult(name = "Booking_Status",type=String.class),
					@ColumnResult(name = "Reshedule_Status",type=String.class),
					@ColumnResult(name = "Booking_Scheduled_Date",type=String.class),
					@ColumnResult(name = "Booking_Scheduled_Time",type=String.class),
					@ColumnResult(name = "Booking_Service_Type",type=String.class),
					@ColumnResult(name = "Assigned_SA",type=String.class),
					@ColumnResult(name = "PickUp_Option",type=String.class),
					@ColumnResult(name = "Pickup_Driver",type=String.class),
					@ColumnResult(name = "PickUpTime_From",type=String.class),
					@ColumnResult(name = "PickUpTime_Upto",type=String.class),
					@ColumnResult(name = "PickUpAddress",type=String.class),
					@ColumnResult(name = "Booked_Workshop",type=String.class),
					@ColumnResult(name = "Reported_Status",type=Boolean.class),
					@ColumnResult(name = "Reported_Date",type=String.class),
					@ColumnResult(name = "Reported_RO",type=String.class),
					@ColumnResult(name = "Last_SAName",type=String.class),
					@ColumnResult(name = "last_service_Date",type=String.class),
					@ColumnResult(name = "last_Service_Type",type=String.class),
					@ColumnResult(name = "Is_Upsell_Done",type=String.class),
					@ColumnResult(name = "customer_category",type=String.class),
					@ColumnResult(name = "Offer_Validity",type=String.class),
					@ColumnResult(name = "Offer_Details",type=String.class),
					@ColumnResult(name = "Offer_Code",type=String.class),
					@ColumnResult(name = "Offer_Benefit",type=String.class),
					@ColumnResult(name = "Offer_Condition",type=String.class),
					@ColumnResult(name = "Is_Feedback_orCompliant",type=String.class),
					@ColumnResult(name = "Complaint_For_Dept",type=String.class),
					@ColumnResult(name = "Compliant_Remark",type=String.class)
				})

	}),
	@SqlResultSetMapping(name = "CallLogAjaxLoadMRPSFAssigned", classes = {
			@ConstructorResult(targetClass = CallLogAjaxLoadMR.class, columns = {
               
					@ColumnResult(name = "campaignName"),
					@ColumnResult(name = "CREName"),
					@ColumnResult(name = "customer_name"),
					@ColumnResult(name = "Mobile_number"),
					@ColumnResult(name = "vehicle_RegNo"),
					@ColumnResult(name = "model"),
					@ColumnResult(name = "RONumber"),
					@ColumnResult(name = "ROdate",type = java.sql.Date.class),
					@ColumnResult(name = "BillDate",type = java.sql.Date.class),
					@ColumnResult(name = "vehicle_id"), 
					@ColumnResult(name = "customer_id"),
					@ColumnResult(name = "Category"),
					@ColumnResult(name = "assigned_intercation_id")
					})

	}),
		@SqlResultSetMapping(name = "ServiceAdvisorIdName", classes = {
				@ConstructorResult(targetClass = ServiceAdvisorIdName.class, columns = {
						@ColumnResult(name = "advisorId", type = Long.class),
						@ColumnResult(name = "advisorName", type = String.class),
						@ColumnResult(name = "priority", type = Long.class),
						@ColumnResult(name = "date", type = java.sql.Date.class),
						@ColumnResult(name = "workshopId", type = Long.class)

				}) }),
		
		@SqlResultSetMapping(name = "InsuranceAgentIdName", classes = {
				@ConstructorResult(targetClass = InsuranceAgentIdName.class, columns = {
						@ColumnResult(name = "advisorId", type = Long.class),
						@ColumnResult(name = "advisorName", type = String.class),
						@ColumnResult(name = "priority", type = Long.class),
						@ColumnResult(name = "date", type = java.sql.Date.class)

				}) }),

		@SqlResultSetMapping(name = "CallLogDispositionLoad", classes = {
				@ConstructorResult(targetClass = CallLogDispositionLoad.class, columns = {
						@ColumnResult(name = "customer_name"), @ColumnResult(name = "vehicle_RegNo"),
						@ColumnResult(name = "Mobile_number"), @ColumnResult(name = "callinteraction_id"),
						@ColumnResult(name = "followupdate"), @ColumnResult(name = "followuptime"),
						@ColumnResult(name = "Last_disposition"), @ColumnResult(name = "scheduled_date"),
						@ColumnResult(name = "scheduled_time"), @ColumnResult(name = "reason"),
						@ColumnResult(name = "vehicle_id"), @ColumnResult(name = "customer_id"),
						@ColumnResult(name = "duedate"),@ColumnResult(name = "campaignName"),
						@ColumnResult(name = "call_date" , type = java.sql.Date.class)

				})

		}),
		
		@SqlResultSetMapping(name = "CallLogAjaxLoadPSFScheduled", classes = {
				@ConstructorResult(targetClass = CallLogAjaxLoad.class, columns = { 
						
						@ColumnResult(name = "customer_name"),
						@ColumnResult(name = "Mobile_number"),
						@ColumnResult(name = "vehicle_RegNo"),
						@ColumnResult(name = "model"),
						@ColumnResult(name = "RONumber"), 
						@ColumnResult(name = "ROdate",type = java.sql.Date.class),
						@ColumnResult(name = "BillDate", type= java.sql.Date.class),
						@ColumnResult(name = "vehicle_id"),
						@ColumnResult(name = "customer_id"),
						@ColumnResult(name = "category"),
						@ColumnResult(name = "assigned_intercation_id")
					}) 
				}),
		@SqlResultSetMapping(name = "PSFCallsFollowUpCompleteSurveyAppointmentsIncompletedSurvey", classes = {
				@ConstructorResult(targetClass = CallLogDispositionLoad.class, columns = {
						@ColumnResult(name = "callinteraction_id"),
						@ColumnResult(name = "customer_name"),
						@ColumnResult(name = "Mobile_number"),
						@ColumnResult(name = "vehicle_RegNo"), 
						@ColumnResult(name = "followUpDate"),
						@ColumnResult(name = "followuptime"), 
						@ColumnResult(name = "call_date"),
						@ColumnResult(name = "RONumber"), 
						@ColumnResult(name = "ROdate",type = java.sql.Date.class),
						@ColumnResult(name = "BillDate",type = java.sql.Date.class), 
						@ColumnResult(name = "Servey_date",type = java.sql.Date.class),
						@ColumnResult(name = "vehicle_id"), 
						@ColumnResult(name = "model"), 
						@ColumnResult(name = "customer_id"),
						@ColumnResult(name = "psfAppointmentDate",type = java.sql.Date.class),
						@ColumnResult(name = "psfAppointmentTime")

				})

		}),
		@SqlResultSetMapping(name = "CallLogDispositionLoadPSFNonContactDropped", classes = {
				@ConstructorResult(targetClass = CallLogDispositionLoad.class, columns = {
						@ColumnResult(name = "callinteraction_id"),
						@ColumnResult(name = "customer_name"),
						@ColumnResult(name = "Mobile_number"),
						@ColumnResult(name = "vehicle_RegNo"), 
						@ColumnResult(name = "followUpDate"),
						@ColumnResult(name = "followuptime"), 
						@ColumnResult(name = "call_date"),
						@ColumnResult(name = "RONumber"),
						@ColumnResult(name = "ROdate",type = java.sql.Date.class),
						@ColumnResult(name = "BillDate",type = java.sql.Date.class),
						@ColumnResult(name = "Servey_date",type = java.sql.Date.class),
						@ColumnResult(name = "vehicle_id"),
						@ColumnResult(name = "model"),
						@ColumnResult(name = "customer_id")

				})
		}),@SqlResultSetMapping(name = "CallInteractionHistory", classes = { 
				@ConstructorResult(targetClass = CallInteractionHistory.class, columns = { 
						@ColumnResult(name = "Location"), @ColumnResult(name = "wyzUser_id"), 
						@ColumnResult(name = "Cre_Name"), @ColumnResult(name = "callMadeDateAndTime"),
						@ColumnResult(name = "callTime"), @ColumnResult(name = "callDate"), 
						@ColumnResult(name = "preffered_Contact_number"), @ColumnResult(name = "callType"),
						@ColumnResult(name = "Customer_name"), @ColumnResult(name = "DOb"),
						@ColumnResult(name = "office_address"), @ColumnResult(name = "residential_address"),
						@ColumnResult(name = "permenant_address"), @ColumnResult(name = "Email_id"),
						@ColumnResult(name = "customer_category"), @ColumnResult(name = "customer_city"),
						@ColumnResult(name = "callDuration"), @ColumnResult(name = "Veh_Reg_no"), 
						@ColumnResult(name = "chassisNo"), @ColumnResult(name = "Model"),
						@ColumnResult(name = "fueltype"), @ColumnResult(name = "Variant"), 
						@ColumnResult(name = "last_serviceDate"), @ColumnResult(name = "lastServiceType"), 
						@ColumnResult(name = "nextServicedate"), @ColumnResult(name = "nextServicetype"), 
						@ColumnResult(name = "forecastLogic"), @ColumnResult(name = "previous_disposition"),
						@ColumnResult(name = "primary_disposition"), @ColumnResult(name = "secondary_dispostion"),
						@ColumnResult(name = "Tertiary_disposition"), @ColumnResult(name = "serviceType"),
						@ColumnResult(name = "psf_status"), @ColumnResult(name = "media_avl")

		})

		}), @SqlResultSetMapping(name = "CallLogDispositionLoadMR", classes = {
				@ConstructorResult(targetClass = CallLogDispositionLoadMR.class,columns = {
				@ColumnResult(name = "customer_name"), @ColumnResult(name = "vehicle_RegNo"),
				@ColumnResult(name = "Mobile_number"), @ColumnResult(name = "callinteraction_id"),
				@ColumnResult(name = "followUpDate"), @ColumnResult(name = "followUpTime"), 
				@ColumnResult(name = "Last_disposition"), @ColumnResult(name = "scheduled_date"), 
				@ColumnResult(name = "scheduled_time"), @ColumnResult(name = "reason"),
				@ColumnResult(name = "vehicle_id"), @ColumnResult(name = "customer_id"), 
				@ColumnResult(name = "duedate"), @ColumnResult(name = "userName"), 
				@ColumnResult(name = "mediaFileLob"),@ColumnResult(name = "campaignName"),
				@ColumnResult(name = "call_date" , type = java.sql.Date.class),
				@ColumnResult(name = "isCallinitaited")		

		})

		}),@SqlResultSetMapping(name = "CallLogDispositionLoadMROthers", classes = {
				@ConstructorResult(targetClass = CallLogDispositionLoadMR.class, columns = {
						@ColumnResult(name = "call_date",  type = java.sql.Date.class), @ColumnResult(name = "customer_name"),
						@ColumnResult(name = "vehicle_RegNo"), @ColumnResult(name = "username"),
						@ColumnResult(name = "Media_file"),@ColumnResult(name = "dailedNoIs"),
						@ColumnResult(name = "callinteraction_id")})

		}),
		@SqlResultSetMapping(name = "CallLogAjaxLoadMR", classes = { 
				@ConstructorResult(targetClass = CallLogAjaxLoadMR.class, columns = { 
						@ColumnResult(name = "customer_name"), 
						@ColumnResult(name = "vehicle_RegNo"), 
						@ColumnResult(name = "veh_model"), 
						@ColumnResult(name = "category"), 
						@ColumnResult(name = "Loyalty"), 
						@ColumnResult(name = "duedate"), 
						@ColumnResult(name = "due_type"), 
						@ColumnResult(name = "forecast_type"), 
						@ColumnResult(name = "last_psfstatus"), 
						@ColumnResult(name = "DND_status"),
						@ColumnResult(name = "campaignName"),
						@ColumnResult(name = "complaints_count"),
						@ColumnResult(name = "customer_id"),
						@ColumnResult(name = "vehical_Id"), 
						@ColumnResult(name = "wyzUser_id"), 
						@ColumnResult(name = "assigned_intercation_id"), 
						@ColumnResult(name = "userName")

		})

		}),
		@SqlResultSetMapping(name = "ServiceBookedResultList", classes = { @ConstructorResult(targetClass = ServiceBookedResultList.class,
		columns = { 
			@ColumnResult(name = "status"),@ColumnResult(name = "booking_reg_date" , type = java.sql.Date.class), @ColumnResult(name = "booked_date" ,type = java.sql.Date.class), @ColumnResult(name = "workshop"), @ColumnResult(name = "serviceBookedType"),
			@ColumnResult(name = "customer_name"), @ColumnResult(name = "mobile_number"),
			@ColumnResult(name = "address"), @ColumnResult(name = "veh_model"),
			@ColumnResult(name = "mode_of_visit"), @ColumnResult(name = "pickup_time"),
			@ColumnResult(name = "driver_name"), @ColumnResult(name = "pickup_address"),
			@ColumnResult(name = "CRE_name"), @ColumnResult(name = "advisor_name") , 
			@ColumnResult(name = "serviceBookedId" , type = Long.class), @ColumnResult(name = "callInteraction_id" , type = Long.class),
			@ColumnResult(name = "vehicle_vehicle_id" , type = Long.class),
			@ColumnResult(name = "customer_id" , type = Long.class)



	})

	}),
		@SqlResultSetMapping(name = "InsuranceHistoryReport", classes = { @ConstructorResult(targetClass = InsuranceHistoryReport.class,
		columns = { 
			
			@ColumnResult(name = "creName"),
			@ColumnResult(name = "callDate", type = java.sql.Date.class), 
			@ColumnResult(name = "callTime"),
			@ColumnResult(name = "isCallinitaited"), 
			@ColumnResult(name = "callType"),
			@ColumnResult(name = "callDuration"), 
			@ColumnResult(name = "ringTime"),
			@ColumnResult(name = "campaignTYpe"), 
			@ColumnResult(name = "chassisNo"),
			@ColumnResult(name = "model"), 
			@ColumnResult(name = "variant") , 
			@ColumnResult(name = "saleDate"), 
			@ColumnResult(name = "vehicleRegno"),
			@ColumnResult(name = "customerName"),
			@ColumnResult(name = "preffered_address"),
			@ColumnResult(name = "prefferedPhoneNumber"),
			@ColumnResult(name = "PrimaryDisposition"),
			@ColumnResult(name = "SecondaryDisposition"),
			@ColumnResult(name = "coverNoteNo"),
			@ColumnResult(name = "lastRenewedLocation"),
			@ColumnResult(name = "lastRenewalDate"),
			@ColumnResult(name = "premimum"),
			@ColumnResult(name = "renewalDoneBy"),
			@ColumnResult(name = "Tertiary_disposition"),
			@ColumnResult(name = "followUpDate" , type = java.sql.Date.class),
			@ColumnResult(name = "followUpTime"),
			@ColumnResult(name = "addOnsPrefered_OtherOptionsData"),
			@ColumnResult(name = "addOnsPrefered_PopularOptionsData"),
			@ColumnResult(name = "remarksOfFb"),
			@ColumnResult(name = "departmentForFB"),
			@ColumnResult(name = "addressOfVisit"),
			@ColumnResult(name = "appointeeName"),
			@ColumnResult(name = "appointmentDate" , type = java.sql.Date.class),
			@ColumnResult(name = "appointmentFromTime"),
			@ColumnResult(name = "dsa"),
			@ColumnResult(name = "insuranceAgentData"),
			@ColumnResult(name = "insuranceCompany"),
			@ColumnResult(name = "nomineeName"),
			@ColumnResult(name = "nomineeRelationWithOwner"),
			@ColumnResult(name = "premiumwithTax"),
			@ColumnResult(name = "renewalMode"),
			@ColumnResult(name = "renewalType"),
			@ColumnResult(name = "typeOfPickup"),
			@ColumnResult(name = "PremiumYes"),
			@ColumnResult(name = "comments"),
			@ColumnResult(name = "reason"),
			@ColumnResult(name = "cremanager"),
			@ColumnResult(name = "updatedDate", type = java.sql.Date.class),
			@ColumnResult(name = "reasonForSNR")
			
		})
	})


})

@Entity
public class CallInteraction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@Temporal(TemporalType.TIMESTAMP)
	public java.util.Date callMadeDateAndTime;

	public boolean isComplaintCall;

	public boolean isDriverCall;
	
	@Column(length = 30)
	public String dailedNoIs;

	@Column(length = 30)
	public String dealerCode;
	@Column(length = 30)
	public String agentName;
	@Column(length = 30)
	public String callDate;
	@Column(length = 30)
	public int callTypePicId;
	@Column(length = 30)
	public String callTime;

	@Lob
	public byte[] mediaFileLob;

	public long callCount;
	@Column(length = 10)
	public String callDuration;
	
	@Column(length = 10)
	public String ringTime;
	
	@Column(length = 20)
	public String callType;
	@Column(length = 30)
	public String latitude;
	@Column(length = 30)
	public String longitude;

	public String mediaFile;
	@Column(length = 70)
	public String filePath;
	@Column(length = 30)
	public String makeCallFrom;
	public long droppedCount;
	public int uniqueidForCallSync;
	@Column(length = 30)
	public String firebaseKey;

	@Column(length = 50)
	public String isCallinitaited;
	

	public boolean dailedStatus;

	public boolean isDailedStatus() {
		return dailedStatus;
	}

	public void setDailedStatus(boolean dailedStatus) {
		this.dailedStatus = dailedStatus;
	}

	public String getIsCallinitaited() {
		return isCallinitaited;
	}

	public void setIsCallinitaited(String isCallinitaited) {
		this.isCallinitaited = isCallinitaited;
	}

	@Column(length = 50)
	public String serviceAdvisorfirebaseKey;

	@Column(length = 10)
	public String synchedToFirebase;

	@ManyToOne
	public Customer customer;

	@ManyToOne
	public WyzUser wyzUser;

	@ManyToOne
	public Vehicle vehicle;
	
	@ManyToOne
	public Campaign campaign;

	// @ManyToOne(cascade = CascadeType.ALL)
	// public Insurance insurance;

	@OneToOne(mappedBy = "callInteraction")
	public SRDisposition srdisposition;

	@OneToOne(mappedBy = "callInteraction")
	public InsuranceDisposition insuranceDisposition;

	@ManyToOne(cascade = CascadeType.ALL)
	public ServiceBooked serviceBooked;

	@ManyToOne
	public AssignedInteraction assignedInteraction;

	@ManyToOne
	public InsuranceAssignedInteraction insuranceAssignedInteraction;
	
	@ManyToOne(cascade = CascadeType.ALL)
	public AppointmentBooked appointmentBooked;


	@ManyToOne
	public PSFAssignedInteraction psfAssignedInteraction;

	@OneToOne(mappedBy = "callInteraction")
	public PSFInteraction psfdisposition;

	

	public AssignedInteraction getAssignedInteraction() {
		return assignedInteraction;
	}

	public void setAssignedInteraction(AssignedInteraction assignedInteraction) {
		this.assignedInteraction = assignedInteraction;
	}

	public String getSynchedToFirebase() {
		return synchedToFirebase;
	}

	public void setSynchedToFirebase(String synchedToFirebase) {
		this.synchedToFirebase = synchedToFirebase;
	}

	public ServiceBooked getServiceBooked() {
		return serviceBooked;
	}

	public void setServiceBooked(ServiceBooked serviceBooked) {
		this.serviceBooked = serviceBooked;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDroppedCount() {
		return droppedCount;
	}

	public void setDroppedCount(long droppedCount) {
		this.droppedCount = droppedCount;
	}

	public java.util.Date getCallMadeDateAndTime() {
		return callMadeDateAndTime;
	}

	public void setCallMadeDateAndTime(java.util.Date callMadeDateAndTime) {
		this.callMadeDateAndTime = callMadeDateAndTime;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public String getCallDate() {
		return callDate;
	}

	public void setCallDate(String callDate) {
		this.callDate = callDate;
	}

	public int getCallTypePicId() {
		return callTypePicId;
	}

	public void setCallTypePicId(int callTypePicId) {
		this.callTypePicId = callTypePicId;
	}

	public String getCallTime() {
		return callTime;
	}

	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}

	public byte[] getMediaFileLob() {
		return mediaFileLob;
	}

	public void setMediaFileLob(byte[] mediaFileLob) {
		this.mediaFileLob = mediaFileLob;
	}

	public long getCallCount() {
		return callCount;
	}

	public void setCallCount(long callCount) {
		this.callCount = callCount;
	}

	public String getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getMediaFile() {
		return mediaFile;
	}

	public void setMediaFile(String mediaFile) {
		this.mediaFile = mediaFile;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getMakeCallFrom() {
		return makeCallFrom;
	}

	public void setMakeCallFrom(String makeCallFrom) {
		this.makeCallFrom = makeCallFrom;
	}

	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	// public Insurance getInsurance() {
	// return insurance;
	// }
	//
	// public void setInsurance(Insurance insurance) {
	// this.insurance = insurance;
	// }

	public SRDisposition getSrdisposition() {
		return srdisposition;
	}

	public void setSrdisposition(SRDisposition srdisposition) {
		this.srdisposition = srdisposition;
	}

	public int getUniqueidForCallSync() {
		return uniqueidForCallSync;
	}

	public void setUniqueidForCallSync(int uniqueidForCallSync) {
		this.uniqueidForCallSync = uniqueidForCallSync;
	}

	public String getFirebaseKey() {
		return firebaseKey;
	}

	public void setFirebaseKey(String firebaseKey) {
		this.firebaseKey = firebaseKey;
	}

	public boolean isComplaintCall() {
		return isComplaintCall;
	}

	public void setComplaintCall(boolean isComplaintCall) {
		this.isComplaintCall = isComplaintCall;
	}

	public boolean isDriverCall() {
		return isDriverCall;
	}

	public void setDriverCall(boolean isDriverCall) {
		this.isDriverCall = isDriverCall;
	}

	public PSFInteraction getPsfdisposition() {
		return psfdisposition;
	}

	public void setPsfdisposition(PSFInteraction psfdisposition) {
		this.psfdisposition = psfdisposition;
	}

	public String getServiceAdvisorfirebaseKey() {
		return serviceAdvisorfirebaseKey;
	}

	public void setServiceAdvisorfirebaseKey(String serviceAdvisorfirebaseKey) {
		this.serviceAdvisorfirebaseKey = serviceAdvisorfirebaseKey;
	}

	public PSFAssignedInteraction getPsfAssignedInteraction() {
		return psfAssignedInteraction;
	}

	public void setPsfAssignedInteraction(PSFAssignedInteraction psfAssignedInteraction) {
		this.psfAssignedInteraction = psfAssignedInteraction;
	}

	public InsuranceDisposition getInsuranceDisposition() {
		return insuranceDisposition;
	}

	public void setInsuranceDisposition(InsuranceDisposition insuranceDisposition) {
		this.insuranceDisposition = insuranceDisposition;
	}

	public InsuranceAssignedInteraction getInsuranceAssignedInteraction() {
		return insuranceAssignedInteraction;
	}

	public void setInsuranceAssignedInteraction(InsuranceAssignedInteraction insuranceAssignedInteraction) {
		this.insuranceAssignedInteraction = insuranceAssignedInteraction;
	}

	public String getDailedNoIs() {
		return dailedNoIs;
	}

	public void setDailedNoIs(String dailedNoIs) {
		this.dailedNoIs = dailedNoIs;
	}

	public String getRingTime() {
		return ringTime;
	}

	public void setRingTime(String ringTime) {
		this.ringTime = ringTime;
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	public AppointmentBooked getAppointmentBooked() {
		return appointmentBooked;
	}

	public void setAppointmentBooked(AppointmentBooked appointmentBooked) {
		this.appointmentBooked = appointmentBooked;
	}

	
	
	

}
