package models;

public class CallSummary {
	public String agentName;
	public long allocatedCallsCount;
	public long pendingCallsCount;
	public long contactsCallsCount;
	public long serviceBookedCount;
	public long followUpRequiredCount;
	public long serviceNotRequiredCount;
	public long noncontactsCallsCount;
	public long ringingNoResponseCount;
	public long switchedOffCount;
	public long busyCount;
	public long notReachableCount;
	public long wrongNumberCount;

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public long getAllocatedCallsCount() {
		return allocatedCallsCount;
	}

	public void setAllocatedCallsCount(long allocatedCallsCount) {
		this.allocatedCallsCount = allocatedCallsCount;
	}

	public long getPendingCallsCount() {
		return pendingCallsCount;
	}

	public void setPendingCallsCount(long pendingCallsCount) {
		this.pendingCallsCount = pendingCallsCount;
	}

	public long getContactsCallsCount() {
		return contactsCallsCount;
	}

	public void setContactsCallsCount(long contactsCallsCount) {
		this.contactsCallsCount = contactsCallsCount;
	}

	public long getServiceBookedCount() {
		return serviceBookedCount;
	}

	public void setServiceBookedCount(long serviceBookedCount) {
		this.serviceBookedCount = serviceBookedCount;
	}

	public long getFollowUpRequiredCount() {
		return followUpRequiredCount;
	}

	public void setFollowUpRequiredCount(long followUpRequiredCount) {
		this.followUpRequiredCount = followUpRequiredCount;
	}

	public long getServiceNotRequiredCount() {
		return serviceNotRequiredCount;
	}

	public void setServiceNotRequiredCount(long serviceNotRequiredCount) {
		this.serviceNotRequiredCount = serviceNotRequiredCount;
	}

	public long getNoncontactsCallsCount() {
		return noncontactsCallsCount;
	}

	public void setNoncontactsCallsCount(long noncontactsCallsCount) {
		this.noncontactsCallsCount = noncontactsCallsCount;
	}

	public long getRingingNoResponseCount() {
		return ringingNoResponseCount;
	}

	public void setRingingNoResponseCount(long ringingNoResponseCount) {
		this.ringingNoResponseCount = ringingNoResponseCount;
	}

	public long getSwitchedOffCount() {
		return switchedOffCount;
	}

	public void setSwitchedOffCount(long switchedOffCount) {
		this.switchedOffCount = switchedOffCount;
	}

	public long getBusyCount() {
		return busyCount;
	}

	public void setBusyCount(long busyCount) {
		this.busyCount = busyCount;
	}

	public long getNotReachableCount() {
		return notReachableCount;
	}

	public void setNotReachableCount(long notReachableCount) {
		this.notReachableCount = notReachableCount;
	}

	public long getWrongNumberCount() {
		return wrongNumberCount;
	}

	public void setWrongNumberCount(long wrongNumberCount) {
		this.wrongNumberCount = wrongNumberCount;
	}

}
