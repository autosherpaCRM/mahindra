/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author W-885
 */
@Entity
public class CallHistoryCube {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	@Column(length = 100) 
	public String  Location;
	
	public long  wyzUser_id;
	
	@Column(length = 100) 
	public String  Cre_Name;
	
	@Column(length = 100) 
	public String  callTime;
	
	@Temporal(TemporalType.DATE) 
	public Date  callDate;
	
	@Column(length = 100) 
	public String  preffered_Contact_number;
	
	@Column(length = 100) 
	public String  callType;
	
	@Column(length = 100) 
	public String  Customer_name;
	
	@Column(length = 100) 
	public String  DOb;
	
	@Column(length = 255) 
	public String  office_address;
	
	@Column(length = 255) 
	public String  residential_address;
	
	@Column(length = 255) 
	public String  permenant_address;
	
	@Column(length = 100) 
	public String  Email_id;
	
	@Column(length = 100) 
	public String  customer_category;
	
	@Column(length = 100) 
	public String  customer_city;
	
	@Column(length = 100) 
	public String  callDuration;
	
	@Column(length = 100) 
	public String  ringtime;
	
	@Column(length = 100) 
	public String  customer_remarks;
	
	@Column(length = 50) 
	public String  Veh_Reg_no;
	
	@Column(length = 50) 
	public String  chassisNo;
	
	@Column(length = 100) 
	public String  Model;
	
	@Column(length = 100) 
	public String  fueltype;
	
	@Column(length = 100) 
	public String  Variant;
	
	@Temporal(TemporalType.DATE) 
	public Date  last_serviceDate;
	
	@Column(length = 100) 
	public String  lastServiceType;
	
	@Temporal(TemporalType.DATE) 
	public Date  nextServicedate;
	
	@Column(length = 100) 
	public String  nextServicetype;
	
	@Column(length = 100) 
	public String  forecastLogic;
	
	@Column(length = 100) 
	public String  previous_disposition;
	
	@Column(length = 100) 
	public String  primary_disposition;
	
	@Column(length = 100)
	public String  secondary_dispostion;
	
	@Column(length = 100) 
	public String  Tertiary_disposition;
	
	@Temporal(TemporalType.DATE) 
	public Date  followUpDate;
	
	@Column(length = 100) 
	public String  followUpTime;
	
	@Column(length = 100) 
	public String  scheduledDateTime;
	
	@Column(length = 100) 
	public String  booking_status;
	
	@Column(length = 100)
	public String  serviceType;
	
	@Column(length = 100)
	public String  psf_status;
	
	
	public long  Call_interaction_id;
	
	 
	public long  sr_disposition_id;
	
	@Column(length = 100) 
	public String  calling_data_type;
	
	@Column(length = 100) 
	public String  typeofpickup;
	
	@Column(length = 100) 
	public String  fromtimeofpick;
	
	@Column(length = 100) 
	public String  upto;
	
	@Column(length = 100) 
	public String  driver;
	
	@Column(length = 100) 
	public String  service_advisor;
	
	@Column(length = 100) 
	public String  upselltype;
	
	 
	public long  assignedInteraction_id;
	
	@Temporal(TemporalType.DATE) 
	public Date  assigned_date;
	
	@Column(length = 100) 
	public String  isCallinitaited;
	
	@Column(length = 100) 
	public String  nonContactsReason;
	
	@Column(length = 100) 
	public String  customerRemarks;
	
	@Column(length = 100) 
	public String  Reason;
	
	
	public boolean  isCallDurationUpdated;
	
	@Temporal(TemporalType.DATE) 
	public Date  updatedDate;
	
	 
	public boolean  Conversion;
	
	@Temporal(TemporalType.DATE)
	public Date  rodate;
	
	@Column(length = 100) 
	public String  ronumber;



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public long getWyzUser_id() {
		return wyzUser_id;
	}

	public void setWyzUser_id(long wyzUser_id) {
		this.wyzUser_id = wyzUser_id;
	}

	public String getCre_Name() {
		return Cre_Name;
	}

	public void setCre_Name(String cre_Name) {
		Cre_Name = cre_Name;
	}

	public String getCallTime() {
		return callTime;
	}

	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}

	public Date getCallDate() {
		return callDate;
	}

	public void setCallDate(Date callDate) {
		this.callDate = callDate;
	}

	public String getPreffered_Contact_number() {
		return preffered_Contact_number;
	}

	public void setPreffered_Contact_number(String preffered_Contact_number) {
		this.preffered_Contact_number = preffered_Contact_number;
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}

	public String getCustomer_name() {
		return Customer_name;
	}

	public void setCustomer_name(String customer_name) {
		Customer_name = customer_name;
	}

	public String getDOb() {
		return DOb;
	}

	public void setDOb(String dOb) {
		DOb = dOb;
	}

	public String getOffice_address() {
		return office_address;
	}

	public void setOffice_address(String office_address) {
		this.office_address = office_address;
	}

	public String getResidential_address() {
		return residential_address;
	}

	public void setResidential_address(String residential_address) {
		this.residential_address = residential_address;
	}

	public String getPermenant_address() {
		return permenant_address;
	}

	public void setPermenant_address(String permenant_address) {
		this.permenant_address = permenant_address;
	}

	public String getEmail_id() {
		return Email_id;
	}

	public void setEmail_id(String email_id) {
		Email_id = email_id;
	}

	public String getCustomer_category() {
		return customer_category;
	}

	public void setCustomer_category(String customer_category) {
		this.customer_category = customer_category;
	}

	public String getCustomer_city() {
		return customer_city;
	}

	public void setCustomer_city(String customer_city) {
		this.customer_city = customer_city;
	}

	public String getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}

	public String getRingtime() {
		return ringtime;
	}

	public void setRingtime(String ringtime) {
		this.ringtime = ringtime;
	}

	public String getCustomer_remarks() {
		return customer_remarks;
	}

	public void setCustomer_remarks(String customer_remarks) {
		this.customer_remarks = customer_remarks;
	}

	public String getVeh_Reg_no() {
		return Veh_Reg_no;
	}

	public void setVeh_Reg_no(String veh_Reg_no) {
		Veh_Reg_no = veh_Reg_no;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public String getModel() {
		return Model;
	}

	public void setModel(String model) {
		Model = model;
	}

	public String getFueltype() {
		return fueltype;
	}

	public void setFueltype(String fueltype) {
		this.fueltype = fueltype;
	}

	public String getVariant() {
		return Variant;
	}

	public void setVariant(String variant) {
		Variant = variant;
	}

	public Date getLast_serviceDate() {
		return last_serviceDate;
	}

	public void setLast_serviceDate(Date last_serviceDate) {
		this.last_serviceDate = last_serviceDate;
	}

	public String getLastServiceType() {
		return lastServiceType;
	}

	public void setLastServiceType(String lastServiceType) {
		this.lastServiceType = lastServiceType;
	}

	public Date getNextServicedate() {
		return nextServicedate;
	}

	public void setNextServicedate(Date nextServicedate) {
		this.nextServicedate = nextServicedate;
	}

	public String getNextServicetype() {
		return nextServicetype;
	}

	public void setNextServicetype(String nextServicetype) {
		this.nextServicetype = nextServicetype;
	}

	public String getForecastLogic() {
		return forecastLogic;
	}

	public void setForecastLogic(String forecastLogic) {
		this.forecastLogic = forecastLogic;
	}

	public String getPrevious_disposition() {
		return previous_disposition;
	}

	public void setPrevious_disposition(String previous_disposition) {
		this.previous_disposition = previous_disposition;
	}

	public String getPrimary_disposition() {
		return primary_disposition;
	}

	public void setPrimary_disposition(String primary_disposition) {
		this.primary_disposition = primary_disposition;
	}

	public String getSecondary_dispostion() {
		return secondary_dispostion;
	}

	public void setSecondary_dispostion(String secondary_dispostion) {
		this.secondary_dispostion = secondary_dispostion;
	}

	public String getTertiary_disposition() {
		return Tertiary_disposition;
	}

	public void setTertiary_disposition(String tertiary_disposition) {
		Tertiary_disposition = tertiary_disposition;
	}

	public Date getFollowUpDate() {
		return followUpDate;
	}

	public void setFollowUpDate(Date followUpDate) {
		this.followUpDate = followUpDate;
	}

	public String getFollowUpTime() {
		return followUpTime;
	}

	public void setFollowUpTime(String followUpTime) {
		this.followUpTime = followUpTime;
	}

	public String getScheduledDateTime() {
		return scheduledDateTime;
	}

	public void setScheduledDateTime(String scheduledDateTime) {
		this.scheduledDateTime = scheduledDateTime;
	}

	public String getBooking_status() {
		return booking_status;
	}

	public void setBooking_status(String booking_status) {
		this.booking_status = booking_status;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getPsf_status() {
		return psf_status;
	}

	public void setPsf_status(String psf_status) {
		this.psf_status = psf_status;
	}

	

	public String getCalling_data_type() {
		return calling_data_type;
	}

	public void setCalling_data_type(String calling_data_type) {
		this.calling_data_type = calling_data_type;
	}

	public String getTypeofpickup() {
		return typeofpickup;
	}

	public void setTypeofpickup(String typeofpickup) {
		this.typeofpickup = typeofpickup;
	}

	public String getFromtimeofpick() {
		return fromtimeofpick;
	}

	public void setFromtimeofpick(String fromtimeofpick) {
		this.fromtimeofpick = fromtimeofpick;
	}

	public String getUpto() {
		return upto;
	}

	public void setUpto(String upto) {
		this.upto = upto;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getService_advisor() {
		return service_advisor;
	}

	public void setService_advisor(String service_advisor) {
		this.service_advisor = service_advisor;
	}

	public String getUpselltype() {
		return upselltype;
	}

	public void setUpselltype(String upselltype) {
		this.upselltype = upselltype;
	}

	

	public Date getAssigned_date() {
		return assigned_date;
	}

	public void setAssigned_date(Date assigned_date) {
		this.assigned_date = assigned_date;
	}

	public String getIsCallinitaited() {
		return isCallinitaited;
	}

	public void setIsCallinitaited(String isCallinitaited) {
		this.isCallinitaited = isCallinitaited;
	}

	public String getNonContactsReason() {
		return nonContactsReason;
	}

	public void setNonContactsReason(String nonContactsReason) {
		this.nonContactsReason = nonContactsReason;
	}

	public String getCustomerRemarks() {
		return customerRemarks;
	}

	public void setCustomerRemarks(String customerRemarks) {
		this.customerRemarks = customerRemarks;
	}

	public String getReason() {
		return Reason;
	}

	public void setReason(String reason) {
		Reason = reason;
	}

	

	public long getCall_interaction_id() {
		return Call_interaction_id;
	}

	public void setCall_interaction_id(long call_interaction_id) {
		Call_interaction_id = call_interaction_id;
	}

	public long getSr_disposition_id() {
		return sr_disposition_id;
	}

	public void setSr_disposition_id(long sr_disposition_id) {
		this.sr_disposition_id = sr_disposition_id;
	}

	public long getAssignedInteraction_id() {
		return assignedInteraction_id;
	}

	public void setAssignedInteraction_id(long assignedInteraction_id) {
		this.assignedInteraction_id = assignedInteraction_id;
	}

	public boolean isCallDurationUpdated() {
		return isCallDurationUpdated;
	}

	public void setCallDurationUpdated(boolean isCallDurationUpdated) {
		this.isCallDurationUpdated = isCallDurationUpdated;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public boolean isConversion() {
		return Conversion;
	}

	public void setConversion(boolean conversion) {
		Conversion = conversion;
	}

	public Date getRodate() {
		return rodate;
	}

	public void setRodate(Date rodate) {
		this.rodate = rodate;
	}

	public String getRonumber() {
		return ronumber;
	}

	public void setRonumber(String ronumber) {
		this.ronumber = ronumber;
	}
}
