package models;

import java.util.List;

import javax.persistence.*;

@Entity
public class Location {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long cityId;
	@Column(length = 30)
	public String name;
	@Column(length = 30)
	public String state;
	@Column(length = 10)
	public String pinCode;

	@Column(length = 10)
	public String locCode;

	public String getLocCode() {
		return locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	@Column(length = 10)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "location")
	public List<Workshop> workshops;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "location")
	public List<Complaint> complaint;

	@OneToMany(mappedBy = "location")
	public List<WyzUser> users;

	@OneToMany(mappedBy = "location")
	public List<Vehicle> vehicles;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "location")
	public List<Campaign> campaigns;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "location")
	public List<ComplaintInteraction> complaintInteractions;

	public List<Campaign> getCampaigns() {
		return campaigns;
	}

	public void setCampaigns(List<Campaign> campaigns) {
		this.campaigns = campaigns;
	}

	public List<Vehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	@OneToMany(mappedBy = "location")
	public List<TaggingUsers> taggingUsers;
	
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "locationList")
	private List<WyzUser> userLocation;
	
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	public Dealer dealer;
	
	public Dealer getDealer() {
		return dealer;
	}

	public void setDealer(Dealer dealer) {
		this.dealer = dealer;
	}
	

	public String upload_id;

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	public List<ComplaintInteraction> getComplaintInteractions() {
		return complaintInteractions;
	}

	public void setComplaintInteractions(List<ComplaintInteraction> complaintInteractions) {
		this.complaintInteractions = complaintInteractions;
	}

	public List<TaggingUsers> getTaggingUsers() {
		return taggingUsers;
	}

	public void setTaggingUsers(List<TaggingUsers> taggingUsers) {
		this.taggingUsers = taggingUsers;
	}

	public List<WyzUser> getUsers() {
		return users;
	}

	public void setUsers(List<WyzUser> users) {
		this.users = users;
	}

	public List<Complaint> getComplaint() {
		return complaint;
	}

	public void setComplaint(List<Complaint> complaint) {
		this.complaint = complaint;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public List<Workshop> getWorkshops() {
		return workshops;
	}

	public void setWorkshops(List<Workshop> workshops) {
		this.workshops = workshops;
	}

	public List<WyzUser> getUserLocation() {
		return userLocation;
	}

	public void setUserLocation(List<WyzUser> userLocation) {
		this.userLocation = userLocation;
	}

}
