package models;

import java.util.Date;
import java.util.List;

public class ListingForm {

	public long wyzUser_Id;
	public long vehicalId;
	public long customer_Id;

	public long singleId;
	public String singleData;
	public String tranferData;
	public String selectedFile;
	public String typeOfsubmit;
	public String assignCall;
	public String cityName;

	public long time_From;
	public long time_To;

	public String time_FromIn;
	public String time_ToIn;

	public String departmentForFB1;
	public String complaintOrFB_TagTo1;
	public String remarksOfFB1;

	public String departmentForFB2;
	public String complaintOrFB_TagTo2;
	public String remarksOfFB2;

	public int uniqueidForCallSync;
	public String serviceScheduledDate;
	public String serviceScheduledTime;

	public String serviceScheduledDateIn;
	public String serviceScheduledTimeIn;
	
	public Date appointmentScheduledDateIn;

	public long campaignType;
	public long campaignName;
	public long campaignTypeToAss;
	public long campaignNameToAss;

	public String cityNameToAss;
	public long workshopToAss;

	public long campaignNamePSF;
	public long campaignPSFToAss;

	public long interactionid;
	public long dispositionHistory;
	public String isCallinitaited;
	
	public long offerSelId;
	public String serviceBookId;
	
	public String commercialVeh;
	public long ageOfVehicleIS;
	
	public String moduleAssign;
	public String getModuleAssign() {
		return moduleAssign;
	}
	public void setModuleAssign(String moduleAssign) {
		this.moduleAssign = moduleAssign;
	}

	
	public List<Long> assignedNoCallId;
	
	public List<Long> getAssignedNoCallId() {
		return assignedNoCallId;
	}

	public void setAssignedNoCallId(List<Long> assignedNoCallId) {
		this.assignedNoCallId = assignedNoCallId;
	}

	public String getCommercialVeh() {
		return commercialVeh;
	}

	public void setCommercialVeh(String commercialVeh) {
		this.commercialVeh = commercialVeh;
	}

	public long getAgeOfVehicleIS() {
		return ageOfVehicleIS;
	}

	public void setAgeOfVehicleIS(long ageOfVehicleIS) {
		this.ageOfVehicleIS = ageOfVehicleIS;
	}

	public String getIsCallinitaited() {
		return isCallinitaited;
	}

	public void setIsCallinitaited(String isCallinitaited) {
		this.isCallinitaited = isCallinitaited;
	}

	public List<String> crename;
	public List<String> all_dispositions;
	public List<String> locations;
	
	public List<Long> callId;
	public List<Long> userId;
	public List<Long> campainId;
	public List<String> remarksList;

	public List<String> getLocations() {
		return locations;
	}

	public void setLocations(List<String> locations) {
		this.locations = locations;
	}

	public List<String> getCrename() {
		return crename;
	}

	public void setCrename(List<String> crename) {
		this.crename = crename;
	}

	public List<String> getAll_dispositions() {
		return all_dispositions;
	}

	public void setAll_dispositions(List<String> all_dispositions) {
		this.all_dispositions = all_dispositions;
	}

	public long getDispositionHistory() {
		return dispositionHistory;
	}

	public void setDispositionHistory(long dispositionHistory) {
		this.dispositionHistory = dispositionHistory;
	}

	public long getInteractionid() {
		return interactionid;
	}

	public void setInteractionid(long interactionid) {
		this.interactionid = interactionid;
	}

	public long getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(long campaignName) {
		this.campaignName = campaignName;
	}

	public long getCampaignNameToAss() {
		return campaignNameToAss;
	}

	public void setCampaignNameToAss(long campaignNameToAss) {
		this.campaignNameToAss = campaignNameToAss;
	}

	public long getCampaignTypeToAss() {
		return campaignTypeToAss;
	}

	public void setCampaignTypeToAss(long campaignTypeToAss) {
		this.campaignTypeToAss = campaignTypeToAss;
	}

	public long getCampaignType() {
		return campaignType;
	}

	public void setCampaignType(long campaignType) {
		this.campaignType = campaignType;
	}

	public String getServiceScheduledDate() {
		return serviceScheduledDate;
	}

	public void setServiceScheduledDate(String serviceScheduledDate) {
		this.serviceScheduledDate = serviceScheduledDate;
	}

	public String getServiceScheduledTime() {
		return serviceScheduledTime;
	}

	public void setServiceScheduledTime(String serviceScheduledTime) {
		this.serviceScheduledTime = serviceScheduledTime;
	}	

	public long getTime_From() {
		return time_From;
	}

	public void setTime_From(long time_From) {
		this.time_From = time_From;
	}

	public long getTime_To() {
		return time_To;
	}

	public void setTime_To(long time_To) {
		this.time_To = time_To;
	}

	public long serviceAdvisorId;
	public long serviceAdvisorIdIn;

	public long driverId;
	public long vehicleId_SB;
	public long workshopId;

	public String fuel_type;

	public String dateOfServiceNonAuth;
	public String ServicedAtOtherDealerRadio;

	public String fromDateToAss;
	public String toDateToAss;

	public String customerCatToAss;
	public String fuelTypeToAss;
	public String serviceTypeToAss;
	public String cust_category;

	public String getTypeOfsubmit() {
		return typeOfsubmit;
	}

	public void setTypeOfsubmit(String typeOfsubmit) {
		this.typeOfsubmit = typeOfsubmit;
	}

	public List<UnAvailability> unavaiabiltyList;

	public List<UnAvailability> getUnavaiabiltyList() {
		return unavaiabiltyList;
	}

	public void setUnavaiabiltyList(List<UnAvailability> unavaiabiltyList) {
		this.unavaiabiltyList = unavaiabiltyList;
	}

	public List<WyzUser> userList;

	public List<WyzUser> getUserList() {
		return userList;
	}

	public String getCust_category() {
		return cust_category;
	}

	public String getFuel_type() {
		return fuel_type;
	}

	public void setFuel_type(String fuel_type) {
		this.fuel_type = fuel_type;
	}

	public void setCust_category(String cust_category) {
		this.cust_category = cust_category;
	}

	public void setUserList(List<WyzUser> userList) {
		this.userList = userList;
	}

	public String getServicedAtOtherDealerRadio() {
		return ServicedAtOtherDealerRadio;
	}

	public void setServicedAtOtherDealerRadio(String ServicedAtOtherDealerRadio) {
		this.ServicedAtOtherDealerRadio = ServicedAtOtherDealerRadio;
	}

	public String getCustomerCatToAss() {
		return customerCatToAss;
	}

	public void setCustomerCatToAss(String customerCatToAss) {
		this.customerCatToAss = customerCatToAss;
	}

	public String getFuelTypeToAss() {
		return fuelTypeToAss;
	}

	public void setFuelTypeToAss(String fuelTypeToAss) {
		this.fuelTypeToAss = fuelTypeToAss;
	}

	public String getServiceTypeToAss() {
		return serviceTypeToAss;
	}

	public void setServiceTypeToAss(String serviceTypeToAss) {
		this.serviceTypeToAss = serviceTypeToAss;
	}

	public String getFromDateToAss() {
		return fromDateToAss;
	}

	public void setFromDateToAss(String fromDateToAss) {
		this.fromDateToAss = fromDateToAss;
	}

	public String getToDateToAss() {
		return toDateToAss;
	}

	public void setToDateToAss(String toDateToAss) {
		this.toDateToAss = toDateToAss;
	}

	public String getDateOfServiceNonAuth() {
		return dateOfServiceNonAuth;
	}

	public void setDateOfServiceNonAuth(String dateOfServiceNonAuth) {
		this.dateOfServiceNonAuth = dateOfServiceNonAuth;
	}
	
	
	public String dealerNameNonAuth;
	public String serviceTypeNonAuth;
	
	
	public String getDealerNameNonAuth() {
		return dealerNameNonAuth;
	}

	public void setDealerNameNonAuth(String dealerNameNonAuth) {
		this.dealerNameNonAuth = dealerNameNonAuth;
	}
	
	public String getServiceTypeNonAuth() {
		return serviceTypeNonAuth;
	}

	public void setServiceTypeNonAuth(String serviceTypeNonAuth) {
		this.serviceTypeNonAuth = serviceTypeNonAuth;
	}

	public String mielageAtServiceAltId;
	public String serviceTypeAltId;

	public String statusContact;
	public String statusNoncontact;
	public String serviceNRType;
	public String alreadyServedOtherReason;

	public String service_type;
	public String contactType;
	public long vehicleIn;
	public String pickUpAddressIn;
	public long callInteractionId;
	public long driverIdSelectIn;
	public long workshopIn;

	public String typeOfPickupIn;

	public String noServiceReasonTaggedTo1;
	public String noServiceReasonTaggedToComments1;
	
	public String remarksPickUp;
	public String remarksDissatisfied;
	
	
	
	//purchase new car
	
	public String PurchaseYes;
	
	public String getPurchaseYes() {
		return PurchaseYes;
	}

	public void setPurchaseYes(String purchaseYes) {
		PurchaseYes = purchaseYes;
	}
	//New customer
	
	public String VehicleSoldYes;
	
	

	public String getVehicleSoldYes() {
		return VehicleSoldYes;
	}

	public void setVehicleSoldYes(String vehicleSoldYes) {
		VehicleSoldYes = vehicleSoldYes;
	}
	
	//userfeedback to raise complaint
	
	public String userfeedback;
	
	

	public String getUserfeedback() {
		return userfeedback;
	}

	public void setUserfeedback(String userfeedback) {
		this.userfeedback = userfeedback;
	}

	public String getRemarksPickUp() {
		return remarksPickUp;
	}

	public void setRemarksPickUp(String remarksPickUp) {
		this.remarksPickUp = remarksPickUp;
	}

	public String getRemarksDissatisfied() {
		return remarksDissatisfied;
	}

	public void setRemarksDissatisfied(String remarksDissatisfied) {
		this.remarksDissatisfied = remarksDissatisfied;
	}

	public List<Long> listId;
	public List<Integer> listOfintId;
	public List<String> data;

	public List<String> phoneList;

	public List<String> selectedValues;
	public List<UpsellLead> upsellLead;

	public int getUniqueidForCallSync() {
		return uniqueidForCallSync;
	}

	public void setUniqueidForCallSync(int uniqueidForCallSync) {
		this.uniqueidForCallSync = uniqueidForCallSync;
	}

	public List<String> getPhoneList() {
		return phoneList;
	}

	public void setPhoneList(List<String> phoneList) {
		this.phoneList = phoneList;
	}

	public List<UpsellLead> getUpsellLead() {
		return upsellLead;
	}

	public long getVehicleIn() {
		return vehicleIn;
	}

	public void setVehicleIn(long vehicleIn) {
		this.vehicleIn = vehicleIn;
	}

	public void setUpsellLead(List<UpsellLead> upsellLead) {
		this.upsellLead = upsellLead;
	}

	public long getSingleId() {
		return singleId;
	}

	public void setSingleId(long singleId) {
		this.singleId = singleId;
	}

	public String getSingleData() {
		return singleData;
	}

	public void setSingleData(String singleData) {
		this.singleData = singleData;
	}

	public String getNoServiceReasonTaggedTo1() {
		return noServiceReasonTaggedTo1;
	}

	public void setNoServiceReasonTaggedTo1(String noServiceReasonTaggedTo1) {
		this.noServiceReasonTaggedTo1 = noServiceReasonTaggedTo1;
	}

	public String getNoServiceReasonTaggedToComments1() {
		return noServiceReasonTaggedToComments1;
	}

	public void setNoServiceReasonTaggedToComments1(String noServiceReasonTaggedToComments1) {
		this.noServiceReasonTaggedToComments1 = noServiceReasonTaggedToComments1;
	}

	public String getTranferData() {
		return tranferData;
	}

	public void setTranferData(String tranferData) {
		this.tranferData = tranferData;
	}

	public String getSelectedFile() {
		return selectedFile;
	}

	public void setSelectedFile(String selectedFile) {
		this.selectedFile = selectedFile;
	}

	public String getMielageAtServiceAltId() {
		return mielageAtServiceAltId;
	}

	public void setMielageAtServiceAltId(String mielageAtServiceAltId) {
		this.mielageAtServiceAltId = mielageAtServiceAltId;
	}

	public String getServiceTypeAltId() {
		return serviceTypeAltId;
	}

	public void setServiceTypeAltId(String serviceTypeAltId) {
		this.serviceTypeAltId = serviceTypeAltId;
	}

	public List<Long> getListId() {
		return listId;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public List<Integer> getListOfintId() {
		return listOfintId;
	}

	public void setListOfintId(List<Integer> listOfintId) {
		this.listOfintId = listOfintId;
	}

	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}

	public List<String> getSelectedValues() {
		return selectedValues;
	}

	public void setSelectedValues(List<String> selectedValues) {
		this.selectedValues = selectedValues;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getStatusContact() {
		return statusContact;
	}

	public void setStatusContact(String statusContact) {
		this.statusContact = statusContact;
	}

	public String getStatusNoncontact() {
		return statusNoncontact;
	}

	public void setStatusNoncontact(String statusNoncontact) {
		this.statusNoncontact = statusNoncontact;
	}

	public String getServiceNRType() {
		return serviceNRType;
	}

	public void setServiceNRType(String serviceNRType) {
		this.serviceNRType = serviceNRType;
	}

	public String getAlreadyServedOtherReason() {
		return alreadyServedOtherReason;
	}

	public void setAlreadyServedOtherReason(String alreadyServedOtherReason) {
		this.alreadyServedOtherReason = alreadyServedOtherReason;
	}

	public long getWyzUser_Id() {
		return wyzUser_Id;
	}

	public void setWyzUser_Id(long wyzUser_Id) {
		this.wyzUser_Id = wyzUser_Id;
	}

	public long getVehicalId() {
		return vehicalId;
	}

	public void setVehicalId(long vehicalId) {
		this.vehicalId = vehicalId;
	}

	public long getCustomer_Id() {
		return customer_Id;
	}

	public void setCustomer_Id(long customer_Id) {
		this.customer_Id = customer_Id;
	}

	public String getAssignCall() {
		return assignCall;
	}

	public void setAssignCall(String assignCall) {
		this.assignCall = assignCall;
	}

	public long getServiceAdvisorId() {
		return serviceAdvisorId;
	}

	public void setServiceAdvisorId(long serviceAdvisorId) {
		this.serviceAdvisorId = serviceAdvisorId;
	}

	public long getDriverId() {
		return driverId;
	}

	public void setDriverId(long driverId) {
		this.driverId = driverId;
	}

	public long getVehicleId_SB() {
		return vehicleId_SB;
	}

	public void setVehicleId_SB(long vehicleId_SB) {
		this.vehicleId_SB = vehicleId_SB;
	}

	public long getWorkshopId() {
		return workshopId;
	}

	public void setWorkshopId(long workshopId) {
		this.workshopId = workshopId;
	}

	public long getCallInteractionId() {
		return callInteractionId;
	}

	public void setCallInteractionId(long callInteractionId) {
		this.callInteractionId = callInteractionId;
	}

	public String getServiceScheduledDateIn() {
		return serviceScheduledDateIn;
	}

	public void setServiceScheduledDateIn(String serviceScheduledDateIn) {
		this.serviceScheduledDateIn = serviceScheduledDateIn;
	}

	public String getServiceScheduledTimeIn() {
		return serviceScheduledTimeIn;
	}

	public void setServiceScheduledTimeIn(String serviceScheduledTimeIn) {
		this.serviceScheduledTimeIn = serviceScheduledTimeIn;
	}

	public String getTypeOfPickupIn() {
		return typeOfPickupIn;
	}

	public void setTypeOfPickupIn(String typeOfPickupIn) {
		this.typeOfPickupIn = typeOfPickupIn;
	}

	public String getPickUpAddressIn() {
		return pickUpAddressIn;
	}

	public void setPickUpAddressIn(String pickUpAddressIn) {
		this.pickUpAddressIn = pickUpAddressIn;
	}

	public String getTime_FromIn() {
		return time_FromIn;
	}

	public void setTime_FromIn(String time_FromIn) {
		this.time_FromIn = time_FromIn;
	}

	public String getTime_ToIn() {
		return time_ToIn;
	}

	public void setTime_ToIn(String time_ToIn) {
		this.time_ToIn = time_ToIn;
	}

	
	public long getDriverIdSelectIn() {
		return driverIdSelectIn;
	}

	public void setDriverIdSelectIn(long driverIdSelectIn) {
		this.driverIdSelectIn = driverIdSelectIn;
	}

	public long getWorkshopIn() {
		return workshopIn;
	}

	public void setWorkshopIn(long workshopIn) {
		this.workshopIn = workshopIn;
	}

	public long getServiceAdvisorIdIn() {
		return serviceAdvisorIdIn;
	}

	public void setServiceAdvisorIdIn(long serviceAdvisorIdIn) {
		this.serviceAdvisorIdIn = serviceAdvisorIdIn;
	}

	public String getDepartmentForFB1() {
		return departmentForFB1;
	}

	public void setDepartmentForFB1(String departmentForFB1) {
		this.departmentForFB1 = departmentForFB1;
	}

	public String getComplaintOrFB_TagTo1() {
		return complaintOrFB_TagTo1;
	}

	public void setComplaintOrFB_TagTo1(String complaintOrFB_TagTo1) {
		this.complaintOrFB_TagTo1 = complaintOrFB_TagTo1;
	}

	public String getDepartmentForFB2() {
		return departmentForFB2;
	}

	public void setDepartmentForFB2(String departmentForFB2) {
		this.departmentForFB2 = departmentForFB2;
	}

	public String getComplaintOrFB_TagTo2() {
		return complaintOrFB_TagTo2;
	}

	public void setComplaintOrFB_TagTo2(String complaintOrFB_TagTo2) {
		this.complaintOrFB_TagTo2 = complaintOrFB_TagTo2;
	}

	public String getRemarksOfFB1() {
		return remarksOfFB1;
	}

	public void setRemarksOfFB1(String remarksOfFB1) {
		this.remarksOfFB1 = remarksOfFB1;
	}

	public String getRemarksOfFB2() {
		return remarksOfFB2;
	}

	public void setRemarksOfFB2(String remarksOfFB2) {
		this.remarksOfFB2 = remarksOfFB2;
	}

	public String satisfiedWithWashingPickup;
	public String isChargesAndRepairAsMentionedPickup;
	public String isChargesAsEstimatedpickUp;
	public String chargesInfoExplainedBeforeServiceMMS;
	public String isDemandOfSerDoneInLVisitpickup;
	public String vehiclePerformancePickUp;
	public String satisfiedWithWashingSelfD;

	public String getSatisfiedWithWashingPickup() {
		return satisfiedWithWashingPickup;
	}

	public void setSatisfiedWithWashingPickup(String satisfiedWithWashingPickup) {
		this.satisfiedWithWashingPickup = satisfiedWithWashingPickup;
	}

	public String getIsChargesAndRepairAsMentionedPickup() {
		return isChargesAndRepairAsMentionedPickup;
	}

	public void setIsChargesAndRepairAsMentionedPickup(String isChargesAndRepairAsMentionedPickup) {
		this.isChargesAndRepairAsMentionedPickup = isChargesAndRepairAsMentionedPickup;
	}

	public String getIsChargesAsEstimatedpickUp() {
		return isChargesAsEstimatedpickUp;
	}

	public void setIsChargesAsEstimatedpickUp(String isChargesAsEstimatedpickUp) {
		this.isChargesAsEstimatedpickUp = isChargesAsEstimatedpickUp;
	}

	public String getChargesInfoExplainedBeforeServiceMMS() {
		return chargesInfoExplainedBeforeServiceMMS;
	}

	public void setChargesInfoExplainedBeforeServiceMMS(String chargesInfoExplainedBeforeServiceMMS) {
		this.chargesInfoExplainedBeforeServiceMMS = chargesInfoExplainedBeforeServiceMMS;
	}

	public String getIsDemandOfSerDoneInLVisitpickup() {
		return isDemandOfSerDoneInLVisitpickup;
	}

	public void setIsDemandOfSerDoneInLVisitpickup(String isDemandOfSerDoneInLVisitpickup) {
		this.isDemandOfSerDoneInLVisitpickup = isDemandOfSerDoneInLVisitpickup;
	}

	public String getVehiclePerformancePickUp() {
		return vehiclePerformancePickUp;
	}

	public void setVehiclePerformancePickUp(String vehiclePerformancePickUp) {
		this.vehiclePerformancePickUp = vehiclePerformancePickUp;
	}

	public String getSatisfiedWithWashingSelfD() {
		return satisfiedWithWashingSelfD;
	}

	public void setSatisfiedWithWashingSelfD(String satisfiedWithWashingSelfD) {
		this.satisfiedWithWashingSelfD = satisfiedWithWashingSelfD;
	}

	public long getCampaignNamePSF() {
		return campaignNamePSF;
	}

	public void setCampaignNamePSF(long campaignNamePSF) {
		this.campaignNamePSF = campaignNamePSF;
	}

	public long getCampaignPSFToAss() {
		return campaignPSFToAss;
	}

	public void setCampaignPSFToAss(long campaignPSFToAss) {
		this.campaignPSFToAss = campaignPSFToAss;
	}

	public long getOfferSelId() {
		return offerSelId;
	}

	public void setOfferSelId(long offerSelId) {
		this.offerSelId = offerSelId;
	}
	
	//Insurance Extra parameters
	
	public String insuranceProvidedByOEM;
	
	public String insuranceProvidedUnAuth;
	
	public Date dateOfRenewalNonAuth;
	
	public String renewalTypeIn;
	public String renewalModeIn;
	
	
	
	public String cityIn;
	
	
	
	public Date getDateOfRenewalNonAuth() {
		return dateOfRenewalNonAuth;
	}

	public void setDateOfRenewalNonAuth(Date dateOfRenewalNonAuth) {
		this.dateOfRenewalNonAuth = dateOfRenewalNonAuth;
	}

	public String getInsuranceProvidedByOEM() {
		return insuranceProvidedByOEM;
	}

	public void setInsuranceProvidedByOEM(String insuranceProvidedByOEM) {
		this.insuranceProvidedByOEM = insuranceProvidedByOEM;
	}

	public String getInsuranceProvidedUnAuth() {
		return insuranceProvidedUnAuth;
	}

	public void setInsuranceProvidedUnAuth(String insuranceProvidedUnAuth) {
		this.insuranceProvidedUnAuth = insuranceProvidedUnAuth;
	}

	public Date getAppointmentScheduledDateIn() {
		return appointmentScheduledDateIn;
	}

	public void setAppointmentScheduledDateIn(Date appointmentScheduledDateIn) {
		this.appointmentScheduledDateIn = appointmentScheduledDateIn;
	}

	public String getCityIn() {
		return cityIn;
	}

	public void setCityIn(String cityIn) {
		this.cityIn = cityIn;
	}

	public String getRenewalTypeIn() {
		return renewalTypeIn;
	}

	public void setRenewalTypeIn(String renewalTypeIn) {
		this.renewalTypeIn = renewalTypeIn;
	}

	public String getRenewalModeIn() {
		return renewalModeIn;
	}

	public void setRenewalModeIn(String renewalModeIn) {
		this.renewalModeIn = renewalModeIn;
	}
	
	
	//Insurance
	
	public String appointmentFromTimeHomevisit;
	public String appointmentToTimeHomeVisit;
	
	public List<String> addOnsPrefered_PopularOptions; 
	
	public List<String> addOnsPrefered_OtherOptions;
	
	
	public String insuranceCompanyIn;
	
	public String dsaIn;
	
	public double premiumwithTaxIn;
	
	public String nomineeNameIn;
	
	public String nomineeAgeIn;
	
	public String nomineeRelationWithOwnerIn;
	
	public String appointeeNameIn;
	
	public long showRoom_idIn;
	
	public String PremiumYesInB;
	
	//Show room visit in bound
	
	public String appointmentFromTimeIn;
	public String appointmentToTimeIn;
	
	//Home visit in bound
	
	public String appointmentFromTimeHomevisitIn;
	public String appointmentToTimeHomeVisitIn;
	
	public String insuranceAgentDataIn;
	
	
	

	public String getAppointmentFromTimeHomevisit() {
		return appointmentFromTimeHomevisit;
	}

	public void setAppointmentFromTimeHomevisit(String appointmentFromTimeHomevisit) {
		this.appointmentFromTimeHomevisit = appointmentFromTimeHomevisit;
	}

	public String getAppointmentToTimeHomeVisit() {
		return appointmentToTimeHomeVisit;
	}

	public void setAppointmentToTimeHomeVisit(String appointmentToTimeHomeVisit) {
		this.appointmentToTimeHomeVisit = appointmentToTimeHomeVisit;
	}

	public List<String> getAddOnsPrefered_PopularOptions() {
		return addOnsPrefered_PopularOptions;
	}

	public void setAddOnsPrefered_PopularOptions(List<String> addOnsPrefered_PopularOptions) {
		this.addOnsPrefered_PopularOptions = addOnsPrefered_PopularOptions;
	}

	public List<String> getAddOnsPrefered_OtherOptions() {
		return addOnsPrefered_OtherOptions;
	}

	public void setAddOnsPrefered_OtherOptions(List<String> addOnsPrefered_OtherOptions) {
		this.addOnsPrefered_OtherOptions = addOnsPrefered_OtherOptions;
	}

	public String getInsuranceCompanyIn() {
		return insuranceCompanyIn;
	}

	public void setInsuranceCompanyIn(String insuranceCompanyIn) {
		this.insuranceCompanyIn = insuranceCompanyIn;
	}

	public String getDsaIn() {
		return dsaIn;
	}

	public void setDsaIn(String dsaIn) {
		this.dsaIn = dsaIn;
	}

	public double getPremiumwithTaxIn() {
		return premiumwithTaxIn;
	}

	public void setPremiumwithTaxIn(double premiumwithTaxIn) {
		this.premiumwithTaxIn = premiumwithTaxIn;
	}

	public String getNomineeNameIn() {
		return nomineeNameIn;
	}

	public void setNomineeNameIn(String nomineeNameIn) {
		this.nomineeNameIn = nomineeNameIn;
	}

	public String getNomineeAgeIn() {
		return nomineeAgeIn;
	}

	public void setNomineeAgeIn(String nomineeAgeIn) {
		this.nomineeAgeIn = nomineeAgeIn;
	}

	public String getNomineeRelationWithOwnerIn() {
		return nomineeRelationWithOwnerIn;
	}

	public void setNomineeRelationWithOwnerIn(String nomineeRelationWithOwnerIn) {
		this.nomineeRelationWithOwnerIn = nomineeRelationWithOwnerIn;
	}

	public String getAppointeeNameIn() {
		return appointeeNameIn;
	}

	public void setAppointeeNameIn(String appointeeNameIn) {
		this.appointeeNameIn = appointeeNameIn;
	}

	public long getShowRoom_idIn() {
		return showRoom_idIn;
	}

	public void setShowRoom_idIn(long showRoom_idIn) {
		this.showRoom_idIn = showRoom_idIn;
	}

	public String getAppointmentFromTimeIn() {
		return appointmentFromTimeIn;
	}

	public void setAppointmentFromTimeIn(String appointmentFromTimeIn) {
		this.appointmentFromTimeIn = appointmentFromTimeIn;
	}

	public String getAppointmentToTimeIn() {
		return appointmentToTimeIn;
	}

	public void setAppointmentToTimeIn(String appointmentToTimeIn) {
		this.appointmentToTimeIn = appointmentToTimeIn;
	}

	public String getAppointmentFromTimeHomevisitIn() {
		return appointmentFromTimeHomevisitIn;
	}

	public void setAppointmentFromTimeHomevisitIn(String appointmentFromTimeHomevisitIn) {
		this.appointmentFromTimeHomevisitIn = appointmentFromTimeHomevisitIn;
	}

	public String getAppointmentToTimeHomeVisitIn() {
		return appointmentToTimeHomeVisitIn;
	}

	public void setAppointmentToTimeHomeVisitIn(String appointmentToTimeHomeVisitIn) {
		this.appointmentToTimeHomeVisitIn = appointmentToTimeHomeVisitIn;
	}

	public String getCityNameToAss() {
		return cityNameToAss;
	}

	public void setCityNameToAss(String cityNameToAss) {
		this.cityNameToAss = cityNameToAss;
	}

	public long getWorkshopToAss() {
		return workshopToAss;
	}

	public void setWorkshopToAss(long workshopToAss) {
		this.workshopToAss = workshopToAss;
	}

	public String getInsuranceAgentDataIn() {
		return insuranceAgentDataIn;
	}

	public void setInsuranceAgentDataIn(String insuranceAgentDataIn) {
		this.insuranceAgentDataIn = insuranceAgentDataIn;
	}

	public String getPremiumYesInB() {
		return PremiumYesInB;
	}

	public void setPremiumYesInB(String premiumYesInB) {
		PremiumYesInB = premiumYesInB;
	}

	public List<Long> getCallId() {
		return callId;
	}

	public void setCallId(List<Long> callId) {
		this.callId = callId;
	}

	public List<Long> getUserId() {
		return userId;
	}

	public void setUserId(List<Long> userId) {
		this.userId = userId;
	}

	public List<Long> getCampainId() {
		return campainId;
	}

	public void setCampainId(List<Long> campainId) {
		this.campainId = campainId;
	}

	public List<String> getRemarksList() {
		return remarksList;
	}

	public void setRemarksList(List<String> remarksList) {
		this.remarksList = remarksList;
	}

	public String getServiceBookId() {
		return serviceBookId;
	}

	public void setServiceBookId(String serviceBookId) {
		this.serviceBookId = serviceBookId;
	}

	
	
	

}
