package models;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class PickupDrop {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public boolean ispickup;
	public boolean isdrop;
	@Column(length = 200)
	public String pickUpAddress;
	
	@Temporal(TemporalType.DATE)
	public Date pickupDate;
	@Column(length = 15)
	public String pickupTime;
	@Column(length = 10)
	public String CRN;

	@Temporal(TemporalType.TIME)
	public java.util.Date timeFrom;

	@Temporal(TemporalType.TIME)
	public java.util.Date timeTo;

	public java.util.Date getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(java.util.Date timeFrom) {
		this.timeFrom = timeFrom;
	}

	public java.util.Date getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(java.util.Date timeTo) {
		this.timeTo = timeTo;
	}

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "pickupDrop")
	public ServiceBooked serviceBooked;
	
	
	@OneToOne(fetch = FetchType.EAGER, mappedBy = "pickupDrop")
	public PSFInteraction psfInteraction;

	@ManyToOne(cascade = CascadeType.ALL)
	public Driver driver;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isIspickup() {
		return ispickup;
	}

	public void setIspickup(boolean ispickup) {
		this.ispickup = ispickup;
	}

	public boolean isIsdrop() {
		return isdrop;
	}

	public void setIsdrop(boolean isdrop) {
		this.isdrop = isdrop;
	}

	public String getPickUpAddress() {
		return pickUpAddress;
	}

	public void setPickUpAddress(String pickUpAddress) {
		this.pickUpAddress = pickUpAddress;
	}

	public String getPickupDateStr() {
		
		if(pickupDate!=null){
		
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringDate = formatter.format(pickupDate);
		return stringDate;
		}else{
			return "";
		}
	}
	
	public String getPickupDateString() {
		
		if(pickupDate!=null){
		
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringDate = formatter.format(pickupDate);
		return stringDate;
		}else{
			return "";
		}
	}
	
	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getPickupTime() {
		String pattern = "HH:mm:ss";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringDate = formatter.format(timeFrom);
		return stringDate;
	}

	public void setPickupTime(String pickupTime) {
		this.pickupTime = pickupTime;
	}

	public ServiceBooked getServiceBooked() {
		return serviceBooked;
	}

	public void setServiceBooked(ServiceBooked serviceBooked) {
		this.serviceBooked = serviceBooked;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public String getCRN() {
		return CRN;
	}

	public void setCRN(String cRN) {
		CRN = cRN;
	}

	public PSFInteraction getPsfInteraction() {
		return psfInteraction;
	}

	public void setPsfInteraction(PSFInteraction psfInteraction) {
		this.psfInteraction = psfInteraction;
	}
	
	

}
