package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class ShowRooms {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long showRoom_id;
	
	@Column(length = 30)
	public String showroomName;
	
	@OneToMany(mappedBy = "showRooms")
	public List<AppointmentBooked> appointmentBooked;

	public long getShowRoom_id() {
		return showRoom_id;
	}

	public void setShowRoom_id(long showRoom_id) {
		this.showRoom_id = showRoom_id;
	}

	public String getShowroomName() {
		return showroomName;
	}

	public void setShowroomName(String showroomName) {
		this.showroomName = showroomName;
	}

	public List<AppointmentBooked> getAppointmentBooked() {
		return appointmentBooked;
	}

	public void setAppointmentBooked(List<AppointmentBooked> appointmentBooked) {
		this.appointmentBooked = appointmentBooked;
	}
	
	
	
}
