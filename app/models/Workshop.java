package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Workshop {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	@Column(length = 30)
	public String workshopName;
	@Column(length = 50)
	public String workshopAddress;
	@Column(length = 30)
	public String pincode;

	@Column(length = 30)
	public String workshopCode;

	@OneToMany(mappedBy = "workshop")
	public List<Driver> driver;

	@OneToMany(mappedBy = "workshop")
	public List<WyzUser> users;
	
	
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "workshopList")
	private List<WyzUser> userWorkshop;
	

	public List<WyzUser> getUserWorkshop() {
		return userWorkshop;
	}

	public void setUserWorkshop(List<WyzUser> userWorkshop) {
		this.userWorkshop = userWorkshop;
	}

	public List<Driver> getDriver() {
		return driver;
	}

	public void setDriver(List<Driver> driver) {
		this.driver = driver;
	}

	public String getWorkshopCode() {
		return workshopCode;
	}

	public void setWorkshopCode(String workshopCode) {
		this.workshopCode = workshopCode;
	}

	public int capacityPerDay;

	@ManyToOne(fetch = FetchType.EAGER)
	public Location location;

	public String upload_id;

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	@ManyToOne
	public Campaign campaign;

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	@OneToMany(mappedBy = "workshop")
	public List<WorkshopSummary> workshopSummarys;

	@OneToMany(mappedBy = "workshop")
	public List<Bay> bays;

	@OneToMany(mappedBy = "workshop")
	public List<Service> service;

	@OneToMany(mappedBy = "workshop")
	public List<ServiceBooked> servicesBooked;

	@OneToMany(mappedBy = "workshop")
	public List<ServiceAdvisor> serviceAdvisor;

	public List<ServiceAdvisor> getServiceAdvisor() {
		return serviceAdvisor;
	}

	public void setServiceAdvisor(List<ServiceAdvisor> serviceAdvisor) {
		this.serviceAdvisor = serviceAdvisor;
	}

	public List<Service> getService() {
		return service;
	}

	public void setService(List<Service> service) {
		this.service = service;
	}

	public List<ServiceBooked> getServicesBooked() {
		return servicesBooked;
	}

	public void setServicesBooked(List<ServiceBooked> servicesBooked) {
		this.servicesBooked = servicesBooked;
	}

	public List<WorkshopSummary> getWorkshopSummarys() {
		return workshopSummarys;
	}

	public void setWorkshopSummarys(List<WorkshopSummary> workshopSummarys) {
		this.workshopSummarys = workshopSummarys;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getWorkshopName() {
		return workshopName;
	}

	public void setWorkshopName(String workshopName) {
		this.workshopName = workshopName;
	}

	public String getWorkshopAddress() {
		return workshopAddress;
	}

	public void setWorkshopAddress(String workshopAddress) {
		this.workshopAddress = workshopAddress;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<Bay> getBays() {
		return bays;
	}

	public void setBays(List<Bay> bays) {
		this.bays = bays;
	}

	public int getCapacityPerDay() {
		return capacityPerDay;
	}

	public void setCapacityPerDay(int capacityPerDay) {
		this.capacityPerDay = capacityPerDay;
	}

	public List<WyzUser> getUsers() {
		return users;
	}

	public void setUsers(List<WyzUser> users) {
		this.users = users;
	}

}
