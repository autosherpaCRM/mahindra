/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import controllers.webmodels.UploadFileHistory;

/**
 *
 * @author wct-09
 */

@SqlResultSetMappings({ @SqlResultSetMapping(name = "UploadFileHistory", classes = {
		@ConstructorResult(targetClass = UploadFileHistory.class, columns = { @ColumnResult(name = "report_Name"),
				 @ColumnResult(name = "report_Date"),
				@ColumnResult(name = "city"), @ColumnResult(name = "workshop"), @ColumnResult(name = "totalRecords"),
				@ColumnResult(name = "approved_records"), @ColumnResult(name = "rejectedRecord"),
				@ColumnResult(name = "wyzUser_id"), @ColumnResult(name = "uploaded_by_user"),
				@ColumnResult(name = "uploadDate"), @ColumnResult(name = "upload_time"),
				@ColumnResult(name = "uploaded_File_Name"), @ColumnResult(name = "upload_id") })

		}) })

@Entity
public class UploadData {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	public String dataType;
	public String sheetName;
	public String fileNametimeStamp;
	public long campaignId;
	public String campaignStartdate;
	public String campaignExpiry;
	public String campaignName;
	public int totalRecords;
	public int rejectedRecords;
	public int successRecords;

	@Temporal(TemporalType.DATE)
	public Date recordedDate;

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getCampaignStartdate() {
		return campaignStartdate;
	}

	public void setCampaignStartdate(String campaignStartdate) {
		this.campaignStartdate = campaignStartdate;
	}

	public String getCampaignExpiry() {
		return campaignExpiry;
	}

	public void setCampaignExpiry(String campaignExpiry) {
		this.campaignExpiry = campaignExpiry;
	}

	public String getFileNametimeStamp() {
		return fileNametimeStamp;
	}

	public void setFileNametimeStamp(String fileNametimeStamp) {
		this.fileNametimeStamp = fileNametimeStamp;
	}

	@Temporal(TemporalType.DATE)
	public Date uploadedDate;

	@Temporal(TemporalType.TIMESTAMP)
	public Date uploadedDateAndTime;

	@ManyToOne
	public Location location;

	@ManyToOne
	public Workshop workshop;

	@ManyToOne
	public WyzUser wyzUser;

	
	public String upload_id;

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}
	
	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Workshop getWorkshop() {
		return workshop;
	}

	public void setWorkshop(Workshop workshop) {
		this.workshop = workshop;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public Date getUploadedDate() {
		return uploadedDate;
	}

	public void setUploadedDate(Date uploadedDate) {
		this.uploadedDate = uploadedDate;
	}

	public Date getUploadedDateAndTime() {
		return uploadedDateAndTime;
	}

	public void setUploadedDateAndTime(Date uploadedDateAndTime) {
		this.uploadedDateAndTime = uploadedDateAndTime;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getRejectedRecords() {
		return rejectedRecords;
	}

	public void setRejectedRecords(int rejectedRecords) {
		this.rejectedRecords = rejectedRecords;
	}

	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	public Date getRecordedDate() {
		return recordedDate;
	}

	public void setRecordedDate(Date recordedDate) {
		this.recordedDate = recordedDate;
	}

	public int getSuccessRecords() {
		return successRecords;
	}

	public void setSuccessRecords(int successRecords) {
		this.successRecords = successRecords;
	}

	public long getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	
	

}
