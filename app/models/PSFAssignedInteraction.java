package models;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class PSFAssignedInteraction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	public String callMade;
	
	public String interactionType;
	
	public Date uplodedCurrentDate;	
	
	@Column(length = 30)
	public String lastDisposition;

	@ManyToOne
	public Customer customer;

	@ManyToOne
	public WyzUser wyzUser;

	@ManyToOne	
	public Vehicle vehicle;
	
	@OneToOne
	public CallDispositionData finalDisposition;	
	
	@ManyToOne
	public Campaign campaign;
	
	@ManyToOne
	public Service service;
	
	
	@OneToMany(mappedBy = "psfAssignedInteraction")
	public List<CallInteraction> interactions;
	
		
	public boolean displayFlag;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCallMade() {
		return callMade;
	}

	public void setCallMade(String callMade) {
		this.callMade = callMade;
	}

	public String getInteractionType() {
		return interactionType;
	}

	public void setInteractionType(String interactionType) {
		this.interactionType = interactionType;
	}

	public Date getUplodedCurrentDate() {
		return uplodedCurrentDate;
	}

	public void setUplodedCurrentDate(Date uplodedCurrentDate) {
		this.uplodedCurrentDate = uplodedCurrentDate;
	}

	public String getLastDisposition() {
		return lastDisposition;
	}

	public void setLastDisposition(String lastDisposition) {
		this.lastDisposition = lastDisposition;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public CallDispositionData getFinalDisposition() {
		return finalDisposition;
	}

	public void setFinalDisposition(CallDispositionData finalDisposition) {
		this.finalDisposition = finalDisposition;
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public boolean isDisplayFlag() {
		return displayFlag;
	}

	public void setDisplayFlag(boolean displayFlag) {
		this.displayFlag = displayFlag;
	}

	
	public List<CallInteraction> getInteractions() {
		return interactions;
	}

	public void setInteractions(List<CallInteraction> interactions) {
		this.interactions = interactions;
	}
	
	
	
}
