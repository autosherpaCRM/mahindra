package models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class WyzUser {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@Column(length = 30)
	public String firstName;
	@Column(length = 30)
	public String lastName;
	@Column(length = 30)
	public String role;
	@Column(length = 30)
	public String dealerName;
	@Column(length = 30)
	public String dealerId;
	@Column(length = 30)
	public String phoneNumber;
	@Column(length = 30)
	public String phoneIMEINo;
	@Column(length = 30)
	public String emailId;
	@Column(length = 30)
	public String password;
	@Column(length = 30)
	public String dealerCode;
	@Column(length = 30)
	public String creManager;
	@Column(length = 30)
	public String role1;

	public boolean insuranceRole;
	@Column(length = 30)
	public String registrationId;

	public long allocatedCount;

	public boolean unAvailable;

	@Temporal(TemporalType.DATE)
	public Date updatedDate;

	@Column(unique = true, length = 30)
	public String userName;

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "wyzUser")
	public List<CallInteraction> callInteraction;

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "wyzUser")
	public List<AssignedInteraction> assignedInteraction;

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "wyzUser")
	public List<PSFAssignedInteraction> psfAssignedInteraction;

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "wyzUser")
	public List<InsuranceAssignedInteraction> insuranceAssignedInteraction;

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "wyzUser")
	public List<SMSInteraction> sentSMSes;

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "assignedUserTo")
	public List<Complaint> complaints;

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "wyzUser")
	public List<TaggingUsers> taggingusers;

	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	public Location location;

	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	public Workshop workshop;

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "wyzUser")
	public List<Availability> availabilities;

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "wyzUser")
	public List<UnAvailability> unAvailabilities;

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "wyzUser")
	public List<Complaint> complaint;
	
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private Tenant tenant;
	
	@ManyToMany
	@JoinTable(name = "userRoles")
	private List<Role> roles;
	
	@ManyToMany
	@JoinTable(name = "userLocation")
	private List<Location> locationList;
	
	@ManyToMany
	@JoinTable(name = "userWorkshop")
	private List<Workshop> workshopList;
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "wyzUser")
	public List<ServiceBooked> serviceBooked;

	

	public List<ServiceBooked> getServiceBooked() {
		return serviceBooked;
	}

	public void setServiceBooked(List<ServiceBooked> serviceBooked) {
		this.serviceBooked = serviceBooked;
	}

	public List<Workshop> getWorkshopList() {
		return workshopList;
	}

	public void setWorkshopList(List<Workshop> workshopList) {
		this.workshopList = workshopList;
	}

	public List<Complaint> getComplaint() {
		return complaint;
	}

	public void setComplaint(List<Complaint> complaint) {
		this.complaint = complaint;
	}

	public List<PSFAssignedInteraction> getPsfAssignedInteraction() {
		return psfAssignedInteraction;
	}

	public void setPsfAssignedInteraction(List<PSFAssignedInteraction> psfAssignedInteraction) {
		this.psfAssignedInteraction = psfAssignedInteraction;
	}

	public List<Availability> getAvailabilities() {
		return availabilities;
	}

	public void setAvailabilities(List<Availability> availabilities) {
		this.availabilities = availabilities;
	}

	public List<Complaint> getComplaints() {
		return complaints;
	}

	public void setComplaints(List<Complaint> complaint) {
		this.complaints = complaint;
	}

	public List<UnAvailability> getUnAvailabilities() {
		return unAvailabilities;
	}

	public void setUnAvailabilities(List<UnAvailability> unAvailabilities) {
		this.unAvailabilities = unAvailabilities;
	}

	public long getAllocatedCount() {
		return allocatedCount;
	}

	public void setAllocatedCount(long allocatedCount) {
		this.allocatedCount = allocatedCount;
	}

	public boolean isUnAvailable() {
		return unAvailable;
	}

	public void setUnAvailable(boolean unAvailable) {
		this.unAvailable = unAvailable;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getDealerId() {
		return dealerId;
	}

	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneIMEINo() {
		return phoneIMEINo;
	}

	public void setPhoneIMEINo(String phoneIMEINo) {
		this.phoneIMEINo = phoneIMEINo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public String getCreManager() {
		return creManager;
	}

	public void setCreManager(String creManager) {
		this.creManager = creManager;
	}

	public String getRole1() {
		return role1;
	}

	public void setRole1(String role1) {
		this.role1 = role1;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public boolean isInsuranceRole() {
		return insuranceRole;
	}

	public void setInsuranceRole(boolean insuranceRole) {
		this.insuranceRole = insuranceRole;
	}

	public List<CallInteraction> getCallInteractions() {
		return callInteraction;
	}

	public void setCallInteractions(List<CallInteraction> callInteraction) {
		this.callInteraction = callInteraction;
	}

	public List<AssignedInteraction> getAssignedInteractions() {
		return assignedInteraction;
	}

	public void setAssignedInteractions(List<AssignedInteraction> assignedInteraction) {
		this.assignedInteraction = assignedInteraction;
	}

	public List<CallInteraction> getCallInteraction() {
		return callInteraction;
	}

	public void setCallInteraction(List<CallInteraction> callInteraction) {
		this.callInteraction = callInteraction;
	}

	public List<AssignedInteraction> getAssignedInteraction() {
		return assignedInteraction;
	}

	public void setAssignedInteraction(List<AssignedInteraction> assignedInteraction) {
		this.assignedInteraction = assignedInteraction;
	}

	public List<SMSInteraction> getSentSMSes() {
		return sentSMSes;
	}

	public void setSentSMSes(List<SMSInteraction> sentSMSes) {
		this.sentSMSes = sentSMSes;
	}

	public Workshop getWorkshop() {
		return workshop;
	}

	public void setWorkshop(Workshop workshop) {
		this.workshop = workshop;
	}

	public List<InsuranceAssignedInteraction> getInsuranceAssignedInteraction() {
		return insuranceAssignedInteraction;
	}

	public void setInsuranceAssignedInteraction(List<InsuranceAssignedInteraction> insuranceAssignedInteraction) {
		this.insuranceAssignedInteraction = insuranceAssignedInteraction;
	}

	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public List<Location> getLocationList() {
		return locationList;
	}

	public void setLocationList(List<Location> locationList) {
		this.locationList = locationList;
	}

	public List<TaggingUsers> getTaggingusers() {
		return taggingusers;
	}

	public void setTaggingusers(List<TaggingUsers> taggingusers) {
		this.taggingusers = taggingusers;
	}
	
	//Linking dealer	
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	public Dealer dealer;
	
	public Dealer getDealer() {
		return dealer;
	}

	public void setDealer(Dealer dealer) {
		this.dealer = dealer;
	}
	
	//Merge the code
	
	
	
	
	

}
