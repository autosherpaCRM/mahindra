package models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DriverInteraction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	public boolean isCall;

	@Lob
	public byte[] mediaFileLob;

	public String mediaFile;

	@ManyToOne(cascade = CascadeType.ALL)
	public Customer customer;

	@ManyToOne(cascade = CascadeType.ALL)
	public WyzUser wyzUser;

	@ManyToOne(cascade = CascadeType.ALL)
	public Vehicle vehicle;
	

	@Temporal(TemporalType.TIMESTAMP)
	public java.util.Date interactionDateAndTime;

	@Column(length = 30)
	public String interactionDate;
	@Column(length = 30)
	public String interactionTime;
	@Column(length = 10)
	public String pickupOrDrop;

	public int customerCRN;

	@OneToOne
	public DriverInteractionStatus status;

	@Column(length = 30)
	public String firebaseKey;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	

	public java.util.Date getInteractionDateAndTime() {
		return interactionDateAndTime;
	}

	public void setInteractionDateAndTime(java.util.Date interactionDateAndTime) {
		this.interactionDateAndTime = interactionDateAndTime;
	}

	public String getInteractionDate() {
		return interactionDate;
	}

	public void setInteractionDate(String interactionDate) {
		this.interactionDate = interactionDate;
	}

	public String getInteractionTime() {
		return interactionTime;
	}

	public void setInteractionTime(String interactionTime) {
		this.interactionTime = interactionTime;
	}

	public String getPickupOrDrop() {
		return pickupOrDrop;
	}

	public void setPickupOrDrop(String pickupOrDrop) {
		this.pickupOrDrop = pickupOrDrop;
	}

	public int getCustomerCRN() {
		return customerCRN;
	}

	public void setCustomerCRN(int customerCRN) {
		this.customerCRN = customerCRN;
	}

	public DriverInteractionStatus getStatus() {
		return status;
	}

	public void setStatus(DriverInteractionStatus status) {
		this.status = status;
	}

	public String getFirebaseKey() {
		return firebaseKey;
	}

	public void setFirebaseKey(String firebaseKey) {
		this.firebaseKey = firebaseKey;
	}

}
