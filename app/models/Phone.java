package models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Phone {

	public static int PHONE_TYPE_RESIDENCE = 1;
	public static int PHONE_TYPE_OFFICE = 2;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long phone_Id;
	public String phoneNumber;
	public boolean isPreferredPhone;

	public int phoneTye;
	public String updatedBy;

	public String upload_id;

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	@ManyToOne
	public Customer customer;

	public long getPhone_Id() {
		return phone_Id;
	}

	public void setPhone_Id(long phone_Id) {
		this.phone_Id = phone_Id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getPhoneTye() {
		return phoneTye;
	}

	public void setPhoneTye(int phoneTye) {
		this.phoneTye = phoneTye;
	}

	public boolean isIsPreferredPhone() {
		return isPreferredPhone;
	}

	public void setIsPreferredPhone(boolean isPreferredPhone) {
		this.isPreferredPhone = isPreferredPhone;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
