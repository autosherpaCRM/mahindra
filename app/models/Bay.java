package models;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class Bay {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long bayId;
	public String workshopName;
	public long bayCapacityperday;
	
	@ManyToOne(cascade = CascadeType.ALL) 
	public Workshop workshop;
}
