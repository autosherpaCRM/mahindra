/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.List;
import javax.persistence.*;

/**
 *
 * @author W-885
 */
@Entity
public class PostServiceFeedBack {
    
        @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
        
       @Column(length = 250)
       public String specialMessage;
       
       @OneToMany(mappedBy = "postServiceFeedBack")
       public List<PSFInteraction> psfInteraction;
    
}
