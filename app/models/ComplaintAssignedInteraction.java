/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;
import java.util.List;

import javax.persistence.*;


/**
 *
 * @author wct-09
 */
@Entity
public class ComplaintAssignedInteraction {
       @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;
    public String isAssigned;
    public String callMade;
        @Temporal(TemporalType.DATE)
    public Date assignedDate;
        @Column(length = 20)
    public String complaintStatus;
    //public String interactionType;
    
    @ManyToOne
    public WyzUser assignedTo;
    
    @ManyToOne
    public Complaint complaint;

    public String getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(String complaintStatus) {
        this.complaintStatus = complaintStatus;
    }

     
    public WyzUser getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(WyzUser assignedTo) {
        this.assignedTo = assignedTo;
    }

    
    public Date getAssignedDate() {
        return assignedDate;
    }

    public void setAssignedDate(Date assignedDate) {
        this.assignedDate = assignedDate;
    }

    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIsAssigned() {
        return isAssigned;
    }

    public void setIsAssigned(String isAssigned) {
        this.isAssigned = isAssigned;
    }

    public String getCallMade() {
        return callMade;
    }

    public void setCallMade(String callMade) {
        this.callMade = callMade;
    }

    

    public Complaint getComplaint() {
        return complaint;
    }

    public void setComplaint(Complaint complaint) {
        this.complaint = complaint;
    }
    
}
