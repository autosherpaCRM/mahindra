package models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
	

@Entity
public class InsuranceCallHistoryCube {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	public long vehicle_vehicle_id;
	
	public long customer_id;

	public long insuranceAssignedInteraction_id;
	
	public long cicallinteraction_id;
	
	public long callDispositionData_id;
	

	@Column(length = 100)
	public String creName;
	
	@Column(length = 100) 
	public String  callTime;
	
	@Temporal(TemporalType.DATE) 
	public Date  callDate;
	
		
	@Column(length = 100) 
	public String  isCallinitaited;
	
	@Column(length = 100) 
	public String  callType;
	
	@Column(length = 100) 
	public String  callDuration;
	
	@Column(length = 100) 
	public String  ringTime;

	@Column(length = 100) 
	public String  campaignTYpe;

	@Column(length = 100) 
	public String  chassisNo;
	

	@Column(length = 100) 
	public String  model;
	

	@Column(length = 100) 
	public String  variant;
	
	@Temporal(TemporalType.DATE) 
	public Date  saleDate;
	

	@Column(length = 100) 
	public String  vehicleRegNo;
	
	@Column(length = 100) 
	public String  customerName;
	
	@Column(length = 100) 
	public String  preffered_address;
	
	@Column(length = 100) 
	public String  prefferedPhoneNumber;
	
	@Column(length = 100) 
	public String  PrimaryDisposition;
	
	@Column(length = 100) 
	public String  SecondaryDisposition;
	
	@Column(length = 100) 
	public String  coverNoteNo;
	
	@Column(length = 100) 
	public String  lastRenewedLocation;
	
	@Column(length = 100) 
	public String  lastRenewalDate;
	
	@Column(length = 100) 
	public String  premimum;
	
	@Column(length = 100) 
	public String  renewalDoneBy;
	
	@Column(length = 100) 
	public String  Tertiary_disposition;
	
	@Temporal(TemporalType.DATE) 
	public Date  followUpDate;
	
	@Column(length = 100) 
	public String  followUpTime;
	
	@Column(length = 100) 
	public String  addOnsPrefered_OtherOptionsData;
	
	@Column(length = 100) 
	public String  addOnsPrefered_PopularOptionsData;
	
	@Column(length = 100) 
	public String  remarksOfFB;
	
	@Column(length = 100) 
	public String  departmentForFB;
	
	@Column(length = 100) 
	public String  complaintOrFB_TagTo;
	
	@Column(length = 100) 
	public String  appointmentId;
	
	
	@Column(length = 100) 
	public String  appointeeName;
	
	@Temporal(TemporalType.DATE) 
	public Date  appointmentDate;
	
	@Column(length = 100) 
	public String  appointmentFromTime;
	
	@Column(length = 100) 
	public String  dsa;
	
	@Column(length = 100) 
	public String  insuranceAgentData;
	
	@Column(length = 100) 
	public String  insuranceCompany;
	
	@Column(length = 100) 
	public String  nomineeName;
	
	@Column(length = 100) 
	public String  nomineeRelationWithOwner;
	
	@Column(length = 100) 
	public String  premiumwithTax;
	
	@Column(length = 100) 
	public String  renewalMode;
	
	@Column(length = 100) 
	public String  renewalType;
	
	@Column(length = 100) 
	public String  typeOfPickup;
	
	@Column(length = 100) 
	public String  insuranceAgent_insuranceAgentId;
	
	@Column(length = 100) 
	public String  showRooms_showRoom_id;
	
	@Column(length = 100) 
	public String  PremiumYes;
	
	@Column(length = 100) 
	public String  comments;
	
	@Column(length = 100) 
	public String  reason;
	
	@Column(length = 100) 
	public String  cremanager;
	
	@Column(length = 100) 
	public String  addressOfVisit;
	
	
	public long wyzuser_id;
		
	public boolean  isCallDurationUpdated;
	
	
	@Temporal(TemporalType.DATE) 
	public Date  updatedDate;

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}



	public long getInsuranceAssignedInteraction_id() {
		return insuranceAssignedInteraction_id;
	}


	public void setInsuranceAssignedInteraction_id(long insuranceAssignedInteraction_id) {
		this.insuranceAssignedInteraction_id = insuranceAssignedInteraction_id;
	}


	public long getCicallinteraction_id() {
		return cicallinteraction_id;
	}


	public void setCicallinteraction_id(long cicallinteraction_id) {
		this.cicallinteraction_id = cicallinteraction_id;
	}


	public long getVehicle_vehicle_id() {
		return vehicle_vehicle_id;
	}


	public void setVehicle_vehicle_id(long vehicle_vehicle_id) {
		this.vehicle_vehicle_id = vehicle_vehicle_id;
	}


	public String getCallTime() {
		return callTime;
	}


	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}


	public Date getCallDate() {
		return callDate;
	}


	public void setCallDate(Date callDate) {
		this.callDate = callDate;
	}


	public String getIsCallinitaited() {
		return isCallinitaited;
	}


	public void setIsCallinitaited(String isCallinitaited) {
		this.isCallinitaited = isCallinitaited;
	}


	public String getCallType() {
		return callType;
	}


	public void setCallType(String callType) {
		this.callType = callType;
	}


	public String getCallDuration() {
		return callDuration;
	}


	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}


	public String getRingTime() {
		return ringTime;
	}


	public void setRingTime(String ringTime) {
		this.ringTime = ringTime;
	}


	public String getCampaignTYpe() {
		return campaignTYpe;
	}


	public void setCampaignTYpe(String campaignTYpe) {
		this.campaignTYpe = campaignTYpe;
	}


	public String getChassisNo() {
		return chassisNo;
	}


	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}


	public String getModel() {
		return model;
	}


	public void setModel(String model) {
		this.model = model;
	}


	public String getVariant() {
		return variant;
	}


	public void setVariant(String variant) {
		this.variant = variant;
	}


	public Date getSaleDate() {
		return saleDate;
	}


	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}


	public String getVehicleRegNo() {
		return vehicleRegNo;
	}


	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}


	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	public String getPreffered_address() {
		return preffered_address;
	}


	public void setPreffered_address(String preffered_address) {
		this.preffered_address = preffered_address;
	}


	public String getPrefferedPhoneNumber() {
		return prefferedPhoneNumber;
	}


	public void setPrefferedPhoneNumber(String prefferedPhoneNumber) {
		this.prefferedPhoneNumber = prefferedPhoneNumber;
	}


	public String getPrimaryDisposition() {
		return PrimaryDisposition;
	}


	public void setPrimaryDisposition(String primaryDisposition) {
		PrimaryDisposition = primaryDisposition;
	}


	public String getSecondaryDisposition() {
		return SecondaryDisposition;
	}


	public void setSecondaryDisposition(String secondaryDisposition) {
		SecondaryDisposition = secondaryDisposition;
	}


	public String getCoverNoteNo() {
		return coverNoteNo;
	}


	public void setCoverNoteNo(String coverNoteNo) {
		this.coverNoteNo = coverNoteNo;
	}


	public String getLastRenewedLocation() {
		return lastRenewedLocation;
	}


	public void setLastRenewedLocation(String lastRenewedLocation) {
		this.lastRenewedLocation = lastRenewedLocation;
	}


	public String getLastRenewalDate() {
		return lastRenewalDate;
	}


	public void setLastRenewalDate(String lastRenewalDate) {
		this.lastRenewalDate = lastRenewalDate;
	}


	public String getPremimum() {
		return premimum;
	}


	public void setPremimum(String premimum) {
		this.premimum = premimum;
	}


	public String getRenewalDoneBy() {
		return renewalDoneBy;
	}


	public void setRenewalDoneBy(String renewalDoneBy) {
		this.renewalDoneBy = renewalDoneBy;
	}


	public String getTertiary_disposition() {
		return Tertiary_disposition;
	}


	public void setTertiary_disposition(String tertiary_disposition) {
		Tertiary_disposition = tertiary_disposition;
	}


	public Date getFollowUpDate() {
		return followUpDate;
	}


	public void setFollowUpDate(Date followUpDate) {
		this.followUpDate = followUpDate;
	}


	public String getFollowUpTime() {
		return followUpTime;
	}


	public void setFollowUpTime(String followUpTime) {
		this.followUpTime = followUpTime;
	}


	public String getAddOnsPrefered_OtherOptionsData() {
		return addOnsPrefered_OtherOptionsData;
	}


	public void setAddOnsPrefered_OtherOptionsData(String addOnsPrefered_OtherOptionsData) {
		this.addOnsPrefered_OtherOptionsData = addOnsPrefered_OtherOptionsData;
	}


	public String getAddOnsPrefered_PopularOptionsData() {
		return addOnsPrefered_PopularOptionsData;
	}


	public void setAddOnsPrefered_PopularOptionsData(String addOnsPrefered_PopularOptionsData) {
		this.addOnsPrefered_PopularOptionsData = addOnsPrefered_PopularOptionsData;
	}


	public String getRemarksOfFB() {
		return remarksOfFB;
	}


	public void setRemarksOfFB(String remarksOfFB) {
		this.remarksOfFB = remarksOfFB;
	}


	public String getDepartmentForFB() {
		return departmentForFB;
	}


	public void setDepartmentForFB(String departmentForFB) {
		this.departmentForFB = departmentForFB;
	}


	public String getComplaintOrFB_TagTo() {
		return complaintOrFB_TagTo;
	}


	public void setComplaintOrFB_TagTo(String complaintOrFB_TagTo) {
		this.complaintOrFB_TagTo = complaintOrFB_TagTo;
	}


	public String getAppointmentId() {
		return appointmentId;
	}


	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}


	public String getAppointeeName() {
		return appointeeName;
	}


	public void setAppointeeName(String appointeeName) {
		this.appointeeName = appointeeName;
	}


	public Date getAppointmentDate() {
		return appointmentDate;
	}


	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}


	public String getAppointmentFromTime() {
		return appointmentFromTime;
	}


	public void setAppointmentFromTime(String appointmentFromTime) {
		this.appointmentFromTime = appointmentFromTime;
	}


	public String getDsa() {
		return dsa;
	}


	public void setDsa(String dsa) {
		this.dsa = dsa;
	}


	public String getInsuranceAgentData() {
		return insuranceAgentData;
	}


	public void setInsuranceAgentData(String insuranceAgentData) {
		this.insuranceAgentData = insuranceAgentData;
	}


	public String getInsuranceCompany() {
		return insuranceCompany;
	}


	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}


	public String getNomineeName() {
		return nomineeName;
	}


	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}


	public String getNomineeRelationWithOwner() {
		return nomineeRelationWithOwner;
	}


	public void setNomineeRelationWithOwner(String nomineeRelationWithOwner) {
		this.nomineeRelationWithOwner = nomineeRelationWithOwner;
	}


	public String getPremiumwithTax() {
		return premiumwithTax;
	}


	public void setPremiumwithTax(String premiumwithTax) {
		this.premiumwithTax = premiumwithTax;
	}


	public String getRenewalMode() {
		return renewalMode;
	}


	public void setRenewalMode(String renewalMode) {
		this.renewalMode = renewalMode;
	}


	public String getRenewalType() {
		return renewalType;
	}


	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}


	public String getTypeOfPickup() {
		return typeOfPickup;
	}


	public void setTypeOfPickup(String typeOfPickup) {
		this.typeOfPickup = typeOfPickup;
	}


	public String getInsuranceAgent_insuranceAgentId() {
		return insuranceAgent_insuranceAgentId;
	}


	public void setInsuranceAgent_insuranceAgentId(String insuranceAgent_insuranceAgentId) {
		this.insuranceAgent_insuranceAgentId = insuranceAgent_insuranceAgentId;
	}


	public String getShowRooms_showRoom_id() {
		return showRooms_showRoom_id;
	}


	public void setShowRooms_showRoom_id(String showRooms_showRoom_id) {
		this.showRooms_showRoom_id = showRooms_showRoom_id;
	}


	public String getPremiumYes() {
		return PremiumYes;
	}


	public void setPremiumYes(String premiumYes) {
		PremiumYes = premiumYes;
	}


	public String getComments() {
		return comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}


	public String getReason() {
		return reason;
	}


	public void setReason(String reason) {
		this.reason = reason;
	}


	public String getCremanager() {
		return cremanager;
	}


	public void setCremanager(String cremanager) {
		this.cremanager = cremanager;
	}


	public String getAddressOfVisit() {
		return addressOfVisit;
	}


	public void setAddressOfVisit(String addressOfVisit) {
		this.addressOfVisit = addressOfVisit;
	}


	public long getWyzuser_id() {
		return wyzuser_id;
	}


	public void setWyzuser_id(long wyzuser_id) {
		this.wyzuser_id = wyzuser_id;
	}


	


	public boolean isCallDurationUpdated() {
		return isCallDurationUpdated;
	}


	public void setCallDurationUpdated(boolean isCallDurationUpdated) {
		this.isCallDurationUpdated = isCallDurationUpdated;
	}


	public Date getUpdatedDate() {
		return updatedDate;
	}


	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


	public long getCustomer_id() {
		return customer_id;
	}


	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}


	public String getCreName() {
		return creName;
	}


	public void setCreName(String creName) {
		this.creName = creName;
	}


	public long getCallDispositionData_id() {
		return callDispositionData_id;
	}


	public void setCallDispositionData_id(long callDispositionData_id) {
		this.callDispositionData_id = callDispositionData_id;
	}
	
	
	
}	