package models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ServiceDoneSchCall {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public String customerName;
	public String customerPhone;
	public String vehicleNumber;
	public String agentId;
	public String agentName;
	public String dealerCode;
	public String syncedToFireBaseServiceBooked;
	public String vehicalRegNo;
	public String model;
	public Date serviceBookeduplodedCurrentDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public String getSyncedToFireBaseServiceBooked() {
		return syncedToFireBaseServiceBooked;
	}

	public void setSyncedToFireBaseServiceBooked(
			String syncedToFireBaseServiceBooked) {
		this.syncedToFireBaseServiceBooked = syncedToFireBaseServiceBooked;
	}

	public String getVehicalRegNo() {
		return vehicalRegNo;
	}

	public void setVehicalRegNo(String vehicalRegNo) {
		this.vehicalRegNo = vehicalRegNo;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Date getServiceBookeduplodedCurrentDate() {
		return serviceBookeduplodedCurrentDate;
	}

	public void setServiceBookeduplodedCurrentDate(
			Date serviceBookeduplodedCurrentDate) {
		this.serviceBookeduplodedCurrentDate = serviceBookeduplodedCurrentDate;
	}

}
