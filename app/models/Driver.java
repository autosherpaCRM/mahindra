package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Driver {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public String driverName;
	public String driverPhoneNum;
	public String driverAltPhoneNum;

	@OneToOne(cascade = CascadeType.ALL)
	public WyzUser wyzUser;

	@OneToMany(mappedBy = "driver")
	public List<ServiceBooked> servicesBooked;

	public String upload_id;

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	@ManyToOne
	public Workshop workshop;

	public Workshop getWorkshop() {
		return workshop;
	}

	public void setWorkshop(Workshop workshop) {
		this.workshop = workshop;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDriverPhoneNum() {
		return driverPhoneNum;
	}

	public void setDriverPhoneNum(String driverPhoneNum) {
		this.driverPhoneNum = driverPhoneNum;
	}

	public String getDriverAltPhoneNum() {
		return driverAltPhoneNum;
	}

	public void setDriverAltPhoneNum(String driverAltPhoneNum) {
		this.driverAltPhoneNum = driverAltPhoneNum;
	}

	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	public List<ServiceBooked> getServicesBooked() {
		return servicesBooked;
	}

	public void setServicesBooked(List<ServiceBooked> servicesBooked) {
		this.servicesBooked = servicesBooked;
	}

}
