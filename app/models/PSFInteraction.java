/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 *
 * @author W-885
 */

@Entity
public class PSFInteraction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@OneToOne
	public CallDispositionData callDispositionData;

	@Column(length = 20)
	public String isContacted;

	@Column(length = 40)
	public String PSFDispositon;

	@Column(length = 30)
	public String modeOfService;

	@Column(length = 30)
	public String modeOfServiceDone;

	@Temporal(TemporalType.DATE)
	public Date psfFollowUpDate;

	@Column(length = 30)
	public String psfFollowUpTime;

	@Column(length = 30)
	public String isVisitedWorkshopBeforeAppoint;

	@Column(length = 30)
	public String ratePerformanceOfSA;

	@Column(length = 30)
	public String workDoneOnVehiandServExpAtTimeOfDeli;

	@Column(length = 30)
	public String isAppointmentGotAsDesired;

	@Column(length = 30)
	public String isDeliveredAsPromisedTime;

	@Column(length = 30)
	public String isProblemFixedInFirstVisit;

	@Column(length = 30)
	public String isSatisfiedWithServiceProvided;

	@Column(length = 30)
	public String iscallMadeToFixPickUptime;

	@Column(length = 30)
	public String isCallRecievedBeforeVehWorkshop;

	@Column(length = 30)
	public String isVehicleReadyAsPromisedDate;

	@Column(length = 30)
	public String selfDriveInFeedBack;

	@Column(length = 30)
	public String demandedRequestDone;

	@Column(length = 30)
	public String isPickUpDropStaffCourteous;

	@Column(length = 30)
	public String isQualityOfPickUpDone;

	@Column(length = 30)
	public String vehiclePerformance;

	@Column(length = 30)
	public String isDemandOfSeriviceDoneInLastVisit;

	@Column(length = 30)
	public String isAppointmentReceviedAsReq;

	@Column(length = 30)
	public String iscallMadeOneDayBeforeService;

	@Column(length = 30)
	public String chargesInfoExplainedBeforeService;

	@Column(length = 30)
	public String isTimeTakenForServiceReasonable;

	@Column(length = 30)
	public String isChargesAsEstimated;

	@Column(length = 30)
	public String isChargesAndRepairAsMentioned;

	@Column(length = 30)
	public String satisfiedWithWashing;

	@Column(length = 30)
	public String isworkshopStaffCourteous;

	@Column(length = 30)
	public String satisfiedWithQualityOfDoorService;

	@Column(length = 300)
	public String comments;

	@Column(length = 300)
	public String OtherComments;

	public String reasonOfDissatification;

	@Temporal(TemporalType.DATE)
	public Date dateOfVisit;

	@Column(length = 30)
	public String timeOfVisit;

	@Column(length = 250)
	public String specificDetailsOfVisitNotCompleted;

	@Column(length = 250)
	public String detailsOfProblemsInPSFList;

	public Integer delayInHours;
	public Integer delayInMin;

	@Column(length = 250)
	public String specificDetailsOfComplaint;

	@Column(length = 250)
	public String afterServiceSatisfication;

	@Column(length = 250)
	public String recentServiceSatisfication;

	@Column(length = 250)
	public String afterServiceComments;

	@Column(length = 250)
	public String recentServiceComments;

	@Column(length = 50)
	public String isFollowupRequired;

	@Column(length = 50)
	public String isFollowUpDone;

	@Column(length = 250)
	public String vehiclePerformanceAfterService;

	@Column(length = 250)
	public String feedbackrating;

	@Column(length = 250)
	public String remarks;

	public long upsellCount;

	@OneToMany(mappedBy = "psfInteraction")
	public List<UpsellLead> upsellLead;

	@ManyToOne(fetch = FetchType.EAGER)
	public PostServiceFeedBack postServiceFeedBack;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "callInteraction_id", nullable = true)
	public CallInteraction callInteraction;


	// Hyundai PSF Fields
	@Column(length = 30)
	public String q1_CompleteSatisfication;

	@Column(length = 30)
	public String q2_InconvinenceDSRFunction;

	@Column(length = 50)
	public String q3_InconvinenceDSRAssignedTo;

	@Column(length = 250)
	public String q4_InconvinenceDSRRemarks;

	@Column(length = 5)
	public String q5_RateExplanationOfSA;

	@Column(length = 5)
	public String q6_RatePickUpProcessOfCar;

	@Column(length = 5)
	public String q7_RateCleanlinessOfCar;

	@Column(length = 5)
	public String q8_RateOverAllInteraction;

	@Column(length = 50)
	public String q9_FeedbackFunction;

	@Column(length = 50)
	public String q10_FeedbackAssignedTo;

	@Column(length = 50)
	public String q11_FeedbackRemarks;

	@Column(length = 50)
	public String q12_FeedbackTaken;

	@Column(length = 50)
	public String q13_IsCarPerformingWell;
	
	@Column(length = 50)
	public String q14_availedPickUpGivenByDealer;
	
	@Column(length = 10)
	public String q15_explainedReferAndEarnPRGM;
	
	@Column(length = 10)
	public String q16_RateCleanlinessOfDealerFacility;
	
	@Column(length = 10)
	public String q17_RateMaintenanceAndRepair;
	

	// Maruthi PSF Fields

	@Column(length = 20)
	public String qM1_SatisfiedWithQualityOfServ;

	@Column(length = 40)
	public String qM2_ReasonOfAreaOfImprovement;

	@Column(length = 40)
	public String qM3_SubReasonOfAreaOfImprovement1;
	
	@Column(length = 40)
	public String qM3_SubReasonOfAreaOfImprovement2;
	
	@Column(length = 20)
	public String qM4_confirmingCustomer;
	
	@Column(length = 20)
	public String qM4_confirmingCustomerRightTime;
	
	@Column(length = 20)
	public String qM4_RightTimeToEnquireOrderbillDate;
	

	@Column(length = 20)
	public String qM1RatingOverAllAppointProcess;
	
	@Column(length = 20)
	public String qM2RatingTimeTakenVehiclehandOver;
	
	@Column(length = 20)
	public String qM3RatingServicAdvCourtesyResponse;
	
	@Column(length = 20)
	public String qM4RatingServiceAdvJobExpl;
	
	@Column(length = 20)
	public String qM5RatingCleanlinessOfDealer;
	
	@Column(length = 20)
	public String qM6RatingTimelinessVehicleDelivary;
	
	@Column(length = 20)
	public String qM7RatingFairnessOfCharge;
	
	
	@Column(length = 20)
	public String qM8RatingHelpFulnessOfStaff;
	
	
	@Column(length = 20)
	public String qM9RatingTotalTimeRequired;
	
	
	@Column(length = 20)
	public String qM10RatingQualityOfMaintenance;
	
	
	@Column(length = 20)
	public String qM11RatingCondAndCleanlinessOfVeh;
	
	
	@Column(length = 20)
	public String qM12RatingOverAllServExp;
	
	
	//Mahindra Commercial PSF 
	
	@Column(length = 50)
	public String qMC1_OverallServiceExp;
	
	@Column(length = 50)
	public String qMC2_HappyWithRepairJob;
	
	@Column(length = 50)
	public String qMC3_CoutnessHelpfulnessOfSA;
	
	@Column(length = 50)
	public String qMC4_TimeTakenCompleteJob;
	
	@Column(length = 50)
	public String qMC5_CostEstimateAdherance;
	
	@Column(length = 50)
	public String qMC6_ServiceConvinenantTime;
	
	@Column(length = 50)
	public String qMC7_LikeToRevistDealer;
	
	
	// Ford PSF variables

		@Column(length = 20)
		public String qFordQ1;

		@Column(length = 40)
		public String qFordQ2;

		@Column(length = 40)
		public String qFordQ3;

		@Column(length = 40)
		public String qFordQ4;

		@Column(length = 20)
		public String qFordQ5;

		@Column(length = 20)
		public String qFordQ6;

		@Column(length = 20)
		public String qFordQ7;

		@Column(length = 20)
		public String qFordQ8;

		@Column(length = 20)
		public String qFordQ9;

		@Column(length = 20)
		public String qFordQ10;

		@Column(length = 20)
		public String qFordQ11;

		@Column(length = 20)
		public String qFordQ12;

		@Column(length = 20)
		public String qFordQ13;

		@Column(length = 20)
		public String qFordQ14;

		@Column(length = 20)
		public String qFordQ15;

		@Column(length = 20)
		public String qFordQ16;

	
	
	
	@Temporal(TemporalType.DATE)
	public Date psfAppointmentDate;

	@Column(length = 30)
	public String psfAppointmentTime;

	@Column(length = 200)
	public String psfAddressForVisit;

	
	@OneToOne
	public PickupDrop pickupDrop;
	
	
	
	

	public PickupDrop getPickupDrop() {
		return pickupDrop;
	}

	public void setPickupDrop(PickupDrop pickupDrop) {
		this.pickupDrop = pickupDrop;
	}

	

	public CallInteraction getCallInteraction() {
		return callInteraction;
	}

	public void setCallInteraction(CallInteraction callInteraction) {
		this.callInteraction = callInteraction;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIsContacted() {
		return isContacted;
	}

	public void setIsContacted(String isContacted) {
		this.isContacted = isContacted;
	}

	public String getPSFDispositon() {
		return PSFDispositon;
	}

	public void setPSFDispositon(String PSFDispositon) {
		this.PSFDispositon = PSFDispositon;
	}

	public String getModeOfService() {
		return modeOfService;
	}

	public void setModeOfService(String modeOfService) {
		this.modeOfService = modeOfService;
	}

	public String getIsVisitedWorkshopBeforeAppoint() {
		return isVisitedWorkshopBeforeAppoint;
	}

	public void setIsVisitedWorkshopBeforeAppoint(String isVisitedWorkshopBeforeAppoint) {
		this.isVisitedWorkshopBeforeAppoint = isVisitedWorkshopBeforeAppoint;
	}

	public String getRatePerformanceOfSA() {
		return ratePerformanceOfSA;
	}

	public void setRatePerformanceOfSA(String ratePerformanceOfSA) {
		this.ratePerformanceOfSA = ratePerformanceOfSA;
	}

	public String getWorkDoneOnVehiandServExpAtTimeOfDeli() {
		return workDoneOnVehiandServExpAtTimeOfDeli;
	}

	public void setWorkDoneOnVehiandServExpAtTimeOfDeli(String workDoneOnVehiandServExpAtTimeOfDeli) {
		this.workDoneOnVehiandServExpAtTimeOfDeli = workDoneOnVehiandServExpAtTimeOfDeli;
	}

	public String getIsAppointmentGotAsDesired() {
		return isAppointmentGotAsDesired;
	}

	public void setIsAppointmentGotAsDesired(String isAppointmentGotAsDesired) {
		this.isAppointmentGotAsDesired = isAppointmentGotAsDesired;
	}

	public String getIsDeliveredAsPromisedTime() {
		return isDeliveredAsPromisedTime;
	}

	public void setIsDeliveredAsPromisedTime(String isDeliveredAsPromisedTime) {
		this.isDeliveredAsPromisedTime = isDeliveredAsPromisedTime;
	}

	public String getIsProblemFixedInFirstVisit() {
		return isProblemFixedInFirstVisit;
	}

	public void setIsProblemFixedInFirstVisit(String isProblemFixedInFirstVisit) {
		this.isProblemFixedInFirstVisit = isProblemFixedInFirstVisit;
	}

	public String getIsSatisfiedWithServiceProvided() {
		return isSatisfiedWithServiceProvided;
	}

	public void setIsSatisfiedWithServiceProvided(String isSatisfiedWithServiceProvided) {
		this.isSatisfiedWithServiceProvided = isSatisfiedWithServiceProvided;
	}

	public String getIscallMadeToFixPickUptime() {
		return iscallMadeToFixPickUptime;
	}

	public void setIscallMadeToFixPickUptime(String iscallMadeToFixPickUptime) {
		this.iscallMadeToFixPickUptime = iscallMadeToFixPickUptime;
	}

	public String getIsCallRecievedBeforeVehWorkshop() {
		return isCallRecievedBeforeVehWorkshop;
	}

	public void setIsCallRecievedBeforeVehWorkshop(String isCallRecievedBeforeVehWorkshop) {
		this.isCallRecievedBeforeVehWorkshop = isCallRecievedBeforeVehWorkshop;
	}

	public String getIsVehicleReadyAsPromisedDate() {
		return isVehicleReadyAsPromisedDate;
	}

	public void setIsVehicleReadyAsPromisedDate(String isVehicleReadyAsPromisedDate) {
		this.isVehicleReadyAsPromisedDate = isVehicleReadyAsPromisedDate;
	}

	public String getSelfDriveInFeedBack() {
		return selfDriveInFeedBack;
	}

	public void setSelfDriveInFeedBack(String selfDriveInFeedBack) {
		this.selfDriveInFeedBack = selfDriveInFeedBack;
	}

	public String getDemandedRequestDone() {
		return demandedRequestDone;
	}

	public void setDemandedRequestDone(String demandedRequestDone) {
		this.demandedRequestDone = demandedRequestDone;
	}

	public String getIsPickUpDropStaffCourteous() {
		return isPickUpDropStaffCourteous;
	}

	public void setIsPickUpDropStaffCourteous(String isPickUpDropStaffCourteous) {
		this.isPickUpDropStaffCourteous = isPickUpDropStaffCourteous;
	}

	public String getIsQualityOfPickUpDone() {
		return isQualityOfPickUpDone;
	}

	public void setIsQualityOfPickUpDone(String isQualityOfPickUpDone) {
		this.isQualityOfPickUpDone = isQualityOfPickUpDone;
	}

	public String getVehiclePerformance() {
		return vehiclePerformance;
	}

	public void setVehiclePerformance(String vehiclePerformance) {
		this.vehiclePerformance = vehiclePerformance;
	}

	public String getIsDemandOfSeriviceDoneInLastVisit() {
		return isDemandOfSeriviceDoneInLastVisit;
	}

	public void setIsDemandOfSeriviceDoneInLastVisit(String isDemandOfSeriviceDoneInLastVisit) {
		this.isDemandOfSeriviceDoneInLastVisit = isDemandOfSeriviceDoneInLastVisit;
	}

	public String getIsAppointmentReceviedAsReq() {
		return isAppointmentReceviedAsReq;
	}

	public void setIsAppointmentReceviedAsReq(String isAppointmentReceviedAsReq) {
		this.isAppointmentReceviedAsReq = isAppointmentReceviedAsReq;
	}

	public String getIscallMadeOneDayBeforeService() {
		return iscallMadeOneDayBeforeService;
	}

	public void setIscallMadeOneDayBeforeService(String iscallMadeOneDayBeforeService) {
		this.iscallMadeOneDayBeforeService = iscallMadeOneDayBeforeService;
	}

	public String getChargesInfoExplainedBeforeService() {
		return chargesInfoExplainedBeforeService;
	}

	public void setChargesInfoExplainedBeforeService(String chargesInfoExplainedBeforeService) {
		this.chargesInfoExplainedBeforeService = chargesInfoExplainedBeforeService;
	}

	public String getIsTimeTakenForServiceReasonable() {
		return isTimeTakenForServiceReasonable;
	}

	public void setIsTimeTakenForServiceReasonable(String isTimeTakenForServiceReasonable) {
		this.isTimeTakenForServiceReasonable = isTimeTakenForServiceReasonable;
	}

	public String getIsChargesAsEstimated() {
		return isChargesAsEstimated;
	}

	public void setIsChargesAsEstimated(String isChargesAsEstimated) {
		this.isChargesAsEstimated = isChargesAsEstimated;
	}

	public String getIsChargesAndRepairAsMentioned() {
		return isChargesAndRepairAsMentioned;
	}

	public void setIsChargesAndRepairAsMentioned(String isChargesAndRepairAsMentioned) {
		this.isChargesAndRepairAsMentioned = isChargesAndRepairAsMentioned;
	}

	public String getSatisfiedWithWashing() {
		return satisfiedWithWashing;
	}

	public void setSatisfiedWithWashing(String satisfiedWithWashing) {
		this.satisfiedWithWashing = satisfiedWithWashing;
	}

	public String getIsworkshopStaffCourteous() {
		return isworkshopStaffCourteous;
	}

	public void setIsworkshopStaffCourteous(String isworkshopStaffCourteous) {
		this.isworkshopStaffCourteous = isworkshopStaffCourteous;
	}

	public String getSatisfiedWithQualityOfDoorService() {
		return satisfiedWithQualityOfDoorService;
	}

	public void setSatisfiedWithQualityOfDoorService(String satisfiedWithQualityOfDoorService) {
		this.satisfiedWithQualityOfDoorService = satisfiedWithQualityOfDoorService;
	}

	public CallDispositionData getCallDispositionData() {
		return callDispositionData;
	}

	public void setCallDispositionData(CallDispositionData callDispositionData) {
		this.callDispositionData = callDispositionData;
	}

	public PostServiceFeedBack getPostServiceFeedBack() {
		return postServiceFeedBack;
	}

	public void setPostServiceFeedBack(PostServiceFeedBack postServiceFeedBack) {
		this.postServiceFeedBack = postServiceFeedBack;
	}

	public Date getPsfFollowUpDate() {
		return psfFollowUpDate;
	}

	public void setPsfFollowUpDate(Date psfFollowUpDate) {
		this.psfFollowUpDate = psfFollowUpDate;
	}

	public String getPsfFollowUpTime() {
		return psfFollowUpTime;
	}

	public void setPsfFollowUpTime(String psfFollowUpTime) {
		this.psfFollowUpTime = psfFollowUpTime;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getModeOfServiceDone() {
		return modeOfServiceDone;
	}

	public void setModeOfServiceDone(String modeOfServiceDone) {
		this.modeOfServiceDone = modeOfServiceDone;
	}

	public String getOtherComments() {
		return OtherComments;
	}

	public void setOtherComments(String OtherComments) {
		this.OtherComments = OtherComments;
	}

	public String getReasonOfDissatification() {
		return reasonOfDissatification;
	}

	public void setReasonOfDissatification(String reasonOfDissatification) {
		this.reasonOfDissatification = reasonOfDissatification;
	}

	public Date getDateOfVisit() {
		return dateOfVisit;
	}

	public void setDateOfVisit(Date dateOfVisit) {
		this.dateOfVisit = dateOfVisit;
	}

	public String getTimeOfVisit() {
		return timeOfVisit;
	}

	public void setTimeOfVisit(String timeOfVisit) {
		this.timeOfVisit = timeOfVisit;
	}

	public String getSpecificDetailsOfVisitNotCompleted() {
		return specificDetailsOfVisitNotCompleted;
	}

	public void setSpecificDetailsOfVisitNotCompleted(String specificDetailsOfVisitNotCompleted) {
		this.specificDetailsOfVisitNotCompleted = specificDetailsOfVisitNotCompleted;
	}

	public String getDetailsOfProblemsInPSFList() {
		return detailsOfProblemsInPSFList;
	}

	public void setDetailsOfProblemsInPSFList(String detailsOfProblemsInPSFList) {
		this.detailsOfProblemsInPSFList = detailsOfProblemsInPSFList;
	}

	public Integer getDelayInHours() {
		return delayInHours;
	}

	public void setDelayInHours(Integer delayInHours) {
		this.delayInHours = delayInHours;
	}

	public Integer getDelayInMin() {
		return delayInMin;
	}

	public void setDelayInMin(Integer delayInMin) {
		this.delayInMin = delayInMin;
	}

	public String getSpecificDetailsOfComplaint() {
		return specificDetailsOfComplaint;
	}

	public void setSpecificDetailsOfComplaint(String specificDetailsOfComplaint) {
		this.specificDetailsOfComplaint = specificDetailsOfComplaint;
	}

	public String getAfterServiceSatisfication() {
		return afterServiceSatisfication;
	}

	public void setAfterServiceSatisfication(String afterServiceSatisfication) {
		this.afterServiceSatisfication = afterServiceSatisfication;
	}

	public String getRecentServiceSatisfication() {
		return recentServiceSatisfication;
	}

	public void setRecentServiceSatisfication(String recentServiceSatisfication) {
		this.recentServiceSatisfication = recentServiceSatisfication;
	}

	public String getAfterServiceComments() {
		return afterServiceComments;
	}

	public void setAfterServiceComments(String afterServiceComments) {
		this.afterServiceComments = afterServiceComments;
	}

	public String getRecentServiceComments() {
		return recentServiceComments;
	}

	public void setRecentServiceComments(String recentServiceComments) {
		this.recentServiceComments = recentServiceComments;
	}

	public String getIsFollowupRequired() {
		return isFollowupRequired;
	}

	public void setIsFollowupRequired(String isFollowupRequired) {
		this.isFollowupRequired = isFollowupRequired;
	}

	public String getIsFollowUpDone() {
		return isFollowUpDone;
	}

	public void setIsFollowUpDone(String isFollowUpDone) {
		this.isFollowUpDone = isFollowUpDone;
	}

	public String getVehiclePerformanceAfterService() {
		return vehiclePerformanceAfterService;
	}

	public void setVehiclePerformanceAfterService(String vehiclePerformanceAfterService) {
		this.vehiclePerformanceAfterService = vehiclePerformanceAfterService;
	}

	public String getFeedbackrating() {
		return feedbackrating;
	}

	public void setFeedbackrating(String feedbackrating) {
		this.feedbackrating = feedbackrating;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public List<UpsellLead> getUpsellLead() {
		return upsellLead;
	}

	public void setUpsellLead(List<UpsellLead> upsellLead) {
		this.upsellLead = upsellLead;
	}

	public long getUpsellCount() {
		return upsellCount;
	}

	public void setUpsellCount(long upsellCount) {
		this.upsellCount = upsellCount;
	}

	public String getQ1_CompleteSatisfication() {
		return q1_CompleteSatisfication;
	}

	public void setQ1_CompleteSatisfication(String q1_CompleteSatisfication) {
		this.q1_CompleteSatisfication = q1_CompleteSatisfication;
	}

	public String getQ2_InconvinenceDSRFunction() {
		return q2_InconvinenceDSRFunction;
	}

	public void setQ2_InconvinenceDSRFunction(String q2_InconvinenceDSRFunction) {
		this.q2_InconvinenceDSRFunction = q2_InconvinenceDSRFunction;
	}

	public String getQ3_InconvinenceDSRAssignedTo() {
		return q3_InconvinenceDSRAssignedTo;
	}

	public void setQ3_InconvinenceDSRAssignedTo(String q3_InconvinenceDSRAssignedTo) {
		this.q3_InconvinenceDSRAssignedTo = q3_InconvinenceDSRAssignedTo;
	}

	public String getQ4_InconvinenceDSRRemarks() {
		return q4_InconvinenceDSRRemarks;
	}

	public void setQ4_InconvinenceDSRRemarks(String q4_InconvinenceDSRRemarks) {
		this.q4_InconvinenceDSRRemarks = q4_InconvinenceDSRRemarks;
	}

	public String getQ5_RateExplanationOfSA() {
		return q5_RateExplanationOfSA;
	}

	public void setQ5_RateExplanationOfSA(String q5_RateExplanationOfSA) {
		this.q5_RateExplanationOfSA = q5_RateExplanationOfSA;
	}

	public String getQ6_RatePickUpProcessOfCar() {
		return q6_RatePickUpProcessOfCar;
	}

	public void setQ6_RatePickUpProcessOfCar(String q6_RatePickUpProcessOfCar) {
		this.q6_RatePickUpProcessOfCar = q6_RatePickUpProcessOfCar;
	}

	public String getQ7_RateCleanlinessOfCar() {
		return q7_RateCleanlinessOfCar;
	}

	public void setQ7_RateCleanlinessOfCar(String q7_RateCleanlinessOfCar) {
		this.q7_RateCleanlinessOfCar = q7_RateCleanlinessOfCar;
	}

	public String getQ8_RateOverAllInteraction() {
		return q8_RateOverAllInteraction;
	}

	public void setQ8_RateOverAllInteraction(String q8_RateOverAllInteraction) {
		this.q8_RateOverAllInteraction = q8_RateOverAllInteraction;
	}

	public String getQ9_FeedbackFunction() {
		return q9_FeedbackFunction;
	}

	public void setQ9_FeedbackFunction(String q9_FeedbackFunction) {
		this.q9_FeedbackFunction = q9_FeedbackFunction;
	}

	public String getQ10_FeedbackAssignedTo() {
		return q10_FeedbackAssignedTo;
	}

	public void setQ10_FeedbackAssignedTo(String q10_FeedbackAssignedTo) {
		this.q10_FeedbackAssignedTo = q10_FeedbackAssignedTo;
	}

	public String getQ11_FeedbackRemarks() {
		return q11_FeedbackRemarks;
	}

	public void setQ11_FeedbackRemarks(String q11_FeedbackRemarks) {
		this.q11_FeedbackRemarks = q11_FeedbackRemarks;
	}

	public String getQ12_FeedbackTaken() {
		return q12_FeedbackTaken;
	}

	public void setQ12_FeedbackTaken(String q12_FeedbackTaken) {
		this.q12_FeedbackTaken = q12_FeedbackTaken;
	}

	public String getQ13_IsCarPerformingWell() {
		return q13_IsCarPerformingWell;
	}

	public void setQ13_IsCarPerformingWell(String q13_IsCarPerformingWell) {
		this.q13_IsCarPerformingWell = q13_IsCarPerformingWell;
	}

	public String getqM1_SatisfiedWithQualityOfServ() {
		return qM1_SatisfiedWithQualityOfServ;
	}

	public void setqM1_SatisfiedWithQualityOfServ(String qM1_SatisfiedWithQualityOfServ) {
		this.qM1_SatisfiedWithQualityOfServ = qM1_SatisfiedWithQualityOfServ;
	}

	public String getqM2_ReasonOfAreaOfImprovement() {
		return qM2_ReasonOfAreaOfImprovement;
	}

	public void setqM2_ReasonOfAreaOfImprovement(String qM2_ReasonOfAreaOfImprovement) {
		this.qM2_ReasonOfAreaOfImprovement = qM2_ReasonOfAreaOfImprovement;
	}

	public String getqM3_SubReasonOfAreaOfImprovement1() {
		return qM3_SubReasonOfAreaOfImprovement1;
	}

	public void setqM3_SubReasonOfAreaOfImprovement1(String qM3_SubReasonOfAreaOfImprovement1) {
		this.qM3_SubReasonOfAreaOfImprovement1 = qM3_SubReasonOfAreaOfImprovement1;
	}

	public String getqM3_SubReasonOfAreaOfImprovement2() {
		return qM3_SubReasonOfAreaOfImprovement2;
	}

	public void setqM3_SubReasonOfAreaOfImprovement2(String qM3_SubReasonOfAreaOfImprovement2) {
		this.qM3_SubReasonOfAreaOfImprovement2 = qM3_SubReasonOfAreaOfImprovement2;
	}

	public String getqM4_confirmingCustomer() {
		return qM4_confirmingCustomer;
	}

	public void setqM4_confirmingCustomer(String qM4_confirmingCustomer) {
		this.qM4_confirmingCustomer = qM4_confirmingCustomer;
	}

	public String getqM4_confirmingCustomerRightTime() {
		return qM4_confirmingCustomerRightTime;
	}

	public void setqM4_confirmingCustomerRightTime(String qM4_confirmingCustomerRightTime) {
		this.qM4_confirmingCustomerRightTime = qM4_confirmingCustomerRightTime;
	}

	public Date getPsfAppointmentDate() {
		return psfAppointmentDate;
	}

	public void setPsfAppointmentDate(Date psfAppointmentDate) {
		this.psfAppointmentDate = psfAppointmentDate;
	}

	public String getPsfAppointmentTime() {
		return psfAppointmentTime;
	}

	public void setPsfAppointmentTime(String psfAppointmentTime) {
		this.psfAppointmentTime = psfAppointmentTime;
	}

	public String getPsfAddressForVisit() {
		return psfAddressForVisit;
	}

	public void setPsfAddressForVisit(String psfAddressForVisit) {
		this.psfAddressForVisit = psfAddressForVisit;
	}
	public String getqM4_RightTimeToEnquireOrderbillDate() {
		return qM4_RightTimeToEnquireOrderbillDate;
	}

	public void setqM4_RightTimeToEnquireOrderbillDate(String qM4_RightTimeToEnquireOrderbillDate) {
		this.qM4_RightTimeToEnquireOrderbillDate = qM4_RightTimeToEnquireOrderbillDate;
	}

	public String getqM1RatingOverAllAppointProcess() {
		return qM1RatingOverAllAppointProcess;
	}

	public void setqM1RatingOverAllAppointProcess(String qM1RatingOverAllAppointProcess) {
		this.qM1RatingOverAllAppointProcess = qM1RatingOverAllAppointProcess;
	}

	public String getqM2RatingTimeTakenVehiclehandOver() {
		return qM2RatingTimeTakenVehiclehandOver;
	}

	public void setqM2RatingTimeTakenVehiclehandOver(String qM2RatingTimeTakenVehiclehandOver) {
		this.qM2RatingTimeTakenVehiclehandOver = qM2RatingTimeTakenVehiclehandOver;
	}

	public String getqM3RatingServicAdvCourtesyResponse() {
		return qM3RatingServicAdvCourtesyResponse;
	}

	public void setqM3RatingServicAdvCourtesyResponse(String qM3RatingServicAdvCourtesyResponse) {
		this.qM3RatingServicAdvCourtesyResponse = qM3RatingServicAdvCourtesyResponse;
	}

	public String getqM4RatingServiceAdvJobExpl() {
		return qM4RatingServiceAdvJobExpl;
	}

	public void setqM4RatingServiceAdvJobExpl(String qM4RatingServiceAdvJobExpl) {
		this.qM4RatingServiceAdvJobExpl = qM4RatingServiceAdvJobExpl;
	}

	public String getqM5RatingCleanlinessOfDealer() {
		return qM5RatingCleanlinessOfDealer;
	}

	public void setqM5RatingCleanlinessOfDealer(String qM5RatingCleanlinessOfDealer) {
		this.qM5RatingCleanlinessOfDealer = qM5RatingCleanlinessOfDealer;
	}

	public String getqM6RatingTimelinessVehicleDelivary() {
		return qM6RatingTimelinessVehicleDelivary;
	}

	public void setqM6RatingTimelinessVehicleDelivary(String qM6RatingTimelinessVehicleDelivary) {
		this.qM6RatingTimelinessVehicleDelivary = qM6RatingTimelinessVehicleDelivary;
	}

	public String getqM7RatingFairnessOfCharge() {
		return qM7RatingFairnessOfCharge;
	}

	public void setqM7RatingFairnessOfCharge(String qM7RatingFairnessOfCharge) {
		this.qM7RatingFairnessOfCharge = qM7RatingFairnessOfCharge;
	}

	public String getqM8RatingHelpFulnessOfStaff() {
		return qM8RatingHelpFulnessOfStaff;
	}

	public void setqM8RatingHelpFulnessOfStaff(String qM8RatingHelpFulnessOfStaff) {
		this.qM8RatingHelpFulnessOfStaff = qM8RatingHelpFulnessOfStaff;
	}

	public String getqM9RatingTotalTimeRequired() {
		return qM9RatingTotalTimeRequired;
	}

	public void setqM9RatingTotalTimeRequired(String qM9RatingTotalTimeRequired) {
		this.qM9RatingTotalTimeRequired = qM9RatingTotalTimeRequired;
	}

	public String getqM10RatingQualityOfMaintenance() {
		return qM10RatingQualityOfMaintenance;
	}

	public void setqM10RatingQualityOfMaintenance(String qM10RatingQualityOfMaintenance) {
		this.qM10RatingQualityOfMaintenance = qM10RatingQualityOfMaintenance;
	}

	public String getqM11RatingCondAndCleanlinessOfVeh() {
		return qM11RatingCondAndCleanlinessOfVeh;
	}

	public void setqM11RatingCondAndCleanlinessOfVeh(String qM11RatingCondAndCleanlinessOfVeh) {
		this.qM11RatingCondAndCleanlinessOfVeh = qM11RatingCondAndCleanlinessOfVeh;
	}

	public String getqM12RatingOverAllServExp() {
		return qM12RatingOverAllServExp;
	}

	public void setqM12RatingOverAllServExp(String qM12RatingOverAllServExp) {
		this.qM12RatingOverAllServExp = qM12RatingOverAllServExp;
	}

	public String getQ14_availedPickUpGivenByDealer() {
		return q14_availedPickUpGivenByDealer;
	}

	public void setQ14_availedPickUpGivenByDealer(String q14_availedPickUpGivenByDealer) {
		this.q14_availedPickUpGivenByDealer = q14_availedPickUpGivenByDealer;
	}

	public String getQ15_explainedReferAndEarnPRGM() {
		return q15_explainedReferAndEarnPRGM;
	}

	public void setQ15_explainedReferAndEarnPRGM(String q15_explainedReferAndEarnPRGM) {
		this.q15_explainedReferAndEarnPRGM = q15_explainedReferAndEarnPRGM;
	}

	public String getQ16_RateCleanlinessOfDealerFacility() {
		return q16_RateCleanlinessOfDealerFacility;
	}

	public void setQ16_RateCleanlinessOfDealerFacility(String q16_RateCleanlinessOfDealerFacility) {
		this.q16_RateCleanlinessOfDealerFacility = q16_RateCleanlinessOfDealerFacility;
	}

	public String getQ17_RateMaintenanceAndRepair() {
		return q17_RateMaintenanceAndRepair;
	}

	public void setQ17_RateMaintenanceAndRepair(String q17_RateMaintenanceAndRepair) {
		this.q17_RateMaintenanceAndRepair = q17_RateMaintenanceAndRepair;
	}

	public String getqMC1_OverallServiceExp() {
		return qMC1_OverallServiceExp;
	}

	public void setqMC1_OverallServiceExp(String qMC1_OverallServiceExp) {
		this.qMC1_OverallServiceExp = qMC1_OverallServiceExp;
	}

	public String getqMC2_HappyWithRepairJob() {
		return qMC2_HappyWithRepairJob;
	}

	public void setqMC2_HappyWithRepairJob(String qMC2_HappyWithRepairJob) {
		this.qMC2_HappyWithRepairJob = qMC2_HappyWithRepairJob;
	}

	public String getqMC3_CoutnessHelpfulnessOfSA() {
		return qMC3_CoutnessHelpfulnessOfSA;
	}

	public void setqMC3_CoutnessHelpfulnessOfSA(String qMC3_CoutnessHelpfulnessOfSA) {
		this.qMC3_CoutnessHelpfulnessOfSA = qMC3_CoutnessHelpfulnessOfSA;
	}

	public String getqMC4_TimeTakenCompleteJob() {
		return qMC4_TimeTakenCompleteJob;
	}

	public void setqMC4_TimeTakenCompleteJob(String qMC4_TimeTakenCompleteJob) {
		this.qMC4_TimeTakenCompleteJob = qMC4_TimeTakenCompleteJob;
	}

	public String getqMC5_CostEstimateAdherance() {
		return qMC5_CostEstimateAdherance;
	}

	public void setqMC5_CostEstimateAdherance(String qMC5_CostEstimateAdherance) {
		this.qMC5_CostEstimateAdherance = qMC5_CostEstimateAdherance;
	}

	public String getqMC6_ServiceConvinenantTime() {
		return qMC6_ServiceConvinenantTime;
	}

	public void setqMC6_ServiceConvinenantTime(String qMC6_ServiceConvinenantTime) {
		this.qMC6_ServiceConvinenantTime = qMC6_ServiceConvinenantTime;
	}

	public String getqMC7_LikeToRevistDealer() {
		return qMC7_LikeToRevistDealer;
	}

	public void setqMC7_LikeToRevistDealer(String qMC7_LikeToRevistDealer) {
		this.qMC7_LikeToRevistDealer = qMC7_LikeToRevistDealer;
	}

	public String getqFordQ1() {
		return qFordQ1;
	}

	public void setqFordQ1(String qFordQ1) {
		this.qFordQ1 = qFordQ1;
	}

	public String getqFordQ2() {
		return qFordQ2;
	}

	public void setqFordQ2(String qFordQ2) {
		this.qFordQ2 = qFordQ2;
	}

	public String getqFordQ3() {
		return qFordQ3;
	}

	public void setqFordQ3(String qFordQ3) {
		this.qFordQ3 = qFordQ3;
	}

	public String getqFordQ4() {
		return qFordQ4;
	}

	public void setqFordQ4(String qFordQ4) {
		this.qFordQ4 = qFordQ4;
	}

	public String getqFordQ5() {
		return qFordQ5;
	}

	public void setqFordQ5(String qFordQ5) {
		this.qFordQ5 = qFordQ5;
	}

	public String getqFordQ6() {
		return qFordQ6;
	}

	public void setqFordQ6(String qFordQ6) {
		this.qFordQ6 = qFordQ6;
	}

	public String getqFordQ7() {
		return qFordQ7;
	}

	public void setqFordQ7(String qFordQ7) {
		this.qFordQ7 = qFordQ7;
	}

	public String getqFordQ8() {
		return qFordQ8;
	}

	public void setqFordQ8(String qFordQ8) {
		this.qFordQ8 = qFordQ8;
	}

	public String getqFordQ9() {
		return qFordQ9;
	}

	public void setqFordQ9(String qFordQ9) {
		this.qFordQ9 = qFordQ9;
	}

	public String getqFordQ10() {
		return qFordQ10;
	}

	public void setqFordQ10(String qFordQ10) {
		this.qFordQ10 = qFordQ10;
	}

	public String getqFordQ11() {
		return qFordQ11;
	}

	public void setqFordQ11(String qFordQ11) {
		this.qFordQ11 = qFordQ11;
	}

	public String getqFordQ12() {
		return qFordQ12;
	}

	public void setqFordQ12(String qFordQ12) {
		this.qFordQ12 = qFordQ12;
	}

	public String getqFordQ13() {
		return qFordQ13;
	}

	public void setqFordQ13(String qFordQ13) {
		this.qFordQ13 = qFordQ13;
	}

	public String getqFordQ14() {
		return qFordQ14;
	}

	public void setqFordQ14(String qFordQ14) {
		this.qFordQ14 = qFordQ14;
	}

	public String getqFordQ15() {
		return qFordQ15;
	}

	public void setqFordQ15(String qFordQ15) {
		this.qFordQ15 = qFordQ15;
	}

	public String getqFordQ16() {
		return qFordQ16;
	}

	public void setqFordQ16(String qFordQ16) {
		this.qFordQ16 = qFordQ16;
	}
	
	
	

}
