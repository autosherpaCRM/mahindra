package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SynchedKeySchCalls {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;
	
	
	public String firebaseKey;
	
	public String onMorekey;
	
	


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getFirebaseKey() {
		return firebaseKey;
	}


	public void setFirebaseKey(String firebaseKey) {
		this.firebaseKey = firebaseKey;
	}


	public String getOnMorekey() {
		return onMorekey;
	}


	public void setOnMorekey(String onMorekey) {
		this.onMorekey = onMorekey;
	}

	
}
