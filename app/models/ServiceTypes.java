package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class ServiceTypes {
	
	public static int STATIC_FR1=1;
	public static int STATIC_FR2=2;
	public static int STATIC_FR3=3;
	public static int STATIC_PMS=4;
	public static int STATIC_ACC_FITMENT=5;
	public static int STATIC_BODY_REPAIR=6;
	public static int STATIC_MOS=7;
	public static int STATIC_REPEAT_JOB=8;
	public static int STATIC_RUNNING_REPAIR=9;
	public static int STATIC_SERVICE_CAMP=10;
	public static int STATIC_WASHING=11;
	public static int STATIC_ON_ROAD=12;
	public static int STATIC_WARANTY=13;
	public static int STATIC_TRUE_VALUE_FR1=14;
	public static int STATIC_TRUE_VALUE_FR2=15;
	public static int STATIC_TRUE_VALUE_FR3=16;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	public int serviceId;

	@Column(length = 30)
	public String serviceTypeName;
	
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "serviceBookedType")
	public List<ServiceBooked> serviceBooked;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceTypeName() {
		return serviceTypeName;
	}

	public void setServiceTypeName(String serviceTypeName) {
		this.serviceTypeName = serviceTypeName;
	}

	public List<ServiceBooked> getServiceBooked() {
		return serviceBooked;
	}

	public void setServiceBooked(List<ServiceBooked> serviceBooked) {
		this.serviceBooked = serviceBooked;
	}
	
	

}
