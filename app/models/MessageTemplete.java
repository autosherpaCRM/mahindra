package models;

import javax.persistence.*;



/**
 * @author w-885
 *
 */
@Entity
public class MessageTemplete {
	

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;
	
	public String dealerCode;
	public String messageType;
	public String messageHeader;
	public String messageBody;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDealerCode() {
		return dealerCode;
	}
	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}
	
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getMessageHeader() {
		return messageHeader;
	}
	public void setMessageHeader(String messageHeader) {
		this.messageHeader = messageHeader;
	}
	public String getMessageBody() {
		return messageBody;
	}
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	
	

}
