/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.persistence.*;

/**
 *
 * @author W-885
 */
@Entity
public class Segment {
    
        @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
        private String name;
        private String type;
        private String dataUploadType;
        private String dataUploadLocation;
        private String upload_id;

    public String getDataUploadLocation() {
        return dataUploadLocation;
    }

    public void setDataUploadLocation(String dataUploadLocation) {
        this.dataUploadLocation = dataUploadLocation;
    }
        @ManyToOne(fetch = FetchType.EAGER)
        private Customer customer;

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }        

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDataUploadType() {
            return dataUploadType;
        }

        public void setDataUploadType(String dataUploadType) {
            this.dataUploadType = dataUploadType;
        }

		public String getUpload_id() {
			return upload_id;
		}

		public void setUpload_id(String upload_id) {
			this.upload_id = upload_id;
		}
        
        
}
