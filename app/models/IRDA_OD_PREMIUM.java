package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class IRDA_OD_PREMIUM {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	public String zone;
	
	public String vehicleAge;
	
	public String cubicCapacity;
	
	public double odPercentage;
	
	public double thirdPartyPremium;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getVehicleAge() {
		return vehicleAge;
	}

	public void setVehicleAge(String vehicleAge) {
		this.vehicleAge = vehicleAge;
	}

	public String getCubicCapacity() {
		return cubicCapacity;
	}

	public void setCubicCapacity(String cubicCapacity) {
		this.cubicCapacity = cubicCapacity;
	}

	public double getOdPercentage() {
		return odPercentage;
	}

	public void setOdPercentage(double odPercentage) {
		this.odPercentage = odPercentage;
	}

	public double getThirdPartyPremium() {
		return thirdPartyPremium;
	}

	public void setThirdPartyPremium(double thirdPartyPremium) {
		this.thirdPartyPremium = thirdPartyPremium;
	}	
	
	
}
