package models;

import javax.persistence.*;

@Entity
public class EmailTemplate {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public String emailTemplate;
	public String emailSubject;
	public String emailGreetings;
	public String emailSign;
	public String emailId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmailTemplate() {
		return emailTemplate;
	}

	public void setEmailTemplate(String emailTemplate) {
		this.emailTemplate = emailTemplate;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailGreetings() {
		return emailGreetings;
	}

	public void setEmailGreetings(String emailGreetings) {
		this.emailGreetings = emailGreetings;
	}

	public String getEmailSign() {
		return emailSign;
	}

	public void setEmailSign(String emailSign) {
		this.emailSign = emailSign;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

}
