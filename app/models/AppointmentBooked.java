package models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class AppointmentBooked {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long appointmentId;
	
	
	@Column(length = 70)
	public String renewalType;
	
	@Column(length = 70)
	public String renewalMode;
	
	@Temporal(TemporalType.DATE)
	public Date appointmentDate;
	
	@Column(length = 70)
	public String typeOfPickup;
	
	@Column(length = 200)
	public String addressOfVisit;
	
	@Column(length = 10)
	public String appointmentFromTime;
	
	@Column(length = 10)
	public String appointmentToTime;
	
	@Column(length = 60)
	public String dsa;
	
	public double premiumwithTax;
	
	@Column(length = 100)
	public String nomineeName;
	
	@Column(length = 100)
	public String nomineeAge;
	
	@Column(length = 100)
	public String nomineeRelationWithOwner;
	
	@Column(length = 100)
	public String appointeeName;
	
	@Column(length = 300)
	public String insuranceCompany;
	
	@Column(length = 100)
	public String insuranceAgentData;
	
	public String PremiumYes;
	
	@ManyToOne
	public InsuranceAgent insuranceAgent;
	
	@ManyToOne
	public ShowRooms showRooms;
	
	@ManyToOne
	public Customer customer;
	
	
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "appointmentBooked")
	public List<CallInteraction>  callInteraction;
	
	
	@OneToOne
	public CallDispositionData insuranceBookStatus;

	public long getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(long appointmentId) {
		this.appointmentId = appointmentId;
	}	

	public String getRenewalType() {
		return renewalType;
	}

	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}

	public String getRenewalMode() {
		return renewalMode;
	}

	public void setRenewalMode(String renewalMode) {
		this.renewalMode = renewalMode;
	}

	public Date getAppointmentDate() {
		return appointmentDate;
	}
	
	
	public String getAppointmentDateStr() {
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringDate = formatter.format(appointmentDate);
		return stringDate;
	}

	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}	

	public String getTypeOfPickup() {
		return typeOfPickup;
	}

	public void setTypeOfPickup(String typeOfPickup) {
		this.typeOfPickup = typeOfPickup;
	}

	public String getAddressOfVisit() {
		return addressOfVisit;
	}

	public void setAddressOfVisit(String addressOfVisit) {
		this.addressOfVisit = addressOfVisit;
	}

	public String getAppointmentFromTime() {
		return appointmentFromTime;
	}

	public void setAppointmentFromTime(String appointmentFromTime) {
		this.appointmentFromTime = appointmentFromTime;
	}

	public String getAppointmentToTime() {
		return appointmentToTime;
	}

	public void setAppointmentToTime(String appointmentToTime) {
		this.appointmentToTime = appointmentToTime;
	}

	public InsuranceAgent getInsuranceAgent() {
		return insuranceAgent;
	}

	public void setInsuranceAgent(InsuranceAgent insuranceAgent) {
		this.insuranceAgent = insuranceAgent;
	}

	
	public String getDsa() {
		return dsa;
	}

	public void setDsa(String dsa) {
		this.dsa = dsa;
	}

	public double getPremiumwithTax() {
		return premiumwithTax;
	}

	public void setPremiumwithTax(double premiumwithTax) {
		this.premiumwithTax = premiumwithTax;
	}

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public String getNomineeAge() {
		return nomineeAge;
	}

	public void setNomineeAge(String nomineeAge) {
		this.nomineeAge = nomineeAge;
	}

	public String getNomineeRelationWithOwner() {
		return nomineeRelationWithOwner;
	}

	public void setNomineeRelationWithOwner(String nomineeRelationWithOwner) {
		this.nomineeRelationWithOwner = nomineeRelationWithOwner;
	}

	public String getAppointeeName() {
		return appointeeName;
	}

	public void setAppointeeName(String appointeeName) {
		this.appointeeName = appointeeName;
	}

	public ShowRooms getShowRooms() {
		return showRooms;
	}

	public void setShowRooms(ShowRooms showRooms) {
		this.showRooms = showRooms;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public String getInsuranceAgentData() {
		return insuranceAgentData;
	}

	public void setInsuranceAgentData(String insuranceAgentData) {
		this.insuranceAgentData = insuranceAgentData;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	
	public String getPremiumYes() {
		return PremiumYes;
	}

	public void setPremiumYes(String premiumYes) {
		PremiumYes = premiumYes;
	}

	public List<CallInteraction> getCallInteraction() {
		return callInteraction;
	}

	public void setCallInteraction(List<CallInteraction> callInteraction) {
		this.callInteraction = callInteraction;
	}

	public CallDispositionData getInsuranceBookStatus() {
		return insuranceBookStatus;
	}

	public void setInsuranceBookStatus(CallDispositionData insuranceBookStatus) {
		this.insuranceBookStatus = insuranceBookStatus;
	}
	
	
	

}
