package models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SqlResultSetMappings({ 
@SqlResultSetMapping(name = "customerSearchByIndv", classes = {
		@ConstructorResult(targetClass = CustomerSearchResult.class, columns = { @ColumnResult(name = "cid"),
				@ColumnResult(name = "vehicle_id"), @ColumnResult(name = "customerName"),
				@ColumnResult(name = "vehicleRegNo"), @ColumnResult(name = "customerCategory"),
				@ColumnResult(name = "phoneNumber"), @ColumnResult(name = "nextServicedate"),
				@ColumnResult(name = "model"), @ColumnResult(name = "variant"),
				@ColumnResult(name = "serviceId", type=Long.class) })

		}) 


})

@Entity
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@Column(length = 30)
	public String customerId;
	@Column(length = 30)
	public String customerFName;
	@Column(length = 30)
	public String customerLName;
	@Column(length = 70)
	public String customerName;
	@Column(length = 30)
	public String customerCategory;
	@Column(length = 30)
	public String createdBy;

	@Temporal(TemporalType.DATE)
	public Date createdDate;

	@Column(length = 30)
	public String customerEmail;
	@Column(length = 30)
	public String dob;
	
	public String getDobStr(){
		if(dob!=null && dob!="" ){
		
		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDateNew = new Date();
		try {
			fromDateNew = dmyFormat.parse(dob);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringDate = formatter.format(fromDateNew);
		return stringDate;
		
		}else{
			
			return "";
		}
		
		
	}
	
	@Column(length = 30)
	public String anniversary_date;
	
	public String getAnniversaryDateStr(){
		if(anniversary_date!=null && anniversary_date!=""){
		
		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDateNew = new Date();
		try {
			fromDateNew = dmyFormat.parse(anniversary_date);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringDate = formatter.format(fromDateNew);
		return stringDate;
		
		}else{
			
			return "";
		}
		
		
	}
	
	
	
	
	@Column(length = 30)
	public String mode_of_contact;
	@Column(length = 30)
	public String preferred_day;
	@Column(length = 30)
	public String preferred_time_start;
	@Column(length = 30)
	public String preferred_time_end;

	@Column(length = 300)
	public String comments;

	public boolean doNotDisturb;
	public String salutation;
	public String sourceType;

	@Column(length = 100)
	public String userDriver;

	@Column(length = 100)
	public String dbVehRegNo;

	@Column(length = 100)
	public String regNoSMR;

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<AssignedInteraction> assignedInteraction;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<Insurance> insurances;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<Phone> phones;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<Email> emails;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<Address> addresses;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<Vehicle> vehicles;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<ServiceBooked> servicesBooked;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<CallInteraction> callInteraction;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<Complaint> complaint;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<Segment> segment;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<PSFAssignedInteraction> psfAssignedInteraction;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<SMSInteraction> sentSMSes;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	public List<InsuranceAssignedInteraction> insuranceAssignedInteraction;

	public List<PSFAssignedInteraction> getPsfAssignedInteraction() {
		return psfAssignedInteraction;
	}

	public void setPsfAssignedInteraction(List<PSFAssignedInteraction> psfAssignedInteraction) {
		this.psfAssignedInteraction = psfAssignedInteraction;
	}

	public String upload_id;

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	public String getConcatinatedName() {

		StringBuffer buff = new StringBuffer();
		buff.append(customerFName).append("\n");
		buff.append(customerLName).append("\n");

		return buff.toString();

	}

	public Address getPreferredAdress() {

		Address preferred = null;

		for (Address address : addresses) {

			if (address.isIsPreferred()) {
				preferred = address;
			}
		}
		return preferred;
	}

	public String getConcatinatedPrefAddree() {

		StringBuffer buff = new StringBuffer();

		Address preferred = null;

		for (Address address : addresses) {

			if (address.isIsPreferred()) {

				if (address.getAddressLine1() != null) {
					buff.append(address.getAddressLine1()).append("\n");
				}
				if (address.getAddressLine2() != null) {
					buff.append(address.getAddressLine2()).append("\n");
				}
				if (address.getAddressLine3() != null) {
					buff.append(address.getAddressLine3()).append("\n");
				}
				if (address.getCity() != null) {
					buff.append(address.getCity()).append("\n");
				}
				if (address.getPincode() != 0) {
					buff.append(address.getPincode()).append("\n");
				}
				if (address.getState() != null) {
					buff.append(address.getState()).append("\n");
				}
				if (address.getCountry() != null) {
					buff.append(address.getCountry()).append("\n");
				}

			}
		}

		return buff.toString();

	}

	public Email getPreferredEmail() {

		Email preferred = null;

		for (Email email : emails) {

			if (email.isPreferredEmail) {
				preferred = email;
			}
		}
		return preferred;
	}

	public String getOfficeAddress() {
		Address office = null;

		for (Address address : addresses) {
			if (address.getAddressType() == Address.ADDRESS_TYPE_OFFICE) {
				office = address;
			}
		}
		return office.toString();
	}

	public Address getOfficeAddressData() {
		Address office = null;

		for (Address address : addresses) {
			if (address.getAddressType() == Address.ADDRESS_TYPE_OFFICE) {
				office = address;
			}
		}
		return office;
	}

	public String getResidenceAddress() {
		Address residence = null;

		for (Address address : addresses) {
			if (address.getAddressType() == Address.ADDRESS_TYPE_RESIDENCE) {
				residence = address;
			}
		}
		return residence.toString();

	}

	public Address getResidenceAddressData() {
		Address residence = null;

		for (Address address : addresses) {
			if (address.getAddressType() == Address.ADDRESS_TYPE_RESIDENCE) {
				residence = address;
			}
		}
		return residence;
	}

	public Address getPermanentAddressData() {
		Address permanent = null;

		for (Address address : addresses) {
			if (address.getAddressType() == Address.ADDRESS_TYPE_OTHERS) {
				permanent = address;
			}
		}
		return permanent;
	}

	public Phone getPreferredPhone() {

		Phone preferred = null;
		for (Phone phone : phones) {
			if (phone.isPreferredPhone) {
				preferred = phone;
			}
		}
		return preferred;
	}

	public List<Phone> getNonPreferredPhones() {
		List<Phone> phones = new ArrayList<Phone>();

		for (Phone phone : phones) {
			if (!phone.isPreferredPhone) {
				phones.add(phone);
			}
		}
		return phones;
	}

	/*public List<Phone> getUniquePhones() {
		List<Phone> phones = getPhones();

		for (Phone phone : phones) {
			phones.add(phone);
		}
		Set<Phone> noDups = new HashSet<Phone>();
		noDups.addAll(phones);
		return new ArrayList<Phone>(noDups);
	}
*/
	public List<Segment> getSegment() {
		return segment;
	}

	public void setSegment(List<Segment> segment) {
		this.segment = segment;
	}

	public String getCustomerCategory() {
		return customerCategory;
	}

	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustomerFName() {
		return customerFName;
	}

	public void setCustomerFName(String customerFName) {
		this.customerFName = customerFName;
	}

	public String getCustomerLName() {
		return customerLName;
	}

	public void setCustomerLName(String customerLName) {
		this.customerLName = customerLName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public List<Complaint> getComplaints() {
		return complaint;
	}

	public void setComplaints(List<Complaint> complaint) {
		this.complaint = complaint;
	}

	public List<AssignedInteraction> getAssignedInteractions() {
		return assignedInteraction;
	}

	public void setAssignedInteractions(List<AssignedInteraction> assignedInteraction) {
		this.assignedInteraction = assignedInteraction;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public List<Insurance> getInsurances() {
		return insurances;
	}

	public void setInsurances(List<Insurance> insurance) {
		this.insurances = insurance;
	}

	public String getAnniversary_date() {
		return anniversary_date;
	}

	public void setAnniversary_date(String anniversary_date) {
		this.anniversary_date = anniversary_date;
	}

	public String getMode_of_contact() {
		return mode_of_contact;
	}

	public void setMode_of_contact(String mode_of_contact) {
		this.mode_of_contact = mode_of_contact;
	}

	public String getPreferred_day() {
		return preferred_day;
	}

	public void setPreferred_day(String preferred_day) {
		this.preferred_day = preferred_day;
	}

	public String getPreferred_time_start() {
		return preferred_time_start;
	}

	public void setPreferred_time_start(String preferred_time_start) {
		this.preferred_time_start = preferred_time_start;
	}

	public String getPreferred_time_end() {
		return preferred_time_end;
	}

	public void setPreferred_time_end(String preferred_time_end) {
		this.preferred_time_end = preferred_time_end;
	}

	public List<Phone> getPhones() {
		return phones;
	}

	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public List<Vehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	public List<ServiceBooked> getServicesBooked() {
		return servicesBooked;
	}

	public void setServicesBooked(List<ServiceBooked> servicesBooked) {
		this.servicesBooked = servicesBooked;
	}

	public List<CallInteraction> getCallInteractions() {
		return callInteraction;
	}

	public void setCallInteractions(List<CallInteraction> callInteraction) {
		this.callInteraction = callInteraction;
	}

	public boolean isDoNotDisturb() {
		return doNotDisturb;
	}

	public void setDoNotDisturb(boolean doNotDisturb) {
		this.doNotDisturb = doNotDisturb;
	}

	public List<Email> getEmails() {
		return emails;
	}

	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}

	public List<SMSInteraction> getSentSMSes() {
		return sentSMSes;
	}

	public void setSentSMSes(List<SMSInteraction> sentSMSes) {
		this.sentSMSes = sentSMSes;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getUserDriver() {
		return userDriver;
	}

	public void setUserDriver(String userDriver) {
		this.userDriver = userDriver;
	}

	public List<InsuranceAssignedInteraction> getInsuranceAssignedInteraction() {
		return insuranceAssignedInteraction;
	}

	public void setInsuranceAssignedInteraction(List<InsuranceAssignedInteraction> insuranceAssignedInteraction) {
		this.insuranceAssignedInteraction = insuranceAssignedInteraction;
	}

	public String getDbVehRegNo() {
		return dbVehRegNo;
	}

	public void setDbVehRegNo(String dbVehRegNo) {
		this.dbVehRegNo = dbVehRegNo;
	}

	public String getRegNoSMR() {
		return regNoSMR;
	}

	public void setRegNoSMR(String regNoSMR) {
		this.regNoSMR = regNoSMR;
	}

}
