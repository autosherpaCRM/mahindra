package models;

import java.text.DecimalFormat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import controllers.webmodels.AssignedListForCRE;
import controllers.webmodels.CallLogAjaxLoadInsurance;
import controllers.webmodels.CallLogDispositionLoadInsurance;
import controllers.webmodels.CallRecordingDataTable;
import controllers.webmodels.ChangeAssignment;
import controllers.webmodels.FollowUpNotificationModel;
import controllers.webmodels.MediaModel;
import controllers.webmodels.PSFFollowupNotificationModel;
import controllers.webmodels.ReportSummary;

@SqlResultSetMappings({ @SqlResultSetMapping(name = "CallLogAjaxLoadInsurance", classes = {
		@ConstructorResult(targetClass = CallLogAjaxLoadInsurance.class, columns = {
				@ColumnResult(name = "campaignName", type = String.class),
				@ColumnResult(name = "customerId", type = String.class),
				@ColumnResult(name = "customer_name", type = String.class),
				@ColumnResult(name = "phoneNumber", type = String.class),
				@ColumnResult(name = "vehicle_RegNo", type = String.class),
				@ColumnResult(name = "renewalType", type = String.class),
				@ColumnResult(name = "policy_due_month", type = String.class),
				@ColumnResult(name = "duedate", type = java.sql.Date.class),
				@ColumnResult(name = "last_insuirancecompanyname", type = String.class),
				@ColumnResult(name = "customer_id", type = Long.class),
				@ColumnResult(name = "vehicle_vehicle_id", type = Long.class),
				@ColumnResult(name = "wyzUser_id", type = Long.class),
				@ColumnResult(name = "assigned_intercation_id", type = Long.class)

		})

		}), @SqlResultSetMapping(name = "CallLogDispositionLoadInsurance", classes = {
				@ConstructorResult(targetClass = CallLogDispositionLoadInsurance.class, columns = {

						@ColumnResult(name = "callinteraction_id", type = Long.class),
						@ColumnResult(name = "customer_name", type = String.class),
						@ColumnResult(name = "mobile_number", type = String.class),
						@ColumnResult(name = "vehicle_RegNo", type = String.class),
						@ColumnResult(name = "duedate", type = java.sql.Date.class),
						@ColumnResult(name = "dispoDate", type = String.class),
						@ColumnResult(name = "renewalType", type = String.class),
						@ColumnResult(name = "last_disposition", type = String.class),
						@ColumnResult(name = "dispoTime", type = String.class),
						@ColumnResult(name = "appointmentToTime", type = String.class),
						@ColumnResult(name = "reason", type = String.class),
						@ColumnResult(name = "vehicle_id", type = Long.class),
						@ColumnResult(name = "customer_id", type = Long.class) })

		}),
		@SqlResultSetMapping(name = "CallLogAjaxLoadInsuranceAssignedLog", classes = {
				@ConstructorResult(targetClass = CallLogAjaxLoadInsurance.class, columns = {
						@ColumnResult(name = "campaign", type = String.class),
						@ColumnResult(name = "customerName", type = String.class),
						@ColumnResult(name = "phone", type = String.class),
						@ColumnResult(name = "RegNo", type = String.class),
						@ColumnResult(name = "chassisNo", type = String.class),
						@ColumnResult(name = "policyDueMonth", type = String.class),
						@ColumnResult(name = "policyDueDate", type = java.sql.Date.class),
						@ColumnResult(name = "NextRenewalType", type = String.class),
						@ColumnResult(name = "lastInsuranceCompany", type = String.class),
						@ColumnResult(name = "insuranceassigned_id", type = Long.class),
						@ColumnResult(name = "customer_id", type = Long.class),
						@ColumnResult(name = "vehicle_vehicle_id", type = Long.class)

				})

		}), @SqlResultSetMapping(name = "CallLogDispositionLoadInsuranceContacts", classes = { @ConstructorResult(targetClass = CallLogDispositionLoadInsurance.class, columns = {

				@ColumnResult(name = "campaignName", type = String.class),
				@ColumnResult(name = "call_date", type = java.sql.Date.class),
				@ColumnResult(name = "customer_name", type = String.class),
				@ColumnResult(name = "Mobile_number", type = String.class),
				@ColumnResult(name = "vehicle_RegNo", type = String.class),
				@ColumnResult(name = "appointmentDate", type = java.sql.Date.class),
				@ColumnResult(name = "appointmentTime", type = String.class),
				@ColumnResult(name = "renewalType", type = String.class),
				@ColumnResult(name = "appointmentstatus", type = String.class),
				@ColumnResult(name = "insurance_agent", type = String.class),
				@ColumnResult(name = "Last_disposition", type = String.class),
				@ColumnResult(name = "reason", type = String.class),
				@ColumnResult(name = "followUpDate", type = String.class),
				@ColumnResult(name = "followUpTime", type = String.class),
				@ColumnResult(name = "policyDueDate", type = java.sql.Date.class),
				@ColumnResult(name = "vehicle_id", type = Long.class),
				@ColumnResult(name = "customer_id", type = Long.class) })

		}),

		@SqlResultSetMapping(name = "CallLogDispositionLoadInsuranceNonContacts", classes = {
				@ConstructorResult(targetClass = CallLogDispositionLoadInsurance.class, columns = {
						@ColumnResult(name = "droppedCount", type = Long.class),
						@ColumnResult(name = "campaignName", type = String.class),
						@ColumnResult(name = "call_date", type = java.sql.Date.class),
						@ColumnResult(name = "customer_name", type = String.class),
						@ColumnResult(name = "Mobile_number", type = String.class),
						@ColumnResult(name = "vehicle_RegNo", type = String.class),
						@ColumnResult(name = "appointmentDate", type = java.sql.Date.class),
						@ColumnResult(name = "appointmentTime", type = String.class),
						@ColumnResult(name = "renewalType", type = String.class),
						@ColumnResult(name = "appointmentstatus", type = String.class),
						@ColumnResult(name = "insurance_agent", type = String.class),
						@ColumnResult(name = "Last_disposition", type = String.class),
						@ColumnResult(name = "reason", type = String.class),
						@ColumnResult(name = "followUpDate", type = String.class),
						@ColumnResult(name = "followUpTime", type = String.class),
						@ColumnResult(name = "policyDueDate", type = java.sql.Date.class),
						@ColumnResult(name = "vehicle_id", type = Long.class),
						@ColumnResult(name = "customer_id", type = Long.class) })

		}), @SqlResultSetMapping(name = "ChangeAssignment", classes = { @ConstructorResult(targetClass = ChangeAssignment.class, columns = {

				@ColumnResult(name = "CreName", type = String.class),
				@ColumnResult(name = "finalDisposition_id", type = String.class),
				@ColumnResult(name = "customer_name", type = String.class),
				@ColumnResult(name = "id", type = Long.class),
				@ColumnResult(name = "Mobile_number", type = String.class),
				@ColumnResult(name = "vehicle_RegNo", type = String.class),
				@ColumnResult(name = "vehical_Id", type = Long.class),
				@ColumnResult(name = "customer_id", type = Long.class),
				@ColumnResult(name = "wyzUser_id", type = Long.class),
				@ColumnResult(name = "uplodedCurrentDate", type = java.sql.Date.class),
				@ColumnResult(name = "campaign_id", type = Long.class) })

		}), @SqlResultSetMapping(name = "AssignedListForCRE", classes = { @ConstructorResult(targetClass = AssignedListForCRE.class, columns = {

				@ColumnResult(name = "serviceType", type = String.class),
				@ColumnResult(name = "psfinteraction_id", type = Long.class),
				@ColumnResult(name = "customer_name", type = String.class),
				@ColumnResult(name = "Mobile_number", type = String.class),
				@ColumnResult(name = "vehicle_RegNo", type = String.class),
				@ColumnResult(name = "variant", type = String.class),
				@ColumnResult(name = "veh_model", type = String.class),
				@ColumnResult(name = "customer_id", type = Long.class),
				@ColumnResult(name = "vehicle_vehicle_id", type = Long.class),
				@ColumnResult(name = "service_id", type = Long.class),
				@ColumnResult(name = "nextservicedate", type = String.class),
				@ColumnResult(name = "category", type = String.class),
				@ColumnResult(name = "assigninteraction_id", type = Long.class) })

		}), @SqlResultSetMapping(name = "AssignedListForCRECampSMR", classes = { @ConstructorResult(targetClass = AssignedListForCRE.class, columns = { @ColumnResult(name = "serviceType", type = String.class), @ColumnResult(name = "customer_name", type = String.class), @ColumnResult(name = "Mobile_number", type = String.class), @ColumnResult(name = "vehicle_RegNo", type = String.class), @ColumnResult(name = "variant", type = String.class), @ColumnResult(name = "veh_model", type = String.class), @ColumnResult(name = "customer_id", type = Long.class), @ColumnResult(name = "vehicle_vehicle_id", type = Long.class), @ColumnResult(name = "nextservicedate", type = String.class), @ColumnResult(name = "category", type = String.class), @ColumnResult(name = "assigninteraction_id", type = Long.class) })

		}), @SqlResultSetMapping(name = "FollowUpNotificationModel", classes = { @ConstructorResult(targetClass = FollowUpNotificationModel.class, columns = {

				@ColumnResult(name = "followUpTime", type = String.class),
				@ColumnResult(name = "callInteraction_id", type = Long.class),
				@ColumnResult(name = "customerName", type = String.class),
				@ColumnResult(name = "customer_id", type = Long.class),
				@ColumnResult(name = "vehicle_vehicle_id", type = Long.class) })

		}), @SqlResultSetMapping(name = "PSFFollowupNotificationModel", classes = { @ConstructorResult(targetClass = PSFFollowupNotificationModel.class, columns = { @ColumnResult(name = "customer_id", type = Long.class), @ColumnResult(name = "customer_name", type = String.class), @ColumnResult(name = "vehicle_id", type = Long.class), @ColumnResult(name = "callinteraction_id", type = Long.class), @ColumnResult(name = "psfFollowUpTime", type = String.class), @ColumnResult(name = "campaign_id", type = Long.class) })

		}), @SqlResultSetMapping(name = "MediaModel", classes = { @ConstructorResult(targetClass = MediaModel.class, columns = { @ColumnResult(name = "mediaFileLob", type = byte[].class) })

		}), @SqlResultSetMapping(name = "CallRecordingDataTable", 
		classes = { @ConstructorResult(targetClass = CallRecordingDataTable.class, columns = { 
				@ColumnResult(name = "callinteraction_id", type = Long.class), 
				@ColumnResult(name = "customer_id", type = Long.class), 
				@ColumnResult(name = "vehicle_vehicle_id", type = Long.class), 
				@ColumnResult(name = "callDate", type = java.sql.Date.class), 
				@ColumnResult(name = "callTime", type = String.class), 
				@ColumnResult(name = "callType", type = String.class), 
				@ColumnResult(name = "isCallinitaited", type = String.class),
				@ColumnResult(name = "userName", type = String.class), 
				@ColumnResult(name = "campaignName", type = String.class), 
				@ColumnResult(name = "customerName", type = String.class), 
				@ColumnResult(name = "phoneNumber", type = String.class), 
				@ColumnResult(name = "vehicleRegNo", type = String.class),
				@ColumnResult(name = "chassisNo", type = String.class),
				@ColumnResult(name = "Disposition", type = String.class),
				@ColumnResult(name = "media", type = String.class) })

		}) , @SqlResultSetMapping(name = "ReportSummary", 
		classes = { @ConstructorResult(targetClass = ReportSummary.class, columns = { 
				@ColumnResult(name = "Date", type = java.sql.Date.class), 
				@ColumnResult(name = "day", type = String.class), 
				@ColumnResult(name = "Reportdate", type = String.class), 
				@ColumnResult(name = "count", type = String.class), 
				@ColumnResult(name = "WorkshopOrCity", type = String.class)
				 })

		})})

@Entity
public class Insurance {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@Temporal(TemporalType.DATE)
	public Date policyDueDate;
	@Column(length = 30)
	public String lastRenewalBy;

	@Column(length = 100)
	public String renewalType;

	@Column(length = 50)
	public String lastCoverNoteNumber;
	@Column(length = 100)
	public String insuranceCompanyName;
	@Column(length = 50)
	public String coverageFromTo;
	@Column(length = 30)
	public double idv;

	@Column(length = 30)
	public String ncBPercentage;
	public float ncBAmount;

	@Column(length = 30)
	public String odPercentage;

	public double odAmount;
	public double premiumAmountBeforeTax;
	public double premiumAmountAfterTax;

	@Column(length = 30)
	public String serviceTax;

	@Column(length = 60)
	public String policyNo;
	@Column(length = 50)
	public String coveragePeriod;
	@Column(length = 30)
	public String discountPercentage;

	@Column(length = 30)
	public String typeCode;

	public double ODpremium;

	public double liabilityPremium;

	@Column(length = 30)
	public String add_ON_Premium;

	public boolean isCurrent;

	public double exShowroomPrice;

	@Column(length = 60)
	public String discValue;

	public double totalODpremium;

	@Column(length = 30)
	public String add_ON_CoverPercentage;

	@Column(length = 30)
	public String cubicCapacity;

	@Column(length = 30)
	public String vehicleAge;

	@Column(length = 30)
	public String vehicleCity;

	@Column(length = 30)
	public String vehicleZone;

	// New parameters

	@Temporal(TemporalType.DATE)
	public Date policyIssueDate;

	@Column(length = 100)
	public String policyType;

	@Column(length = 100)
	public String classType;

	@Column(length = 100)
	public String aPPremium;

	@Column(length = 100)
	public String pAPremium;

	@Column(length = 100)
	public String legalLiability;

	@Column(length = 100)
	public String netLiability;

	@Column(length = 100)
	public String addOn;

	@Column(length = 100)
	public String otherPremium;

	@Column(length = 100)
	public String grossPremium;

	@Column(length = 100)
	public String dealer;

	@ManyToOne(cascade = CascadeType.ALL)
	public Customer customer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vehicle_id", nullable = true)
	public Vehicle vehicle;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "insurance")
	public List<InsuranceAssignedInteraction> insuranceAssignedInteraction;

	public String upload_id;

	public String getPolicyDueDateStr() {

		if (policyDueDate != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate = formatter.format(policyDueDate);
			return stringDate;
		} else {

			return "";
		}
	}

	public String getNcBAmountStr() {

		DecimalFormat myformatter = new DecimalFormat("#.00");
		String formattedString = myformatter.format(ncBAmount);

		return formattedString;

	}

	public String getOdPercentageStr() {
		if (odPercentage != null) {

			DecimalFormat myformatter = new DecimalFormat("#.00");
			String formattedString = myformatter.format(odPercentage);
			return formattedString;
		} else {

			return "";
		}
	}

	public String getOdAmountStr() {
		DecimalFormat myformatter = new DecimalFormat("#.00");
		String formattedString = myformatter.format(odAmount);

		return formattedString;
	}

	public String getPremiumAmountStr() {
		DecimalFormat myformatter = new DecimalFormat("#.00");
		String formattedString = myformatter.format(premiumAmountBeforeTax);

		return formattedString;
	}

	public String getPremiumAmountAfterTaxStr() {
		DecimalFormat myformatter = new DecimalFormat("#.00");
		String formattedString = myformatter.format(premiumAmountAfterTax);

		return formattedString;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getPolicyDueDate() {
		return policyDueDate;
	}

	public void setPolicyDueDate(Date policyDueDate) {
		this.policyDueDate = policyDueDate;
	}

	public String getLastRenewalBy() {
		return lastRenewalBy;
	}

	public void setLastRenewalBy(String lastRenewalBy) {
		this.lastRenewalBy = lastRenewalBy;
	}

	public String getRenewalType() {
		return renewalType;
	}

	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}

	public String getLastCoverNoteNumber() {
		return lastCoverNoteNumber;
	}

	public void setLastCoverNoteNumber(String lastCoverNoteNumber) {
		this.lastCoverNoteNumber = lastCoverNoteNumber;
	}

	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}

	public void setInsuranceCompanyName(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}

	public String getCoverageFromTo() {
		return coverageFromTo;
	}

	public void setCoverageFromTo(String coverageFromTo) {
		this.coverageFromTo = coverageFromTo;
	}

	public double getIdv() {
		return idv;
	}

	public void setIdv(double idv) {
		this.idv = idv;
	}

	public String getNcBPercentage() {
		return ncBPercentage;
	}

	public void setNcBPercentage(String ncBPercentage) {
		this.ncBPercentage = ncBPercentage;
	}

	public float getNcBAmount() {
		return ncBAmount;
	}

	public void setNcBAmount(float ncBAmount) {
		this.ncBAmount = ncBAmount;
	}

	public String getOdPercentage() {
		return odPercentage;
	}

	public void setOdPercentage(String odPercentage) {
		this.odPercentage = odPercentage;
	}

	public double getOdAmount() {
		return odAmount;
	}

	public void setOdAmount(double odAmount) {
		this.odAmount = odAmount;
	}

	public double getPremiumAmountBeforeTax() {
		return premiumAmountBeforeTax;
	}

	public void setPremiumAmountBeforeTax(double premiumAmountBeforeTax) {
		this.premiumAmountBeforeTax = premiumAmountBeforeTax;
	}

	public double getPremiumAmountAfterTax() {
		return premiumAmountAfterTax;
	}

	public void setPremiumAmountAfterTax(double premiumAmountAfterTax) {
		this.premiumAmountAfterTax = premiumAmountAfterTax;
	}

	public String getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getCoveragePeriod() {
		return coveragePeriod;
	}

	public void setCoveragePeriod(String coveragePeriod) {
		this.coveragePeriod = coveragePeriod;
	}

	public String getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(String discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public double getODpremium() {
		return ODpremium;
	}

	public void setODpremium(double oDpremium) {
		ODpremium = oDpremium;
	}

	public double getLiabilityPremium() {
		return liabilityPremium;
	}

	public void setLiabilityPremium(double liabilityPremium) {
		this.liabilityPremium = liabilityPremium;
	}

	public String getAdd_ON_Premium() {
		return add_ON_Premium;
	}

	public void setAdd_ON_Premium(String add_ON_Premium) {
		this.add_ON_Premium = add_ON_Premium;
	}

	public boolean isCurrent() {
		return isCurrent;
	}

	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	public List<InsuranceAssignedInteraction> getInsuranceAssignedInteraction() {
		return insuranceAssignedInteraction;
	}

	public void setInsuranceAssignedInteraction(List<InsuranceAssignedInteraction> insuranceAssignedInteraction) {
		this.insuranceAssignedInteraction = insuranceAssignedInteraction;
	}

	public double getExShowroomPrice() {
		return exShowroomPrice;
	}

	public void setExShowroomPrice(double exShowroomPrice) {
		this.exShowroomPrice = exShowroomPrice;
	}

	public String getDiscValue() {
		return discValue;
	}

	public void setDiscValue(String discValue) {
		this.discValue = discValue;
	}

	public double getTotalODpremium() {
		return totalODpremium;
	}

	public void setTotalODpremium(double totalODpremium) {
		this.totalODpremium = totalODpremium;
	}

	public String getAdd_ON_CoverPercentage() {
		return add_ON_CoverPercentage;
	}

	public void setAdd_ON_CoverPercentage(String add_ON_CoverPercentage) {
		this.add_ON_CoverPercentage = add_ON_CoverPercentage;
	}

	public String getCubicCapacity() {
		return cubicCapacity;
	}

	public void setCubicCapacity(String cubicCapacity) {
		this.cubicCapacity = cubicCapacity;
	}

	public String getVehicleAge() {
		return vehicleAge;
	}

	public void setVehicleAge(String vehicleAge) {
		this.vehicleAge = vehicleAge;
	}

	public String getVehicleCity() {
		return vehicleCity;
	}

	public void setVehicleCity(String vehicleCity) {
		this.vehicleCity = vehicleCity;
	}

	public String getVehicleZone() {
		return vehicleZone;
	}

	public void setVehicleZone(String vehicleZone) {
		this.vehicleZone = vehicleZone;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public Date getPolicyIssueDate() {
		return policyIssueDate;
	}

	public void setPolicyIssueDate(Date policyIssueDate) {
		this.policyIssueDate = policyIssueDate;
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}

	public String getaPPremium() {
		return aPPremium;
	}

	public void setaPPremium(String aPPremium) {
		this.aPPremium = aPPremium;
	}

	public String getpAPremium() {
		return pAPremium;
	}

	public void setpAPremium(String pAPremium) {
		this.pAPremium = pAPremium;
	}

	public String getLegalLiability() {
		return legalLiability;
	}

	public void setLegalLiability(String legalLiability) {
		this.legalLiability = legalLiability;
	}

	public String getNetLiability() {
		return netLiability;
	}

	public void setNetLiability(String netLiability) {
		this.netLiability = netLiability;
	}

	public String getAddOn() {
		return addOn;
	}

	public void setAddOn(String addOn) {
		this.addOn = addOn;
	}

	public String getOtherPremium() {
		return otherPremium;
	}

	public void setOtherPremium(String otherPremium) {
		this.otherPremium = otherPremium;
	}

	public String getGrossPremium() {
		return grossPremium;
	}

	public void setGrossPremium(String grossPremium) {
		this.grossPremium = grossPremium;
	}

	public String getDealer() {
		return dealer;
	}

	public void setDealer(String dealer) {
		this.dealer = dealer;
	}

}
