package models;

import javax.persistence.*;

@Entity
public class SMSTemplate {
	
	public static String OFFER ="OFFER";
	public static String OFFER1 ="OFFER1";
	public static String OFFER2 ="OFFER2";
	public static String NONCONTACT ="NONCONTACT";
	public static String EXCHANGE ="EXCHANGE";
	public static String VAS_OFFER ="VAS OFFER";
	public static String COMPLIANT ="COMPLAINT";
	public static String SERVICE_BOOKED ="Service Booked";
	public static String SERVICE_DUE ="Service Due";
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long smsId;
	
	@Column(length = 300)
	public String smsTemplate;
	@Column(length = 20)
	public String smsAPI;
	
	@Column(length = 50)
	public String smsType;
	
	@Column(length = 50)
	public String dealerName;

	@Column(length = 50)
	public String dealer;
	
	@Column(length = 50)
	public String locationId;
	
	
	
	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getDealer() {
		return dealer;
	}

	public void setDealer(String dealer) {
		this.dealer = dealer;
	}

	public long getSmsId() {
		return smsId;
	}

	public void setSmsId(long smsId) {
		this.smsId = smsId;
	}

	public String getSmsTemplate() {
		return smsTemplate;
	}

	public void setSmsTemplate(String smsTemplate) {
		this.smsTemplate = smsTemplate;
	}

	public String getSmsAPI() {
		return smsAPI;
	}

	public void setSmsAPI(String smsHeader) {
		this.smsAPI = smsHeader;
	}

	public String getSmsType() {
		return smsType;
	}

	public void setSmsType(String smsType) {
		this.smsType = smsType;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	
	
	

}