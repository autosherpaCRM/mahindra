/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author W-885
 */
@Entity
public class UpsellLead {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	@Column(length = 50)
	public String upSellType;
	@Column(length = 50)
	public String taggedTo;
	@Column(length = 300)
	public String upsellComments;
	
	public int upsellId;

	@ManyToOne(fetch = FetchType.EAGER)
	public SRDisposition srDisposition;

	@ManyToOne(fetch = FetchType.EAGER)
	public InsuranceDisposition insuranceDisposition;

	@ManyToOne(fetch = FetchType.EAGER)
	public PSFInteraction psfInteraction;

	@OneToOne(fetch = FetchType.EAGER)
	public Vehicle vehicle;

	@ManyToOne(fetch = FetchType.EAGER)
	public TaggingUsers taggingUsers;

	public TaggingUsers getTaggingUsers() {
		return taggingUsers;
	}

	public void setTaggingUsers(TaggingUsers taggingUsers) {
		this.taggingUsers = taggingUsers;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUpSellType() {
		return upSellType;
	}

	public void setUpSellType(String upSellType) {
		this.upSellType = upSellType;
	}

	public String getTaggedTo() {
		return taggedTo;
	}

	public void setTaggedTo(String taggedTo) {
		this.taggedTo = taggedTo;
	}

	public String getUpsellComments() {
		return upsellComments;
	}

	public void setUpsellComments(String upsellComments) {
		this.upsellComments = upsellComments;
	}

	public SRDisposition getSrDisposition() {
		return srDisposition;
	}

	public void setSrDisposition(SRDisposition srDisposition) {
		this.srDisposition = srDisposition;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public InsuranceDisposition getInsuranceDisposition() {
		return insuranceDisposition;
	}

	public void setInsuranceDisposition(InsuranceDisposition insuranceDisposition) {
		this.insuranceDisposition = insuranceDisposition;
	}

	public PSFInteraction getPsfInteraction() {
		return psfInteraction;
	}

	public void setPsfInteraction(PSFInteraction psfInteraction) {
		this.psfInteraction = psfInteraction;
	}

	public int getUpsellId() {
		return upsellId;
	}

	public void setUpsellId(int upsellId) {
		this.upsellId = upsellId;
	}
	
	
}
