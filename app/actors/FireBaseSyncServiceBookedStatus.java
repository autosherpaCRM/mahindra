/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import play.Logger;
import repositories.FirebaseSyncRepository;
import akka.actor.UntypedActor;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author W-885
 */
public class FireBaseSyncServiceBookedStatus extends UntypedActor{
    
    Logger.ALogger logger = play.Logger.of("application");
    
    @Override
    public void onReceive(Object protocol) throws Exception {
    
        ServiceAdvisorCallSyncProtocol.StartSync fireproto = (ServiceAdvisorCallSyncProtocol.StartSync) protocol;
         
         FirebaseSyncRepository repo=fireproto.firebaseRepo;
         String firebaseUrl = fireproto.urlToSync;
         
         
         RestTemplate restTemplate = new RestTemplate();

		String dealerkeysUrl = firebaseUrl
				+ ".json?auth=wMKa80oIu3tw4dFm8PbAovPcsTp9W9hEBByXGrVd&shallow=true";

		ResponseEntity<Map> result = restTemplate.getForEntity(dealerkeysUrl,
				Map.class);

		Map dealerMap = result.getBody();

		if (dealerMap != null) {
                    Set dealerkeys = dealerMap.keySet();
                    
                    
			for (Object key : dealerkeys) {
				String dealer = (String) key;
                                
                                
                             String userkeysUrl = firebaseUrl + dealer;
				userkeysUrl += "/ServiceAgent";
				String requsetuserurl = userkeysUrl
						+ ".json?auth=wMKa80oIu3tw4dFm8PbAovPcsTp9W9hEBByXGrVd&shallow=true";
				logger.info("Requesting url:" + requsetuserurl);

				RestTemplate restTemplate2 = new RestTemplate();
				ResponseEntity<Map> usersResult = restTemplate.getForEntity(
						requsetuserurl, Map.class);

				Map usersmap = usersResult.getBody();

				if (usersmap != null) {
					Set userskeys = usersmap.keySet();

					for (Object ukey : userskeys) {
						String user = (String) ukey;
						String callinfoUrl = userkeysUrl + "/";
						callinfoUrl += user;
                                                callinfoUrl += "/";
                                                callinfoUrl += "SummaryDetails";
                                                callinfoUrl += "/";
						callinfoUrl += "CallInfo.json?orderBy=%22status%22&equalTo=%22Vehicle%20Received%22";

						logger.info("CallInfo Url:" + callinfoUrl);

						RestTemplate restTemplate3 = new RestTemplate();

						String json = restTemplate3.getForObject(callinfoUrl,String.class);
                                                
						Map<String, Object> map = new HashMap<String, Object>();
						ObjectMapper mapper = new ObjectMapper();
                                                logger.info("json value for sync of SB :"+json);
                                                try {
							// convert JSON string to Map
							map = mapper
									.readValue(
											json,
											new TypeReference<HashMap<String, Object>>() {
											});
							
							Set keys = map.keySet();
							
							for(Object fkey : keys){
								String fkeyString = (String)fkey;
								logger.info("fkeyString :"+fkeyString);
								repo.updateStatusOfServiceBookedOfSA(fkeyString,"Vehicle Received");
							}

							logger.info("Successfully obtained Map:" + map);

							
							
						} catch (Exception e) {
							logger.info("Exception converting {} to map", json,
									e);
						}
                                                
                                                
                                        }
                                }
                                
                        }
                }
    }
}
              
