package actors;

import java.util.Map;
import java.util.Set;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import play.Logger.ALogger;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.WyzUserRepository;
import akka.actor.UntypedActor;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FireBaseCallLogSync extends UntypedActor {

	ALogger logger = play.Logger.of("application");

	@Override
	public void onReceive(Object protocol) throws Exception {
		// TODO Auto-generated method stub
		FireBaseCallLogSyncProtocol.StartSync fireproto = (FireBaseCallLogSyncProtocol.StartSync) protocol;

		CallInfoRepository repo = fireproto.callLogRepository;
		WyzUserRepository userRepo = fireproto.userRepository;
		CallInteractionsRepository callIntRepo = fireproto.callInteractionRepository;

		String firebaseUrl = fireproto.urlToSync;

		RestTemplate restTemplate = new RestTemplate();

		String dealerkeysUrl = firebaseUrl
				+ ".json?auth=vki9nCdP8QQpaWX0NG7iDrxznBlrmzo56enmA34L&shallow=true";

		ResponseEntity<Map> result = restTemplate.getForEntity(dealerkeysUrl,
				Map.class);

		Map dealerMap = result.getBody();

		if (dealerMap != null) {

			Set dealerkeys = dealerMap.keySet();

			for (Object key : dealerkeys) {
				String dealer = (String) key;
		
				//String dealer = "MAHINDRATEST";
				
				String userkeysUrl = firebaseUrl + dealer;
				userkeysUrl += "/CRE";
				String requsetuserurl = userkeysUrl
						+ ".json?auth=vki9nCdP8QQpaWX0NG7iDrxznBlrmzo56enmA34L&shallow=true";
				logger.info("Requesting url:" + requsetuserurl);

				RestTemplate restTemplate2 = new RestTemplate();
				ResponseEntity<Map> usersResult = restTemplate.getForEntity(
						requsetuserurl, Map.class);

				Map usersmap = usersResult.getBody();

				if (usersmap != null) {
					Set userskeys = usersmap.keySet();

					for (Object ukey : userskeys) {
						String user = (String) ukey;

						String callinfoUrl = userkeysUrl + "/";
						callinfoUrl += user;
						callinfoUrl += "/";
						callinfoUrl += "WebHistory";
						callinfoUrl += "/";
						callinfoUrl += "CallInfo";
						callinfoUrl += "/";
						logger.info("CallInfo Url WebHistory :" + callinfoUrl);

						
						DatabaseReference callinfoRef = FirebaseDatabase
								.getInstance().getReferenceFromUrl(callinfoUrl);

						
						CallLogValueEventListner callinfoLogListener = new CallLogValueEventListner(
								callIntRepo);
						callinfoRef
								.addChildEventListener(callinfoLogListener);

						
					}
				}
			}
		}
	}
}
