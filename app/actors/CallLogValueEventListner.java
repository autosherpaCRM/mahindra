package actors;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import play.Logger.ALogger;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import controllers.webmodels.CustomerDataOnTabLoad;
import models.CallInteraction;
import models.Customer;
import repositories.CallInteractionsRepository;

public class CallLogValueEventListner implements ChildEventListener {

	ALogger logger = play.Logger.of("application");

	CallInteractionsRepository callInterRepository;

	public CallLogValueEventListner(CallInteractionsRepository repo) {
		this.callInterRepository = repo;
	}

	@Override
	public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {

		String key = dataSnapshot.getKey();

		logger.info("dataSnapshot key for webhistory :" + key);

		CallInteraction callinfo = dataSnapshot.getValue(CallInteraction.class);

		CustomerDataOnTabLoad customerinfo = dataSnapshot.getValue(CustomerDataOnTabLoad.class);

		String date = callinfo.getCallDate();
		String time = callinfo.getCallTime();

		String dateTimeStr = date + " " + time;
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		// logger.info("Call time time: " + dateTimeStr);
		//logger.info("call Date :" + date + "Call Time :" + time);

		try {
			Date datetime = format.parse(dateTimeStr);
			callinfo.setCallMadeDateAndTime(datetime);
		} catch (ParseException e) {
			logger.info("Could not parse date");
			e.printStackTrace();
		}

		String base64String = callinfo.getMediaFile();
		callinfo.setMediaFile("");

		byte[] lobvalue = null;

		if (base64String != null) {
			if (base64String.length() > 0) {
				lobvalue = DatatypeConverter.parseBase64Binary(base64String);
			}
		}
		callinfo.setMediaFileLob(lobvalue);

		callInterRepository.updateTheMediaData(callinfo, key, customerinfo, dataSnapshot);

		lobvalue = null;

	}

	@Override
	public void onCancelled(DatabaseError arg0) {
		// TODO Auto-generated method stub
		logger.info("onCancelled Message");

	}

	@Override
	public void onChildChanged(DataSnapshot arg0, String arg1) {
		// TODO Auto-generated method stub
		logger.info("onChildChanged Message");

	}

	@Override
	public void onChildMoved(DataSnapshot arg0, String arg1) {
		// TODO Auto-generated method stub
		logger.info("onChildMoved Message");

	}

	@Override
	public void onChildRemoved(DataSnapshot arg0) {
		// TODO Auto-generated method stub

		logger.info("onChildRemoved Message");

	}
}
