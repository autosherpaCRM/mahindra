package actors;

import repositories.FirebaseSyncRepository;

public class ServiceAdvisorHistorySyncProtocol {
	
	public static class StartSync {

		public final String urlToSync;

		public final FirebaseSyncRepository firebaseSyncRepo;

		public StartSync(String url, FirebaseSyncRepository repository) {
			this.urlToSync = url;
			this.firebaseSyncRepo = repository;
		}
	}


}
