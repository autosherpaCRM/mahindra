package actors;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import actors.model.ServiceAdvisorHistoryInfo;
import play.Logger;
import repositories.FirebaseSyncRepository;

public class InsuranceAgentHistoryListener implements ChildEventListener{
	
Logger.ALogger logger = play.Logger.of("application");
	
	private FirebaseSyncRepository firebaseSyncRepository;
	
	public InsuranceAgentHistoryListener(FirebaseSyncRepository repository){
		firebaseSyncRepository = repository;
	}

	

	@Override
	public void onChildAdded(DataSnapshot dataSnapshot,String prevChildKey) {
		// TODO Auto-generated method stub
		String firebaseKey = dataSnapshot.getKey();
		logger.info("Received onChildAdded Message for key: "+firebaseKey);
		
		ServiceAdvisorHistoryInfo insuranceHistory=dataSnapshot.getValue(ServiceAdvisorHistoryInfo.class);
		
		
		//logger.info("Received Object: "+serviceAdvi_History.customerName);
		
		logger.info("Received Object: "+insuranceHistory.callDuration);
		logger.info("Received Object: "+insuranceHistory.callType);
		firebaseSyncRepository.addInsuranceAgentCallHistory(firebaseKey, insuranceHistory,dataSnapshot);
	
		
	}

	@Override
	public void onCancelled(DatabaseError arg0) {
		// TODO Auto-generated method stub
		logger.info("onCancelled Message");
		
	}

	
	@Override
	public void onChildChanged(DataSnapshot arg0, String arg1) {
		// TODO Auto-generated method stub
		logger.info("onChildChanged Message");
		
	}

	@Override
	public void onChildMoved(DataSnapshot arg0, String arg1) {
		// TODO Auto-generated method stub
		logger.info("onChildMoved Message");
		
	}

	@Override
	public void onChildRemoved(DataSnapshot arg0) {
		// TODO Auto-generated method stub
		
		logger.info("onChildRemoved Message");
		
	}

}
