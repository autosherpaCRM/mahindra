package actors;

import repositories.ExcelUploadRepo;

public class InsuranceDataInsertProtocol {

	public static class StartSync {

		public final String uploadId;

		public final ExcelUploadRepo excelUploadRepo;

		public StartSync(String upload_id, ExcelUploadRepo repository) {
			this.uploadId = upload_id;
			this.excelUploadRepo = repository;
		}
	}
	
}
