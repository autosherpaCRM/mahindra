/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors;

import akka.actor.UntypedActor;
import com.google.firebase.database.DatabaseReference;
import java.util.List;
import java.util.Map;
import java.util.Set;
import models.CallInteraction;
import models.Service;
import models.ServiceBooked;
import models.WyzUser;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import play.Logger;
import repositories.FirebaseSyncRepository;

/**
 *
 * @author W-885
 */
public class FireBaseSyncServiceBookedStatusCount extends UntypedActor{
    
    Logger.ALogger logger = play.Logger.of("application");
    
    @Override
    public void onReceive(Object protocol) throws Exception {
       
          
           ServiceAdvisorCallSyncProtocol.StartSync fireproto = (ServiceAdvisorCallSyncProtocol.StartSync) protocol;
           
         FirebaseSyncRepository repo=fireproto.firebaseRepo;
         String firebaseUrl = fireproto.urlToSync;
         
         
         RestTemplate restTemplate = new RestTemplate();

		String dealerkeysUrl = firebaseUrl
				+ ".json?auth=wMKa80oIu3tw4dFm8PbAovPcsTp9W9hEBByXGrVd&shallow=true";

		ResponseEntity<Map> result = restTemplate.getForEntity(dealerkeysUrl,
				Map.class);

		Map dealerMap = result.getBody();

		if (dealerMap != null) {
                    Set dealerkeys = dealerMap.keySet();
                    
                    for (Object key : dealerkeys) {
				String dealer = (String) key;
				logger.info("dealer for report details is : "+dealer);
				DatabaseReference ref;
				
				List<WyzUser> getListServiceAdvisor=repo.getListOfServiceAdvisorData();
				for(WyzUser userData:getListServiceAdvisor){				
					
                                    
                                    
					String user=userData.getUserName();
                                        logger.info("the cre for reports is :"+user);
					
					String fireUrl = fireproto.urlToSync;
					fireUrl += dealer;	
					fireUrl+="/";
					fireUrl+="ServiceAgent/";
					fireUrl+=user;
					fireUrl += "/";
                                        fireUrl += "SummaryDetails";
                                        fireUrl += "/";
                                        fireUrl += "CallInfo";
                                        fireUrl += "/";
                                       List<ServiceBooked> list_serv_booked=repo.getListOfServicebookedData(user);
                                       for(ServiceBooked serv_booked_data:list_serv_booked){
                                           
                                           Service serv_data=repo.getServiceDataByVehicleId(serv_booked_data.getVehicle().getVehicle_id());
                                           if(serv_data.getNextServiceType()!=null){
                                               
                                               
                                               
                                               
                                               
                                           }
                                           
                                     
                                       
                                       
                                       }
                                       
                               
                                }
                  
                    }
           
    }
    
}
}
