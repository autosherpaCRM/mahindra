package actors;

import java.util.List;

import models.Driver;
import models.PickupDrop;
import models.ServiceBooked;
import play.Logger;
import repositories.FirebaseSyncRepository;
import actors.model.DriverPickupInfo;
import akka.actor.UntypedActor;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DriverPickupDropListSync extends UntypedActor {

	Logger.ALogger logger = play.Logger.of("application");

	@Override
	public void onReceive(Object protocol) throws Exception {

		DriverPickupDropListSyncProtocol.StartSync fireproto = (DriverPickupDropListSyncProtocol.StartSync) protocol;
		FirebaseSyncRepository repo = fireproto.firebaseSyncRepo;

		List<Driver> drivers = repo.getAllDrivers();

		for (Driver driver : drivers) {
			String firebaseUrl = fireproto.urlToSync;
			String userName = driver.getWyzUser().getUserName();
			firebaseUrl += driver.getWyzUser().getDealerCode();
			firebaseUrl += "/Driver/";
			firebaseUrl += userName;
			firebaseUrl += "/DriverPickupList/CallInfo";
			logger.info("fireUrl is :" + firebaseUrl);

			List<ServiceBooked> servicesBooked = repo.getDriverPickupDropList(userName);

			for (ServiceBooked serviceBooked : servicesBooked) {

				DatabaseReference ref = FirebaseDatabase.getInstance()
						.getReferenceFromUrl(firebaseUrl);

				ref.removeValue();

				DriverPickupInfo info = new DriverPickupInfo();
				
				PickupDrop pickupDrop = serviceBooked.getPickupDrop();
				
				
							
				info.setCustomerAddress(pickupDrop.getPickUpAddress());
				
				info.setCustomerCRN(pickupDrop.getCRN());
				info.setCustomerName(serviceBooked.getCustomer().getCustomerName());
				info.setCustomerPhone(serviceBooked.getCustomer().getPreferredPhone().getPhoneNumber());
				info.setDeliveredAtws("");
				info.setFuelType(serviceBooked.getVehicle().getFuelType());
				info.setIsCallMade("");
				info.setModel(serviceBooked.getVehicle().getModel());
				info.setPickupOrDrop(Boolean.toString(serviceBooked.isPickupRequired()));
				info.setPickupPointReached("");
				info.setPickupStarted("");
				info.setServiceBookedId(Long.toString(serviceBooked.getServiceBookedId()));
				info.setServiceScheduledDate(pickupDrop.getPickupDateStr());
				info.setServiceScheduledTime(pickupDrop.getPickupTime());
				info.setTrackStatus("");
				info.setUserId(serviceBooked.getDriver().getWyzUser().getUserName());
				info.setVehicleRegNo(serviceBooked.getVehicle().getVehicleRegNo());
				info.setVehicleColor(serviceBooked.getVehicle().getColor());
				info.setVehicleId(Long.toString(serviceBooked.getVehicle().getVehicle_id()));
				info.setVehicleNumber(serviceBooked.getVehicle().getVehicleNumber());
				info.setVehiclePicked("");

				ref.push().setValue(info);
			}

		}

	}
	

}
