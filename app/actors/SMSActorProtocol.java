package actors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import repositories.SMSTriggerRespository;

public class SMSActorProtocol {
	
	public static class startSMSActor{
		
		public final List<Long> vehicleId;
		public final String smsTemplete;
		public final String smsType;
		public final long userId;
		public final String tenentCode;
		public final SMSTriggerRespository repository;
		
		
		public startSMSActor(List<Long> vehicleId, String smsTemplete,String smsType, long userId,
				String tenentCode,SMSTriggerRespository smsRepo) {
			
			this.vehicleId = vehicleId;
			this.smsTemplete = smsTemplete;	
			this.smsType=smsType;
			this.userId = userId;
			this.tenentCode=tenentCode;
			this.repository=smsRepo;
		}
		
		
		
		
	}

}
