package actors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.ListingForm;
import models.WyzUser;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;

import com.google.firebase.database.DatabaseError;

import play.Logger.ALogger;
import repositories.CallInfoRepository;
import repositories.ScheduledCallRepository;
import repositories.SearchRepository;
import repositories.WyzUserRepository;
import akka.actor.UntypedActor;

public class FireBaseReportDetailsSync extends UntypedActor{
	ALogger logger = play.Logger.of("application");

	@Override
	public void onReceive(Object protocol) throws Exception {
		
		
		FireBaseReportDetailsSyncProtocol.StartSync fireproto=(FireBaseReportDetailsSyncProtocol.StartSync) protocol;
		
		SearchRepository repoSch=fireproto.schRepo;
		WyzUserRepository repoWyz=fireproto.wyzRepository;
		
		RestTemplate restTemplate = new RestTemplate();
		
		String firebaseUrl = fireproto.urlToSync;

		String dealerkeysUrl = firebaseUrl
				+ ".json?auth=vki9nCdP8QQpaWX0NG7iDrxznBlrmzo56enmA34L&shallow=true";

		ResponseEntity<Map> result = restTemplate.getForEntity(dealerkeysUrl,
				Map.class);

		Map dealerMap = result.getBody();

		if (dealerMap != null) {

			Set dealerkeys = dealerMap.keySet();

			for (Object key : dealerkeys) {
				String dealer = (String) key;
		
						
                    List<WyzUser> listOfUser=repoWyz.getAllCREOFDealer(dealer);
                    long sizeOFcres=listOfUser.size();
                    logger.info("size of the cre :"+sizeOFcres);
                    for(WyzUser creUser: listOfUser){
                            if(sizeOFcres!=0){

                            String user=creUser.getUserName();
                            logger.info("the cre for reports is :"+user);

                            String fireUrl1 = fireproto.urlToSync;
                            fireUrl1 += dealer;	
                            fireUrl1+="/";
                            fireUrl1+="CRE/";
                            fireUrl1+=user;
                            fireUrl1+="/";
                            fireUrl1+="CREDashBoard";

                            DatabaseReference ref1 = FirebaseDatabase.getInstance().getReferenceFromUrl(fireUrl1);
                            logger.info("Old report got deleted "+fireUrl1);
                            ref1.removeValue();
                           
                            double percentageOfConversion=repoSch.getConversionRate(creUser.getId(), "conversion_rate_Crewise");
                			long countOfSchPendingCalls=repoSch.getPendingandtotalcount(creUser.getId(), "pending_calls_crewise");                			
                			long countOfServiceBooked=repoSch.getAllDispositionRequiredData(creUser.getId(), 3);
                			long countOfSchCallOfCRE =repoSch.getPendingandtotalcount(creUser.getId(), "scheduledcalls_count_crewise");
                            
                            logger.info("ServiceBooked calls count"+countOfServiceBooked);
                            logger.info("countOfSchCallOfCRE: "+countOfSchCallOfCRE+"countOfSchPendingCalls :"+countOfSchPendingCalls+"countOfServiceBooked :"+countOfServiceBooked+"percentageOfConversion :"+percentageOfConversion);
                            Map<String, Object> addReportCounts = new HashMap<String, Object>();

                            addReportCounts.put("totalAssignedCalls", String.valueOf(countOfSchCallOfCRE));
                            addReportCounts.put("pendingCalls", String.valueOf(countOfSchPendingCalls));
                            addReportCounts.put("serviceBooked", String.valueOf(countOfServiceBooked));
                            addReportCounts.put("conversionRate", String.valueOf(percentageOfConversion));	


                            DatabaseReference newref= ref1.push();
                            logger.info("newref is :"+newref);
                            String referencekey = newref.getKey();
                            logger.info("referencekey is reports "+referencekey);
                             logger.info("addReportCounts "+addReportCounts.toString());
                            newref.setValue(addReportCounts);

			}
                    }
			}
		}
	}
}
