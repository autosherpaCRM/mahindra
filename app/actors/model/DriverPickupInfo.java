package actors.model;

public class DriverPickupInfo {
	
	public String customerAddress;
	public String customerCRN;
	
	public String customerName;
	public String customerPhone;
	public String deliveredAtws;
	public String fuelType;
	public String isCallMade;
	public String model;
	public String pickupOrDrop;
	public String pickupPointReached;
	public String pickupStarted;
	public String serviceScheduledDate;
	public String serviceScheduledTime;
	public String trackStatus;
	public String userId;
	
	public String vehicleRegNo;
	public String vehicleColor;
	public String vehicleId;
	public String vehicleNumber;
	public String vehiclePicked;
	public String serviceBookedId;
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCustomerCRN() {
		return customerCRN;
	}
	public void setCustomerCRN(String customerCRN) {
		this.customerCRN = customerCRN;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getDeliveredAtws() {
		return deliveredAtws;
	}
	public void setDeliveredAtws(String deliveredAtws) {
		this.deliveredAtws = deliveredAtws;
	}
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	public String getIsCallMade() {
		return isCallMade;
	}
	public void setIsCallMade(String isCallMade) {
		this.isCallMade = isCallMade;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getPickupOrDrop() {
		return pickupOrDrop;
	}
	public void setPickupOrDrop(String pickupOrDrop) {
		this.pickupOrDrop = pickupOrDrop;
	}
	public String getPickupPointReached() {
		return pickupPointReached;
	}
	public void setPickupPointReached(String pickupPointReached) {
		this.pickupPointReached = pickupPointReached;
	}
	public String getPickupStarted() {
		return pickupStarted;
	}
	public void setPickupStarted(String pickupStarted) {
		this.pickupStarted = pickupStarted;
	}
	public String getServiceScheduledDate() {
		return serviceScheduledDate;
	}
	public void setServiceScheduledDate(String serviceScheduledDate) {
		this.serviceScheduledDate = serviceScheduledDate;
	}
	public String getServiceScheduledTime() {
		return serviceScheduledTime;
	}
	public void setServiceScheduledTime(String serviceScheduledTime) {
		this.serviceScheduledTime = serviceScheduledTime;
	}
	public String getTrackStatus() {
		return trackStatus;
	}
	public void setTrackStatus(String trackStatus) {
		this.trackStatus = trackStatus;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getVehicleRegNo() {
		return vehicleRegNo;
	}
	public void setVehicleRegNo(String vehicalRegNo) {
		this.vehicleRegNo = vehicalRegNo;
	}
	public String getVehicleColor() {
		return vehicleColor;
	}
	public void setVehicleColor(String vehicleColor) {
		this.vehicleColor = vehicleColor;
	}
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getVehiclePicked() {
		return vehiclePicked;
	}
	public void setVehiclePicked(String vehiclePicked) {
		this.vehiclePicked = vehiclePicked;
	}
	public String getServiceBookedId() {
		return serviceBookedId;
	}
	public void setServiceBookedId(String serviceBookedId) {
		this.serviceBookedId = serviceBookedId;
	}

}
