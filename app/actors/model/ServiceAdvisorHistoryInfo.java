package actors.model;

import javax.persistence.Column;

public class ServiceAdvisorHistoryInfo {

	public String vehicleId;
	public String custId;
	public String userId;
	public String assignedId;
	public String serviceBookedId;
	public String customerCategory;
	public String loyaltyType;
	public String model;
	public String variant;
	public String fuelType;
	public String vehicalRegNo;
	public String vehicleNumber;
	public String saleDate;
	public String dob;
	public String anniversary_date;
	public String lastServiceDate;
	public String lastVisitMileage;
	public String lastServiceType;
	public String lastVisitDate;
	public String lastServiceMileage;
	public String lastVisitType;
	public String averageRunning;
	public String serviceNoShowPeriod;
	public String serviceDueBasedonMileage;
	public String serviceDueBasedonTenure;
	public String nextServiceDate;
	public String forecastLogic;
	public String comments;
	public String status;
	public String customerName;
	public String customerPhone;
	public String customerAddress;
	public String callCount;
	public String chassisNo;
	public String serviceScheduledDate;
	public String serviceType;
	public String jobCardNumber;
	public String uploadedDate;
	public String reasonForHTML;
	public String billDate;

	public String callDate;
	public Integer callTypePicId;
	public String callTime;
	public String callDuration;
	public String ringTime;
	public String callType;
	public String latitude;
	public String longitude;
	public String mediaFile;
	public String filePath;
	public String dealerCode;
	public String makeCallFrom;
	public String isFollowupRequired;
	public String followUpDate;
	public String followUpTime;

	public String noServiceReason;
	public Integer daysBetweenVisit;
	public Integer ageOfVehicle;
	public String agentName;
	public Integer uniqueidForCallSync;
	public String reason;
	public String isFollowUpDone;

	private String mileage;
	private String dealerName;
	private String mileageAtService;
	private String dateOfService;
	private String city;
	private String workshop;

	private String afterServiceSatisfication;
	private String recentServiceSatisfication;
	private String afterServiceComments;
	private String recentServiceComments;

	private String creName;
	private String interactionDate;
	private String interactionTime;
	private String pickUpRequired;
	private String alreadyservicedatId;

	// New Insurance Agent Field
	private String appointmentTime;
	private String insuranceCompany;
	private String lastPolicyNo;
	private String lastPremium;
	private String lastIDV;
	private String renewalType;
	private String renewalMode;
	private String premium;
	private String addOns;
	private String idvPercentage;

	private String paymentType;
	private String paymentReference;

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentReference() {
		return paymentReference;
	}

	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}

	public String getAppointmentTime() {
		return appointmentTime;
	}

	public void setAppointmentTime(String appointmentTime) {
		this.appointmentTime = appointmentTime;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public String getLastPremium() {
		return lastPremium;
	}

	public void setLastPremium(String lastPremium) {
		this.lastPremium = lastPremium;
	}

	public String getRenewalMode() {
		return renewalMode;
	}

	public void setRenewalMode(String renewalMode) {
		this.renewalMode = renewalMode;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getAddOns() {
		return addOns;
	}

	public void setAddOns(String addOns) {
		this.addOns = addOns;
	}

	public String getIdvPercentage() {
		return idvPercentage;
	}

	public void setIdvPercentage(String idvPercentage) {
		this.idvPercentage = idvPercentage;
	}

	public String getCreName() {
		return creName;
	}

	public void setCreName(String creName) {
		this.creName = creName;
	}

	public String getInteractionDate() {
		return interactionDate;
	}

	public void setInteractionDate(String interactionDate) {
		this.interactionDate = interactionDate;
	}

	public String getInteractionTime() {
		return interactionTime;
	}

	public void setInteractionTime(String interactionTime) {
		this.interactionTime = interactionTime;
	}

	public String getPickUpRequired() {
		return pickUpRequired;
	}

	public void setPickUpRequired(String pickUpRequired) {
		this.pickUpRequired = pickUpRequired;
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getServiceBookedId() {
		return serviceBookedId;
	}

	public void setServiceBookedId(String serviceBookedId) {
		this.serviceBookedId = serviceBookedId;
	}

	public String getCustomerCategory() {
		return customerCategory;
	}

	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}

	public String getLoyaltyType() {
		return loyaltyType;
	}

	public void setLoyaltyType(String loyaltyType) {
		this.loyaltyType = loyaltyType;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getVehicalRegNo() {
		return vehicalRegNo;
	}

	public void setVehicalRegNo(String vehicalRegNo) {
		this.vehicalRegNo = vehicalRegNo;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(String saleDate) {
		this.saleDate = saleDate;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAnniversary_date() {
		return anniversary_date;
	}

	public void setAnniversary_date(String anniversary_date) {
		this.anniversary_date = anniversary_date;
	}

	public String getLastServiceDate() {
		return lastServiceDate;
	}

	public void setLastServiceDate(String lastServiceDate) {
		this.lastServiceDate = lastServiceDate;
	}

	public String getLastVisitMileage() {
		return lastVisitMileage;
	}

	public void setLastVisitMileage(String lastVisitMileage) {
		this.lastVisitMileage = lastVisitMileage;
	}

	public String getLastServiceType() {
		return lastServiceType;
	}

	public void setLastServiceType(String lastServiceType) {
		this.lastServiceType = lastServiceType;
	}

	public String getLastVisitDate() {
		return lastVisitDate;
	}

	public void setLastVisitDate(String lastVisitDate) {
		this.lastVisitDate = lastVisitDate;
	}

	public String getLastServiceMileage() {
		return lastServiceMileage;
	}

	public void setLastServiceMileage(String lastServiceMileage) {
		this.lastServiceMileage = lastServiceMileage;
	}

	public String getLastVisitType() {
		return lastVisitType;
	}

	public void setLastVisitType(String lastVisitType) {
		this.lastVisitType = lastVisitType;
	}

	public String getAverageRunning() {
		return averageRunning;
	}

	public void setAverageRunning(String averageRunning) {
		this.averageRunning = averageRunning;
	}

	public String getServiceNoShowPeriod() {
		return serviceNoShowPeriod;
	}

	public void setServiceNoShowPeriod(String serviceNoShowPeriod) {
		this.serviceNoShowPeriod = serviceNoShowPeriod;
	}

	public String getServiceDueBasedonMileage() {
		return serviceDueBasedonMileage;
	}

	public void setServiceDueBasedonMileage(String serviceDueBasedonMileage) {
		this.serviceDueBasedonMileage = serviceDueBasedonMileage;
	}

	public String getServiceDueBasedonTenure() {
		return serviceDueBasedonTenure;
	}

	public void setServiceDueBasedonTenure(String serviceDueBasedonTenure) {
		this.serviceDueBasedonTenure = serviceDueBasedonTenure;
	}

	public String getNextServiceDate() {
		return nextServiceDate;
	}

	public void setNextServiceDate(String nextServiceDate) {
		this.nextServiceDate = nextServiceDate;
	}

	public String getForecastLogic() {
		return forecastLogic;
	}

	public void setForecastLogic(String forecastLogic) {
		this.forecastLogic = forecastLogic;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCallCount() {
		return callCount;
	}

	public void setCallCount(String callCount) {
		this.callCount = callCount;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public String getServiceScheduledDate() {
		return serviceScheduledDate;
	}

	public void setServiceScheduledDate(String serviceScheduledDate) {
		this.serviceScheduledDate = serviceScheduledDate;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getJobCardNumber() {
		return jobCardNumber;
	}

	public void setJobCardNumber(String jobCardNumber) {
		this.jobCardNumber = jobCardNumber;
	}

	public String getUploadedDate() {
		return uploadedDate;
	}

	public void setUploadedDate(String uploadedDate) {
		this.uploadedDate = uploadedDate;
	}

	public String getCallDate() {
		return callDate;
	}

	public void setCallDate(String callDate) {
		this.callDate = callDate;
	}

	public String getCallTime() {
		return callTime;
	}

	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}

	public String getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getMediaFile() {
		return mediaFile;
	}

	public void setMediaFile(String mediaFile) {
		this.mediaFile = mediaFile;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public String getMakeCallFrom() {
		return makeCallFrom;
	}

	public void setMakeCallFrom(String makeCallFrom) {
		this.makeCallFrom = makeCallFrom;
	}

	public String getIsFollowupRequired() {
		return isFollowupRequired;
	}

	public void setIsFollowupRequired(String isFollowupRequired) {
		this.isFollowupRequired = isFollowupRequired;
	}

	public String getFollowUpDate() {
		return followUpDate;
	}

	public void setFollowUpDate(String followUpDate) {
		this.followUpDate = followUpDate;
	}

	public String getFollowUpTime() {
		return followUpTime;
	}

	public void setFollowUpTime(String followUpTime) {
		this.followUpTime = followUpTime;
	}

	public String getNoServiceReason() {
		return noServiceReason;
	}

	public void setNoServiceReason(String noServiceReason) {
		this.noServiceReason = noServiceReason;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Integer getUniqueidForCallSync() {
		return uniqueidForCallSync;
	}

	public void setUniqueidForCallSync(Integer uniqueidForCallSync) {
		this.uniqueidForCallSync = uniqueidForCallSync;
	}

	public Integer getCallTypePicId() {
		return callTypePicId;
	}

	public void setCallTypePicId(Integer callTypePicId) {
		this.callTypePicId = callTypePicId;
	}

	public Integer getDaysBetweenVisit() {
		return daysBetweenVisit;
	}

	public void setDaysBetweenVisit(Integer daysBetweenVisit) {
		this.daysBetweenVisit = daysBetweenVisit;
	}

	public Integer getAgeOfVehicle() {
		return ageOfVehicle;
	}

	public void setAgeOfVehicle(Integer ageOfVehicle) {
		this.ageOfVehicle = ageOfVehicle;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getIsFollowUpDone() {
		return isFollowUpDone;
	}

	public void setIsFollowUpDone(String isFollowUpDone) {
		this.isFollowUpDone = isFollowUpDone;
	}

	public String getReasonForHTML() {
		return reasonForHTML;
	}

	public void setReasonForHTML(String reasonForHTML) {
		this.reasonForHTML = reasonForHTML;
	}

	public String getMileage() {
		return mileage;
	}

	public void setMileage(String mileage) {
		this.mileage = mileage;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getMileageAtService() {
		return mileageAtService;
	}

	public void setMileageAtService(String mileageAtService) {
		this.mileageAtService = mileageAtService;
	}

	public String getDateOfService() {
		return dateOfService;
	}

	public void setDateOfService(String dateOfService) {
		this.dateOfService = dateOfService;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getWorkshop() {
		return workshop;
	}

	public void setWorkshop(String workshop) {
		this.workshop = workshop;
	}

	public String getAssignedId() {
		return assignedId;
	}

	public void setAssignedId(String assignedId) {
		this.assignedId = assignedId;
	}

	public String getAfterServiceSatisfication() {
		return afterServiceSatisfication;
	}

	public void setAfterServiceSatisfication(String afterServiceSatisfication) {
		this.afterServiceSatisfication = afterServiceSatisfication;
	}

	public String getRecentServiceSatisfication() {
		return recentServiceSatisfication;
	}

	public void setRecentServiceSatisfication(String recentServiceSatisfication) {
		this.recentServiceSatisfication = recentServiceSatisfication;
	}

	public String getAfterServiceComments() {
		return afterServiceComments;
	}

	public void setAfterServiceComments(String afterServiceComments) {
		this.afterServiceComments = afterServiceComments;
	}

	public String getRecentServiceComments() {
		return recentServiceComments;
	}

	public void setRecentServiceComments(String recentServiceComments) {
		this.recentServiceComments = recentServiceComments;
	}

	public String getRingTime() {
		return ringTime;
	}

	public void setRingTime(String ringTime) {
		this.ringTime = ringTime;
	}

	public String getAlreadyservicedatId() {
		return alreadyservicedatId;
	}

	public void setAlreadyservicedatId(String alreadyservicedatId) {
		this.alreadyservicedatId = alreadyservicedatId;
	}

	public String getLastPolicyNo() {
		return lastPolicyNo;
	}

	public void setLastPolicyNo(String lastPolicyNo) {
		this.lastPolicyNo = lastPolicyNo;
	}

	public String getLastIDV() {
		return lastIDV;
	}

	public void setLastIDV(String lastIDV) {
		this.lastIDV = lastIDV;
	}

	public String getRenewalType() {
		return renewalType;
	}

	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}
	
	

}
