package actors;

import repositories.CallInfoRepository;
import repositories.ScheduledCallRepository;
import repositories.SearchRepository;
import repositories.WyzUserRepository;

public class FireBaseReportDetailsSyncProtocol {
public static class StartSync{
		
		public final String urlToSync;
		
		public final WyzUserRepository wyzRepository;
		public final SearchRepository schRepo;
		
		public StartSync(String urlToSync,WyzUserRepository wyzRepository,SearchRepository searchRepository) {
			
			this.urlToSync = urlToSync;			
			this.wyzRepository=wyzRepository;
			this.schRepo=searchRepository;
		}
		
}

}
