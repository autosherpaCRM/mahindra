/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.CallInteraction;
import models.Customer;
import models.Dealer;
import models.Segment;
import models.Service;
import models.ServiceBooked;

import org.hibernate.Hibernate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import play.Logger;
import repositories.FirebaseSyncRepository;
import akka.actor.UntypedActor;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import actors.model.ServiceAdvisorHistoryInfo;

/**
 *
 * @author W-885
 */
public class ServiceAdvisorCallSync extends UntypedActor {

	Logger.ALogger logger = play.Logger.of("application");

	@Override
	public void onReceive(Object protocol) throws Exception {

		ServiceAdvisorCallSyncProtocol.StartSync fireproto = (ServiceAdvisorCallSyncProtocol.StartSync) protocol;
		FirebaseSyncRepository repo = fireproto.firebaseRepo;

		String firebaseUrl = fireproto.urlToSync;

		RestTemplate restTemplate = new RestTemplate();
		String dealerkeysUrl = firebaseUrl + ".json?auth=vki9nCdP8QQpaWX0NG7iDrxznBlrmzo56enmA34L&shallow=true";

		ResponseEntity<Map> result = restTemplate.getForEntity(dealerkeysUrl, Map.class);

		Map dealerMap = result.getBody();

		 if (dealerMap != null) {

		 Set dealerkeys = dealerMap.keySet();

		 for (Object key : dealerkeys) {
		String dealer = (String) key;
		//String dealer = "MAHINDRATEST";
		String userkeysUrl = firebaseUrl + dealer;
		userkeysUrl += "/ServiceAgent";

		logger.info("userkeysUrl url:" + userkeysUrl);

		DatabaseReference ref;
		DatabaseReference ref1;

		if (repo != null) {

			List<CallInteraction> listOfService = repo.getListOfServicesNotSynced(dealer);

			logger.info("size of Service booked for syncing:" + listOfService.size());

			for (CallInteraction ser_data : listOfService) {

				String customerPhoneData = "+91";

				// logger.info("customer phone of Service booked for
				// syncing:"+
				// ser_data.getCustomer().getPreferredPhone().getPhoneNumber());

				Service service_data = repo.getServiceDataByVehicleId(ser_data.getVehicle().getVehicle_id());

				ServiceBooked service_bookedData = repo.getSBbyCallInterId(ser_data.getId());

				Segment segment = repo.getSegmentByCustomerId(ser_data.getCustomer().getId());

				String userName = service_bookedData.getServiceAdvisor().getAdvisorName();

				String fireUrl = userkeysUrl + "/";
				fireUrl += userName;
				fireUrl += "/";
				fireUrl += "BookedDetails";
				fireUrl += "/";
				fireUrl += "CallInfo";
				fireUrl += "/";

				logger.info("fireUrl is :" + fireUrl);
				ref = FirebaseDatabase.getInstance().getReferenceFromUrl(fireUrl);

				DatabaseReference newref = ref.push();

				String referencekey = newref.getKey();
				logger.info("referencekey : " + referencekey);

				ServiceAdvisorHistoryInfo service_advisor = new ServiceAdvisorHistoryInfo();

				if (ser_data.getVehicle().getVehicle_id() != 0) {
					service_advisor.setVehicleId(String.valueOf(ser_data.getVehicle().getVehicle_id()));
				}

				if (ser_data.getCustomer().getId() != 0) {
					service_advisor.setCustId(String.valueOf(ser_data.getCustomer().getId()));
				}

				if (service_bookedData.getServiceAdvisor().getWyzUser() != null) {
					service_advisor
							.setUserId(String.valueOf(service_bookedData.getServiceAdvisor().getWyzUser().getId()));
				}

				if (ser_data.getVehicle().getCustomer().getCustomerName() != null) {
					service_advisor.setCustomerName(ser_data.getVehicle().getCustomer().getCustomerName());
				}
				if (ser_data.getVehicle().getCustomer().getPreferredPhone() != null) {

					customerPhoneData = customerPhoneData
							+ ser_data.getVehicle().getCustomer().getPreferredPhone().getPhoneNumber();

					service_advisor
							.setCustomerPhone(ser_data.getVehicle().getCustomer().getPreferredPhone().getPhoneNumber());
				}

				if (segment.getName() != null) {
					service_advisor.setCustomerCategory(segment.getName());
				}

				/*
				 * if (segment.getType() != null) {
				 * service_advisor.setLoyaltyType(segment.getType()); }
				 */

				/*
				 * if (ser_data.getVehicle().getCustomer().getDob() != null) {
				 * service_advisor.setDob(ser_data.getVehicle().getCustomer().
				 * getDob()); }
				 */

				/*
				 * if (ser_data.getVehicle().getCustomer().getAnniversary_date()
				 * != null) { service_advisor
				 * .setAnniversary_date(ser_data.getVehicle().getCustomer().
				 * getAnniversary_date()); }
				 */

				if (ser_data.getDealerCode() != null) {
					service_advisor.setDealerCode(ser_data.getDealerCode());
				}

				if (service_bookedData.getServiceBookedId() != 0) {
					service_advisor.setServiceBookedId(String.valueOf(service_bookedData.getServiceBookedId()));
				}

				/*
				 * if (ser_data.getCallCount() != 0) {
				 * service_advisor.setCallCount(String.valueOf(ser_data.
				 * getCallCount())); }
				 */

				if (ser_data.getVehicle().getVehicleRegNo() != null) {

					service_advisor.setVehicalRegNo(ser_data.getVehicle().getVehicleRegNo());
				}

				/*
				 * if (ser_data.getVehicle().getVehicleNumber() != null) {
				 * service_advisor.setVehicleNumber(ser_data.getVehicle().
				 * getVehicleNumber()); }
				 */

				if (ser_data.getVehicle().getModel() != null) {
					service_advisor.setModel(ser_data.getVehicle().getModel());
				}

				/*
				 * if (ser_data.getVehicle().getFuelType() != null) {
				 * service_advisor.setFuelType(ser_data.getVehicle().getFuelType
				 * ()); }
				 */

				if (service_data.getLastServiceDate() != null) {

					service_advisor.setLastServiceDate(service_data.getLastServiceDate().toString());
				}

				if (service_data.getLastServiceType() != null) {

					service_advisor.setLastServiceType(service_data.getLastServiceType());
				}

				if (service_data.getLastServiceMeterReading() != null) {

					service_advisor.setLastServiceMileage(service_data.getLastServiceMeterReading());
				}

				if (service_bookedData.getWorkshop() != null) {

					service_advisor.setWorkshop(service_bookedData.getWorkshop().getWorkshopName());

				}

				/*
				 * if (ser_data.getVehicle().getNextServicedate() != null) {
				 * 
				 * service_advisor.setNextServiceDate(ser_data.getVehicle().
				 * getNextServicedate().toString()); }
				 */

				if (service_bookedData.getServiceScheduledDateStr() != null) {

					String schdDateTime = service_bookedData.getServiceScheduledDateStr() + " "
							+ service_bookedData.getServiceScheduledTimeStr();

					service_advisor.setServiceScheduledDate(schdDateTime);

				}

				if (service_bookedData.getServiceBookedType() != null) {

					service_advisor.setServiceType(service_bookedData.getServiceBookedType());
				}

				if (service_bookedData.getTypeOfPickup() != null) {

					String typeIs = service_bookedData.getTypeOfPickup();
					if (typeIs.equals("true")) {
						typeIs = "Pick up Required";
					}
					service_advisor.setPickUpRequired(typeIs);
				}
				
				if (ser_data.getWyzUser() != null) {

					service_advisor.setCreName(ser_data.getWyzUser().getUserName());
				}


				if (ser_data.getCallDate() != null) {

					service_advisor.setInteractionDate(ser_data.getCallDate());
				}

				if (ser_data.getCallTime() != null) {

					service_advisor.setInteractionTime(ser_data.getCallTime());
				}

				if (ser_data.getVehicle().getSaleDate() != null) {
					service_advisor.setSaleDate(ser_data.getVehicle().getSaleDate().toString());

				}

				/*
				 * if (ser_data.getVehicle().getForecastLogic() != null) {
				 * service_advisor.setForecastLogic(ser_data.getVehicle().
				 * getForecastLogic()); }
				 * 
				 * if (ser_data.getVehicle().getChassisNo() != null) {
				 * service_advisor.setChassisNo(ser_data.getVehicle().
				 * getChassisNo()); }
				 */

				if (ser_data.getSrdisposition().getComments() != null) {
					service_advisor.setComments(ser_data.getSrdisposition().getComments());
				}
				service_advisor.setMakeCallFrom("ServiceAdvisor");
				service_advisor.setUploadedDate(ser_data.getCallDate());
				service_advisor.setStatus("Not yet Called");
				newref.setValue(service_advisor);
				repo.updateSynchedToFirebase(ser_data.getId(), referencekey);

				String fireUrl1 = userkeysUrl + "/";
				fireUrl1 += userName;
				fireUrl1 += "/";
				fireUrl1 += "LastCallHistory";
				fireUrl1 += "/";

				logger.info("fireUrl is :" + fireUrl);
				ref1 = FirebaseDatabase.getInstance().getReferenceFromUrl(fireUrl1);

				DatabaseReference newref1 = ref1.child(customerPhoneData);
				String referencekey1 = newref1.getKey();
				newref1.setValue(service_advisor);

			}

		}
		}
		 }
	}

}
