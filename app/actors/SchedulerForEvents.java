package actors;

import java.util.Map;
import java.util.Set;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import akka.actor.UntypedActor;
import repositories.FirebaseSyncRepository;

public class SchedulerForEvents extends UntypedActor{

	@Override
	public void onReceive(Object protocol) throws Exception {
		// TODO Auto-generated method stub
		
		InsuranceAgentSyncProtocol.StartSync fireproto = (InsuranceAgentSyncProtocol.StartSync) protocol;
		FirebaseSyncRepository repo = fireproto.firebaseRepo;

		String firebaseUrl = fireproto.urlToSync;

		RestTemplate restTemplate = new RestTemplate();
		String dealerkeysUrl = firebaseUrl + ".json?auth=vki9nCdP8QQpaWX0NG7iDrxznBlrmzo56enmA34L&shallow=true";

		ResponseEntity<Map> result = restTemplate.getForEntity(dealerkeysUrl, Map.class);

		Map dealerMap = result.getBody();

		/*if (dealerMap != null) {

		 Set dealerkeys = dealerMap.keySet();

		 for (Object key : dealerkeys) {*/
		String dealer = "PRODUCTTEST";
			// String dealer = (String) key;
			 
			 repo.startEventsOfCRM(dealer);
			 
		// }
		//}
		
	}

}
