package actors;

import java.util.List;

import play.Logger;
import models.Driver;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import repositories.FirebaseSyncRepository;
import actors.model.DriverPickupInfo;
import akka.actor.UntypedActor;

public class DriverHistorySync extends UntypedActor {
	
	Logger.ALogger logger = play.Logger.of("application");

	@Override
	public void onReceive(Object protocol) throws Exception {
		DriverHistorySyncProtocol.StartSync fireproto = (DriverHistorySyncProtocol.StartSync) protocol;

		logger.info("Recieved the message to start the history sync task");
		
		String firebaseUrl = fireproto.urlToSync;

		FirebaseSyncRepository repo = fireproto.firebaseSyncRepo;

		List<Driver> drivers = repo.getAllDrivers();

		for (Driver driver : drivers) {

			String userName = driver.getWyzUser().getUserName();
			firebaseUrl += driver.getWyzUser().getDealerCode();
			firebaseUrl += "/Driver/";
			firebaseUrl += userName;
			firebaseUrl += "/DriverHistory";

			DatabaseReference ref = FirebaseDatabase.getInstance()
					.getReferenceFromUrl(firebaseUrl);

			DriverHistoryListener listener = new DriverHistoryListener(repo);
			
			ref.addChildEventListener(listener);

		}
	}
}
