package actors;

import repositories.FirebaseSyncRepository;

public class PSFMobileCallSyncProtocol {

	
	public static class StartSync{
	    
	    public final String urlToSync;
	    public final FirebaseSyncRepository firebaseRepo;
	    public StartSync(String url, FirebaseSyncRepository firebaseRepository){
	    
	    this.urlToSync = url;
	    this.firebaseRepo=firebaseRepository;
	    
	    }
	    
	    }
}
