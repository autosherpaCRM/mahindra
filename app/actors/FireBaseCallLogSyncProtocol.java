package actors;

import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.WyzUserRepository;

public class FireBaseCallLogSyncProtocol {

	public static class StartSync {

		public final String urlToSync;

		public final CallInfoRepository callLogRepository;

		public final WyzUserRepository userRepository;

		public final CallInteractionsRepository callInteractionRepository;

		public StartSync(String url, CallInfoRepository repository,
				WyzUserRepository userRepo,
				CallInteractionsRepository callIntRepo) {
			this.urlToSync = url;
			this.callLogRepository = repository;
			this.userRepository = userRepo;
			this.callInteractionRepository = callIntRepo;
		}
	}
}
