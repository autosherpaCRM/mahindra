package actors;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import actors.model.ServiceAdvisorHistoryInfo;
import akka.actor.UntypedActor;
import play.Logger;
import repositories.FirebaseSyncRepository;
import models.CallInteraction;
import models.Insurance;
import models.AppointmentBooked;

public class InsuranceAgentCallSync extends UntypedActor {

	Logger.ALogger logger = play.Logger.of("application");

	@Override
	public void onReceive(Object protocol) throws Exception {
		// TODO Auto-generated method stub

		InsuranceAgentSyncProtocol.StartSync fireproto = (InsuranceAgentSyncProtocol.StartSync) protocol;
		FirebaseSyncRepository repo = fireproto.firebaseRepo;

		String firebaseUrl = fireproto.urlToSync;

		RestTemplate restTemplate = new RestTemplate();
		String dealerkeysUrl = firebaseUrl + ".json?auth=vki9nCdP8QQpaWX0NG7iDrxznBlrmzo56enmA34L&shallow=true";

		ResponseEntity<Map> result = restTemplate.getForEntity(dealerkeysUrl, Map.class);

		Map dealerMap = result.getBody();
		if (dealerMap != null) {

			Set dealerkeys = dealerMap.keySet();

			for (Object key : dealerkeys) {
				String dealer = (String) key;
		//String dealer = "PRODUCTTEST";
		String userkeysUrl = firebaseUrl + dealer;
		userkeysUrl += "/InsuranceAgent";

		DatabaseReference ref;
		DatabaseReference ref1;

		if (repo != null) {

			List<CallInteraction> listNotSyncedAppointments = repo.getAllNotSyncedAppointment(dealer);
			for (CallInteraction appointment_data : listNotSyncedAppointments) {

				AppointmentBooked appointment = repo
						.getAppointmentById(appointment_data.getAppointmentBooked().getAppointmentId());

				Insurance insurance = repo.getLatestInsuranceOfVehicle(appointment_data.getVehicle().getVehicle_id());

				String userName = appointment.getInsuranceAgent().getInsuranceAgentName();

				String fireUrl = userkeysUrl + "/";
				fireUrl += userName;
				fireUrl += "/";
				fireUrl += "ScheduledCalls";
				fireUrl += "/";
				fireUrl += "CallInfo";
				fireUrl += "/";

				logger.info("fireUrl is :" + fireUrl);
				ref = FirebaseDatabase.getInstance().getReferenceFromUrl(fireUrl);

				DatabaseReference newref = ref.push();

				String referencekey = newref.getKey();
				logger.info("referencekey : " + referencekey);

				ServiceAdvisorHistoryInfo insurance_agent = new ServiceAdvisorHistoryInfo();

				if (appointment_data.getVehicle() != null) {
					insurance_agent.setVehicleId(String.valueOf(appointment_data.getVehicle().getVehicle_id()));
				}

				if (appointment_data.getCustomer() != null) {
					insurance_agent.setCustId(String.valueOf(appointment_data.getCustomer().getId()));
				}

				if (appointment.getInsuranceAgent().getWyzUser() != null) {
					insurance_agent.setUserId(String.valueOf(appointment.getInsuranceAgent().getWyzUser().getId()));
				}

				if (appointment_data.getCustomer() != null) {
					insurance_agent.setCustomerName(appointment_data.getCustomer().getCustomerName());
				}
				if (appointment_data.getCustomer().getPreferredPhone() != null) {

					insurance_agent
							.setCustomerPhone(appointment_data.getCustomer().getPreferredPhone().getPhoneNumber());
				}

				if (appointment_data.getDealerCode() != null) {
					insurance_agent.setDealerCode(appointment_data.getDealerCode());
				}

				if (appointment_data.getVehicle().getVehicleRegNo() != null) {

					insurance_agent.setVehicalRegNo(appointment_data.getVehicle().getVehicleRegNo());
				}

				if (appointment_data.getVehicle().getModel() != null) {
					insurance_agent.setModel(appointment_data.getVehicle().getModel());
				}

				if (appointment_data.getVehicle().getSaleDate() != null) {
					insurance_agent.setSaleDate(appointment_data.getVehicle().getSaleDate().toString());

				}

				if (appointment_data.getVehicle().getChassisNo() != null) {
					insurance_agent.setChassisNo(appointment_data.getVehicle().getChassisNo());
				}

				if (appointment.getAppointmentDateStr() != null) {

					String schdDateTime = appointment.getAppointmentDateStr();

					insurance_agent.setServiceScheduledDate(schdDateTime);

				}
				if (appointment.getAppointmentFromTime() != null) {

					insurance_agent.setAppointmentTime(appointment.getAppointmentFromTime());

				}
				if (appointment.getAddressOfVisit() != null) {

					insurance_agent.setCustomerAddress(appointment.getAddressOfVisit());

				}

				if (appointment.getRenewalType() != null) {

					insurance_agent.setServiceType(appointment.getRenewalType());
				}

				if (appointment_data.getWyzUser() != null) {

					insurance_agent.setCreName(appointment_data.getWyzUser().getUserName());
				}

				if (appointment_data.getInsuranceDisposition().getComments() != null) {
					insurance_agent.setComments(appointment_data.getInsuranceDisposition().getComments());
				}

				if (appointment_data.getCallDate() != null) {

					insurance_agent.setInteractionDate(appointment_data.getCallDate());

					String schdDateTime = appointment_data.getCallDate() + " " + appointment_data.getCallTime();

					insurance_agent.setInteractionDate(schdDateTime);

				}

				if (insurance.getInsuranceCompanyName() != null) {

					insurance_agent.setInsuranceCompany(insurance.getInsuranceCompanyName());

				}
				if (insurance.getPolicyNo() != null) {

					insurance_agent.setLastPolicyNo(insurance.getPolicyNo());

				}

				if (insurance.getIdv() != 0) {

					insurance_agent.setLastIDV(String.valueOf(insurance.getIdv()));

				}

				if (insurance.getPremiumAmountStr() != null) {

					insurance_agent.setLastPremium(insurance.getPremiumAmountStr());

				}

				if (appointment.getRenewalMode() != null) {

					insurance_agent.setRenewalMode(appointment.getRenewalMode());

				}

				if (appointment.getRenewalType() != null) {

					insurance_agent.setRenewalType(appointment.getRenewalType());

				}

				if (appointment.getPremiumwithTax() != 0) {

					insurance_agent.setPremium(String.valueOf(appointment.getPremiumwithTax()));

				}
				if (appointment.getAppointmentId() != 0) {

					insurance_agent.setServiceBookedId(String.valueOf(appointment.getAppointmentId()));

				}
				
				String pattern = "dd/MM/yyyy";
				java.util.TimeZone tz = java.util.TimeZone.getTimeZone("IST");
				Calendar c = Calendar.getInstance(tz);

				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
				simpleDateFormat.setTimeZone(tz);
				String currentDateIs = simpleDateFormat.format(c.getTime());
				
				insurance_agent.setMakeCallFrom("Insurance Agent");
				insurance_agent.setUploadedDate(currentDateIs);
				insurance_agent.setStatus("Not yet Called");

				newref.setValue(insurance_agent);
				repo.updateSynchedToFirebase(appointment_data.getId(), referencekey);

				String fireUrl1 = userkeysUrl + "/";
				fireUrl1 += userName;
				fireUrl1 += "/";
				fireUrl1 += "LastCallHistory";
				fireUrl1 += "/";

				logger.info("fireUrl is :" + fireUrl);
				ref1 = FirebaseDatabase.getInstance().getReferenceFromUrl(fireUrl1);

				DatabaseReference newref1 = ref1
						.child(appointment_data.getCustomer().getPreferredPhone().getPhoneNumber());
				String referencekey1 = newref1.getKey();
				newref1.setValue(insurance_agent);

			}
		}
			}
		}

	}

}
