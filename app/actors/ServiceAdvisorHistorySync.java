package actors;

import java.util.*;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import akka.actor.UntypedActor;
import play.Logger;
import repositories.FirebaseSyncRepository;
import models.ServiceAdvisor;

public class ServiceAdvisorHistorySync extends UntypedActor {

	Logger.ALogger logger = play.Logger.of("application");

	@Override
	public void onReceive(Object protocol) throws Exception {
		ServiceAdvisorHistorySyncProtocol.StartSync fireproto = (ServiceAdvisorHistorySyncProtocol.StartSync) protocol;

		logger.info("Recieved the message to start the history sync task");

		String firebaseUrl = fireproto.urlToSync;

		FirebaseSyncRepository repo = fireproto.firebaseSyncRepo;

		RestTemplate restTemplate = new RestTemplate();
		String dealerkeysUrl = firebaseUrl + ".json?auth=vki9nCdP8QQpaWX0NG7iDrxznBlrmzo56enmA34L&shallow=true";

		ResponseEntity<Map> result = restTemplate.getForEntity(dealerkeysUrl, Map.class);

		Map dealerMap = result.getBody();

		if (dealerMap != null) {

			Set dealerkeys = dealerMap.keySet();

			for (Object key : dealerkeys) {
				String dealer = (String) key;
		
				//String dealer = "MAHINDRATEST";

				List<ServiceAdvisor> serviceAdvisors = repo.getAllActiveServiceAdvisors(dealer);

				logger.info("count of serviceAdvisors : " + serviceAdvisors.size());

				for (ServiceAdvisor advisor : serviceAdvisors) {

					// logger.info("advisor in name
					// :"+advisor.getAdvisorName()+"
					// username :"+advisor.getWyzUser().getUserName());

					String userName = advisor.getWyzUser().getUserName();
					String fireUrl = firebaseUrl;
					fireUrl += dealer;
					fireUrl += "/ServiceAgent/";
					fireUrl += userName;
					fireUrl += "/SMRHistory";
					fireUrl += "/";
					fireUrl += "CallInfo";
					fireUrl += "/";
					logger.info("serviceAdvisors firebaseUrl : " + fireUrl);
					DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(fireUrl);

					ServiceAdvisorHistoryListener listener = new ServiceAdvisorHistoryListener(repo);

					ref.addChildEventListener(listener);

				}
			}
		}

	}

}
