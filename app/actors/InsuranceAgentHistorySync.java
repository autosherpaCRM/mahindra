package actors;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import akka.actor.UntypedActor;
import models.InsuranceAgent;
import play.Logger;
import repositories.FirebaseSyncRepository;

public class InsuranceAgentHistorySync extends UntypedActor{
	
	Logger.ALogger logger = play.Logger.of("application");

	@Override
	public void onReceive(Object protocol) throws Exception {

		InsuranceAgentSyncProtocol.StartSync fireproto = (InsuranceAgentSyncProtocol.StartSync) protocol;
		FirebaseSyncRepository repo = fireproto.firebaseRepo;

		String firebaseUrl = fireproto.urlToSync;

		RestTemplate restTemplate = new RestTemplate();
		String dealerkeysUrl = firebaseUrl + ".json?auth=vki9nCdP8QQpaWX0NG7iDrxznBlrmzo56enmA34L&shallow=true";

		ResponseEntity<Map> result = restTemplate.getForEntity(dealerkeysUrl, Map.class);

		Map dealerMap = result.getBody();

		if (dealerMap != null) {

		 Set dealerkeys = dealerMap.keySet();

		 for (Object key : dealerkeys) {
		 String dealer = (String) key;
		//String dealer = "PRODUCTTEST";
		
		List<InsuranceAgent> insuranceAgent = repo.getAllActiveInsuranceAgent(dealer);

		logger.info("count of serviceAdvisors : " + insuranceAgent.size());

		for (InsuranceAgent agent : insuranceAgent) {

			// logger.info("advisor in name
			// :"+advisor.getAdvisorName()+"
			// username :"+advisor.getWyzUser().getUserName());

			String userName = agent.getInsuranceAgentName();
			String fireUrl = firebaseUrl;
			fireUrl += dealer;
			fireUrl += "/InsuranceAgent/";
			fireUrl += userName;
			fireUrl += "/History";
			fireUrl += "/";
			fireUrl += "CallInfo";
			fireUrl += "/";
			logger.info("serviceAdvisors firebaseUrl : " + fireUrl);
			DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(fireUrl);

			InsuranceAgentHistoryListener listener = new InsuranceAgentHistoryListener(repo);

			ref.addChildEventListener(listener);

		}
		
	
 }
}

	}

}
