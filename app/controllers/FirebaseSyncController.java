/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.pac4j.core.profile.CommonProfile;

import play.Logger;
import play.libs.Akka;
import play.mvc.Result;
import repositories.FirebaseSyncRepository;
import scala.concurrent.duration.Duration;
import actors.PSFMobileCallSyncProtocol;
import actors.SchedulerForEvents;
import actors.PSFMobileCallSync;
import actors.DriverHistorySync;
import actors.DriverHistorySyncProtocol;
import actors.DriverPickupDropListSync;
import actors.DriverPickupDropListSyncProtocol;
import actors.FireBaseSyncServiceBookedStatus;
import actors.FireBaseSyncServiceBookedStatusCount;
import actors.InsuranceAgentCallSync;
import actors.InsuranceAgentHistorySync;
import actors.InsuranceAgentSyncProtocol;
import actors.ServiceAdvisorCallSync;
import actors.ServiceAdvisorCallSyncProtocol;
import actors.ServiceAdvisorHistorySync;
import actors.ServiceAdvisorHistorySyncProtocol;
import actors.ServiceAdvisorSummaryDayWiseSync;
import actors.ServiceAdvisorPsfHistorySync;
import actors.ServiceAdvisorPsfHistorySyncProtocol;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.Props;

/**
 *
 * @author W-885
 */
public class FirebaseSyncController extends play.mvc.Controller {

	Logger.ALogger logger = play.Logger.of("application");

	private final FirebaseSyncRepository fireBaseRepo;
	private Cancellable scheduler = null;
	private Cancellable driverPickupDrop = null;
	private Cancellable driverHistory = null;
	private Cancellable serviceHistory = null;
	private Cancellable summaryAdvisorDayWise = null;
	private Cancellable psfScheduler = null;
	private Cancellable psfHistory = null;
	private Cancellable insuranceScheduler=null;
	private Cancellable insuranceSchedulerHistory=null;
	private Cancellable eventScheduler=null;

	private PlaySessionStore playSessionStore;

	private org.pac4j.core.config.Config config;

	@Inject
	public FirebaseSyncController(FirebaseSyncRepository fireRepo, org.pac4j.core.config.Config config,
			PlaySessionStore plstore) {
		fireBaseRepo = fireRepo;
		config = (org.pac4j.core.config.Config) config;
		playSessionStore = plstore;

	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getTimeZoneOfDealerCode() {

		String time_zone = (String) getUserProfile().getAttribute("TIME_ZONE");

		return time_zone;

	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	public Result testDriverData() {

		fireBaseRepo.addTestServiceBooked();

		return ok("done");

	}

	public Result startSyncOperationDriverPickupDropList() {

		boolean startsyncState = false;
		if (driverPickupDrop != null) {
			if (driverPickupDrop.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(DriverPickupDropListSync.class));
			driverPickupDrop = Akka.system().scheduler().schedule( // REPLACE
																	// BELOW for
																	// executing
																	// every day
																	// at 6
																	// Duration.create(nextExecutionInSeconds(6,
																	// 0),
																	// TimeUnit.SECONDS),//
																	// Duration.create(24,
																	// TimeUnit.HOURS),
																	// //
					Duration.create(0, TimeUnit.MICROSECONDS), //
					Duration.create(15, TimeUnit.MINUTES), //
					actor,
					new DriverPickupDropListSyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), fireBaseRepo),
					Akka.system().dispatcher(), null);
		}

		return ok("Done DriverPickups sync started");

	}

	public Result startDriverHistorySync() {

		boolean startsyncState = false;
		if (driverHistory != null) {
			if (driverHistory.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(DriverHistorySync.class));
			driverHistory = Akka.system().scheduler().scheduleOnce(Duration.create(0, TimeUnit.MILLISECONDS), // Initial
																												// delay
																												// 0
																												// milliseconds
					actor,
					new DriverHistorySyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), fireBaseRepo),
					Akka.system().dispatcher(), null);
		}

		return ok("Done Driver History Sync Started");
	}

	public Result startServiceAdvisorHistorySync() {

		boolean startsyncState = false;
		if (serviceHistory != null) {
			if (serviceHistory.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(ServiceAdvisorHistorySync.class));
			serviceHistory = Akka.system().scheduler().schedule( 
					Duration.create(0, TimeUnit.MICROSECONDS), 
					Duration.create(1, TimeUnit.MINUTES),
					 actor,
					new ServiceAdvisorHistorySyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), fireBaseRepo),
					Akka.system().dispatcher(), null);
		}

		return ok("Service Advisor History Sync Started");
	}

	public Result startServiceAdvisorSummaryDetailSync() {

		boolean startsyncState = false;
		if (summaryAdvisorDayWise != null) {
			if (summaryAdvisorDayWise.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(ServiceAdvisorSummaryDayWiseSync.class));
			summaryAdvisorDayWise = Akka.system().scheduler().schedule( 
					Duration.create(0, TimeUnit.MICROSECONDS), 
					Duration.create(5, TimeUnit.MINUTES),															// 0
																														// milliseconds
					actor,
					new ServiceAdvisorHistorySyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), fireBaseRepo),
					Akka.system().dispatcher(), null);
		}

		return ok("Service Advisor Summary Sync Started");

	}

	public Result startSyncOperationServiceAgent() {

		boolean startsyncState = false;
		if (scheduler != null) {
			if (scheduler.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(ServiceAdvisorCallSync.class));
			scheduler = Akka.system().scheduler().schedule( 
					Duration.create(0, TimeUnit.MICROSECONDS), 
					Duration.create(1, TimeUnit.MINUTES),
					 actor,
					new ServiceAdvisorCallSyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), fireBaseRepo),
					Akka.system().dispatcher(), null);
		}

		return ok("Service Agent call sync started");

	}
	
	
	
	public Result stopSyncOperationServiceAgent() {

		if (scheduler != null) {
			if (!scheduler.isCancelled()) {
				scheduler.cancel();
			}
		}

		return ok("Service Agent call sync stopped");

	}
	
	public Result stopServiceAdvisorSummaryDetailSync() {

		if (summaryAdvisorDayWise != null) {
			if (!summaryAdvisorDayWise.isCancelled()) {
				summaryAdvisorDayWise.cancel();
			}
		}

		return ok("Service Summary call sync stopped");

	}
	
	
	public Result stopServiceAdvisorHistorySync() {

		if (serviceHistory != null) {
			if (!serviceHistory.isCancelled()) {
				serviceHistory.cancel();
			}
		}

		return ok("Service History call sync stopped");

	}

	public Result startSyncOperationPSFCallServiceAgent() {
		boolean startsyncState = false;
		if (psfScheduler != null) {
			if (psfScheduler.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(PSFMobileCallSync.class));
			psfScheduler = Akka.system().scheduler().schedule( // REPLACE BELOW
																// for executing
																// every day at
																// 6
																// Duration.create(nextExecutionInSeconds(6,
																// 0),
																// TimeUnit.SECONDS),//
																// Duration.create(24,
																// TimeUnit.HOURS),
																// //
					Duration.create(0, TimeUnit.MICROSECONDS), //
					Duration.create(15, TimeUnit.MINUTES), //
					actor,
					new PSFMobileCallSyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), fireBaseRepo),
					Akka.system().dispatcher(), null);
		}
		return ok("Service Agent psf call sync started");

	}

	public Result stopSyncOperationPSFCallServiceAgent() {

		if (psfScheduler != null) {
			if (!psfScheduler.isCancelled()) {
				psfScheduler.cancel();
			}
		}

		return ok("Service Agent call sync stopped");

	}

	

	public Result syncServiceBookedSummmary() {

		boolean startsyncState = false;
		if (scheduler != null) {
			if (scheduler.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(FireBaseSyncServiceBookedStatus.class));
			scheduler = Akka.system().scheduler().schedule(Duration.create(0, TimeUnit.MILLISECONDS), // Initial
																										// delay
																										// 0
																										// milliseconds
					Duration.create(1, TimeUnit.MINUTES), // Frequency 1
															// minutes
															// actor,
					actor,
					new ServiceAdvisorCallSyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), fireBaseRepo),
					Akka.system().dispatcher(), null);
		}

		return ok("service data synched");
	}

	public Result syncStatusOfServiceBookedCount() {

		boolean startsyncState = false;
		if (scheduler != null) {
			if (scheduler.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(FireBaseSyncServiceBookedStatusCount.class));
			scheduler = Akka.system().scheduler().schedule(Duration.create(0, TimeUnit.MILLISECONDS), // Initial
																										// delay
																										// 0
																										// milliseconds
					Duration.create(1, TimeUnit.MINUTES), // Frequency 1
															// minutes
															// actor,
					actor,
					new ServiceAdvisorCallSyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), fireBaseRepo),
					Akka.system().dispatcher(), null);
		}

		return ok("service data synched count");

	}

	public static int nextExecutionInSeconds(int hour, int minute) {
		return Seconds.secondsBetween(new DateTime(), nextExecution(hour, minute)).getSeconds();
	}

	public static DateTime nextExecution(int hour, int minute) {
		DateTime next = new DateTime().withHourOfDay(hour).withMinuteOfHour(minute).withSecondOfMinute(0)
				.withMillisOfSecond(0);

		return (next.isBeforeNow()) ? next.plusHours(24) : next;
	}

	public Result startServiceAdvisorPSFHistorySync() {

		boolean startsyncState = false;
		if (psfHistory != null) {
			if (psfHistory.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(ServiceAdvisorPsfHistorySync.class));
			psfHistory = Akka.system().scheduler().scheduleOnce(Duration.create(0, TimeUnit.MILLISECONDS), actor,
					new ServiceAdvisorPsfHistorySyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), fireBaseRepo),
					Akka.system().dispatcher(), null);
		}

		return ok("Service Advisor PSF History Sync Started");
	}
	
	//INSURANCE AGENT SYNC
	
	public Result startSyncOperationInsuranceAgent() {

		boolean startsyncState = false;
		if (insuranceScheduler != null) {
			if (insuranceScheduler.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(InsuranceAgentCallSync.class));
			insuranceScheduler = Akka.system().scheduler().schedule( 
					Duration.create(0, TimeUnit.MICROSECONDS), 
					Duration.create(10, TimeUnit.MINUTES),
					 actor,
					new InsuranceAgentSyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), fireBaseRepo),
					Akka.system().dispatcher(), null);
		}

		return ok("Insurance Agent call sync started");

	}
	
	//INSURANCE AGENT SYNC HISTORY
	
	public Result startSyncOperationInsuranceAgentHistory() {

		boolean startsyncState = false;
		if (insuranceSchedulerHistory != null) {
			if (insuranceSchedulerHistory.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(InsuranceAgentHistorySync.class));
			insuranceSchedulerHistory = Akka.system().scheduler().schedule( 
					Duration.create(0, TimeUnit.MICROSECONDS), 
					Duration.create(10, TimeUnit.MINUTES),
					 actor,
					new InsuranceAgentSyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), fireBaseRepo),
					Akka.system().dispatcher(), null);
		}

		return ok("Insurance Agent call sync started");

	}
	
	public Result startSyncOperationAllEvents() {

		boolean startsyncState = false;
		if (eventScheduler != null) {
			if (eventScheduler.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(SchedulerForEvents.class));
			eventScheduler = Akka.system().scheduler().schedule( 
					Duration.create(0, TimeUnit.MICROSECONDS), 
					Duration.create(12, TimeUnit.HOURS),
					 actor,
					new InsuranceAgentSyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), fireBaseRepo),
					Akka.system().dispatcher(), null);
		}

		return ok("eventScheduler");

	}
	
	

}
