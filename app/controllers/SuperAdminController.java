/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import static play.libs.Json.toJson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject; 
import play.libs.Json; 
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import controllers.webmodels.AssignedIntreactionNoCall;
import models.ListingForm;
import models.Location;
import models.Role;
import models.Workshop;
import models.WyzUser;
import models.Campaign;
import play.Logger.ALogger;
import play.data.Form;
import play.mvc.Result;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.SMSTriggerRespository;
import repositories.SuperAdminRepository;
import repositories.WyzUserRepository;
import views.html.addMasterDataSuperAdmin;
import views.html.addMasterDataUsersBySuperAdmin;
import views.html.deleteNoCallAssignedInteraction;

@Controller
public class SuperAdminController extends play.mvc.Controller {

	ALogger logger = play.Logger.of("application");
	private final CallInfoRepository repo;
	private final CallInteractionsRepository call_int_repo;
	private final WyzUserRepository wyzRepo;
	private final SMSTriggerRespository sms_repo;
	private final SuperAdminRepository superAdminRepo;
	private PlaySessionStore playSessionStore;

	@Inject
	public SuperAdminController(CallInfoRepository repository, org.pac4j.core.config.Config config,
			CallInteractionsRepository interRepo, WyzUserRepository wyzRepository, SMSTriggerRespository smsRepository, SuperAdminRepository AdminRepo,
			PlaySessionStore plstore) {
		repo 			 = repository;
		call_int_repo 	 = interRepo;
		wyzRepo 		 = wyzRepository;
		sms_repo 		 = smsRepository;
		superAdminRepo 	 = AdminRepo;
		config 		     = (org.pac4j.core.config.Config) config;
		playSessionStore = plstore;

	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");
		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();
		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");
		return dealerName;

	}

	private String getOEMOfDealer() {

		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;

	}
	
	
	
	
	// Add location by Super admin method
	/**
	 *
	 * @return
	 */
	@Secure(clients = "FormClient")
	public Result LocationBySuperAdmin() {
		List<Location> locationList = call_int_repo.getLocationList();
		List<Workshop> workshopList = call_int_repo.getWorkshopList();
		return ok(addMasterDataSuperAdmin.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(), getUserLoginName(), locationList , workshopList));
	}
	
	
	@Secure(clients = "FormClient")
	public Result postLocationBySuperAdmin(){
		Form<Location> Location_data    = Form.form(Location.class).bindFromRequest();
		Location Location			    = Location_data.get();
		
		
	
		Form<Workshop> Workshop_data 	= Form.form(Workshop.class).bindFromRequest();
		Workshop Workshop 				= Workshop_data.get();
		
		
		superAdminRepo.addLocationdataBySuperAdmin(Location, Workshop, getUserLoginName());
		return redirect(controllers.routes.SuperAdminController.LocationBySuperAdmin());
	}
	
	@Secure(clients = "FormClient")
	public Result getListCampaignByModule(String selectedCampaigns) {

		logger.info("selected Campaign :"+selectedCampaigns);
		List<String> selectedCampaign = Arrays.asList(selectedCampaigns.split(","));
		List<String> campaign_full_list = new ArrayList<String>();;
		for(String cname :selectedCampaign) {
			logger.info(cname);
			List<String> campaign_list = superAdminRepo.getCampaignByType(cname);
			if(campaign_list == null) {
				continue;
			}
			campaign_full_list.addAll(campaign_list);
		}
		
		return ok(toJson(campaign_full_list));

	}
	
	
@Secure(clients = "FormClient")
public Result AssignedInteractionDeletion() {
	List<Location> locationList = call_int_repo.getLocationList();
	List<Workshop> workshopList = call_int_repo.getWorkshopList();
	List<Campaign> campaignList = call_int_repo.getAllCampaignList();
	List<WyzUser> users = wyzRepo.getAllUsers("BALAJI");

return ok(deleteNoCallAssignedInteraction.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(), getUserLoginName(),users,locationList,workshopList,campaignList));
}

@SuppressWarnings("deprecation")
@Secure(clients = "FormClient")
public Result deleteAssignedInteractionNoCall() throws Exception{

	Form<ListingForm> listingForm = Form.form(ListingForm.class).bindFromRequest();
	ListingForm Listing_Form = listingForm.get();
	//logger.info(""+Listing_Form.getAssignedNoCallId().size());
	superAdminRepo.deleteAssignedNoCall(Listing_Form);
	/*JOptionPane.showMessageDialog(null, 
	        "No Call Made Assignments Deleted", 
	        "Done", 
	        JOptionPane.PLAIN_MESSAGE);*/
	return redirect(controllers.routes.SuperAdminController.AssignedInteractionDeletion());

}

@Secure(clients = "FormClient")
public Result assignedInteractionData(long wyzuserId,String location, String moduleName,String campaignName,String fromDate,String toDate) throws ParseException {
	
	//List<WyzUser> wyzUsersList = getAllWyzUsers();
	
	Map<String, String[]> paramMap = request().queryString();
	ObjectNode result = Json.newObject();
	
	Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(fromDate); 
	Date date2=new SimpleDateFormat("yyyy-MM-dd").parse(toDate); 
	List<AssignedIntreactionNoCall> assignedNoCallList = superAdminRepo.getAssignedInteractionListData(wyzuserId, location, moduleName, campaignName, date1, date2);
	
	logger.info("assignedNoCallList size " +assignedNoCallList.size());
	long toIndex = Long.valueOf(paramMap.get("length")[0]);
	
	long totalSize = assignedNoCallList.size();
	
	logger.info("total size : "+totalSize);
	
	if (toIndex < 0) {
		toIndex = 10;
	
	}
	
	if (toIndex > totalSize) {
		toIndex = totalSize;
	}
	
	result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
	result.put("recordsTotal", totalSize);
	result.put("recordsFiltered", totalSize);
	ArrayNode an = result.putArray("data");
	
	for (AssignedIntreactionNoCall a : assignedNoCallList) {
		long id1 = a.getNoCallAssignId();
		ObjectNode row = Json.newObject();
		row.put("0", a.getWyzUserName());
		row.put("1", a.getLocations());
		row.put("2", a.getCampaignName());
		row.put("3", a.getCustomerName());
		row.put("4", a.getVehicleId());
		row.put("5", a.getChassisNo());
		row.put("6", a.getModel());
		row.put("7", a.getVehicleRegNo());
		row.put("8", a.getUploadDate().toString());
		row.put("9","<input type=\"hidden\" name=\"assignedNoCallId[]\" value="+id1+">");
		an.add(row);
	}
	
	return ok(result);

}
	
	@Secure(clients = "FormClient")
	public Result UsersBySuperAdmin(){
		List<Location> locationList = call_int_repo.getLocationList();
		List<Workshop> workshopList = call_int_repo.getWorkshopList();
		List<Role> roleList 		= call_int_repo.getRoles();
		return ok(addMasterDataUsersBySuperAdmin.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(), getUserLoginName(),locationList,workshopList,roleList));
	}
	
	@Secure(clients = "FormClient")
	public Result postUserBySuperAdmin(){
		Form<WyzUser> Wyzuser_data 		= Form.form(WyzUser.class).bindFromRequest();
		WyzUser WyzUser 				= Wyzuser_data.get();
		
		Form<Location> Location_data 	= Form.form(Location.class).bindFromRequest();
		Location Location 				= Location_data.get();
		
		Form<Workshop> workshop_data 	= Form.form(Workshop.class).bindFromRequest();
		Workshop Workshop 				= workshop_data.get();
		
		Form<Role> roleData 			= Form.form(Role.class).bindFromRequest();
		Role Role 						= roleData.get();
		
		//logger.info("inside controller:"+ Location.getName());
		superAdminRepo.addUsersBySuperAdmin(WyzUser , Location, Workshop ,Role,getUserLoginName(),getUserLogindealerCode());
		
		return redirect(controllers.routes.SuperAdminController.UsersBySuperAdmin());
	}
	
	
	@Secure(clients = "FormClient")
	public Result checkIfUserExists(String uname){
		List<WyzUser> UsersList = superAdminRepo.getExistingWyzUsers(uname);
		return ok(toJson(UsersList));
	}
	

}