/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

/**
 *
 * @author W-885
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import models.CallInteraction;
import models.MessageTemplete;
import models.Service;
import models.Vehicle;

import org.pac4j.core.profile.CommonProfile;

import org.springframework.stereotype.Controller;

import actors.ProcessUploadActor;
import actors.SMSActor;
import actors.SMSActorProtocol;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Cancellable;
import play.Logger.ALogger;
import play.mvc.Result;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.SMSTriggerRespository;
import repositories.ScheduledCallRepository;
import repositories.WyzUserRepository;
import scala.concurrent.ExecutionContextExecutor;
import scala.concurrent.duration.Duration;

import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;

/**
 *
 * @author W-885
 */
@Controller
public class SMSandEmailController extends play.mvc.Controller {

	private final WyzUserRepository userRepository;
	private final SMSTriggerRespository smsRepo;
	private PlaySessionStore playSessionStore;
	
	private ActorSystem system;
	
	private Cancellable smsScheduler;
	
	
	ALogger logger = play.Logger.of("application");

	private org.pac4j.core.config.Config configuration;

	@Inject
	public SMSandEmailController(org.pac4j.core.config.Config config,
			WyzUserRepository userRepo,SMSTriggerRespository repo,
			PlaySessionStore playSession,ActorSystem actorsys) {
		config = (org.pac4j.core.config.Config) config;	
		userRepository = userRepo;
		smsRepo=repo;	
		playSessionStore=playSession;
		system=actorsys;
		
	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	
	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}
	
	@Secure(clients = "FormClient")
	public void sendSMSBulk(List<Long> vehList, String smsTemplete, long userId, 
			String tenentCode,String type){
		
		logger.info(" sendSMSBulk controller");
		
		ActorRef smsSendActor= system.actorOf(SMSActor.props);
		
		SMSActorProtocol.startSMSActor protocol=new SMSActorProtocol.startSMSActor(vehList, smsTemplete, type,
				userId, tenentCode, smsRepo);
		
		ExecutionContextExecutor excecutor = system.dispatcher();
		
		smsScheduler = system.scheduler().scheduleOnce(Duration.create(50, TimeUnit.MILLISECONDS),
				smsSendActor, protocol, excecutor, null);
		
		
	}
	
	@Secure(clients = "FormClient")
	public Result startTriggerSMS(){
		
		List<Long> vehList=new ArrayList<Long>();
		vehList.add((long) 13929);
		
		
		sendSMSBulk( vehList, "Hi messgae", 1,"maruthimotors","CUSTOM");
		
		return ok("done");
	}
	

	
}
