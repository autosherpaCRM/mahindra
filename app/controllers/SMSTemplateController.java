/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import static play.libs.Json.toJson;
import play.data.Form;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.CustomerSearchResult;
import models.MessageParameters;
import models.SMSTemplate;
import models.SavedSearchResult;
import models.Vehicle;
import play.Logger.ALogger;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.SMSTemplateRepository;
import repositories.SMSTriggerRespository;
import repositories.SuperAdminRepository;
import repositories.WyzUserRepository;
import views.html.messageTemplateSuperAdmin;


@Controller
public class SMSTemplateController extends play.mvc.Controller {

	ALogger logger = play.Logger.of("application");
	private final CallInfoRepository repo;
	private final CallInteractionsRepository call_int_repo;
	private final WyzUserRepository wyzRepo;
	private final SMSTriggerRespository sms_repo;
	private final SuperAdminRepository superAdminRepo;
	private final SMSTemplateRepository SMSTemplateRepo;
	private PlaySessionStore playSessionStore;

	@Inject
	public SMSTemplateController(CallInfoRepository repository, org.pac4j.core.config.Config config,
			CallInteractionsRepository interRepo, WyzUserRepository wyzRepository, SMSTriggerRespository smsRepository,
			SuperAdminRepository AdminRepo,SMSTemplateRepository SMSTemplateResultRepo,
			PlaySessionStore plstore) {
		repo 			 = repository;
		call_int_repo 	 = interRepo;
		wyzRepo 		 = wyzRepository;
		sms_repo 		 = smsRepository;
		superAdminRepo 	 = AdminRepo;
		SMSTemplateRepo =  SMSTemplateResultRepo;
		config 		     = (org.pac4j.core.config.Config) config;
		playSessionStore = plstore;

	}
	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");
		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();
		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");
		return dealerName;

	}

	private String getOEMOfDealer() {

		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;

	}
	
	@Secure(clients = "FormClient")
	public Result SMSTemplateBySuperAdmin(){
		
		List<SMSTemplate> smsTemplateList = SMSTemplateRepo.getAllSMSTemplate();
		List<MessageParameters> msgParameters = SMSTemplateRepo.getAllMsgParameters();
		List<SavedSearchResult> savedSearchList = SMSTemplateRepo.getAllSavedSearchList();
		SMSTemplate smsapi = SMSTemplateRepo.getSMSAPI(getDealerName());
		String getsmsapi = smsapi.getSmsAPI();
		logger.info("savesSearchREsult : "+savedSearchList.size());
			
		return ok(messageTemplateSuperAdmin.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(), getUserLoginName(),smsTemplateList , msgParameters ,savedSearchList,getsmsapi));
	}
	
	@Secure(clients = "FormClient")
	public Result checkIfSearchNameExists(String searchnamevalue){
		logger.info("searchnamevalue:"+searchnamevalue);
		long savedsearch = call_int_repo.checkIfSearchNameExists(searchnamevalue);

		return ok(toJson(savedsearch));
	}
	
	/*@Secure(clients = "FormClient")
	public Result SMSTemplateBySuperAdminManager(){
		List<SMSTemplate> smsTemplateList = SMSTemplateRepo.getAllSMSTemplate();
		List<MessageParameters> msgParameters = SMSTemplateRepo.getAllMsgParameters();
		List<SavedSearchResult> savedSearchList = SMSTemplateRepo.getAllSavedSearchList();
			
		return ok(messageTemplateSuperAdminManager.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(), getUserLoginName(),smsTemplateList , msgParameters ,savedSearchList));
	}*/
	
	/*@Secure(clients = "FormClient")
	public Result postSMSTemplate(String messageTemplate,String msgAPI){
		
		logger.info("template");
//		Form<SMSTemplate> smsTemplateData 		= Form.form(SMSTemplate.class).bindFromRequest();
//		SMSTemplate SMSTemplate 				= smsTemplateData.get();
		SMSTemplateRepo.addNewSMSTemplate(messageTemplate,msgAPI,getUserLoginName(),getUserLogindealerCode());
		return redirect(controllers.routes.SMSTemplateController.SMSTemplateBySuperAdmin());
	}*/
	
	@Secure(clients = "FormClient")
	@BodyParser.Of(BodyParser.Json.class)
	public Result postSMSTemplate(){
		
		 JsonNode json 	 				= request().body().asJson();
	     String msgapi 					= json.get("msgAPI").asText();
	     String msgTemp  				= json.get("messageTemplate").asText();
		logger.info("msgapi"+msgapi);
		logger.info("msgTemp"+msgTemp);

		SMSTemplate getLatestSMS 				=  SMSTemplateRepo.addNewSMSTemplate(msgTemp,msgapi,getUserLoginName(),getUserLogindealerCode());
		return ok(toJson(getLatestSMS));
	}
	
	@Secure(clients = "FormClient")
	public Result getCustomersListBySavedName(long savedsearchname){
		logger.info("inside tis");
		Map<String, String[]> paramMap = request().queryString();
		//Map<String, String[]> parambody = request().body().asFormUrlEncoded();
		//String savedsearchname = (parambody.get("savedsearchname")[0]);
		logger.info("saved search name inside controller:"+savedsearchname);
//		  Set<String> keys = paramMap.keySet();
//		  for (String key : keys) {
//		  String[] params = paramMap.get(key);
//		  for (String param : params) { logger.info("Key :" + key + " Value:" +
//		  param); } }
		ObjectNode result = Json.newObject();
		String searchPattern = "";
		boolean allflag = false;
		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
			logger.info(" searchPattern paramMap : " + searchPattern);
		}
		if (searchPattern.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		List<CustomerSearchResult> savedSearchResultsList = SMSTemplateRepo.getCustomerList(savedsearchname);
		long totalSize = savedSearchResultsList.size();
		logger.info("totalSize of customer : "+totalSize);
		if (toIndex < 0) {
			toIndex = 10;
		}
		if (toIndex > totalSize) {
			toIndex = totalSize;
		}
		long patternCount = 0;
		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);
		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");
		for (CustomerSearchResult c : savedSearchResultsList) {
			// String dispositionLink="<a href='/"+c.getCustomerName()+"'><i
			// class=\"fa fa-pencil-square\" data-toggel=\"tooltip\"
			// title=\"Disposition\"
			// style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			String selectIndex = "<input type='checkbox'/ class ='selectUser' id="+ c.getId()+" / name='searchname'/value=" + c.getId()+ " onclick='getSelectedUser();'>";
			ObjectNode row = Json.newObject();
//			row.put("0", c.getCid());
			row.put("0", c.getCustomerName());
			row.put("1", c.getVehicleRegNo());
			row.put("2", c.getCustomerCategory());
			row.put("3", c.getPhoneNumber());
			row.put("4", c.getNextServicedate());
			row.put("5", c.getModel());
			row.put("6", c.getVariant());
			row.put("7", c.getLastdisposition());
			row.put("8", selectIndex);
			an.add(row);
		}
		return ok(result);
		//logger.info("saved search name:"+ savedsearchname);
		//return ok(toJson(savedSearchResultsList));
	}
	@Secure(clients = "FormClient")
    @BodyParser.Of(BodyParser.Json.class)
	public Result postSMSBulk(){
		logger.info("inside postsmsbulk");
        JsonNode json 	 				= request().body().asJson();
        String savedNameString 			= json.get("savedName").asText();
        String setFlags  				= json.get("setFlag").asText();
        boolean setFlag  				= Boolean.parseBoolean(setFlags);
        logger.info("set flag:"+setFlags);
        List<JsonNode> searchResultList = json.findValues("searchResultIds");
        String smstemplateIds 			= json.get("smstemplateId").asText();
        long smstemplateId 				= Long.parseLong(smstemplateIds);
        long savedName 					= Long.parseLong(savedNameString);
        logger.info("saved name is::"+savedName);
        ObjectMapper objectMapper 		= new ObjectMapper();
        //loopg searchResultIds
        List<Long> searchResultIds = new ArrayList<Long>();
        for(JsonNode resultidNode : searchResultList){        		
        	resultidNode.forEach( node->{
        		logger.info("IndividualValue:"+node.asText());
        		searchResultIds.add(Long.parseLong(node.asText()));
        	});
        	//logger.info("search id result:"+resultId);
        	logger.info("search id result:"+resultidNode);
        	//long searchResultIds = Long.parseLong(searchResultIdSet);
        }
		Map<String,String> sendSMSList = SMSTemplateRepo.getMessagesForSMSBlast(getUserLoginName(),savedName, searchResultIds, smstemplateId, setFlag);
		return redirect(controllers.routes.SMSTemplateController.SMSTemplateBySuperAdmin());
	}
	
	
//	@Secure(clients = "FormClient")
//	public Result postSMSBulk(String savedName  ,List<Long> searchResultIds, long smstemplateId ,boolean setFlag){
//		
//		Map<String,String> sendSMSList = SMSTemplateRepo.getMessagesForSMSBlast(savedName, searchResultIds, smstemplateId, setFlag);
//
//		return redirect(controllers.routes.SMSTemplateController.SMSTemplateBySuperAdmin());
//
//	}
	
	
	@Secure(clients = "FormClient")
	public Result getSelectedUserListforDatatable(String selectedUsers, boolean setFlag ,long savedsearchname){
		logger.info("inside getSelectedUserListforDatatable:"+selectedUsers);
		Map<String, String[]> paramMap = request().queryString();
		//Map<String, String[]> parambody = request().body().asFormUrlEncoded();
		//String savedsearchname = (parambody.get("savedsearchname")[0]);
		logger.info("selected users name:"+selectedUsers);
//		  Set<String> keys = paramMap.keySet();
//		  
//		  for (String key : keys) {
//		  String[] params = paramMap.get(key);
//		  for (String param : params) { logger.info("Key :" + key + " Value:" +
//		  param); } }
		ObjectNode result = Json.newObject();
		String searchPattern = "";
		boolean allflag = false;
		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
			logger.info(" searchPattern paramMap : " + searchPattern);
		}
		if (searchPattern.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		List<Long> ListOfIds = new ArrayList<>();
		if(selectedUsers.equals("all")){
		}else{
			List<String> selectedusers = Arrays.asList(selectedUsers.split(","));
			for(String selectedList : selectedusers){
				long sa = Long.parseLong(selectedList);
				ListOfIds.add(sa);
			}
		}
		List<CustomerSearchResult> SelectedSearchResultsList = SMSTemplateRepo.getSelectedCustomerList(savedsearchname,ListOfIds,setFlag);
		long totalSize = SelectedSearchResultsList.size();
		logger.info("totalSize of customer : "+totalSize);
		if (toIndex < 0) {
			toIndex = 10;
		}
		if (toIndex > totalSize) {
			toIndex = totalSize;
		}
		long patternCount = 0;
		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);
		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");
		for (CustomerSearchResult c : SelectedSearchResultsList) {
			// String dispositionLink="<a href='/"+c.getCustomerName()+"'><i
			// class=\"fa fa-pencil-square\" data-toggel=\"tooltip\"
			// title=\"Disposition\"
			// style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			String selectIndex = "<input type='checkbox'/ class ='selectUser' id="+ c.getCid()+" / name='searchname'/value=" + c.getCid()+ " onclick='getSelectedUser();'>";
			ObjectNode row = Json.newObject();
			row.put("0", c.getCustomerName());
			row.put("1", c.getVehicleRegNo());
			row.put("2", c.getCustomerCategory());
			row.put("3", c.getPhoneNumber());
			row.put("4", c.getNextServicedate());
			row.put("5", c.getModel());
			row.put("6", c.getVariant());
			row.put("7", c.getLastdisposition());
			an.add(row);
		}
		return ok(result);
		//logger.info("saved search name:"+ savedsearchname);
		//return ok(toJson(savedSearchResultsList));
	}
}

