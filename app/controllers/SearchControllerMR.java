package controllers;

import static play.libs.Json.toJson;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import controllers.webmodels.CallLogAjaxLoadMR;
import controllers.webmodels.CallLogDispositionLoadMR;
import models.CallInteraction;
import models.WyzUser;
import play.Logger.ALogger;
import play.libs.Json;
import play.mvc.Result;
import repositories.AllCallInteractionRepository;
import repositories.CallInteractionsRepository;
import repositories.SearchRepository;
import repositories.WyzUserRepository;
//import repositories;

public class SearchControllerMR extends play.mvc.Controller {
	ALogger logger = play.Logger.of("application");
	private SearchRepository searchRepo;
	private final WyzUserRepository userRepository;
	private final CallInteractionsRepository call_int_repo;
	private final AllCallInteractionRepository all_int_repo;
	private PlaySessionStore playSessionStore;

	@Inject
	public SearchControllerMR(SearchRepository repository, WyzUserRepository userRepo,
			CallInteractionsRepository interRepo, PlaySessionStore playSession,
			AllCallInteractionRepository allInterRepo) {
		searchRepo = repository;
		userRepository = userRepo;
		call_int_repo = interRepo;
		playSessionStore = playSession;
		all_int_repo = allInterRepo;
	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();
		return userloginname;
	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");
		return dealerName;
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");
		return dealer_Code;
	}

	// SERVICE REMAINDER LIST DATA
	@Secure(clients = "FormClient")
	public Result assignedInteractionDataMR(String CREIds) {
		logger.info("Forecast controller");
		if (CREIds.equals("Select")) {
			CREIds = "";
		}
		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();
		String campaignName = "";
		String fromDateNew = "";
		String toDateNew = "";
		String serviceTypeName = "";
		String serviceBookedType = "";
		String creidsare = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			campaignName = paramMap.get("columns[0][search][value]")[0];
			if (campaignName.equals("0")) {
				campaignName = "";
			}
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDateNew = paramMap.get("columns[2][search][value]")[0];
		}

		if (paramMap.get("columns[3][search][value]") != null) {
			serviceTypeName = paramMap.get("columns[3][search][value]")[0];
			if (serviceTypeName.equals("0")) {
				serviceTypeName = "";
			}
		}

		if (paramMap.get("columns[4][search][value]") != null) {
			serviceBookedType = paramMap.get("columns[4][search][value]")[0];
			if (serviceBookedType.equals("0")) {
				serviceBookedType = "";
			}
		}

		if (paramMap.get("columns[7][search][value]") != null) {
			creidsare = paramMap.get("columns[7][search][value]")[0];
			if (creidsare.equals("Select")) {
				creidsare = "";
			}
		}

		logger.info("campaign Name: " + campaignName);
		logger.info("SearchDate From: " + fromDateNew);
		logger.info("SearchDate To: " + toDateNew);
		logger.info("serviceTypeName: " + serviceTypeName);
		logger.info("serviceBookedType: " + serviceBookedType);
		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && serviceTypeName.length() == 0 && serviceBookedType.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}
		logger.info("searchPattern :" + searchPattern);

		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		long totalSize = 0;
		String storedProcedureFun = "cremanagerServiceReminderCount";
		totalSize = searchRepo.methodToFindTotalSizeParam3(creidsare, totalSize, storedProcedureFun,
				getUserLoginName());

		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;
		}
		if (toIndex > totalSize) {
			toIndex = totalSize;
		}
		long patternCount = 0;
		List<CallLogAjaxLoadMR> assignedList = searchRepo.assignedListOfUserMR(fromDateNew, toDateNew, campaignName,
				serviceTypeName, searchPattern, getUserLoginName(), creidsare, fromIndex, toIndex);
		if (!allflag) {
			if (globalSearch)
				//patternCount = totalSize;
				patternCount = searchRepo.assignedListOfUserMR(fromDateNew, toDateNew, campaignName, serviceTypeName,
						searchPattern, getUserLoginName(), creidsare, 0, totalSize).size();

			else
				patternCount = searchRepo.assignedListOfUserMR(fromDateNew, toDateNew, campaignName, serviceTypeName,
						searchPattern, getUserLoginName(), creidsare, 0, totalSize).size();

		}
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

		logger.info("allFlag : " + allflag + " global : " + globalSearch);

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);
		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");
		for (CallLogAjaxLoadMR c : assignedList) {
			logger.info("complaint cunt" +c.getComplaints_count());
			logger.info("username cunt" +c.getUserName());
			
			ObjectNode row = Json.newObject();
			row.put("0", c.getCustomer_name());
			row.put("1", c.getVehicle_RegNo());
			row.put("2", c.getVeh_model());
			row.put("3", c.getCategory());
			row.put("4", c.getLoyalty());
			row.put("5", c.getDuedate());
			row.put("6", c.getDue_type());
			row.put("7", c.getForecast_type());
			row.put("8", c.getLast_psfstatus());
			row.put("9", c.getDND_status());
			row.put("10", c.getComplaints_count());
			row.put("11", c.getUserName());
			an.add(row);
		}
		return ok(result);
	}

	// FOLLOWUP REQUIRED DATA
	@Secure(clients = "FormClient")
	public Result followUpRequiredInteractionDataMR(String CREIds) {
		
		logger.info("follow up controller");

		if (CREIds.equals("Select")) {
			logger.info("CREIdsIF :" + CREIds);
			CREIds = "";
		}

		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();
		String campaignName = "";
		String fromDateNew = "";
		String toDateNew = "";
		String serviceTypeName = "";
		String serviceBookedType = "";
		String creidsare = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			campaignName = paramMap.get("columns[0][search][value]")[0];
			if (campaignName.equals("0")) {
				campaignName = "";
			}
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDateNew = paramMap.get("columns[2][search][value]")[0];
		}

		if (paramMap.get("columns[3][search][value]") != null) {
			serviceTypeName = paramMap.get("columns[3][search][value]")[0];
			if (serviceTypeName.equals("0")) {
				serviceTypeName = "";
			}
		}

		if (paramMap.get("columns[4][search][value]") != null) {
			serviceBookedType = paramMap.get("columns[4][search][value]")[0];
			if (serviceBookedType.equals("0")) {
				serviceBookedType = "";
			}
		}
		if (paramMap.get("columns[7][search][value]") != null) {
			creidsare = paramMap.get("columns[7][search][value]")[0];
			if (creidsare.equals("Select")) {
				creidsare = "";
			}
		}

		logger.info("campaign Name: " + campaignName);
		logger.info("SearchDate From: " + fromDateNew);
		logger.info("SearchDate To: " + toDateNew);
		logger.info("serviceTypeName: " + serviceTypeName);
		logger.info("serviceBookedType: " + serviceBookedType);
		logger.info("creidsare: " + creidsare);
		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;
		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}
		if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && serviceTypeName.length() == 0 && serviceBookedType.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		long totalSize = 0;
		logger.info("CREIds : " + creidsare);
		logger.info("totalSizepro : " + totalSize);

		String storedProcedureFun = "cremanagerdispositionfilterCount";

		logger.info("storedProcedureFun : " + storedProcedureFun);
		totalSize = searchRepo.methodToFindTotalSizeParam4(creidsare, totalSize, storedProcedureFun, 4,
				getUserLoginName());

		logger.info("totalSize : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		long typeOfDispo = 4;
		int orderDirection = 1;
		List<CallLogDispositionLoadMR> followUpList = searchRepo.getDispositionListOfUserMR(fromDateNew, toDateNew,
				campaignName, serviceTypeName, searchPattern, getUserLoginName(), creidsare, typeOfDispo, fromIndex,
				toIndex, orderDirection);

		if (!allflag) {
			if (globalSearch)
				//patternCount = totalSize;
				patternCount = searchRepo
				.getDispositionListOfUserMR(fromDateNew, toDateNew, campaignName, serviceTypeName,
						searchPattern, getUserLoginName(), creidsare, typeOfDispo, 0, totalSize, orderDirection)
				.size();
			else
				patternCount = searchRepo
						.getDispositionListOfUserMR(fromDateNew, toDateNew, campaignName, serviceTypeName,
								searchPattern, getUserLoginName(), creidsare, typeOfDispo, 0, totalSize, orderDirection)
						.size();

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogDispositionLoadMR c : followUpList) {

			// String urlDisposition = "<a href='/" + getUserLogindealerCode() +
			// "/CRE/getFollowUpCallDispositionPage/"
			// + c.getCallinteraction_id()
			// + "'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\"
			// title=\"Disposition\"
			// style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			// String urlEdit = "<a href='/" + getUserLogindealerCode() +
			// "/CRE/toeditcallLog/" + c.getCallinteraction_id()
			// + "'><i class=\"fa fa-pencil-square-o\" data-toggle=\"tooltip\"
			// title=\"edit\"></i></a>";
			String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";

			// logger.info(urlDisposition);
			/*String downloadMediaMR = "<a href='/CREManager/downloadMediaFile/" + c.getCallinteraction_id()
					+ "'><i class='fa fa-play' data-toggle='tooltip' title='audio'></i></a>";
			*/
			//String downloadMediaMR="<audio controls><source src='http://localhost:9000/CRE/audiostream/"+c.getCallinteraction_id()+"' type='audio/mp3'></audio>";				
			
			
			//logger.info(downloadMediaMR);
			String stringDate=getStringDate(c.getCall_date());			

			ObjectNode row = Json.newObject();
			row.put("0", c.getCampaignName());
			row.put("1", stringDate);
			row.put("2", c.getCustomer_name());
			row.put("3", c.getMobile_number());
			row.put("4", c.getVehicle_RegNo());
			row.put("5", c.getDuedate());
			row.put("6", c.getUserName());
			row.put("7", c.getFollowUpDate());
			row.put("8", c.getFollowUpTime());
			row.put("9", disposition);
			row.put("10",c.getIsCallinitaited());
			/*if (c.getMediaFileLob().equals("available")) {
				row.put("11", downloadMediaMR);
			} else {
				row.put("11", "");
			}*/

			an.add(row);
		}

		return ok(result);
	}

	// SERVICE BOOKED DATA
	@Secure(clients = "FormClient")
	public Result serviceBookedInteractionDataMR(String CREIds) {
		logger.info("service booked controller");
		
		if (CREIds.equals("Select")) {
			CREIds = "";
		}
		logger.info(" CREIds manager : " + CREIds);
		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();
		String campaignName = "";
		String fromDateNew = "";
		String toDateNew = "";
		String serviceTypeName = "";
		String serviceBookedType = "";
		String creidsare = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			campaignName = paramMap.get("columns[0][search][value]")[0];
			if (campaignName.equals("0")) {
				campaignName = "";
			}
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDateNew = paramMap.get("columns[2][search][value]")[0];
		}

		if (paramMap.get("columns[3][search][value]") != null) {
			serviceTypeName = paramMap.get("columns[3][search][value]")[0];
			if (serviceTypeName.equals("0")) {
				serviceTypeName = "";
			}
		}

		if (paramMap.get("columns[4][search][value]") != null) {
			serviceBookedType = paramMap.get("columns[4][search][value]")[0];
			if (serviceBookedType.equals("0")) {
				serviceBookedType = "";
			}
		}
		if (paramMap.get("columns[7][search][value]") != null) {
			creidsare = paramMap.get("columns[7][search][value]")[0];
			if (creidsare.equals("Select")) {
				creidsare = "";
			}
		}

		logger.info("campaign Name: " + campaignName);
		logger.info("SearchDate From: " + fromDateNew);
		logger.info("SearchDate To: " + toDateNew);
		logger.info("serviceTypeName: " + serviceTypeName);
		logger.info("serviceBookedType: " + serviceBookedType);
		logger.info("creidsare  : " + creidsare);

		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && serviceTypeName.length() == 0 && serviceBookedType.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}

		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		long totalSize = 0;

		String storedProcedureFun = "cremanagerdispositionfilterCount";
		totalSize = searchRepo.methodToFindTotalSizeParam4(creidsare, totalSize, storedProcedureFun, 3,
				getUserLoginName());
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		long typeOfDispo = 3;
		int orderDirection = 1;
		List<CallLogDispositionLoadMR> serviceBookedList = searchRepo.getDispositionListOfUserMR(fromDateNew, toDateNew,
				campaignName, serviceBookedType, searchPattern, getUserLoginName(), creidsare, typeOfDispo, fromIndex,
				toIndex, orderDirection);

		if (!allflag) {
			if (globalSearch)
				//patternCount = totalSize;
				patternCount = searchRepo
				.getDispositionListOfUserMR(fromDateNew, toDateNew, campaignName, serviceBookedType,
						searchPattern, getUserLoginName(), creidsare, typeOfDispo, 0, totalSize, orderDirection)
				.size();
			else
				patternCount = searchRepo
						.getDispositionListOfUserMR(fromDateNew, toDateNew, campaignName, serviceBookedType,
								searchPattern, getUserLoginName(), creidsare, typeOfDispo, 0, totalSize, orderDirection)
						.size();

		}
		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");
		for (CallLogDispositionLoadMR c : serviceBookedList) {
			String disposition = "<span class=\"label label-primary\">" + c.getReason()+ "</span>";
			//logger.info(disposition);
			/*String downloadMediaMR = "<a href='/CREManager/downloadMediaFile/" + c.getCallinteraction_id()
					+ "'><i class='fa fa-play' data-toggle='tooltip' title='audio'></i></a>";
			*/
			//String downloadMediaMR="<audio controls><source src='http://localhost:9000/CRE/audiostream/"+c.getCallinteraction_id()+"' type='audio/mp3'></audio>";				
			
			String stringDate=getStringDate(c.getCall_date());			


			ObjectNode row = Json.newObject();
			row.put("0", c.getCampaignName());
			row.put("1", stringDate);
			row.put("2", c.getCustomer_name());
			row.put("3", c.getMobile_number());
			row.put("4", c.getVehicle_RegNo());
			row.put("5", c.getDuedate());
			row.put("6", c.getUserName());
			row.put("7", c.getScheduled_date());
			row.put("8", c.getScheduled_time());
			row.put("9", disposition);
			row.put("10", c.getIsCallinitaited());
			/*if (c.getMediaFileLob().equals("available")) {
				row.put("11", downloadMediaMR);
			} else {
				row.put("11", "");
			}*/
			an.add(row);
		}
		return ok(result);
	}

	// SERVICE NOT REQUIRED DATA
	@Secure(clients = "FormClient")
	public Result serviceNotRequiredInteractionDataMR(String CREIds) {
		logger.info("service not required controller");
		if (CREIds.equals("Select")) {
			CREIds = "";
		}
		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String campaignName = "";
		String fromDateNew = "";
		String toDateNew = "";
		String serviceTypeName = "";
		String creidsare = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			campaignName = paramMap.get("columns[0][search][value]")[0];
			if (campaignName.equals("0")) {
				campaignName = "";
			}
		}

		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && campaignName.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (paramMap.get("columns[7][search][value]") != null) {
			creidsare = paramMap.get("columns[7][search][value]")[0];
			if (creidsare.equals("Select")) {
				creidsare = "";
			}
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}

		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = 0;
		String storedProcedureFun = "cremanagerdispositionfilterCount";
		totalSize = searchRepo.methodToFindTotalSizeParam4(creidsare, totalSize, storedProcedureFun, 5,
				getUserLoginName());
		// logger.info("totalSize of assigned interaction : "+totalSize);

		if (toIndex < 0) {
			toIndex = 10;
		}

		if (toIndex > totalSize) {
			toIndex = totalSize;
		}
		long patternCount = 0;
		long typeOfDispo = 5;
		int orderDirection = 1;

		List<CallLogDispositionLoadMR> serviceNotRequiredList = searchRepo.getDispositionListOfUserMR(fromDateNew,
				toDateNew, campaignName, serviceTypeName, searchPattern, getUserLoginName(), creidsare, typeOfDispo,
				fromIndex, toIndex, orderDirection);

		if (!allflag) {
			if (globalSearch)
				//patternCount = totalSize;
				patternCount = searchRepo
				.getDispositionListOfUserMR(fromDateNew, toDateNew, campaignName, serviceTypeName,
						searchPattern, getUserLoginName(), creidsare, typeOfDispo, 0, totalSize, orderDirection)
				.size();
			else
				patternCount = searchRepo
						.getDispositionListOfUserMR(fromDateNew, toDateNew, campaignName, serviceTypeName,
								searchPattern, getUserLoginName(), creidsare, typeOfDispo, 0, totalSize, orderDirection)
						.size();

		}

		// logger.info("subList Size:" + serviceNotRequiredList.size());
		// long sublistSize = customerList.size();
		// logger.info("fromIndex:" + fromIndex);
		// logger.info("toIndex:" + toIndex);
		// logger.info("Search Pattern" + searchPattern);

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);
		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");
		for (CallLogDispositionLoadMR c : serviceNotRequiredList) {
			String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";
			/*String downloadMediaMR = "<a href='/CREManager/downloadMediaFile/" + c.getCallinteraction_id()
					+ "'><i class='fa fa-play' data-toggle='tooltip' title='audio'></i></a>";
*/
			//String downloadMediaMR="<audio controls><source src='http://localhost:9000/CRE/audiostream/"+c.getCallinteraction_id()+"' type='audio/mp3'></audio>";				
			
			// String callid=c.getCallinteraction_id();
			// String callid=String.valueOf(128);

			// String downloadMediaMR ="<span onclick='getAudioPLay('\'"
			// +callid+ "\'');'><i class='fa fa-play' data-toggle='tooltip'
			// title='audio'></i></span><audio id='" +callid+"'
			// controls='controls' src=''></audio>";

			// String downloadMediaMR="<span onclick='getAudioPLay(" + callid +
			// ");'><i class='fa fa-play' data-toggle='tooltip'
			// title='audio'></i></span><audio id='" +callid+"'
			// controls='controls' type='audio/mp3' ></audio>";

			String stringDate=getStringDate(c.getCall_date());			

			
			ObjectNode row = Json.newObject();
			row.put("0", c.getCampaignName());
			row.put("1", stringDate);
			row.put("2", c.getCustomer_name());
			row.put("3", c.getMobile_number());
			row.put("4", c.getVehicle_RegNo());
			row.put("5", c.getDuedate());
			row.put("6", c.getUserName());
			row.put("7", disposition);
			row.put("8", c.getReason());
			// row.put("7",downloadMediaMR);
			/*if (c.getMediaFileLob().equals("available")) {
				row.put("9", downloadMediaMR);
			} else {
				row.put("9", "");
			}*/
			an.add(row);
		}
		return ok(result);
	}

	// NON CONTACT DATA
	@Secure(clients = "FormClient")
	public Result nonContactsInteractionDataMR(String CREIds) {
		logger.info("non contacts controller");
		if (CREIds.equals("Select")) {
			CREIds = "";
		}
		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();
		String campaignName = "";
		String fromDateNew = "";
		String toDateNew = "";
		String lastDipo = "";
		String droppedCount = "";
		String creidsare = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			campaignName = paramMap.get("columns[0][search][value]")[0];
			if (campaignName.equals("0")) {

				campaignName = "";
			}
		}
		
		logger.info(paramMap.get("columns[1][search][value]").toString());
		if (paramMap.get("columns[1][search][value]") != null) {
			fromDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDateNew = paramMap.get("columns[2][search][value]")[0];
		}

		if (paramMap.get("columns[5][search][value]") != null) {
			lastDipo = paramMap.get("columns[5][search][value]")[0];
			if (lastDipo.equals("0")) {

				lastDipo = "";
			}
		}

		if (paramMap.get("columns[6][search][value]") != null) {
			droppedCount = paramMap.get("columns[6][search][value]")[0];
			if (droppedCount.equals("0")) {

				droppedCount = "";
			}
		}
		if (paramMap.get("columns[7][search][value]") != null) {
			creidsare = paramMap.get("columns[7][search][value]")[0];
			if (creidsare.equals("Select")) {
				creidsare = "";
			}
		}

		logger.info("campaign Name: " + campaignName);
		logger.info("SearchDate From: " + fromDateNew);
		logger.info("SearchDate To: " + toDateNew);
		logger.info("lastDipo: " + lastDipo);
		logger.info("droppedCount: " + droppedCount);
		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;
		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}
		if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && lastDipo.length() == 0 && droppedCount.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		long totalSize = 0;
		String storedProcedureFun = "cremanagerNonContactsSearchCount";
		totalSize = searchRepo.methodToFindTotalSizeParam3(creidsare, totalSize, storedProcedureFun,
				getUserLoginName());
		logger.info("totalSize of noncontacts interaction : "+totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {
			toIndex = totalSize;
		}
		long patternCount = 0;
		List<CallLogDispositionLoadMR> nonContactsList = searchRepo.getnonContactsListOfUserMR(fromDateNew, toDateNew,
				campaignName, lastDipo, droppedCount, searchPattern, getUserLoginName(), creidsare, fromIndex, toIndex);

		if (!allflag) {
			if (globalSearch)
				//patternCount = totalSize;
				patternCount = searchRepo.getnonContactsListOfUserMR(fromDateNew, toDateNew, campaignName, lastDipo,
						droppedCount, searchPattern, getUserLoginName(), creidsare, fromIndex, toIndex).size();

			else
				patternCount = searchRepo.getnonContactsListOfUserMR(fromDateNew, toDateNew, campaignName, lastDipo,
						droppedCount, searchPattern, getUserLoginName(), creidsare, fromIndex, toIndex).size();

		}

		// logger.info("nonContactsList : "+nonContactsList.size());

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogDispositionLoadMR c : nonContactsList) {

			String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";
			//String downloadMediaMR="<audio controls><source src='http://localhost:9000/CRE/audiostream/"+c.getCallinteraction_id()+"' type='audio/mp3'></audio>";

			// logger.info(urlDisposition);
			
			String stringDate=getStringDate(c.getCall_date());			


			ObjectNode row = Json.newObject();
			row.put("0", c.getCampaignName());
			row.put("1", stringDate);
			row.put("2", c.getCustomer_name());
			row.put("3", c.getMobile_number());
			row.put("4", c.getVehicle_RegNo());
			row.put("5", c.getDuedate());
			row.put("6", c.getUserName());
			row.put("7", disposition);
			row.put("8" , c.getIsCallinitaited());
			/*if (c.getMediaFileLob().equals("available")) {
				row.put("9", downloadMediaMR);
			} else {
				row.put("9", "");
			}*/
			an.add(row);
		}

		return ok(result);

	}

	// DROPPED LIST DATA
	@Secure(clients = "FormClient")
	public Result droppedCallInteractionDataMR(String CREIds) {

		logger.info("dropped contacts controller");
		if (CREIds.equals("Select")) {
			CREIds = "";
		}
		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}

		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = 0;
		String storedProcedureFun = "cremanagerdroppedSearchCount";
		totalSize = searchRepo.methodToFindTotalSizeParam3(CREIds, totalSize, storedProcedureFun, getUserLoginName());

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		List<CallLogDispositionLoadMR> droppedDataList = searchRepo.getDroppedDataListMR(searchPattern,
				getUserLoginName(), CREIds, fromIndex, toIndex);

		if (!allflag) {
			patternCount = droppedDataList.size();
			// logger.info("patternCount of droppedDataList : "+patternCount);

		}

		// logger.info("DroppedData : "+droppedDataList.size());

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogDispositionLoadMR c : droppedDataList) {

			String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";

			// logger.info(urlDisposition);
			String stringDate=getStringDate(c.getCall_date());			


			ObjectNode row = Json.newObject();
			row.put("0", c.getCampaignName());
			row.put("1", stringDate);
			row.put("2", c.getCustomer_name());
			row.put("3", String.valueOf(c.getCall_date()));
			row.put("4", c.getMobile_number());
			row.put("5", c.getVehicle_RegNo());
			row.put("6", c.getDuedate());
			row.put("7", c.getUserName());
			row.put("8", c.getIsCallinitaited());
			row.put("9", disposition);

			an.add(row);
		}

		return ok(result);

	}
	
		// missed call LIST DATA
		@Secure(clients = "FormClient")
		public Result missedCallInteractionDataMR(String CREIds) {
			logger.info("missed contacts controller");
			if (CREIds.equals("Select")) {
				CREIds = "";
			}
			WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

			Map<String, String[]> paramMap = request().queryString();
			ObjectNode result = Json.newObject();

			String searchPattern = "";
			String callType = "MISSED";
			String toCallDate = "";
			String fromCallDate = "";
			boolean allflag = false;

			if (paramMap.get("search[value]") != null) {
				searchPattern = paramMap.get("search[value]")[0];
			}

			if (searchPattern.length() == 0) {
				allflag = true;
			}
			
			if (paramMap.get("columns[1][search][value]") != null) {
				toCallDate = paramMap.get("columns[1][search][value]")[0];
			}

			if (paramMap.get("columns[2][search][value]") != null) {
				fromCallDate = paramMap.get("columns[2][search][value]")[0];
			}

			long fromIndex = Long.valueOf(paramMap.get("start")[0]);
			long toIndex = Long.valueOf(paramMap.get("length")[0]);

			long totalSize = 0;
			//String storedProcedureFun = "callogMissedIncomingcount";
			totalSize = searchRepo.getCountForMissedIncominOutgoing(getUserLoginName(),callType,CREIds,toCallDate,fromCallDate,searchPattern);

			if (toIndex < 0) {
				toIndex = 10;

			}

			if (toIndex > totalSize) {

				toIndex = totalSize;
			}
			long patternCount = 0;
			List<CallLogDispositionLoadMR> missedDataList = searchRepo.getDataMissedIncominOutgoing(getUserLoginName(),callType,CREIds,fromCallDate,toCallDate,searchPattern,fromIndex,toIndex);

			if (!allflag) {
				patternCount = missedDataList.size();
				// logger.info("patternCount of droppedDataList : "+patternCount);
			}

			// logger.info("DroppedData : "+droppedDataList.size());

			result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
			result.put("recordsTotal", totalSize);

			if (allflag) {
				result.put("recordsFiltered", totalSize);
			} else {
				result.put("recordsFiltered", patternCount);
			}
			ArrayNode an = result.putArray("data");

			for (CallLogDispositionLoadMR c : missedDataList) {

				String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";
				//String downloadMediaMR="<audio controls><source src='http://localhost:9000/CRE/audiostream/"+c.getCallinteraction_id()+"' type='audio/mp3'></audio>";				

				// logger.info(urlDisposition);
				String stringDate=getStringDate(c.getCall_date());			


				ObjectNode row = Json.newObject();
				row.put("0", String.valueOf(c.getCall_date()));
				row.put("1", c.getCustomer_name());
				row.put("2", c.getVehicle_RegNo());				
				row.put("3", c.getDialedNoIs());
				row.put("4", c.getUserName());

				/*if (c.getMedia_file().equals("Available")) {
				row.put("5", downloadMediaMR);
				} else {
					row.put("5", "");
				}*/

				an.add(row);
			}

			return ok(result);

		}
	
				// incoming call LIST DATA
				@Secure(clients = "FormClient")
				public Result incomingCallInteractionDataMR(String CREIds) {

					logger.info("incoming contacts controller");
					if (CREIds.equals("Select")) {
						CREIds = "";
					}
					WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

					Map<String, String[]> paramMap = request().queryString();
					ObjectNode result = Json.newObject();

					String searchPattern = "";
					String callType = "INCOMING";

					String toCallDate ="";
					String fromCallDate ="";
					boolean allflag = false;

					if (paramMap.get("search[value]") != null) {
						searchPattern = paramMap.get("search[value]")[0];
					}

					if (searchPattern.length() == 0) {
						allflag = true;
					}
					if (paramMap.get("columns[1][search][value]") != null) {
						toCallDate = paramMap.get("columns[1][search][value]")[0];
					}

					if (paramMap.get("columns[2][search][value]") != null) {
						fromCallDate = paramMap.get("columns[2][search][value]")[0];
					}

					long fromIndex = Long.valueOf(paramMap.get("start")[0]);
					long toIndex = Long.valueOf(paramMap.get("length")[0]);

					long totalSize = 0;
					//String storedProcedureFun = "cremanagerdroppedSearchCount";
					totalSize = searchRepo.getCountForMissedIncominOutgoing(getUserLoginName(),callType,CREIds,toCallDate,fromCallDate,searchPattern);

					if (toIndex < 0) {
						toIndex = 10;

					}

					if (toIndex > totalSize) {

						toIndex = totalSize;
					}
					long patternCount = 0;

					List<CallLogDispositionLoadMR> incomingDataList = searchRepo.getDataMissedIncominOutgoing(getUserLoginName(),callType,CREIds,fromCallDate,toCallDate,searchPattern,fromIndex,toIndex);


					if (!allflag) {
						patternCount = incomingDataList.size();
						// logger.info("patternCount of droppedDataList : "+patternCount);

					}

					// logger.info("DroppedData : "+droppedDataList.size());

					result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
					result.put("recordsTotal", totalSize);

					if (allflag) {
						result.put("recordsFiltered", totalSize);
					} else {
						result.put("recordsFiltered", patternCount);
					}
					ArrayNode an = result.putArray("data");

					for (CallLogDispositionLoadMR c : incomingDataList) {

						String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";
						//String downloadMediaMR="<audio controls><source src='http://localhost:9000/CRE/audiostream/"+c.getCallinteraction_id()+"' type='audio/mp3'></audio>";				

						// logger.info(urlDisposition);
						String stringDate=getStringDate(c.getCall_date());			


						ObjectNode row = Json.newObject();
						row.put("0", String.valueOf(c.getCall_date()));
						row.put("1", c.getCustomer_name());
						row.put("2", c.getVehicle_RegNo());						
						row.put("3", c.getDialedNoIs());
						row.put("4", c.getUserName());
						/*if (c.getMedia_file().equals("Available")) {
							row.put("5", downloadMediaMR);
						} else {
							row.put("5", "");
						}*/

						an.add(row);
					}

					return ok(result);

				}
				
				
			
			// outgoing call LIST DATA 
				@Secure(clients = "FormClient")
				public Result outgoingCallsServerDataTableMR(String CREIds) {

					logger.info("dropped contacts controller");
					if (CREIds.equals("Select")) {
						CREIds = "";
					}
					WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

					Map<String, String[]> paramMap = request().queryString();
					ObjectNode result = Json.newObject();

					String searchPattern = "";
					String callType = "OUTGOING";
					String toCallDate    = "";
					String fromCallDate  = "";
					boolean allflag = false;

					if (paramMap.get("search[value]") != null) {
						searchPattern = paramMap.get("search[value]")[0];
					}

					if (searchPattern.length() == 0) {
						allflag = true;
					}
					if (paramMap.get("columns[1][search][value]") != null) {
						toCallDate = paramMap.get("columns[1][search][value]")[0];
					}

					if (paramMap.get("columns[2][search][value]") != null) {
						fromCallDate = paramMap.get("columns[2][search][value]")[0];
					}

					long fromIndex = Long.valueOf(paramMap.get("start")[0]);
					long toIndex = Long.valueOf(paramMap.get("length")[0]);

					long totalSize = 0;
					//String storedProcedureFun = "cremanagerdroppedSearchCount";
					totalSize = searchRepo.getCountForMissedIncominOutgoing(getUserLoginName(),callType,CREIds,toCallDate,fromCallDate,searchPattern);

					if (toIndex < 0) {
						toIndex = 10;

					}

					if (toIndex > totalSize) {

						toIndex = totalSize;
					}
					long patternCount = 0;

					List<CallLogDispositionLoadMR> outgoingDataList = searchRepo.getDataMissedIncominOutgoing(getUserLoginName(),callType,CREIds,fromCallDate,toCallDate,searchPattern,fromIndex,toIndex);


					if (!allflag) {
						patternCount = outgoingDataList.size();
						// logger.info("patternCount of droppedDataList : "+patternCount);

					}

					// logger.info("DroppedData : "+droppedDataList.size());

					result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
					result.put("recordsTotal", totalSize);

					if (allflag) {
						result.put("recordsFiltered", totalSize);
					} else {
						result.put("recordsFiltered", patternCount);
					}
					ArrayNode an = result.putArray("data");

					for (CallLogDispositionLoadMR c : outgoingDataList) {

						String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";
						//String downloadMediaMR="<audio controls><source src='http://localhost:9000/CRE/audiostream/"+c.getCallinteraction_id()+"' type='audio/mp3'></audio>";				

						// logger.info(urlDisposition);
						String stringDate=getStringDate(c.getCall_date());			

						ObjectNode row = Json.newObject();
						row.put("0", String.valueOf(c.getCall_date()));
						row.put("1", c.getCustomer_name());
						row.put("2", c.getVehicle_RegNo());						
						row.put("3", c.getDialedNoIs());
						row.put("4", c.getUserName());
						/*if (c.getMedia_file().equals("Available")) {
						row.put("5", downloadMediaMR);
						} else {
							row.put("5", "");
						}*/
						
						an.add(row);
					}

					return ok(result);

				}
	
				
		private String getStringDate(Date call_date) {
		// TODO Auto-generated method stub
		if (call_date != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate= formatter.format(call_date);
			return stringDate;
			
		}else{
			
		}
		return "";
	}
	
	@Secure(clients = "FormClient")
	public Result getMediaFileMR(long callInteractionId) throws IOException {

		CallInteraction callInte = call_int_repo.getCallInteractionById(callInteractionId);

		String nameMedia = callInte.getVehicle().getVehicleRegNo()+"_"+callInte.getWyzUser().getUserName() + "_" + callInte.getCallDate() + "_"
				+ callInte.getCallTime();

		byte[] bytes = all_int_repo.getMediaFileMR(callInteractionId);

		int size = bytes.length;
		response().setContentType("audio/mp3");
		response().setHeader("Content-Disposition", "attachment;filename=" + nameMedia + ".mp3");
		response().setHeader("Cache-Control", "no-cache");
		return ok(bytes);
	}

	/*@Secure(clients = "FormClient")
	public Result getMediaFileMR(long callInteractionId) throws IOException {

		CallInteraction callInte = call_int_repo.getCallInteractionById(callInteractionId);
		
		String nameMedia = callInte.getVehicle().getVehicleRegNo()+"_"+callInte.getWyzUser().getUserName() + "_" + callInte.getCallDate() + "_"
				+ callInte.getCallTime();
		//String fileName = nameMedia +".mp3";
		byte[] bytes = all_int_repo.getMediaFileMR(callInteractionId);
		File audFile = new File("C:/ffmpeg/aud.3gp");
		String absolutePath = audFile.getAbsolutePath();
		logger.info("absolutePath " + absolutePath);
		//int size = bytes.length;
		byte[] ConvertedFileBytes = null;
		
		
		BufferedOutputStream bs = null;
		FileOutputStream fs = null;

		try {

				 fs = new FileOutputStream(audFile);
				bs = new BufferedOutputStream(fs);
				bs.write(bytes);				
				bs.close();
				bs = null;

			} catch (Exception e) {
				e.printStackTrace();
			}

		if (bs != null) try { bs.close(); } catch (Exception e) {}
		String path = "C:/ffmpeg/";
		
		try
		{
			URL url = getClass().getResource(absolutePath);
			File file = null;
			
			logger.info("url " + url);
			try{
			file = new File(absolutePath);
			logger.info("Fileurl " + absolutePath);
			if(file.exists())
			{
				logger.info("File existed");
				// String infoFile = nameMedia+".mp3";
				// logger.info("infoFile " + infoFile);
				// String command = "ffmpeg -i \""+ file +"\" >"+infoFile+" 2>&1";
				//logger.info("command " + command);
				
				Process p = Runtime.getRuntime().exec("cmd /c ffmpeg -i C:/ffmpeg/aud.3gp C:/ffmpeg/nameMedia.mp3");
				InputStream in = p.getErrorStream();
				int c;
				
				while ((c = in.read()) != -1)
				{
					System.out.print((char)c);
				}
				in.close();
				try
				{
					ConvertedFileBytes = Files.readAllBytes(new File("C:/ffmpeg/nameMedia.mp3").toPath());	
					logger.info("File converted to bytes");
				}
				catch(Exception e)
				{
					logger.info("BytesException" + e);
				}					
				logger.info("File converted");
				response().setContentType("audio/mp3");
				response().setHeader("Content-Disposition", "attachment;filename=" + path+"/nameMedia.mp3");
				response().setHeader("Cache-Control", "no-cache");
				logger.info("File downloaded");
			}
			else
			{
				logger.info("File not found!");
			}
			}
			catch(IOException io)
			{
				logger.info("ExceptionIO " + io);
			}
		}catch(Exception fe)
		{
			logger.info("ExceptionMain " + fe);
		}


		return ok(ConvertedFileBytes);
		//return null; 
	}*/

	// Dashboard count of CRE Manager
	@Secure(clients = "FormClient")
	public Result getDashboardCount() {

		//double countOfconversion = searchRepo.getConversionRateMAN(getUserLoginName(), "conversion_rate_overall");
		long countCREMFreshCalls = searchRepo.getPendingAndScheduledcountMan(getUserLoginName(),"CreMtotalFreshCalls");
		long countCREMFollowups = searchRepo.getPendingAndScheduledcountMan(getUserLoginName(),"CreMTotalfollowups");
		long countCREMNonContacts = searchRepo.getPendingAndScheduledcountMan(getUserLoginName(),"CreMTotalNoncontacts");
		long countCREMoverDueBookings = searchRepo.getPendingAndScheduledcountMan(getUserLoginName(),"CreMTotaloverDueBookings");
		long countCREMtotalCalls = searchRepo.getPendingAndScheduledcountMan(getUserLoginName(),"CreMTotalCallstoday");
		long countCREMbookings = searchRepo.getPendingAndScheduledcountMan(getUserLoginName(),"CreMTotalBookings");
		//long servicebookedcount = 0;
		//String storedProcedureFun = "cremanagerdispositionfilterCount";
		// servicebookedcount =
		// methodToFindTotalSizeParam4("",servicebookedcount,storedProcedureFun,3);
		//servicebookedcount = searchRepo.serviceBookDashboardCountManager(getUserLoginName());

		//long schcount = searchRepo.getPendingAndScheduledcountMan(getUserLoginName(), "scheduledcalls_count_overall");
		// String storedProcedureFun1 = "cremanagerServiceReminderCount(?,?,?)";

		// methodToFindTotalSizeParam4(String CREIds, long totalSize,String
		// storedProcedureFun,long typeDec)
		// schcount = methodToFindTotalSizeParam3("",
		// schcount,storedProcedureFun1);
		
		
		
		ArrayList<Object> ajaxData = new ArrayList<Object>();
		ajaxData.add(countCREMFreshCalls);
		ajaxData.add(countCREMFollowups);
		ajaxData.add(countCREMNonContacts);
		ajaxData.add(countCREMoverDueBookings);
		ajaxData.add(countCREMbookings);
		ajaxData.add(countCREMtotalCalls);
		;
		logger.info("countCREMoverDueBookings : " + countCREMoverDueBookings + "countCREMFollowups : " + countCREMFollowups
				+ "countCREMFreshCalls :" + countCREMFreshCalls + " countCREMNonContacts " + countCREMNonContacts);

		return ok(toJson(ajaxData));
		
	}
	//manager psf call log view
	//psf 
	// SERVICE REMAINDER LIST DATA
		@Secure(clients = "FormClient")
		public Result getPSFassignedInteractionTableDataMR(String CREIds) {
			//String typeOfPSF = "4";


			if (CREIds.equals("Select")) {
				CREIds = "";
			}
			WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());
			Map<String, String[]> paramMap = request().queryString();
			ObjectNode result = Json.newObject();
			String typeOfPSF = "";
			String fromDateNew = "";
			String toDateNew = "";
			String serviceTypeName = "";
			String serviceBookedType = "";
			String creidsare = "";

			if (paramMap.get("columns[0][search][value]") != null) {
				typeOfPSF = paramMap.get("columns[0][search][value]")[0];
				if (typeOfPSF.equals("0")) {

					typeOfPSF = "";
				}else if (typeOfPSF.equals("psf6thday")) {

						typeOfPSF = "4";

					} else if (typeOfPSF.equals("psf15thday")) {

						typeOfPSF = "5";

					} else if (typeOfPSF.equals("psf30thday")) {

						typeOfPSF = "6";

					}else if(typeOfPSF.equals("psf3rdday")){
						
						typeOfPSF = "7";
					}else if(typeOfPSF.equals("psf1stday")){
						
						typeOfPSF="4";
					}else if (typeOfPSF.equals("psf4thday")) {

						typeOfPSF = "5";

					}
					
				}

			if (paramMap.get("columns[1][search][value]") != null) {
				fromDateNew = paramMap.get("columns[1][search][value]")[0];
			}

			if (paramMap.get("columns[2][search][value]") != null) {
				toDateNew = paramMap.get("columns[2][search][value]")[0];
			}

			/*if (paramMap.get("columns[3][search][value]") != null) {
				serviceTypeName = paramMap.get("columns[3][search][value]")[0];
				if (serviceTypeName.equals("0")) {
					serviceTypeName = "";
				}
			}

			if (paramMap.get("columns[4][search][value]") != null) {
				serviceBookedType = paramMap.get("columns[4][search][value]")[0];
				if (serviceBookedType.equals("0")) {
					serviceBookedType = "";
				}
			}*/

			if (paramMap.get("columns[7][search][value]") != null) {
				creidsare = paramMap.get("columns[7][search][value]")[0];
				if (creidsare.equals("Select")) {
					creidsare = "";
				}
			}

			logger.info("SearchDate From: " + fromDateNew);
			logger.info("SearchDate To: " + toDateNew);
			//logger.info("serviceTypeName: " + serviceTypeName);
			//logger.info("serviceBookedType: " + serviceBookedType);
			String searchPattern = "";
			boolean allflag = false;
			boolean globalSearch = false;

			if (paramMap.get("search[value]") != null) {
				searchPattern = paramMap.get("search[value]")[0];
			}

			if (searchPattern.length() == 0 && typeOfPSF.length() == 0 && fromDateNew.length() == 0
					&& toDateNew.length() == 0) {
				allflag = true;
				logger.info("search length is 0 ");
			}
			if (searchPattern.length() > 0) {
				globalSearch = true;
				logger.debug("Global Search is true");
			}
			logger.info("searchPattern :" + searchPattern);

			long fromIndex = Long.valueOf(paramMap.get("start")[0]);
			long toIndex = Long.valueOf(paramMap.get("length")[0]);
			long totalSize = 0;
			//String storedProcedureFun = "cremanagerServiceReminderCount";
			totalSize = searchRepo.getPSFassignedInteractionTableDataMRCount(getUserLoginName(),fromDateNew, toDateNew,typeOfPSF,searchPattern,creidsare);

			logger.info("totalSize of assigned interaction : " + totalSize);

			if (toIndex < 0) {
				toIndex = 10;
			}
			if (toIndex > totalSize) {
				toIndex = totalSize;
			}
			long patternCount = 0;
			List<CallLogAjaxLoadMR> assignedList = searchRepo.PSFassignedListOfUserMR(getUserLoginName(),fromDateNew, toDateNew, typeOfPSF,
					 searchPattern, creidsare, fromIndex, toIndex);
			if (!allflag) {
				if (globalSearch)
					//patternCount = totalSize;
					patternCount = searchRepo.PSFassignedListOfUserMR(getUserLoginName(),fromDateNew, toDateNew, typeOfPSF,
							 searchPattern, creidsare, fromIndex, toIndex).size();

				else
					patternCount = searchRepo.PSFassignedListOfUserMR(getUserLoginName(),fromDateNew, toDateNew, typeOfPSF,
							 searchPattern, creidsare, fromIndex, toIndex).size();

			}
			logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
					+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

			logger.info("allFlag : " + allflag + " global : " + globalSearch);

			result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
			result.put("recordsTotal", totalSize);
			if (allflag) {
				result.put("recordsFiltered", totalSize);
			} else {
				result.put("recordsFiltered", patternCount);
			}
			ArrayNode an = result.putArray("data");
			for (CallLogAjaxLoadMR c : assignedList) {
				
				ObjectNode row = Json.newObject();
				row.put("0", c.getCampaignName());
				row.put("1", c.getCREName());
				row.put("2", c.getCustomer_name());
				row.put("3", c.getMobile_number());
				row.put("4", c.getVehicle_RegNo());
				row.put("5", c.getModel());
				row.put("6", c.getRONumber());
				row.put("7", String.valueOf(c.getROdate()));
				row.put("8", String.valueOf(c.getBillDate()));
				row.put("9", c.getCategory());
				an.add(row);
			}
			return ok(result);
		}
		// psf follow up
	@Secure(clients = "FormClient")
	public Result getPSFfollowUpCallLogTableDataMR(String CREIds , long buckettype) {
		//String typeOfPSF = "4";
			
		
		if (CREIds.equals("Select")) {
			logger.info("CREIdsIF :" + CREIds);
			CREIds = "";
		}

		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();
		String typeOfPSF = "";
		String fromDateNew = "";
		String toDateNew = "";
		String serviceTypeName = "";
		String serviceBookedType = "";
		String creidsare = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			typeOfPSF = paramMap.get("columns[0][search][value]")[0];
			if (typeOfPSF.equals("0")) {

				typeOfPSF = "";
			}else if (typeOfPSF.equals("psf6thday")) {

					typeOfPSF = "4";

				} else if (typeOfPSF.equals("psf15thday")) {

					typeOfPSF = "5";

				} else if (typeOfPSF.equals("psf30thday")) {

					typeOfPSF = "6";

				}else if(typeOfPSF.equals("psf3rdday")){
					
					typeOfPSF = "7";
				}else if(typeOfPSF.equals("psf1stday")){
					
					typeOfPSF="4";
				}else if (typeOfPSF.equals("psf4thday")) {

					typeOfPSF = "5";

				}
				
			}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDateNew = paramMap.get("columns[2][search][value]")[0];
		}

	

		/*if (paramMap.get("columns[4][search][value]") != null) {
			serviceBookedType = paramMap.get("columns[4][search][value]")[0];
			if (serviceBookedType.equals("0")) {
				serviceBookedType = "";
			}
		}*/
		if (paramMap.get("columns[7][search][value]") != null) {
			creidsare = paramMap.get("columns[7][search][value]")[0];
			if (creidsare.equals("Select")) {
				creidsare = "";
			}
		}

		logger.info("campaign Name: " + typeOfPSF);
		logger.info("SearchDate From: " + fromDateNew);
		logger.info("SearchDate To: " + toDateNew);
		//logger.info("serviceTypeName: " + serviceTypeName);
		//logger.info("serviceBookedType: " + serviceBookedType);
		logger.info("creidsare: " + creidsare);
		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;
		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}
		if (searchPattern.length() == 0 && typeOfPSF.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		long totalSize = 0;
		logger.info("CREIds : " + creidsare);
		logger.info("totalSizepro : " + totalSize);

		String storedProcedureFun = "cremanagerdispositionfilterCount";

		logger.info("storedProcedureFun : " + storedProcedureFun);
		totalSize = searchRepo.getPSFfollowUpCallLogTableDataMRCount(getUserLoginName(),fromDateNew,toDateNew,typeOfPSF,searchPattern,creidsare,buckettype);

		logger.info("totalSize : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		int orderDirection = 1;
		List<CallLogDispositionLoadMR> followUpList = searchRepo.getPSFfollowUpCallLogTableDataMR(getUserLoginName(),fromDateNew, toDateNew,
				typeOfPSF, searchPattern,creidsare, buckettype, fromIndex,
				toIndex);

		if (!allflag) {
			if (globalSearch)
				//patternCount = totalSize;
				patternCount = searchRepo
						.getPSFfollowUpCallLogTableDataMR(getUserLoginName(),fromDateNew, toDateNew,
								typeOfPSF, searchPattern,creidsare, buckettype, fromIndex,
								toIndex)
				.size();
			else
				patternCount =searchRepo.getPSFfollowUpCallLogTableDataMR(getUserLoginName(),fromDateNew, toDateNew,
						typeOfPSF, searchPattern,creidsare, buckettype, fromIndex,
						toIndex)
						.size();

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogDispositionLoadMR c : followUpList) {

			
			String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";

			
			String stringDate=getStringDate(c.getCall_date());			
			String stringROdate = getStringDate(c.getROdate());
			String stringBilldate = getStringDate(c.getBillDate());
			String stringSurveydate = getStringDate(c.getServey_date());
			String appointmentdateString = getStringDate(c.getPsfAppointmentDate());
			ObjectNode row = Json.newObject();
			
			if(buckettype== 4){
			row.put("0", c.getCampaignName());
			row.put("1", c.getCreName());
			row.put("2", c.getCustomer_name());
			row.put("3", c.getMobile_number());
			row.put("4", c.getVehicle_RegNo());
			row.put("5", c.getFollowUpDate());
			row.put("6", c.getFollowUpTime());
			row.put("7", stringDate);
			row.put("8", c.getRONumber());
			row.put("9", stringROdate);
			row.put("10", stringBilldate);
			row.put("11", stringSurveydate);
			row.put("12", c.getModel());
			
			
			}else if(buckettype== 22){				
				row.put("0", c.getCampaignName());
				row.put("1", c.getCreName());
				row.put("2", c.getCustomer_name());
				row.put("3", c.getMobile_number());
				row.put("4", c.getVehicle_RegNo());
				row.put("5", stringDate);
				row.put("6", c.getRONumber());
				row.put("7", stringROdate);
				row.put("8", stringBilldate);
				row.put("9", stringSurveydate);
				row.put("10", c.getModel());
				
				
				
			}else if(buckettype== 25){
				row.put("0", c.getCampaignName());
				row.put("1", c.getCreName());
				row.put("2", c.getCustomer_name());
				row.put("3", c.getMobile_number());
				row.put("4", c.getVehicle_RegNo());
				row.put("5", stringDate);
				row.put("6", c.getRONumber());
				row.put("7", stringROdate);
				row.put("8", stringBilldate);
				row.put("9", stringSurveydate);
				row.put("10", c.getModel());
				row.put("11", appointmentdateString);
				row.put("12", c.getPsfAppointmentTime());
				
				
			}
			

			an.add(row);
		}

		return ok(result);
	}
	// psf non contacts
	// NON CONTACT DATA
		@Secure(clients = "FormClient")
		public Result getPSFnonContactsServerDataTableMR(String CREIds, long buckettype) {
			//String typeOfPSF = "4";
				
			

			logger.info("non contacts controller");
			if (CREIds.equals("Select")) {
				CREIds = "";
			}
			WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());
			Map<String, String[]> paramMap = request().queryString();
			ObjectNode result = Json.newObject();
			String typeOfPSF = "";
			String fromDateNew = "";
			String toDateNew = "";
			String lastDipo = "";
			String droppedCount = "";
			String creidsare = "";

			if (paramMap.get("columns[0][search][value]") != null) {
				typeOfPSF = paramMap.get("columns[0][search][value]")[0];
				if (typeOfPSF.equals("0")) {

					typeOfPSF = "";
				}else if (typeOfPSF.equals("psf6thday")) {

						typeOfPSF = "4";

					} else if (typeOfPSF.equals("psf15thday")) {

						typeOfPSF = "5";

					} else if (typeOfPSF.equals("psf30thday")) {

						typeOfPSF = "6";

					}else if(typeOfPSF.equals("psf3rdday")){
						
						typeOfPSF = "7";
					}else if(typeOfPSF.equals("psf1stday")){
						
						typeOfPSF="4";
					}else if (typeOfPSF.equals("psf4thday")) {

						typeOfPSF = "5";

					}
					
				}
			
			
			logger.info(paramMap.get("columns[1][search][value]").toString());
			if (paramMap.get("columns[1][search][value]") != null) {
				fromDateNew = paramMap.get("columns[1][search][value]")[0];
			}

			if (paramMap.get("columns[2][search][value]") != null) {
				toDateNew = paramMap.get("columns[2][search][value]")[0];
			}

			/*if (paramMap.get("columns[5][search][value]") != null) {
				lastDipo = paramMap.get("columns[5][search][value]")[0];
				if (lastDipo.equals("0")) {

					lastDipo = "";
				}
			}

			if (paramMap.get("columns[6][search][value]") != null) {
				droppedCount = paramMap.get("columns[6][search][value]")[0];
				if (droppedCount.equals("0")) {

					droppedCount = "";
				}
			}*/
			if (paramMap.get("columns[7][search][value]") != null) {
				creidsare = paramMap.get("columns[7][search][value]")[0];
				if (creidsare.equals("Select")) {
					creidsare = "";
				}
			}

			logger.info("campaign Name: " + typeOfPSF);
			logger.info("SearchDate From: " + fromDateNew);
			logger.info("SearchDate To: " + toDateNew);
			logger.info("lastDipo: " + lastDipo);
			logger.info("droppedCount: " + droppedCount);
			String searchPattern = "";
			boolean allflag = false;
			boolean globalSearch = false;
			if (paramMap.get("search[value]") != null) {
				searchPattern = paramMap.get("search[value]")[0];
			}
			if (searchPattern.length() == 0 && typeOfPSF.length() == 0 && fromDateNew.length() == 0
					&& toDateNew.length() == 0) {
				allflag = true;
				logger.info("search length is 0 ");
			}
			if (searchPattern.length() > 0) {
				globalSearch = true;
				logger.debug("Global Search is true");
			}
			long fromIndex = Long.valueOf(paramMap.get("start")[0]);
			long toIndex = Long.valueOf(paramMap.get("length")[0]);
			long totalSize = 0;
			//String storedProcedureFun = "cremanagerNonContactsSearchCount";
			totalSize = searchRepo.getPSFnonContactsServerDataTableMRCount(getUserLoginName(),fromDateNew,toDateNew,typeOfPSF,searchPattern, creidsare,buckettype);
			logger.info("totalSize of noncontacts interaction : "+totalSize);

			if (toIndex < 0) {
				toIndex = 10;

			}

			if (toIndex > totalSize) {
				toIndex = totalSize;
			}
			long patternCount = 0;
			List<CallLogDispositionLoadMR> nonContactsList = searchRepo.getPSFnonContactsServerDataTableMR(getUserLoginName(),fromDateNew, toDateNew,
					typeOfPSF, searchPattern, creidsare, buckettype, fromIndex, toIndex);

			if (!allflag) {
				if (globalSearch)
					//patternCount = totalSize;
					patternCount = searchRepo.getPSFnonContactsServerDataTableMR(getUserLoginName(),fromDateNew, toDateNew,
							typeOfPSF, searchPattern, creidsare, buckettype, fromIndex, toIndex).size();

				else
					patternCount = searchRepo.getPSFnonContactsServerDataTableMR(getUserLoginName(),fromDateNew, toDateNew,
							typeOfPSF, searchPattern, creidsare, buckettype, fromIndex, toIndex).size();

			}

			// logger.info("nonContactsList : "+nonContactsList.size());

			result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
			result.put("recordsTotal", totalSize);

			if (allflag) {
				result.put("recordsFiltered", totalSize);
			} else {
				result.put("recordsFiltered", patternCount);
			}
			ArrayNode an = result.putArray("data");

			for (CallLogDispositionLoadMR c : nonContactsList) {

				String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";
				//String downloadMediaMR="<audio controls><source src='http://localhost:9000/CRE/audiostream/"+c.getCallinteraction_id()+"' type='audio/mp3'></audio>";

				// logger.info(urlDisposition);
				
				String stringDate=getStringDate(c.getCall_date());			
				String ROdatetostring = getStringDate(c.getROdate());
				String BilldatetoString = getStringDate(c.getBillDate());
				String serveyDateString = getStringDate(c.getServey_date());
				ObjectNode row = Json.newObject();
				row.put("0", c.getCampaignName());
				row.put("1", c.getCREName());
				row.put("2", c.getCustomer_name());
				row.put("3", c.getMobile_number());
				row.put("4", c.getVehicle_RegNo());				
				row.put("5", stringDate);
				row.put("6", c.getRONumber());				
				row.put("7", BilldatetoString);
				row.put("8", serveyDateString);
				row.put("9", c.getModel());
				row.put("10", disposition);
				
				an.add(row);
			}

			return ok(result);

		}
		//manager psf call log view
				//psf 
				// SERVICE REMAINDER LIST DATA
					@Secure(clients = "FormClient")
					public Result getInsuranceassignedInteractionTableDataMR(String CREIds) {
						String typeOfPSF = "";

			logger.info("logged in user:"+getUserLoginName());
						if (CREIds.equals("Select")) {
							CREIds = "";
						}
						WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());
						Map<String, String[]> paramMap = request().queryString();
						ObjectNode result = Json.newObject();
						String campaignName = "";
						String fromDateNew = "";
						String toDateNew = "";
						String serviceTypeName = "";
						String serviceBookedType = "";
						String creidsare = "";
						String renewalType ="";
						if (paramMap.get("columns[0][search][value]") != null) {
							campaignName = paramMap.get("columns[0][search][value]")[0];
							if (campaignName.equals("0")) {

								campaignName = "";
							}else{
								typeOfPSF = campaignName;
							}
							}

						if (paramMap.get("columns[1][search][value]") != null) {
							fromDateNew = paramMap.get("columns[1][search][value]")[0];
						}

						if (paramMap.get("columns[2][search][value]") != null) {
							toDateNew = paramMap.get("columns[2][search][value]")[0];
						}

						/*if (paramMap.get("columns[3][search][value]") != null) {
							serviceTypeName = paramMap.get("columns[3][search][value]")[0];
							if (serviceTypeName.equals("0")) {
								serviceTypeName = "";
							}
						}*/

						if (paramMap.get("columns[4][search][value]") != null) {
							renewalType = paramMap.get("columns[4][search][value]")[0];
							if (renewalType.equals("0")) {
								renewalType = "";
							}
						}

						if (paramMap.get("columns[7][search][value]") != null) {
							creidsare = paramMap.get("columns[7][search][value]")[0];
							if (creidsare.equals("Select")) {
								creidsare = "";
							}
						}

						logger.info("campaign Name: " + campaignName);
						logger.info("SearchDate From: " + fromDateNew);
						logger.info("SearchDate To: " + toDateNew);
						//logger.info("serviceTypeName: " + serviceTypeName);
						//logger.info("serviceBookedType: " + serviceBookedType);
						String searchPattern = "";
						boolean allflag = false;
						boolean globalSearch = false;

						if (paramMap.get("search[value]") != null) {
							searchPattern = paramMap.get("search[value]")[0];
						}

						if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
								&& toDateNew.length() == 0) {
							allflag = true;
							logger.info("search length is 0 ");
						}
						if (searchPattern.length() > 0) {
							globalSearch = true;
							logger.debug("Global Search is true");
						}
						logger.info("searchPattern :" + searchPattern);

						long fromIndex = Long.valueOf(paramMap.get("start")[0]);
						long toIndex = Long.valueOf(paramMap.get("length")[0]);
						long totalSize = 0;
						
						//String storedProcedureFun = "cremanagerServiceReminderCount";
						totalSize = searchRepo.getInsuranceassignedInteractionTableDataMRCount(getUserLoginName(),creidsare,typeOfPSF,fromDateNew, toDateNew,renewalType,searchPattern);

						logger.info("totalSize of assigned interaction : " + totalSize);

						if (toIndex < 0) {
							toIndex = 10;
						}
						if (toIndex > totalSize) {
							toIndex = totalSize;
						}
						long patternCount = 0;
						List<CallLogAjaxLoadMR> assignedList = searchRepo.InsuranceassignedListOfUserMR(getUserLoginName(),creidsare,typeOfPSF,fromDateNew, toDateNew,renewalType,
								 searchPattern, fromIndex, toIndex);
						if (!allflag) {
							if (globalSearch)
								//patternCount = totalSize;
								patternCount = searchRepo.InsuranceassignedListOfUserMR(getUserLoginName(),creidsare,typeOfPSF,fromDateNew, toDateNew,renewalType,
										
										 searchPattern, fromIndex, toIndex).size();

							else
								patternCount = searchRepo.InsuranceassignedListOfUserMR(getUserLoginName(),creidsare,typeOfPSF,fromDateNew, toDateNew,renewalType,
										 searchPattern, fromIndex, toIndex).size();

						}
						logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
								+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

						logger.info("allFlag : " + allflag + " global : " + globalSearch);

						result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
						result.put("recordsTotal", totalSize);
						if (allflag) {
							result.put("recordsFiltered", totalSize);
						} else {
							result.put("recordsFiltered", patternCount);
						}
						ArrayNode an = result.putArray("data");
						for (CallLogAjaxLoadMR c : assignedList) {
							
							ObjectNode row = Json.newObject();
							row.put("0", c.getCreName());
							row.put("1", c.getCampaign());
							row.put("2", c.getCustomerName());
							row.put("3", c.getPhone());
							row.put("4", c.getRegNo());
							row.put("5", c.getChassisNo());
							row.put("6", c.getPolicyDueMonth());
							row.put("7", String.valueOf(c.getPolicyDueDate()));
							row.put("8", c.getNextRenewalType());
							row.put("9", c.getLastInsuranceCompany());
							
							an.add(row);
						}
						return ok(result);
					}
					// psf follow up
				@Secure(clients = "FormClient")
				public Result getInsurancefollowUpCallLogTableDataMR(String CREIds , long buckettype) {
					String typeOfPSF = "";
						
					
					if (CREIds.equals("Select")) {
						logger.info("CREIdsIF :" + CREIds);
						CREIds = "";
					}

					WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());
					Map<String, String[]> paramMap = request().queryString();
					ObjectNode result = Json.newObject();
					String campaignName = "";
					String fromDateNew = "";
					String toDateNew = "";
					String serviceTypeName = "";
					String serviceBookedType = "";
					String creidsare = "";
					String renewalType ="";
					if (paramMap.get("columns[0][search][value]") != null) {
						campaignName = paramMap.get("columns[0][search][value]")[0];
						if (campaignName.equals("0")) {

							campaignName = "";
						}else{
							typeOfPSF = campaignName;
						}
							
						}

					if (paramMap.get("columns[1][search][value]") != null) {
						fromDateNew = paramMap.get("columns[1][search][value]")[0];
					}

					if (paramMap.get("columns[2][search][value]") != null) {
						toDateNew = paramMap.get("columns[2][search][value]")[0];
					}

				

					if (paramMap.get("columns[4][search][value]") != null) {
						renewalType = paramMap.get("columns[4][search][value]")[0];
						if (renewalType.equals("0")) {
							renewalType = "";
						}
					}
					if (paramMap.get("columns[7][search][value]") != null) {
						creidsare = paramMap.get("columns[7][search][value]")[0];
						if (creidsare.equals("Select")) {
							creidsare = "";
						}
					}

					logger.info("campaign Name: " + campaignName);
					logger.info("SearchDate From: " + fromDateNew);
					logger.info("SearchDate To: " + toDateNew);
					//logger.info("serviceTypeName: " + serviceTypeName);
					//logger.info("serviceBookedType: " + serviceBookedType);
					logger.info("creidsare: " + creidsare);
					String searchPattern = "";
					boolean allflag = false;
					boolean globalSearch = false;
					if (paramMap.get("search[value]") != null) {
						searchPattern = paramMap.get("search[value]")[0];
					}
					if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
							&& toDateNew.length() == 0) {
						allflag = true;
						logger.info("search length is 0 ");
					}
					if (searchPattern.length() > 0) {
						globalSearch = true;
						logger.debug("Global Search is true");
					}
					long fromIndex = Long.valueOf(paramMap.get("start")[0]);
					long toIndex = Long.valueOf(paramMap.get("length")[0]);
					long totalSize = 0;
					logger.info("CREIds : " + creidsare);
					logger.info("totalSizepro : " + totalSize);

					//String storedProcedureFun = "cremanagerdispositionfilterCount";

//					logger.info("storedProcedureFun : " + storedProcedureFun);
					totalSize = searchRepo.getInsurancefollowUpCallLogTableDataMRCount(getUserLoginName(),creidsare,fromDateNew,toDateNew,typeOfPSF,renewalType,searchPattern,buckettype);

					logger.info("totalSize : " + totalSize);

					if (toIndex < 0) {
						toIndex = 10;

					}

					if (toIndex > totalSize) {

						toIndex = totalSize;
					}
					long patternCount = 0;

					int orderDirection = 1;
					List<CallLogDispositionLoadMR> followUpList = searchRepo.getInsurancefollowUpCallLogTableDataMR(getUserLoginName(),creidsare,fromDateNew, toDateNew,
							campaignName,renewalType, searchPattern, buckettype, fromIndex,
							toIndex);

					if (!allflag) {
						if (globalSearch)
							//patternCount = totalSize;
							patternCount = searchRepo
									.getInsurancefollowUpCallLogTableDataMR(getUserLoginName(),creidsare,fromDateNew, toDateNew,
											campaignName,renewalType, searchPattern, buckettype, fromIndex,
											toIndex)
							.size();
						else
							patternCount =searchRepo.getInsurancefollowUpCallLogTableDataMR(getUserLoginName(),creidsare,fromDateNew, toDateNew,
									campaignName,renewalType, searchPattern, buckettype, fromIndex,
									toIndex)
									.size();

					}

					result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
					result.put("recordsTotal", totalSize);

					if (allflag) {
						result.put("recordsFiltered", totalSize);
					} else {
						result.put("recordsFiltered", patternCount);
					}
					ArrayNode an = result.putArray("data");

					for (CallLogDispositionLoadMR c : followUpList) {

						// String urlDisposition = "<a href='/" + getUserLogindealerCode() +
						// "/CRE/getFollowUpCallDispositionPage/"
						// + c.getCallinteraction_id()
						// + "'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\"
						// title=\"Disposition\"
						// style=\"font-size:30px;color:#DD4B39;\"></i></a>";
						// String urlEdit = "<a href='/" + getUserLogindealerCode() +
						// "/CRE/toeditcallLog/" + c.getCallinteraction_id()
						// + "'><i class=\"fa fa-pencil-square-o\" data-toggle=\"tooltip\"
						// title=\"edit\"></i></a>";
						String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";

						// logger.info(urlDisposition);
						/*String downloadMediaMR = "<a href='/CREManager/downloadMediaFile/" + c.getCallinteraction_id()
								+ "'><i class='fa fa-play' data-toggle='tooltip' title='audio'></i></a>";
						*/
					//	String downloadMediaMR="<audio controls><source src='http://localhost:9000/CRE/audiostream/"+c.getCallinteraction_id()+"' type='audio/mp3'></audio>";				
						
						
						//logger.info(downloadMediaMR);
						String stringDate=getStringDate(c.getCall_date());			
						String stringpolicydate = getStringDate(c.getPolicyDueDate());
						String stringBilldate = getStringDate(c.getBillDate());
						String stringSurveydate = getStringDate(c.getServey_date());
						String appointmentdateString = getStringDate(c.getAppointmentDate());
					
						ObjectNode row = Json.newObject();
						if(buckettype == 4){
							row.put("0", c.getIsCallinitaited());
							row.put("1", c.getCreName());
							row.put("2", c.getCampaignName());
							row.put("3", c.getCustomerName());
							row.put("4", c.getVehicle_RegNo());
							row.put("5", c.getModel());
							row.put("6", c.getFollowUpDate());
							row.put("7", c.getFollowUpTime());
							row.put("8", c.getRenewalType());							
							row.put("9", c.getLast_disposition());						
						}else if(buckettype == 25){
							row.put("0", c.getCampaignName());
							row.put("1", c.getCreName());							
							row.put("2", c.getCustomerName());
							row.put("3", c.getVehicle_RegNo());							
							row.put("4", appointmentdateString);
							row.put("5", c.getAppointmentTime());
							row.put("6", c.getAppointmentstatus());
							row.put("7", c.getRenewalType());
							row.put("8", c.getInsurance_agent());
							row.put("9", c.getLast_disposition());	
						}else if(buckettype == 26){
							row.put("0", c.getCampaignName());
							row.put("1", c.getCreName());							
							row.put("2", c.getCustomerName());
							row.put("3", c.getVehicle_RegNo());							
							row.put("4", c.getRenewalType());
							row.put("5", c.getInsurance_agent());
							row.put("6", c.getReason());
							row.put("7", c.getLast_disposition());	
						}
						
						an.add(row);
					}

					return ok(result);
				}
				// insurance non contacts
				// NON CONTACT DATA
					@Secure(clients = "FormClient")
					public Result getInsurancenonContactsServerDataTableMR(String CREIds, long buckettype) {
						String typeOfPSF = "";
							
						

						logger.info("non contacts controller");
						if (CREIds.equals("Select")) {
							CREIds = "";
						}
						WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());
						Map<String, String[]> paramMap = request().queryString();
						ObjectNode result = Json.newObject();
						String campaignName = "";
						String fromDateNew = "";
						String toDateNew = "";
						String lastDipo = "";
						String droppedCount = "";
						String creidsare = "";
						String renewalType="";
						if (paramMap.get("columns[0][search][value]") != null) {
							campaignName = paramMap.get("columns[0][search][value]")[0];
							if (campaignName.equals("0")) {

								campaignName = "";
							}else {
								typeOfPSF=campaignName;
								
							}
						}
						
						logger.info(paramMap.get("columns[1][search][value]").toString());
						if (paramMap.get("columns[1][search][value]") != null) {
							fromDateNew = paramMap.get("columns[1][search][value]")[0];
						}

						if (paramMap.get("columns[2][search][value]") != null) {
							toDateNew = paramMap.get("columns[2][search][value]")[0];
						}

						if (paramMap.get("columns[4][search][value]") != null) {
							renewalType = paramMap.get("columns[4][search][value]")[0];
							if (renewalType.equals("0")) {
								renewalType = "";
							}
						}
						/*if (paramMap.get("columns[5][search][value]") != null) {
							lastDipo = paramMap.get("columns[5][search][value]")[0];
							if (lastDipo.equals("0")) {

								lastDipo = "";
							}
						}

						if (paramMap.get("columns[6][search][value]") != null) {
							droppedCount = paramMap.get("columns[6][search][value]")[0];
							if (droppedCount.equals("0")) {

								droppedCount = "";
							}
						}*/
						
						if (paramMap.get("columns[7][search][value]") != null) {
							creidsare = paramMap.get("columns[7][search][value]")[0];
							if (creidsare.equals("Select")) {
								creidsare = "";
							}
						}

						logger.info("campaign Name: " + campaignName);
						logger.info("SearchDate From: " + fromDateNew);
						logger.info("SearchDate To: " + toDateNew);
						logger.info("lastDipo: " + lastDipo);
						logger.info("droppedCount: " + droppedCount);
						String searchPattern = "";
						boolean allflag = false;
						boolean globalSearch = false;
						if (paramMap.get("search[value]") != null) {
							searchPattern = paramMap.get("search[value]")[0];
						}
						if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
								&& toDateNew.length() == 0) {
							allflag = true;
							logger.info("search length is 0 ");
						}
						if (searchPattern.length() > 0) {
							globalSearch = true;
							logger.debug("Global Search is true");
						}
						long fromIndex = Long.valueOf(paramMap.get("start")[0]);
						long toIndex = Long.valueOf(paramMap.get("length")[0]);
						long totalSize = 0;
						//String storedProcedureFun = "cremanagerNonContactsSearchCount";
						totalSize = searchRepo.getInsurancenonContactsServerDataTableMRCount(getUserLoginName(),creidsare,fromDateNew,toDateNew,typeOfPSF,
								renewalType,searchPattern, buckettype, droppedCount,lastDipo);
						logger.info("totalSize of noncontacts interaction : "+totalSize);

						if (toIndex < 0) {
							toIndex = 10;

						}

						if (toIndex > totalSize) {
							toIndex = totalSize;
						}
						long patternCount = 0;
						List<CallLogDispositionLoadMR> nonContactsList = searchRepo.getInsurancenonContactsServerDataTableMR(getUserLoginName(),creidsare, fromDateNew, toDateNew,
								typeOfPSF,renewalType, searchPattern,buckettype,droppedCount,lastDipo, fromIndex, toIndex);

						if (!allflag) {
							if (globalSearch)
								//patternCount = totalSize;
								patternCount = searchRepo.getInsurancenonContactsServerDataTableMR(getUserLoginName(),creidsare, fromDateNew, toDateNew,
										typeOfPSF,renewalType, searchPattern,buckettype,droppedCount,lastDipo, fromIndex, toIndex).size();

							else
								patternCount = searchRepo.getInsurancenonContactsServerDataTableMR(getUserLoginName(),creidsare, fromDateNew, toDateNew,
										typeOfPSF,renewalType, searchPattern,buckettype,droppedCount,lastDipo, fromIndex, toIndex).size();

						}

						// logger.info("nonContactsList : "+nonContactsList.size());

						result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
						result.put("recordsTotal", totalSize);

						if (allflag) {
							result.put("recordsFiltered", totalSize);
						} else {
							result.put("recordsFiltered", patternCount);
						}
						ArrayNode an = result.putArray("data");

						for (CallLogDispositionLoadMR c : nonContactsList) {

							//String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";
							//String downloadMediaMR="<audio controls><source src='http://localhost:9000/CRE/audiostream/"+c.getCallinteraction_id()+"' type='audio/mp3'></audio>";

							// logger.info(urlDisposition);
							
							String stringDate=getStringDate(c.getCall_date());			
							String appointmentDatetoString = getStringDate(c.getAppointmentDate());
							String policyString = getStringDate(c.getPolicyDueDate());
							String serveyDateString = getStringDate(c.getServey_date());
							ObjectNode row = Json.newObject();
							row.put("0", c.getCampaignName());
							row.put("1", c.getCreName());
							row.put("2", c.getCustomerName());
							row.put("3", stringDate);
							row.put("4", c.getMobileNumber());
							row.put("5", c.getVehicle_RegNo());
							row.put("6", c.getLast_disposition());
							
							an.add(row);
						}

						return ok(result);

					}
}
