package controllers;

import static play.libs.Json.toJson;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.collections.CollectionUtils;
import org.apache.metamodel.DataContextFactory;
import org.apache.metamodel.excel.ExcelConfiguration;
import org.apache.metamodel.excel.ExcelDataContext;
import org.apache.metamodel.schema.Schema;
import org.apache.metamodel.schema.Table;
import org.apache.metamodel.schema.naming.CustomColumnNamingStrategy;
import org.pac4j.core.config.Config;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import controllers.pac4j.MyAppAuthenticator;
import controllers.webModel.FileDelete;
import controllers.webModel.FileUploadData;
import controllers.webModel.FileUploadResponse;
import controllers.webModel.FileUploadWithError;
import models.Campaign;
import models.ColumnDefinition;
import models.FormData;
import models.Location;
import models.Phone;
import models.SMSTemplate;
import models.Upload;
import models.UploadType;
import models.Vehicle;
import models.WyzUser;
import play.Configuration;
import play.Logger.ALogger;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import repositories.UploadRepository;
import repositories.WyzUserRepository;
import repositories.SMSTemplateRepository;
import repositories.SMSTriggerRespository;

public class FileUploadController extends Controller {

	public static String PLAY_CONFIG_STORAGE_PATH = "filestorage_path";

	public static String PLAY_CONFIG_PATH_SEPARATOR = "path_separator";

	private WyzUserRepository wyzrepo;

	private UploadRepository uploadrepo;
	private SMSTemplateRepository smsTemplate;
	private SMSTriggerRespository smsTrigger;

	protected static ALogger logger = play.Logger.of("application");

	private Config config;

	private Configuration playConfiguration;

	private PlaySessionStore playSessionStore;

	private HttpExecutionContext ec;

	@Inject
	public FileUploadController(WyzUserRepository wyz, UploadRepository urepo,SMSTemplateRepository smsTemp,
			SMSTriggerRespository smsTrig, Config cnfg, PlaySessionStore plstore,
			Configuration plyconfig, HttpExecutionContext httpec) {
		this.wyzrepo = wyz;
		this.uploadrepo = urepo;
		this.smsTemplate=smsTemp;
		this.smsTrigger=smsTrig;
		this.config = cnfg;
		this.playSessionStore = plstore;
		this.playConfiguration = plyconfig;
		this.ec = httpec;
	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");
		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();
		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");
		return dealerName;

	}

	private String getOEMOfDealer() {

		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;

	}
	
	@Secure(clients = "FormClient")
	public Result uploadPage() {
		List<Location> locationList = uploadrepo.getLocationList();
		List<UploadType> utypes = uploadrepo.getUploadTypes();
		ArrayList<String> utypeList = new ArrayList<String>();
		utypes.forEach(type -> {
			utypeList.add(type.getUploadTypeName());
		});
		return ok(views.html.upload.fileUpload.render(utypeList,getUserId(),getDealerName(),locationList));
	}

	@Secure(clients = "FormClient")
	public Result uploadReportPage() {
		List<UploadType> utypes = uploadrepo.getUploadTypes();
		ArrayList<String> utypeList = new ArrayList<String>();
		utypes.forEach(type -> {
			utypeList.add(type.getUploadTypeName());
		});
		return ok(views.html.upload.uploadReport.render(utypeList,getUserId(),getDealerName()));
	}

	public Result headRequest() {
		return ok();
	}

	@Secure(clients = "FormClient")
	public Result startUpload() {
		logger.debug("In the server side code...............");

		MultipartFormData body = request().body().asMultipartFormData();
		FilePart file = body.getFile("uploadfile");

		Map<String, String[]> otherParameters = request().body().asMultipartFormData().asFormUrlEncoded();

		if (file != null) {
			String fileName = file.getFilename();
			logger.debug("FileName:" + fileName);
			String contentType = file.getContentType();
			File fileObj = (File) file.getFile();

			String uploadType = otherParameters.get("uploadTypeHidden")[0];
			logger.debug("Obtained upload Type:" + uploadType);
			if (uploadType == null) {
				flash("error", "uploadType not present");
				return badRequest();
			}

			String newFilePath = getFileStoragePath();

			if (newFilePath == null) {
				flash("error", "problem in configuring storage path");
				return badRequest();
			}

			File directory = new File(newFilePath);
			if (!directory.exists()) {
				logger.debug("Folders does not exists creating the same");
				directory.mkdirs();
			}

			int indx = fileName.indexOf(".");

			String fileNamePart = fileName.substring(0, indx);

			fileNamePart += "_";
			GregorianCalendar cal = new GregorianCalendar();
			SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyy");
			String datestr = formatter.format(cal.getTime());
			fileNamePart += datestr;
			fileNamePart += Long.toString(cal.getTimeInMillis());

			String extn = fileName.substring(indx);
			String newFileName = fileNamePart + extn;
			newFilePath += newFileName;

			logger.debug("About to save file to->:" + newFilePath);

			File newFile = new File(newFilePath);
			fileObj.renameTo(newFile);
			logger.debug("Successfully saved the file to->" + newFilePath);

			fileObj.delete();
			logger.debug("Deleted the old file");

			long size = newFile.length();

			ArrayList<FormData> parameterList = new ArrayList<FormData>();
			otherParameters.forEach((key, value) -> {
				logger.debug("Other Parameter Key:"+key+" Value: "+value);
				if (!(key.trim().equalsIgnoreCase("uploadTypeHidden")) && !(key.trim().equalsIgnoreCase("uploadType"))) {
					FormData data = new FormData();
					data.setName(key);
					data.setValue(value[0]);
					parameterList.add(data);
				}
			});

			try {
				Upload upload = makeUploadObject(newFilePath, uploadType, fileName, size,parameterList);

				if (upload == null) {
					flash("error", "could not make the upload object");
					return badRequest();
				} else {

					if (!upload.isUploadError()) {
						logger.debug("There is no error addding to repository");
						uploadrepo.addUpload(upload);
					}
					long id = upload.getId();
					logger.debug("Upload object successfully persisted with id:" + id);

					// Get all uploads for past 3 days with status
					String userName = getUserId();
					if (userName == null) {
						flash("error", "Invalid session no userid found");
						return badRequest();
					}

					// List<Upload> uploadList =
					// uploadrepo.getLast3DaysUploads(userName, currentDate,
					// uploadType);

					// uploadList.remove(upload);

					ArrayList files = new ArrayList();

					populateResponseArrayList(upload, files);
					/*
					 * uploadList.forEach(up -> { populateResponseArrayList(up,
					 * files); });
					 */
					FileUploadResponse response = new FileUploadResponse();
					response.setFiles(files);
					return ok(toJson(response));

				}

			} catch (Exception e) {
				logger.debug("Some exception occured");
				e.printStackTrace();

				File tobeDeleted = new File(newFilePath);
				if (tobeDeleted.exists()) {
					tobeDeleted.delete();
					logger.debug("Deleted the file --->" + newFilePath);
				}

				return internalServerError();
			}
		} else {
			flash("error", "Missing file");
			return badRequest();
		}

	}


	@Secure(clients = "FormClient")
	public CompletionStage<Result> downloadFile(String id) {
		return CompletableFuture.supplyAsync(() -> {
			Upload upload = uploadrepo.getUpload(Long.parseLong(id));
			logger.debug("Obtained Upload object to download file:" + upload);

			String filePath = upload.getUploadPath();
			logger.debug("Obtained path for the download:" + filePath);
			return ok(new File(filePath));
		}, ec.current());
	}

	public CompletionStage<Result> downloadErrors(String id) {
		return CompletableFuture.supplyAsync(() -> {
			Upload upload = uploadrepo.getUpload(Long.parseLong(id));
			logger.debug("Obtained upload object to download file:" + upload);

			String filePath = upload.getDiscardedEntriesPath();
			if (filePath == null) {
				flash("error", "No error files found");
				return badRequest();
			} else if (filePath.isEmpty()) {
				flash("error", "No error files found");
				return badRequest();
			} else {
				logger.debug("Obtained path for the download:" + filePath);
				return ok(new File(filePath));
			}

		}, ec.current());

	}

	@Secure(clients = "FormClient")
	public Result getExistingFiles(String upType) {
		logger.debug("Got the method call to get files for type:" + upType);

		// Get all uploads for past 3 days with status
		GregorianCalendar cal = new GregorianCalendar();
		Date currentDate = cal.getTime();
		String userName = getUserId();
		if (userName == null) {
			flash("error", "Invalid session no userid found");
			return badRequest();
		}

		List<Upload> uploadList = uploadrepo.getLast3DaysUploads(userName, currentDate, upType);

		if (uploadList == null) {
			flash("error", "UploadTypes not defined");
			return badRequest();
		}

		ArrayList files = new ArrayList();

		uploadList.forEach(up -> {
			populateResponseArrayList(up, files);
		});

		FileUploadResponse response = new FileUploadResponse();
		response.setFiles(files);
		return ok(toJson(response));

	}

	@Secure(clients = "FormClient")
	public CompletionStage<Result> deleteFile(String id) {

		return CompletableFuture.supplyAsync(() -> deleteFileCall(id), ec.current())
				.thenApply(done -> processDeleteResult(done));
	}

	@Secure(clients = "FormClient")
	public Result processFile(String id) {
		logger.debug("....Got the request to process the file...:" + id);

		Upload upload = uploadrepo.getUpload(Long.parseLong(id));
		if (upload != null) {

			uploadrepo.updateProcessingStarted(Long.parseLong(id));

			logger.debug("Found upload object for submission");
			FileUploadResponse response = new FileUploadResponse();
			ArrayList files = new ArrayList();
			FileDelete delete = new FileDelete();
			delete.setName(upload.getFileName());
			files.add(delete);
			response.setFiles(files);
			return ok(toJson(response));

		} else {
			logger.debug("Could not find the upload object with the given id");
			flash("error", "Invalid upload id, could not find record");
			return badRequest();
		}

	}

	@Secure(clients = "FormClient")
	public Result getUploadsList(String uploadType) {

		logger.debug("Gettting all uploads for upload type:" + uploadType);
		List<Upload> uploads = uploadrepo.getAllUploads(uploadType);

		ObjectNode result = Json.newObject();

		ArrayNode an = result.putArray("data");

		for (Upload u : uploads) {
			ObjectNode row = Json.newObject();
			row.put("0", u.getFileName());
			String downloadUrl = "/downloadFile/" + u.getId();
			row.put("1", downloadUrl);
			row.put("2", u.getUploadedBy().getUserName());
			row.put("3", u.getProcessingStatus());
			Date upDate = u.getUploadedDateTime();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			String formatted = formatter.format(upDate);
			row.put("4", formatted);
			String error = "";
			String errorPath = "";
			if (u.isProcessingError()) {
				error = u.getError();
				errorPath = "/downloadErrors/" + u.getId();
			}
			row.put("5", u.getNumberProcessed());
			if (u.isProcessingError()) {
				row.put("6", "PROCESSING ERRORS");
			} else {
				row.put("6", "NO ERRORS");
			}
			row.put("7", u.getNumberDiscarded());
			row.put("8", errorPath);
			row.put("9", u.isProcessingError());
			Date pstart = u.getProcessingStartedDT();
			String pstartF = "";
			if (pstart != null)
				pstartF = formatter.format(pstart);

			row.put("10", pstartF);
			Date pedate = u.getProcessingEndedDT();
			String pedateF = "";
			if (pedate != null)
				pedateF = formatter.format(pedate);
			row.put("11", pedateF);
			an.add(row);
		}

		return ok(result);
	}

	
	@Secure(clients ="FormClient")
	public Result getSendSMS(long upId){
		Upload uploadType = uploadrepo.getUploadTypeBasedOnId(upId);
		logger.info("inside getsendsms method");
		//fromdate//
		long formvalueid=0;
		FormData formdataType = uploadrepo.getCampaignNameByUploadID(upId); 
		if(formdataType != null){
		 formvalueid = Long.valueOf(formdataType.getValue());
		}
		Campaign campaigntypeidvalue = uploadrepo.getcampaignIdByFormData(formvalueid);
		List<Vehicle> vehData = uploadrepo.getVehicleBasedOnUpId(upId);
		logger.info("vehData:"+vehData.size());
		// mobile,smid
		Map<String,String> dataList = new HashMap<String,String>();
		long smstemplateId=0;
		String messageTemp="";
	if(campaigntypeidvalue != null){
		if(campaigntypeidvalue.getCampaignTypeIdValue().equals("FS")){
			SMSTemplate msgTemp = smsTemplate.getSmsTemplate("FS");
			smstemplateId =msgTemp.getSmsId();
			logger.info("smr smstemplateId:"+smstemplateId);
			messageTemp=msgTemp.getSmsTemplate();
			}else if(campaigntypeidvalue.getCampaignTypeIdValue().equals("PS")){
				logger.info("inside campaign condition");
				SMSTemplate msgTemp = smsTemplate.getSmsTemplate("PS");
				smstemplateId =msgTemp.getSmsId();
				logger.info("smr smstemplateId:"+smstemplateId);
				messageTemp=msgTemp.getSmsTemplate();
				logger.info("smr messageTemp:"+messageTemp);
			}else if(campaigntypeidvalue.getCampaignTypeIdValue().equals("LOSTCUSTOMER")){
				logger.info("inside campaign condition");
				SMSTemplate msgTemp = smsTemplate.getSmsTemplate("LOSTCUSTOMER");
				smstemplateId =msgTemp.getSmsId();
				logger.info("smr smstemplateId:"+smstemplateId);
				messageTemp=msgTemp.getSmsTemplate();
				logger.info("smr messageTemp:"+messageTemp);
		}
		
	}else if(uploadType.getUploadType().getUploadDisplayName().equals("THANKYOU")){
			logger.info("inside campaign condition");
			SMSTemplate msgTemp = smsTemplate.getSmsTemplate("THANKYOU");
			smstemplateId =msgTemp.getSmsId();
			logger.info("smr smstemplateId:"+smstemplateId);
			messageTemp=msgTemp.getSmsTemplate();
			logger.info("smr messageTemp:"+messageTemp);
			
		}
		for(Vehicle vehList : vehData){
			String custname = vehList.getCustomer().getCustomerName();
			Phone phonenumb = smsTemplate.getPhonenumberbyUploadId(vehList.getCustomer().getId());
			String phonenum = phonenumb.getPhoneNumber();

			logger.info("custname:"+custname);
			logger.info("phonenumb:"+phonenumb);
			logger.info("smr for veicle:"+messageTemp);
			logger.info("phonenumber lazy loading:"+phonenum);
			dataList.put(phonenum, messageTemp);
		}
		logger.info("SendBulkSMSToUsers smsId : "+dataList.size());
		logger.info("SendBulkSMSToUsers smsId : "+smstemplateId);
		smsTemplate.SendBulkSMSToUsers(getUserLoginName(),dataList,smstemplateId);
		return ok("done");
	}
	
	@Secure(clients = "FormClient")
	public Result getUploadsListById(long upId) {

		logger.debug("Gettting all uploads for upload Id:" + upId);
		List<Upload> uploads = uploadrepo.getAllUploadsById(upId);

		ObjectNode result = Json.newObject();

		ArrayNode an = result.putArray("data");

		for (Upload u : uploads) {
			ObjectNode row = Json.newObject();
			row.put("0", u.getFileName());
			String downloadUrl = "/downloadFile/" + u.getId();
			row.put("1", downloadUrl);
			row.put("2", u.getUploadedBy().getUserName());
			row.put("3", u.getProcessingStatus());
			Date upDate = u.getUploadedDateTime();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			String formatted = formatter.format(upDate);
			row.put("4", formatted);
			String error = "";
			String errorPath = "";
			if (u.isProcessingError()) {
				error = u.getError();
				errorPath = "/downloadErrors/" + u.getId();
			}
			row.put("5", u.getNumberProcessed());
			if (u.isProcessingError()) {
				row.put("6", "PROCESSING ERRORS");
			} else {
				row.put("6", "NO ERRORS");
			}
			row.put("7", u.getNumberDiscarded());
			row.put("8", errorPath);
			row.put("9", u.isProcessingError());
			Date pstart = u.getProcessingStartedDT();
			String pstartF = "";
			if (pstart != null)
				pstartF = formatter.format(pstart);

			row.put("10", pstartF);
			Date pedate = u.getProcessingEndedDT();
			String pedateF = "";
			if (pedate != null)
				pedateF = formatter.format(pedate);
			row.put("11", pedateF);
			an.add(row);
		}

		return ok(result);
	}

	private String deleteFileCall(String id) {
		try {
			logger.debug("Trying to delete file with upload id:" + id);
			long idLong = Long.parseLong(id);

			logger.debug("Converted value of string id:" + idLong);

			Upload upload = uploadrepo.getUpload(idLong);
			if (upload == null)
				logger.debug("Upload object is null!!!!!!");

			String path = upload.getUploadPath();
			String filename = upload.getFileName();

			uploadrepo.deleteUpload(idLong);
			logger.debug("Deleted from the database upload id:" + id);

			logger.debug("Trying to delete ->" + path);
			File file = new File(path);
			if (file.exists()) {
				file.delete();
				logger.debug("Deleted the file->" + path);
			}
			return filename;

		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Some problem while deleting the file");
			return null;
		}
	}

	private Result processDeleteResult(String fileName) {

		if (fileName != null) {
			FileUploadResponse response = new FileUploadResponse();
			ArrayList files = new ArrayList();
			FileDelete delete = new FileDelete();
			delete.setName(fileName);
			files.add(delete);
			response.setFiles(files);
			return ok(toJson(response));
		} else {
			FileUploadResponse response = new FileUploadResponse();
			ArrayList files = new ArrayList();
			FileUploadWithError error = new FileUploadWithError();
			error.setName(fileName);
			error.setError("Could not delete the file.");
			files.add(error);
			response.setFiles(files);
			return ok(toJson(response));
		}
	}

	private Upload makeUploadObject(String filePath, String uploadType, String fileName, long size,
			List<FormData> parameterList) {

		Upload upload = new Upload();
		upload.setFileName(fileName);
		upload.setProcessed(false);
		upload.setProcessingStarted(false);
		upload.setProcessingError(false);
		upload.setUploadError(false);
		upload.setProcessingStatus(Upload.PROCESSING_UPLOADED);
		upload.setSize(size);
		String username = getUserId();
		if (username == null) {
			return null;
		}
		logger.debug("Trying to get user object for user:" + username);
		WyzUser user = wyzrepo.getUser(username);
		upload.setUploadedBy(user);

		UploadType utype = uploadrepo.getUploadType(uploadType);
		upload.setUploadType(utype);

		GregorianCalendar cal = new GregorianCalendar();
		upload.setUploadedDateTime(cal.getTime());
		upload.setUploadPath(filePath);	
		upload.setFormDataList(parameterList);
		//if(utype.getUploadDisplayName()!=null){		
		parameterList.forEach(parameter->{
			parameter.setUpload(upload);
		});
		
		//uploadrepo.saveFormDataList(parameterList,upload);
		
		/*}else{			
			parameterList.forEach(parameter->{
				if(parameter.getValue().equals("")){
					parameterList.remove(parameter);					
				}else{					
					parameter.setUpload(upload);
				}
			});
		}*/
		//upload.setFormDataList(parameterList);		
		basicAnalysis(upload);

		return upload;
	}

	private void basicAnalysis(Upload upload) {

		logger.debug("Starting analysis of file| uploadType:" + upload.getUploadType());
		logger.debug("Starting analysis of file| path->" + upload.getUploadPath());

		File file = new File(upload.getUploadPath());
		ExcelConfiguration config = null;

		List<ColumnDefinition> definitions = uploadrepo
				.getColumnDefinitions(upload.getUploadType().getUploadTypeName());

		ArrayList<String> defColumns = new ArrayList<String>();

		definitions.sort(new Comparator<ColumnDefinition>() {

			@Override
			public int compare(ColumnDefinition o1, ColumnDefinition o2) {
				if (o1.getId() > o2.getId())
					return 1;
				else if (o1.getId() < o2.getId())
					return -1;
				else
					return 0;
			}

		});
		
		String rowToIgnore = upload.getUploadType().getRowsToignore();
		int firstRowsToIgnore=0;
		int lastRowsToIgnore=0;
		Set<Long> ignoreRows = new HashSet<Long>();
		
		if (rowToIgnore != null) {
			
			String[] rows = rowToIgnore.split(",");
			for (String number : rows) {
				if (number != null) {
					if(number.startsWith("first")){
						logger.debug("String first encountered..");
						int indx = number.indexOf(":");
						String firstRows = number.substring(indx+1);
						firstRowsToIgnore = Integer.parseInt(firstRows);
						logger.debug("First rows to Ignore: "+firstRowsToIgnore);
					}else if (number.startsWith("last")) {
						logger.debug("String last encountered..");
						int indx = number.indexOf(":");
						String lastRows = number.substring(indx+1);
						lastRowsToIgnore = Integer.parseInt(lastRows);
						logger.debug("Last rows to Ignore: "+lastRowsToIgnore);
						
					} else {
						ignoreRows.add(new Long(Long.parseLong(number)));
					}
				}
			}
		}
		
		if(firstRowsToIgnore==0){
			firstRowsToIgnore =1;
		}
		
		List<ColumnDefinition> filteredDefinitions = definitions.stream()
				.filter(definition -> (!definition.isAdditionalFormData())).collect(Collectors.toList());

		if (upload.getUploadType().isIgnoreSourceColumnNames()) {
			logger.debug("Source Column names should be ignored......");
			filteredDefinitions.forEach(d -> {
				defColumns.add(d.getMetaColumnName());
			});
			logger.debug("Switching over to custom column naming strategy...");
			CustomColumnNamingStrategy strategy = new CustomColumnNamingStrategy(defColumns);
			config = new ExcelConfiguration(firstRowsToIgnore, strategy, true, true);
		} else {
			config = new ExcelConfiguration(firstRowsToIgnore, true, true);
			filteredDefinitions.forEach(d -> {
				defColumns.add(d.getSourceColumnName());
			});
		}

		ExcelDataContext excelContext = (ExcelDataContext) DataContextFactory.createExcelDataContext(file, config);

		Schema schema = excelContext.getDefaultSchema();

		logger.debug(schema.getName());
		logger.debug(schema.getTables()[0].getName());

		Table table = schema.getTable(0);

		String[] columNames = table.getColumnNames();
		
		for(int i=0;i<columNames.length;i++){
			logger.debug("Column Name: "+columNames[i]);
		}
		
		List<String> excelColumns = Arrays.asList(columNames);
		int excelcount = excelColumns.size();

		int expectedCount = defColumns.size();
		logger.debug("ExcelColumns Count:" + excelcount + "| Expected Count:" + expectedCount);

		if (expectedCount > excelcount) {
			logger.debug("Number or excel columns not matching defined columns");
			upload.setUploadError(true);
			upload.setError("Columns in the uploaded files are lesser than expected");
		}else if(expectedCount < excelcount){
			
			logger.debug("Number or excel columns not matching defined columns");
			upload.setUploadError(true);
			upload.setError("Columns in the uploaded files are greater than expected");
			
		} else {

			Collection subtract = CollectionUtils.subtract(defColumns, excelColumns);

			int substractsize = subtract.size();
			logger.debug("Difference between excel column names and expected names count:" + substractsize);

			if (substractsize != 0) {
				logger.debug("There are some column names not matching with the defined columns");
				upload.setUploadError(true);
				String joinedString = String.join(",", subtract);
				upload.setError("Following columns are not as expected:" + joinedString);
			}
		}
	}

	private void populateResponseArrayList(Upload upload, ArrayList files) {

		if (upload.isUploadError()) {
			logger.debug("There are some errors in upload");
			FileUploadWithError uploadError = new FileUploadWithError();
			uploadError.setId(upload.getId());
			uploadError.setName(upload.getFileName());
			uploadError.setSize(upload.getSize());
			uploadError.setError(upload.getError());
			files.add(uploadError);
		} else if (!upload.isProcessed()) {
			FileUploadData uploadresponse = new FileUploadData();
			uploadresponse.setId(upload.getId());
			uploadresponse.setName(upload.getFileName());
			uploadresponse.setSize(upload.getSize());
			uploadresponse.setDeleteType("DELETE");
			String deleteUrl = "/deletefile/" + upload.getId();
			uploadresponse.setDeleteUrl(deleteUrl);
			String downloadUrl = "/downloadFile/" + upload.getId();
			uploadresponse.setUrl(downloadUrl);

			String processUrl = "/wyzprocessFile/" + upload.getId();
			uploadresponse.setProcessUrl(processUrl);

			files.add(uploadresponse);
		}

	}

	private String getFileStoragePath() {

		String basePath = playConfiguration.getString(PLAY_CONFIG_STORAGE_PATH);
		logger.debug("Obtained basePath for filestorage:" + basePath);
		if (basePath == null) {
			return null;
		}
		String tenantid = getTenantId();
		if (tenantid == null) {
			return null;
		}
		String username = getUserId();
		if (username == null) {
			return null;
		}
		basePath += tenantid;
		basePath += playConfiguration.getString(PLAY_CONFIG_PATH_SEPARATOR);
		basePath += username;
		basePath += playConfiguration.getString(PLAY_CONFIG_PATH_SEPARATOR);
		logger.debug("Returning file save path:" + basePath);
		return basePath;
	}

	private String getUserId() {
		List<CommonProfile> profiles = getProfiles();
		if (profiles == null) {
			return null;
		} else if (profiles.get(0) == null) {
			return null;
		} else {
			CommonProfile profile = profiles.get(0);
			String username = profile.getUsername();
			logger.debug("Obtained username:" + username);
			if (username == null) {
				return null;
			} else {
				return username;
			}
		}
	}

	private String getTenantId() {
		List<CommonProfile> profiles = getProfiles();
		if (profiles == null) {
			return null;
		} else if (profiles.get(0) == null) {
			return null;
		} else {
			CommonProfile profile = profiles.get(0);
			String tenantid = (String) profile.getAttribute(MyAppAuthenticator.TENANT_KEY);
			logger.debug("Obtained tenantid:" + tenantid);
			if (tenantid == null) {
				return null;
			} else {
				return tenantid;
			}
		}
	}

	private List<CommonProfile> getProfiles() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles;
	}
	
	

	
}