package controllers;


import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;
import models.Dealer;
import play.Logger.ALogger;

import org.pac4j.core.config.Config;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.java.Secure;

import org.springframework.stereotype.Controller;

import play.data.Form;
import play.mvc.Result;
import repositories.DealerRepository;

import java.util.List;

import views.html.dealerRegistration;
import views.html.dealerInfo;
import views.html.editDealerInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

import static play.libs.Json.toJson;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;

/**
 * Created by enda on 31/08/15.
 */
@Controller

public class DealerController extends play.mvc.Controller {
	private final DealerRepository repo;

	ALogger logger = play.Logger.of("application");
	private Config configuration;
	private PlaySessionStore playSessionStore;

	
	@Inject
	public DealerController(DealerRepository repository, Config config,PlaySessionStore playses) {
		this.repo = repository;
		this.configuration = config;
		this.playSessionStore=playses;
	}
	
	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	
	@Secure(clients = "FormClient")
	public Result adddel() {

		String userloginname = getUserLoginName();
		String dealerName = getSelectedDealerName();
		return ok(dealerRegistration.render(dealerName, userloginname, Form.form(Dealer.class)));
	}
	@Secure(clients = "FormClient")
	public Result adddealerForm() {
		Form<Dealer> form = Form.form(Dealer.class).bindFromRequest();
		Dealer deal = form.get();
		String userloginname = getUserLoginName();

		String dealerName = getSelectedDealerName();

		Dealer savedapplication = repo.addDealerData(deal);
		return ok(dealerRegistration.render(dealerName, userloginname, Form.form(Dealer.class)));

	}
	@Secure(clients = "FormClient")
	public Result dealerInformation() {
		Form<Dealer> form = Form.form(Dealer.class).bindFromRequest();
		List<Dealer> dealersData = repo.getAllDealer();

		String userloginname = getUserLoginName();

		String dealerName = getSelectedDealerName();

		return ok(dealerInfo.render(dealerName, userloginname, dealersData, Form.form(Dealer.class)));
	}
	@Secure(clients = "FormClient")
	public Result deleteDealerData(Long did) {
		Dealer deleteDealerdata = repo.deleteDealer(did);
		List<Dealer> dealers = repo.getAllDealer();

		String userloginname = getUserLoginName();

		String dealerName = getSelectedDealerName();
		return ok(dealerInfo.render(dealerName, userloginname, dealers, Form.form(Dealer.class)));
	}
	@Secure(clients = "FormClient")
	public Result geteditDealer(Long did) {
		Form<Dealer> form = Form.form(Dealer.class).bindFromRequest();

		String userloginname = getUserLoginName();
		Dealer editDealerdata = repo.editDealer(did);

		String dealerName = getSelectedDealerName();

		return ok(editDealerInfo.render(dealerName, userloginname, did, editDealerdata, Form.form(Dealer.class)));
	}
	@Secure(clients = "FormClient")
	public Result postEditDealer(Long did) {
		Form<Dealer> form = Form.form(Dealer.class).bindFromRequest();
		Dealer del = form.get();
		Dealer editDealerdata = repo.editDealer(did);
		Dealer savedapplication = repo.posteditedDealerData(did, editDealerdata);
		List<Dealer> dealers = repo.getAllDealer();

		String userloginname = getUserLoginName();

		String dealerName = getSelectedDealerName();

		return ok(dealerInfo.render(dealerName, userloginname, dealers, Form.form(Dealer.class)));
	}
	
	@Secure(clients = "FormClient")
	public Result getOEMOfDealer(){
		String oemis=repo.getOEMbyDealer();
		
		return ok(toJson(oemis));
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getSelectedDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

}