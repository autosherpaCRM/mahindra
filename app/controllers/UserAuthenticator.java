package controllers;
import com.fasterxml.jackson.databind.JsonNode;

import play.mvc.BodyParser;
import static play.libs.Json.toJson;
import play.data.Form;
import play.Logger.ALogger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import javax.inject.Inject;
import javax.inject.Singleton;



import javax.sql.DataSource;

import play.api.Play;
import play.mvc.Result;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;
import controllers.pac4j.CustomAuthorizer;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;


import java.util.HashMap;
import java.util.Map;

import models.AndroidUser;
import models.WyzUser;

import org.pac4j.core.config.Config;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.LogoutController;
import org.pac4j.play.java.Secure;

import org.springframework.stereotype.Controller;
import org.pac4j.core.profile.UserProfile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;
import play.db.DB;


import repositories.WyzUserRepository;

import com.firebase.security.token.TokenGenerator;

@Controller
public class UserAuthenticator extends play.mvc.Controller{
	
	private final WyzUserRepository repo;	
	ALogger logger = play.Logger.of("application");

	private Config configuration;

	@Inject
	public UserAuthenticator(WyzUserRepository repository, Config config) {
		repo = repository;
		this.configuration = config;
		
		
	}


	public Result authenticateUser(String phoneNumber, String phoneIMEINo,String registrationId) {
		logger.info("the registrationNumber is : "+registrationId+" phoneNumber : "+phoneNumber+ "phoneIMEINo :"+phoneIMEINo);
		
		AndroidUser aUser = new AndroidUser();	
		String dealercode = null;	
		

		PreparedStatement pstament = null;
		ResultSet rs = null;
		Connection connection = null;
		try {

			DataSource datasource = DB.getDataSource();
			logger.info("the datasource is : "+datasource);

			connection = datasource.getConnection();

			pstament = connection
					.prepareStatement("SELECT * from WyzUser where phoneNumber=? and phoneIMEINo=?");
			pstament.setString(1, phoneNumber);
			pstament.setString(2, phoneIMEINo);

			rs = pstament.executeQuery();
			

			while (rs.next()) {				
				
				logger.info("Obtained user");

				
				
				 dealercode=rs.getString(3);	
				 repo.toaddTheRegistrationIdTOUserForTheParticulardatabase(phoneNumber, phoneIMEINo, registrationId,dealercode);

				if (dealercode != null) {

					Map<String, Object> authPayload = new HashMap<String, Object>();

					aUser.setAuthenticationStatus("true");
					aUser.setDealerId(rs.getString(4));

					authPayload.put("some", "arbitrary");
					authPayload.put("data", "here");
					aUser.setDealerName(rs.getString(5));

					aUser.setUserEmail(rs.getString(6));
					aUser.setUserFName(rs.getString(7));
					aUser.setUserId(rs.getString(16));

					aUser.setUserLName(rs.getString(8));
					aUser.setUserRole(rs.getString(14));
					aUser.setFireBaseUrl("https://wyzcrm-2feff.firebaseio.com/");

					authPayload.put("uid", rs.getString(16));
					authPayload.put("role", rs.getString(14));
					authPayload.put("dealerCode", rs.getString(3));

					TokenGenerator tokenGenerator = new TokenGenerator(
							"L6FlQ082WldrWTfRQqKSeKdlh1hUFXmFwf1iOqG1");
					String token = tokenGenerator.createToken(authPayload);

					aUser.setJwtToken(token);

				} else {
					aUser.setAuthenticationStatus("false");
					return ok(toJson(aUser));

				}
				return ok(toJson(aUser));
				
			}
		} catch (Exception ex) {

			ex.printStackTrace();

		} finally {
			logger.info("Executing finally block");
			try {
				if (rs != null) {
					rs.close();
					logger.info("result set closed");
				}
			} catch (Exception e) {
			}
			;
			try {
				if (pstament != null) {
					pstament.close();
					logger.info("Prepared Statement closed");
				}
			} catch (Exception e) {
			}
			;
			try {
				if (connection != null) {
					if (!connection.isClosed()) {
						connection.close();
						logger.info("connection is closed");
					}
				}
			} catch (Exception e) {
			}
			;
		}
		aUser.setAuthenticationStatus("false");
		return ok(toJson(aUser));
		
	}
	
	public Result updateUserAuthentication(String phoneIMEINo,String registrationId){
		Map<String ,String> resultStatus=repo.updateRegistrationIdOfIMEINo(phoneIMEINo,registrationId);
		
		
		return ok(toJson(resultStatus));
	}

}
