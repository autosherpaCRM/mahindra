package controllers;

import com.fasterxml.jackson.databind.JsonNode;

import play.mvc.BodyParser;
import static play.libs.Json.toJson;
import play.data.Form;
import play.Logger.ALogger;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.text.DecimalFormat;
import java.util.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpSession;

import views.html.appUser;
import views.html.index;
import views.html.userInfo;
import views.html.edit;
import views.html.login;
import views.html.changePassword;
import views.html.viewReport;
import views.html.indexPageCREManager;
import views.html.indexPageCRE;
import views.html.viewReportCREManager;
import views.html.changePassword;
import views.html.changePasswordCRE;
import views.html.changePasswordsuperAdmin;
import views.html.changePasswordServiceAdvisor;
import views.html.indexPageSalesManager;
import views.html.indexPageSalesExecutive;
import views.html.viewReportOfSE;
import views.html.indexPageServiceAdvisor;

import play.api.Play;
import play.mvc.Result;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;
import models.AndroidUser;
import models.Dealer;
import models.ListingForm;

import models.WyzUser;
import controllers.pac4j.CustomAuthorizer;
import controllers.webmodels.PieChartCallTypeData;

import java.util.concurrent.TimeUnit;

import play.libs.Akka;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Cancellable;
import akka.actor.Props;
import akka.actor.Scheduler;

import org.pac4j.core.config.Config;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.LogoutController;
import org.pac4j.play.java.Secure;

import org.springframework.stereotype.Controller;
import org.pac4j.core.profile.UserProfile;

import repositories.CallInfoRepository;
import repositories.DealerRepository;
import repositories.ScheduledCallRepository;
import repositories.WyzUserRepository;
import scala.concurrent.duration.Duration;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import com.firebase.security.token.TokenGenerator;
import com.google.api.client.http.HttpRequest;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;

@Controller
public class WyzUserController extends play.mvc.Controller {

	private final WyzUserRepository repo;
	private final DealerRepository repoDealer;
	private final CallInfoRepository repoCallInfo;
	private ScheduledCallRepository scheduledCallRepository;
	private PlaySessionStore playSessionStore;

	ALogger logger = play.Logger.of("application");

	private Config configuration;

	private LogoutController applicationLogoutController;

	private Cancellable schedulerCallLog = null;

	@Inject
	public WyzUserController(WyzUserRepository repository, Config config, DealerRepository repositoryDealer,
			ScheduledCallRepository schrepo, CallInfoRepository repoCallInfoRepository, LogoutController logoutCont,
			PlaySessionStore playses) {
		repo = repository;
		repoDealer = repositoryDealer;
		repoCallInfo = repoCallInfoRepository;
		scheduledCallRepository = schrepo;
		this.configuration = config;
		this.applicationLogoutController = logoutCont;
		playSessionStore = playses;
	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private List<CommonProfile> getProfiles() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.info("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.info("..............Obtained profiles........." + profiles);
		return profiles;
	}

	public Result loginfo() {

		return ok(views.html.logtail.render());
	}

	@Secure(clients = "FormClient")
	public Result indexPageCRE() {

		if (checkCRERole()) {
			logger.info("Entered in CRE");

			// Map<String, String[]> map = request().headers();
			//
			//
			// for (Map.Entry<String, String[]> entrydata : map.entrySet()) {
			//
			//
			//
			// String[] data=entrydata.getValue();
			//
			// // int size = data.length;
			//
			// logger.info("KEY : "+entrydata.getKey()+",VALUE : "+data[0]);
			//
			// }

			String referereAdress = request().getHeader("Referer");
			// logger.info("referereAdress : " + referereAdress);

			return redirect(
					controllers.routes.ScheduledCallController.getCallDispositionTabPAgeCRE());

		} else {
			applicationLogoutController.logout();

			return forbidden(views.html.error403.render());
		}

	}

	@Secure(clients = "FormClient")
	public Result indexPageServiceAdvisor() {
		logger.info("Entered in SA index page :" + checkSA_Role());

		if (checkSA_Role()) {
			logger.info("Entered in SA");

			return ok(indexPageServiceAdvisor.render(getUserLogindealerCode(), getDealerName(), getUserLoginName()));

		} else {
			applicationLogoutController.logout();

			return forbidden(views.html.error403.render());
		}

	}

	@Secure(clients = "FormClient")
	public Result indexPageOthers() {

		if (checkOthers_Role()) {
			logger.info("Entered in others");

			return ok(indexPageServiceAdvisor.render(getUserLogindealerCode(), getDealerName(), getUserLoginName()));

		} else {
			applicationLogoutController.logout();

			return forbidden(views.html.error403.render());
		}

	}

	@Secure(clients = "FormClient")
	public Result indexPageCREManager() {

		logger.info("Entered in CRE Manager");

		if (checkCREManagerRole()) {

			ArrayList<String> ajaxData = new ArrayList<String>();

			 /*return ok(indexPageCREManager.render(ajaxData, getDealerName(), getUserLoginName(),
			Form.form(WyzUser.class)));*/

			return redirect(
			controllers.routes.CallInfoController.getCallDispositionBucketForCREMan());
		} else {
			applicationLogoutController.logout();

			return forbidden(views.html.error403.render());
		}

	}

	@Secure(clients = "FormClient")
	public Result index() {

		return ok(index.render(getProfiles(), getUserLoginName(), ""));

	}

	@Secure(clients = "FormClient")
	public Result indexPageSalesManager() {
		CommonProfile profile = getUserProfile();

		String role = (String) profile.getAttribute("USER_ROLE");
		String dealercode = (String) profile.getAttribute("DEALER_CODE");
		String user = profile.getId();
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		if (checkSalesManagerRole()) {
			return ok(indexPageSalesManager.render(dealerName, user, Form.form(WyzUser.class)));
		} else {
			applicationLogoutController.logout();

			return forbidden(views.html.error403.render());
		}

	}

	@Secure(clients = "FormClient")
	public Result indexPageSalesExecutive() {

		CommonProfile profile = getUserProfile();
		String role = (String) profile.getAttribute("USER_ROLE");
		String userName = profile.getId();
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");
		String dealerCode = (String) getUserProfile().getAttribute("DEALER_CODE");
		if (checkSalesExecutiveRole()) {
			return ok(indexPageSalesExecutive.render(dealerName, userName, Form.form(WyzUser.class)));
		} else {
			applicationLogoutController.logout();

			return forbidden(views.html.error403.render());
		}

	}

	public Result login() {

		return ok("Done");
	}

	public Result changePassword() {
		String userloginname = getUserLoginName();
		String dealername = getDealerName();
		return ok(changePassword.render(dealername, userloginname, Form.form(WyzUser.class)));
	}

	public Result viewReport() {
		String userloginname = getUserLoginName();
		String dealername = getDealerName();
		List<WyzUser> allSE = repo.getSalesExecutiveofSalesManager(userloginname, getUserLogindealerCode());

		return ok(viewReport.render(dealername, userloginname, Form.form(WyzUser.class), allSE));
	}

	public Result viewReportOfSalesExecutive() {

		String userName = getUserProfile().getId();
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");
		return ok(viewReportOfSE.render(dealerName, userName, Form.form(WyzUser.class)));

	}

	// Report
	@Secure(clients = "FormClient")
	public Result viewReportCREManager() {
		CommonProfile profile = getUserProfile();
		String userName = profile.getId();
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");
		List<WyzUser> allcre = repo.getCREofCREManager(userName, getUserLogindealerCode());
		if (checkCREManagerRole()) {
			return ok(viewReportCREManager.render(dealerName, userName, Form.form(WyzUser.class), allcre));
		} else {

			return forbidden(views.html.error403.render());
		}
	}

	private static CellProcessor[] getUserDataCellProcessor() {
		return new CellProcessor[] { new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional() };
	}

	// Add User Information

	public Result addapp() {
		// List<WyzUser> users = repo.getAllUsers(getUserLogindealerCode());
		List<WyzUser> salesdata = repo.getSalesManagerbyDealerCodeBysuperAdmin();
		List<WyzUser> credata = repo.getAllCreManagersBySuperAdmin();
		List<Dealer> dealerdata = repoDealer.getAllDealer();
		List<WyzUser> users = repo.getAllUsersForSync();
		ArrayList<String> listUsers = new ArrayList<String>();
		for (WyzUser listData : users) {

			listUsers.add(listData.getUserName());
		}

		String userloginname = getUserLoginName();
		String dealername = getDealerName();

		return ok(appUser.render(dealername, userloginname, dealerdata, credata, salesdata, Form.form(WyzUser.class),
				listUsers));
		// return ok(toJson(salesdata));
	}

	// Add User Information
	public Result addApplicationUser() {

		Form<WyzUser> form = Form.form(WyzUser.class).bindFromRequest();
		WyzUser user = form.get();

		String enteredUserName = user.getUserName();
		Form<ListingForm> form1 = Form.form(ListingForm.class).bindFromRequest();
		ListingForm user1 = form1.get();
		String selectedRole = user.getRole();
		String selectedManager = user.getCreManager();
		String selectedDealerCode = user.getDealerCode();
		String selectedDealerName = repoDealer.getDealerCodebyAdmin(selectedDealerCode);

		logger.info("selectedRole: " + selectedRole);
		logger.info("selectedManager : " + selectedManager);
		logger.info("selectedDealerCode : " + selectedDealerCode);
		if ((selectedRole.equals("CREManager")) || (selectedRole.equals("SalesManager"))) {

			WyzUser savedapplication = repo.addWyzUser(user, selectedDealerCode, selectedDealerName, selectedRole,
					selectedManager);

		} else {
			if (selectedRole.equals("SalesExecutive")) {
				String selecteSalesManager = user1.getSingleData();

				WyzUser dataOfMan = repo.getDealerNameByManager(selecteSalesManager);
				String selectedDealerCode1 = dataOfMan.getDealerCode();
				String selectedDealerName1 = dataOfMan.getDealerName();
				WyzUser savedapplication1 = repo.addWyzUser(user, selectedDealerCode1, selectedDealerName1,
						selectedRole, selecteSalesManager);
			} else {

				WyzUser dataOfMan = repo.getDealerNameByManager(selectedManager);
				String selectedDealerCode1 = dataOfMan.getDealerCode();
				String selectedDealerName1 = dataOfMan.getDealerName();
				WyzUser savedapplication1 = repo.addWyzUser(user, selectedDealerCode1, selectedDealerName1,
						selectedRole, selectedManager);

			}

		}
		return redirect(controllers.routes.WyzUserController.addapp());

	}

	// Listing the user info in table

	public Result userInformation() {
		Form<WyzUser> form = Form.form(WyzUser.class).bindFromRequest();
		List<WyzUser> users = repo.getAllUsersForSync();
		String userloginname = getUserLoginName();
		String dealername = getDealerName();
		return ok(userInfo.render(dealername, userloginname, users, Form.form(WyzUser.class)));
	}

	// Editing the information and update
	public Result geteditUser(Long id) {
		Form<WyzUser> form = Form.form(WyzUser.class).bindFromRequest();
		WyzUser user = new WyzUser();
		WyzUser editUser = repo.editUser(id, getUserLogindealerCode());
		String userloginname = getUserLoginName();
		String dealername = getDealerName();

		return ok(edit.render(dealername, userloginname, id, editUser, Form.form(WyzUser.class)));
	}

	public Result postEditUser(Long id) {
		Form<WyzUser> form = Form.form(WyzUser.class).bindFromRequest();
		WyzUser user = form.get();
		WyzUser editdata = repo.editUser(id, getUserLogindealerCode());
		// use list just to return to table again

		WyzUser savedapplication = repo.editedData(id, editdata, getUserLogindealerCode());
		List<WyzUser> users = repo.getAllUsers(getUserLogindealerCode());
		String userloginname = getUserLoginName();
		String dealername = getDealerName();
		return ok(userInfo.render(dealername, userloginname, users, Form.form(WyzUser.class)));

	}

	// Delete the user
	public Result deleteUserData(Long id) {

		WyzUser deletedData = repo.deletUser(id, getUserLogindealerCode());

		List<WyzUser> users = repo.getAllUsers(getUserLogindealerCode());
		String userloginname = getUserLoginName();
		String dealername = getDealerName();
		return ok(userInfo.render(dealername, userloginname, users, Form.form(WyzUser.class)));

	}

	public Result logout() {
		response().setHeader("Cache-Control", "no-cache"); // HTTP 1.1. ,
															// no-store,
															// must-revalidate
		response().setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response().setHeader("Expires", "0"); // Proxies.

		// repo.removeUserFromSession(getUserLoginName());

		applicationLogoutController.logout();
		return redirect(controllers.routes.Application.landingPage());
	}

	// change password for CRE Manager
	public Result changepassword() {

		String userloginname = getUserLoginName();
		String dealername = getDealerName();
		return ok(changePassword.render(dealername, userloginname, Form.form(WyzUser.class)));
	}

	public Result changepasswordediting() {

		String userloginname = getUserLoginName();
		String userLogindealerCode = getUserLogindealerCode();

		// long changingId=repo.changepasswordFunction(userloginname);
		Form<WyzUser> form = Form.form(WyzUser.class).bindFromRequest();
		WyzUser user = form.get();
		String currentPswd = user.getFirstName();
		String newPswd = user.getLastName();
		String confirmPswd = user.getPassword();
		logger.info("currrent Password : " + currentPswd);
		logger.info("New Password : " + newPswd);
		logger.info("Confirm New  Password : " + confirmPswd);
		// logger.info("Changing Password of id with username : "+changingId);
		if (newPswd.equals(confirmPswd)) {

			Boolean state = repo.changepasswordFunction(userLogindealerCode,userloginname, confirmPswd, currentPswd);
			if (state.equals(true)) {
				return redirect(controllers.routes.WyzUserController.indexPageCREManager());
			} else {

				return redirect(controllers.routes.WyzUserController.changepassword());
			}

		} else {

			logger.info("New password doesnt match to Confirm New password");
			return redirect(controllers.routes.WyzUserController.changepassword());
		}

	}

	// change password for CRE

	public Result changepasswordCRE() {

		String userloginname = getUserLoginName();
		String dealername = getDealerName();
		return ok(changePasswordCRE.render(getOEMOfDealer(), getUserLogindealerCode(), dealername, userloginname,
				Form.form(WyzUser.class)));
	}

	public Result changepasswordeditingCRE() {

		String userloginname = getUserLoginName();
		String userLogindealerCode = getUserLogindealerCode();

		// long changingId=repo.changepasswordFunction(userloginname);
		Form<WyzUser> form = Form.form(WyzUser.class).bindFromRequest();
		WyzUser user = form.get();
		String currentPswd = user.getFirstName();
		String newPswd = user.getLastName();
		String confirmPswd = user.getPassword();
		logger.info("currrent Password : " + currentPswd);
		logger.info("New Password : " + newPswd);
		logger.info("Confirm New  Password : " + confirmPswd);
		// logger.info("Changing Password of id with username : "+changingId);
		if (newPswd.equals(confirmPswd)) {

			Boolean state = repo.changepasswordFunction(userLogindealerCode,userloginname, confirmPswd, currentPswd);
			if (state.equals(true)) {
				return redirect(controllers.routes.WyzUserController.indexPageCRE());
			} else {

				return redirect(controllers.routes.WyzUserController.changepasswordCRE());
			}

		} else {

			logger.info("New password doesnt match to Confirm New password");
			return redirect(controllers.routes.WyzUserController.changepasswordCRE());
		}

	}

	// change password for CRE superAdmin

	public Result changepasswordsuperAdmin() {

		String userloginname = getUserLoginName();
		String dealername = getDealerName();
		return ok(changePasswordsuperAdmin.render(dealername, userloginname, Form.form(WyzUser.class)));
	}

	public Result changepasswordeditingsuperAdmin() {

		String userloginname = getUserLoginName();
		String userLogindealerCode = getUserLogindealerCode();

		// long changingId=repo.changepasswordFunction(userloginname);
		Form<WyzUser> form = Form.form(WyzUser.class).bindFromRequest();
		WyzUser user = form.get();
		String currentPswd = user.getFirstName();
		String newPswd = user.getLastName();
		String confirmPswd = user.getPassword();
		logger.info("currrent Password : " + currentPswd);
		logger.info("New Password : " + newPswd);
		logger.info("Confirm New  Password : " + confirmPswd);
		// logger.info("Changing Password of id with username : "+changingId);
		if (newPswd.equals(confirmPswd)) {

			Boolean state = repo.changepasswordFunction(userLogindealerCode,userloginname, confirmPswd, currentPswd);
			if (state.equals(true)) {
				return redirect(controllers.routes.WyzUserController.index());
			} else {

				return redirect(controllers.routes.WyzUserController.changepasswordsuperAdmin());
			}

		} else {

			logger.info("New password doesnt match to Confirm New password");
			return redirect(controllers.routes.WyzUserController.changepasswordsuperAdmin());
		}

	}

	// Change password for Sales Manager

	public Result changepasswordServiceAdvisor() {

		String userloginname = getUserLoginName();
		String dealername = getDealerName();
		return ok(changePasswordServiceAdvisor.render(getUserLogindealerCode(), dealername, userloginname,
				Form.form(WyzUser.class)));
	}

	public Result changepasswordeditingServiceAdvisor() {

		String userloginname = getUserLoginName();
		String userLogindealerCode = getUserLogindealerCode();
		// long changingId=repo.changepasswordFunction(userloginname);
		Form<WyzUser> form = Form.form(WyzUser.class).bindFromRequest();
		WyzUser user = form.get();
		String currentPswd = user.getFirstName();
		String newPswd = user.getLastName();
		String confirmPswd = user.getPassword();
		logger.info("currrent Password : " + currentPswd);
		logger.info("New Password : " + newPswd);
		logger.info("Confirm New  Password : " + confirmPswd);
		// logger.info("Changing Password of id with username : "+changingId);
		if (newPswd.equals(confirmPswd)) {

			Boolean state = repo.changepasswordFunction(userLogindealerCode,userloginname, confirmPswd, currentPswd);
			if (state.equals(true)) {
				return redirect(controllers.routes.WyzUserController.indexPageServiceAdvisor());
			} else {

				return redirect(controllers.routes.WyzUserController.changepasswordServiceAdvisor());
			}

		} else {

			logger.info("New password doesnt match to Confirm New password");
			return redirect(controllers.routes.WyzUserController.changepasswordServiceAdvisor());
		}

	}

	// Change password for sales executive

	private boolean checkCREManagerRole() {

		String role = (String) getUserProfile().getAttribute("USER_ROLE");
		if ("CREManager".equals(role))
			return true;
		else
			return false;
	}

	private boolean checkCRERole() {

		String role = (String) getUserProfile().getAttribute("USER_ROLE");
		if ("CRE".equals(role))
			return true;
		else
			return false;
	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private boolean checkSalesManagerRole() {

		String role = (String) getUserProfile().getAttribute("USER_ROLE");
		if ("SalesManager".equals(role))
			return true;
		else
			return false;
	}

	private boolean checkSalesExecutiveRole() {

		String role = (String) getUserProfile().getAttribute("USER_ROLE");
		if ("SalesExecutive".equals(role))
			return true;
		else
			return false;
	}

	private boolean checkSA_Role() {

		String role = (String) getUserProfile().getAttribute("USER_ROLE");
		if ("Service Advisor".equals(role))
			return true;
		else
			return false;

	}

	private boolean checkOthers_Role() {

		String role = (String) getUserProfile().getAttribute("USER_ROLE");
		if ("CRE".equals(role) || "CREManager".equals(role) || "Service Advisor".equals(role))
			return false;
		else
			return true;

	}

	public Result logoutParticularUser(String userIs) {

		response().setHeader("Cache-Control", "no-cache"); // HTTP 1.1. ,
		// no-store,
		// must-revalidate
		response().setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response().setHeader("Expires", "0"); // Proxies.

		repo.removeUserFromSession(userIs);

		applicationLogoutController.logout();
		return redirect(controllers.routes.Application.landingPage());

	}

	private String getOEMOfDealer() {

		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;

	}

}
