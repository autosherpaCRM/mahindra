package controllers;


import static play.libs.Json.toJson;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import controllers.webmodels.CallLogAjaxLoadInsurance;
import controllers.webmodels.CallLogDispositionLoadInsurance;
import controllers.webmodels.DispositionPage;
import controllers.webmodels.FollowUpNotificationModel;
import controllers.webmodels.InsuranceHistoryData;
import controllers.webmodels.NewAddress;
import models.Address;
import models.AppointmentBooked;
import models.CallDispositionData;
import models.CallInteraction;
import models.Campaign;
import models.Customer;
import models.Insurance;
import models.InsuranceAgent;
import models.InsuranceDisposition;
import models.InsuranceExcelError;
import models.ListingForm;
import models.Location;
import models.PSFAssignedInteraction;
import models.PickupDrop;
import models.SMSTemplate;
import models.SRDisposition;
import models.Service;
import models.ServiceBooked;
import models.ServiceTypes;
import models.ShowRooms;
import models.SpecialOfferMaster;
import models.Vehicle;
import models.Workshop;
import models.WyzUser;
import play.Logger;
import play.data.Form;
import play.libs.Json;
import play.mvc.Result;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.InboundCallInteractionRepository;
import repositories.InsuranceRepository;
import repositories.PSFRepository;
import repositories.SearchRepository;
import repositories.ServiceBookRepository;
import repositories.WyzUserRepository;
import views.html.assignCallBasedOnSelectedRangeInsurance;
import views.html.callDispositionFormServiceDivision;
import views.html.commonDispositionPage;
import views.html.insuranceCallLogPage;
import views.html.serviceInBound;
import views.html.uploadMasterFilesFormInsurance;

public class InsuranceController extends play.mvc.Controller {

	Logger.ALogger logger = play.Logger.of("application");

	private final InsuranceRepository insurRepo;
	private final CallInteractionsRepository call_int_repo;
	private final CallInfoRepository repo;
	private final WyzUserRepository wyzRepo;
	private final InboundCallInteractionRepository inbound_repo;
	private final ServiceBookRepository serviceBookRepo;
	public final SearchRepository searchRepo;
	public final PSFRepository psfRepo;
	private PlaySessionStore playSessionStore;


	@Inject
	public InsuranceController(InsuranceRepository insurRepo, CallInteractionsRepository call_int_repo,
			CallInfoRepository repo, WyzUserRepository wyzRepo, InboundCallInteractionRepository inbound_repo,
			SearchRepository search_Repo,PSFRepository psfRepository,
			org.pac4j.core.config.Config config,PlaySessionStore playSession,ServiceBookRepository serviceRepo) {

		this.insurRepo = insurRepo;
		this.call_int_repo = call_int_repo;
		this.repo = repo;
		this.wyzRepo = wyzRepo;
		this.inbound_repo = inbound_repo;
		searchRepo = search_Repo;
		psfRepo=psfRepository;
		config = (org.pac4j.core.config.Config) config;
		this.playSessionStore=playSession;
		this.serviceBookRepo=serviceRepo;
		
	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private long getUserLoginId() {
		long user_id = (long) getUserProfile().getAttribute("USER_ID");

		return user_id;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}
	
	private String getOEMOfDealer(){
		
		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;
		
	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	@Secure(clients = "FormClient")
	public Result getAllInsuranceAssignedList() {
		List<Campaign> campaignList = insurRepo.getCampaignNamesOfinsurance();

		String toActivateTab1 = "in active";
		String toActivateTab2 = "";
		String toActivateTab3 = "";
		String toActivateTab4 = "";
		String toActivateTab5 = "";

		return ok(insuranceCallLogPage.render(toActivateTab1, toActivateTab2, toActivateTab3, toActivateTab4,
				toActivateTab5, getUserLoginName(), getDealerName(), getUserLogindealerCode(),getOEMOfDealer(),campaignList));
	}

	@Secure(clients = "FormClient")
	public Result getCommonCallDispositionForm(long cid, long vehicle_id, String typeDispo) {
		
		

		String userName = getUserProfile().getId();
		WyzUser userdata = wyzRepo.getUserbyUserName(userName);
		long uniqueid = repo.getUniqueIdForCallInitaiating();

		Customer customerData = inbound_repo.getCustomerInforById(cid);
		Vehicle vehicleData = inbound_repo.getVehicleInformationById(vehicle_id);

		CallInteraction getLatestIntOfVehicle = new CallInteraction();

		if (typeDispo.equals("insurance")) {

			getLatestIntOfVehicle = insurRepo.getLatestInterOfVehicleFromInsurance(vehicle_id);
			
			

		} else {

			getLatestIntOfVehicle = inbound_repo.getLatestInterOfVehicleById(vehicle_id);
		}

		List<Workshop> workshopList = call_int_repo.getWorkshopListByLoca(userdata.getLocation().getName());
		List<Location> locationList = call_int_repo.getLocationList();
		List<ServiceTypes> servicetypeList = call_int_repo.getAllServiceTypeList();

		long countOfServicePresent = call_int_repo.getCountOfServiceHistoryOfVehicle(vehicleData.getVehicle_id());
		logger.info("countOfServicePresent count : " + countOfServicePresent);
		Service latestService = new Service();
		Insurance latestinsurance=new Insurance();
		
		long countOfInsurancePresent=insurRepo.getLatestInsuranceOfVehicle(vehicleData.getVehicle_id());

		if (countOfServicePresent != 0) {

			//latestService = call_int_repo.getLatestServiceData(vehicleData.getVehicle_id());
			//logger.info(" getLatestServiceData : "+latestService.getLastServiceDate());
			latestService=call_int_repo.getLatestServiceDataFiltering(vehicleData.getVehicle_id());
			logger.info(" getLatestServiceDataFiltering : "+latestService.getLastServiceDate());
		}
		

		PSFAssignedInteraction lastPSFAssignStatus=psfRepo.getLastPSFAssignStatusOfVehicle(vehicle_id);
		
		
		
		if(countOfInsurancePresent !=0){
			
			latestinsurance = insurRepo.getLatestInsuranceData(vehicleData.getVehicle_id());
		}

		List<SMSTemplate> templates = repo.getMessageTemplatesForDisplay();
		
		for (SMSTemplate template : templates) {
			if (vehicleData != null) {
				
				if(vehicleData.getVehicleRegNo()!=null){
				template.setSmsTemplate(template.getSmsTemplate().replace("(Vehicle)", vehicleData.getVehicleRegNo()));
				}else{
					
					template.setSmsTemplate(template.getSmsTemplate().replace("(Vehicle)", ""));
				}
				if (vehicleData.getNextServicedate() != null) {

					template.setSmsTemplate(template.getSmsTemplate().replace("(Service Due Date)",
							vehicleData.getNextServicedate().toString()));
				} else {

					template.setSmsTemplate(template.getSmsTemplate().replace("(Service Due Date)", ""));
				}

			} else {
				template.setSmsTemplate(template.getSmsTemplate().replace("(Vehicle)", ""));
			}
			
			
			if(customerData != null){
				if(customerData.getCustomerName() != null){
					template.setSmsTemplate(template.getSmsTemplate().replace("(customer name)",customerData.getCustomerName()));
					
				}
				
			}
			if(userdata != null){
				if(userdata.getPhoneNumber() != null){
					template.setSmsTemplate(template.getSmsTemplate().replace("(phonenumber)",userdata.getPhoneNumber()));
					
				}
				
			}
			template.setSmsTemplate(template.getSmsTemplate().replace("(Dealer)", userdata.getDealerCode()));
		}

		List<String> complaintOFCust = call_int_repo.getComplaintStatusandCount(customerData.getId());
		List<SpecialOfferMaster> offersList = repo.getActiveSpecialOffers();
		List<InsuranceAgent> insurAgent=insurRepo.getAllInsuranceAgents();
		List<String> statesList=insurRepo.getAllDistinctStates();
		
		Collections.sort(statesList);
		
		if (typeDispo.equals("insurance")) {

			logger.info(" Insurance Page : ");
			return ok(commonDispositionPage.render(getOEMOfDealer(),statesList,latestinsurance,uniqueid, getUserLogindealerCode(), getDealerName(), userName,
					customerData, vehicleData, locationList, userdata, latestService, templates, complaintOFCust,
					servicetypeList, getLatestIntOfVehicle, offersList, typeDispo,lastPSFAssignStatus,
					views.html.insuranceOutBoundPage.render(statesList,insurAgent,workshopList, customerData, vehicleData, locationList,
							getLatestIntOfVehicle, servicetypeList, userdata, latestService),
					views.html.insurancePageInbound.render(insurAgent,customerData, vehicleData, locationList,
							getLatestIntOfVehicle, userdata)));
		} else {

			logger.info(" Service Page : ");
			return ok(commonDispositionPage.render(getOEMOfDealer(),statesList,latestinsurance,uniqueid, getUserLogindealerCode(), getDealerName(), userName,
					customerData, vehicleData, locationList, userdata, latestService, templates, complaintOFCust,
					servicetypeList, getLatestIntOfVehicle, offersList, typeDispo,lastPSFAssignStatus,
					views.html.serviceOutBound.render(statesList,workshopList, customerData, vehicleData, locationList,
							getLatestIntOfVehicle, servicetypeList, userdata, latestService),
					serviceInBound.render(workshopList, servicetypeList, customerData, vehicleData, locationList,
							getLatestIntOfVehicle, userdata)));
		}

	}

	public Result postCommonCallDispositionForm() {

		Form<DispositionPage> type_data = Form.form(DispositionPage.class).bindFromRequest();
		DispositionPage pageType = type_data.get();

		String typeOfPageView = pageType.getTypeOfDispoPageView();
		logger.info("Type Of view page for disposition : " + pageType.getTypeOfDispoPageView());

		Form<ListingForm> list_data = Form.form(ListingForm.class).bindFromRequest();
		ListingForm listData = list_data.get();

		String uploadAction = listData.getTypeOfsubmit();
		String userName = getUserProfile().getId();
		String dealername = getDealerName();

		Form<CallInteraction> form = Form.form(CallInteraction.class).bindFromRequest();
		CallInteraction call_interaction = form.get();

		Form<CallDispositionData> disposition_data = Form.form(CallDispositionData.class).bindFromRequest();
		CallDispositionData dispo_data = disposition_data.get();

		if (typeOfPageView.equals("insurance")) {

			if (uploadAction == null || uploadAction.length() == 0) {
				return forbidden(views.html.error403.render());
			} else {

				String action = uploadAction;

				Form<InsuranceDisposition> insur_dispo_data = Form.form(InsuranceDisposition.class).bindFromRequest();
				InsuranceDisposition insurDispo_data = insur_dispo_data.get();

				

				return insuranceDispositionSubmitData(action, insurDispo_data, dispo_data, call_interaction, listData);

			}

		} else if (typeOfPageView.equals("service") || typeOfPageView.equals("serviceSearch")) {

			if (uploadAction == null || uploadAction.length() == 0) {
				return forbidden(views.html.error403.render());
			} else {

				String action = uploadAction;

				Form<SRDisposition> sr_Disposition_data = Form.form(SRDisposition.class).bindFromRequest();
				SRDisposition sr_Disposition = sr_Disposition_data.get();

				return serviceDispositionSubmitData(action, sr_Disposition, dispo_data, call_interaction, listData,
						typeOfPageView);
			}
		} else {

			return forbidden(views.html.error403.render());
		}
	}

	private Result serviceDispositionSubmitData(String action, SRDisposition sr_Disposition,
			CallDispositionData dispo_data, CallInteraction call_interaction, ListingForm listData,
			String typeOfPageView) {

		if ("nonContact".equals(action)) {

			call_interaction.setDroppedCount(call_interaction.getDroppedCount() + 1);

			logger.info("nonContact " + dispo_data.getDisposition());
			repo.addCallInteraction(call_interaction, listData, sr_Disposition, getUserLogindealerCode(), dispo_data);

			return activePageAccordingtoLastDispo(listData.getSelectedFile(), typeOfPageView);
			// return
			// redirect(routes.CallInteractionController.searchByCustomer());

		} else if ("pickUpCancel".equals(action)) {

			logger.info("TYPE OF DISPO " + dispo_data.getDisposition());

			dispo_data.setDisposition("Pick Up Cancelled");

			repo.addCallInteraction(call_interaction, listData, sr_Disposition, getUserLogindealerCode(), dispo_data);

			logger.info("TYPE OF DISPO Booking id" + listData.getServiceBookId());
			serviceBookRepo.cancelPickupOfBooking(Long.valueOf(listData.getServiceBookId()));

			return activePageAccordingtoLastDispo(listData.getSelectedFile(), typeOfPageView);

		} else if ("Confirmed".equals(action)) {

			logger.info("TYPE OF DISPO " + dispo_data.getDisposition());
			repo.addCallInteraction(call_interaction, listData, sr_Disposition, getUserLogindealerCode(), dispo_data);
			serviceBookRepo.confirmStatusOfBooking(Long.valueOf(listData.getServiceBookId()));

			return activePageAccordingtoLastDispo(listData.getSelectedFile(), typeOfPageView);

		} else if ("callMeLater".equals(action)) {

			sr_Disposition.setIsFollowupRequired("Yes");
			sr_Disposition.setIsFollowUpDone("No");
			logger.info("callMeLater " + dispo_data.getDisposition());
			repo.addCallInteraction(call_interaction, listData, sr_Disposition, getUserLogindealerCode(), dispo_data);
			// return
			// redirect(routes.CallInteractionController.searchByCustomer());
			return activePageAccordingtoLastDispo(listData.getSelectedFile(), typeOfPageView);

		} else if ("bookMyservice".equals(action)) {

			Form<ServiceBooked> service_Booked_data = Form.form(ServiceBooked.class).bindFromRequest();
			ServiceBooked service_Booked = service_Booked_data.get();

			Form<PickupDrop> pick_up_data = Form.form(PickupDrop.class).bindFromRequest();
			PickupDrop pick_up = pick_up_data.get();

			logger.info("bookMyservice " + dispo_data.getDisposition());

			repo.addCallInteractionOfServiceBooked(call_interaction, sr_Disposition, listData, service_Booked, pick_up,
					getUserLogindealerCode(), dispo_data);

			// call_int_repo.updateAssignedCallMade(listData.getSingleId());
			repo.addUpsellLead(sr_Disposition, listData, listData.getVehicleId_SB());

			if (listData.getUserfeedback() != null && listData.getUserfeedback().equals("feedback Yes")) {
				repo.sendComplaintSMS(call_interaction);
			}

			// return
			// redirect(routes.CallInteractionController.searchByCustomer());

			return activePageAccordingtoLastDispo(listData.getSelectedFile(), typeOfPageView);
		} else if ("alreadyServiced".equals(action)) {

			logger.info("listData dispo already : " + dispo_data.getDisposition());

			if (sr_Disposition.getServicedAtOtherDealerRadio() != null) {
				if (sr_Disposition.getServicedAtOtherDealerRadio().equals("Non Autorized workshop")) {

					sr_Disposition.setDateOfService(listData.getDateOfServiceNonAuth());
					sr_Disposition.setDealerName(listData.getDealerNameNonAuth());
					sr_Disposition.setServiceType(listData.getServiceTypeNonAuth());

				}
			}
			sr_Disposition.setComplaintOrFB_TagTo(listData.getComplaintOrFB_TagTo1());
			sr_Disposition.setDepartmentForFB(listData.getDepartmentForFB1());
			sr_Disposition.setRemarksOfFB(listData.getRemarksOfFB1());

			logger.info("alreadyServiced " + sr_Disposition.getNoServiceReason());

			repo.addCallInteraction(call_interaction, listData, sr_Disposition, getUserLogindealerCode(), dispo_data);

			repo.addUpsellLead(sr_Disposition, listData, listData.getVehicalId());

			if (dispo_data.getDisposition().equals("Cancelled")) {

				serviceBookRepo.cancelBooking(Long.valueOf(listData.getServiceBookId()));
			}

			logger.info("alreadyServiced " + sr_Disposition.getNoServiceReason());
			// return
			// redirect(routes.CallInteractionController.searchByCustomer());

			return activePageAccordingtoLastDispo(listData.getSelectedFile(), typeOfPageView);

		} else if ("vehicleSold".equals(action)) {

			logger.info("listData vehiclesold : " + dispo_data.getDisposition());

			logger.info("vehicleSold " + sr_Disposition.getNoServiceReason());

			repo.addCallInteraction(call_interaction, listData, sr_Disposition, getUserLogindealerCode(), dispo_data);

			Form<Customer> customerData = Form.form(Customer.class).bindFromRequest();
			Customer newCustomer = customerData.get();

			Form<Address> addressData = Form.form(Address.class).bindFromRequest();
			Address newCustomerAdress = addressData.get();

			call_int_repo.addNewCustomerInformation(0, sr_Disposition.getId(), newCustomer, listData,
					newCustomerAdress);

			Form<Vehicle> vehicleData = Form.form(Vehicle.class).bindFromRequest();
			Vehicle newVehicle = vehicleData.get();

			call_int_repo.addNewVehicleInformation(newVehicle, listData);

			if (dispo_data.getDisposition().equals("Cancelled")) {

				serviceBookRepo.cancelBooking(Long.valueOf(listData.getServiceBookId()));
			}

			// return
			// redirect(routes.CallInteractionController.searchByCustomer());

			return activePageAccordingtoLastDispo(listData.getSelectedFile(), typeOfPageView);

		} else if ("dissatifiedwithPreviousService".equals(action)) {

			logger.info("listData dissatifiedwithPreviousService : " + dispo_data.getDisposition());

			if (sr_Disposition.getNoServiceReason().equals("Dissatisfied with Insurance")) {

				sr_Disposition.setNoServiceReasonTaggedTo(listData.getNoServiceReasonTaggedTo1());
				sr_Disposition.setNoServiceReasonTaggedToComments(listData.getNoServiceReasonTaggedToComments1());

			}

			repo.addCallInteraction(call_interaction, listData, sr_Disposition, getUserLogindealerCode(), dispo_data);
			logger.info("dissatifiedwithPreviousService " + sr_Disposition.getNoServiceReason());

			if (dispo_data.getDisposition().equals("Cancelled")) {

				serviceBookRepo.cancelBooking(Long.valueOf(listData.getServiceBookId()));
			}

			repo.sendComplaintSMS(call_interaction);

			// return
			// redirect(routes.CallInteractionController.searchByCustomer());

			return activePageAccordingtoLastDispo(listData.getSelectedFile(), typeOfPageView);

		} else if ("inBoundCallSubmit".equals(action)) {

			Form<ServiceBooked> service_Booked_data = Form.form(ServiceBooked.class).bindFromRequest();
			ServiceBooked service_Booked = service_Booked_data.get();

			Form<PickupDrop> pick_up_data = Form.form(PickupDrop.class).bindFromRequest();
			PickupDrop pick_up = pick_up_data.get();

			logger.info("inBoundCallSubmit is: " + action);

			sr_Disposition.setServiceType(listData.getServiceTypeAltId());
			sr_Disposition.setComplaintOrFB_TagTo(listData.getComplaintOrFB_TagTo2());
			sr_Disposition.setDepartmentForFB(listData.getDepartmentForFB2());
			sr_Disposition.setRemarksOfFB(listData.getRemarksOfFB2());

			dispo_data.setDisposition(listData.getContactType());
			listData.setVehicleId_SB(listData.getVehicleIn());
			listData.setWorkshopId(listData.getWorkshopIn());
			listData.setServiceScheduledDate(listData.getServiceScheduledDateIn());
			listData.setServiceScheduledTime(listData.getServiceScheduledTimeIn());
			// listData.setDriverId(listData.getDriverIdSelectIn());
			// listData.setTime_From(listData.getTime_FromIn());
			// listData.setTime_To(listData.getTime_ToIn());
			listData.setServiceAdvisorId(listData.getServiceAdvisorIdIn());
			service_Booked.setTypeOfPickup(listData.getTypeOfPickupIn());
			service_Booked.setServiceBookedType(listData.getServiceTypeAltId());
			service_Booked.setServiceBookingAddress(listData.getPickUpAddressIn());

			pick_up.setPickUpAddress(listData.getPickUpAddressIn());

			repo.addCallInteractionOfServiceBooked(call_interaction, sr_Disposition, listData, service_Booked, pick_up,
					getUserLogindealerCode(), dispo_data);

			repo.addUpsellLead(sr_Disposition, listData, listData.getVehicleId_SB());

			// return
			// redirect(routes.CallInteractionController.searchByCustomer());

			return activePageAccordingtoLastDispo(listData.getSelectedFile(), typeOfPageView);

		} else {

			return forbidden(views.html.error403.render());

		}

	}


	private Result activePageAccordingtoLastDispo(String lastDisposition, String typeOfPageView) {

		if (typeOfPageView.equals("serviceSearch")) {

			return redirect(routes.CallInteractionController.searchByCustomer());

		} else {
			String toActivateTab1 = "";
			String toActivateTab2 = "";
			String toActivateTab3 = "";
			String toActivateTab4 = "";
			String toActivateTab5 = "";

			if (lastDisposition.equals("Call Me Later")) {

				toActivateTab1 = "";
				toActivateTab2 = "in active";
				toActivateTab3 = "";
				toActivateTab4 = "";
				toActivateTab5 = "";
			} else if (lastDisposition.equals("Book My Service")) {
				toActivateTab1 = "";
				toActivateTab2 = "";
				toActivateTab3 = "in active";
				toActivateTab4 = "";
				toActivateTab5 = "";

			} else if (lastDisposition.equals("Service Not Required")) {

				toActivateTab1 = "";
				toActivateTab2 = "";
				toActivateTab3 = "";
				toActivateTab4 = "in active";
				toActivateTab5 = "";
			} else if (lastDisposition.equals("") || lastDisposition.equals(null)) {

				toActivateTab1 = "in active";
				toActivateTab2 = "";
				toActivateTab3 = "";
				toActivateTab4 = "";
				toActivateTab5 = "";

			} else {
				toActivateTab1 = "";
				toActivateTab2 = "";
				toActivateTab3 = "";
				toActivateTab4 = "";
				toActivateTab5 = "in active";

			}

			List<Campaign> campaignList=new ArrayList<Campaign>();
			List<Campaign> campaignListCampign = call_int_repo.getCampaignNamesBYType("Campaign");
			List<Campaign> campaignListremainder = call_int_repo.getCampaignNamesBYType("Service Reminder");
			campaignList.addAll(campaignListCampign);
			campaignList.addAll(campaignListremainder);
			List<ServiceTypes> serviceTypeList = call_int_repo.getAllServiceTypeList();
			List<String> bookedServiceTypeList = searchRepo.getBookedServiceType();
			List<CallDispositionData> newlistDispo = wyzRepo.getDispositionListOfNoncontacts();
			return ok(callDispositionFormServiceDivision.render(toActivateTab1, toActivateTab2, toActivateTab3,
			toActivateTab4, toActivateTab5, getUserLogindealerCode(), getDealerName(), 
			getUserLoginName(),getOEMOfDealer(),campaignList,serviceTypeList,bookedServiceTypeList,newlistDispo));
		}
	}

	// Insurance Form submit data
	private Result insuranceDispositionSubmitData(String action, InsuranceDisposition insurDispo_data,
			CallDispositionData dispo_data, CallInteraction call_interaction, ListingForm listData) {

		if ("nonContact".equals(action)) {

			logger.info("nonContact");
			call_interaction.setDroppedCount(call_interaction.getDroppedCount() + 1);
			insurRepo.addInsurCallInter(listData, call_interaction, dispo_data, insurDispo_data,
					getUserLogindealerCode());

			return activePageAccordingtoLastDispoOfInsurance(listData.getSelectedFile());

			// return
			// redirect(controllers.routes.InsuranceController.getAllInsuranceAssignedList());

		} else if ("callMeLater".equals(action)) {

			logger.info("callMeLater");

			insurDispo_data.setIsFollowupRequired("Yes");
			insurDispo_data.setIsFollowUpDone("No");

			logger.info("insurDispo_data followup date : " + insurDispo_data.getFollowUpDate());
			logger.info("insurDispo_data followup Time : " + insurDispo_data.getFollowUpTime());

			insurRepo.addInsurCallInter(listData, call_interaction, dispo_data, insurDispo_data,
					getUserLogindealerCode());

			// return
			// redirect(controllers.routes.InsuranceController.getAllInsuranceAssignedList());

			return activePageAccordingtoLastDispoOfInsurance(listData.getSelectedFile());

		} else if ("bookMyAppointment".equals(action)) {

			logger.info("bookMyAppointment");

			
			
			Form<AppointmentBooked> appointment_data = Form.form(AppointmentBooked.class).bindFromRequest();
			AppointmentBooked bookAppointment = appointment_data.get();
			
			Form<ShowRooms> showroom_data = Form.form(ShowRooms.class).bindFromRequest();
			ShowRooms showroomData = showroom_data.get();

			/*if(bookAppointment.getTypeOfPickup().equals("Home Visit")){
				
				bookAppointment.setAppointmentFromTime(listData.getAppointmentFromTimeHomevisit());
				bookAppointment.setAppointmentToTime(listData.getAppointmentToTimeHomeVisit());
			}*/
			
			Form<Insurance> insurance_data = Form.form(Insurance.class).bindFromRequest();
			Insurance insuranceData = insurance_data.get();
			
			Form<NewAddress> newAddress = Form.form(NewAddress.class).bindFromRequest();
			NewAddress newAdressData = newAddress.get();
			
			if(newAdressData.getAddress1New()!=null && newAdressData.getAddress1New()!=""){
				
				bookAppointment.setAddressOfVisit(newAdressData.getAddress1New()+","+newAdressData.getAddress2New()
				+","+newAdressData.cityNew+","+newAdressData.getStateNew()+","+newAdressData.getPincodeNew());
				
			}
			
			insurRepo.addBookAppointmentOfInsurance(listData, call_interaction, dispo_data, insurDispo_data,
					bookAppointment, getUserLogindealerCode(),showroomData,insuranceData);

			insurRepo.addUpsellLeadOfInsurance(insurDispo_data, listData, listData.getVehicleId_SB());

			// return
			// redirect(controllers.routes.InsuranceController.getAllInsuranceAssignedList());
			
			
			
			
			insurRepo.addNewAddressToCustomer(newAdressData,listData.getCustomer_Id());



			return activePageAccordingtoLastDispoOfInsurance(listData.getSelectedFile());
		} else if ("alreadyRenewed".equals(action)) {

			logger.info("alreadyRenewed");

			if (insurDispo_data.getTypeOfAutherization() != null) {

				if (insurDispo_data.getTypeOfAutherization().equals("OEM Authorized Dealer")) {

					insurDispo_data.setInsuranceProvidedBy(listData.getInsuranceProvidedByOEM());

				} else if (insurDispo_data.getTypeOfAutherization().equals("Unauthorized Dealer")) {

					insurDispo_data.setInsuranceProvidedBy(listData.getInsuranceProvidedUnAuth());
					insurDispo_data.setDateOfRenewal(listData.getDateOfRenewalNonAuth());
				}

			}

			insurDispo_data.setComplaintOrFB_TagTo(listData.getComplaintOrFB_TagTo1());
			insurDispo_data.setDepartmentForFB(listData.getDepartmentForFB1());
			insurDispo_data.setRemarksOfFB(listData.getRemarksOfFB1());

			insurRepo.addInsurCallInter(listData, call_interaction, dispo_data, insurDispo_data,
					getUserLogindealerCode());

			insurRepo.addUpsellLeadOfInsurance(insurDispo_data, listData, listData.getVehicalId());

			// return
			// redirect(controllers.routes.InsuranceController.getAllInsuranceAssignedList());

			return activePageAccordingtoLastDispoOfInsurance(listData.getSelectedFile());

		} else if ("vehicleSold".equals(action)) {

			logger.info("vehicleSold");

			insurRepo.addInsurCallInter(listData, call_interaction, dispo_data, insurDispo_data,
					getUserLogindealerCode());

			Form<Customer> customerData = Form.form(Customer.class).bindFromRequest();
			Customer newCustomer = customerData.get();
			
			Form<Address> addressData = Form.form(Address.class).bindFromRequest();
			Address newCustomerAdress = addressData.get();


			call_int_repo.addNewCustomerInformation(insurDispo_data.getId(),0,newCustomer, listData,newCustomerAdress);

			Form<Vehicle> vehicleData = Form.form(Vehicle.class).bindFromRequest();
			Vehicle newVehicle = vehicleData.get();

			call_int_repo.addNewVehicleInformation(newVehicle, listData);

			// return
			// redirect(controllers.routes.InsuranceController.getAllInsuranceAssignedList());

			return activePageAccordingtoLastDispoOfInsurance(listData.getSelectedFile());

		} else if ("dissatifiedwithPreviousService".equals(action)) {

			logger.info("dissatifiedwithPreviousService");

			if (insurDispo_data.getRenewalNotRequiredReason().equals("Dissatisfied with Insurance")) {

				insurDispo_data.setNoServiceReasonTaggedTo(listData.getNoServiceReasonTaggedTo1());
				insurDispo_data.setNoServiceReasonTaggedToComments(listData.getNoServiceReasonTaggedToComments1());

			}

			insurRepo.addInsurCallInter(listData, call_interaction, dispo_data, insurDispo_data,
					getUserLogindealerCode());

			// return
			// redirect(controllers.routes.InsuranceController.getAllInsuranceAssignedList());

			return activePageAccordingtoLastDispoOfInsurance(listData.getSelectedFile());

		} else if ("inBoundCallSubmit".equals(action)) {

			logger.info("inBoundCallSubmit");

			Form<AppointmentBooked> appointment_data = Form.form(AppointmentBooked.class).bindFromRequest();
			AppointmentBooked bookAppointment = appointment_data.get();

			insurDispo_data.setComplaintOrFB_TagTo(listData.getComplaintOrFB_TagTo2());
			insurDispo_data.setDepartmentForFB(listData.getDepartmentForFB2());
			insurDispo_data.setRemarksOfFB(listData.getRemarksOfFB2());
			insurDispo_data.setCityName(listData.getCityIn());

			dispo_data.setDisposition(listData.getContactType());

			listData.setVehicleId_SB(listData.getVehicleIn());

			bookAppointment.setAppointmentDate(listData.getAppointmentScheduledDateIn());
			bookAppointment.setAppointmentFromTime(listData.getAppointmentFromTimeIn());
			
			/*if(listData.getTypeOfPickupIn().equals("Showroom Visit")){
			bookAppointment.setAppointmentFromTime(listData.getAppointmentFromTimeIn());
			bookAppointment.setAppointmentToTime(listData.getAppointmentToTimeIn());
			}else{
			
				bookAppointment.setAppointmentFromTime(listData.getAppointmentFromTimeHomevisitIn());
				bookAppointment.setAppointmentToTime(listData.getAppointmentToTimeHomeVisitIn());
			}*/
			bookAppointment.setPremiumYes(listData.getPremiumYesInB());
			bookAppointment.setTypeOfPickup(listData.getTypeOfPickupIn());
			bookAppointment.setAddressOfVisit(listData.getPickUpAddressIn());
			bookAppointment.setRenewalMode(listData.getRenewalModeIn());
			bookAppointment.setRenewalType(listData.getRenewalTypeIn());
			bookAppointment.setPremiumwithTax(listData.getPremiumwithTaxIn());
			bookAppointment.setInsuranceCompany(listData.getInsuranceCompanyIn());
			bookAppointment.setDsa(listData.getDsaIn());
			
			bookAppointment.setNomineeAge(listData.getNomineeAgeIn());
			bookAppointment.setNomineeName(listData.getNomineeNameIn());
			bookAppointment.setNomineeRelationWithOwner(listData.getNomineeRelationWithOwnerIn());
			bookAppointment.setAppointeeName(listData.getAppointeeNameIn());
			bookAppointment.setInsuranceAgentData(listData.getInsuranceAgentDataIn());
			//logger.info("listData of insurance premium : "+listData.getPremiumwithTaxIn());
			

			Form<ShowRooms> showroom_data = Form.form(ShowRooms.class).bindFromRequest();
			ShowRooms showroomData = showroom_data.get();

			showroomData.setShowRoom_id(listData.getShowRoom_idIn());
			
			
			Form<Insurance> insurance_data = Form.form(Insurance.class).bindFromRequest();
			Insurance insuranceData = insurance_data.get();
			
			insurRepo.addBookAppointmentOfInsurance(listData, call_interaction, dispo_data, insurDispo_data,
					bookAppointment, getUserLogindealerCode(),showroomData,insuranceData);

			insurRepo.addUpsellLeadOfInsurance(insurDispo_data, listData, listData.getVehicleId_SB());
			
			return activePageAccordingtoLastDispoOfInsurance(listData.getSelectedFile());

		} else {

			return forbidden(views.html.error403.render());

		}

	}

	private Result activePageAccordingtoLastDispoOfInsurance(String lastDisposition) {

		List<Campaign> campaignList = insurRepo.getCampaignNamesOfinsurance();

		logger.info("insurance lastDisposition : " + lastDisposition);
		String toActivateTab1 = "";
		String toActivateTab2 = "";
		String toActivateTab3 = "";
		String toActivateTab4 = "";
		String toActivateTab5 = "";

		if (lastDisposition.equals("Call Me Later")) {

			toActivateTab1 = "";
			toActivateTab2 = "in active";
			toActivateTab3 = "";
			toActivateTab4 = "";
			toActivateTab5 = "";
		} else if (lastDisposition.equals("Book Appointment")) {
			toActivateTab1 = "";
			toActivateTab2 = "";
			toActivateTab3 = "in active";
			toActivateTab4 = "";
			toActivateTab5 = "";

		} else if (lastDisposition.equals("Renewal Not Required")) {

			toActivateTab1 = "";
			toActivateTab2 = "";
			toActivateTab3 = "";
			toActivateTab4 = "in active";
			toActivateTab5 = "";
		} else if (lastDisposition.equals("") || lastDisposition.equals(null)) {

			toActivateTab1 = "in active";
			toActivateTab2 = "";
			toActivateTab3 = "";
			toActivateTab4 = "";
			toActivateTab5 = "";

		} else {
			toActivateTab1 = "";
			toActivateTab2 = "";
			toActivateTab3 = "";
			toActivateTab4 = "";
			toActivateTab5 = "in active";

		}

		return ok(insuranceCallLogPage.render(toActivateTab1, toActivateTab2, toActivateTab3, toActivateTab4,
				toActivateTab5, getUserLoginName(), getDealerName(), getUserLogindealerCode(),getOEMOfDealer(),campaignList));

	}

	@Secure(clients = "FormClient")
	public Result getAssignedInsuInteraction() {

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String fromDueDate = "";
		String toDueDate = "";
		String campaignType="";
		String renewalType ="";
		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}
		if (paramMap.get("columns[1][search][value]") != null) {
			fromDueDate = paramMap.get("columns[1][search][value]")[0];
			logger.info("inside from due date:"+fromDueDate);
		}

		if (paramMap.get("columns[3][search][value]") != null) {
			campaignType = paramMap.get("columns[3][search][value]")[0];
			if (campaignType.equals("0")) {
				campaignType = "";
			}
		}
		if (paramMap.get("columns[4][search][value]") != null) {
			renewalType = paramMap.get("columns[4][search][value]")[0];
			logger.info("inside renewalType:"+renewalType);
			if (renewalType.equals("0")) {
				renewalType = "";
			}
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDueDate = paramMap.get("columns[2][search][value]")[0];
		}
		logger.info("searchPattern :" + searchPattern);
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		
		long totalSize = insurRepo.getAllInsuranceAssignedInteraction(userdata.getId(),campaignType,fromDueDate,toDueDate,renewalType,searchPattern);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		List<CallLogAjaxLoadInsurance> assignedList = insurRepo.assignedInsuranceListOfUser(userdata.getId(),
				campaignType,fromDueDate,toDueDate,renewalType,searchPattern,
				fromIndex, toIndex);

		if (!allflag) {
			patternCount = assignedList.size();
			logger.info("patternCount of assignedList : " + patternCount);

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogAjaxLoadInsurance c : assignedList) {

			String urlDisposition = "<a href='/CRE/getDispositionFormPage/" + c.getCustomer_id() + "/"
					+ c.getVehicle_vehicle_id()
					+ "/insurance'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";

			ObjectNode row = Json.newObject();

			String dueDatais = "";
			if (c.getDuedate() != null) {

				dueDatais = c.getDuedate().toString();

			}
			
			row.put("0", urlDisposition);
			row.put("1", c.getCampaign());
			row.put("2", c.getCustomerName());
			row.put("3", c.getPhone());
			row.put("4", c.getRegNo());
			row.put("5", c.getChassisNo());
			row.put("6", c.getPolicyDueMonth());
			row.put("7", String.valueOf(c.getPolicyDueDate()));
			row.put("8", c.getNextRenewalType());
			row.put("9", c.getLastInsuranceCompany());
			row.put("10", urlDisposition);
			an.add(row);
		}

		return ok(result);
	}

	@Secure(clients = "FormClient")
	public Result getContactedDispoFormData(long typeOfdispo) {
		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String fromDueDate = "";
		String toDueDate = "";
		String campaignType="";
		String renewalType ="";
		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}
		if (paramMap.get("columns[1][search][value]") != null) {
			fromDueDate = paramMap.get("columns[1][search][value]")[0];
			logger.info("inside from due date:"+fromDueDate);
		}

		if (paramMap.get("columns[3][search][value]") != null) {
			campaignType = paramMap.get("columns[3][search][value]")[0];
			if (campaignType.equals("0")) {
				campaignType = "";
			}
		}
		if (paramMap.get("columns[4][search][value]") != null) {
			renewalType = paramMap.get("columns[4][search][value]")[0];
			logger.info("inside renewalType:"+renewalType);
			if (renewalType.equals("0")) {
				renewalType = "";
			}
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDueDate = paramMap.get("columns[2][search][value]")[0];
		}
		logger.info("searchPattern :" + searchPattern);
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);


		long totalSize = insurRepo.getAllInsuranceDispoInter(fromDueDate,toDueDate,campaignType,renewalType,searchPattern,userdata.getId(), typeOfdispo);
		logger.info("totalSize of Dispo interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		List<CallLogDispositionLoadInsurance> InteracList = insurRepo.dispoInsuranceListOfUser(fromDueDate,toDueDate,campaignType,renewalType,searchPattern,
				userdata.getId(), typeOfdispo, fromIndex, toIndex);

		if (!allflag) {
			patternCount = InteracList.size();
			logger.info("patternCount of dispo : " + patternCount);

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogDispositionLoadInsurance c : InteracList) {

			String urlDisposition = "<a href='/CRE/getDispositionFormPage/" + c.getCustomer_id() + "/"
					+ c.getVehicle_id()
					+ "/insurance'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";

			ObjectNode row = Json.newObject();
			
			logger.info("number : "+c.getMobile_number());
			
			String dueDatais = "";
			if (c.getPolicyDueDate() != null) {

				dueDatais = c.getPolicyDueDate().toString();

			}
			if (typeOfdispo == 25) {
				row.put("0", urlDisposition);
				row.put("1", c.getCampaignName());
				row.put("2", String.valueOf(c.getCall_date()));
				row.put("3", c.getCustomer_name());
				row.put("4", c.getMobile_number());
				row.put("5", c.getVehicle_RegNo());
				row.put("6", dueDatais);
				row.put("7", c.getRenewalType());
				row.put("8", String.valueOf(c.getAppointmentDate()));
				row.put("9", c.getAppointmentTime());
				row.put("10", c.getInsurance_agent());
				row.put("11", c.getAppointmentstatus());
				row.put("12", c.getLast_disposition());
				row.put("13", urlDisposition);
			} else if (typeOfdispo == 26) {
				row.put("0", urlDisposition);
				row.put("1", c.getCampaignName());
				row.put("2", String.valueOf(c.getCall_date()));
				row.put("3", c.getCustomer_name());
				row.put("4", c.getMobile_number());
				row.put("5", c.getVehicle_RegNo());
				row.put("6", dueDatais);
				row.put("7", c.getRenewalType());
				row.put("8", c.getLast_disposition());
				row.put("9", c.getReason());
				row.put("10", urlDisposition);
			} else if (typeOfdispo == 4) {
				row.put("0", urlDisposition);
				row.put("1", c.getCampaignName());
				row.put("2", String.valueOf(c.getCall_date()));
				row.put("3", c.getCustomer_name());
				row.put("4", c.getMobile_number());
				row.put("5", c.getVehicle_RegNo());
				row.put("6", dueDatais);
				row.put("7", c.getRenewalType());
				row.put("8", c.getFollowUpDate());
				row.put("9", c.getFollowUpTime());
				row.put("10", "");
				row.put("11", c.getLast_disposition());
				row.put("12", urlDisposition);
			}

			an.add(row);
		}

		return ok(result);
	}

	@Secure(clients = "FormClient")
	public Result getNonContactDroppedInsuranceDispoData(long typeOfdispo) {

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String fromDueDate = "";
		String toDueDate = "";
		String campaignType="";
		String renewalType ="";
		String lastdispo ="";
		String droppedcount="";
		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}
		if (paramMap.get("columns[1][search][value]") != null) {
			fromDueDate = paramMap.get("columns[1][search][value]")[0];
			logger.info("inside from due date:"+fromDueDate);
		}

		if (paramMap.get("columns[3][search][value]") != null) {
			campaignType = paramMap.get("columns[3][search][value]")[0];
			if (campaignType.equals("0")) {
				campaignType = "";
			}
		}
		if (paramMap.get("columns[4][search][value]") != null) {
			renewalType = paramMap.get("columns[4][search][value]")[0];
			logger.info("inside renewalType:"+renewalType);
			if (renewalType.equals("0")) {
				renewalType = "";
			}
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDueDate = paramMap.get("columns[2][search][value]")[0];
		}
		logger.info("searchPattern :" + searchPattern);
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = insurRepo.getAllInsuranceNonContactInter(fromDueDate,toDueDate,campaignType,renewalType,searchPattern,userdata.getId(),typeOfdispo,droppedcount,lastdispo);
		logger.info("totalSize of Dispo interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		List<CallLogDispositionLoadInsurance> InteracList = insurRepo.nonContactInsuranceListOfUser(fromDueDate,toDueDate,campaignType,renewalType,searchPattern,userdata.getId(),typeOfdispo,droppedcount,lastdispo, fromIndex, toIndex);

		if (!allflag) {
			patternCount = InteracList.size();
			logger.info("patternCount of dispo : " + patternCount);

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogDispositionLoadInsurance c : InteracList) {

			String urlDisposition = "<a href='/CRE/getDispositionFormPage/" + c.getCustomer_id() + "/"
					+ c.getVehicle_id()
					+ "/insurance'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";

			ObjectNode row = Json.newObject();
			
			String dueDatais = "";
			if (c.getPolicyDueDate() != null) {

				dueDatais = c.getPolicyDueDate().toString();

			}

			if (typeOfdispo != 2) {
				row.put("0", urlDisposition);
				row.put("1", c.getCampaignName());
				row.put("2", String.valueOf(c.getCall_date()));
				row.put("3", c.getCustomer_name());
				row.put("4", c.getMobile_number());
				row.put("5", c.getVehicle_RegNo());
				row.put("6", dueDatais);
				row.put("7", c.getLast_disposition());
				row.put("8", urlDisposition);
			} else {
				row.put("0", urlDisposition);
				row.put("1", c.getCampaignName());
				row.put("2", String.valueOf(c.getCall_date()));
				row.put("3", c.getCustomer_name());
				row.put("4", c.getMobile_number());
				row.put("5", c.getVehicle_RegNo());
				row.put("6", dueDatais);
				row.put("7", c.getLast_disposition());
				row.put("8",c.getNo_of_attempts());
				row.put("9", urlDisposition);

			}

			an.add(row);
		}

		return ok(result);
	}

	// upload Insurance

	@Secure(clients = "FormClient")
	public Result upload_Excel_Sheet_insurance(String uploadId, String uploadType, String resultDataCount) {
		List<Location> locationList = call_int_repo.getLocationList();
		List<Campaign> campaignList = insurRepo.getCampaignNamesOfinsurance();
		List<Integer> resultdata = new ArrayList();
		if (resultDataCount.equals("0")) {

		} else {

			List<String> selectedList = Arrays.asList(resultDataCount.split(","));
			resultdata.add(Integer.parseInt(selectedList.get(0)));
			resultdata.add(Integer.parseInt(selectedList.get(1)));

		}

		logger.info("campaign names list:" + campaignList.size());
		return ok(uploadMasterFilesFormInsurance.render(resultdata, uploadId, uploadType, campaignList, locationList,
				getUserLogindealerCode(), getUserLoginName()));
	}

	@Secure(clients = "FormClient")
	public Result getPageForAssigningCallsInsurance() {
		// List<String> cresList =
		// call_int_repo.getCREofCREManager(getUserLoginName(),
		// getUserLogindealerCode());

		List<String> cresList = new ArrayList<String>();
		List<CallLogAjaxLoadInsurance > scheduleData = new ArrayList<CallLogAjaxLoadInsurance >();

		String fromDate = "";
		String toDate = "";
		String cityName="";
		
		long campaignTypeCat = 0;
		long campaignNameCat = 0;
		long workshopId=0;
		boolean isRangeSelected = false;
		boolean toAssigncall = true;

		List<Campaign> campaign_list = insurRepo.getCampainListOfInsurance();
		List<Campaign> campaignList = insurRepo.getCampaignNamesOfinsurance();
		List<Location> locationList = call_int_repo.getLocationList();

		return ok(assignCallBasedOnSelectedRangeInsurance.render(cityName,workshopId,campaignNameCat, campaignTypeCat, fromDate, toDate, isRangeSelected, toAssigncall, cresList, scheduleData, getUserLoginName(),
				getDealerName(), campaign_list, locationList, campaignList));
	}

	@Secure(clients = "FormClient")
	public Result postAssignCallInsurance() {
		Form<ListingForm> data_range = Form.form(ListingForm.class).bindFromRequest();
		ListingForm range = data_range.get();

		Form<Campaign> campaign_data = Form.form(Campaign.class).bindFromRequest();
		Campaign campaigninfo = campaign_data.get();

		String uploadAction = range.getSelectedFile();

		if (uploadAction == null || uploadAction.length() == 0) {
			return forbidden(views.html.error403.render());
		} else {

			String action = uploadAction;
			logger.info("uploadAction[0] first id value : " + action);
			if ("viewAssignCalls".equals(action)) {
				String fromDate = range.getSingleData();
				String toDate = range.getTranferData();
				long campaignTypeCat = range.getCampaignType();
				long campaignNameCat = range.getCampaignName();

				String cityName = range.getCityName();
				
				String cityId = insurRepo.getLocationIdByName(cityName);

				long workshopName = range.getWorkshopId();

				logger.info("campaignTypeCat : " + campaignTypeCat + "campaignNameCat : " + campaignNameCat+"cityName : "+cityName);

				List<String> cresList = repo.getCREUserNameofCREManagerByLocation(getUserLoginName(),
						getUserLogindealerCode(), cityName, workshopName);

				SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date fromDateNew = new Date();
				Date toDateNew = new Date();
				try {
					fromDateNew = dmyFormat.parse(fromDate);
					toDateNew = dmyFormat.parse(toDate);
				} catch (Exception e) {
					e.printStackTrace();
				}
				logger.info("fromDateNew  is : " + fromDateNew);
				logger.info("toDateNew selected type date :" + toDateNew);
				List<CallLogAjaxLoadInsurance> scheduleData = new ArrayList<CallLogAjaxLoadInsurance>();

				String selectedCampaignType = call_int_repo.getCampaignTypeById(campaignTypeCat);

				logger.info("selectedCampaignType  is : " + selectedCampaignType);
				logger.info("selectedCampaignType :" + selectedCampaignType);
				scheduleData=insurRepo.getAssignedListOfInsurance(fromDateNew,toDateNew,campaignTypeCat,
						campaignNameCat,cityId,workshopName);
				
				// Add procedure and get the list and save in scheduled data

				logger.info("list of vehicles to be assigned is :" + scheduleData.size());
				boolean isRangeSelected = false;
				boolean toAssigncall = false;
				if (scheduleData.size() > 0) {
					isRangeSelected = true;
					toAssigncall = false;
				} else {
					isRangeSelected = false;
					toAssigncall = true;
				}
				List<Campaign> campaign_list = insurRepo.getCampainListOfInsurance();

				List<Campaign> campaignList = insurRepo.getCampaignNamesOfinsurance();

				List<Location> locationList = call_int_repo.getLocationList();

				return ok(assignCallBasedOnSelectedRangeInsurance.render(cityName,workshopName,campaignNameCat, campaignTypeCat, fromDate, toDate, isRangeSelected, toAssigncall, cresList, scheduleData,
						getUserLoginName(), getDealerName(), campaign_list, locationList, campaignList));

			} else if ("assignCalls".equals(action)) {
				
				logger.info("Assign calls");
				
				List<String> selectedList = range.getData();
				for (String s : selectedList) {
					logger.info("s user :" + s);

				}

				String fromDate = range.getFromDateToAss();
				String toDate = range.getToDateToAss();
				long campaignCat = range.getCampaignTypeToAss();
				long campaignNameCat = range.getCampaignNameToAss();

				logger.info("campaignNameCat sub id in assign button:" + campaignNameCat);
				long campaignTypeCat = range.getCampaignType();

				String cityName = range.getCityNameToAss();
				long workshopName = range.getWorkshopToAss();
				
				String cityId = insurRepo.getLocationIdByName(cityName);

				boolean isRangeSelected = false;
				boolean toAssigncall = true;
				List<String> cresList = repo.getCREUserNameofCREManager(getUserLoginName(), getUserLogindealerCode());
				SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date fromDateNew = new Date();
				Date toDateNew = new Date();
				try {
					fromDateNew = dmyFormat.parse(fromDate);
					toDateNew = dmyFormat.parse(toDate);
				} catch (Exception e) {
					e.printStackTrace();
				}

				List<CallLogAjaxLoadInsurance> scheduleData = new ArrayList<CallLogAjaxLoadInsurance>();

				String selectedCampaignType = call_int_repo.getCampaignTypeById(campaignCat);

				logger.info("selectedCampaignType :" + selectedCampaignType);

				// Add procedure and get the list and save in scheduled data

				scheduleData=insurRepo.getAssignedListOfInsurance(fromDateNew,toDateNew,campaignTypeCat,
						campaignNameCat,cityId,workshopName);
				
				
				int scheduledCallCount = scheduleData.size();
				
				logger.info("To Assign count : "+scheduledCallCount);
				

				int creSize = selectedList.size();

				// Insurance assignment

				if (creSize != 0) {
					logger.info("count of CRE :" + creSize);

					int divCountRemainder = scheduledCallCount % creSize;

					logger.info("divCountRemainder of CRE and call from CRE Manager:" + divCountRemainder);

					int divCount = (scheduledCallCount) / (creSize);
					logger.info("divCount of CRE and call :" + divCount);

					Collections.shuffle(scheduleData);

					int calls_count = 0;
					for (int i = 0; i < creSize; i++) {

						for (int j = calls_count; j < divCount + calls_count; j++) {

							String userdata = selectedList.get(i);
							CallLogAjaxLoadInsurance  scall = scheduleData.get(j);

							long insAssigId = scall.getInsuranceassigned_id();

							// to write assiugn method
							
							insurRepo.assignCallByManagerForInsurance(userdata, insAssigId,getUserLogindealerCode());
						}

						calls_count = divCount + calls_count;
						if (calls_count >= scheduledCallCount) {
							break;
						}

					}
					int call_countcompleted = divCount * creSize;
					logger.info("call_countcompleted : " + call_countcompleted);

					for (int i = 0; i < creSize; i++) {

						if (call_countcompleted < scheduledCallCount) {

							String userdata = selectedList.get(i);

							CallLogAjaxLoadInsurance  scall = scheduleData.get(call_countcompleted);

							long insAssigId = scall.getInsuranceassigned_id();
							// to write assign method
							
							insurRepo.assignCallByManagerForInsurance(userdata, insAssigId,getUserLogindealerCode());
							call_countcompleted++;

						} else {
							break;
						}

					}

				}

				return redirect(controllers.routes.InsuranceController.getPageForAssigningCallsInsurance());

			} else {
				return forbidden(views.html.error403.render());

			}

		}

	}

	// PREMIUM CALCULATION REQUIRED AJAX METHODS

	@Secure(clients = "FormClient")
	public Result getODPercentage(String cubicCap, String vehAge, String zoneid) {

				
		List<Double> OD_Percentage = insurRepo.getODPercentageByRestParams(cubicCap, vehAge, zoneid);

		//logger.info(" OD_Percentage : "+OD_Percentage);
		return ok(toJson(OD_Percentage));
	}

	@Secure(clients = "FormClient")
	public Result getBasicODVaue(double odvValue, double idvValue) {

		// logger.info(" odvValue : "+odvValue+" idvValue : "+idvValue);

		DecimalFormat df2 = new DecimalFormat(".##");
		
		
		double OD_Value = (odvValue) / 100;

		double OD_Basic = (OD_Value) * (idvValue);

		return ok(toJson(df2.format(OD_Basic)));
	}

	@Secure(clients = "FormClient")
	public Result getNCBValueByBasicValue(double ncbPercenVal, double basicODValue) {

		// logger.info(" ncbPercenVal : "+ncbPercenVal+" idvValue :
		// "+basicODValue);
		
		DecimalFormat df2 = new DecimalFormat(".##");
		
		
		double ncb_Perc_Value = (ncbPercenVal) / 100;

		double ncb_Value = (ncb_Perc_Value) * (basicODValue);
		double odpremium = basicODValue - ncb_Value;

		List<String> ncbData = new ArrayList();
		ncbData.add(df2.format(ncb_Value));
		ncbData.add(df2.format(odpremium));

		return ok(toJson(ncbData));
	}

	@Secure(clients = "FormClient")
	public Result gettotalPremiumAndDiscValue(double odPremiumVaue, double commercialDiscPerceValue,double thirdPartPremiumValue) {

		DecimalFormat df2 = new DecimalFormat(".##");
		
		double commercialDiscPerceValue_Value = (commercialDiscPerceValue) / 100;

		double dis_Value = (commercialDiscPerceValue_Value) * (odPremiumVaue);
		double totalODpremium = odPremiumVaue - dis_Value;

		double totalPackagePremium = totalODpremium + thirdPartPremiumValue+100;

		double finalPremium = ((0.15) * totalPackagePremium) + totalPackagePremium;

		// logger.info("finalPremium : "+finalPremium);

		List<String> premiumData = new ArrayList();
		premiumData.add(df2.format(dis_Value));
		premiumData.add(df2.format(totalODpremium));
		premiumData.add(df2.format(totalPackagePremium));
		premiumData.add(df2.format(finalPremium));
		return ok(toJson(premiumData));

	}

	@Secure(clients = "FormClient")
	public Result gettotalPremiumForAddOn(double addOnPremiumValuePer, double odPremiumVaue,
			double commercialDiscPerceValue,double idvVal,double thirdPartPremiumValue) {

		DecimalFormat df2 = new DecimalFormat(".##");
		
		double addOnPremiumValuePer_val=addOnPremiumValuePer/100;
		
		double addOnPremiumValue=idvVal*addOnPremiumValuePer_val;
		
		double commercialDiscPerceValue_Value = (commercialDiscPerceValue) / 100;

		double dis_Value = (commercialDiscPerceValue_Value) * (odPremiumVaue);
		double totalODpremium = (odPremiumVaue) - (dis_Value);

		double totalPackagePremium = totalODpremium + addOnPremiumValue + thirdPartPremiumValue+100;

		double finalPremium = ((0.15) * totalPackagePremium) + totalPackagePremium;

		List<String> premiumData = new ArrayList();
		premiumData.add(df2.format(totalPackagePremium));
		premiumData.add(df2.format(finalPremium));
		premiumData.add(df2.format(addOnPremiumValue));

		return ok(toJson(premiumData));

	}
	
	@Secure(clients = "FormClient")
	public Result showRoomList(){
		
		List<ShowRooms> listShowrooms=insurRepo.getShowRoomListData();
		
		for(ShowRooms sa:listShowrooms){
			
			sa.setAppointmentBooked(null);
		}
		
		logger.info(" listShowrooms : "+listShowrooms.size());
		
		return ok(toJson(listShowrooms));
	}
	
	private static CellProcessor[] getProcessors() {

		final CellProcessor[] processors = new CellProcessor[] { new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), 
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional()};

		return processors;
	}
	
	
	@Secure(clients = "FormClient")
	public Result downloadInsuranceErrorData(String uploadId,String uploadType) throws IOException, ParseException{
		
		
		Config configuration = ConfigFactory.load();
		String pathOfTemp = configuration.getString("app.configuration");
		
		String fileNameIs=uploadId+uploadType+".csv";
		
		File fis = new File(pathOfTemp +fileNameIs);

		ICsvBeanWriter csvwriter = new CsvBeanWriter(new FileWriter(fis, false), CsvPreference.STANDARD_PREFERENCE);
		final String[] headers = new String[] {"customerName","contactNo", "email", "doa", "dob",
				"address", "city", "state", "vehicleRegNo", "saleDate", "engineNo",
				"chassisNo", "model", "variant", "color", "lastMileage",
				"policyNo", "coveragePeriod", "policyDueDate", "insuranceCompanyName", "idv", "odPercentage",
				"odAmount", "ncBPercentage","ncBAmount","discountPercentage","ODpremium","liabilityPremium",
				"add_ON_Premium","premiumAmountBeforeTax","serviceTax","premiumAmountAfterTax","lastRenewalBy",
				"renewalType","errorInformation","rowNumber"};

		List<InsuranceExcelError> errorInsuranceData=insurRepo.getErrorOccuredDataInsurance(uploadId);
		
		logger.info(" errorInsuranceData : "+errorInsuranceData.size());
		try {

			final CellProcessor[] processors = getProcessors();

			csvwriter.writeHeader(headers);
			

			for (InsuranceExcelError aci : errorInsuranceData) {
				
				csvwriter.write(aci, headers, processors);

			}
			logger.info("csv created anfd ready to download");
		} catch (IOException ex) {
			logger.info("Error writing the CSV file: " + ex);
		} finally {
			try {
				logger.info("bean writer is not null ");
				csvwriter.close();
			} catch (IOException ex) {
				logger.info("Error closing the writer: " + ex);
			}
		}
		response().setContentType("application/x-download");
		response().setHeader("Content-disposition", "attachment; filename="+fileNameIs+"");
		return ok(fis, fileNameIs);
		
	}
	
	@Secure(clients = "FormClient")
	public Result insuranceFollowUpNotificationToday(){
		String dealercode = getUserLogindealerCode();
		String cre = getUserLoginName();
		WyzUser userdata = wyzRepo.getUserbyUserName(cre);
		
		
		
		
		//List<FollowUpNotificationModel> folowupData = new ArrayList<FollowUpNotificationModel>();
		
		List<FollowUpNotificationModel> folowupData=insurRepo.getFollowupNotificationOfInsu(userdata.getId(),dealercode);
		
		
		logger.info("folowupData Insurance: "+folowupData.size());
		return ok(toJson(folowupData));
		
		
	}
	
	
	public Result insuranceHistoryOfCustomerId(long customerId){
		
		List<AppointmentBooked> customerAppointments=insurRepo.getAppointMentsByCustomerId(customerId); 
		
		List<InsuranceHistoryData> insurHisData=new ArrayList<InsuranceHistoryData>();
		
		long countOfCustomerAppoint=customerAppointments.size();
		
		if(countOfCustomerAppoint!=0){
			
			for(AppointmentBooked sa:customerAppointments){
				
				
				
				InsuranceHistoryData insHist=new InsuranceHistoryData();
				if(sa.getCallInteraction()!=null){
					
					
					
				//	Insurance insData=sa.getInsurance();
					
					Insurance insData=new Insurance();
					
					insHist.setInsuranceCompanyName(insData.getInsuranceCompanyName());
					insHist.setIdv(insData.getIdv());
					insHist.setOdPercentage(insData.getOdPercentage());
					insHist.setOdAmount(insData.getOdAmount());
					insHist.setNcBPercentage(insData.getNcBPercentage());
					insHist.setNcBAmount(insData.getNcBAmount());
					insHist.setDiscountPercentage(insData.getDiscountPercentage());
					insHist.setODpremium(insData.getODpremium());
					insHist.setLiabilityPremium(insData.getLiabilityPremium());
					insHist.setAdd_ON_Premium(insData.getAdd_ON_Premium());
					insHist.setPremiumAmountBeforeTax(insData.getPremiumAmountBeforeTax());
					insHist.setServiceTax(insData.getServiceTax());
					insHist.setPremiumAmountAfterTax(insData.getPremiumAmountAfterTax());
					
					insurHisData.add(insHist);
					
					
				}else{
					
				
					insHist.setInsuranceCompanyName(sa.getInsuranceCompany());
					insHist.setIdv(0);
					insHist.setOdPercentage("0");
					insHist.setOdAmount(0);
					insHist.setNcBPercentage("0");
					insHist.setNcBAmount(0);
					insHist.setDiscountPercentage("0");
					insHist.setODpremium(0);
					insHist.setLiabilityPremium(0);
					insHist.setAdd_ON_Premium("0");
					insHist.setPremiumAmountBeforeTax(0);
					insHist.setServiceTax("0");
					insHist.setPremiumAmountAfterTax(sa.getPremiumwithTax());
					
					insurHisData.add(insHist);
					
				}
				
				
			}
		}
		
		logger.info("insurHisData : "+insurHisData.size());
		return ok(toJson(insurHisData));
	}
	
	
	@Secure(clients = "FormClient")	
	public Result getSMSbyLocAndSMSType(String locId,String smstypeid,long custId,long vehiId){
			logger.info("locId:"+locId);
			logger.info("smstypeid:"+smstypeid);
			
			String userName = getUserProfile().getId();
			WyzUser userdata = wyzRepo.getUserbyUserName(userName);
		
			
			Customer customerData = inbound_repo.getCustomerInforById(custId);
			Vehicle vehicleData = inbound_repo.getVehicleInformationById(vehiId);

	
			SMSTemplate template = wyzRepo.getSMSbyLocAndSMSType(locId,smstypeid);
			
			
			if (vehicleData != null && template.getSmsTemplate()!=null) {
				
				if(vehicleData.getVehicleRegNo()!=null){
				template.setSmsTemplate(template.getSmsTemplate().replace("(Vehicle)", vehicleData.getVehicleRegNo()));
				}else{
					
					template.setSmsTemplate(template.getSmsTemplate().replace("(Vehicle)", ""));
				}
				if (vehicleData.getNextServicedate() != null) {

					template.setSmsTemplate(template.getSmsTemplate().replace("(Service Due Date)",
							vehicleData.getNextServicedate().toString()));
				} else {

					template.setSmsTemplate(template.getSmsTemplate().replace("(Service Due Date)", ""));
				}

			} else if(template.getSmsTemplate()!=null){
				template.setSmsTemplate(template.getSmsTemplate().replace("(Vehicle)", ""));
			}
			
			
			if(customerData != null  && template.getSmsTemplate()!=null){
				if(customerData.getCustomerName() != null){
					template.setSmsTemplate(template.getSmsTemplate().replace("(customer name)",customerData.getCustomerName()));
					
				}
				
			}
			if(userdata != null  && template.getSmsTemplate()!=null){
				if(userdata.getPhoneNumber() != null){
					template.setSmsTemplate(template.getSmsTemplate().replace("(phonenumber)",userdata.getPhoneNumber()));
					
				}
				
				
				
			}
			
			if(template.getSmsTemplate()!=null){
				
				
				template.setSmsTemplate(template.getSmsTemplate().replace("(Dealer)", userdata.getDealerCode()));
				
			}
			
			
			
			String smsname = template.getSmsTemplate();
			
			logger.info("sms "+smsname);
			if(smsname != null){
				return ok(toJson(smsname));
			}else{
				return ok("Sorry!! No SMS found");
	
		}
		}


}
