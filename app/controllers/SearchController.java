package controllers;

import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;
import static play.libs.Json.toJson;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.pac4j.core.profile.CommonProfile;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import actors.FireBaseReportDetailsSync;
import actors.FireBaseReportDetailsSyncProtocol;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.Props;
import controllers.webmodels.CallLogAjaxLoad;
import controllers.webmodels.CallLogDispositionLoad;
import controllers.webmodels.CallLogDispositionLoadMR;
import controllers.webmodels.UploadFileHistory;
import models.CallInteraction;

import models.CustomerSearchResult;
import models.WyzUser;
import org.pac4j.play.java.Secure;
import play.Logger.ALogger;
import play.db.DB;
import play.libs.Akka;
import play.libs.Json;
import play.mvc.Result;
import repositories.CallInteractionsRepository;
import repositories.SearchRepository;
import repositories.WyzUserRepository;
import scala.concurrent.duration.Duration;
import views.html.uploadDateWiseForm;

public class SearchController extends play.mvc.Controller {

	ALogger logger = play.Logger.of("application");

	private SearchRepository searchRepo;
	private final WyzUserRepository userRepository;
	private final CallInteractionsRepository call_int_repo;
	private PlaySessionStore playSessionStore;

	private Cancellable reportCallSatatus = null;

	@Inject
	public SearchController(SearchRepository repository, WyzUserRepository userRepo,
			CallInteractionsRepository interRepo, PlaySessionStore playSession) {

		searchRepo = repository;
		userRepository = userRepo;
		call_int_repo = interRepo;
		playSessionStore = playSession;
	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	// SEARCH CUSTOMER
	@Secure(clients = "FormClient")
	public Result searchCustomer() {

		Map<String, String[]> paramMap = request().queryString();

		Set<String> keys = paramMap.keySet();

		for (String key : keys) {

			String[] params = paramMap.get(key);

			for (String param : params) {
			//	logger.info("Key :" + key + " Value:" + param);
			}
		}

		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String searchCust = "";
		String searchVeh = "";
		String searchCat = "";
		String searchPh = "";
		String searchserDateFrom = "";
		String searchDateTo = "";
		String searchModel = "";
		String searchVar = "";
		String searchDisp = "";
		String searchName = "";

		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
			logger.info(" searchPattern paramMap : " + searchPattern);
		}

		if (paramMap.get("columns[0][search][value]") != null) {
			searchCust = paramMap.get("columns[0][search][value]")[0];
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			searchVeh = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			searchCat = paramMap.get("columns[2][search][value]")[0];
		}

		if (paramMap.get("columns[3][search][value]") != null) {
			searchPh = paramMap.get("columns[3][search][value]")[0];
		}

		if (paramMap.get("columns[4][search][value]") != null) {
			String searchserDate = paramMap.get("columns[4][search][value]")[0];
			logger.debug("searchserDate : "+searchserDate);
			String[] searchserDateArr = searchserDate.split(",");
			
			if (searchserDateArr.length == 2) {
				searchserDateFrom = searchserDateArr[0];
				searchDateTo = searchserDateArr[1];
			}
		}

		if (paramMap.get("columns[5][search][value]") != null) {
			searchModel = paramMap.get("columns[5][search][value]")[0];
		}

		if (paramMap.get("columns[6][search][value]") != null) {
			searchVar = paramMap.get("columns[6][search][value]")[0];
		}

		if (paramMap.get("columns[7][search][value]") != null) {
			searchDisp = paramMap.get("columns[7][search][value]")[0];
		}

		if (paramMap.get("columns[8][search][value]") != null) {
			searchName = paramMap.get("columns[8][search][value]")[0];
		}

		logger.debug("##############Search parameters##############");
		logger.debug("Customer Name: " + searchCust);
		logger.debug("Vehicle: " + searchVeh);
		logger.debug("Category: " + searchCat);
		logger.debug("Phone: " + searchPh);
		logger.debug("SearchDate From: " + searchserDateFrom);
		logger.debug("SearchDate To: " + searchDateTo);
		logger.debug("Model: " + searchModel);
		logger.debug("Variant: " + searchVar);
		logger.debug("Disposition: " + searchDisp);
		logger.info("searchName: " + searchName);

		if (searchPattern.length() == 0 && searchCust.length() == 0 && searchVeh.length() == 0
				&& searchCat.length() == 0 && searchPh.length() == 0 && searchserDateFrom.length() == 0
				&& searchDateTo.length() == 0 && searchModel.length() == 0 && searchVar.length() == 0
				&& searchDisp.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}

		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}

		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		
		long totalSize=searchRepo.getTotalCountOfCustomers();
		
		 logger.info("totalSize of customer : "+totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}

		long patternCount = 0;

		

		if (!allflag) {
			if (globalSearch){
				//patternCount = searchRepo.getCustomerCountForPattern(searchPattern);
				//patternCount = searchRepo.getAllVehicleCount();
				/*patternCount = searchRepo.getCustomerCountIndv(searchCust, searchVeh, searchPh, searchserDateFrom,
						searchDateTo, searchModel, searchVar,searchDisp, searchCat,searchPattern );

				else*/
				patternCount = searchRepo.getCustomerCountIndv(searchCust, searchVeh, searchPh, searchserDateFrom,
						searchDateTo, searchModel, searchVar,searchDisp, searchCat,searchPattern );

			 logger.info("patternCount of customer : "+patternCount);
			 
			 
			 logger.info("patternCount of customer is : "+patternCount);
		}
			 else{
				 
				 patternCount = searchRepo.getCustomerCountIndv(searchCust, searchVeh, searchPh, searchserDateFrom,
							searchDateTo, searchModel, searchVar,searchDisp, searchCat,searchPattern );
			 }

		}
		
		if (searchName.equals("")) {
			logger.info("search name is empty");

		} else {

			HashMap<String, String> searchParams = new HashMap<String, String>();
			searchParams.put(SearchRepository.CUST_NAME, searchCust);
			searchParams.put(SearchRepository.VEH_REG_NO, searchVeh);
			searchParams.put(SearchRepository.PHONE_NUM, searchPh);

			searchParams.put(SearchRepository.SEARCH_DT_FROM, searchserDateFrom);
			searchParams.put(SearchRepository.SEARCH_DT_TO, searchDateTo);
			searchParams.put(SearchRepository.MODEL, searchModel);
			searchParams.put(SearchRepository.VARIANT, searchVar);
			searchParams.put(SearchRepository.CUST_CAT, searchCat);
			searchParams.put(SearchRepository.LAST_DISP, searchDisp);
			searchParams.put(SearchRepository.START_INDX, paramMap.get("start")[0]);
			searchParams.put(SearchRepository.SIZE, String.valueOf(patternCount));
			searchRepo.saveSearchName(searchName, searchParams);
			logger.info("search name is not empty");

		}
		

		
		List<CustomerSearchResult> customerList = searchRepo.searchCustomersIndv(searchCust, searchVeh, searchPh, searchserDateFrom,
				searchDateTo, searchModel, searchVar, searchDisp, searchCat, searchPattern, fromIndex, toIndex);
		
		
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

		
		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CustomerSearchResult c : customerList) {

			// String dispositionLink="<a href='/"+c.getCustomerName()+"'><i
			// class=\"fa fa-pencil-square\" data-toggel=\"tooltip\"
			// title=\"Disposition\"
			// style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			
			String urlDisposition = "<a href='/CRE/getDispositionFormPage/" + c.getCid() + "/"
					+ c.getVehicle_id()
					+ "/insurance'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\" Insurance Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			
			String urlDispositionPSF = "";
			long campainIdIS=5;
			
			if(c.getServiceId()!=0){
				
				List<Long> ids=searchRepo.getInteractionID(campainIdIS,c.getServiceId());
				logger.info("disposition history : "+ids.get(0));
				logger.info("Interaction  id : "+ids.get(1));
				
				urlDispositionPSF="<a href='/CRE/psfDispo/" + c.getCid() + "/" + c.getVehicle_id() + "/"+ids.get(1)+"/"+ids.get(0)+"/4'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"PSF Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			}

			ObjectNode row = Json.newObject();
			// row.put("0", c.getCid());
			row.put("0", c.getCustomerName());
			row.put("1", c.getVehicleRegNo());
			row.put("2", c.getCustomerCategory());
			row.put("3", c.getPhoneNumber());
			row.put("4", c.getNextServicedate());
			row.put("5", c.getModel());
			row.put("6", c.getVariant());				
			row.put("7", "<a href='/CRE/getDispositionFormPage/" + c.getCid() + "/" + c.getVehicle_id()
					+ "/serviceSearch'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Service Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>");
			row.put("8", urlDisposition);				
			row.put("9", urlDispositionPSF);
			
			an.add(row);
		}

		return ok(result);
	}	// SERVICE REMAINDER LIST DATA

	// SERVICE REMAINDER LIST DATA
	@Secure(clients = "FormClient")
	public Result assignedInteractionData() {

		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String campaignName = "";
		String fromDateNew = "";
		String toDateNew = "";
		String serviceTypeName = "";
		String serviceBookedType = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			campaignName = paramMap.get("columns[0][search][value]")[0];
			if (campaignName.equals("0")) {
				campaignName = "";
			}
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDateNew = paramMap.get("columns[2][search][value]")[0];
		}

		if (paramMap.get("columns[3][search][value]") != null) {
			serviceTypeName = paramMap.get("columns[3][search][value]")[0];
			if (serviceTypeName.equals("0")) {
				serviceTypeName = "";
			}
		}

		if (paramMap.get("columns[4][search][value]") != null) {
			serviceBookedType = paramMap.get("columns[4][search][value]")[0];
			if (serviceBookedType.equals("0")) {
				serviceBookedType = "";
			}
		}

		logger.info("campaign Name: " + campaignName);
		logger.info("SearchDate From: " + fromDateNew);
		logger.info("SearchDate To: " + toDateNew);
		logger.info("serviceTypeName: " + serviceTypeName);
		logger.info("serviceBookedType: " + serviceBookedType);

		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && serviceTypeName.length() == 0 && serviceBookedType.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}
		logger.info("searchPattern :" + searchPattern);
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = searchRepo.getAllAssignedInteraction(userdata.getId());
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		List<CallLogAjaxLoad> assignedList = searchRepo.assignedListOfUser(fromDateNew, toDateNew, campaignName,
				serviceTypeName, searchPattern, userdata.getId(), fromIndex, toIndex);

		if (!allflag) {
			if (globalSearch)
				//patternCount = searchRepo.getAllAssignedInteraction(userdata.getId());
				patternCount = searchRepo.assignedListOfUser(fromDateNew, toDateNew, campaignName, serviceTypeName,
						searchPattern, userdata.getId(), 0, totalSize).size();
			else
				patternCount = searchRepo.assignedListOfUser(fromDateNew, toDateNew, campaignName, serviceTypeName,
						searchPattern, userdata.getId(), 0, totalSize).size();

		}
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

		logger.info("allFlag : " + allflag + " global : " + globalSearch);

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogAjaxLoad c : assignedList) {

			String urlDisposition = "<a href='/CRE/getDispositionFormPage/" + c.getCustomer_id() + "/"
					+ c.getVehical_Id() + ""
					+ "/service'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";

			ObjectNode row = Json.newObject();
			row.put("0", c.getCampaignName());
			row.put("1", c.getCustomer_name());
			row.put("2", c.getVehicle_RegNo());
			row.put("3", c.getVeh_model());
			row.put("4", c.getCategory());
			row.put("5", c.getDuedate());
			row.put("6", c.getDue_type());
			row.put("7", c.getLast_psfstatus());
			row.put("8", c.getDND_status());
			row.put("9", c.getComplaints_count());
			row.put("10", urlDisposition);

			an.add(row);
		}

		return ok(result);
	}

	// FOLLOWUP REQUIRED DATA
	@Secure(clients = "FormClient")
	public Result followUpRequiredInteractionData() {

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String campaignName = "";
		String fromDateNew = "";
		String toDateNew = "";
		String serviceTypeName = "";
		String serviceBookedType = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			campaignName = paramMap.get("columns[0][search][value]")[0];
			if (campaignName.equals("0")) {
				campaignName = "";
			}
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDateNew = paramMap.get("columns[2][search][value]")[0];
		}

		if (paramMap.get("columns[3][search][value]") != null) {
			serviceTypeName = paramMap.get("columns[3][search][value]")[0];
			if (serviceTypeName.equals("0")) {
				serviceTypeName = "";
			}
		}

		if (paramMap.get("columns[4][search][value]") != null) {
			serviceBookedType = paramMap.get("columns[4][search][value]")[0];
			if (serviceBookedType.equals("0")) {
				serviceBookedType = "";
			}
		}

		logger.info("campaign Name: " + campaignName);
		logger.info("SearchDate From: " + fromDateNew);
		logger.info("SearchDate To: " + toDateNew);
		logger.info("serviceTypeName: " + serviceTypeName);
		logger.info("serviceBookedType: " + serviceBookedType);

		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && serviceTypeName.length() == 0 && serviceBookedType.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}

		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		// logger.info("fromIndex from follow up : "+fromIndex+" toIndex :
		// "+toIndex);

		long totalSize = searchRepo.getAllDispositionRequiredData(userdata.getId(), 4);
		// logger.info("totalSize of followup interaction : "+totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		long typeOfDispo = 4;
		int orderDirection = 1;
		List<CallLogDispositionLoad> followUpList = searchRepo.getDispositionListOfUser(fromDateNew, toDateNew,
				campaignName, serviceTypeName, searchPattern, userdata.getId(), typeOfDispo, fromIndex, toIndex,
				orderDirection);

		if (!allflag) {
			if (globalSearch)
				//patternCount = searchRepo.getAllDispositionRequiredData(userdata.getId(), 4);
				patternCount = searchRepo.getDispositionListOfUser(fromDateNew, toDateNew, campaignName,
						serviceTypeName, searchPattern, userdata.getId(), typeOfDispo, 0, totalSize, orderDirection)
						.size();
			else
				patternCount = searchRepo.getDispositionListOfUser(fromDateNew, toDateNew, campaignName,
						serviceTypeName, searchPattern, userdata.getId(), typeOfDispo, 0, totalSize, orderDirection)
						.size();

		}
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

		// logger.info("subList Size:" + followUpList.size());

		// long sublistSize = customerList.size();

		// logger.info("fromIndex:" + fromIndex);
		// logger.info("toIndex:" + toIndex);
		// logger.info("Search Pattern" + searchPattern);

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogDispositionLoad c : followUpList) {

			String urlDisposition = "<a href='/CRE/getDispositionFormPage/" + c.getCustomer_id() + "/"
					+ c.getVehicle_id() + ""
					+ "/service'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			// String urlEdit = "<a href='/" + getUserLogindealerCode() +
			// "/CRE/toeditcallLog/" + c.getCallinteraction_id()
			// + "'><i class=\"fa fa-pencil-square-o\" data-toggle=\"tooltip\"
			// title=\"edit\"></i></a>";
			//
			String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";

			// logger.info(urlDisposition);
			String stringDate=getStringDate(c.getCall_date());			

			ObjectNode row = Json.newObject();
			// row.put("0", urlEdit);
			row.put("0", c.getCampaignName());
			row.put("1", stringDate);
			row.put("2", c.getCustomer_name());
			row.put("3", c.getMobile_number());
			row.put("4", c.getVehicle_RegNo());
			row.put("5", c.getDuedate());
			row.put("6", c.getFollowUpDate());
			row.put("7", c.getFollowUpTime());
			row.put("8", disposition);
			row.put("9", urlDisposition);
			an.add(row);
		}

		return ok(result);
	}

	private String getStringDate(Date call_date) {
		// TODO Auto-generated method stub
		if (call_date != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate= formatter.format(call_date);
			return stringDate;
			
		}else{
			
		}
		return "";
	}

	// SERVICE BOOKED DATA
	@Secure(clients = "FormClient")
	public Result serviceBookedInteractionData() {

		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String campaignName = "";
		String fromDateNew = "";
		String toDateNew = "";
		String serviceTypeName = "";
		String serviceBookedType = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			campaignName = paramMap.get("columns[0][search][value]")[0];
			if (campaignName.equals("0")) {
				campaignName = "";
			}
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDateNew = paramMap.get("columns[2][search][value]")[0];
		}

		if (paramMap.get("columns[3][search][value]") != null) {
			serviceTypeName = paramMap.get("columns[3][search][value]")[0];
			if (serviceTypeName.equals("0")) {
				serviceTypeName = "";
			}
		}

		if (paramMap.get("columns[4][search][value]") != null) {
			serviceBookedType = paramMap.get("columns[4][search][value]")[0];
			if (serviceBookedType.equals("0")) {
				serviceBookedType = "";
			}
		}

		logger.info("campaign Name: " + campaignName);
		logger.info("SearchDate From: " + fromDateNew);
		logger.info("SearchDate To: " + toDateNew);
		logger.info("serviceTypeName: " + serviceTypeName);
		logger.info("serviceBookedType: " + serviceBookedType);

		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && serviceTypeName.length() == 0 && serviceBookedType.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}

		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = searchRepo.getAllDispositionRequiredData(userdata.getId(), 3);
		// logger.info("totalSize of assigned interaction : "+totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		long typeOfDispo = 3;
		int orderDirection = 1;
		List<CallLogDispositionLoad> serviceBookedList = searchRepo.getDispositionListOfUser(fromDateNew, toDateNew,
				campaignName, serviceBookedType, searchPattern, userdata.getId(), typeOfDispo, fromIndex, toIndex,
				orderDirection);

		if (!allflag) {
			if (globalSearch)
				//patternCount = searchRepo.getAllDispositionRequiredData(userdata.getId(), 3);
				patternCount = searchRepo.getDispositionListOfUser(fromDateNew, toDateNew, campaignName,
						serviceBookedType, searchPattern, userdata.getId(), typeOfDispo, 0, totalSize, orderDirection)
						.size();

			else
				patternCount = searchRepo.getDispositionListOfUser(fromDateNew, toDateNew, campaignName,
						serviceBookedType, searchPattern, userdata.getId(), typeOfDispo, 0, totalSize, orderDirection)
						.size();

		}
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

		// logger.info("serviceBookedList Size:" + serviceBookedList.size());

		// long sublistSize = customerList.size();

		// logger.info("fromIndex:" + fromIndex);
		// logger.info("toIndex:" + toIndex);
		// logger.info("Search Pattern" + searchPattern);

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogDispositionLoad c : serviceBookedList) {

			String urlDisposition = "<a href='/CRE/getDispositionFormPage/" + c.getCustomer_id() + "/"
					+ c.getVehicle_id() + ""
					+ "/service'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			//
			// String urlEdit = "<a href='/" + getUserLogindealerCode() +
			// "/CRE/toeditcallLog/" + c.getCallinteraction_id()
			// + "'><i class=\"fa fa-pencil-square-o\" data-toggle=\"tooltip\"
			// title=\"edit\"></i></a>";
			//
			String disposition = "<span class=\"label label-primary\">" + c.getReason() + "</span>";

			// logger.info(urlDisposition);
			String stringDate=getStringDate(c.getCall_date());			


			ObjectNode row = Json.newObject();
			// row.put("0", urlEdit);
			row.put("0", c.getFollowUpDate());			
			row.put("1", c.getCampaignName());
			row.put("2", stringDate);
			row.put("3", c.getCustomer_name());
			row.put("4", c.getMobile_number());
			row.put("5", c.getVehicle_RegNo());
			row.put("6", c.getDuedate());
			row.put("7", c.getScheduled_date());
			row.put("8", c.getScheduled_time());
			row.put("9", c.getFollowUpTime());
			row.put("10", disposition);
			row.put("11", c.getLast_disposition());
			row.put("12", urlDisposition);
			an.add(row);
		}

		return ok(result);

	}

	// SERVICE NOT REQUIRED DATA
	@Secure(clients = "FormClient")
	public Result serviceNotRequiredInteractionData() {

		

		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();
		
		String campaignName = "";
		String fromDateNew = "";
		String toDateNew = "";
		String serviceTypeName = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			campaignName = paramMap.get("columns[0][search][value]")[0];
			if (campaignName.equals("0")) {
				campaignName = "";
			}
		}

		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		/*
		 * if (searchPattern.length() == 0) { allflag = true; }
		 */

		if (searchPattern.length() == 0 && campaignName.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}

		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = searchRepo.getAllDispositionRequiredData(userdata.getId(), 5);
		// logger.info("totalSize of assigned interaction : "+totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		long typeOfDispo = 5;
		int orderDirection = 1;
		List<CallLogDispositionLoad> serviceNotRequiredList = searchRepo.getDispositionListOfUser(fromDateNew,
				toDateNew, campaignName, serviceTypeName, searchPattern, userdata.getId(), typeOfDispo, fromIndex,
				toIndex, orderDirection);

		if (!allflag) {
			if (globalSearch)
				//patternCount = searchRepo.getAllDispositionRequiredData(userdata.getId(), 5);
				patternCount = searchRepo.getDispositionListOfUser(fromDateNew, toDateNew, campaignName,
						serviceTypeName, searchPattern, userdata.getId(), typeOfDispo, 0, totalSize, orderDirection)
						.size();
			else
				patternCount = searchRepo.getDispositionListOfUser(fromDateNew, toDateNew, campaignName,
						serviceTypeName, searchPattern, userdata.getId(), typeOfDispo, 0, totalSize, orderDirection)
						.size();

		}
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogDispositionLoad c : serviceNotRequiredList) {

			String urlDisposition = "<a href='/CRE/getDispositionFormPage/" + c.getCustomer_id() + "/"
					+ c.getVehicle_id() + ""
					+ "/service'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			//
			// String urlEdit = "<a href='/" + getUserLogindealerCode() +
			// "/CRE/toeditcallLog/" + c.getCallinteraction_id()
			// + "'><i class=\"fa fa-pencil-square-o\" data-toggle=\"tooltip\"
			// title=\"edit\"></i></a>";
			//
			String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";

			// logger.info(urlDisposition);
			String stringDate=getStringDate(c.getCall_date());			


			ObjectNode row = Json.newObject();
			// row.put("0", urlEdit);
			row.put("0", c.getCampaignName());
			row.put("1", stringDate);
			row.put("2", c.getCustomer_name());
			row.put("3", c.getMobile_number());
			row.put("4", c.getVehicle_RegNo());
			row.put("5", c.getDuedate());
			row.put("6", disposition);
			row.put("7", c.getReason());
			row.put("8", urlDisposition);
			an.add(row);
		}

		return ok(result);

	}

	// NON CONTACT DATA
	@Secure(clients = "FormClient")
	public Result nonContactsInteractionData() {

		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String campaignName = "";
		String fromDateNew = "";
		String toDateNew = "";
		String lastDipo = "";
		String droppedCount = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			campaignName = paramMap.get("columns[0][search][value]")[0];
			if (campaignName.equals("0")) {

				campaignName = "";
			}
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDateNew = paramMap.get("columns[2][search][value]")[0];
		}

		if (paramMap.get("columns[5][search][value]") != null) {
			lastDipo = paramMap.get("columns[5][search][value]")[0];
			if (lastDipo.equals("0")) {

				lastDipo = "";
			}
		}

		if (paramMap.get("columns[6][search][value]") != null) {
			droppedCount = paramMap.get("columns[6][search][value]")[0];
			if (droppedCount.equals("0")) {

				droppedCount = "";
			}
		}

		logger.info("campaign Name: " + campaignName);
		logger.info("SearchDate From: " + fromDateNew);
		logger.info("SearchDate To: " + toDateNew);
		logger.info("lastDipo: " + lastDipo);
		logger.info("droppedCount: " + droppedCount);

		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && lastDipo.length() == 0 && droppedCount.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}

		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = searchRepo.getNoncontactsDataCount(userdata.getId());
		// logger.info("totalSize of noncontacts interaction : "+totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		List<CallLogDispositionLoad> nonContactsList = searchRepo.getnonContactsListOfUser(fromDateNew, toDateNew,
				campaignName, lastDipo, droppedCount, searchPattern, userdata.getId(), fromIndex, toIndex);

		if (!allflag) {
			if (globalSearch)
				//patternCount = searchRepo.getNoncontactsDataCount(userdata.getId());
				patternCount = searchRepo.getnonContactsListOfUser(fromDateNew, toDateNew, campaignName, lastDipo,
						droppedCount, searchPattern, userdata.getId(), 0, totalSize).size();

			else
				patternCount = searchRepo.getnonContactsListOfUser(fromDateNew, toDateNew, campaignName, lastDipo,
						droppedCount, searchPattern, userdata.getId(), 0, totalSize).size();

		}
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

		// logger.info("nonContactsList : "+nonContactsList.size());

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogDispositionLoad c : nonContactsList) {

			String urlDisposition = "<a href='/CRE/getDispositionFormPage/" + c.getCustomer_id() + "/"
					+ c.getVehicle_id() + ""
					+ "/service'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			//
			// String urlEdit = "<a href='/" + getUserLogindealerCode() +
			// "/CRE/toeditcallLog/" + c.getCallinteraction_id()
			// + "'><i class=\"fa fa-pencil-square-o\" data-toggle=\"tooltip\"
			// title=\"edit\"></i></a>";
			//
			String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";

			// logger.info(urlDisposition);
			String stringDate=getStringDate(c.getCall_date());			


			ObjectNode row = Json.newObject();
			// row.put("0", urlEdit);
			row.put("0", c.getCampaignName());
			row.put("1", stringDate);
			row.put("2", c.getCustomer_name());
			row.put("3", c.getMobile_number());
			row.put("4", c.getVehicle_RegNo());
			row.put("5", c.getDuedate());
			row.put("6", disposition);
			row.put("7", urlDisposition);
			an.add(row);
		}

		return ok(result);

	}

	// DROPPED LIST DATA
	@Secure(clients = "FormClient")
	public Result droppedCallInteractionData() {

		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}

		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = searchRepo.getDroppedDataCount(userdata.getId());
		logger.info("totalSize of dropped count : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		List<CallLogDispositionLoad> droppedDataList = searchRepo.getDroppedDataList(searchPattern, userdata.getId(),
				fromIndex, toIndex);

		if (!allflag) {
			patternCount = droppedDataList.size();
			// logger.info("patternCount of droppedDataList : "+patternCount);

		}

		// logger.info("DroppedData : "+droppedDataList.size());

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallLogDispositionLoad c : droppedDataList) {

			String disposition = "<span class=\"label label-primary\">" + c.getLast_disposition() + "</span>";

			// logger.info(urlDisposition);
			String stringDate=getStringDate(c.getCall_date());			


			ObjectNode row = Json.newObject();
			row.put("0", c.getCampaignName());
			row.put("1", stringDate);
			row.put("2", c.getCustomer_name());
			row.put("3", c.getMobile_number());
			row.put("4", c.getVehicle_RegNo());
			row.put("5", c.getDuedate());
			row.put("6", disposition);

			an.add(row);
		}

		return ok(result);

	}

	// Dashboard CRE count
	@Secure(clients = "FormClient")
	public Result getDashboardCountCRE() {

		WyzUser userdata = userRepository.getUserbyUserName(getUserLoginName());

		//double countOfconversion = searchRepo.getConversionRate(userdata.getId(), "conversion_rate_Crewise");
		long pendingfollowupCount =searchRepo.getPendingandtotalcount(userdata.getId(),"totalFollowupCountCRE");
		long countOfpendingcalls = searchRepo.getPendingandtotalcount(userdata.getId(), "pending_calls_crewise");
		long countofOverdueBookings= searchRepo.getPendingandtotalcount(userdata.getId(),"totaloverDueCountCRE");
		long countofNoncontacts= searchRepo.getPendingandtotalcount(userdata.getId(),"totalNoncontactsCountCRE");

		//long servicebookedcount = searchRepo.serviceBookDashboardCount(userdata.getId());
		
		long totalcallsCount= searchRepo.getPendingandtotalcount(userdata.getId(),"totalCallsCountCRE");
		long totalbookingperdayCount= searchRepo.getPendingandtotalcount(userdata.getId(),"totalBookingPerday");
		// long servicebookedcount=0;
		// long schcount=searchRepo.getAllAssignedInteraction(userdata.getId());

		//long schcount = searchRepo.getPendingandtotalcount(userdata.getId(), "scheduledcalls_count_crewise");
		ArrayList<Object> ajaxData = new ArrayList<Object>();
		ajaxData.add(countOfpendingcalls);
		ajaxData.add(pendingfollowupCount);
		ajaxData.add(countofNoncontacts);
		ajaxData.add(countofOverdueBookings);
		ajaxData.add(totalbookingperdayCount);
		ajaxData.add(totalcallsCount);
		
		logger.info("pendingfollowupCount : " + pendingfollowupCount + "countOfpendingcalls : " + countOfpendingcalls
				+ "countofNoncontacts :" + countofNoncontacts +" countofOverdueBookings"+countofOverdueBookings +"totalcallsCount :"+totalcallsCount);

		return ok(toJson(ajaxData));
	}
	// Upload Data History
	@Secure(clients = "FormClient")
	public Result uploadHistoryReport(String uploadId, String uploadType) {

		logger.info(" uploadId from report is : " + uploadId);
		Date minDate = null;
		Date maxDate = null;

		HashMap<String, String> typeOfReport = new HashMap<String, String>();

		typeOfReport.put("job_card", "Job Card Status");
		typeOfReport.put("bill", "Work Bills");
		typeOfReport.put("SMR", "SMR");
		typeOfReport.put("campaign", "Campaign");
		typeOfReport.put("sale_register", "Sale Register");

		if (uploadId.equals("0")) {

		} else {
			logger.info("upload id is not zero");

			List<Date> uploadIdMAxAndMinDate = searchRepo.getUploadIdMaxMinDate(uploadId, uploadType);
			minDate = uploadIdMAxAndMinDate.get(0);
			maxDate = uploadIdMAxAndMinDate.get(1);

		}
		return ok(uploadDateWiseForm.render(typeOfReport, uploadId, uploadType, minDate, maxDate,
				getUserLogindealerCode(), getDealerName(), getUserLoginName()));

	}

	@Secure(clients = "FormClient")
	public Result uploadHistoryViewData(String typeIds, String fromDate, String toDate, String uploadReportId) {

		if (uploadReportId.equals("0")) {

			uploadReportId = "";
		}

		if (typeIds.equals("Select")) {
			logger.info("CREIdsIF :" + typeIds);
			typeIds = "";
		}

		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDateNew = new Date();
		Date toDateNew = new Date();
		try {
			fromDateNew = dmyFormat.parse(fromDate);
			toDateNew = dmyFormat.parse(toDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("fromDateNew  is : " + fromDateNew);
		logger.info("toDateNew selected type date :" + toDateNew);
		logger.info("typeIds selected type date :" + typeIds);
		logger.info("uploadReportId selected type date :" + uploadReportId);

		java.sql.Date sqlDatefrom = new java.sql.Date(fromDateNew.getTime());
		java.sql.Date sqlDateTo = new java.sql.Date(toDateNew.getTime());

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();
		String searchPattern = "";
		boolean allflag = false;
		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}
		if (searchPattern.length() == 0) {
			allflag = true;
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		long totalSize = 0;

		totalSize = searchRepo.getCountOfUploadFileHistory(uploadReportId, typeIds, fromDateNew, toDateNew, fromIndex,
				toIndex);

		logger.info("totalSize of upload history: " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		List<UploadFileHistory> uploadHistoryList = searchRepo.getListOfUploadFileHistory(uploadReportId, typeIds,
				fromDateNew, toDateNew, fromIndex, toIndex);

		logger.info("uploadHistoryList : " + uploadHistoryList.size());
		if (!allflag) {

			patternCount = uploadHistoryList.size();
			logger.info("patternCount of uploadHistoryList : " + patternCount);

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (UploadFileHistory c : uploadHistoryList) {

			ObjectNode row = Json.newObject();

			row.put("0", c.getReport_Name());
			row.put("1", c.getReport_Date());
			row.put("2", c.getCity());
			row.put("3", c.getWorkshop());
			row.put("4", c.getTotalRecords());
			row.put("5", c.getApproved_records());
			row.put("6", c.getRejectedRecord());
			row.put("7", c.getUploaded_File_Name());
			row.put("8", c.getUploaded_by_user());
			row.put("9", c.getUploadDate());
			row.put("10", c.getUpload_time());
			row.put("11", c.getUpload_id());

			an.add(row);
		}
		return ok(result);

	}

	public Result startSyncOfReports() {

		boolean startsyncState = false;
		if (reportCallSatatus != null) {
			if (reportCallSatatus.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}
		if (startsyncState) {

			ActorRef actor = Akka.system().actorOf(Props.create(FireBaseReportDetailsSync.class));

			reportCallSatatus = Akka.system().scheduler().schedule(Duration.create(0, TimeUnit.MILLISECONDS), // Initial
																												// delay
																												// 0
																												// milliseconds
					Duration.create(1, TimeUnit.HOURS), // Frequency
														// 1
														// Hour
														// actor,
					actor,
					new FireBaseReportDetailsSyncProtocol.StartSync(
							play.Play.application().configuration().getString("app.myfirebaseurl"), userRepository,
							searchRepo),
					Akka.system().dispatcher(), null);

			logger.info("Reports made Sync operation");
			return ok("done");

		}
		return ok("done");

	}

	/*
	 * public void savedCustomerData(){
	 * 
	 * 
	 * 
	 * }
	 */

}
