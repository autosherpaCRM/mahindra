package controllers;

import static play.libs.Json.toJson;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.apache.metamodel.DataContextFactory;
import org.apache.metamodel.create.CreateTable;
import org.apache.metamodel.data.DataSet;
import org.apache.metamodel.data.Row;
import org.apache.metamodel.excel.ExcelConfiguration;
import org.apache.metamodel.excel.ExcelDataContext;
import org.apache.metamodel.insert.InsertInto;
import org.apache.metamodel.jdbc.JdbcDataContext;
import org.apache.metamodel.query.Query;
import org.apache.metamodel.query.SelectItem;
import org.apache.metamodel.schema.Column;
import org.apache.metamodel.schema.ColumnType;
import org.apache.metamodel.schema.Schema;
import org.apache.metamodel.schema.Table;
import org.apache.metamodel.schema.naming.CustomColumnNamingStrategy;
import org.pac4j.core.config.Config;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;

import actors.DataSourceHelper;
import actors.ProcessUploadActor;
import actors.ProcessUploadProtocol;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Cancellable;
import controllers.pac4j.MyAppAuthenticator;
import controllers.webModel.FileDelete;
import controllers.webModel.FileUploadData;
import controllers.webModel.FileUploadResponse;
import models.ColumnDefinition;
import models.FormData;
import models.Upload;
import play.Logger.ALogger;
import play.db.DBApi;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import repositories.UploadRepository;
import scala.concurrent.ExecutionContextExecutor;
import scala.concurrent.duration.Duration;

public class ProcessUploadedFiles extends Controller {

	protected static ALogger logger = play.Logger.of("application");

	private Config config;

	private PlaySessionStore playSessionStore;

	private DBApi dbApi;

	private UploadRepository uploadrepo;

	private ActorSystem system;

	private final ActorRef processUploadActor;

	private Cancellable uploadScheduler;
	

	private List<CommonProfile> getProfiles() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.info("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.info("..............Obtained profiles........." + profiles);
		return profiles;
	}

	@Inject
	public ProcessUploadedFiles(DBApi db, PlaySessionStore store, Config conf, UploadRepository repo,
			ActorSystem actorsys) {
		this.config = conf;
		this.dbApi = db;
		this.playSessionStore = store;
		this.uploadrepo = repo;
		this.system = actorsys;
		this.processUploadActor = system.actorOf(ProcessUploadActor.props);
	}

	public CompletionStage<Result> process() {
		return CompletableFuture.supplyAsync(() -> analyzeFile()).thenApply(done -> ok("Got result: " + done));
	}

	@Secure(clients = "FormClient")
	public Result processFile(String id) {
		
		logger.debug("....Got the request to process the file...:" + id);
		Upload upload = uploadrepo.getUpload(Long.parseLong(id));
		String tenantCode = getTenantId();		
		//return CompletableFuture.supplyAsync(() -> {		
		if (upload != null && tenantCode != null) {
			logger.debug("Found upload object for submission");
			FileUploadResponse response = new FileUploadResponse();
			ArrayList files = new ArrayList();
			FileDelete delete = new FileDelete();
			
			delete.setName(upload.getFileName());
			delete.setId(upload.getId());
			files.add(delete);			
			response.setFiles(files);
			
			List<FormData> formdataList = upload.getFormDataList();
			List<Map<String,String>> formdataflattened = new ArrayList<Map<String,String>>();
			formdataList.forEach(data->{
				String name = data.getName();
				String value = data.getValue();
				Map<String,String> nameValues = new HashMap<String,String>();
				nameValues.put(name, value);
				formdataflattened.add(nameValues);
			});

			/*ProcessUploadProtocol.BeginProcessing protocol = new ProcessUploadProtocol.BeginProcessing(dbApi,
					tenantCode, Long.parseLong(id),formdataflattened);
			ExecutionContextExecutor excecutor = system.dispatcher();
			uploadScheduler = system.scheduler().scheduleOnce(Duration.create(10, TimeUnit.MILLISECONDS),
					processUploadActor, protocol, excecutor, null);
			*/
			
			logger.info("formdataflattened : "+formdataflattened.size());
			ProcessUploadProtocolMethod(dbApi,tenantCode, Long.parseLong(id),formdataflattened);
			
			logger.info("process is completed"+toJson(response));
			return ok(toJson(response));
			
		} else {
			logger.debug("Could not find the upload object with the given id");
			flash("error", "Invalid upload id, could not find record");
			return badRequest();
		}
		
		//}).thenApply(done -> ok("Got result: " + done));

	}

	private void ProcessUploadProtocolMethod(DBApi dbapi, String tenantCode, long uploadId,
			List<Map<String, String>> formData) {
		// TODO Auto-generated method stub
		logger.debug("Obtained tenantCode:" + tenantCode);
		logger.debug("Obtained upload Id:" + uploadId);

		DataSource dataSource = dbapi.getDatabase("spring").getDataSource();

		JdbcDataContext jdbcDataContext = new JdbcDataContext(dataSource);

		ArrayList<String> utypidsColl = DataSourceHelper.getSingleColumnWhere(tenantCode, "upload", "uploadType_id",
				"id", uploadId, jdbcDataContext);

		String utypeid = utypidsColl.get(0);

		logger.debug("Obtained uplaod type id:" + utypeid);

		GregorianCalendar cal = new GregorianCalendar();
		Date date = cal.getTime();

		HashMap<String, Object> updates = new HashMap<String, Object>();
		updates.put("processingStartedDT", date);
		updates.put("processingStatus", Upload.PROCESSING_STARTED);
		updates.put("processingStarted", true);
		DataSourceHelper.updateMultiColumns(tenantCode, "upload", updates, "id", uploadId, jdbcDataContext);

		/*
		 * ArrayList<String> utypesColl =
		 * DataSourceHelper.getSingleColumnWhere(tenantCode, "uploadType",
		 * "uploadTypeName", "id", Long.parseLong(utypeid), jdbcDataContext);
		 * 
		 * String utype = utypesColl.get(0);
		 */
		DataSet ds = DataSourceHelper.getAllColumnsFrom(tenantCode, "uploadType", "id", Long.parseLong(utypeid),
				jdbcDataContext);

		HashMap<String, Object> values = new HashMap<String, Object>();
		ds.forEach(row -> {
			SelectItem[] items = row.getSelectItems();
			for (SelectItem item : items) {
				Column col = item.getColumn();
				Object value = row.getValue(col);
				values.put(col.getName(), value);
			}
		});
		String utype = (String) values.get("uploadTypeName");
		String rowsToignore = (String) values.get("rowsToignore");
		Boolean duplicateAllowed = (Boolean) values.get("duplicateAllowed");
		Boolean ignoreSourceColumnNames = (Boolean) values.get("ignoreSourceColumnNames");

		logger.debug("Obtained Upload Type for processing:" + utype);
		logger.debug("Obtained rows to ignore:" + rowsToignore);
		logger.debug("Obtained duplicate allowed:" + duplicateAllowed.booleanValue());
		// Check if the table already exists

		boolean creationstatus = DataSourceHelper.createTable(tenantCode, utype, jdbcDataContext);

		if (creationstatus) {
			DataSourceHelper.populateData(tenantCode, uploadId, utype, rowsToignore, duplicateAllowed,
					ignoreSourceColumnNames, formData, jdbcDataContext);
		}
		
	}

	private boolean analyzeFile() {

		File file = new File("E:\\DAS\\DMS Report Formats\\Hyundai\\SERVICE\\OPERATION WISE ANALYSIS REPORT.xls");

		List<ColumnDefinition> definitions = uploadrepo.getColumnDefinitions("SERVICE_OPERATION_WISE_ANALYSIS_REPORT");
		
		definitions.sort(new Comparator<ColumnDefinition>() {

			@Override
			public int compare(ColumnDefinition o1, ColumnDefinition o2) {
				if(o1.getId() > o2.getId()) return 1;
				if(o1.getId() < o2.getId()) return -1;
				else return 0;
				
			}
			
		});
		
		List<String> columnNames = new ArrayList<String>();
		definitions.forEach(d -> {
			columnNames.add(d.getMetaColumnName());
		});
		
		CustomColumnNamingStrategy strategy = new CustomColumnNamingStrategy(columnNames);
		ExcelConfiguration config = new ExcelConfiguration(0,strategy,true,true);

		ExcelDataContext excelContext = (ExcelDataContext) DataContextFactory.createExcelDataContext(file, config);

		Schema schema = excelContext.getDefaultSchema();

		logger.debug(schema.getName());
		logger.debug(schema.getTables()[0].getName());

		Table table = schema.getTable(0);

		String[] columNames = table.getColumnNames();
		
		for(String col : columnNames){
			System.out.println(col);
		}

		Query query = new Query();

		query.from(table).selectAll();


		DataSet ds = excelContext.executeQuery(query);
		
		Iterator<Row> iterator = ds.iterator();
		
		
		int count =0;
		while(iterator.hasNext()){
			count++;
			if(count ==3) break;
			Row row = iterator.next();
			Object[] values = row.getValues();
			for(Object value : values){
				System.out.println((String)value);
			}
		}
		

			return true;
	}

	private boolean testActorActions() {
		logger.debug("##########Received Messaage to start the process##############");

		// ProcessUploadProtocol.BeginProcessing bpProtocol =
		// (ProcessUploadProtocol.BeginProcessing) protocol;

		DBApi dbapi = this.dbApi;// bpProtocol.dbapi;
		String tenantCode = "advait";// bpProtocol.tenantCode;
		long uploadId = 80;// bpProtocol.uploadId;
		logger.debug("Obtained tenantCode:" + tenantCode);
		logger.debug("Obtained upload Id:" + uploadId);

		DataSource dataSource = dbapi.getDatabase("spring").getDataSource();

		JdbcDataContext jdbcDataContext = new JdbcDataContext(dataSource);

		ArrayList<String> utypidsColl = DataSourceHelper.getSingleColumnWhere(tenantCode, "upload", "uploadType_id",
				"id", uploadId, jdbcDataContext);

		String utypeid = utypidsColl.get(0);

		logger.debug("Obtained uplaod type id:" + utypeid);

		GregorianCalendar cal = new GregorianCalendar();
		Date date = cal.getTime();

		DataSourceHelper.updateSingleColumn(tenantCode, "upload", "processingStartedDT", date, "id", uploadId,
				jdbcDataContext);

		ArrayList<String> utypesColl = DataSourceHelper.getSingleColumnWhere(tenantCode, "uploadType", "uploadTypeName",
				"id", Long.parseLong(utypeid), jdbcDataContext);

		String utype = utypesColl.get(0);
		logger.debug("Obtained Upload Type for processing:" + utype);
		// Check if the table already exists

		boolean creationstatus = DataSourceHelper.createTable(tenantCode, utype, jdbcDataContext);

/*		if (creationstatus) {
			DataSourceHelper.populateData(tenantCode, uploadId, utype, jdbcDataContext);
		}

*/		return true;

	}

	private String getTenantId() {
		List<CommonProfile> profiles = getProfiles();
		if (profiles == null) {
			return null;
		} else if (profiles.get(0) == null) {
			return null;
		} else {
			CommonProfile profile = profiles.get(0);
			String tenantid = (String) profile.getAttribute(MyAppAuthenticator.TENANT_KEY);
			logger.debug("Obtained tenantid:" + tenantid);
			if (tenantid == null) {
				return null;
			} else {
				return tenantid;
			}
		}
	}

}
