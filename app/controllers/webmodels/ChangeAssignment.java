package controllers.webmodels;

import java.util.Date;
import java.util.List;

public class ChangeAssignment {
	
	public String CreName;
	public String finalDisposition_id;
	public String customer_name;
	public Long id;
	public String Mobile_number;
	public String vehicle_RegNo;
	public long vehical_Id;
	public long customer_id;
	public long wyzUser_id;
	public Date uplodedCurrentDate;
	public long campaign_id;
	
	public ChangeAssignment(String creName, String finalDisposition_id, String customer_name, long id,
			String mobile_number, String vehicle_RegNo, long vehical_Id, long customer_id, long wyzUser_id,
			Date uplodedCurrentDate, long campaign_id) {
		super();
		this.CreName = creName;
		this.finalDisposition_id = finalDisposition_id;
		this.customer_name = customer_name;
		this.id = id;
		this.Mobile_number = mobile_number;
		this.vehicle_RegNo = vehicle_RegNo;
		this.vehical_Id = vehical_Id;
		this.customer_id = customer_id;
		this.wyzUser_id = wyzUser_id;
		this.uplodedCurrentDate = uplodedCurrentDate;
		this.campaign_id = campaign_id;
	}
	
	public ChangeAssignment() {
		super();
		this.CreName = "";
		this.finalDisposition_id = "";
		this.customer_name = "";
		this.id = (long) 0;;
		this.Mobile_number = "";
		this.vehicle_RegNo = "";
		this.vehical_Id = (long) 0;;
		this.customer_id = (long) 0;;
		this.wyzUser_id = (long) 0;;
		this.uplodedCurrentDate = null;
		this.campaign_id = (long) 0;;
	}

	public String getCreName() {
		return CreName;
	}

	public void setCreName(String creName) {
		CreName = creName;
	}

	public String getFinalDisposition_id() {
		return finalDisposition_id;
	}

	public void setFinalDisposition_id(String finalDisposition_id) {
		this.finalDisposition_id = finalDisposition_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMobile_number() {
		return Mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		Mobile_number = mobile_number;
	}

	public String getVehicle_RegNo() {
		return vehicle_RegNo;
	}

	public void setVehicle_RegNo(String vehicle_RegNo) {
		this.vehicle_RegNo = vehicle_RegNo;
	}

	public long getVehical_Id() {
		return vehical_Id;
	}

	public void setVehical_Id(long vehical_Id) {
		this.vehical_Id = vehical_Id;
	}

	public long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}

	public long getWyzUser_id() {
		return wyzUser_id;
	}

	public void setWyzUser_id(long wyzUser_id) {
		this.wyzUser_id = wyzUser_id;
	}

	public Date getUplodedCurrentDate() {
		return uplodedCurrentDate;
	}

	public void setUplodedCurrentDate(Date uplodedCurrentDate) {
		this.uplodedCurrentDate = uplodedCurrentDate;
	}

	public long getCampaign_id() {
		return campaign_id;
	}

	public void setCampaign_id(long campaign_id) {
		this.campaign_id = campaign_id;
	}
	
	
	

}
