package controllers.webmodels;

import java.util.Date;

import models.ServiceAdvisor;

public class ServiceAdvisorIdName implements Comparable<ServiceAdvisorIdName> {
	
	

	public Long advisorId;
	public String advisorName;
	public Long priority;
	public Date date;
	public Long workshopId;
	
	public ServiceAdvisorIdName() {
		super();
		this.advisorId = (long) 0;
		this.advisorName = "";
		this.priority = (long) 0;
		this.date = null;
		this.workshopId = (long) 0;
	}
	
	public ServiceAdvisorIdName(Long advisorId, String advisorName, Long priority, Date date, Long workshopId) {
		super();
		this.advisorId = advisorId;
		this.advisorName = advisorName;
		this.priority = priority;
		this.date = date;
		this.workshopId = workshopId;
	}
	
	public Long getAdvisorId() {
		return advisorId;
	}
	public void setAdvisorId(Long advisorId) {
		this.advisorId = advisorId;
	}
	public String getAdvisorName() {
		return advisorName;
	}
	public void setAdvisorName(String advisorName) {
		this.advisorName = advisorName;
	}
	public Long getPriority() {
		return priority;
	}
	public void setPriority(Long priority) {
		this.priority = priority;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Long getWorkshopId() {
		return workshopId;
	}
	public void setWorkshopId(Long workshopId) {
		this.workshopId = workshopId;
	}
	
	@Override
	public int compareTo(ServiceAdvisorIdName sa) {
		if(this.priority>sa.priority) return 1;
		if(this.priority==sa.priority) return 0;
		else return -1;
	}
	@Override
	public boolean equals(Object sa) {
		if (sa == null) return false;
	    if (sa == this) return true;
	    if (!(sa instanceof ServiceAdvisorIdName)) return false;
	    ServiceAdvisorIdName o = (ServiceAdvisorIdName) sa;
	    return o.getAdvisorId() == this.getAdvisorId();
	}
	
}
