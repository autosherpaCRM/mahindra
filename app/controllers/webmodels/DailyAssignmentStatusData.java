package controllers.webmodels;

import java.util.Date;

import javax.persistence.ColumnResult;

public class DailyAssignmentStatusData {

	public String module;
	public String CRE;
	public String campaignName;
	public Date assignedDate;
	public long TotalAssigned;

	
	public DailyAssignmentStatusData(String module,String CRE,String campaignName,Date assignedDate,long TotalAssigned){
		this.module=module;
		this.CRE=CRE;
		this.campaignName=campaignName;
		this.assignedDate=assignedDate;
		this.TotalAssigned=TotalAssigned;
		
	}
	
	public DailyAssignmentStatusData(){
		this.module="";
		this.CRE="";
		this.campaignName="";
		this.assignedDate=null;
		this.TotalAssigned=(long) 0;
	}

	
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getCRE() {
		return CRE;
	}

	public void setCRE(String cRE) {
		CRE = cRE;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	

	public long getTotalAssigned() {
		return TotalAssigned;
	}

	public void setTotalAssigned(long totalAssigned) {
		TotalAssigned = totalAssigned;
	}

	public Date getAssignedDate() {
		return assignedDate;
	}

	public void setAssignedDate(Date assignedDate) {
		this.assignedDate = assignedDate;
	}
	
	
	
}
