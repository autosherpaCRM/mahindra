package controllers.webmodels;

public class MediaModel {
	
	public byte[] mediaFileLob;

	public MediaModel(byte[] mediaFileLob) {
		super();
		this.mediaFileLob = mediaFileLob;
	}
	
	public MediaModel() {
	
		this.mediaFileLob = null;
	}

	public byte[] getMediaFileLob() {
		return mediaFileLob;
	}

	public void setMediaFileLob(byte[] mediaFileLob) {
		this.mediaFileLob = mediaFileLob;
	}
	
	

}
