/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author W-885
 */
public class WorkShopSummaryDataOnTabLoad {
    
         @Temporal(TemporalType.DATE)
        public Date serviceBookedDate;
        
        public int noOfServicesBooked;

    public Date getServiceBookedDate() {
        return serviceBookedDate;
    }

    public void setServiceBookedDate(Date serviceBookedDate) {
        this.serviceBookedDate = serviceBookedDate;
    }

    public int getNoOfServicesBooked() {
        return noOfServicesBooked;
    }

    public void setNoOfServicesBooked(int noOfServicesBooked) {
        this.noOfServicesBooked = noOfServicesBooked;
    }
        
    
}
