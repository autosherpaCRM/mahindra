package controllers.webmodels;

import java.util.Date;




public class CallInteractionHistoryCubeReport {
	
	public String assigned_date;
	public String call_campaign;
	public String call_type;
	public Date call_date;
	public String call_Time;
	public String CRE_Name;
	public String chassis_No;
	public String Veh_Reg_No;
	public String Model;
	public String Customer_Name;
	public String Email_ID;
	public String Preffered_Contact_Number;
	public String Location_DB;
	public String Address_office;
	public String Address_Permanenent;
	public String Address_residential;
	public String forecast_Logic;
	public String ForecastService_Date;
	public String ForecastService_Type;
	public String IsCall_Initiated;
	public String Ring_Time;
	public String call_duration;
	public String Primary_Disposition;
	public String Secondary_Dispostion;
	public String Tertiary_Disposition;
	public String followUp_Date;
	public String followUp_Time;
	public String No_Service_Reason;
	public String fourth_Dispoition;
	public String Already_Serviced_Date;
	public String Verified_with_DMS;
	public String Authorised_workshop_or_Not;
	public String Already_ServicedType;
	public String Already_Serviced_Dealer_Name;
	public String Already_Serviced_Mileage_recorded;
	public String Transferred_City;
	public String pinCode_City;
	public String Lead_Source_For_incoming_call;
	public String current_Mileage;
	public String expected_Visit_Date;
	public String Remarks_1;
	public String Remarks_2;
	public String Remarks_3;
	public String Remarks_nonContact_Others;
	public String Booking_Status;
	public String Reshedule_Status;
	public String Booking_Scheduled_Date;
	public String Booking_Scheduled_Time;
	public String Booking_Service_Type;
	public String Assigned_SA;
	public String PickUp_Option;
	public String Pickup_Driver;
	public String PickUpTime_From;
	public String PickUpTime_Upto;
	public String PickUpAddress;
	public String Booked_Workshop;
	public boolean Reported_Status;
	public String Reported_Date;
	public String Reported_RO;
	public String Last_SAName;
	public String last_service_Date;
	public String last_Service_Type;
	public String Is_Upsell_Done;
	public String customer_category;
	public String Offer_Validity;
	public String Offer_Details;
	public String Offer_Code;
	public String Offer_Benefit;
	public String Offer_Condition;
	public String Is_Feedback_orCompliant;
	public String Complaint_For_Dept;
	public String Compliant_Remark;
	
	
	public CallInteractionHistoryCubeReport(String assigned_date, 
			String call_campaign,String call_type , Date call_date,
			String call_Time, String CRE_Name, String chassis_No,
			String Veh_Reg_No,String Model, String Customer_Name,
			String Email_ID, String Preffered_Contact_Number,
			String Location_DB, String Address_office,
			String Address_Permanenent, String Address_residential,
			String forecast_Logic, String ForecastService_Date,
			String ForecastService_Type, String IsCall_Initiated,
			String Ring_Time, String call_duration,
			String Primary_Disposition, String Secondary_Dispostion,
			String Tertiary_Disposition, String followUp_Date,
			String followUp_Time,
			String No_Service_Reason,
			 String fourth_Dispoition,
			String Already_Serviced_Date,
			String Verified_with_DMS,
			String Authorised_workshop_or_Not,
		String Already_ServicedType,
			 String Already_Serviced_Dealer_Name,
			 String Already_Serviced_Mileage_recorded,
			 String Transferred_City,
			 String pinCode_City,
			 String Lead_Source_For_incoming_call,
			String current_Mileage,
			String expected_Visit_Date, String Remarks_1, String Remarks_2,
			String Remarks_3, String Remarks_nonContact_Others,
			String Booking_Status, String Reshedule_Status, 
			String Booking_Scheduled_Date, String Booking_Scheduled_Time,
			String Booking_Service_Type, String Assigned_SA, String PickUp_Option,
			String Pickup_Driver, String PickUpTime_From, String PickUpTime_Upto,
			String PickUpAddress, String Booked_Workshop, boolean Reported_Status,
			String Reported_Date, String Reported_RO, String Last_SAName,
			String last_service_Date, String last_Service_Type,
			String Is_Upsell_Done, String customer_category,
			String Offer_Validity,String Offer_Details,
			String Offer_Code, String Offer_Benefit,
			String Offer_Condition, String Is_Feedback_orCompliant,
			String Complaint_For_Dept, String Compliant_Remark){
		
		this.assigned_date=assigned_date;
		this.call_campaign=call_campaign;
		this.call_type=call_type;
		this.call_date=call_date;
		this.call_Time=call_Time;
		this.CRE_Name=CRE_Name;
		this.chassis_No=chassis_No;
		this.Veh_Reg_No=Veh_Reg_No;
		this.Model=Model;
		this.Customer_Name=Customer_Name;
		this.Email_ID=Email_ID;
		this.Preffered_Contact_Number=Preffered_Contact_Number;
		this.Location_DB=Location_DB;
		this.Address_office=Address_office;
		this.Address_Permanenent=Address_Permanenent;
		this.Address_residential=Address_residential;
		this.forecast_Logic=forecast_Logic;
		this.ForecastService_Date=ForecastService_Date;
		this.ForecastService_Type=ForecastService_Type;
		this.IsCall_Initiated=IsCall_Initiated;
		this.Ring_Time=Ring_Time;
		this.call_duration=call_duration;
		this.Primary_Disposition=Primary_Disposition;
		this.Secondary_Dispostion=Secondary_Dispostion;
		this.Tertiary_Disposition=Tertiary_Disposition;
		this.followUp_Date=followUp_Date;
		this.followUp_Time=followUp_Time;
		this.No_Service_Reason=No_Service_Reason;
		this.fourth_Dispoition=fourth_Dispoition;
		this.Already_Serviced_Date=Already_Serviced_Date;
		this.Verified_with_DMS=Verified_with_DMS;
		this.Authorised_workshop_or_Not=Authorised_workshop_or_Not;
		this.Already_ServicedType=Already_ServicedType;
		 this.Already_Serviced_Dealer_Name=Already_Serviced_Dealer_Name;
		 this.Already_Serviced_Mileage_recorded=Already_Serviced_Mileage_recorded;
		 this.Transferred_City=Transferred_City;
		this.pinCode_City=pinCode_City;
		 this.Lead_Source_For_incoming_call=Lead_Source_For_incoming_call;
		this.current_Mileage=current_Mileage;
		this.expected_Visit_Date=expected_Visit_Date;
		this.Remarks_1=Remarks_1;
		this.Remarks_2=Remarks_2;
		this.Remarks_3=Remarks_3;
		this.Remarks_nonContact_Others=Remarks_nonContact_Others;
		this.Booking_Status=Booking_Status;
		this.Reshedule_Status=Reshedule_Status;
		this.Booking_Scheduled_Date=Booking_Scheduled_Date;
		this.Booking_Scheduled_Time=Booking_Scheduled_Time;
		this.Booking_Service_Type=Booking_Service_Type;
		this.Assigned_SA=Assigned_SA;
		this.PickUp_Option=PickUp_Option;
		this.Pickup_Driver=Pickup_Driver;
		this.PickUpTime_From=PickUpTime_From;
		this.PickUpTime_Upto=PickUpTime_Upto;
		this.PickUpAddress=PickUpAddress;
		this.Booked_Workshop=Booked_Workshop;
		this.Reported_Status=Reported_Status;
		this.Reported_Date=Reported_Date;
		this.Reported_RO=Reported_RO;
		this.Last_SAName=Last_SAName;
		this.last_service_Date=last_service_Date;
		this.last_Service_Type=last_Service_Type;
		this.Is_Upsell_Done=Is_Upsell_Done;
		this.customer_category=customer_category;
		this.Offer_Validity=Offer_Validity;
		this.Offer_Details=Offer_Details;
		this.Offer_Code=Offer_Code;
		this.Offer_Benefit=Offer_Benefit;
		this.Offer_Condition=Offer_Condition;
		this.Is_Feedback_orCompliant=Is_Feedback_orCompliant;
		this.Complaint_For_Dept=Complaint_For_Dept;
		this.Compliant_Remark=Compliant_Remark;
		
	}
	

	public CallInteractionHistoryCubeReport(){
		this.assigned_date="";
		this.call_campaign="";
		this.call_type="";
		this.call_date=null;
		this.call_Time="";
		this.CRE_Name="";
		this.chassis_No="";
		this.Veh_Reg_No="";
		this.Model="";
		this.Customer_Name="";
		this.Email_ID="";
		this.Preffered_Contact_Number="";
		this.Location_DB="";
		this.Address_office="";
		this.Address_Permanenent="";
		this.Address_residential="";
		this.forecast_Logic="";
		this.ForecastService_Date="";
		this.ForecastService_Type="";
		this.IsCall_Initiated="";
		this.Ring_Time="";
		this.call_duration="";
		this.Primary_Disposition="";
		this.Secondary_Dispostion="";
		this.Tertiary_Disposition="";
		this.followUp_Date="";
		this.followUp_Time="";
		this.No_Service_Reason="";
		this.fourth_Dispoition="";
		this.Already_Serviced_Date="";
		this.Verified_with_DMS="";
		this.Authorised_workshop_or_Not="";
		this.Already_ServicedType="";
		 this.Already_Serviced_Dealer_Name="";
		 this.Already_Serviced_Mileage_recorded="";
		 this.Transferred_City="";
		this.pinCode_City="";
		 this.Lead_Source_For_incoming_call="";
		this.current_Mileage="";
		this.expected_Visit_Date="";
		this.Remarks_1="";
		this.Remarks_2="";
		this.Remarks_3="";
		this.Remarks_nonContact_Others="";
		this.Booking_Status="";
		this.Reshedule_Status="";
		this.Booking_Scheduled_Date="";
		this.Booking_Scheduled_Time="";
		this.Booking_Service_Type="";
		this.Assigned_SA="";
		this.PickUp_Option="";
		this.Pickup_Driver="";
		this.PickUpTime_From="";
		this.PickUpTime_Upto="";
		this.PickUpAddress="";
		this.Booked_Workshop="";
		this.Reported_Status=false;
		this.Reported_Date="";
		this.Reported_RO="";
		this.Last_SAName="";
		this.last_service_Date="";
		this.last_Service_Type="";
		this.Is_Upsell_Done="";
		this.customer_category="";
		this.Offer_Validity="";
		this.Offer_Details="";
		this.Offer_Code="";
		this.Offer_Benefit="";
		this.Offer_Condition="";
		this.Is_Feedback_orCompliant="";
		this.Complaint_For_Dept="";
		this.Compliant_Remark="";
	}

	

	public String getFourth_Dispoition() {
		return fourth_Dispoition;
	}


	public void setFourth_Dispoition(String fourth_Dispoition) {
		this.fourth_Dispoition = fourth_Dispoition;
	}


	public String getAssigned_date() {
		return assigned_date;
	}


	public void setAssigned_date(String assigned_date) {
		this.assigned_date = assigned_date;
	}


	public String getCall_campaign() {
		return call_campaign;
	}


	public void setCall_campaign(String call_campaign) {
		this.call_campaign = call_campaign;
	}


	public String getCall_type() {
		return call_type;
	}


	public void setCall_type(String call_type) {
		this.call_type = call_type;
	}


	public Date getCall_date() {
		return call_date;
	}


	public void setCall_date(Date call_date) {
		this.call_date = call_date;
	}


	public String getCall_Time() {
		return call_Time;
	}


	public void setCall_Time(String call_Time) {
		this.call_Time = call_Time;
	}


	public String getCRE_Name() {
		return CRE_Name;
	}


	public void setCRE_Name(String cRE_Name) {
		CRE_Name = cRE_Name;
	}


	public String getChassis_No() {
		return chassis_No;
	}


	public void setChassis_No(String chassis_No) {
		this.chassis_No = chassis_No;
	}


	public String getVeh_Reg_No() {
		return Veh_Reg_No;
	}


	public void setVeh_Reg_No(String veh_Reg_No) {
		Veh_Reg_No = veh_Reg_No;
	}


	public String getModel() {
		return Model;
	}


	public void setModel(String model) {
		Model = model;
	}


	public String getCustomer_Name() {
		return Customer_Name;
	}


	public void setCustomer_Name(String customer_Name) {
		Customer_Name = customer_Name;
	}


	public String getEmail_ID() {
		return Email_ID;
	}


	public void setEmail_ID(String email_ID) {
		Email_ID = email_ID;
	}


	public String getPreffered_Contact_Number() {
		return Preffered_Contact_Number;
	}


	public void setPreffered_Contact_Number(String preffered_Contact_Number) {
		Preffered_Contact_Number = preffered_Contact_Number;
	}


	public String getLocation_DB() {
		return Location_DB;
	}


	public void setLocation_DB(String location_DB) {
		Location_DB = location_DB;
	}


	public String getAddress_office() {
		return Address_office;
	}


	public void setAddress_office(String address_office) {
		Address_office = address_office;
	}


	public String getAddress_Permanenent() {
		return Address_Permanenent;
	}


	public void setAddress_Permanenent(String address_Permanenent) {
		Address_Permanenent = address_Permanenent;
	}


	public String getAddress_residential() {
		return Address_residential;
	}


	public void setAddress_residential(String address_residential) {
		Address_residential = address_residential;
	}


	public String getForecast_Logic() {
		return forecast_Logic;
	}


	public void setForecast_Logic(String forecast_Logic) {
		this.forecast_Logic = forecast_Logic;
	}


	public String getForecastService_Date() {
		return ForecastService_Date;
	}


	public void setForecastService_Date(String forecastService_Date) {
		ForecastService_Date = forecastService_Date;
	}


	public String getForecastService_Type() {
		return ForecastService_Type;
	}


	public void setForecastService_Type(String forecastService_Type) {
		ForecastService_Type = forecastService_Type;
	}


	public String getIsCall_Initiated() {
		return IsCall_Initiated;
	}


	public void setIsCall_Initiated(String isCall_Initiated) {
		IsCall_Initiated = isCall_Initiated;
	}


	public String getRing_Time() {
		return Ring_Time;
	}


	public void setRing_Time(String ring_Time) {
		Ring_Time = ring_Time;
	}


	public String getCall_duration() {
		return call_duration;
	}


	public void setCall_duration(String call_duration) {
		this.call_duration = call_duration;
	}


	public String getPrimary_Disposition() {
		return Primary_Disposition;
	}


	public void setPrimary_Disposition(String primary_Disposition) {
		Primary_Disposition = primary_Disposition;
	}


	public String getSecondary_Dispostion() {
		return Secondary_Dispostion;
	}


	public void setSecondary_Dispostion(String secondary_Dispostion) {
		Secondary_Dispostion = secondary_Dispostion;
	}


	public String getTertiary_Disposition() {
		return Tertiary_Disposition;
	}


	public void setTertiary_Disposition(String tertiary_Disposition) {
		Tertiary_Disposition = tertiary_Disposition;
	}


	public String getFollowUp_Date() {
		return followUp_Date;
	}


	public void setFollowUp_Date(String followUp_Date) {
		this.followUp_Date = followUp_Date;
	}


	public String getFollowUp_Time() {
		return followUp_Time;
	}


	public void setFollowUp_Time(String followUp_Time) {
		this.followUp_Time = followUp_Time;
	}

	

	public String getNo_Service_Reason() {
		return No_Service_Reason;
	}


	public void setNo_Service_Reason(String no_Service_Reason) {
		No_Service_Reason = no_Service_Reason;
	}


	public String getAlready_Serviced_Date() {
		return Already_Serviced_Date;
	}


	public void setAlready_Serviced_Date(String already_Serviced_Date) {
		Already_Serviced_Date = already_Serviced_Date;
	}


	public String getVerified_with_DMS() {
		return Verified_with_DMS;
	}


	public void setVerified_with_DMS(String verified_with_DMS) {
		Verified_with_DMS = verified_with_DMS;
	}


	public String getAuthorised_workshop_or_Not() {
		return Authorised_workshop_or_Not;
	}


	public void setAuthorised_workshop_or_Not(String authorised_workshop_or_Not) {
		Authorised_workshop_or_Not = authorised_workshop_or_Not;
	}


	public String getAlready_ServicedType() {
		return Already_ServicedType;
	}


	public void setAlready_ServicedType(String already_ServicedType) {
		Already_ServicedType = already_ServicedType;
	}


	public String getAlready_Serviced_Dealer_Name() {
		return Already_Serviced_Dealer_Name;
	}


	public void setAlready_Serviced_Dealer_Name(String already_Serviced_Dealer_Name) {
		Already_Serviced_Dealer_Name = already_Serviced_Dealer_Name;
	}


	public String getAlready_Serviced_Mileage_recorded() {
		return Already_Serviced_Mileage_recorded;
	}


	public void setAlready_Serviced_Mileage_recorded(String already_Serviced_Mileage_recorded) {
		Already_Serviced_Mileage_recorded = already_Serviced_Mileage_recorded;
	}


	public String getTransferred_City() {
		return Transferred_City;
	}


	public void setTransferred_City(String transferred_City) {
		Transferred_City = transferred_City;
	}


	public String getPinCode_City() {
		return pinCode_City;
	}


	public void setPinCode_City(String pinCode_City) {
		this.pinCode_City = pinCode_City;
	}


	public String getLead_Source_For_incoming_call() {
		return Lead_Source_For_incoming_call;
	}


	public void setLead_Source_For_incoming_call(String lead_Source_For_incoming_call) {
		Lead_Source_For_incoming_call = lead_Source_For_incoming_call;
	}


	public String getCurrent_Mileage() {
		return current_Mileage;
	}


	public void setCurrent_Mileage(String current_Mileage) {
		this.current_Mileage = current_Mileage;
	}


	public String getExpected_Visit_Date() {
		return expected_Visit_Date;
	}


	public void setExpected_Visit_Date(String expected_Visit_Date) {
		this.expected_Visit_Date = expected_Visit_Date;
	}


	public String getRemarks_1() {
		return Remarks_1;
	}


	public void setRemarks_1(String remarks_1) {
		Remarks_1 = remarks_1;
	}


	public String getRemarks_2() {
		return Remarks_2;
	}


	public void setRemarks_2(String remarks_2) {
		Remarks_2 = remarks_2;
	}


	public String getRemarks_3() {
		return Remarks_3;
	}


	public void setRemarks_3(String remarks_3) {
		Remarks_3 = remarks_3;
	}


	public String getRemarks_nonContact_Others() {
		return Remarks_nonContact_Others;
	}


	public void setRemarks_nonContact_Others(String remarks_nonContact_Others) {
		Remarks_nonContact_Others = remarks_nonContact_Others;
	}


	public String getBooking_Status() {
		return Booking_Status;
	}


	public void setBooking_Status(String booking_Status) {
		Booking_Status = booking_Status;
	}


	



	public String getReshedule_Status() {
		return Reshedule_Status;
	}


	public void setReshedule_Status(String reshedule_Status) {
		Reshedule_Status = reshedule_Status;
	}


	public String getBooking_Scheduled_Date() {
		return Booking_Scheduled_Date;
	}


	public void setBooking_Scheduled_Date(String booking_Scheduled_Date) {
		Booking_Scheduled_Date = booking_Scheduled_Date;
	}


	public String getBooking_Scheduled_Time() {
		return Booking_Scheduled_Time;
	}


	public void setBooking_Scheduled_Time(String booking_Scheduled_Time) {
		Booking_Scheduled_Time = booking_Scheduled_Time;
	}


	public String getBooking_Service_Type() {
		return Booking_Service_Type;
	}


	public void setBooking_Service_Type(String booking_Service_Type) {
		Booking_Service_Type = booking_Service_Type;
	}


	public String getAssigned_SA() {
		return Assigned_SA;
	}


	public void setAssigned_SA(String assigned_SA) {
		Assigned_SA = assigned_SA;
	}


	public String getPickUp_Option() {
		return PickUp_Option;
	}


	public void setPickUp_Option(String pickUp_Option) {
		PickUp_Option = pickUp_Option;
	}


	public String getPickup_Driver() {
		return Pickup_Driver;
	}


	public void setPickup_Driver(String pickup_Driver) {
		Pickup_Driver = pickup_Driver;
	}


	public String getPickUpTime_From() {
		return PickUpTime_From;
	}


	public void setPickUpTime_From(String pickUpTime_From) {
		PickUpTime_From = pickUpTime_From;
	}


	public String getPickUpTime_Upto() {
		return PickUpTime_Upto;
	}


	public void setPickUpTime_Upto(String pickUpTime_Upto) {
		PickUpTime_Upto = pickUpTime_Upto;
	}


	public String getPickUpAddress() {
		return PickUpAddress;
	}


	public void setPickUpAddress(String pickUpAddress) {
		PickUpAddress = pickUpAddress;
	}


	public String getBooked_Workshop() {
		return Booked_Workshop;
	}


	public void setBooked_Workshop(String booked_Workshop) {
		Booked_Workshop = booked_Workshop;
	}




	public boolean isReported_Status() {
		return Reported_Status;
	}


	public void setReported_Status(boolean reported_Status) {
		Reported_Status = reported_Status;
	}


	public String getReported_Date() {
		return Reported_Date;
	}


	public void setReported_Date(String reported_Date) {
		Reported_Date = reported_Date;
	}


	public String getReported_RO() {
		return Reported_RO;
	}


	public void setReported_RO(String reported_RO) {
		Reported_RO = reported_RO;
	}


	public String getLast_SAName() {
		return Last_SAName;
	}


	public void setLast_SAName(String last_SAName) {
		Last_SAName = last_SAName;
	}


	public String getLast_service_Date() {
		return last_service_Date;
	}


	public void setLast_service_Date(String last_service_Date) {
		this.last_service_Date = last_service_Date;
	}


	public String getLast_Service_Type() {
		return last_Service_Type;
	}


	public void setLast_Service_Type(String last_Service_Type) {
		this.last_Service_Type = last_Service_Type;
	}


	public String getIs_Upsell_Done() {
		return Is_Upsell_Done;
	}


	public void setIs_Upsell_Done(String is_Upsell_Done) {
		Is_Upsell_Done = is_Upsell_Done;
	}


	public String getCustomer_category() {
		return customer_category;
	}


	public void setCustomer_category(String customer_category) {
		this.customer_category = customer_category;
	}


	public String getOffer_Validity() {
		return Offer_Validity;
	}


	public void setOffer_Validity(String offer_Validity) {
		Offer_Validity = offer_Validity;
	}


	public String getOffer_Details() {
		return Offer_Details;
	}


	public void setOffer_Details(String offer_Details) {
		Offer_Details = offer_Details;
	}


	public String getOffer_Code() {
		return Offer_Code;
	}


	public void setOffer_Code(String offer_Code) {
		Offer_Code = offer_Code;
	}


	public String getOffer_Benefit() {
		return Offer_Benefit;
	}


	public void setOffer_Benefit(String offer_Benefit) {
		Offer_Benefit = offer_Benefit;
	}


	public String getOffer_Condition() {
		return Offer_Condition;
	}


	public void setOffer_Condition(String offer_Condition) {
		Offer_Condition = offer_Condition;
	}


	public String getIs_Feedback_orCompliant() {
		return Is_Feedback_orCompliant;
	}


	public void setIs_Feedback_orCompliant(String is_Feedback_orCompliant) {
		Is_Feedback_orCompliant = is_Feedback_orCompliant;
	}


	public String getComplaint_For_Dept() {
		return Complaint_For_Dept;
	}


	public void setComplaint_For_Dept(String complaint_For_Dept) {
		Complaint_For_Dept = complaint_For_Dept;
	}


	public String getCompliant_Remark() {
		return Compliant_Remark;
	}


	public void setCompliant_Remark(String compliant_Remark) {
		Compliant_Remark = compliant_Remark;
	}
	
	
	

}
