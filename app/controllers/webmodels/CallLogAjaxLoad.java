/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

import java.util.Date;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;

public class CallLogAjaxLoad {

	private String customer_name;
	private String vehicle_RegNo;
	private String veh_model;
	private String category;
	private String Loyalty;
	private String duedate;
	private String due_type;
	private String forecast_type;
	private String last_psfstatus;
	private String DND_status;
	private String campaignName;
	private String complaints_count;
	private String vehical_Id;
	private String wyzUser_id;
	private String customer_id;
	private String assigned_intercation_id;
	private String RONumber;
	private Date ROdate;
	private Date BillDate;
	private String Mobile_number;
	private String model;
	private String vehicle_id;
	private String callinteraction_id;
	

	public CallLogAjaxLoad(String customer_name,String Mobile_number,String vehicle_RegNo ,String model,
			String RONumber,Date ROdate,Date BillDate, String vehicle_id, String customer_id,
			String category,String assigned_intercation_id){
	    	this.customer_name = customer_name;
	    	this.Mobile_number = Mobile_number;
	    	this.vehicle_RegNo=vehicle_RegNo;
	    	this.model= model;
	    	this.RONumber= RONumber;
	    	this.ROdate=ROdate;
	    	this.BillDate=BillDate;
	    	this.vehicle_id=vehicle_id;
	    	this.customer_id=customer_id;
	    	this.category=category;
	    	this.assigned_intercation_id=assigned_intercation_id;
	    }
	public CallLogAjaxLoad(String customer_name, String vehicle_RegNo, String veh_model, String category,
			String Loyalty, String duedate, String due_type, String forecast_type, String last_psfstatus,
			String DND_status, String campaignName, String complaints_count, String vehical_Id, String wyzUser_id,
			String customer_id, String assigned_intercation_id) {
		this.customer_name = customer_name;
		this.vehicle_RegNo = vehicle_RegNo;
		this.veh_model = veh_model;
		this.category = category;
		this.Loyalty = Loyalty;
		this.duedate = duedate;
		this.due_type = due_type;
		this.forecast_type = forecast_type;
		this.last_psfstatus = last_psfstatus;
		this.DND_status = DND_status;
		this.campaignName = campaignName;
		this.complaints_count = complaints_count;
		this.vehical_Id = vehical_Id;
		this.wyzUser_id = wyzUser_id;
		this.customer_id = customer_id;
		this.assigned_intercation_id = assigned_intercation_id;
	}

	public CallLogAjaxLoad() {
		this.customer_name = "";
		this.vehicle_RegNo = "";
		this.veh_model = "";
		this.category = "";
		this.Loyalty = "";
		this.duedate = "";
		this.due_type = "";
		this.forecast_type = "";
		this.last_psfstatus = "";
		this.DND_status = "";
		this.campaignName = "";
		this.complaints_count = "";
		this.vehical_Id = "";
		this.wyzUser_id = "";
		this.customer_id = "";
		this.assigned_intercation_id = "";
	}

	
	public String getVehicle_id() {
		return vehicle_id;
	}
	public void setVehicle_id(String vehicle_id) {
		this.vehicle_id = vehicle_id;
	}
	public String getCallinteraction_id() {
		return callinteraction_id;
	}
	public void setCallinteraction_id(String callinteraction_id) {
		this.callinteraction_id = callinteraction_id;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getRONumber() {
		return RONumber;
	}
	public void setRONumber(String rONumber) {
		RONumber = rONumber;
	}
	public Date getROdate() {
		return ROdate;
	}
	public void setROdate(Date rOdate) {
		ROdate = rOdate;
	}
	public Date getBillDate() {
		return BillDate;
	}
	public void setBillDate(Date billDate) {
		BillDate = billDate;
	}
	public String getMobile_number() {
		return Mobile_number;
	}
	public void setMobile_number(String mobile_number) {
		Mobile_number = mobile_number;
	}
	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getVehicle_RegNo() {
		return vehicle_RegNo;
	}

	public void setVehicle_RegNo(String vehicle_RegNo) {
		this.vehicle_RegNo = vehicle_RegNo;
	}

	public String getVeh_model() {
		return veh_model;
	}

	public void setVeh_model(String veh_model) {
		this.veh_model = veh_model;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getLoyalty() {
		return Loyalty;
	}

	public void setLoyalty(String Loyalty) {
		this.Loyalty = Loyalty;
	}

	public String getDuedate() {
		return duedate;
	}

	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}

	public String getDue_type() {
		return due_type;
	}

	public void setDue_type(String due_type) {
		this.due_type = due_type;
	}

	public String getForecast_type() {
		return forecast_type;
	}

	public void setForecast_type(String forecast_type) {
		this.forecast_type = forecast_type;
	}

	public String getLast_psfstatus() {
		return last_psfstatus;
	}

	public void setLast_psfstatus(String last_psfstatus) {
		this.last_psfstatus = last_psfstatus;
	}

	public String getDND_status() {
		return DND_status;
	}

	public void setDND_status(String DND_status) {
		this.DND_status = DND_status;
	}

	public String getComplaints_count() {
		return complaints_count;
	}

	public void setComplaints_count(String complaints_count) {
		this.complaints_count = complaints_count;
	}

	public String getVehical_Id() {
		return vehical_Id;
	}

	public void setVehical_Id(String vehical_Id) {
		this.vehical_Id = vehical_Id;
	}

	public String getWyzUser_id() {
		return wyzUser_id;
	}

	public void setWyzUser_id(String wyzUser_id) {
		this.wyzUser_id = wyzUser_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getAssigned_intercation_id() {
		return assigned_intercation_id;
	}

	public void setAssigned_intercation_id(String assigned_intercation_id) {
		this.assigned_intercation_id = assigned_intercation_id;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	
}
