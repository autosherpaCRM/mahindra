package controllers.webmodels;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class CallRecordingDataTable {

	private long callinteraction_id;
	private long customer_id;
	private long vehicle_vehicle_id;
	@Temporal(TemporalType.DATE)
	private Date callDate;	
	private String callTime;
	private String callType;
	private String isCallinitaited;
	private String userName;
	private String campaignName;
	private String customerName;
	private String phoneNumber;
	private String vehicleRegNo;
	private String chassisNo;
	private String Disposition;
	private String media;

	public CallRecordingDataTable(long callinteraction_id, long customer_id, long vehicle_vehicle_id, Date callDate,
			String callTime, String callType, String isCallinitaited, String userName, String campaignName,
			String customerName, String phoneNumber, String vehicleRegNo, String chassisNo, String disposition,
			String media) {

		this.callinteraction_id = callinteraction_id;
		this.customer_id = customer_id;
		this.vehicle_vehicle_id = vehicle_vehicle_id;
		this.callDate = callDate;
		this.callTime = callTime;
		this.callType = callType;
		this.isCallinitaited = isCallinitaited;
		this.userName = userName;
		this.campaignName = campaignName;
		this.customerName = customerName;
		this.phoneNumber = phoneNumber;
		this.vehicleRegNo = vehicleRegNo;
		this.chassisNo = chassisNo;
		this.Disposition = disposition;
		this.media = media;
	}

	public CallRecordingDataTable() {

		this.callinteraction_id = (long) 0;
		this.customer_id = (long) 0;
		this.vehicle_vehicle_id = (long) 0;
		this.callDate = null;
		this.callTime = "";
		this.callType = "";
		this.isCallinitaited = "";
		this.userName = "";
		this.campaignName = "";
		this.customerName = "";
		this.phoneNumber = "";
		this.vehicleRegNo = "";
		this.chassisNo = "";
		this.Disposition = "";
		this.media = "";
	}

	public long getCallinteraction_id() {
		return callinteraction_id;
	}

	public void setCallinteraction_id(long callinteraction_id) {
		this.callinteraction_id = callinteraction_id;
	}

	public long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}

	public long getVehicle_vehicle_id() {
		return vehicle_vehicle_id;
	}

	public void setVehicle_vehicle_id(long vehicle_vehicle_id) {
		this.vehicle_vehicle_id = vehicle_vehicle_id;
	}

	public Date getCallDate() {
		return callDate;
	}

	public void setCallDate(Date callDate) {
		this.callDate = callDate;
	}

	public String getCallTime() {
		return callTime;
	}

	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}

	public String getIsCallinitaited() {
		return isCallinitaited;
	}

	public void setIsCallinitaited(String isCallinitaited) {
		this.isCallinitaited = isCallinitaited;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public String getDisposition() {
		return Disposition;
	}

	public void setDisposition(String disposition) {
		Disposition = disposition;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

}
