/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

/**
 *
 * @author W-885
 */
public class CallInteractionDataOnTabLoad {
    
    public long id;
    public String dealerCode;
    public String callDate;
    public String callTime;
    public String makeCallFrom;
    public String callType;

    public String getCallDate() {
        return callDate;
    }

    public void setCallDate(String callDate) {
        this.callDate = callDate;
    }

    public String getCallTime() {
        return callTime;
    }

    public void setCallTime(String callTime) {
        this.callTime = callTime;
    }

    public String getMakeCallFrom() {
        return makeCallFrom;
    }

    public void setMakeCallFrom(String makeCallFrom) {
        this.makeCallFrom = makeCallFrom;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}
    
    
    
}
