/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

import java.util.List;

import models.BookingDateTime;
import models.WyzUser;

/**
 *
 * @author W-885
 */
public class DriverDataOnTabLoad {
	public List<PickupDropDataOnTabLoad> driverListData;
	public List<BookingDateTime> timeSlotData;
	public List<PickupDropDataOnTabLoad> getDriverListData() {
		return driverListData;
	}
	public void setDriverListData(List<PickupDropDataOnTabLoad> driverListData) {
		this.driverListData = driverListData;
	}
	public List<BookingDateTime> getTimeSlotData() {
		return timeSlotData;
	}
	public void setTimeSlotData(List<BookingDateTime> timeSlotData) {
		this.timeSlotData = timeSlotData;
	}
    
	
	
}
