package controllers.webmodels;

import java.util.Date;




public class CallInteractionHistory {
	
	public String Location;
	public String wyzUser_id;
	public String Cre_Name;
	public String callMadeDateAndTime;
	public String callTime;
	public String callDate;
	public String preffered_Contact_number;
	public String callType;
	public String Customer_name;
	public String DOb;
	public String office_address;
	public String residential_address;
	public String permenant_address;
	public String Email_id;
	public String customer_category;
	public String customer_city;
	public String callDuration;
	public String customer_remarks;
	public String Veh_Reg_no;
	public String chassisNo;
	public String Model;
	public String fueltype;
	public String Variant;
	public String last_serviceDate;
	public String lastServiceType;
	public String nextServicedate;
	public String nextServicetype;
	public String forecastLogic;
	public String previous_disposition;
	public String primary_disposition;
	public String secondary_disposition;
	public String secondary_dispostion;
	public String Tertiary_disposition;
	public String serviceType;
	public String psf_status;
	public String media_avl;
	public long   Call_interaction_id;
	public long   sr_disposition_id;
	public String calling_data_type;
	public String typeofpickup;
	public String fromtimeofpick;
	public String upto;
	public String driver;
	public String service_advisor;
	public String upselltype;
	public long   assignedInteraction_id;
	public Date   assigned_date;
	public String isCallinitaited;
	
	public String interactionDate;
		public String interactionTime;
		public String smsMessage;
		public String CustomerName;
		public String VehicleRegNo;
		public String WyzuserName;
		public boolean smsStatus;
		public String mobileNumber;
	
	
	
	
	public CallInteractionHistory() {
		this.Location = "";
		this.wyzUser_id = "";
		this.Cre_Name = "";
		this.callMadeDateAndTime = "";
		this.callTime = "";
		this.callDate = "";
		this.preffered_Contact_number = "";
		this.callType = "";
		this.Customer_name = "";
		this.DOb = "";
		this.office_address = "";
		this.residential_address = "";
		this.permenant_address = "";
		this.Email_id = "";
		this.customer_category = "";
		this.customer_city = "";
		this.callDuration = "";
		this.customer_remarks="";
		this.Veh_Reg_no = "";
		this.chassisNo = "";
		this.Model = "";
		this.fueltype = "";
		this.Variant = "";
		this.last_serviceDate = "";
		this.lastServiceType = "";
		this.nextServicedate = "";
		this.nextServicetype = "";
		this.forecastLogic = "";
		this.previous_disposition = "";
		this.primary_disposition = "";
		this.secondary_disposition = "";
		this.secondary_dispostion="";
		this.Tertiary_disposition = "";
		this.serviceType = "";
		this.psf_status = "";
		this.Call_interaction_id =(long) 0;
		this.sr_disposition_id=(long) 0;
		this.calling_data_type="";
		this.typeofpickup="";
		this.fromtimeofpick="";
		this.upto="";
		this.driver="";
		this.service_advisor="";
		this.upselltype="";
		this.assigned_date=null;
		this.isCallinitaited="";
		this.interactionDate="";
		this.interactionTime="";
		this.smsMessage="";
		this.CustomerName="";
		this.VehicleRegNo="";
		this.WyzuserName="";
		this.smsStatus=false;
		this.mobileNumber="";
		
		
		}

	
	public CallInteractionHistory(String interactionDate, String interactionTime, String smsMessage, boolean smsStatus, String CustomerName, String VehicleRegNo, String WyzuserName,String mobileNumber){
		this.interactionDate=interactionDate;
				this.interactionTime=interactionTime;
				this.smsMessage=smsMessage;
				this.smsStatus=smsStatus;
				this.CustomerName=CustomerName;
				this.VehicleRegNo=VehicleRegNo;
				this.WyzuserName=WyzuserName;
				this.mobileNumber=mobileNumber;
		
			}
	public CallInteractionHistory(String Location, String wyzUser_id, String Cre_Name, String callMadeDateAndTime,
			String callTime, String callDate, String preffered_Contact_number, String callType, String Customer_name,
			String DOb, String office_address, String residential_address, String permenant_address, String Email_id
			,String customer_category, String customer_city, String callDuration,String customer_remarks, String Veh_Reg_no, String chassisNo
			,String Model, String fueltype, String Variant, String last_serviceDate, String lastServiceType,String nextServicedate,
			String nextServicetype, String forecastLogic, String previous_disposition,String primary_disposition, String secondary_dispostion,
			String Tertiary_disposition, String serviceType,String psf_status,Long Call_interaction_id, Long sr_disposition_id, 
			String calling_data_type, String typeofpickup,String fromtimeofpick,String upto, String driver,String service_advisor,
			String upselltype,Date assigned_date, String isCallinitaited){
		this.Location = Location;
		this.wyzUser_id = wyzUser_id;
		this.Cre_Name = Cre_Name;
		this.callMadeDateAndTime = callMadeDateAndTime;
		this.callTime = callTime;
		this.callDate = callDate;
		this.preffered_Contact_number = preffered_Contact_number;
		this.callType = callType;
		this.Customer_name = Customer_name;
		this.DOb = DOb;
		this.office_address = office_address;
		this.residential_address = residential_address;
		this.permenant_address = permenant_address;
		this.Email_id = Email_id;
		this.customer_category = customer_category;
		this.customer_city = customer_city;
		this.callDuration = callDuration;
		this.customer_remarks=customer_remarks;
		this.Veh_Reg_no = Veh_Reg_no;
		this.chassisNo = chassisNo;
		this.Model = Model;
		this.fueltype = fueltype;
		this.Variant = Variant;
		this.last_serviceDate = last_serviceDate;
		this.lastServiceType = lastServiceType;
		this.nextServicedate = nextServicedate;
		this.nextServicetype = nextServicetype;
		this.forecastLogic = forecastLogic;
		this.previous_disposition = previous_disposition;
		this.primary_disposition = primary_disposition;
		this.secondary_disposition = secondary_dispostion;
		this.Tertiary_disposition = Tertiary_disposition;
		this.serviceType = serviceType;
		this.psf_status = psf_status;
		this.Call_interaction_id = Call_interaction_id;
		this.sr_disposition_id=sr_disposition_id;
		this.calling_data_type=calling_data_type;
		this.typeofpickup=typeofpickup;
		this.fromtimeofpick=fromtimeofpick;
		this.upto=upto;
		this.driver=driver;
		this.service_advisor=service_advisor;
		this.upselltype=upselltype;
		this.assigned_date=assigned_date;
		this.isCallinitaited=isCallinitaited;
	}
	
	

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public String getWyzUser_id() {
		return wyzUser_id;
	}

	public void setWyzUser_id(String wyzUser_id) {
		this.wyzUser_id = wyzUser_id;
	}

	public String getCre_Name() {
		return Cre_Name;
	}

	public void setCre_Name(String cre_Name) {
		Cre_Name = cre_Name;
	}

	public String getCallMadeDateAndTime() {
		return callMadeDateAndTime;
	}

	public void setCallMadeDateAndTime(String callMadeDateAndTime) {
		this.callMadeDateAndTime = callMadeDateAndTime;
	}

	public String getCallTime() {
		return callTime;
	}

	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}

	public String getCallDate() {
		return callDate;
	}

	public void setCallDate(String callDate) {
		this.callDate = callDate;
	}

	public String getPreffered_Contact_number() {
		return preffered_Contact_number;
	}

	public void setPreffered_Contact_number(String preffered_Contact_number) {
		this.preffered_Contact_number = preffered_Contact_number;
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}

	public String getCustomer_name() {
		return Customer_name;
	}

	public void setCustomer_name(String customer_name) {
		Customer_name = customer_name;
	}

	public String getDOb() {
		return DOb;
	}

	public void setDOb(String dOb) {
		DOb = dOb;
	}

	public String getOffice_address() {
		return office_address;
	}

	public void setOffice_address(String office_address) {
		this.office_address = office_address;
	}

	public String getResidential_address() {
		return residential_address;
	}

	public void setResidential_address(String residential_address) {
		this.residential_address = residential_address;
	}

	public String getPermenant_address() {
		return permenant_address;
	}

	public void setPermenant_address(String permenant_address) {
		this.permenant_address = permenant_address;
	}

	public String getEmail_id() {
		return Email_id;
	}

	public void setEmail_id(String email_id) {
		Email_id = email_id;
	}

	public String getCustomer_category() {
		return customer_category;
	}

	public void setCustomer_category(String customer_category) {
		this.customer_category = customer_category;
	}

	public String getCustomer_city() {
		return customer_city;
	}

	public void setCustomer_city(String customer_city) {
		this.customer_city = customer_city;
	}

	public String getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}

	public String getVeh_Reg_no() {
		return Veh_Reg_no;
	}

	public void setVeh_Reg_no(String veh_Reg_no) {
		Veh_Reg_no = veh_Reg_no;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public String getModel() {
		return Model;
	}

	public void setModel(String model) {
		Model = model;
	}

	public String getFueltype() {
		return fueltype;
	}

	public void setFueltype(String fueltype) {
		this.fueltype = fueltype;
	}

	public String getVariant() {
		return Variant;
	}

	public void setVariant(String variant) {
		Variant = variant;
	}

	public String getLast_serviceDate() {
		return last_serviceDate;
	}

	public void setLast_serviceDate(String last_serviceDate) {
		this.last_serviceDate = last_serviceDate;
	}

	public String getLastServiceType() {
		return lastServiceType;
	}

	public void setLastServiceType(String lastServiceType) {
		this.lastServiceType = lastServiceType;
	}

	public String getNextServicedate() {
		return nextServicedate;
	}

	public void setNextServicedate(String nextServicedate) {
		this.nextServicedate = nextServicedate;
	}

	public String getNextServicetype() {
		return nextServicetype;
	}

	public void setNextServicetype(String nextServicetype) {
		this.nextServicetype = nextServicetype;
	}

	public String getForecastLogic() {
		return forecastLogic;
	}

	public void setForecastLogic(String forecastLogic) {
		this.forecastLogic = forecastLogic;
	}

	public String getPrevious_disposition() {
		return previous_disposition;
	}

	public void setPrevious_disposition(String previous_disposition) {
		this.previous_disposition = previous_disposition;
	}

	public String getPrimary_disposition() {
		return primary_disposition;
	}

	public void setPrimary_disposition(String primary_disposition) {
		this.primary_disposition = primary_disposition;
	}


	public String getSecondary_disposition() {
		return secondary_disposition;
	}

	public void setSecondary_disposition(String secondary_disposition) {
		this.secondary_disposition = secondary_disposition;
	}

	public String getSecondary_dispostion() {
		return secondary_dispostion;
	}

	public void setSecondary_dispostion(String secondary_dispostion) {
		this.secondary_dispostion = secondary_dispostion;
	}

	public String getTertiary_disposition() {
		return Tertiary_disposition;
	}

	public void setTertiary_disposition(String tertiary_disposition) {
		Tertiary_disposition = tertiary_disposition;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getPsf_status() {
		return psf_status;
	}

	public void setPsf_status(String psf_status) {
		this.psf_status = psf_status;
	}
	
	public String getMedia_avl() {
		return media_avl;
	}

	public void setMedia_avl(String media_avl) {
		this.media_avl = media_avl;
	}

	public long getCall_interaction_id() {
		return Call_interaction_id;
	}

	public void setCall_interaction_id(long call_interaction_id) {
		Call_interaction_id = call_interaction_id;
	}

	public long getSr_disposition_id() {
		return sr_disposition_id;
	}

	public void setSr_disposition_id(long sr_disposition_id) {
		this.sr_disposition_id = sr_disposition_id;
	}

	public String getCalling_data_type() {
		return calling_data_type;
	}

	public void setCalling_data_type(String calling_data_type) {
		this.calling_data_type = calling_data_type;
	}

	public String getTypeofpickup() {
		return typeofpickup;
	}

	public void setTypeofpickup(String typeofpickup) {
		this.typeofpickup = typeofpickup;
	}

	public String getFromtimeofpick() {
		return fromtimeofpick;
	}

	public void setFromtimeofpick(String fromtimeofpick) {
		this.fromtimeofpick = fromtimeofpick;
	}

	public String getUpto() {
		return upto;
	}

	public void setUpto(String upto) {
		this.upto = upto;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getService_advisor() {
		return service_advisor;
	}

	public void setService_advisor(String service_advisor) {
		this.service_advisor = service_advisor;
	}

	public String getUpselltype() {
		return upselltype;
	}

	public void setUpselltype(String upselltype) {
		this.upselltype = upselltype;
	}

	public long getAssignedInteraction_id() {
		return assignedInteraction_id;
	}

	public void setAssignedInteraction_id(long assignedInteraction_id) {
		this.assignedInteraction_id = assignedInteraction_id;
	}

	public Date getAssigned_date() {
		return assigned_date;
	}

	public void setAssigned_date(Date assigned_date) {
		this.assigned_date = assigned_date;
	}

	

	public String getIsCallinitaited() {
		return isCallinitaited;
	}

	public void setIsCallinitaited(String isCallinitaited) {
		this.isCallinitaited = isCallinitaited;
	}

	public String getCustomer_remarks() {
		return customer_remarks;
	}

	public void setCustomer_remarks(String customer_remarks) {
		this.customer_remarks = customer_remarks;
	}


	public String getInteractionDate() {
		return interactionDate;
	}


	public void setInteractionDate(String interactionDate) {
		this.interactionDate = interactionDate;
	}


	public String getInteractionTime() {
		return interactionTime;
	}


	public void setInteractionTime(String interactionTime) {
		this.interactionTime = interactionTime;
	}


	public String getSmsMessage() {
		return smsMessage;
	}


	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}


	public String getCustomerName() {
		return CustomerName;
	}


	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}


	public String getVehicleRegNo() {
		return VehicleRegNo;
	}


	public void setVehicleRegNo(String vehicleRegNo) {
		VehicleRegNo = vehicleRegNo;
	}


	public String getWyzuserName() {
		return WyzuserName;
	}


	public void setWyzuserName(String wyzuserName) {
		WyzuserName = wyzuserName;
	}


	public boolean isSmsStatus() {
		return smsStatus;
	}


	public void setSmsStatus(boolean smsStatus) {
		this.smsStatus = smsStatus;
	}


	public String getMobileNumber() {
		return mobileNumber;
	}


	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}	

}
