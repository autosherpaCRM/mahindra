package controllers.webmodels;



public class InsuranceHistoryData {

	public String insuranceCompanyName;
	public double idv;
	public String odPercentage;

	public double odAmount;

	public String ncBPercentage;
	public float ncBAmount;
	public String discountPercentage;

	public double ODpremium;

	public double liabilityPremium;

	public String add_ON_Premium;

	public double premiumAmountBeforeTax;

	public String serviceTax;

	public double premiumAmountAfterTax;

	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}

	public void setInsuranceCompanyName(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}

	public double getIdv() {
		return idv;
	}

	public void setIdv(double idv) {
		this.idv = idv;
	}

	public String getOdPercentage() {
		return odPercentage;
	}

	public void setOdPercentage(String odPercentage) {
		this.odPercentage = odPercentage;
	}

	public double getOdAmount() {
		return odAmount;
	}

	public void setOdAmount(double odAmount) {
		this.odAmount = odAmount;
	}

	public String getNcBPercentage() {
		return ncBPercentage;
	}

	public void setNcBPercentage(String ncBPercentage) {
		this.ncBPercentage = ncBPercentage;
	}

	public float getNcBAmount() {
		return ncBAmount;
	}

	public void setNcBAmount(float ncBAmount) {
		this.ncBAmount = ncBAmount;
	}

	public String getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(String discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public double getODpremium() {
		return ODpremium;
	}

	public void setODpremium(double oDpremium) {
		ODpremium = oDpremium;
	}

	public double getLiabilityPremium() {
		return liabilityPremium;
	}

	public void setLiabilityPremium(double liabilityPremium) {
		this.liabilityPremium = liabilityPremium;
	}

	public String getAdd_ON_Premium() {
		return add_ON_Premium;
	}

	public void setAdd_ON_Premium(String add_ON_Premium) {
		this.add_ON_Premium = add_ON_Premium;
	}

	public double getPremiumAmountBeforeTax() {
		return premiumAmountBeforeTax;
	}

	public void setPremiumAmountBeforeTax(double premiumAmountBeforeTax) {
		this.premiumAmountBeforeTax = premiumAmountBeforeTax;
	}

	public String getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}

	public double getPremiumAmountAfterTax() {
		return premiumAmountAfterTax;
	}

	public void setPremiumAmountAfterTax(double premiumAmountAfterTax) {
		this.premiumAmountAfterTax = premiumAmountAfterTax;
	}
	
	

}
