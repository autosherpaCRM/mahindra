package controllers.webmodels;

import java.util.Date;

public class CallLogAjaxLoadInsurance {

	private String campaignName;
	private String customerId;
	private String customer_name;
	private String phoneNumber;
	private String vehicle_RegNo;
	private String renewalType;	
	private String policy_due_month;
	private Date duedate;
	private String last_insuirancecompanyname;
	private long customer_id;
	private long vehicle_vehicle_id;
	private long wyzUser_id;	
	private long assigned_intercation_id;	
	private long insuranceassigned_id;
	private String campaign;
	private String customerName;
	private String Phone;
	private String RegNo;
	private String chassisNo;
	private String policyDueMonth;
	private Date policyDueDate;
	private String NextRenewalType;
	private String lastInsuranceCompany;
		
		
	
	public CallLogAjaxLoadInsurance(String campaign, String customerName , String Phone , String RegNo , String chassisNo,String policyDueMonth,
			Date policyDueDate , String NextRenewalType , String lastInsuranceCompany, long insuranceassigned_id, long customer_id,long vehicle_vehicle_id){
		this.campaign =campaign;
		this.customerName =customerName;
		this.Phone =Phone;
		this.RegNo =RegNo;
		this.chassisNo=chassisNo;
		this.policyDueMonth=policyDueMonth;
		this.policyDueDate=policyDueDate;
		this.NextRenewalType=NextRenewalType;
		this.lastInsuranceCompany=lastInsuranceCompany;
		this.insuranceassigned_id=insuranceassigned_id;
		this.customer_id=customer_id;
		this.vehicle_vehicle_id=vehicle_vehicle_id;
		
	}
	public CallLogAjaxLoadInsurance(String campaignName, String customerId, String customer_name, String phoneNumber,
			String vehicle_RegNo, String renewalType, String policy_due_month, Date duedate, String last_insuirancecompanyname,
			long customer_id, long vehicle_vehicle_id,long wyzUser_id,  long assigned_intercation_id) {
		
		this.campaignName = campaignName;
		this.customerId = customerId;
		this.customer_name = customer_name;
		this.phoneNumber = phoneNumber;
		this.vehicle_RegNo = vehicle_RegNo;
		this.renewalType = renewalType;
		this.policy_due_month = policy_due_month;
		this.duedate = duedate;
		this.last_insuirancecompanyname = last_insuirancecompanyname;
		this.customer_id = customer_id;
		this.vehicle_vehicle_id = vehicle_vehicle_id;
		this.wyzUser_id = wyzUser_id;		
		this.assigned_intercation_id = assigned_intercation_id;
	}
		
	public CallLogAjaxLoadInsurance() {
		this.campaign ="";
		this.customerName ="";
		this.Phone ="";
		this.RegNo ="";
		this.chassisNo="";
		this.policyDueMonth="";
		this.policyDueDate=null;
		this.NextRenewalType="";
		this.lastInsuranceCompany="";
		this.campaignName = "";
		this.customerId = "";
		this.customer_name = "";
		this.phoneNumber = "";
		this.vehicle_RegNo = "";
		this.renewalType = "";
		this.policy_due_month = "";
		this.duedate = null;
		this.last_insuirancecompanyname = "";
		this.customer_id = (long) 0;
		this.vehicle_vehicle_id = (long) 0;
		this.wyzUser_id = (long) 0;		
		this.assigned_intercation_id = (long) 0;
		this.insuranceassigned_id=(long) 0;
		this.customer_id=(long) 0;
		
	}
	
	public long getInsuranceassigned_id() {
		return insuranceassigned_id;
	}

	public void setInsuranceassigned_id(long insuranceassigned_id) {
		this.insuranceassigned_id = insuranceassigned_id;
	}
	
	public String getCampaign() {
		return campaign;
	}

	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getRegNo() {
		return RegNo;
	}

	public void setRegNo(String regNo) {
		RegNo = regNo;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public String getPolicyDueMonth() {
		return policyDueMonth;
	}

	public void setPolicyDueMonth(String policyDueMonth) {
		this.policyDueMonth = policyDueMonth;
	}

	public Date getPolicyDueDate() {
		return policyDueDate;
	}

	public void setPolicyDueDate(Date policyDueDate) {
		this.policyDueDate = policyDueDate;
	}

	public String getNextRenewalType() {
		return NextRenewalType;
	}

	public void setNextRenewalType(String nextRenewalType) {
		NextRenewalType = nextRenewalType;
	}

	public String getLastInsuranceCompany() {
		return lastInsuranceCompany;
	}

	public void setLastInsuranceCompany(String lastInsuranceCompany) {
		this.lastInsuranceCompany = lastInsuranceCompany;
	}

	
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getVehicle_RegNo() {
		return vehicle_RegNo;
	}
	public void setVehicle_RegNo(String vehicle_RegNo) {
		this.vehicle_RegNo = vehicle_RegNo;
	}
	public String getRenewalType() {
		return renewalType;
	}
	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}
	
	public String getPolicy_due_month() {
		return policy_due_month;
	}

	public void setPolicy_due_month(String policy_due_month) {
		this.policy_due_month = policy_due_month;
	}

	public Date getDuedate() {
		return duedate;
	}
	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}
	public String getLast_insuirancecompanyname() {
		return last_insuirancecompanyname;
	}
	public void setLast_insuirancecompanyname(String last_insuirancecompanyname) {
		this.last_insuirancecompanyname = last_insuirancecompanyname;
	}
	public long getVehicle_vehicle_id() {
		return vehicle_vehicle_id;
	}
	public void setVehicle_vehicle_id(long vehicle_vehicle_id) {
		this.vehicle_vehicle_id = vehicle_vehicle_id;
	}
	public long getWyzUser_id() {
		return wyzUser_id;
	}
	public void setWyzUser_id(long wyzUser_id) {
		this.wyzUser_id = wyzUser_id;
	}
	public long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}
	public long getAssigned_intercation_id() {
		return assigned_intercation_id;
	}
	public void setAssigned_intercation_id(long assigned_intercation_id) {
		this.assigned_intercation_id = assigned_intercation_id;
	}
		
}
