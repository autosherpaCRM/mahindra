package controllers.webmodels;

public class SMSInteractionhistory {
	
	public String interactionDate;
	public String interactionTime;
	public String creId;
	public String scenario;
	public String message;
	public String status;
	
	
	
	public String getInteractionDate() {
		return interactionDate;
	}
	public void setInteractionDate(String interactionDate) {
		this.interactionDate = interactionDate;
	}
	public String getInteractionTime() {
		return interactionTime;
	}
	public void setInteractionTime(String interactionTime) {
		this.interactionTime = interactionTime;
	}
	public String getCreId() {
		return creId;
	}
	public void setCreId(String creId) {
		this.creId = creId;
	}
	public String getScenario() {
		return scenario;
	}
	public void setScenario(String scenario) {
		this.scenario = scenario;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	

}
