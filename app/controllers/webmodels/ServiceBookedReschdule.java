package controllers.webmodels;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class ServiceBookedReschdule {

	public String serviceBookWorkshop;
	public long workshopId;
	public String selectedCityName;
	
	public String serviceBookedDate;
	public String serviceBookTime;
	
	public String serviceAdvisor;
	public long serviceAdvisorId;
	
	public String typeOfPickUp;
	public String serviceBookedAddress;
	
	public long vehicleId;
	public String vehicleRegNo;
	
	public long serviceTypeId;
	public String serviceTypeName;
	
	public String remarksOfService;
	
	
	public long callIntId;
	
	public long serviceBookIntId;
	
	public String driverName;
	@Temporal(TemporalType.TIME)
	public Date fromTime;
	@Temporal(TemporalType.TIME)
	public Date toTime;
	

	public String getServiceBookWorkshop() {
		return serviceBookWorkshop;
	}

	public void setServiceBookWorkshop(String serviceBookWorkshop) {
		this.serviceBookWorkshop = serviceBookWorkshop;
	}

	public long getWorkshopId() {
		return workshopId;
	}

	public void setWorkshopId(long workshopId) {
		this.workshopId = workshopId;
	}

	public String getServiceBookedDate() {
		return serviceBookedDate;
	}

	public void setServiceBookedDate(String serviceBookedDate) {
		this.serviceBookedDate = serviceBookedDate;
	}

	public String getServiceBookTime() {
		return serviceBookTime;
	}

	public void setServiceBookTime(String serviceBookTime) {
		this.serviceBookTime = serviceBookTime;
	}

	public String getServiceAdvisor() {
		return serviceAdvisor;
	}

	public void setServiceAdvisor(String serviceAdvisor) {
		this.serviceAdvisor = serviceAdvisor;
	}

	public long getServiceAdvisorId() {
		return serviceAdvisorId;
	}

	public void setServiceAdvisorId(long serviceAdvisorId) {
		this.serviceAdvisorId = serviceAdvisorId;
	}

	public String getTypeOfPickUp() {
		return typeOfPickUp;
	}

	public void setTypeOfPickUp(String typeOfPickUp) {
		this.typeOfPickUp = typeOfPickUp;
	}

	public String getServiceBookedAddress() {
		return serviceBookedAddress;
	}

	public void setServiceBookedAddress(String serviceBookedAddress) {
		this.serviceBookedAddress = serviceBookedAddress;
	}

	public long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public long getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(long serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public String getServiceTypeName() {
		return serviceTypeName;
	}

	public void setServiceTypeName(String serviceTypeName) {
		this.serviceTypeName = serviceTypeName;
	}

	public String getSelectedCityName() {
		return selectedCityName;
	}

	public void setSelectedCityName(String selectedCityName) {
		this.selectedCityName = selectedCityName;
	}

	public String getRemarksOfService() {
		return remarksOfService;
	}

	public void setRemarksOfService(String remarksOfService) {
		this.remarksOfService = remarksOfService;
	}

	public long getCallIntId() {
		return callIntId;
	}

	public void setCallIntId(long callIntId) {
		this.callIntId = callIntId;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public Date getFromTime() {
		return fromTime;
	}

	public void setFromTime(Date fromTime) {
		this.fromTime = fromTime;
	}

	public Date getToTime() {
		return toTime;
	}

	public void setToTime(Date toTime) {
		this.toTime = toTime;
	}

	public long getServiceBookIntId() {
		return serviceBookIntId;
	}

	public void setServiceBookIntId(long serviceBookIntId) {
		this.serviceBookIntId = serviceBookIntId;
	}
	
	
	

}
