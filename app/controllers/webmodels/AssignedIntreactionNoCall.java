package controllers.webmodels;


import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class AssignedIntreactionNoCall {
	
	String wyzUserName;
	String locations;
	String campaignName;
	String customerName;
	long vehicleId;
	String chassisNo;
	String model;
	String vehicleRegNo;
	
	@Temporal(TemporalType.DATE)
	Date uploadDate;
	
	long noCallAssignId;
	
	public long getNoCallAssignId() {
		return noCallAssignId;
	}
	public void setNoCallAssignId(long noCallAssignId) {
		this.noCallAssignId = noCallAssignId;
	}
	public String getWyzUserName() {
		return wyzUserName;
	}
	public void setWyzUserName(String wyzUserName) {
		this.wyzUserName = wyzUserName;
	}
	public String getLocations() {
		return locations;
	}
	public void setLocations(String locations) {
		this.locations = locations;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public long getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getChassisNo() {
		return chassisNo;
	}
	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getVehicleRegNo() {
		return vehicleRegNo;
	}
	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}
	public Date getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}
	
	
	
	
	public AssignedIntreactionNoCall(String wyzUserName, String locations, String campaignName, String customerName,
			long vehicleId, String chassisNo, String model, String vehicleRegNo, Date uploadDate, long noCallAssignId) {
		
		this.wyzUserName = wyzUserName;
		this.locations = locations;
		this.campaignName = campaignName;
		this.customerName = customerName;
		this.vehicleId = vehicleId;
		this.chassisNo = chassisNo;
		this.model = model;
		this.vehicleRegNo = vehicleRegNo;
		this.uploadDate = uploadDate;
		this.noCallAssignId = noCallAssignId;
	}
	public AssignedIntreactionNoCall() {
		
		this.wyzUserName = null;
		this.locations = null;
		this.campaignName = null;
		this.customerName = null;
		this.vehicleId = (long)0;
		this.chassisNo = null;
		this.model = null;
		this.vehicleRegNo = null;
		this.uploadDate = null;
		this.noCallAssignId = (long)0;
	}
	
}
