package controllers.webmodels;

import java.util.List;

public class PieChartCallTypeData {
	
	public String missedCallCount;
	
	public String outgoingCallCount;
	
	public String incomingCallCount;
	
	public List<String> missedList;
	public List<String> outGoingList;
	public List<String> incomingList;

	public String getMissedCallCount() {
		return missedCallCount;
	}

	public void setMissedCallCount(String missedCallCount) {
		this.missedCallCount = missedCallCount;
	}

	public String getOutgoingCallCount() {
		return outgoingCallCount;
	}

	public void setOutgoingCallCount(String outgoingCallCount) {
		this.outgoingCallCount = outgoingCallCount;
	}

	public String getIncomingCallCount() {
		return incomingCallCount;
	}

	public void setIncomingCallCount(String incomingCallCount) {
		this.incomingCallCount = incomingCallCount;
	}

	public List<String> getMissedList() {
		return missedList;
	}

	public void setMissedList(List<String> missedList) {
		this.missedList = missedList;
	}

	public List<String> getOutGoingList() {
		return outGoingList;
	}

	public void setOutGoingList(List<String> outGoingList) {
		this.outGoingList = outGoingList;
	}

	public List<String> getIncomingList() {
		return incomingList;
	}

	public void setIncomingList(List<String> incomingList) {
		this.incomingList = incomingList;
	}
	
	

}
