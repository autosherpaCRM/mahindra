package controllers.webmodels;

import java.sql.Time;
import java.util.Date;


public class InsuranceHistoryReport {
	
	public String creName;
	public Date callDate;
	public String callTime;
	public String isCallinitaited;
	public String callType;
	public String callDuration;
	public String ringTime;
	public String campaignTYpe;
	public String chassisNo;
	public String model;
	public String variant;
	public String saleDate;
	public String vehicleRegno;
	public String customerName;
	public String preffered_address;
	public String prefferedPhoneNumber;
	public String PrimaryDisposition;
	public String SecondaryDisposition;
	public String coverNoteNo;
	public String lastRenewedLocation;
	public String lastRenewalDate;
	public String premimum;
	public String renewalDoneBy;
	public String Tertiary_disposition;
	public Date followUpDate;
	public String followUpTime;
	public String addOnsPrefered_OtherOptionsData;
	public String addOnsPrefered_PopularOptionsData;
	public String remarksOfFb;
	public String departmentForFB;
	public String addressOfVisit;
	public String appointeeName;
	public Date   appointmentDate;
	public String appointmentFromTime;
	public String dsa;
	public String insuranceAgentData;
	public String insuranceCompany;
	public String nomineeName;
	public String nomineeRelationWithOwner;
	public String premiumwithTax;
	public String renewalMode;
	public String renewalType;
	public String typeOfPickup;
	public String PremiumYes;
	public String comments;
	public String reason;
	public String cremanager;
	public Date updatedDate;
	public String reasonForSNR;
	
	
	public InsuranceHistoryReport(String creName, Date callDate, String callTime, String isCallinitaited, String callType,String callDuration, 
			String ringTime,String campaignTYpe, String chassisNo,String model, String variant, String saleDate, String vehicleRegno, String customerName,
			String preffered_address, String prefferedPhoneNumber, String PrimaryDisposition, String SecondaryDisposition,
			String coverNoteNo, String lastRenewedLocation, String lastRenewalDate, String premimum ,String renewalDoneBy,
			String Tertiary_disposition, Date followUpDate, String followUpTime, String addOnsPrefered_OtherOptionsData,
			String addOnsPrefered_PopularOptionsData,String remarksOfFb, String departmentForFB, String addressOfVisit, 
			String appointeeName, Date appointmentDate, String appointmentFromTime, String dsa, String insuranceAgentData,
			String insuranceCompany, String nomineeName, String nomineeRelationWithOwner, String premiumwithTax, String renewalMode,
			String renewalType, String typeOfPickup, String PremiumYes, String comments, String reason, String cremanager,
			Date updatedDate, String reasonForSNR) {
		this.creName= creName;
		this.callDate=callDate;
		this.callTime=callTime;
		this.isCallinitaited=isCallinitaited;
		this.callType=callType;
		this.callDuration=callDuration;
		this.ringTime=ringTime;
		this.campaignTYpe=campaignTYpe;
		this.chassisNo=chassisNo;
		this.model=model;
		this.variant=variant;
		this.saleDate=saleDate;
		this.vehicleRegno=vehicleRegno;
		this.customerName=customerName;
		this.preffered_address=preffered_address;
		this.prefferedPhoneNumber=prefferedPhoneNumber;
		this.PrimaryDisposition=PrimaryDisposition;
		this.SecondaryDisposition=SecondaryDisposition;
		this.coverNoteNo=coverNoteNo;
		this.lastRenewedLocation=lastRenewedLocation;
		this.lastRenewalDate=lastRenewalDate;
		this.premimum=premimum;
		this.renewalDoneBy=renewalDoneBy;
		this.Tertiary_disposition=Tertiary_disposition;
		this.followUpDate=followUpDate;
		this.followUpTime=followUpTime;
		this.addOnsPrefered_OtherOptionsData=addOnsPrefered_OtherOptionsData;
		this.addOnsPrefered_PopularOptionsData=addOnsPrefered_PopularOptionsData;
		this.remarksOfFb=remarksOfFb;
		this.departmentForFB=departmentForFB;
		this.addressOfVisit=addressOfVisit;
		this.appointeeName=appointeeName;
		this.appointmentDate=appointmentDate;
		this.appointmentFromTime=appointmentFromTime;
		this.dsa=dsa;
		this.insuranceAgentData=insuranceAgentData;
		this.insuranceCompany=insuranceCompany;
		this.nomineeName=nomineeName;
		this.nomineeRelationWithOwner=nomineeRelationWithOwner;
		this.premiumwithTax=premiumwithTax;
		this.renewalMode=renewalMode;
		this.renewalType=renewalType;
		this.typeOfPickup=typeOfPickup;
		this.PremiumYes=PremiumYes;
		this.comments=comments;
		this.reason=reason;
		this.cremanager=cremanager;
		this.updatedDate=updatedDate;
		this.reasonForSNR=reasonForSNR;
	}	
	public InsuranceHistoryReport() {
		this.creName="";
		this.callDate=null;
		this.callTime="";
		this.isCallinitaited="";
		this.callType="";
		this.callDuration="";
		this.ringTime="";
		this.campaignTYpe="";
		this.chassisNo="";
		this.model="";
		this.variant="";
		this.saleDate="";
		this.vehicleRegno="";
		this.customerName="";
		this.preffered_address="";
		this.prefferedPhoneNumber="";
		this.PrimaryDisposition="";
		this.SecondaryDisposition="";
		this.coverNoteNo="";
		this.lastRenewedLocation="";
		this.lastRenewalDate="";
		this.premimum="";
		this.renewalDoneBy="";
		this.Tertiary_disposition="";
		this.followUpDate=null;
		this.followUpTime="";
		this.addOnsPrefered_OtherOptionsData="";
		this.addOnsPrefered_PopularOptionsData="";
		this.remarksOfFb="";
		this.departmentForFB="";
		this.addressOfVisit="";
		this.appointeeName="";
		this.appointmentDate=null;
		this.appointmentFromTime="";
		this.dsa="";
		this.insuranceAgentData="";
		this.insuranceCompany="";
		this.nomineeName="";
		this.nomineeRelationWithOwner="";
		this.premiumwithTax="";
		this.renewalMode="";
		this.renewalType="";
		this.typeOfPickup="";
		this.PremiumYes="";
		this.comments="";
		this.reason="";
		this.cremanager="";
		this.updatedDate=null;
		this.reasonForSNR="";	
	}
	
	public String getCreName() {
		return creName;
	}
	public void setCreName(String creName) {
		this.creName = creName;
	}
	public Date getCallDate() {
		return callDate;
	}
	public void setCallDate(Date callDate) {
		this.callDate = callDate;
	}
	
	
	public String getCallTime() {
		return callTime;
	}
	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}
	public String getIsCallinitaited() {
		return isCallinitaited;
	}
	public void setIsCallinitaited(String isCallinitaited) {
		this.isCallinitaited = isCallinitaited;
	}
	public String getCallType() {
		return callType;
	}
	public void setCallType(String callType) {
		this.callType = callType;
	}
	public String getCallDuration() {
		return callDuration;
	}
	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}
	public String getRingTime() {
		return ringTime;
	}
	public void setRingTime(String ringTime) {
		this.ringTime = ringTime;
	}
	public String getCampaignTYpe() {
		return campaignTYpe;
	}
	public void setCampaignTYpe(String campaignTYpe) {
		this.campaignTYpe = campaignTYpe;
	}
	public String getChassisNo() {
		return chassisNo;
	}
	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getVariant() {
		return variant;
	}
	public void setVariant(String variant) {
		this.variant = variant;
	}
	public String getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(String saleDate) {
		this.saleDate = saleDate;
	}
	
	public String getVehicleRegno() {
		return vehicleRegno;
	}
	public void setVehicleRegno(String vehicleRegno) {
		this.vehicleRegno = vehicleRegno;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPreffered_address() {
		return preffered_address;
	}
	public void setPreffered_address(String preffered_address) {
		this.preffered_address = preffered_address;
	}
	public String getPrefferedPhoneNumber() {
		return prefferedPhoneNumber;
	}
	public void setPrefferedPhoneNumber(String prefferedPhoneNumber) {
		this.prefferedPhoneNumber = prefferedPhoneNumber;
	}

	public String getPrimaryDisposition() {
		return PrimaryDisposition;
	}
	public void setPrimaryDisposition(String primaryDisposition) {
		PrimaryDisposition = primaryDisposition;
	}
	public String getSecondaryDisposition() {
		return SecondaryDisposition;
	}
	public void setSecondaryDisposition(String secondaryDisposition) {
		SecondaryDisposition = secondaryDisposition;
	}
	public String getCoverNoteNo() {
		return coverNoteNo;
	}
	public void setCoverNoteNo(String coverNoteNo) {
		this.coverNoteNo = coverNoteNo;
	}
	public String getLastRenewedLocation() {
		return lastRenewedLocation;
	}
	public void setLastRenewedLocation(String lastRenewedLocation) {
		this.lastRenewedLocation = lastRenewedLocation;
	}
	public String getLastRenewalDate() {
		return lastRenewalDate;
	}
	public void setLastRenewalDate(String lastRenewalDate) {
		this.lastRenewalDate = lastRenewalDate;
	}
	public String getPremimum() {
		return premimum;
	}
	public void setPremimum(String premimum) {
		this.premimum = premimum;
	}
	public String getRenewalDoneBy() {
		return renewalDoneBy;
	}
	public void setRenewalDoneBy(String renewalDoneBy) {
		this.renewalDoneBy = renewalDoneBy;
	}
	public String getTertiary_disposition() {
		return Tertiary_disposition;
	}
	public void setTertiary_disposition(String tertiary_disposition) {
		Tertiary_disposition = tertiary_disposition;
	}
	public Date getFollowUpDate() {
		return followUpDate;
	}
	public void setFollowUpDate(Date followUpDate) {
		this.followUpDate = followUpDate;
	}
	public String getFollowUpTime() {
		return followUpTime;
	}
	public void setFollowUpTime(String followUpTime) {
		this.followUpTime = followUpTime;
	}
	public String getAddOnsPrefered_OtherOptionsData() {
		return addOnsPrefered_OtherOptionsData;
	}
	public void setAddOnsPrefered_OtherOptionsData(String addOnsPrefered_OtherOptionsData) {
		this.addOnsPrefered_OtherOptionsData = addOnsPrefered_OtherOptionsData;
	}
	public String getAddOnsPrefered_PopularOptionsData() {
		return addOnsPrefered_PopularOptionsData;
	}
	public void setAddOnsPrefered_PopularOptionsData(String addOnsPrefered_PopularOptionsData) {
		this.addOnsPrefered_PopularOptionsData = addOnsPrefered_PopularOptionsData;
	}
	
	public String getRemarksOfFb() {
		return remarksOfFb;
	}
	public void setRemarksOfFb(String remarksOfFb) {
		this.remarksOfFb = remarksOfFb;
	}
	public String getDepartmentForFB() {
		return departmentForFB;
	}
	public void setDepartmentForFB(String departmentForFB) {
		this.departmentForFB = departmentForFB;
	}
	
	public String getAddressOfVisit() {
		return addressOfVisit;
	}
	public void setAddressOfVisit(String addressOfVisit) {
		this.addressOfVisit = addressOfVisit;
	}
	public String getAppointeeName() {
		return appointeeName;
	}
	public void setAppointeeName(String appointeeName) {
		this.appointeeName = appointeeName;
	}
	public Date getAppointmentDate() {
		return appointmentDate;
	}
	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	public String getAppointmentFromTime() {
		return appointmentFromTime;
	}
	public void setAppointmentFromTime(String appointmentFromTime) {
		this.appointmentFromTime = appointmentFromTime;
	}
	public String getDsa() {
		return dsa;
	}
	public void setDsa(String dsa) {
		this.dsa = dsa;
	}
	public String getInsuranceAgentData() {
		return insuranceAgentData;
	}
	public void setInsuranceAgentData(String insuranceAgentData) {
		this.insuranceAgentData = insuranceAgentData;
	}
	public String getInsuranceCompany() {
		return insuranceCompany;
	}
	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}
	public String getNomineeName() {
		return nomineeName;
	}
	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}
	public String getNomineeRelationWithOwner() {
		return nomineeRelationWithOwner;
	}
	public void setNomineeRelationWithOwner(String nomineeRelationWithOwner) {
		this.nomineeRelationWithOwner = nomineeRelationWithOwner;
	}
	public String getPremiumwithTax() {
		return premiumwithTax;
	}
	public void setPremiumwithTax(String premiumwithTax) {
		this.premiumwithTax = premiumwithTax;
	}
	public String getRenewalMode() {
		return renewalMode;
	}
	public void setRenewalMode(String renewalMode) {
		this.renewalMode = renewalMode;
	}
	public String getRenewalType() {
		return renewalType;
	}
	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}
	public String getTypeOfPickup() {
		return typeOfPickup;
	}
	public void setTypeOfPickup(String typeOfPickup) {
		this.typeOfPickup = typeOfPickup;
	}
	public String getPremiumYes() {
		return PremiumYes;
	}
	public void setPremiumYes(String premiumYes) {
		PremiumYes = premiumYes;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getCremanager() {
		return cremanager;
	}
	public void setCremanager(String cremanager) {
		this.cremanager = cremanager;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getReasonForSNR() {
		return reasonForSNR;
	}
	public void setReasonForSNR(String reasonForSNR) {
		this.reasonForSNR = reasonForSNR;
	}

}
