package controllers.webmodels;

import java.util.Date;

public class CallInteractionHistory_dwnld {

	private String Location;
	private String Cre_Name;
	private String callTime;
	private String callDate;
	private String preffered_Contact_number;
	private String callType;
	private String Customer_name;
	private String DOb;
	private String office_address;
	private String residential_address;
	private String permenant_address;
	private String Email_id;
	private String customer_category;
	private String customer_city;
	private String callDuration;
	private String Veh_Reg_no;
	private String chassisNo;
	private String Model;
	private String fueltype;
	private String Variant;
	private String last_serviceDate;
	private String lastServiceType;
	private String nextServicedate;
	private String nextServicetype;
	private String forecastLogic;
	private String previous_disposition;
	private String primary_disposition;
	
	private String secondary_dispostion;
	private String Tertiary_disposition;
	private String serviceType;
	private String psf_status;
	public CallInteractionHistory_dwnld() {
		
	}
	public CallInteractionHistory_dwnld(String location, String cre_Name, String callTime, String callDate,
			String preffered_Contact_number, String callType, String customer_name, String dOb, String office_address,
			String residential_address, String permenant_address, String email_id, String customer_category,
			String customer_city, String callDuration, String veh_Reg_no, String chassisNo, String model,
			String fueltype, String variant, String last_serviceDate, String lastServiceType, String nextServicedate,
			String nextServicetype, String forecastLogic, String previous_disposition, String primary_disposition,
			String secondary_dispostion, String tertiary_disposition, String serviceType, String psf_status) {
		Location = location;
		Cre_Name = cre_Name;
		this.callTime = callTime;
		this.callDate = callDate;
		this.preffered_Contact_number = preffered_Contact_number;
		this.callType = callType;
		Customer_name = customer_name;
		DOb = dOb;
		this.office_address = office_address;
		this.residential_address = residential_address;
		this.permenant_address = permenant_address;
		Email_id = email_id;
		this.customer_category = customer_category;
		this.customer_city = customer_city;
		this.callDuration = callDuration;
		Veh_Reg_no = veh_Reg_no;
		this.chassisNo = chassisNo;
		Model = model;
		this.fueltype = fueltype;
		Variant = variant;
		this.last_serviceDate = last_serviceDate;
		this.lastServiceType = lastServiceType;
		this.nextServicedate = nextServicedate;
		this.nextServicetype = nextServicetype;
		this.forecastLogic = forecastLogic;
		this.previous_disposition = previous_disposition;
		this.primary_disposition = primary_disposition;
		this.secondary_dispostion = secondary_dispostion;
		Tertiary_disposition = tertiary_disposition;
		this.serviceType = serviceType;
		this.psf_status = psf_status;
	}
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	public String getCre_Name() {
		return Cre_Name;
	}
	public void setCre_Name(String cre_Name) {
		Cre_Name = cre_Name;
	}
	public String getCallTime() {
		return callTime;
	}
	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}
	public String getCallDate() {
		return callDate;
	}
	public void setCallDate(String callDate) {
		this.callDate = callDate;
	}
	public String getPreffered_Contact_number() {
		return preffered_Contact_number;
	}
	public void setPreffered_Contact_number(String preffered_Contact_number) {
		this.preffered_Contact_number = preffered_Contact_number;
	}
	public String getCallType() {
		return callType;
	}
	public void setCallType(String callType) {
		this.callType = callType;
	}
	public String getCustomer_name() {
		return Customer_name;
	}
	public void setCustomer_name(String customer_name) {
		Customer_name = customer_name;
	}
	public String getDOb() {
		return DOb;
	}
	public void setDOb(String dOb) {
		DOb = dOb;
	}
	public String getOffice_address() {
		return office_address;
	}
	public void setOffice_address(String office_address) {
		this.office_address = office_address;
	}
	public String getResidential_address() {
		return residential_address;
	}
	public void setResidential_address(String residential_address) {
		this.residential_address = residential_address;
	}
	public String getPermenant_address() {
		return permenant_address;
	}
	public void setPermenant_address(String permenant_address) {
		this.permenant_address = permenant_address;
	}
	public String getEmail_id() {
		return Email_id;
	}
	public void setEmail_id(String email_id) {
		Email_id = email_id;
	}
	public String getCustomer_category() {
		return customer_category;
	}
	public void setCustomer_category(String customer_category) {
		this.customer_category = customer_category;
	}
	public String getCustomer_city() {
		return customer_city;
	}
	public void setCustomer_city(String customer_city) {
		this.customer_city = customer_city;
	}
	public String getCallDuration() {
		return callDuration;
	}
	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}
	public String getVeh_Reg_no() {
		return Veh_Reg_no;
	}
	public void setVeh_Reg_no(String veh_Reg_no) {
		Veh_Reg_no = veh_Reg_no;
	}
	public String getChassisNo() {
		return chassisNo;
	}
	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}
	public String getModel() {
		return Model;
	}
	public void setModel(String model) {
		Model = model;
	}
	public String getFueltype() {
		return fueltype;
	}
	public void setFueltype(String fueltype) {
		this.fueltype = fueltype;
	}
	public String getVariant() {
		return Variant;
	}
	public void setVariant(String variant) {
		Variant = variant;
	}
	public String getLast_serviceDate() {
		return last_serviceDate;
	}
	public void setLast_serviceDate(String last_serviceDate) {
		this.last_serviceDate = last_serviceDate;
	}
	public String getLastServiceType() {
		return lastServiceType;
	}
	public void setLastServiceType(String lastServiceType) {
		this.lastServiceType = lastServiceType;
	}
	public String getNextServicedate() {
		return nextServicedate;
	}
	public void setNextServicedate(String nextServicedate) {
		this.nextServicedate = nextServicedate;
	}
	public String getNextServicetype() {
		return nextServicetype;
	}
	public void setNextServicetype(String nextServicetype) {
		this.nextServicetype = nextServicetype;
	}
	public String getForecastLogic() {
		return forecastLogic;
	}
	public void setForecastLogic(String forecastLogic) {
		this.forecastLogic = forecastLogic;
	}
	public String getPrevious_disposition() {
		return previous_disposition;
	}
	public void setPrevious_disposition(String previous_disposition) {
		this.previous_disposition = previous_disposition;
	}
	public String getPrimary_disposition() {
		return primary_disposition;
	}
	public void setPrimary_disposition(String primary_disposition) {
		this.primary_disposition = primary_disposition;
	}
	public String getSecondary_dispostion() {
		return secondary_dispostion;
	}
	public void setSecondary_dispostion(String secondary_dispostion) {
		this.secondary_dispostion = secondary_dispostion;
	}
	public String getTertiary_disposition() {
		return Tertiary_disposition;
	}
	public void setTertiary_disposition(String tertiary_disposition) {
		Tertiary_disposition = tertiary_disposition;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getPsf_status() {
		return psf_status;
	}
	public void setPsf_status(String psf_status) {
		this.psf_status = psf_status;
	}
	
	
	
}
