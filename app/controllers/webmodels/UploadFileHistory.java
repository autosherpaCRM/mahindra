package controllers.webmodels;

public class UploadFileHistory {
	
	public String report_Name;
	public String report_Date;
	public String city;
	public String workshop;
	public String totalRecords;
	public String approved_records;
	public String rejectedRecord;
	public String wyzUser_id;
	public String uploaded_by_user;
	public String uploadDate;
	public String upload_time;
	public String uploaded_File_Name;
	public String upload_id;
	
	
	public UploadFileHistory(String report_Name, String report_Date,String city, String workshop, String totalRecords,
			String approved_records, String rejectedRecord, String wyzUser_id, String uploaded_by_user,
			String uploadDate, String upload_time, String uploaded_File_Name, String upload_id) {
		
		this.report_Name = report_Name;
		this.report_Date=report_Date;
		this.city = city;
		this.workshop = workshop;
		this.totalRecords = totalRecords;
		this.approved_records = approved_records;
		this.rejectedRecord = rejectedRecord;
		this.wyzUser_id = wyzUser_id;
		this.uploaded_by_user = uploaded_by_user;
		this.uploadDate = uploadDate;
		this.upload_time = upload_time;
		this.uploaded_File_Name = uploaded_File_Name;
		this.upload_id = upload_id;
	}
	
	public UploadFileHistory() {
		
		this.report_Name = "";
		this.report_Date="";
		this.city = "";
		this.workshop = "";
		this.totalRecords = "";
		this.approved_records = "";
		this.rejectedRecord = "";
		this.wyzUser_id = "";
		this.uploaded_by_user = "";
		this.uploadDate = "";
		this.upload_time = "";
		this.uploaded_File_Name = "";
		this.upload_id = "";
	}

	public String getReport_Name() {
		return report_Name;
	}

	public void setReport_Name(String report_Name) {
		this.report_Name = report_Name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getWorkshop() {
		return workshop;
	}

	public void setWorkshop(String workshop) {
		this.workshop = workshop;
	}

	public String getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}

	public String getApproved_records() {
		return approved_records;
	}

	public void setApproved_records(String approved_records) {
		this.approved_records = approved_records;
	}

	public String getRejectedRecord() {
		return rejectedRecord;
	}

	public void setRejectedRecord(String rejectedRecord) {
		this.rejectedRecord = rejectedRecord;
	}

	public String getWyzUser_id() {
		return wyzUser_id;
	}

	public void setWyzUser_id(String wyzUser_id) {
		this.wyzUser_id = wyzUser_id;
	}

	public String getUploaded_by_user() {
		return uploaded_by_user;
	}

	public void setUploaded_by_user(String uploaded_by_user) {
		this.uploaded_by_user = uploaded_by_user;
	}

	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getUpload_time() {
		return upload_time;
	}

	public void setUpload_time(String upload_time) {
		this.upload_time = upload_time;
	}

	public String getUploaded_File_Name() {
		return uploaded_File_Name;
	}

	public void setUploaded_File_Name(String uploaded_File_Name) {
		this.uploaded_File_Name = uploaded_File_Name;
	}

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	public String getReport_Date() {
		return report_Date;
	}

	public void setReport_Date(String report_Date) {
		this.report_Date = report_Date;
	}
	
	

}
