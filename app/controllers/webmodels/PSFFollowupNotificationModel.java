package controllers.webmodels;

public class PSFFollowupNotificationModel {

	public long customer_id;
	public String customer_name;
	public long vehicle_id;
	public long callinteraction_id;
	public String psfFollowUpTime;
	public long campaign_id;

	public PSFFollowupNotificationModel(long customer_id, String customer_name, long vehicle_id,
			long callinteraction_id, String psfFollowUpTime, long campaign_id) {
		super();
		this.customer_id = customer_id;
		this.customer_name = customer_name;
		this.vehicle_id = vehicle_id;
		this.callinteraction_id = callinteraction_id;
		this.psfFollowUpTime = psfFollowUpTime;
		this.campaign_id = campaign_id;
	}

	public PSFFollowupNotificationModel() {
		super();
		this.customer_id = (long) 0;
		this.customer_name = "";
		this.vehicle_id = (long) 0;
		this.callinteraction_id = (long) 0;
		this.psfFollowUpTime = "";
		this.campaign_id = (long) 0;
	}

	public long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public long getVehicle_id() {
		return vehicle_id;
	}

	public void setVehicle_id(long vehicle_id) {
		this.vehicle_id = vehicle_id;
	}

	public long getCallinteraction_id() {
		return callinteraction_id;
	}

	public void setCallinteraction_id(long callinteraction_id) {
		this.callinteraction_id = callinteraction_id;
	}

	public String getPsfFollowUpTime() {
		return psfFollowUpTime;
	}

	public void setPsfFollowUpTime(String psfFollowUpTime) {
		this.psfFollowUpTime = psfFollowUpTime;
	}

	public long getCampaign_id() {
		return campaign_id;
	}

	public void setCampaign_id(long campaign_id) {
		this.campaign_id = campaign_id;
	}

}
