package controllers.webmodels;


import java.sql.SQLClientInfoException;
import java.util.Date;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;

public class ServiceBookedResultList {
	
	public String status;
	public Date booking_reg_date;
	public Date booked_date;
	public String workshop;
	public String serviceBookedType;
	public String customer_name;
	public String mobile_number;
	public String address;
	public String veh_model;
	public String mode_of_visit;
	public String pickup_time;
	public String driver_name;
	public String pickup_address;
	public String CRE_name;
	public String advisor_name;
	public long serviceBookedId;
	public long callInteraction_id;
	public long vehicle_vehicl_id;
	public long customer_id;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getBooking_reg_date() {
		return booking_reg_date;
	}
	public void setBooking_reg_date(Date booking_reg_date) {
		this.booking_reg_date = booking_reg_date;
	}
	public Date getBooked_date() {
		return booked_date;
	}
	public void setBooked_date(Date booked_date) {
		this.booked_date = booked_date;
	}
	public String getWorkshop() {
		return workshop;
	}
	public void setWorkshop(String workshop) {
		this.workshop = workshop;
	}
	public String getServiceBookedType() {
		return serviceBookedType;
	}
	public void setServiceBookedType(String serviceBookedType) {
		this.serviceBookedType = serviceBookedType;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getMobile_number() {
		return mobile_number;
	}
	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getVeh_model() {
		return veh_model;
	}
	public void setVeh_model(String veh_model) {
		this.veh_model = veh_model;
	}
	public String getMode_of_visit() {
		return mode_of_visit;
	}
	public void setMode_of_visit(String mode_of_visit) {
		this.mode_of_visit = mode_of_visit;
	}
	public String getPickup_time() {
		return pickup_time;
	}
	public void setPickup_time(String pickup_time) {
		this.pickup_time = pickup_time;
	}
	public String getDriver_name() {
		return driver_name;
	}
	public void setDriver_name(String driver_name) {
		this.driver_name = driver_name;
	}
	public String getPickup_address() {
		return pickup_address;
	}
	public void setPickup_address(String pickup_address) {
		this.pickup_address = pickup_address;
	}
	public String getCRE_name() {
		return CRE_name;
	}
	public void setCRE_name(String cRE_name) {
		CRE_name = cRE_name;
	}
	public String getAdvisor_name() {
		return advisor_name;
	}
	public void setAdvisor_name(String advisor_name) {
		this.advisor_name = advisor_name;
	}
	public long getServiceBookedId() {
		return serviceBookedId;
	}
	public void setServiceBookedId(long serviceBookedId) {
		this.serviceBookedId = serviceBookedId;
	}
	public long getCallInteraction_id() {
		return callInteraction_id;
	}
	public void setCallInteraction_id(long callInteraction_id) {
		this.callInteraction_id = callInteraction_id;
	}
	public long getVehicle_vehicl_id() {
		return vehicle_vehicl_id;
	}
	public void setVehicle_vehicl_id(long vehicle_vehicl_id) {
		this.vehicle_vehicl_id = vehicle_vehicl_id;
	}
	public long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}
	public ServiceBookedResultList(String status, Date booking_reg_date, Date booked_date, String workshop,
			String serviceBookedType, String customer_name, String mobile_number, String address, String veh_model,
			String mode_of_visit, String pickup_time, String driver_name, String pickup_address, String cRE_name,
			String advisor_name, long serviceBookedId, long callInteraction_id, long vehicle_vehicl_id,
			long customer_id) {
		super();
		this.status = status;
		this.booking_reg_date = booking_reg_date;
		this.booked_date = booked_date;
		this.workshop = workshop;
		this.serviceBookedType = serviceBookedType;
		this.customer_name = customer_name;
		this.mobile_number = mobile_number;
		this.address = address;
		this.veh_model = veh_model;
		this.mode_of_visit = mode_of_visit;
		this.pickup_time = pickup_time;
		this.driver_name = driver_name;
		this.pickup_address = pickup_address;
		this.CRE_name = cRE_name;
		this.advisor_name = advisor_name;
		this.serviceBookedId = serviceBookedId;
		this.callInteraction_id = callInteraction_id;
		this.vehicle_vehicl_id = vehicle_vehicl_id;
		this.customer_id = customer_id;
	}
	
	public ServiceBookedResultList() {
		super();
		this.status = "";
		this.booking_reg_date = null;
		this.booked_date = null;
		this.workshop = "";
		this.serviceBookedType = "";
		this.customer_name = "";
		this.mobile_number = "";
		this.address = "";
		this.veh_model = "";
		this.mode_of_visit = "";
		this.pickup_time = "";
		this.driver_name = "";
		this.pickup_address = "";
		this.CRE_name = "";
		this.advisor_name = "";
		this.serviceBookedId = (long)0;
		this.callInteraction_id = (long)0;
		this.vehicle_vehicl_id = (long)0;
		this.customer_id = (long)0;
	}
	
	
	
	
	
}
