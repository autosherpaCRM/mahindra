/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

/**
 *
 * @author W-885
 */
public class ServicebookedDataOnTabLoad {
    
    public String serviceScheduledDate;
    public String serviceScheduledTime;

    public String getServiceScheduledDate() {
        return serviceScheduledDate;
    }

    public void setServiceScheduledDate(String serviceScheduledDate) {
        this.serviceScheduledDate = serviceScheduledDate;
    }

    public String getServiceScheduledTime() {
        return serviceScheduledTime;
    }

    public void setServiceScheduledTime(String serviceScheduledTime) {
        this.serviceScheduledTime = serviceScheduledTime;
    }
    
    
}
