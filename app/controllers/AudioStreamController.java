package controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;

import play.Configuration;
import play.Logger.ALogger;
import play.mvc.RangeResults;
import play.mvc.Result;
import repositories.AllCallInteractionRepository;
import repositories.WyzUserRepository;
import models.CallInteraction;
import repositories.CallInteractionsRepository;

public class AudioStreamController extends play.mvc.Controller {
	
	
	public static String PLAY_CONFIG_TEMP_AUDIO_PATH="app.audiofilepath";
	
	ALogger logger = play.Logger.of("application");
	
	private final WyzUserRepository userRepository;
	private PlaySessionStore playSessionStore;
	private AllCallInteractionRepository repository;
	private Configuration playConfiguration;
	private final CallInteractionsRepository call_int_repo;
	
	private String basePath="";
	
	private File file; 

	
	@Inject
	public AudioStreamController( WyzUserRepository userRepo, AllCallInteractionRepository repo,
			PlaySessionStore playSession,Configuration playconfig,CallInteractionsRepository interRepo) {

		userRepository = userRepo;
		playSessionStore = playSession;
		repository = repo;
		file = new File("D:\\temp\\ScottJoplin-TheEntertainer1902.mp3");
		playConfiguration = playconfig;
		call_int_repo = interRepo;
		
		basePath = playConfiguration.getString(PLAY_CONFIG_TEMP_AUDIO_PATH);
	}
	
	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}
	
	@Secure(clients = "FormClient")
	public Result header(String id)throws Exception{
		long idL = Long.parseLong(id);
		byte[] audio = repository.getMediaFileMR(idL);
		CallInteraction callInte = call_int_repo.getCallInteractionById(Long.parseLong(id));
		String reno="";
		if(callInte.getVehicle()!=null){
			
			reno=callInte.getVehicle().getVehicleRegNo();
		}
		String nameMedia =basePath+reno+"_"+callInte.getWyzUser().getUserName()+"_"+callInte.getId()+ ".mp3";
		//String filePath = basePath + id + ".mp4";
		
		File audiofile = new File(nameMedia);
		if(!audiofile.exists()){
			FileOutputStream fos = new FileOutputStream(audiofile);
			fos.write(audio);
			fos.flush();
			fos.close();
			logger.info("file exist :" +nameMedia);

		}
		response().setHeader(CONTENT_LENGTH, Long.toString(audiofile.length()));
		return status(206);
	}

	@Secure(clients = "FormClient")
	public Result streamAudio(String id) throws Exception{
		long idL = Long.parseLong(id);
		byte[] audio = repository.getMediaFileMR(idL);
		CallInteraction callInte = call_int_repo.getCallInteractionById(Long.parseLong(id));
		String reno="";
		if(callInte.getVehicle()!=null){
			
			reno=callInte.getVehicle().getVehicleRegNo();
		}
		String nameMedia =basePath+reno+"_"+callInte.getWyzUser().getUserName()+"_"+callInte.getId()+ ".mp3";
		
		//String nameMedia = basePath + callInte.getId()+"_"+callInte.getWyzUser().getUserName()+ ".mp3";
		//String filePath = basePath + id + ".mp4";
		
		File audiofile = new File(nameMedia);
		if(!audiofile.exists() && audio!=null){
			logger.info("audioFileExist");
			FileOutputStream fos = new FileOutputStream(audiofile);
			fos.write(audio);
			fos.flush();
			fos.close();
			logger.info("file exist :" +nameMedia);
				
		return buildStreamRange(audiofile);		
		}else if(audiofile.exists()){
			logger.info("audioFileExist");
			return buildStreamRange(audiofile);	
		}else{
			
			logger.info("audioFileEmpty");
			return ok();
		}
		/*if(audio!= null){
			return buildStreamRange(new ByteArrayInputStream(audio));
		}else{
			return internalServerError();
		}
		*/
		
	}
	
	
	@Secure(clients = "FormClient")
	public Result headerFile(String id) throws Exception {

		// int uniqueId=Integer.parseInt(id);

		int uniqueId = 35541;

		CallInteraction callinteraction = call_int_repo.getCallinteractionByUniqueId(uniqueId);
		
		if (callinteraction.getFilePath() != null) {

			String nameMedia = basePath + callinteraction.getFilePath();

			// String nameMedia = basePath +
			// callInte.getId()+"_"+callInte.getWyzUser().getUserName()+ ".mp3";
			// String filePath = basePath + id + ".mp4";

			File audiofile = new File(nameMedia);
			if (audiofile.exists()) {
				logger.info("file exist :" + nameMedia);

				response().setHeader(CONTENT_LENGTH, Long.toString(audiofile.length()));
			}else{
				
				response().setHeader(CONTENT_LENGTH, Long.toString(0));
			}
		}
		
		return status(206);
	}

	@Secure(clients = "FormClient")
	public Result streamAudioFile(String id) throws Exception {

		// int uniqueId=Integer.parseInt(id);

		int uniqueId = 35541;

		CallInteraction callinteraction = call_int_repo.getCallinteractionByUniqueId(uniqueId);
		if (callinteraction.getFilePath() != null) {

			String nameMedia = basePath + callinteraction.getFilePath();

			// String nameMedia = basePath +
			// callInte.getId()+"_"+callInte.getWyzUser().getUserName()+ ".mp3";
			// String filePath = basePath + id + ".mp4";

			File audiofile = new File(nameMedia);
			if (audiofile.exists()) {
				logger.info("file exist :" + nameMedia);

				return buildStreamRange(audiofile);
			} else {
				return ok();
			}
		} else {

			return ok();
		}

	}


	
	
	
	
	
	private Result buildStreamRange(final File asset) throws Exception {
	    response().setHeader("Accept-Ranges", "bytes");
	    return RangeResults.ofFile(asset).as("audio/mp4");
		
	}
	
	private Result buildStreamRange(InputStream ins) throws Exception {
	    response().setHeader("Accept-Ranges", "bytes");
	    
	    //akka.stream.scaladsl.StreamConverters.fromInputStream();
	    return null;
		
	}

	private Result buildStreamRange(byte[] audio) throws Exception {
	    response().setHeader("Accept-Ranges", "bytes");
	    
	    
	    return RangeResults.ofStream(new ByteArrayInputStream(audio)).as("audio/mp4");
	    
	    //akka.stream.scaladsl.StreamConverters.fromInputStream();
	    // return null;
		
	}
	
	
	

}
