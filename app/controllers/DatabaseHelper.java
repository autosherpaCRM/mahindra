/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
/**
 *
 * @author W-885
 */
public class DatabaseHelper {
  private static boolean persistenceEnable = false;
  private static FirebaseDatabase mDatabase;


  public static boolean isPersistenceEnable(){
    return persistenceEnable;
  }
  public static FirebaseDatabase getInstance() {
    if (mDatabase == null) {
      mDatabase = FirebaseDatabase.getInstance();
      if(persistenceEnable==true) {
        mDatabase.setPersistenceEnabled(true);
      }
    }

    return mDatabase;
  }
}
