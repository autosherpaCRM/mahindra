package controllers;

import java.util.List;

import javax.inject.Inject;

import org.pac4j.core.client.Client;
import org.pac4j.core.client.Clients;
import org.pac4j.core.config.Config;
import org.pac4j.core.context.Pac4jConstants;
import org.pac4j.core.exception.HttpAction;
import org.pac4j.core.exception.TechnicalException;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.http.client.indirect.FormClient;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;

import models.JsonContent;
import play.Logger.ALogger;
import play.mvc.Controller;
import play.mvc.Result;
import play.twirl.api.Content;
import repositories.WyzUserRepository;


public class Application extends Controller {

	
	protected static ALogger logger = play.Logger.of("application");

	
	private Config config;

	
	private PlaySessionStore playSessionStore;

	private List<CommonProfile> getProfiles() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.info("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.info("..............Obtained profiles........."+ profiles);
		return profiles;
	}

	@Inject
	public Application(Config cnfg, PlaySessionStore plstore) {
		
		this.config = cnfg;
		this.playSessionStore = plstore;
	}

	@Secure(clients = "AnonymousClient", authorizers = "csrfToken")
	public Result index() throws Exception {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final String sessionId = context.getSessionIdentifier();
		final String token = (String) context.getRequestAttribute(Pac4jConstants.CSRF_TOKEN);
		// profiles (maybe be empty if not authenticated)
		return ok(views.html.index.render(getProfiles(), token, sessionId));
	}

	private Result protectedIndexView() {
		// profiles
		return ok(views.html.protectedIndex.render(getProfiles()));
	}
	
    @Secure
    public Result protectedIndex() {
        return protectedIndexView();
    }

    @Secure(clients = "FormClient")
    public Result formIndex() {
        return protectedIndexView();
    }

    // Setting the isAjax parameter is no longer necessary as AJAX requests are automatically detected:
    // a 401 error response will be returned instead of a redirection to the login url.
    @Secure(clients = "FormClient")
    public Result formIndexJson() {
        Content content = views.html.protectedIndex.render(getProfiles());
        JsonContent jsonContent = new JsonContent(content.body());
        return ok(jsonContent);
    }
    
    //@Secure(clients = "AnonymousClient", authorizers = "csrfCheck")
    public Result csrfIndex() {
        return ok(views.html.csrf.render(getProfiles()));
    }

    public Result login() throws TechnicalException {
        final FormClient formClient = (FormClient) config.getClients().findClient("FormClient");
        return ok(views.html.login.render(formClient.getCallbackUrl()));
    }
    
    @Secure(clients = "IndirectBasicAuthClient")
    public Result basicauthIndex() {
        return protectedIndexView();
    }

    @Secure(clients = "DirectBasicAuthClient,ParameterClient")
    public Result dbaIndex() {
        return protectedIndexView();
    }
    
    public Result landingPage() {
		return ok(views.html.landingPage.render());
	}

    
    public Result forceLogin() {
        final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
        final Client client = config.getClients().findClient(context.getRequestParameter(Clients.DEFAULT_CLIENT_NAME_PARAMETER));
        try {
            final HttpAction action = client.redirect(context);
            return (Result) config.getHttpActionAdapter().adapt(action.getCode(), context);
        } catch (final HttpAction e) {
            throw new TechnicalException(e);
        }
    }



}
