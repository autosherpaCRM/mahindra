package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static play.libs.Json.toJson;
import javax.inject.Inject;
import javax.sql.DataSource;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.java.Secure;

import org.springframework.stereotype.Controller;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.typesafe.config.ConfigException.Parse;

import actors.InsuranceDataInsert;
import actors.InsuranceDataInsertProtocol;
import actors.ServiceAdvisorHistorySyncProtocol;
import actors.ServiceAdvisorSummaryDayWiseSync;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.Props;
import models.Campaign;
import models.JobCardDetailsUpload;
import models.Location;
import models.NewCustomer;
import models.UploadData;
import models.Workshop;
import models.workshopBill;
import play.Logger.ALogger;
import play.data.Form;
import play.db.DB;
import play.libs.Akka;
import play.libs.F;
import play.libs.F.*;
import play.mvc.Http.MultipartFormData;
import play.mvc.Result;
import play.mvc.BodyParser.TolerantJson;
import repositories.CallInteractionsRepository;
import repositories.ExcelUploadRepo;
import scala.concurrent.duration.Duration;
import utils.JobCardStatus;
import utils.SMRData;
import utils.SaleRegisterExcel;
import utils.WorkBillRegister;
import views.html.uploadMasterFilesForm;
import utils.SheetHandler;
import utils.CampaignExcel;
import utils.InsuranceExcel;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;
@Controller
public class UploadExcelController extends  play.mvc.Controller {

	ALogger logger = play.Logger.of("application");
	
	private Cancellable scheduler = null;

	private CallInteractionsRepository callinteractionRepo;
	private ExcelUploadRepo exlRepo;
	private PlaySessionStore playSessionStore;

	private org.pac4j.core.config.Config config;
	@Inject
	public UploadExcelController(CallInteractionsRepository repo, ExcelUploadRepo xlRepo,
			org.pac4j.core.config.Config config,PlaySessionStore playses) {

		callinteractionRepo = repo;
		exlRepo = xlRepo;
		config = (org.pac4j.core.config.Config) config;
		playSessionStore=playses;
	}

	
	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	
	private long getUserLoginId() {
		long user_Id = (long) getUserProfile().getAttribute("USER_ID");

		return user_Id;

	}

	private String getFileExtension(String filename) {
		try {
			return filename.substring(filename.lastIndexOf(".") + 1);
		} catch (Exception e) {
			return "";
		}
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	public Promise<Result> uploadExcelData() {

		MultipartFormData body = request().body().asMultipartFormData();
		MultipartFormData.FilePart profile = body.getFile("uploadfiles");
		File file = (File) profile.getFile();
		String filename = profile.getFilename();
		logger.info("filename new one" + filename);

		String fileExt = getFileExtension(filename);
		String result = filename.substring(0, filename.lastIndexOf("."));
		String fileNameAfterTime = new SimpleDateFormat("yyyyMMddhhmm'.xlsx'").format(new Date());

		String uploadIdAfterTime = new SimpleDateFormat("yyyyMMddhhmm").format(new Date());

		String finalUploadId = "U".concat(uploadIdAfterTime);
		String finalName = result.concat(fileNameAfterTime);
		logger.info("file concatinate " + finalName);
		logger.info("finalUploadId for excel sheet is : " + finalUploadId);

		Promise<List<String>> resultdata = null;

		if (fileExt.equalsIgnoreCase("xlsx")) {
			logger.info("upload excel sheet");
			Form<UploadData> upload_data = Form.form(UploadData.class).bindFromRequest();
			UploadData uploadData = upload_data.get();

			String sheetname = uploadData.getSheetName();
			String dataTypefile = uploadData.getDataType();
			String camFromDate = uploadData.getCampaignStartdate();
			String camExpDate = uploadData.getCampaignExpiry();

			logger.info("camExpDate is :" + camExpDate);
			Form<Location> loc_upload_data = Form.form(Location.class).bindFromRequest();
			Location uploadData_loc = loc_upload_data.get();

			String locationdata = uploadData_loc.getName();

			Form<Workshop> loc_upload_work = Form.form(Workshop.class).bindFromRequest();
			Workshop uploadData_work = loc_upload_work.get();

			Long workshopId = uploadData_work.getId();
			
			com.typesafe.config.Config configuration = com.typesafe.config.ConfigFactory.load();
			String getDirectory = configuration.getString("app.filepath");
			logger.info("directory name" + getDirectory);
			String resultName = getDirectory.concat(finalName);
			logger.info("result name" + resultName);
			boolean results = file.renameTo(new File(getDirectory + finalName));
			logger.info("boolean remane" + results);
			logger.info("I am here............");
			logger.info("data type file" + dataTypefile);

			UploadData insertInUpload = callinteractionRepo.setRequiredFieldsForUpload(getUserLoginId(), sheetname,
					dataTypefile, locationdata, workshopId, finalName, camFromDate, camExpDate, finalUploadId);

			switch (dataTypefile) {

			case "sale_register":

				/* Add for other columns */

				ArrayList<HashMap> mappedDataSales = getSalesRegisterMappingData();

				HashMap<Integer, String> fieldsOfObject = mappedDataSales.get(0);
				HashMap<Integer, String> excelColumnNames = mappedDataSales.get(1);

				HashMap<String, String> formData = new HashMap<String, String>();

				formData.put("dataType", dataTypefile);
				formData.put("uploadId", insertInUpload.getUpload_id());
				formData.put("modelType", "utils.SaleRegisterExcel");

				SaleRegisterExcel SalesPojo = new SaleRegisterExcel();
				try {
					logger.info("Before exceltoDataBase method");

					resultdata = excelToDataBase(resultName, fieldsOfObject, excelColumnNames, formData, SalesPojo,
							insertInUpload);

					// Promise<String> slicedBreadPromise =
					// excelDataToDBSales(resultName, fieldsOfObject,
					// excelColumnNames, formData, SalesPojo,
					// uploadSuccessRecordSale,uploadRejectedRecordSale,insertInUpload);

					logger.info("After exceltoDataBase method");
				} catch (IOException e) {
					logger.error("Error while reading the file", e);
				} catch (OpenXML4JException e) {
					logger.error("XML exception", e);
				} catch (SAXException e) {
					logger.error("SAX Parser exception", e);
				}

				break;

			case "job_card":

				ArrayList<HashMap> mappedDataJobCard = getJobCardMappingData();

				HashMap<Integer, String> fieldsOfObjectJC = mappedDataJobCard.get(0);
				HashMap<Integer, String> excelColumnNamesJC = mappedDataJobCard.get(1);

				HashMap<String, String> formDataJC = new HashMap<String, String>();

				formDataJC.put("dataType", dataTypefile);
				formDataJC.put("uploadId", insertInUpload.getUpload_id());
				formDataJC.put("workShopId", String.valueOf(workshopId));
				formDataJC.put("modelType", "utils.JobCardStatus");
				JobCardStatus pojoJC = new JobCardStatus();
				try {
					logger.info("Before exceltoDataBase method");
					resultdata = excelToDataBase(resultName, fieldsOfObjectJC, excelColumnNamesJC, formDataJC, pojoJC,
							insertInUpload);
					logger.info("After exceltoDataBase method");
				} catch (IOException e) {
					logger.error("Error while reading the file", e);
				} catch (OpenXML4JException e) {
					logger.error("XML exception", e);
				} catch (SAXException e) {
					logger.error("SAX Parser exception", e);
				}

				break;

			case "bill":

				ArrayList<HashMap> mappedDataWorkBill = getWorkBillMappingData();

				HashMap<Integer, String> fieldsOfObjectWB = mappedDataWorkBill.get(0);
				HashMap<Integer, String> excelColumnNamesWB = mappedDataWorkBill.get(1);

				HashMap<String, String> formDataWB = new HashMap<String, String>();

				formDataWB.put("dataType", dataTypefile);
				formDataWB.put("uploadId", insertInUpload.getUpload_id());
				formDataWB.put("modelType", "utils.WorkBillRegister");

				WorkBillRegister pojoWB = new WorkBillRegister();
				try {
					logger.info("Before exceltoDataBase method");
					resultdata = excelToDataBase(resultName, fieldsOfObjectWB, excelColumnNamesWB, formDataWB, pojoWB,
							insertInUpload);
					logger.info("After exceltoDataBase method");
				} catch (IOException e) {
					logger.error("Error while reading the file", e);
				} catch (OpenXML4JException e) {
					logger.error("XML exception", e);
				} catch (SAXException e) {
					logger.error("SAX Parser exception", e);
				}

				break;

			case "SMR":

				ArrayList<HashMap> mappedDataSMR = getSMRMappingData();

				HashMap<Integer, String> fieldsOfObjectSMR = mappedDataSMR.get(0);
				HashMap<Integer, String> excelColumnNamesSMR = mappedDataSMR.get(1);

				HashMap<String, String> formDataSMR = new HashMap<String, String>();

				formDataSMR.put("dataType", dataTypefile);
				formDataSMR.put("uploadId", insertInUpload.getUpload_id());
				formDataSMR.put("modelType", "utils.SMRData");

				SMRData pojoSMR = new SMRData();
				try {
					logger.info("Before exceltoDataBase method");
					resultdata = excelToDataBase(resultName, fieldsOfObjectSMR, excelColumnNamesSMR, formDataSMR,
							pojoSMR, insertInUpload);
					logger.info("After exceltoDataBase method");
				} catch (IOException e) {
					logger.error("Error while reading the file", e);
				} catch (OpenXML4JException e) {
					logger.error("XML exception", e);
				} catch (SAXException e) {
					logger.error("SAX Parser exception", e);
				}

				break;

			case "campaign":

				ArrayList<HashMap> mappedDataCampaign = getCampaignMappingData();

				HashMap<Integer, String> fieldsOfObjectCampaign = mappedDataCampaign.get(0);
				HashMap<Integer, String> excelColumnNamesCampaign = mappedDataCampaign.get(1);

				HashMap<String, String> formDataCampaign = new HashMap<String, String>();
				
				String campName=uploadData.getCampaignName();

				formDataCampaign.put("dataType", dataTypefile);
				formDataCampaign.put("uploadId", insertInUpload.getUpload_id());
				formDataCampaign.put("campaignStartdate", camFromDate);
				formDataCampaign.put("campaignExpiry", camExpDate);
				formDataCampaign.put("modelType", "utils.CampaignExcel");
				
				long campId=exlRepo.getCampaignIdByCamp(campName,"Campaign");

				insertInUpload.setCampaignName(campName);
				insertInUpload.setCampaignId(campId);
				
				CampaignExcel pojoCampaign = new CampaignExcel();
				try {
					logger.info("Before exceltoDataBase method");
					resultdata = excelToDataBase(resultName, fieldsOfObjectCampaign, excelColumnNamesCampaign,
							formDataCampaign, pojoCampaign, insertInUpload);
					logger.info("After exceltoDataBase method");
				} catch (IOException e) {
					logger.error("Error while reading the file", e);
				} catch (OpenXML4JException e) {
					logger.error("XML exception", e);
				} catch (SAXException e) {
					logger.error("SAX Parser exception", e);
				}

				break;
				
			case "Insurance":
				
				ArrayList<HashMap> mappedDataInsurance=getInsuranceMapingData();
				
				HashMap<Integer, String> fieldsOfObjectInsurance = mappedDataInsurance.get(0);
				HashMap<Integer, String> excelColumnNamesInsurance = mappedDataInsurance.get(1);
				
				HashMap<String, String> formDataInsurance = new HashMap<String, String>();

				
				String campNameIns=uploadData.getCampaignName();
				formDataInsurance.put("dataType", dataTypefile);
				formDataInsurance.put("uploadId", insertInUpload.getUpload_id());
				formDataInsurance.put("campaignStartdate", camFromDate);
				formDataInsurance.put("campaignExpiry", camExpDate);
				formDataInsurance.put("modelType", "utils.InsuranceExcel");
				long campIdIn=exlRepo.getCampaignIdByCamp(campNameIns,"Insurance");
				
				insertInUpload.setCampaignName(campNameIns);
				insertInUpload.setCampaignId(campIdIn);

				InsuranceExcel pojoInsurance = new InsuranceExcel();
				try {
					logger.info("Before exceltoDataBase method");
					resultdata = excelToDataBase(resultName, fieldsOfObjectInsurance, excelColumnNamesInsurance,
							formDataInsurance, pojoInsurance, insertInUpload);
					logger.info("After exceltoDataBase method");
				} catch (IOException e) {
					logger.error("Error while reading the file", e);
				} catch (OpenXML4JException e) {
					logger.error("XML exception", e);
				} catch (SAXException e) {
					logger.error("SAX Parser exception", e);
				}

				break;

			}

			// file.delete();

			// final Promise<Result> resultPromise = resultdata.map(new
			// Function<List<HashMap>, Result>() {
			// @Override
			// public Result apply(List<HashMap> userInfo) {
			// return ok("done" + "\n" + "done");
			// }
			// }).recover(new Function<Throwable, Result>() {
			// @Override
			// public Result apply(Throwable throwable) throws Throwable {
			// return
			// badRequest(views.html.login.render(throwable.getMessage()));
			// }
			// });

			logger.info("Exiting the controller");
			// return ok("done");
			// return
			// redirect(controllers.routes.CallInteractionController.upload_Excel_Sheet(insertInUpload.getUpload_id(),dataTypefile,));

			// return
			// ok(uploadMasterFilesForm.render(resultdata,insertInUpload.getUpload_id(),dataTypefile,campaignList,
			// locationList,
			// getUserLogindealerCode(), getUserLoginName()));
			// F.Promise<List<Integer>> promise = F.Promise.promise(() ->
			// longRunningCalculation());
			
			if(dataTypefile.equals("Insurance")){
				
				return resultdata.map((List<String> i) -> redirect(controllers.routes.InsuranceController
						.upload_Excel_Sheet_insurance(i.get(2), i.get(3), i.get(0) + "," + i.get(1))));
				
			}else{
				
				return resultdata.map((List<String> i) -> redirect(controllers.routes.CallInteractionController
						.upload_Excel_Sheet(i.get(2), i.get(3), i.get(0) + "," + i.get(1))));
				
			}

		} else {

			logger.info("Exiting the controller if extension is not xlsx");
			// return null;
			// return
			// redirect(controllers.routes.CallInteractionController.upload_Excel_Sheet("0","0"));

			// F.Promise<List<Integer>> promise = F.Promise.promise(() ->
			// longRunningCalculation());

			return resultdata.map((List<String> i) -> redirect(
					controllers.routes.CallInteractionController.upload_Excel_Sheet("0", "0", "0")));
		}

	}

	private Promise<List<String>> excelToDataBase(String fileName, HashMap modelFields, HashMap excelHeaders,
			HashMap<String, String> additonalParameters, Object uploadModel, UploadData insertInUpload)
			throws IOException, OpenXML4JException, SAXException {

		HashMap<Date, Integer> uploadSuccessRecord = new HashMap<Date, Integer>();
		HashMap<Date, Integer> uploadRejectedRecord = new HashMap<Date, Integer>();

		// Hashmap Key value :- will be used to match the Object Field,Data Type
		// and Excel Headers.
		// additonalParameters :- Data's which need to post from Form.(Optinal);

		List<HashMap> uploadResultFields = new ArrayList<HashMap>();

		List<HashMap> mappingFields = new ArrayList<HashMap>();
		mappingFields.add(modelFields); // Fields of the Object uploadModel
		// mappingFields.add(dataType); // Data Type of the Object Fields
		mappingFields.add(excelHeaders); // Matching Excel Headers for the

		// Object Fields

		logger.info("controller mappingFields.size : " + mappingFields.size());
		// final String fileName = fileName;
		/*
		 * Opens a package (archive / xlsx file) with read / write permissions.
		 * It is also possible to access it read only, which should be the first
		 * choice for read operations in case the file is already accessed by
		 * another user. To open read only provide an InputStream instead of a
		 * file path.
		 */
		InputStream input = new FileInputStream(fileName);

		XSSFWorkbook wb = new XSSFWorkbook(input);
		XSSFSheet sheet = wb.getSheetAt(0);
		int row_count = sheet.getPhysicalNumberOfRows();

		// String upload_id = additonalParameters.get("uploadId");
		// long upload_id_concver = Integer.parseInt(upload_id);

		// exlRepo.updateTherowCountOfExcelsheet(row_count,upload_id_concver);

		logger.info("controller row_count of excel sheet : " + row_count);

		input.close();

		InputStream input1 = new FileInputStream(fileName);
		OPCPackage pkg = OPCPackage.open(input1);
		XSSFReader r = new XSSFReader(pkg);

		/*
		 * Read the sharedStrings.xml from an xlsx file into an object
		 * representation.
		 */
		SharedStringsTable sst = r.getSharedStringsTable();

		/*
		 * Hand a read SharedStringsTable for further reference to the SAXParser
		 * and the underlying ContentHandler.
		 */
		XMLReader parser = fetchSheetParser(sst, mappingFields, additonalParameters, uploadModel, uploadSuccessRecord,
				uploadRejectedRecord);

		/*
		 * O To look up the Sheet Name / Sheet Order / rID, you need to process
		 * the core Workbook stream. Normally it's of the form rId# or rSheet#
		 * 
		 * Great! How do I know, if it is rSheet or rId? Thanks Microsoft.
		 * Anyhow, let's carry on with the noise.
		 * 
		 * I reference the third sheet from the left since the index starts at
		 * one.
		 */
		InputStream sheet1 = r.getSheet("rId1");
		InputSource sheetSource = new InputSource(sheet1);

		/*
		 * Run through a Sheet using a window of several XML tags instead of
		 * attempting to read the whole file into RAM at once. Leaves the
		 * handling of file content to the ContentHandler, which is in this case
		 * the nested class SheetHandler.
		 */
		logger.info("controller Before Parse");
		parser.parse(sheetSource);

		logger.info(" controller After Parse");

		/*
		 * Close the underlying InputStream for a Sheet XML.
		 */
		sheet1.close();

		Map<Date, Integer> map = uploadRejectedRecord;

		for (Map.Entry<Date, Integer> entrydata : map.entrySet()) {
			// logger.info("add data to String array");
			// System.out.println("Key for Rejected is = " + entrydata.getKey()
			// + ", Value = " + entrydata.getValue());

			insertInUpload.setId(0);
			if (uploadSuccessRecord.containsKey(entrydata.getKey())) {

			} else {

				logger.info("22");
				insertInUpload.setRecordedDate(entrydata.getKey());
				insertInUpload.setSuccessRecords(0);
				insertInUpload.setRejectedRecords(entrydata.getValue());

				exlRepo.addTheUplodedExcelData(insertInUpload);
			}

		}

		Map<Date, Integer> mapData = uploadSuccessRecord;

		for (Map.Entry<Date, Integer> entrydata1 : mapData.entrySet()) {

			insertInUpload.setId(0);
			// logger.info("add data to String array");
			// System.out.println("Key for Success is = " + entrydata1.getKey()
			// + ", Value = " + entrydata1.getValue());

			if (uploadRejectedRecord.containsKey(entrydata1.getKey())) {

				insertInUpload.setRecordedDate(entrydata1.getKey());
				insertInUpload.setSuccessRecords(entrydata1.getValue());

				Object rejectedRecordis = uploadRejectedRecord.get(entrydata1.getKey());

				insertInUpload.setRejectedRecords((int) rejectedRecordis);

				exlRepo.addTheUplodedExcelData(insertInUpload);

			} else {

				logger.info("33");
				insertInUpload.setRecordedDate(entrydata1.getKey());
				insertInUpload.setSuccessRecords(entrydata1.getValue());
				insertInUpload.setRejectedRecords(0);
				exlRepo.addTheUplodedExcelData(insertInUpload);

			}

		}
		String selectedDataTyp = additonalParameters.get("dataType");
		if(selectedDataTyp.equals("Insurance")){
			//assignInsuranceDataByProcedure(insertInUpload.getUpload_id());
			logger.info("Sync of insert insurance");
			startInsuranceDetailsSync(insertInUpload.getUpload_id());
			
		}
		
		List<String> rejectedCountOfExce = exlRepo.getExcelTotalAndRejectedCounts(insertInUpload.getUpload_id());
		rejectedCountOfExce.add(insertInUpload.getUpload_id());
		rejectedCountOfExce.add(selectedDataTyp);
		return F.Promise.promise(() -> rejectedCountOfExce);

	}

	private XMLReader fetchSheetParser(SharedStringsTable sst, List<HashMap> mappingFields, HashMap formData,
			Object uploadObject, HashMap uploadSuccessRecord, HashMap uploadRejectedRecord) throws SAXException {
		/*
		 * XMLReader parser = XMLReaderFactory
		 * .createXMLReader("org.apache.xerces.parsers.SAXParser");
		 */
		XMLReader parser = XMLReaderFactory.createXMLReader();
		logger.info("Controller Before Sheet Handler");
		ContentHandler handler = new SheetHandler(sst, mappingFields, formData, uploadObject, exlRepo,
				uploadSuccessRecord, uploadRejectedRecord);
		logger.info("Controller After Sheet Handler");
		parser.setContentHandler(handler);
		return parser;
	}

	private ArrayList<HashMap> getSalesRegisterMappingData() {

		HashMap<Integer, String> fieldsOfObject = new HashMap<Integer, String>();
		HashMap<Integer, String> excelColumnNames = new HashMap<Integer, String>();

		fieldsOfObject.put(0, "outlet");
		excelColumnNames.put(0, "Outlet1");

		fieldsOfObject.put(1, "locCode");
		excelColumnNames.put(1, "Location_Code");

		fieldsOfObject.put(2, "saleType");
		excelColumnNames.put(2, "Sale_TYPE");

		fieldsOfObject.put(3, "status");
		excelColumnNames.put(3, "Status");

		fieldsOfObject.put(4, "invNo");
		excelColumnNames.put(4, "Inv_No");

		fieldsOfObject.put(5, "invDate");
		excelColumnNames.put(5, "Inv_dt");

		fieldsOfObject.put(6, "invCancelNo");
		excelColumnNames.put(6, "CANCEL_No");

		fieldsOfObject.put(7, "invCancelDate");
		excelColumnNames.put(7, "CANCEL_DATE");

		fieldsOfObject.put(8, "delDate");
		excelColumnNames.put(8, "Del_DT");

		fieldsOfObject.put(9, "promisedDelvDate");
		excelColumnNames.put(9, "Prom_Delv._Date");

		fieldsOfObject.put(10, "model");
		excelColumnNames.put(10, "Model");

		fieldsOfObject.put(11, "fuelType");
		excelColumnNames.put(11, "Fuel_Type");

		fieldsOfObject.put(12, "variantCode");
		excelColumnNames.put(12, "Variant_cd");

		fieldsOfObject.put(13, "varaintDesc");
		excelColumnNames.put(13, "Variant_DESC");

		fieldsOfObject.put(14, "colorcode");
		excelColumnNames.put(14, "Clr_cd");

		fieldsOfObject.put(15, "colorDesc");
		excelColumnNames.put(15, "Clr_DESC");

		fieldsOfObject.put(16, "chassis");
		excelColumnNames.put(16, "Chassis");

		fieldsOfObject.put(17, "engine");
		excelColumnNames.put(17, "Engine");

		fieldsOfObject.put(18, "mulInvNo");
		excelColumnNames.put(18, "MUL_INV_No");

		fieldsOfObject.put(19, "mulInvDate");
		excelColumnNames.put(19, "Mul_Inv_Dt");

		fieldsOfObject.put(20, "customerId");
		excelColumnNames.put(20, "Customer_id");

		fieldsOfObject.put(21, "salutation");
		excelColumnNames.put(21, "Salutation");

		fieldsOfObject.put(22, "customerName");
		excelColumnNames.put(22, "Customer_Name");

		fieldsOfObject.put(23, "address1");
		excelColumnNames.put(23, "ADD1");

		fieldsOfObject.put(24, "address2");
		excelColumnNames.put(24, "ADD2");

		fieldsOfObject.put(25, "address3");
		excelColumnNames.put(25, "ADD3");

		fieldsOfObject.put(26, "city");
		excelColumnNames.put(26, "City");

		fieldsOfObject.put(27, "pincode");
		excelColumnNames.put(27, "Pincode");

		fieldsOfObject.put(28, "picDesc");
		excelColumnNames.put(28, "Pin_DESC");

		fieldsOfObject.put(29, "teamLead");
		excelColumnNames.put(29, "Team_Lead");

		fieldsOfObject.put(30, "dse");
		excelColumnNames.put(30, "DSE");

		fieldsOfObject.put(31, "dsa");
		excelColumnNames.put(31, "DSA");

		fieldsOfObject.put(32, "hypthecation");
		excelColumnNames.put(32, "Hypo");

		fieldsOfObject.put(33, "hyp_address");
		excelColumnNames.put(33, "Hyp_Address");

		fieldsOfObject.put(34, "hypAmt");
		excelColumnNames.put(34, "Hyp_Amount");

		fieldsOfObject.put(35, "regNo");
		excelColumnNames.put(35, "Reg_No");

		fieldsOfObject.put(36, "exWarrantyType");
		excelColumnNames.put(36, "EW_Type");

		fieldsOfObject.put(37, "stdCode");
		excelColumnNames.put(37, "STD_code");

		fieldsOfObject.put(38, "resCode");
		excelColumnNames.put(38, "Res_Phone");

		fieldsOfObject.put(39, "stdCodeComp");
		excelColumnNames.put(39, "STD_Code_comp");

		fieldsOfObject.put(40, "offPhone");
		excelColumnNames.put(40, "OFF_Phone");

		fieldsOfObject.put(41, "mobile1");
		excelColumnNames.put(41, "Mobile1");

		fieldsOfObject.put(42, "mobile2");
		excelColumnNames.put(42, "Mobile2");

		fieldsOfObject.put(43, "emailId");
		excelColumnNames.put(43, "E-mail_Id");

		fieldsOfObject.put(44, "basicPrice");
		excelColumnNames.put(44, "Basic_Price");

		fieldsOfObject.put(45, "additionalTax");
		excelColumnNames.put(45, "Addtional Tax");

		fieldsOfObject.put(46, "schemeOfGoi");
		excelColumnNames.put(46, "Disbursement of incentive under FAME Scheme of GOI");

		fieldsOfObject.put(47, "instiCust");
		excelColumnNames.put(47, "Discount for Corp/Institutional Customer");

		fieldsOfObject.put(48, "discount");
		excelColumnNames.put(48, "Discount");

		fieldsOfObject.put(49, "csdExShowRoom");
		excelColumnNames.put(49, "CSD Ex Showroom Discount");

		fieldsOfObject.put(50, "extWarranty");
		excelColumnNames.put(50, "EXT. Warranty");

		fieldsOfObject.put(51, "loyaltyBonusDisc");
		excelColumnNames.put(51, "Exchange / Loyalty Bonus Discount");

		fieldsOfObject.put(52, "vat");
		excelColumnNames.put(52, "VAT");

		fieldsOfObject.put(53, "roundOff");
		excelColumnNames.put(53, "ROUND_OFF");

		fieldsOfObject.put(54, "ivnAmt");
		excelColumnNames.put(54, "Inv_Amt");

		fieldsOfObject.put(55, "purchasePrice");
		excelColumnNames.put(55, "Purchase_price");

		fieldsOfObject.put(56, "panNo");
		excelColumnNames.put(56, "PAN_No");

		fieldsOfObject.put(57, "enquiryNo");
		excelColumnNames.put(57, "Enquiry_No");

		fieldsOfObject.put(58, "enquirySources");
		excelColumnNames.put(58, "Enq_Source");

		fieldsOfObject.put(59, "enquirySubSource");
		excelColumnNames.put(59, "Enq_Sub-Source");

		fieldsOfObject.put(60, "buyerType");
		excelColumnNames.put(60, "Buyer_Type");

		fieldsOfObject.put(61, "tradeIn");
		excelColumnNames.put(61, "TradeIn");

		fieldsOfObject.put(62, "indivScheme");
		excelColumnNames.put(62, "Individual_Scheme");

		fieldsOfObject.put(63, "corporate");
		excelColumnNames.put(63, "Corporate");

		fieldsOfObject.put(64, "orderNum");
		excelColumnNames.put(64, "ORDER_NUMBER");

		fieldsOfObject.put(65, "orderDate");
		excelColumnNames.put(65, "ORDER_DATE");

		fieldsOfObject.put(66, "allotment");
		excelColumnNames.put(66, "Allotment_DATE");

		fieldsOfObject.put(67, "area");
		excelColumnNames.put(67, "Area_Code");

		fieldsOfObject.put(68, "areaType");
		excelColumnNames.put(68, "Area_Type");

		fieldsOfObject.put(69, "mgaSoldAmt");
		excelColumnNames.put(69, "MGA_Sold_Amt");

		fieldsOfObject.put(70, "evaluatorMSPIN");
		excelColumnNames.put(70, "Evaluator_Name_MSPIN");

		fieldsOfObject.put(71, "oldCarRegNo");
		excelColumnNames.put(71, "Old_Car_Reg_Num");

		fieldsOfObject.put(72, "oldCarMfg");
		excelColumnNames.put(72, "Old_Car_Manufacturer");

		fieldsOfObject.put(73, "oldCarModelCode");
		excelColumnNames.put(73, "Old_Car_Model_Code");

		fieldsOfObject.put(74, "oldCarStatus");
		excelColumnNames.put(74, "Old_Car_Status");

		fieldsOfObject.put(75, "oldCarOwner");
		excelColumnNames.put(75, "Old_Car_Owner");

		fieldsOfObject.put(76, "oldCarRelation");
		excelColumnNames.put(76, "Old_Car_Relation");

		fieldsOfObject.put(77, "exchangeCancDate");
		excelColumnNames.put(77, "Exch_Canc_Date");

		fieldsOfObject.put(78, "exchCancReason");
		excelColumnNames.put(78, "Exch_Canc_Reason");

		fieldsOfObject.put(79, "refType");
		excelColumnNames.put(79, "Reference_Type");

		fieldsOfObject.put(80, "refby");
		excelColumnNames.put(80, "Referred_By");

		fieldsOfObject.put(81, "refNo");
		excelColumnNames.put(81, "Refference_No.");

		fieldsOfObject.put(82, "refRegNo");
		excelColumnNames.put(82, "Ref_Vehicle_Regn_No.");

		fieldsOfObject.put(83, "refMobileNo");
		excelColumnNames.put(83, "Ref_Mobile_No.");

		fieldsOfObject.put(84, "miname");
		excelColumnNames.put(84, "MI_Name");

		fieldsOfObject.put(85, "miFlag");
		excelColumnNames.put(85, "MI_Flag");

		fieldsOfObject.put(86, "miDate");
		excelColumnNames.put(86, "MI_Date");

		fieldsOfObject.put(87, "stateDesc");
		excelColumnNames.put(87, "State_Desc");

		fieldsOfObject.put(88, "district");
		excelColumnNames.put(88, "District");

		fieldsOfObject.put(89, "tehsilDesc");
		excelColumnNames.put(89, "Tehsil_Desc");

		fieldsOfObject.put(90, "villageDesc");
		excelColumnNames.put(90, "Village_Desc");

		ArrayList<HashMap> returnData = new ArrayList<HashMap>();
		returnData.add(fieldsOfObject);
		returnData.add(excelColumnNames);

		return returnData;

	}

	private ArrayList<HashMap> getJobCardMappingData() {

		HashMap<Integer, String> fieldsOfObject = new HashMap<Integer, String>();
		HashMap<Integer, String> excelColumnNames = new HashMap<Integer, String>();

		fieldsOfObject.put(0, "slNo");
		excelColumnNames.put(0, "Srl.");

		fieldsOfObject.put(1, "jobCardNo");
		excelColumnNames.put(1, "Job Card No.");

		fieldsOfObject.put(2, "jobCardDate");
		excelColumnNames.put(2, "Date");

		fieldsOfObject.put(3, "repeatRevisit");
		excelColumnNames.put(3, "Repeat/Revisit");

		fieldsOfObject.put(4, "custName");
		excelColumnNames.put(4, "Customer Name");

		fieldsOfObject.put(5, "phone");
		excelColumnNames.put(5, "Phone");

		fieldsOfObject.put(6, "custCat");
		excelColumnNames.put(6, "Customer Catg");

		fieldsOfObject.put(7, "psfStatus");
		excelColumnNames.put(7, "Psf_Status");

		fieldsOfObject.put(8, "regNo");
		excelColumnNames.put(8, "Registration No.");

		fieldsOfObject.put(9, "chassis");
		excelColumnNames.put(9, "Chassis No.");

		fieldsOfObject.put(10, "model");
		excelColumnNames.put(10, "Model");

		fieldsOfObject.put(11, "mi");
		excelColumnNames.put(11, "MI");

		fieldsOfObject.put(12, "saleDate");
		excelColumnNames.put(12, "Sale Date");

		fieldsOfObject.put(13, "group");
		excelColumnNames.put(13, "Group");

		fieldsOfObject.put(14, "serviceAdvisor");
		excelColumnNames.put(14, "S.A");

		fieldsOfObject.put(15, "technician");
		excelColumnNames.put(15, "Technician");

		fieldsOfObject.put(16, "circularNo");
		excelColumnNames.put(16, "Circular No.");

		fieldsOfObject.put(17, "serviceType");
		excelColumnNames.put(17, "Service Type");

		fieldsOfObject.put(18, "mileage");
		excelColumnNames.put(18, "Mileage");

		fieldsOfObject.put(19, "estLabAmt");
		excelColumnNames.put(19, "Est.Lab Amt.");

		fieldsOfObject.put(20, "estPartAmt");
		excelColumnNames.put(20, "Est.Part Amt.");

		fieldsOfObject.put(21, "promiseDt");
		excelColumnNames.put(21, "Promised Dt.");

		fieldsOfObject.put(22, "revPromisedDt");
		excelColumnNames.put(22, "Rev. Promised Dt.");

		fieldsOfObject.put(23, "readyDateandTime");
		excelColumnNames.put(23, "Ready Date & Time");

		fieldsOfObject.put(24, "status");
		excelColumnNames.put(24, "Status");

		fieldsOfObject.put(25, "billNo");
		excelColumnNames.put(25, "Bill No.");

		fieldsOfObject.put(26, "labAmt");
		excelColumnNames.put(26, "Lab Amt");

		fieldsOfObject.put(27, "partsAmt");
		excelColumnNames.put(27, "Parts Amt");

		fieldsOfObject.put(28, "pickupRequired");
		excelColumnNames.put(28, "Pickup Required");

		fieldsOfObject.put(29, "pickupDate");
		excelColumnNames.put(29, "Pickup Date");

		fieldsOfObject.put(30, "pickupLoc");
		excelColumnNames.put(30, "Pickup Location");

		fieldsOfObject.put(31, "billDate");
		excelColumnNames.put(31, "Bill Date");

		fieldsOfObject.put(32, "billAmt");
		excelColumnNames.put(32, "Bill Amount");

		fieldsOfObject.put(33, "address1");
		excelColumnNames.put(33, "Address1");

		fieldsOfObject.put(34, "address2");
		excelColumnNames.put(34, "Address2");

		fieldsOfObject.put(35, "address3");
		excelColumnNames.put(35, "Address3");

		fieldsOfObject.put(36, "city");
		excelColumnNames.put(36, "City");

		fieldsOfObject.put(37, "pincode");
		excelColumnNames.put(37, "PIN");

		fieldsOfObject.put(38, "dob");
		excelColumnNames.put(38, "DOB");

		fieldsOfObject.put(39, "doa");
		excelColumnNames.put(39, "DOA");

		fieldsOfObject.put(40, "engineNo");
		excelColumnNames.put(40, "Engine No");

		fieldsOfObject.put(41, "variant");
		excelColumnNames.put(41, "Variant");

		fieldsOfObject.put(42, "color");
		excelColumnNames.put(42, "Color");

		fieldsOfObject.put(43, "mobile");
		excelColumnNames.put(43, "Mobile No.");

		fieldsOfObject.put(44, "year");
		excelColumnNames.put(44, "Year");

		ArrayList<HashMap> returnData = new ArrayList<HashMap>();
		returnData.add(fieldsOfObject);
		returnData.add(excelColumnNames);

		return returnData;

	}

	private ArrayList<HashMap> getWorkBillMappingData() {

		HashMap<Integer, String> fieldsOfObject = new HashMap<Integer, String>();
		HashMap<Integer, String> excelColumnNames = new HashMap<Integer, String>();

		fieldsOfObject.put(0, "slNo");
		excelColumnNames.put(0, "Srl.");

		fieldsOfObject.put(1, "billNo");
		excelColumnNames.put(1, "Bill No.");

		fieldsOfObject.put(2, "mcpNo");
		excelColumnNames.put(2, "MCP No.");

		fieldsOfObject.put(3, "billDate");
		excelColumnNames.put(3, "Bill Date");

		fieldsOfObject.put(4, "jobCardNo");
		excelColumnNames.put(4, "Job Card No.");

		fieldsOfObject.put(5, "servicetype");
		excelColumnNames.put(5, "Service Type");

		fieldsOfObject.put(6, "regno");
		excelColumnNames.put(6, "Registration No.");

		fieldsOfObject.put(7, "custId");
		excelColumnNames.put(7, "Customer ID");

		fieldsOfObject.put(8, "partyName");
		excelColumnNames.put(8, "Party Name");

		fieldsOfObject.put(9, "partBasic");
		excelColumnNames.put(9, "Part Basic");

		fieldsOfObject.put(10, "partDisc");
		excelColumnNames.put(10, "Part Discount");

		fieldsOfObject.put(11, "partCharges");
		excelColumnNames.put(11, "Part Charges");

		fieldsOfObject.put(12, "labourBasic");
		excelColumnNames.put(12, "Labour Basic");

		fieldsOfObject.put(13, "labourDisc");
		excelColumnNames.put(13, "Labour Discount");

		fieldsOfObject.put(14, "labourCharge");
		excelColumnNames.put(14, "Labour Charges");

		fieldsOfObject.put(15, "roundOffAmt");
		excelColumnNames.put(15, "Round off Amount");

		fieldsOfObject.put(16, "billAmt");
		excelColumnNames.put(16, "Bill Amount");

		fieldsOfObject.put(17, "location");
		excelColumnNames.put(17, "Location");

		fieldsOfObject.put(18, "year");
		excelColumnNames.put(18, "Year");

		ArrayList<HashMap> returnData = new ArrayList<HashMap>();

		returnData.add(fieldsOfObject);
		returnData.add(excelColumnNames);

		return returnData;
	}

	private ArrayList<HashMap> getSMRMappingData() {
		HashMap<Integer, String> fieldsOfObject = new HashMap<Integer, String>();
		HashMap<Integer, String> excelColumnNames = new HashMap<Integer, String>();

		fieldsOfObject.put(0, "followUpType");
		excelColumnNames.put(0, "Follow-up Type");

		fieldsOfObject.put(1, "followUpNum");
		excelColumnNames.put(1, "Follow-up Number");

		fieldsOfObject.put(2, "followUpNew");
		excelColumnNames.put(2, "Follow up Date");

		fieldsOfObject.put(3, "followUpOld");
		excelColumnNames.put(3, "Follow up");

		fieldsOfObject.put(4, "regNo");
		excelColumnNames.put(4, "Regn. No.");

		fieldsOfObject.put(5, "model");
		excelColumnNames.put(5, "Model Name");

		fieldsOfObject.put(6, "fuelType");
		excelColumnNames.put(6, "Fuel Type");

		fieldsOfObject.put(7, "chassis");
		excelColumnNames.put(7, "Chassis No.");

		fieldsOfObject.put(8, "mileage");
		excelColumnNames.put(8, "Mileage");

		fieldsOfObject.put(9, "dueDate");
		excelColumnNames.put(9, "Due Date");

		fieldsOfObject.put(10, "dueService");
		excelColumnNames.put(10, "Due Service");

		fieldsOfObject.put(11, "lastServiceType");
		excelColumnNames.put(11, "Last Service");

		fieldsOfObject.put(12, "lastServiceDate");
		excelColumnNames.put(12, "Last Service Date");

		fieldsOfObject.put(13, "saleDate");
		excelColumnNames.put(13, "Sale Date");

		fieldsOfObject.put(14, "custCat");
		excelColumnNames.put(14, "Cust. Catg.");

		fieldsOfObject.put(15, "custName");
		excelColumnNames.put(15, "Customer Name");

		fieldsOfObject.put(16, "address");
		excelColumnNames.put(16, "Address");

		fieldsOfObject.put(17, "telephone");
		excelColumnNames.put(17, "Telephone No.");

		fieldsOfObject.put(18, "mobile");
		excelColumnNames.put(18, "Mobile No.");

		fieldsOfObject.put(19, "custContactStatus");
		excelColumnNames.put(19, "Customer Contact Status");

		fieldsOfObject.put(20, "currentJC");
		excelColumnNames.put(20, "Current JC");

		fieldsOfObject.put(21, "serviceType");
		excelColumnNames.put(21, "Service Type");

		fieldsOfObject.put(22, "Status");
		excelColumnNames.put(22, "Status");

		fieldsOfObject.put(23, "nextFollowupDate");
		excelColumnNames.put(23, "Next Followup Date");

		fieldsOfObject.put(24, "custPickUpType");
		excelColumnNames.put(24, "Customer Pick-Up Type");

		fieldsOfObject.put(25, "custEmailIdStatus");
		excelColumnNames.put(25, "Customer Email id Status");

		fieldsOfObject.put(26, "lastfollowUpRemarks");
		excelColumnNames.put(26, "Last Followup Remarks");

		fieldsOfObject.put(27, "followUpRemarks");
		excelColumnNames.put(27, "Followup Remarks");

		fieldsOfObject.put(28, "followUpResponse");
		excelColumnNames.put(28, "Followup Response");

		fieldsOfObject.put(29, "deliveryDate");
		excelColumnNames.put(29, "Delivery Date (Sale)");

		fieldsOfObject.put(30, "pickupDrop");
		excelColumnNames.put(30, "Pickup/Drop");

		fieldsOfObject.put(31, "extWarranty");
		excelColumnNames.put(31, "Ext. Warranty");

		ArrayList<HashMap> returnData = new ArrayList<HashMap>();

		returnData.add(fieldsOfObject);
		returnData.add(excelColumnNames);

		return returnData;
	}

	private ArrayList<HashMap> getCampaignMappingData() {
		HashMap<Integer, String> fieldsOfObject = new HashMap<Integer, String>();
		HashMap<Integer, String> excelColumnNames = new HashMap<Integer, String>();

		fieldsOfObject.put(0, "source");
		excelColumnNames.put(0, "SOURCE");

		fieldsOfObject.put(1, "veh_reg_no");
		excelColumnNames.put(1, "Vehicle Reg No");

		fieldsOfObject.put(2, "customerName");
		excelColumnNames.put(2, "Customer Name");

		fieldsOfObject.put(3, "vehicle_types");
		excelColumnNames.put(3, "VEHICLE TYPE");

		fieldsOfObject.put(4, "varaintDesc");
		excelColumnNames.put(4, "Variant DESC");

		fieldsOfObject.put(5, "colorDesc");
		excelColumnNames.put(5, "Color DESC");

		fieldsOfObject.put(6, "chassis");
		excelColumnNames.put(6, "Chassis");

		fieldsOfObject.put(7, "engine");
		excelColumnNames.put(7, "Engine");

		fieldsOfObject.put(8, "salutation");
		excelColumnNames.put(8, "Salutation");

		fieldsOfObject.put(9, "address1");
		excelColumnNames.put(9, "ADDRESS1");

		fieldsOfObject.put(10, "address2");
		excelColumnNames.put(10, "ADDRESS2");

		fieldsOfObject.put(11, "address3");
		excelColumnNames.put(11, "ADDRESS3");

		fieldsOfObject.put(12, "city");
		excelColumnNames.put(12, "City");

		fieldsOfObject.put(13, "pincode");
		excelColumnNames.put(13, "Pincode");

		fieldsOfObject.put(14, "mobile1");
		excelColumnNames.put(14, "Mobile1");

		fieldsOfObject.put(15, "mobile2");
		excelColumnNames.put(15, "Mobile2");

		fieldsOfObject.put(16, "std_code");
		excelColumnNames.put(16, "STD Code");

		fieldsOfObject.put(17, "land_line");
		excelColumnNames.put(17, "LANDLINE");

		fieldsOfObject.put(18, "email_id");
		excelColumnNames.put(18, "E-mail Id");

		ArrayList<HashMap> returnData = new ArrayList<HashMap>();
		returnData.add(fieldsOfObject);
		returnData.add(excelColumnNames);

		return returnData;
	}
	
	private ArrayList<HashMap> getInsuranceMapingData() {
		
		
		
		HashMap<Integer, String> fieldsOfObject = new HashMap<Integer, String>();
		HashMap<Integer, String> excelColumnNames = new HashMap<Integer, String>();

		fieldsOfObject.put(0, "customerName");
		excelColumnNames.put(0, "CUSTOMER NAME");
		
		fieldsOfObject.put(1, "contactNo");
		excelColumnNames.put(1, "CONTACT No.");
		
		fieldsOfObject.put(2, "email");
		excelColumnNames.put(2, "EMAIL");
		
		fieldsOfObject.put(3, "doa");
		excelColumnNames.put(3, "DOA");
		
		fieldsOfObject.put(4, "dob");
		excelColumnNames.put(4, "DOB");
		
		fieldsOfObject.put(5, "address");
		excelColumnNames.put(5, "ADDRESS");
		
		fieldsOfObject.put(6, "city");
		excelColumnNames.put(6, "CITY");
		
		fieldsOfObject.put(7, "state");
		excelColumnNames.put(7, "STATE");
		
		fieldsOfObject.put(8, "vehicleRegNo");
		excelColumnNames.put(8, "REGISTRATION No.");
		
		fieldsOfObject.put(9, "saleDate");
		excelColumnNames.put(9, "SALE DATE");
		
		fieldsOfObject.put(10, "engineNo");
		excelColumnNames.put(10, "ENGINE No.");
		
		fieldsOfObject.put(11, "chassisNo");
		excelColumnNames.put(11, "CHASSIS No.");
		
		fieldsOfObject.put(12, "model");
		excelColumnNames.put(12, "MODEL");
		
		fieldsOfObject.put(13, "variant");
		excelColumnNames.put(13, "VARIANT");
		
		fieldsOfObject.put(14, "color");
		excelColumnNames.put(14, "COLOR");
		
		fieldsOfObject.put(15, "lastMileage");
		excelColumnNames.put(15, "LAST MILEAGE");
		
		fieldsOfObject.put(16, "policyNo");
		excelColumnNames.put(16, "POLICY No.");
		
		fieldsOfObject.put(17, "coveragePeriod");
		excelColumnNames.put(17, "COVERAGE PERIOD");
		
		fieldsOfObject.put(18, "policyDueDate");
		excelColumnNames.put(18, "POLICY DUE DATE");
		
		fieldsOfObject.put(19, "insuranceCompanyName");
		excelColumnNames.put(19, "INSURANCE COMPANY");
		
		fieldsOfObject.put(20, "idv");
		excelColumnNames.put(20, "IDV");
		
		fieldsOfObject.put(21, "odPercentage");
		excelColumnNames.put(21, "OD %");
		
		fieldsOfObject.put(22, "odAmount");
		excelColumnNames.put(22, "OD (Rs.)");
		
		fieldsOfObject.put(23, "ncBPercentage");
		excelColumnNames.put(23, "NCB %");
		
		fieldsOfObject.put(24, "ncBAmount");
		excelColumnNames.put(24, "NCB (Rs.)");
		
		fieldsOfObject.put(25, "discountPercentage");
		excelColumnNames.put(25, "DISCOUNT%");
		
		fieldsOfObject.put(26, "ODpremium");
		excelColumnNames.put(26, "OD PREMIUM");
		
		fieldsOfObject.put(27, "liabilityPremium");
		excelColumnNames.put(27, "LIABILITY PREMIUM");
		
		fieldsOfObject.put(28, "add_ON_Premium");
		excelColumnNames.put(28, "ADD-ON PREMIUM");
		
		fieldsOfObject.put(29, "premiumAmountBeforeTax");
		excelColumnNames.put(29, "PREMIUM (Before Tax)");
		
		fieldsOfObject.put(30, "serviceTax");
		excelColumnNames.put(30, "TAX %");
		
		fieldsOfObject.put(31, "premiumAmountAfterTax");
		excelColumnNames.put(31, "PREMIUM (After Tax)");
		
		fieldsOfObject.put(32, "lastRenewalBy");
		excelColumnNames.put(32, "LAST RENEWAL BY");
		
		fieldsOfObject.put(33, "renewalType");
		excelColumnNames.put(33, "NEXT RENEWAL TYPE");
		
		
		ArrayList<HashMap> returnData = new ArrayList<HashMap>();
		returnData.add(fieldsOfObject);
		returnData.add(excelColumnNames);

		return returnData;
	}
	
	private void assignInsuranceDataByProcedure(String uploadId) {
		// TODO Auto-generated method stub
		
		PreparedStatement cs = null;		
		Connection connection = null;
		try {
		DataSource datasource = DB.getDataSource();
		connection = datasource.getConnection();
		cs= connection.prepareStatement("call insert_insurance_calls(?);");
		cs.setString(1, uploadId);
		cs.executeQuery();
		
   		
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			}
		finally
		 {
			 try {
				 if (cs != null) {
					 cs.close();
			 		}
			 	}
			 catch (Exception e) {
			 	};
			 try {
				 if (connection != null) {
					 if (!connection.isClosed()) {
						 connection.close();
					 }
				 	}
			 	}
			 catch (Exception e) {
			 };
		}
		
	

	}
	
	//Insert the Insurance data to respective tables
	public void startInsuranceDetailsSync(String uploadDataId) {

		boolean startsyncState = false;
		if (scheduler != null) {
			if (scheduler.isCancelled()) {
				startsyncState = true;
			} else {
				startsyncState = false;
			}
		} else {
			startsyncState = true;
		}

		if (startsyncState) {
			
			logger.info("Sync of startInsuranceDetailsSync insurance");

			ActorRef actor = Akka.system().actorOf(Props.create(InsuranceDataInsert.class));
			scheduler = Akka.
					system().
					scheduler().
					schedule(Duration.create(0, TimeUnit.MILLISECONDS),
					
					Duration.create(5, TimeUnit.MILLISECONDS), 
																														// milliseconds
					actor,
					new InsuranceDataInsertProtocol.StartSync(
							uploadDataId, exlRepo),
					Akka.system().dispatcher(), null);
			
			if (scheduler != null) {
				if (!scheduler.isCancelled()) {					
					logger.info("start Insurance Details Sync");
					scheduler.cancel();
				}
			}
		}

		

	}

}
