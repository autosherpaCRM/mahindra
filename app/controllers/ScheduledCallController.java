package controllers;


import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;
import java.sql.Array;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.fasterxml.jackson.databind.JsonNode;

import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;
//import static play.mvc.Http.Response;
import play.api.mvc.AnyContent;
import play.api.mvc.Action;
import play.mvc.*;
import play.*;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Http.RequestBody;

import org.springframework.http.converter.*;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import play.data.Form;
import static play.libs.Json.toJson;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.ws.Response;

import models.Dealer;
import models.ListingForm;
import models.MessageTemplete;
import models.WyzUser;
import models.ServiceDoneSchCall;
import models.ServiceTypes;
import models.CallInteraction;
import models.Campaign;
import models.ServiceAdvisor;
import models.ServiceBooked;
import models.Vehicle;
import models.Workshop;
import models.Customer;
import models.Driver;
import models.SpecialOfferMaster;



import views.html.callDispositionFormServiceDivision;

import org.pac4j.play.java.Secure;

import org.pac4j.core.config.Config;
import org.pac4j.core.profile.CommonProfile;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;
import org.supercsv.io.*;

import actors.FireBaseCallLogSync;
import actors.FireBaseCallLogSyncProtocol;
import actors.FireBaseReportDetailsSync;
import actors.FireBaseReportDetailsSyncProtocol;

import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.Props;
import jdk.nashorn.internal.ir.RuntimeNode.Request;
import play.Logger.ALogger;
import play.api.Play;
import play.libs.Akka;
import play.mvc.Http;
import play.mvc.Result;

import java.io.*;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.FirebaseDatabase;

import controllers.webmodels.BookedLineChartData;
import models.Address;
import models.AssignedInteraction;
import models.CallDispositionData;
import models.Complaint;
import models.Insurance;
import models.Location;
import models.Phone;
import models.PickupDrop;
import models.SMSTemplate;
import models.SRDisposition;
import models.Service;
import models.ServiceBooked;
import models.UpsellLead;
import org.hibernate.Hibernate;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.DealerRepository;
import repositories.ScheduledCallRepository;
import repositories.SearchRepository;
import repositories.WyzUserRepository;
import scala.concurrent.duration.Duration;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;

@Controller
public class ScheduledCallController extends play.mvc.Controller {

	private final ScheduledCallRepository repo;
	private final WyzUserRepository repoWyzUser;
	private final DealerRepository repoDealer;
	private final CallInfoRepository repoCallinfo;
	private final CallInteractionsRepository call_int_repo;
	public final SearchRepository searchRepo;
	private PlaySessionStore playSessionStore;


	ALogger logger = play.Logger.of("application");

	private Config configuration;

	private Cancellable scheduler = null;
	private Cancellable serviceBookedSchedular = null;
	private Cancellable saveScheduledCallInDB = null;

	private Cancellable callsMadeScheduler = null;

	private Cancellable serviceDueSMSScheduler = null;

	@Inject
	public ScheduledCallController(ScheduledCallRepository repository, Config config,
			WyzUserRepository repositoryWyzUser, DealerRepository repositoryDealer,
			CallInfoRepository repositoryCallinfo, CallInteractionsRepository interRepo, SearchRepository search_Repo,
			PlaySessionStore playsession) {
		repo = repository;
		repoWyzUser = repositoryWyzUser;
		repoDealer = repositoryDealer;
		repoCallinfo = repositoryCallinfo;
		call_int_repo = interRepo;
		searchRepo = search_Repo;
		configuration = config;
		playSessionStore=playsession;
		
	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	
	private String getTimeZoneOfDealerCode() {

		String time_zone = (String) getUserProfile().getAttribute("TIME_ZONE");

		return time_zone;

	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	// testing or deleteing child

	public Result startSyncForTesting() {

		DatabaseReference callinfoRef = FirebaseDatabase.getInstance()
				.getReferenceFromUrl("https://wyzcrm-2feff.firebaseio.com/ATULMOTORS/CRE/shri_cre/WebHistory/CallInfo");
		callinfoRef.removeValue();

		return ok("deleted the record");

	}

	// column defined

	private static CellProcessor[] getUserDataCellProcessor() {
		return new CellProcessor[] { new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional() };

	}

	private static CellProcessor[] getUserDataCellProcessorForDirectUpload() {
		return new CellProcessor[] { new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional() };

	}

	private static CellProcessor[] getDataCellProcessorForServiceBooked() {
		return new CellProcessor[] { new Optional(), new Optional(), new Optional(), new Optional() };

	}

	// Ajax Call data

	private List<String> ajaxDataForDashboardOfCREMan() {
		/*
		 * String user = getUserLoginName();
		 * 
		 * long count = repo
		 * .getScheduledCallCountByCREMan(user,getUserLogindealerCode());
		 * 
		 * long count1 = repo
		 * .getScheduledCallsPendingCountCREMan(user,getUserLogindealerCode());
		 * 
		 * long count2 = repo
		 * .getServiceBookedCountCreMan(user,getUserLogindealerCode());
		 * 
		 * double count3 = repo
		 * .getConversionPercentageCreMan(user,getUserLogindealerCode());
		 * 
		 * String pattern = "###.##";
		 * 
		 * DecimalFormat formatter = new DecimalFormat(pattern);
		 * 
		 * String displayPercentage = formatter.format(count3);
		 * 
		 * ArrayList<String> ajaxData = new ArrayList<String>();
		 * ajaxData.add(Long.toString(count));
		 * ajaxData.add(Long.toString(count1));
		 * ajaxData.add(Long.toString(count2)); ajaxData.add(displayPercentage);
		 */ArrayList ajaxData = new ArrayList();
		return ajaxData;
	}

	private List<String> ajaxDataForDashboardOfCRE() {

		String userName = getUserLoginName();

		long countOfSchCallOfCRE = repo.getScheduledCallCountByOfCRE(userName, getUserLogindealerCode());
		long count = repo.getScheduledCallsPendingCountForCRE(userName, getUserLogindealerCode());
		long count1 = repo.getServiceBookedCountForCRE(userName, getUserLogindealerCode());

		double count2 = repo.getConversionPercentageOfCRE(userName, getUserLogindealerCode());

		String pattern = "###.##";

		DecimalFormat formatter = new DecimalFormat(pattern);

		String displayPercentage = formatter.format(count2);

		long countOFFollowUp = repoCallinfo.getCountOfFollowUpsTobeDoneToday(userName, getUserLogindealerCode());

		ArrayList<String> ajaxData = new ArrayList<String>();
		ajaxData.add(Long.toString(countOfSchCallOfCRE));
		ajaxData.add(Long.toString(count));
		ajaxData.add(Long.toString(count1));
		ajaxData.add(displayPercentage);
		ajaxData.add(Long.toString(countOFFollowUp));

		return ajaxData;
	}

	// Listing Scheduled calls by CRE

	private boolean checkCREManagerRole() {

		String role = (String) getUserProfile().getAttribute("USER_ROLE");
		if ("CREManager".equals(role))
			return true;
		else
			return false;
	}

	

	@Secure(clients = "FormClient")
	public Result getCallDispositionTabPAgeCRE() {
		String userName = getUserProfile().getId();
		String dealername = getDealerName();

		String toActivateTab1 = "in active";
		String toActivateTab2 = "";
		String toActivateTab3 = "";
		String toActivateTab4 = "";
		String toActivateTab5 = "";
		List<Campaign> campaignList=new ArrayList<Campaign>();
		List<Campaign> campaignListCampign = call_int_repo.getCampaignNamesBYType("Campaign");
		List<Campaign> campaignListremainder = call_int_repo.getCampaignNamesBYType("Service Reminder");
		campaignList.addAll(campaignListCampign);
		campaignList.addAll(campaignListremainder);
				
		List<ServiceTypes> serviceTypeList = call_int_repo.getAllServiceTypeList();
		List<String> bookedServiceTypeList = searchRepo.getBookedServiceType();
		//List<CallDispositionData> dispoList = repoWyzUser.getDispositionList();
		List<CallDispositionData> newlistDispo = repoWyzUser.getDispositionListOfNoncontacts();
		
		return ok(callDispositionFormServiceDivision.render(toActivateTab1, toActivateTab2, toActivateTab3,
				toActivateTab4, toActivateTab5, getUserLogindealerCode(), dealername, userName,getOEMOfDealer(),campaignList,serviceTypeList,bookedServiceTypeList,newlistDispo));


	}
	
	private String getOEMOfDealer(){
		
		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;
		
	}

}
