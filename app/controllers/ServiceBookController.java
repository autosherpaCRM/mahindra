/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import static play.libs.Json.toJson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import controllers.webmodels.CREListSAListDriverList;
import controllers.webmodels.DriverDataOnTabLoad;
import controllers.webmodels.PickupDropDataOnTabLoad;
import controllers.webmodels.ServiceBookedReschdule;
import controllers.webmodels.ServiceBookedResultList;
import models.BookingDateTime;
import models.CallInteraction;
import models.Location;
import models.SRDisposition;
import models.ServiceBooked;
import models.ServiceTypes;
import models.Workshop;
import models.WyzUser;
import play.Logger.ALogger;
import play.libs.Json;
import play.mvc.Result;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.InsuranceRepository;
import repositories.ServiceBookRepository;
import repositories.WyzUserRepository;
import views.html.bookingReview;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;

import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.xssf.*;

import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

@Controller
public class ServiceBookController extends play.mvc.Controller {

	ALogger logger = play.Logger.of("application");

	private final CallInfoRepository repo;
	private final CallInteractionsRepository call_int_repo;
	private final ServiceBookRepository servicebookedrepository;
	private final InsuranceRepository insurRepo;
	private PlaySessionStore playSessionStore;
	private final WyzUserRepository wyzRepo;

	@Inject
	public ServiceBookController(CallInfoRepository repository, org.pac4j.core.config.Config config,
			CallInteractionsRepository interRepo, ServiceBookRepository serviceBookRepo,
			InsuranceRepository insurRepository, PlaySessionStore plstore, WyzUserRepository wyzRepository) {
		repo = repository;
		call_int_repo = interRepo;
		servicebookedrepository = serviceBookRepo;
		config = (org.pac4j.core.config.Config) config;
		insurRepo = insurRepository;
		playSessionStore = plstore;
		wyzRepo = wyzRepository;

	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	private String getOEMOfDealer() {

		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;

	}

	@Secure(clients = "FormClient")
	public Result getReviewBookingPage() {
		List<String> cresList = repo.getCREUserNameofCREManager(getUserLoginName(), getUserLogindealerCode());
		List<Location> locationList = call_int_repo.getLocationList();
		List<Workshop> workshopList = call_int_repo.getWorkshopList();
		List<ServiceTypes> servicetypelist = call_int_repo.getAllServiceTypeList();

		List<String> statesList = insurRepo.getAllDistinctStates();

		Collections.sort(statesList);

		return ok(bookingReview.render(statesList, getUserLogindealerCode(), getUserLoginName(), cresList,
				servicetypelist, locationList, workshopList));

	}

	@Secure(clients = "FormClient")
	public Result getServiceAdvisorListByWorkshop(long workshopid) {

		List<WyzUser> SAList = servicebookedrepository.getUserByWorkshopAndRole(workshopid, "Service Advisor");
		List<WyzUser> DriverList = servicebookedrepository.getUserByWorkshopAndRole(workshopid, "Driver");
		List<WyzUser> CREList = servicebookedrepository.getUserByWorkshopAndRole(workshopid, "CRE");
		for (WyzUser sa : SAList) {

			long servId = servicebookedrepository.getServiceAdvisorByuserID(sa.getId());
			sa.setId(servId);
			sa.setWorkshopList(null);
			sa.setAssignedInteractions(null);
			sa.setAssignedInteraction(null);
			sa.setAvailabilities(null);
			sa.setCallInteractions(null);
			sa.setCallInteraction(null);
			sa.setComplaint(null);
			sa.setComplaints(null);
			sa.setInsuranceAssignedInteraction(null);
			sa.setLocation(null);
			sa.setWorkshop(null);
			sa.setLocationList(null);
			sa.setRoles(null);
			sa.setTaggingusers(null);
			sa.setUnAvailabilities(null);
			sa.setPsfAssignedInteraction(null);
			sa.setSentSMSes(null);
			sa.setTenant(null);
			sa.setServiceBooked(null);
			sa.setDealer(null);
		}

		for (WyzUser dl : DriverList) {

			long driverid = servicebookedrepository.getDriverByUserId(dl.getId());

			dl.setWorkshopList(null);
			dl.setAssignedInteractions(null);
			dl.setAssignedInteraction(null);
			dl.setAvailabilities(null);
			dl.setCallInteractions(null);
			dl.setCallInteraction(null);
			dl.setComplaint(null);
			dl.setComplaints(null);
			dl.setInsuranceAssignedInteraction(null);
			dl.setLocation(null);
			dl.setWorkshop(null);
			dl.setLocationList(null);
			dl.setRoles(null);
			dl.setTaggingusers(null);
			dl.setUnAvailabilities(null);
			dl.setPsfAssignedInteraction(null);
			dl.setSentSMSes(null);
			dl.setTenant(null);
			dl.setServiceBooked(null);
			dl.setDealer(null);
		}

		for (WyzUser cre : CREList) {
			cre.setWorkshopList(null);
			cre.setAssignedInteractions(null);
			cre.setAssignedInteraction(null);
			cre.setAvailabilities(null);
			cre.setCallInteractions(null);
			cre.setCallInteraction(null);
			cre.setComplaint(null);
			cre.setComplaints(null);
			cre.setInsuranceAssignedInteraction(null);
			cre.setLocation(null);
			cre.setWorkshop(null);
			cre.setLocationList(null);
			cre.setRoles(null);
			cre.setTaggingusers(null);
			cre.setUnAvailabilities(null);
			cre.setPsfAssignedInteraction(null);
			cre.setSentSMSes(null);
			cre.setTenant(null);
			cre.setServiceBooked(null);
			cre.setDealer(null);
		}

		CREListSAListDriverList userList = new CREListSAListDriverList();
		userList.setSAList(SAList);
		userList.setDriverList(DriverList);
		userList.setCREList(CREList);
		return ok(toJson(userList));

	}

	@Secure(clients = "FormClient")
	public Result getsearchServiceBoookedList() {

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String searchCity = "";
		String searchWorkshop = "";
		String searchCRE = "";
		String searchBookingStatus = "";
		String searchSA = "";
		String searchDriver = "";
		String searchDateTo = "";
		String searchDateFrom = "";

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
			logger.info(" searchPattern paramMap : " + searchPattern);
		}

		if (paramMap.get("columns[0][search][value]") != null) {
			searchCity = paramMap.get("columns[0][search][value]")[0];
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			searchWorkshop = paramMap.get("columns[1][search][value]")[0];
			if (searchWorkshop.equals("select")) {
				searchWorkshop = "";
			}
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			searchCRE = paramMap.get("columns[2][search][value]")[0];
			if (searchCRE.equals("0")) {
				searchCRE = "";
			}
		}

		if (paramMap.get("columns[3][search][value]") != null) {
			searchSA = paramMap.get("columns[3][search][value]")[0];
			if (searchSA.equals("0")) {
				searchSA = "";
			}
		}
		if (paramMap.get("columns[4][search][value]") != null) {
			searchDriver = paramMap.get("columns[4][search][value]")[0];
			if (searchDriver.equals("0")) {
				searchDriver = "";
			}
		}
		if (paramMap.get("columns[5][search][value]") != null) {
			searchBookingStatus = paramMap.get("columns[5][search][value]")[0];
			if (searchBookingStatus.equals("All")) {
				searchBookingStatus = "";

			}
		}
		if (paramMap.get("columns[6][search][value]") != null) {
			searchDateFrom = paramMap.get("columns[6][search][value]")[0];
		}
		if (paramMap.get("columns[7][search][value]") != null) {
			searchDateTo = paramMap.get("columns[7][search][value]")[0];
		}

		logger.info("##############Search parameters##############");
		logger.info("searchPattern : " + searchPattern);
		logger.info("searchCity: " + searchCity);
		logger.info("searchWorkshop: " + searchWorkshop);
		logger.info("searchCRE: " + searchCRE);
		logger.info("searchDateFrom From: " + searchDateFrom);
		logger.info("searchDateTo To: " + searchDateTo);
		logger.info("searchBookingStatus: " + searchBookingStatus);
		logger.info("searchDriver: " + searchDriver);
		logger.info("searchSA: " + searchSA);

		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && searchCity.length() == 0 && searchWorkshop.length() == 0
				&& searchCRE.length() == 0 && searchSA.length() == 0 && searchDriver.length() == 0
				&& searchDateTo.length() == 0 && searchDateFrom.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}
		logger.info("searchPattern :" + searchPattern);
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = servicebookedrepository.getSBCount(getUserLoginName());
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;
		}

		if (toIndex > totalSize) {
			toIndex = totalSize;
		}
		long patternCount = 0;

		List<ServiceBookedResultList> serviceBookedList = servicebookedrepository.AllServiceBookedRecords(
				getUserLoginName(), searchBookingStatus, searchDateFrom, searchDateTo, searchCRE, searchDriver,
				searchSA, searchWorkshop, searchPattern, fromIndex, toIndex);

		if (!allflag) {
			if (globalSearch)
				// patternCount =
				// servicebookedrepository.getSBCount(getUserLoginName());

				patternCount = servicebookedrepository.getAllServiceBookedDataCount(getUserLoginName(),
						searchBookingStatus, searchDateFrom, searchDateTo, searchCRE, searchDriver, searchSA,
						searchWorkshop, searchPattern);
			else
				patternCount = servicebookedrepository.getAllServiceBookedDataCount(getUserLoginName(),
						searchBookingStatus, searchDateFrom, searchDateTo, searchCRE, searchDriver, searchSA,
						searchWorkshop, searchPattern);
		}
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

		logger.info("allFlag : " + allflag + " global : " + globalSearch);

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (ServiceBookedResultList s : serviceBookedList) {
			// String reschedule = "<a href=''><i class=\"fa fa-pencil-square\"
			// data-toggel=\"tooltip\" title=\"Disposition\"
			// style=\"font-size:30px;color:#DD4B39;\"></i></a>";

			long schcallId = s.getServiceBookedId();
			String reschedule = "<a class=\"fa fa-edit fa-2x\" data-toggle=\"modal\" data-target=\"#bookingReviewPopup\" data-backdrop=\"static\" data-keyboard=\"false\" onclick=\"resheduleBookingService("
					+ schcallId + ")\"></a>";
			String cancelBooking = "<a class=\"fa fa-close fa-2x\" onclick=\"cancelBookingPickup(" + schcallId
					+ ")\"></a>";
			// String cancelBooking = "<a href=''><i class=\"fa
			// fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\"
			// style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			ObjectNode row = Json.newObject();

			row.put("0", s.getStatus());
			row.put("1", String.valueOf(s.getBooking_reg_date()));
			row.put("2", String.valueOf(s.getBooked_date()));
			row.put("3", s.getWorkshop());
			row.put("4", s.getServiceBookedType());
			row.put("5", s.getCustomer_name());
			row.put("6", s.getMobile_number());
			row.put("7", s.getAddress());
			row.put("8", s.getVeh_model());
			row.put("9", s.getMode_of_visit());
			row.put("10", s.getPickup_time());
			row.put("11", s.getPickup_address());
			row.put("12", s.getDriver_name());
			row.put("13", s.getCRE_name());
			row.put("14", s.getAdvisor_name());

			if (s.getStatus().equals("Cancelled") || s.getStatus().equals("Closed")) {
				row.put("15", "");
				row.put("16", "");

			} else {

				row.put("15", reschedule);
				row.put("16", cancelBooking);

			}

			an.add(row);
		}

		return ok(result);

	}

	// CANCEL BOOKING METHODS
	@Secure(clients = "FormClient")
	public Result cancelBookingOrPickup(long servicebookedId, long cancelId) {

		String status = "";

		if (cancelId == 2) {

			status = servicebookedrepository.cancelPickupOfBooking(servicebookedId);

		} else {

			status = servicebookedrepository.cancelBooking(servicebookedId);
		}

		logger.info(" status service booked : " + status);

		return ok(toJson(status));

	}

	// REVIEW OR RESCHEDULE BOOKING
	@Secure(clients = "FormClient")
	public Result reviewScheduleBooking(long servicebookedId) {
		logger.info("servicebookedId : " + servicebookedId);

		ServiceBooked serviceBook = servicebookedrepository.getServiceBookDataByID(servicebookedId);

		CallInteraction calldispo = call_int_repo.getCallInteractionByBookingId(serviceBook.getServiceBookedId());

		ServiceBookedReschdule serviceReschedule = new ServiceBookedReschdule();
		serviceReschedule.setServiceBookIntId(serviceBook.getServiceBookedId());
		serviceReschedule.setServiceBookWorkshop(serviceBook.getWorkshop().getWorkshopName());
		serviceReschedule.setWorkshopId(serviceBook.getWorkshop().getId());
		serviceReschedule.setServiceBookedAddress(serviceBook.getServiceBookingAddress());
		serviceReschedule.setServiceBookedDate(serviceBook.getServiceScheduledDateStrSqlFor());
		serviceReschedule.setServiceBookTime(serviceBook.getServiceScheduledTimeStrTime());
		serviceReschedule.setTypeOfPickUp(serviceBook.getTypeOfPickup());
		serviceReschedule.setVehicleId(serviceBook.getVehicle().getVehicle_id());
		serviceReschedule.setVehicleRegNo(serviceBook.getVehicle().getVehicleRegNo());
		serviceReschedule.setServiceTypeName(serviceBook.getServiceBookedType());
		if (serviceBook.getServiceBookType() != null) {

			serviceReschedule.setServiceTypeId(serviceBook.getServiceBookType().getId());

		}
		serviceReschedule.setServiceAdvisorId(serviceBook.getServiceAdvisor().getAdvisorId());
		serviceReschedule.setServiceAdvisor(serviceBook.getServiceAdvisor().getAdvisorName());
		serviceReschedule.setSelectedCityName(serviceBook.getWorkshop().getLocation().getName());
		serviceReschedule.setRemarksOfService(calldispo.getSrdisposition().getRemarks());
		serviceReschedule.setCallIntId(calldispo.getId());
		if (serviceBook.getPickupDrop() != null) {
			logger.info("service book driver not equal to null");
			serviceReschedule.setDriverName(serviceBook.getPickupDrop().getDriver().getDriverName());
			serviceReschedule.setFromTime(serviceBook.getPickupDrop().getTimeFrom());
			serviceReschedule.setToTime(serviceBook.getPickupDrop().getTimeTo());
		}
		return ok(toJson(serviceReschedule));
	}

	@Secure(clients = "FormClient")
	public Result postRescheduleBooking() {

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());

		Map<String, String[]> parambody = request().body().asFormUrlEncoded();

		String remar = (parambody.get("remarks")[0]);
		String vehicleId = (parambody.get("vehicle_id")[0]);
		String vehicleReg = (parambody.get("vehicleReg")[0]);
		String scheduleDate = (parambody.get("scheduleDate")[0]);
		String scheduleTime = (parambody.get("scheduleTime")[0]);
		String address = (parambody.get("address")[0]);
		String serviceAdvisorId = (parambody.get("serviceAdvisorId")[0]);
		String workshopId = (parambody.get("workshopId")[0]);
		String serviceBookType = (parambody.get("serviceBookType")[0]);
		String city = (parambody.get("city")[0]);
		String typeOfPickup = (parambody.get("typeOfPickup")[0]);
		String call_int_id = (parambody.get("call_int_id")[0]);
		String driver_id = (parambody.get("driver_id")[0]);
		String startTimeId = (parambody.get("startTimeId")[0]);
		String endTimeId = (parambody.get("endTimeId")[0]);

		long call_id = Long.parseLong(call_int_id);

		logger.info(" remar is : " + remar + " vehicleId : " + vehicleId + " scheduleDate : " + scheduleDate
				+ " scheduleTime : " + scheduleTime + " address :" + address + " serviceAdvisorId : " + serviceAdvisorId
				+ " workshopId : " + workshopId + " serviceBookType :" + serviceBookType + " city :" + city
				+ " typeOfPickup :" + typeOfPickup + " vehicleReg : " + vehicleReg + " call_int_id : " + call_int_id
				+ " driver_id :" + driver_id + " startTimeId :" + startTimeId + " endTimeId :" + endTimeId);

		CallInteraction call_int = call_int_repo.getCallInteractionById(call_id);
		SRDisposition srdispo = servicebookedrepository.getSRDispositionByCallId(call_id);
		ServiceBooked servicebooked = servicebookedrepository.getServiceBookByCallId(call_id);

		servicebookedrepository.updateOldCallIntToReshedule(servicebooked.getServiceBookedId());

		SimpleDateFormat formatingData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String scheduledDateAndtime = scheduleDate + " " + scheduleTime + ":00";

		srdispo.setRemarks(srdispo.getRemarks() + " " + remar);

		try {
			Date datetime = formatingData.parse(scheduledDateAndtime);
			servicebooked.setScheduledDateTime(datetime);
		} catch (ParseException e) {
			// logger.info("Could not parse date");
			e.printStackTrace();
		}

		servicebooked.setServiceBookingAddress(address);
		servicebooked.setServiceBookedType(serviceBookType);
		servicebookedrepository.resheduleBooking(call_int, srdispo, servicebooked, serviceAdvisorId, workshopId,
				typeOfPickup, driver_id, startTimeId, endTimeId, userdata);

		return ok(toJson(""));
	}

	public Result driverListScheduleByWorkshopId(long workshopId, String scheduleDate) {

		long timeslotdifference = 30;

		logger.info("scheduleDate  : " + scheduleDate);

		List<WyzUser> DriverListData = servicebookedrepository.getUserByWorkshopAndRole(workshopId, "Driver");

		List<PickupDropDataOnTabLoad> DriverList = new ArrayList<PickupDropDataOnTabLoad>();

		List<BookingDateTime> timeSlot = servicebookedrepository.getTimeSlot();

		logger.info("DriverList size : " + DriverList.size());

		for (WyzUser dl : DriverListData) {
			PickupDropDataOnTabLoad pickup = new PickupDropDataOnTabLoad();
			pickup.setId(dl.getId());
			pickup.setUserName(dl.getUserName());

			List<List<Date>> pickupList = servicebookedrepository.getDriverScheduleByWyzUser(dl.getId(), scheduleDate);
			List<Date> fromlist = pickupList.get(0);
			List<Date> tolist = pickupList.get(1);
			List<Date> datesList = new ArrayList<Date>();
			for (int i = 0; i < pickupList.get(0).size(); i++) {

				long ONE_MINUTE_IN_MILLIS = 60000;
				long initailMin = fromlist.get(i).getTime();
				long diffMs = tolist.get(i).getTime() - fromlist.get(i).getTime();
				long diffSec = diffMs / 1000;
				long min = diffSec / 60;
				logger.info("difference min : " + min);
				if (min > timeslotdifference) {

					int multipletimes = (int) min / (int) timeslotdifference;
					datesList.add(fromlist.get(i));
					logger.info("fromlist.get(i) : " + fromlist.get(i));
					for (int j = 1; j < multipletimes; j++) {

						long afterAddingMins = initailMin + (timeslotdifference * ONE_MINUTE_IN_MILLIS);
						initailMin = afterAddingMins;
						Time time = new Time(afterAddingMins);
						logger.info("afterAddingMins : " + time);
						datesList.add(time);

					}
				} else {
					datesList.add(fromlist.get(i));
				}

			}

			pickup.setListTime(datesList);

			DriverList.add(pickup);

		}

		DriverDataOnTabLoad driverData = new DriverDataOnTabLoad();
		driverData.setDriverListData(DriverList);
		driverData.setTimeSlotData(timeSlot);

		return ok(toJson(driverData));
	}

	@Secure(clients = "FormClient")
	public Result downloadServiceBooking() throws IOException, IllegalArgumentException, IllegalAccessException {

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());

		String city = "";
		String workshop = "";
		String crenames = "";
		String saNames = "";
		String driverNames = "";
		String bookStatus = "";
		String startDate = "";
		String endDate = "";

		Map<String, String[]> parambody = request().body().asFormUrlEncoded();

		if ((parambody.get("city_id")) != null) {

			if ((parambody.get("city")[0]).equals("0")) {
				city = "";
			} else {
				city = (parambody.get("city")[0]);
			}
		}
		if ((parambody.get("workshop")) != null) {

			if ((parambody.get("workshop")[0]).equals("select")) {
				workshop = "";
			} else {
				workshop = (parambody.get("workshop")[0]);
			}
		}

		if ((parambody.get("userName")) != null) {
			crenames = (parambody.get("userName")[0]);
		}
		if ((parambody.get("selectSAID")) != null) {
			saNames = (parambody.get("selectSAID")[0]);
		}
		if ((parambody.get("selectDriverID")) != null) {
			driverNames = (parambody.get("selectDriverID")[0]);
		}
		if ((parambody.get("BOOKINGStatusId")) != null) {
			if ((parambody.get("BOOKINGStatusId")[0]).equals("0")) {
				bookStatus = "";
			} else if ((parambody.get("BOOKINGStatusId")[0]).equals("All")) {
				(parambody.get("BOOKINGStatusId")[0]) = "";

			} else {
				bookStatus = (parambody.get("BOOKINGStatusId")[0]);
			}

		}
		if ((parambody.get("StartDate")) != null) {
			startDate = (parambody.get("StartDate")[0]);
		}
		if ((parambody.get("EndDate")) != null) {
			endDate = (parambody.get("EndDate")[0]);
		}

		logger.info("city : " + city);
		logger.info("workshop : " + workshop);
		logger.info("crenames : " + crenames);
		logger.info("saNames : " + saNames);
		logger.info("driverNames : " + driverNames);
		logger.info("bookStatus : " + bookStatus);
		logger.info("startDate : " + startDate);
		logger.info("endDate : " + endDate);

		long totalcount = servicebookedrepository.getAllServiceBookedDataCount(getUserLoginName(), bookStatus,
				startDate, endDate, crenames, driverNames, saNames, workshop, "");

		logger.info("totalcount download : " + totalcount);

		List<ServiceBookedResultList> serviceBookedList = servicebookedrepository.AllServiceBookedRecords(
				getUserLoginName(), bookStatus, startDate, endDate, crenames, driverNames, saNames, workshop, "", 0,
				totalcount);

		logger.info("serviceBookedList download : " + serviceBookedList.size());

		Config configuration = ConfigFactory.load();
		String pathOfTemp = configuration.getString("app.configuration");

		String fileName = currentDate() + "_ServiceBooked_Report.xlsx";

		File fis = new File(pathOfTemp + fileName);

		FileOutputStream fileOut = new FileOutputStream(fis);
		XSSFWorkbook wb = new XSSFWorkbook();
		Sheet sheet = wb.createSheet("Sheet1");

		String[] headers = new String[] { "Status", "Booking Reg Date", "Service Booked Date", "Workshop",
				"Service Type", "Customer Name", "Customer Phone Number", "Address", "Model", "Mode of Visit",
				"Pick Up Time", "Driver Name", "Pick Up Address", "CRE Name", "SA Name" };

		int rowCount = 1;
		Row r = sheet.createRow(0);
		for (int rn = 0; rn < headers.length; rn++) {
			r.createCell(rn).setCellValue(headers[rn]);
		}

		for (ServiceBookedResultList aBook : serviceBookedList) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;

			for (Field field : aBook.getClass().getDeclaredFields()) {
				if (columnCount < headers.length) {
					Cell cell = row.createCell(columnCount++);
					cell.setCellValue(String.valueOf(field.get(aBook)));
					// logger.info("cell value : " + field.get(aBook));

				}
			}

		}
		wb.write(fileOut);
		fileOut.close();
		return ok(fis, fileName);
	}

	private java.sql.Date currentDate() {

		return new java.sql.Date(System.currentTimeMillis());
	}

}
