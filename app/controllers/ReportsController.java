package controllers;

import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;
import play.mvc.Controller;
import views.html.viewReportCRE;
import static play.libs.Json.toJson;
import play.data.Form;
import play.libs.Json;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import views.html.uploadDateWiseForm;
import javafx.scene.chart.PieChart;
import javax.inject.Inject;
import models.ListingForm;
import models.WyzUser;
import models.UploadReportFiles;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.LogoutController;
import org.pac4j.play.java.Secure;

import controllers.webmodels.BookedLineChartData;
import controllers.webmodels.CallInteractionHistory;

import controllers.webmodels.CallLogDispositionLoad;
import controllers.webmodels.PieChartCallTypeData;
import controllers.webmodels.ReportSummary;
import controllers.webmodels.InsuranceHistoryReport;

import play.Logger.ALogger;
import repositories.CallInfoRepository;
import repositories.ReportsRepository;
import repositories.ScheduledCallRepository;
import repositories.WyzUserRepository;
import views.html.viewReportCRE;
import views.html.uploadSummaryReportPage;
import play.mvc.Result;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.mvc.BodyParser;

public class ReportsController extends Controller {

	// CRE Manager functionalities

	ALogger logger = play.Logger.of("application");

	private LogoutController applicationLogoutController;

	private ScheduledCallRepository scheduledCallRepository;

	private CallInfoRepository callInfoRepository;

	private WyzUserRepository wyzUserRepository;
	private PlaySessionStore playSessionStore;
	private ReportsRepository reportsRepo;
	private WyzUserRepository wyzUser;


	@Inject
	public ReportsController(org.pac4j.core.config.Config config, ScheduledCallRepository schrepo,
			CallInfoRepository callrepo, LogoutController logoutCont, WyzUserRepository wyzUser,
			PlaySessionStore plstore,ReportsRepository reportsRepository) {
		config = (org.pac4j.core.config.Config) config;
		applicationLogoutController = logoutCont;
		scheduledCallRepository = schrepo;
		callInfoRepository = callrepo;
		wyzUserRepository = wyzUser;
		playSessionStore = plstore;
		reportsRepo=reportsRepository;
	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	public Result getScheduledCallsCountCREMan() {
		if (!checkCREManagerRole()) {
			applicationLogoutController.logout();
			return forbidden("Not Authorized");
		} else {
			String creMan = getUserProfile().getId();
			logger.info("getScheduledCallsCountCREMan");
			long count = scheduledCallRepository.getScheduledCallCountByCREMan(creMan, getUserLogindealerCode());
			return ok(Long.toString(count));
		}
	}

	public Result getBookedListByTime() {
		if (!checkCREManagerRole()) {
			applicationLogoutController.logout();
			return forbidden("Not Authorized");
		} else {
			String creMan = getUserProfile().getId();

			List<List> callCountList = callInfoRepository.getServiceBookedBetweenTimeCREManager(creMan,
					getUserLogindealerCode());

			BookedLineChartData data = new BookedLineChartData();
			data.setCallList(callCountList.get(0));
			data.setBookedList(callCountList.get(1));

			return ok(toJson(data));
		}
	}

	public Result getCallTypePie() {
		if (!checkCREManagerRole()) {
			applicationLogoutController.logout();
			return forbidden("Not Authorized");
		} else {
			String creMan = getUserProfile().getId();
			List<String> pieData = callInfoRepository.getCallTypePie(creMan, getUserLogindealerCode());
			PieChartCallTypeData returnData = new PieChartCallTypeData();
			returnData.setMissedCallCount(pieData.get(0));
			returnData.setOutgoingCallCount(pieData.get(1));
			returnData.setIncomingCallCount(pieData.get(2));

			return ok(toJson(returnData));
		}
	}

	public Result getScheduledCallsCountOfCRE() {
		String creUser = getUserLoginName();
		String dealerDB = getUserLogindealerCode();
		long countOfSchCallOfCRE = scheduledCallRepository.getScheduledCallCountByOfCRE(creUser, dealerDB);

		logger.info("getScheduledCallsCountCRE");
		return ok(Long.toString(countOfSchCallOfCRE));

	}

	public Result getScheduledCallsPendingCountForCRE() {

		String creUser = getUserLoginName();
		long count = scheduledCallRepository.getScheduledCallsPendingCountForCRE(creUser, getUserLogindealerCode());
		return ok(Long.toString(count));

	}

	public Result getServiceBookedForCRE() {

		String creUser = getUserLoginName();

		long count = scheduledCallRepository.getServiceBookedCountForCRE(creUser, getUserLogindealerCode());
		return ok(Long.toString(count));

	}

	public Result getServiceBookedPercentageForCRE() {

		String creUser = getUserLoginName();

		double count = scheduledCallRepository.getConversionPercentageOfCRE(creUser, getUserLogindealerCode());

		String pattern = "###.##";

		DecimalFormat formatter = new DecimalFormat(pattern);

		String displayPercentage = formatter.format(count);

		return ok(displayPercentage);

	}

	@Secure(clients = "FormClient")
	public Result getAllAjaxRequestForCRE() {

		if (!checkCRERole()) {
			applicationLogoutController.logout();
			return forbidden("Not Authorized");
		} else {
			String creUser = getUserLoginName();
			String dealerDB = getUserLogindealerCode();

			long countOfSchCallOfCRE = scheduledCallRepository.getScheduledCallCountByOfCRE(creUser, dealerDB);
			long count = scheduledCallRepository.getScheduledCallsPendingCountForCRE(creUser, getUserLogindealerCode());
			long count1 = scheduledCallRepository.getServiceBookedCountForCRE(creUser, getUserLogindealerCode());

			double count2 = scheduledCallRepository.getConversionPercentageOfCRE(creUser, getUserLogindealerCode());

			String pattern = "###.##";

			DecimalFormat formatter = new DecimalFormat(pattern);

			String displayPercentage = formatter.format(count2);

			long countOFFollowUp = callInfoRepository.getCountOfFollowUpsTobeDoneToday(creUser,
					getUserLogindealerCode());
			// logger.info("");
			ArrayList<String> ajaxData = new ArrayList<String>();
			ajaxData.add(Long.toString(countOfSchCallOfCRE));
			ajaxData.add(Long.toString(count));
			ajaxData.add(Long.toString(count1));
			ajaxData.add(displayPercentage);
			ajaxData.add(Long.toString(countOFFollowUp));
			return ok(toJson(ajaxData));

		}

	}

	public Result getCallTypePieForCRE() {

		if (!checkCRERole()) {
			applicationLogoutController.logout();
			return forbidden("Not Authorized");
		} else {
			String cre = getUserProfile().getId();
			List<String> pieData = callInfoRepository.getCallTypePieForCRE(cre, getUserLogindealerCode());
			PieChartCallTypeData returnData = new PieChartCallTypeData();
			returnData.setMissedCallCount(pieData.get(0));
			returnData.setOutgoingCallCount(pieData.get(1));
			returnData.setIncomingCallCount(pieData.get(2));

			return ok(toJson(returnData));
		}

	}

	public Result getBookedListByTimeForCRE() {

		if (!checkCRERole()) {
			applicationLogoutController.logout();
			return forbidden("Not Authorized");
		} else {
			String cre = getUserProfile().getId();

			List<List> callCountList = callInfoRepository.getServiceBookedBetweenTimeForCRE(cre,
					getUserLogindealerCode());

			BookedLineChartData data = new BookedLineChartData();
			data.setCallList(callCountList.get(0));
			data.setBookedList(callCountList.get(1));

			return ok(toJson(data));
		}

	}

	// count of follow up

	public Result getCountOfFollowUpOfToday() {

		if (!checkCRERole()) {
			applicationLogoutController.logout();
			return forbidden("Not Authorized");
		} else {

			String cre = getUserProfile().getId();
			long countOFFollowUp = callInfoRepository.getCountOfFollowUpsTobeDoneToday(cre, getUserLogindealerCode());

			return ok(Long.toString(countOFFollowUp));
		}

	}

	public Result viewReportCRE() {
		String userName = getUserProfile().getId();
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		long countOfSchCallOfCRE = scheduledCallRepository.getScheduledCallCountByOfCRE(userName,
				getUserLogindealerCode());
		long count = scheduledCallRepository.getScheduledCallsPendingCountForCRE(userName, getUserLogindealerCode());
		long count1 = scheduledCallRepository.getServiceBookedCountForCRE(userName, getUserLogindealerCode());

		double count2 = scheduledCallRepository.getConversionPercentageOfCRE(userName, getUserLogindealerCode());

		String pattern = "###.##";

		DecimalFormat formatter = new DecimalFormat(pattern);

		String displayPercentage = formatter.format(count2);

		long countOFFollowUp = callInfoRepository.getCountOfFollowUpsTobeDoneToday(userName, getUserLogindealerCode());

		ArrayList<String> ajaxData = new ArrayList<String>();
		ajaxData.add(Long.toString(countOfSchCallOfCRE));
		ajaxData.add(Long.toString(count));
		ajaxData.add(Long.toString(count1));
		ajaxData.add(displayPercentage);
		ajaxData.add(Long.toString(countOFFollowUp));

		List<String> pieData = callInfoRepository.getCallTypePieForCRE(userName, getUserLogindealerCode());
		PieChartCallTypeData returnData = new PieChartCallTypeData();
		returnData.setMissedCallCount(pieData.get(0));
		returnData.setOutgoingCallCount(pieData.get(1));
		returnData.setIncomingCallCount(pieData.get(2));

		List<List> callCountList = callInfoRepository.getServiceBookedBetweenTimeForCRE(userName,
				getUserLogindealerCode());

		BookedLineChartData bookeddata = new BookedLineChartData();
		bookeddata.setCallList(callCountList.get(0));
		bookeddata.setBookedList(callCountList.get(1));

		return ok(viewReportCRE.render(ajaxData, bookeddata, returnData, getUserLogindealerCode(), dealerName, userName,
				Form.form(WyzUser.class)));
	}

	/*
	 * public Result getTheBarChartPage(){
	 * 
	 * String userName =getUserProfile().getId(); String dealerName =
	 * getDealerName(); List<WyzUser>
	 * creUser=wyzUserRepository.getCREofCREManager(userName,
	 * getUserLogindealerCode()); List<String> dateOfWeeks=new
	 * ArrayList<String>(); dateOfWeeks.add("Today"); dateOfWeeks.add(
	 * "Last 1 Week"); dateOfWeeks.add("Last 2 Week"); dateOfWeeks.add(
	 * "Last 3 Week");
	 * 
	 * return ok(viewBarChartsOfCREManager.render(dealerName,
	 * userName,creUser,dateOfWeeks)); }
	 */

	/*
	 * public Result getCallTypeChartForCRE(){ Form<ListingForm> form =
	 * Form.form(ListingForm.class).bindFromRequest(); ListingForm formData =
	 * form.get(); List<String> creUser =formData.getSelectedValues();
	 * logger.info("Date type : "+formData.getTranferData());
	 * 
	 * List<List> data=new ArrayList<List>();
	 * 
	 * //List<WyzUser> creUser=wyzUserRepository.getCREofCREManager(cre,
	 * getUserLogindealerCode()); for(String credata:creUser){
	 * 
	 * 
	 * 
	 * Calendar c = Calendar.getInstance(); Date currentDate=c.getTime();
	 * currentDate.setHours(23); currentDate.setMinutes(59);
	 * currentDate.setSeconds(59); logger.info("date converted is : "
	 * +currentDate);
	 * 
	 * 
	 * c.setTime(new Date()); // Now use today date. c.add(Calendar.DATE, -6);
	 * // minus 6 days Date addedDate=c.getTime(); addedDate.setHours(0);
	 * addedDate.setMinutes(0); addedDate.setSeconds(0);
	 * 
	 * 
	 * logger.info("one week :"+addedDate); List<String>
	 * chart1=callInfoRepository.
	 * getCallTypeCountsForCRE(credata,getUserLogindealerCode(),currentDate,
	 * addedDate); logger.info("the outgoing count is : "+chart1.get(0));
	 * logger.info("the missedCall count is : "+chart1.get(1)); logger.info(
	 * "the incoimng count is : "+chart1.get(2)); logger.info("one Form submit"
	 * ); data.add(chart1); }
	 * 
	 * //return ok("done"); return ok(toJson(data));
	 * 
	 * 
	 * }
	 */
	/*
	 * public Result getCallTypeChart(){
	 * 
	 * 
	 * logger.info("second form submit"); return ok("done");
	 * 
	 * }
	 */
	private boolean checkCREManagerRole() {

		String role = (String) getUserProfile().getAttribute("USER_ROLE");
		if ("CREManager".equals(role))
			return true;
		else
			return false;
	}

	private boolean checkCRERole() {

		String role = (String) getUserProfile().getAttribute("USER_ROLE");
		if ("CRE".equals(role))
			return true;
		else
			return false;
	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	@Secure(clients = "FormClient")
	public Result uploadSummayReport() {
		
		List<UploadReportFiles> reportsList=reportsRepo.uploadReportFilesList();

		TreeMap<String, String> typeOfReport = new TreeMap<String, String>();
		for(UploadReportFiles up:reportsList){
			
			typeOfReport.put(up.getFileId(), up.getFileName());
		}
		return ok(uploadSummaryReportPage.render(typeOfReport, getUserLogindealerCode(), getDealerName(),
				getUserLoginName()));
	}

	@Secure(clients = "FormClient")
	public Result uploadSummaryDataTable() {
		
		WyzUser userdata = wyzUserRepository.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String uploadTypeIs="";
		String fromDate="";
		String toDate="";
		
		
		
		if (paramMap.get("columns[0][search][value]") != null) {
			uploadTypeIs = paramMap.get("columns[0][search][value]")[0];
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromDate = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDate = paramMap.get("columns[2][search][value]")[0];
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && uploadTypeIs.length() == 0 && fromDate.length() == 0
				&& toDate.length() == 0 ) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}
		
		logger.info("uploadTypeIs :"+uploadTypeIs+"fromDate"+fromDate+"toDate"+toDate);

		long totalSize = reportsRepo.getUploadReportSummaryListCount(userdata.getUserName(),uploadTypeIs, fromDate,toDate);
		//searchRepo.getAllDispositionRequiredData(userdata.getId(), 4);
		 logger.info("totalSize of followup interaction : "+totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		long typeOfDispo = 4;
		int orderDirection = 1;
		List<ReportSummary> reportlist = reportsRepo.getUploadReportSummaryList(userdata.getUserName(),uploadTypeIs, fromDate,
				toDate, fromIndex, toIndex);

		if (!allflag) {
			if (globalSearch)
				//patternCount = searchRepo.getAllDispositionRequiredData(userdata.getId(), 4);
				patternCount = reportsRepo.getUploadReportSummaryListCount(userdata.getUserName(),uploadTypeIs, fromDate,toDate);
			else
				patternCount = reportsRepo.getUploadReportSummaryListCount(userdata.getUserName(),uploadTypeIs, fromDate,toDate);

		}
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize+"reportlist"+reportlist.size());

		// logger.info("subList Size:" + followUpList.size());

		// long sublistSize = customerList.size();

		// logger.info("fromIndex:" + fromIndex);
		// logger.info("toIndex:" + toIndex);
		// logger.info("Search Pattern" + searchPattern);

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (ReportSummary c : reportlist) {

			ObjectNode row = Json.newObject();
			
			row.put("0", getStringDate(c.getDate()));
			row.put("1", c.getDay());
			row.put("2", c.getReportdate());
			row.put("3", c.getCount());
			row.put("4", c.getWorkshopOrCity());
			
			an.add(row);
		}

		return ok(result);

	}
	
	@Secure(clients = "FormClient")
	public Result downloadUploadSummaryReport() throws IOException, IllegalArgumentException, IllegalAccessException {
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		logger.info("upload summary reports:");
		WyzUser userdata = wyzUserRepository.getUserbyUserName(getUserLoginName());
		
		String searchPattern = "";
		String uploadTypeIs="";
		String fromDate="";
		String toDate="";
		
		if (paramMap.get("columns[0][search][value]") != null) {
			uploadTypeIs = paramMap.get("columns[0][search][value]")[0];
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromDate = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toDate = paramMap.get("columns[2][search][value]")[0];
		}
		//long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		//long toIndex = Long.valueOf(paramMap.get("length")[0]);
		
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && uploadTypeIs.length() == 0 && fromDate.length() == 0
				&& toDate.length() == 0 ) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}
	
		long totalSize = 0;
		
	     totalSize = reportsRepo.getUploadReportSummaryListCount(userdata.getUserName(),uploadTypeIs, fromDate,toDate);

		logger.info("totalSize of download in reports call history: " + totalSize);

		long patternCount = 0;

		List<ReportSummary> reportlist = reportsRepo.getUploadReportSummaryList(userdata.getUserName(),uploadTypeIs, fromDate,
				toDate, 0, totalSize);

		logger.info("reportlist:"+reportlist.size());
		logger.info("pattern count:"+patternCount);

		Config configuration = ConfigFactory.load();
		String pathOfTemp = configuration.getString("app.configuration");

		String fileName = currentDate() + "_Upload_Summary_Report.xlsx";

		File fis = new File(pathOfTemp + fileName);

		FileOutputStream fileOut = new FileOutputStream(fis);
		XSSFWorkbook wb = new XSSFWorkbook();
		Sheet sheet = wb.createSheet("Sheet1");

		String[] headers = new String[] { 
				"Date", 
				"Day", 
				"Recorded Date",
				"Count",
				"Location"};

		int rowCount = 1;
		Row r = sheet.createRow(0);
		for (int rn = 0; rn < headers.length; rn++) {
			r.createCell(rn).setCellValue(headers[rn]);
		}
		for (ReportSummary aBook : reportlist) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;

			for (Field field : aBook.getClass().getDeclaredFields()) {
				if (columnCount < headers.length) {
					Cell cell = row.createCell(columnCount++);
					cell.setCellValue(String.valueOf(field.get(aBook)));
					// logger.info("cell value : " + field.get(aBook));

				}
			}

		}
		wb.write(fileOut);
		fileOut.close();
		return ok(fis, fileName);

	}
	
	private String getStringDate(Date call_date) {
		// TODO Auto-generated method stub
		if (call_date != null) {

			String pattern = "dd/MM/yyyy";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String stringDate= formatter.format(call_date);
			return stringDate;
			
		}else{
			
		}
		return "";
	}
	
	private java.sql.Date currentDate() {

		return new java.sql.Date(System.currentTimeMillis());
	}

}
