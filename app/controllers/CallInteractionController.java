/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import static play.libs.Json.toJson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;
import org.springframework.stereotype.Controller;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import controllers.webmodels.AssignedInteractionDataOnTabLoad;
import controllers.webmodels.AssignedListForCRE;
import controllers.webmodels.CallInteractionDataOnTabLoad;
import controllers.webmodels.CallLogsOnLoadTabData;
import controllers.webmodels.ComplaintInformation;
import controllers.webmodels.CustomerDataOnTabLoad;
import controllers.webmodels.SMSInteractionhistory;
import controllers.webmodels.SRDispositionDataOnTabLoad;
import controllers.webmodels.ServiceDataOnTabLoad;
import controllers.webmodels.ServicebookedDataOnTabLoad;
import controllers.webmodels.VehicleDataOnTabLoad;
import controllers.webmodels.WyzUserDataOnTabLoad;
import models.Address;
import models.AssignedInteraction;
import models.CallInteraction;
import models.Campaign;
import models.Complaint;
import models.ComplaintInteraction;
import models.ComplaintSource;
import models.ComplaintTypes;
import models.Customer;
import models.CustomerJson;
import models.Driver;
import models.Email;
import models.JobCardDetailsUpload;
import models.ListUploadMasterAlias;
import models.ListingForm;
import models.Location;
import models.MessageTemplete;
import models.NewCustomer;
import models.Phone;
import models.PickupDrop;
import models.SMSInteraction;
import models.Service;
import models.ServiceAdvisor;
import models.ServiceBooked;
import models.ServiceTypes;
import models.SMSTemplate;
import models.TaggingUsers;
import models.UnAvailability;
import models.UploadFileFields;
import models.UploadMasterFormat;
import models.Vehicle;
import models.Workshop;
import models.WyzUser;
import models.workshopBill;
import play.Logger.ALogger;
import play.data.Form;
/**
 *
 * @author W-885
 */
import play.db.DB;
import play.mvc.BodyParser;
import play.mvc.Http.MultipartFormData;
import play.mvc.Result;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.SMSTriggerRespository;
import repositories.WyzUserRepository;
import utils.CampaignExcel;
import utils.JobCardStatus;
import utils.SMRData;
import utils.SaleRegisterExcel;
import utils.WorkBillRegister;
import views.html.addCustomer;
import views.html.assignCallBasedOnSelectedRange;
import views.html.assignComplaints;
import views.html.assignedInteractionPageForUpload;
import views.html.assignedInteractionTable;
import views.html.changeAssignmentByCREManager;
import views.html.complaintResolution;
import views.html.complaints;
import views.html.complaintsManager;
import views.html.dataUploadFormatForm;
import views.html.downloadUploadExcelFormat;
import views.html.roasterTableByCREMan;
import views.html.rosterOfUnAvailibilityByUserSelectedRange;
import views.html.rosterOfUnAvailibilityOfUserSelectedRange;
import views.html.searchCustomerCRE;
import views.html.uploadMasterFilesForm;
//import views.html.viewAllComplaints;
import views.html.viewAllComplaints1;
import views.html.viewAllComplaintsReadOnly;
import views.html.searchCustomerManager;


@Controller
public class CallInteractionController extends play.mvc.Controller {

	ALogger logger = play.Logger.of("application");

	private final CallInfoRepository repo;
	private final CallInteractionsRepository call_int_repo;
	private final WyzUserRepository wyzRepo;
	private final SMSTriggerRespository sms_repo;
	private PlaySessionStore playSessionStore;
	

	@Inject
	public CallInteractionController(CallInfoRepository repository, org.pac4j.core.config.Config config,
			CallInteractionsRepository interRepo, WyzUserRepository wyzRepository, SMSTriggerRespository smsRepository,
			PlaySessionStore plstore) {
		repo = repository;
		call_int_repo = interRepo;
		wyzRepo = wyzRepository;
		sms_repo = smsRepository;
		config = (org.pac4j.core.config.Config) config;
		playSessionStore = plstore;

	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	private String getOEMOfDealer() {

		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;

	}

	@Secure(clients = "FormClient")
	public Result getFollowUpRequiredData(String selectAgent) {

		List<CallInteraction> followUpCalls = call_int_repo.getFollowUpCallsOfUser(getUserLoginName(),
				getUserLogindealerCode());
		logger.info("Data enetered");

		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();

		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<SRDispositionDataOnTabLoad> sRD_data = new ArrayList<SRDispositionDataOnTabLoad>();
		List<CallInteractionDataOnTabLoad> int_data = new ArrayList<CallInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();

		for (CallInteraction addToAjaxData : followUpCalls) {

			if (addToAjaxData.getCustomer() != null) {

				CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
				customer_data.setCustomerName(addToAjaxData.getCustomer().getCustomerName());
				customer_data.setCustomerCategory(addToAjaxData.getCustomer().getCustomerCategory());
				List<Phone> listPhone = addToAjaxData.getCustomer().getPhones();
				for (Phone phoneList : listPhone) {

					if (phoneList.isPreferredPhone) {

						customer_data.setCustomerPhone(phoneList.getPhoneNumber());

					}

				}
				cust_data.add(customer_data);
			} else {
				logger.info("Customer is empty");
			}

			if (addToAjaxData.getVehicle() != null) {

				VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

				ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
				Service service_data = call_int_repo.getLatestServiceData(addToAjaxData.getVehicle().getVehicle_id());

				service_data1.setNextServicedate(service_data.getNextServiceDate());

				serv_data.add(service_data1);

				veh_data1.setVehicleRegNo(addToAjaxData.getVehicle().getVehicleRegNo());
				veh_data1.setModel(addToAjaxData.getVehicle().getModel());
				veh_data1.setNextServicedate(addToAjaxData.getVehicle().getNextServicedate());

				veh_data.add(veh_data1);
			} else {
				logger.info("Vehical is empty");
			}
			if (addToAjaxData.getSrdisposition() != null) {
				SRDispositionDataOnTabLoad sr_disposition_data = new SRDispositionDataOnTabLoad();
				sr_disposition_data
						.setCallDisposition(addToAjaxData.getSrdisposition().getCallDispositionData().getDisposition());
				sr_disposition_data.setFollowUpDate(addToAjaxData.getSrdisposition().getFollowUpDate());
				sr_disposition_data.setFollowUpTime(addToAjaxData.getSrdisposition().getFollowUpTime());
				sRD_data.add(sr_disposition_data);
			} else {
				logger.info("srDisposition is empty");
			}

			CallInteractionDataOnTabLoad call_data_int = new CallInteractionDataOnTabLoad();
			call_data_int.setId(addToAjaxData.getId());
			call_data_int.setDealerCode(addToAjaxData.getDealerCode());
			int_data.add(call_data_int);

		}
		logger.info("follow up required cust_data" + cust_data.size() + "veh_data" + veh_data.size() + "sRD_data"
				+ sRD_data.size() + "int_data" + int_data.size() + "serv_data" + serv_data.size());
		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setSrdispositionLoadList(sRD_data);
		call_interaction.setCallInteractionLoadList(int_data);
		call_interaction.setServiceLoadList(serv_data);
		return ok(toJson(call_interaction));

	}

	// service booked

	@Secure(clients = "FormClient")
	public Result getServiceBookedData(String selectAgent) {
		logger.info("Data enetered in serviceBookedCalls");
		List<CallInteraction> serviceBookedCalls = call_int_repo.getServicBookedCalls(getUserLoginName(),
				getUserLogindealerCode());
		// logger.info("serviceBookedCalls :"+serviceBookedCalls.size());

		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();

		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<SRDispositionDataOnTabLoad> sRD_data = new ArrayList<SRDispositionDataOnTabLoad>();
		List<CallInteractionDataOnTabLoad> int_data = new ArrayList<CallInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();

		List<ServicebookedDataOnTabLoad> servbooked_data = new ArrayList<ServicebookedDataOnTabLoad>();

		for (CallInteraction assign : serviceBookedCalls) {

			if (assign.getCustomer() != null) {

				CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
				customer_data.setCustomerName(assign.getCustomer().getCustomerName());
				customer_data.setCustomerCategory(assign.getCustomer().getCustomerCategory());
				List<Phone> listPhone = assign.getCustomer().getPhones();
				for (Phone phoneList : listPhone) {

					if (phoneList.isPreferredPhone) {

						customer_data.setCustomerPhone(phoneList.getPhoneNumber());

					}

				}
				cust_data.add(customer_data);
			} else {
				logger.info("Customer is empty");
			}

			if (assign.getVehicle() != null) {

				VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

				ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
				Service service_data = call_int_repo.getLatestServiceData(assign.getVehicle().getVehicle_id());

				service_data1.setNextServicedate(service_data.getNextServiceDate());

				serv_data.add(service_data1);

				veh_data1.setVehicleRegNo(assign.getVehicle().getVehicleRegNo());
				veh_data1.setModel(assign.getVehicle().getModel());
				veh_data1.setNextServicedate(assign.getVehicle().getNextServicedate());

				veh_data.add(veh_data1);
			} else {
				logger.info("Vehical is empty");
			}
			if (assign.getSrdisposition() != null) {
				SRDispositionDataOnTabLoad sr_disposition_data = new SRDispositionDataOnTabLoad();
				sr_disposition_data
						.setCallDisposition(assign.getSrdisposition().getCallDispositionData().getDisposition());
				sRD_data.add(sr_disposition_data);
			} else {
				logger.info("srDisposition is empty");
			}
			if (assign.getServiceBooked() != null) {
				ServicebookedDataOnTabLoad servicebooked_data = new ServicebookedDataOnTabLoad();

				servicebooked_data.setServiceScheduledDate(assign.getServiceBooked().getServiceScheduledDateStr());
				servicebooked_data.setServiceScheduledTime(assign.getServiceBooked().getServiceScheduledTimeStr());
				servbooked_data.add(servicebooked_data);
			} else {
				logger.info("ServiceBooked is empty");
			}

			CallInteractionDataOnTabLoad call_data_int = new CallInteractionDataOnTabLoad();
			call_data_int.setId(assign.getId());
			call_data_int.setDealerCode(assign.getDealerCode());

			int_data.add(call_data_int);

		}
		logger.info("service booked required cust_data" + cust_data.size() + "veh_data" + veh_data.size() + "sRD_data"
				+ sRD_data.size() + "int_data" + int_data.size() + "serv_data" + serv_data.size());
		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setSrdispositionLoadList(sRD_data);
		call_interaction.setCallInteractionLoadList(int_data);
		call_interaction.setServiceLoadList(serv_data);
		call_interaction.setServicebookedLoadList(servbooked_data);
		return ok(toJson(call_interaction));

	}

	@Secure(clients = "FormClient")
	public Result getNonContactsData(String selectAgent) {
		logger.info("Data enetered in NonContacts");
		List<CallInteraction> NonContacts = call_int_repo.getNonContactsCalls(getUserLoginName(),
				getUserLogindealerCode());
		logger.info("NonContacts :" + NonContacts.size());

		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();

		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<SRDispositionDataOnTabLoad> sRD_data = new ArrayList<SRDispositionDataOnTabLoad>();
		List<CallInteractionDataOnTabLoad> int_data = new ArrayList<CallInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();

		for (CallInteraction addToAjaxData : NonContacts) {

			if (addToAjaxData.getCustomer() != null) {

				CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
				customer_data.setCustomerName(addToAjaxData.getCustomer().getCustomerName());
				customer_data.setCustomerCategory(addToAjaxData.getCustomer().getCustomerCategory());
				List<Phone> listPhone = addToAjaxData.getCustomer().getPhones();
				for (Phone phoneList : listPhone) {

					if (phoneList.isPreferredPhone) {

						customer_data.setCustomerPhone(phoneList.getPhoneNumber());

					}

				}
				cust_data.add(customer_data);
			} else {
				logger.info("Customer is empty");
			}

			if (addToAjaxData.getVehicle() != null) {

				VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

				ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
				Service service_data = call_int_repo.getLatestServiceData(addToAjaxData.getVehicle().getVehicle_id());

				service_data1.setNextServicedate(service_data.getNextServiceDate());

				serv_data.add(service_data1);

				veh_data1.setVehicleRegNo(addToAjaxData.getVehicle().getVehicleRegNo());
				veh_data1.setModel(addToAjaxData.getVehicle().getModel());
				veh_data1.setNextServicedate(addToAjaxData.getVehicle().getNextServicedate());

				veh_data.add(veh_data1);
			} else {
				logger.info("Vehical is empty");
			}

			if (addToAjaxData.getSrdisposition() != null) {
				SRDispositionDataOnTabLoad sr_disposition_data = new SRDispositionDataOnTabLoad();
				sr_disposition_data
						.setCallDisposition(addToAjaxData.getSrdisposition().getCallDispositionData().getDisposition());

				sRD_data.add(sr_disposition_data);
			} else {
				logger.info("srDisposition is empty");
			}

			CallInteractionDataOnTabLoad call_data_int = new CallInteractionDataOnTabLoad();
			call_data_int.setId(addToAjaxData.getId());
			call_data_int.setDealerCode(addToAjaxData.getDealerCode());
			int_data.add(call_data_int);

		}
		logger.info("non contact cust_data" + cust_data.size() + "veh_data" + veh_data.size() + "sRD_data"
				+ sRD_data.size() + "int_data" + int_data.size() + "serv_data" + serv_data.size());
		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setSrdispositionLoadList(sRD_data);
		call_interaction.setCallInteractionLoadList(int_data);
		call_interaction.setServiceLoadList(serv_data);
		return ok(toJson(call_interaction));

	}

	@Secure(clients = "FormClient")
	public Result getDroppedCallsData(String selectAgent) {
		logger.info("Data enetered in DroppedCalls");
		List<CallInteraction> DroppedCalls = call_int_repo.getDroppedCalls(getUserLoginName(),
				getUserLogindealerCode());
		logger.info("DroppedCalls :" + DroppedCalls.size());

		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();

		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<SRDispositionDataOnTabLoad> sRD_data = new ArrayList<SRDispositionDataOnTabLoad>();
		List<CallInteractionDataOnTabLoad> int_data = new ArrayList<CallInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();

		for (CallInteraction addToAjaxData : DroppedCalls) {

			if (addToAjaxData.getCustomer() != null) {

				CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
				customer_data.setCustomerName(addToAjaxData.getCustomer().getCustomerName());
				customer_data.setCustomerCategory(addToAjaxData.getCustomer().getCustomerCategory());
				List<Phone> listPhone = addToAjaxData.getCustomer().getPhones();
				for (Phone phoneList : listPhone) {

					if (phoneList.isPreferredPhone) {

						customer_data.setCustomerPhone(phoneList.getPhoneNumber());

					}

				}
				cust_data.add(customer_data);
			} else {
				logger.info("Customer is empty");
			}

			if (addToAjaxData.getVehicle() != null) {

				VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

				ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
				Service service_data = call_int_repo.getLatestServiceData(addToAjaxData.getVehicle().getVehicle_id());

				service_data1.setNextServicedate(service_data.getNextServiceDate());

				serv_data.add(service_data1);

				veh_data1.setVehicleRegNo(addToAjaxData.getVehicle().getVehicleRegNo());
				veh_data1.setModel(addToAjaxData.getVehicle().getModel());
				veh_data1.setNextServicedate(addToAjaxData.getVehicle().getNextServicedate());

				veh_data.add(veh_data1);
			} else {
				logger.info("Vehical is empty");
			}
			if (addToAjaxData.getSrdisposition() != null) {
				SRDispositionDataOnTabLoad sr_disposition_data = new SRDispositionDataOnTabLoad();
				sr_disposition_data
						.setCallDisposition(addToAjaxData.getSrdisposition().getCallDispositionData().getDisposition());

				sRD_data.add(sr_disposition_data);
			} else {
				logger.info("srDisposition is empty");
			}

			CallInteractionDataOnTabLoad call_data_int = new CallInteractionDataOnTabLoad();
			call_data_int.setId(addToAjaxData.getId());
			call_data_int.setDealerCode(addToAjaxData.getDealerCode());

			int_data.add(call_data_int);

		}
		logger.info("dropped up required cust_data" + cust_data.size() + "veh_data" + veh_data.size() + "sRD_data"
				+ sRD_data.size() + "int_data" + int_data.size() + "serv_data" + serv_data.size());
		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setSrdispositionLoadList(sRD_data);
		call_interaction.setCallInteractionLoadList(int_data);
		call_interaction.setServiceLoadList(serv_data);
		return ok(toJson(call_interaction));

	}

	@Secure(clients = "FormClient")
	public Result getServiceNotRequiredData(String selectAgent) {
		logger.info("Data enetered in ServiceNotRequired");
		List<CallInteraction> ServiceNotRequired = call_int_repo.getServiceNotRequiredCalls(getUserLoginName(),
				getUserLogindealerCode());
		logger.info("ServiceNotRequired :" + ServiceNotRequired.size());

		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();

		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<SRDispositionDataOnTabLoad> sRD_data = new ArrayList<SRDispositionDataOnTabLoad>();
		List<CallInteractionDataOnTabLoad> int_data = new ArrayList<CallInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();

		for (CallInteraction addToAjaxData : ServiceNotRequired) {

			if (addToAjaxData.getCustomer() != null) {

				CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
				customer_data.setCustomerName(addToAjaxData.getCustomer().getCustomerName());
				customer_data.setCustomerCategory(addToAjaxData.getCustomer().getCustomerCategory());
				List<Phone> listPhone = addToAjaxData.getCustomer().getPhones();
				for (Phone phoneList : listPhone) {

					if (phoneList.isPreferredPhone) {

						customer_data.setCustomerPhone(phoneList.getPhoneNumber());

					}

				}
				cust_data.add(customer_data);
			} else {
				logger.info("Customer is empty");
			}

			if (addToAjaxData.getVehicle() != null) {

				VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

				ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
				Service service_data = call_int_repo.getLatestServiceData(addToAjaxData.getVehicle().getVehicle_id());

				service_data1.setNextServicedate(service_data.getNextServiceDate());

				serv_data.add(service_data1);

				veh_data1.setVehicleRegNo(addToAjaxData.getVehicle().getVehicleRegNo());
				veh_data1.setModel(addToAjaxData.getVehicle().getModel());
				veh_data1.setNextServicedate(addToAjaxData.getVehicle().getNextServicedate());

				veh_data.add(veh_data1);
			} else {
				logger.info("Vehical is empty");
			}
			if (addToAjaxData.getSrdisposition() != null) {
				SRDispositionDataOnTabLoad sr_disposition_data = new SRDispositionDataOnTabLoad();
				sr_disposition_data
						.setCallDisposition(addToAjaxData.getSrdisposition().getCallDispositionData().getDisposition());
				sr_disposition_data.setNoServiceReason(addToAjaxData.getSrdisposition().getNoServiceReason());

				sRD_data.add(sr_disposition_data);
			} else {
				logger.info("srDisposition is empty");
			}

			CallInteractionDataOnTabLoad call_data_int = new CallInteractionDataOnTabLoad();
			call_data_int.setId(addToAjaxData.getId());
			call_data_int.setDealerCode(addToAjaxData.getDealerCode());
			int_data.add(call_data_int);

		}

		logger.info("service not required cust_data" + cust_data.size() + "veh_data" + veh_data.size() + "sRD_data"
				+ sRD_data.size() + "int_data" + int_data.size() + "serv_data" + serv_data.size());
		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setSrdispositionLoadList(sRD_data);
		call_interaction.setCallInteractionLoadList(int_data);
		call_interaction.setServiceLoadList(serv_data);
		return ok(toJson(call_interaction));

	}

	@Secure(clients = "FormClient")
	public Result getAssignedList() {

		List<AssignedInteraction> assigned_data = call_int_repo.getAssigendIntOfManager(getUserLoginName());

		// List<WyzUser>
		// allCRE=wyzRepo.getCREofCREManager(getUserLoginName(),getUserLogindealerCode());
		List<WyzUser> availableallCRE = new ArrayList<WyzUser>();
		List<WyzUser> allCRE = wyzRepo.getAllUsersAvailableToday(getUserLogindealerCode(), getUserLoginName());
		for (WyzUser user_data : allCRE) {

			if (user_data.getRole().equals("CRE")) {

				availableallCRE.add(user_data);

			}

		}
		return ok(assignedInteractionTable.render(assigned_data, getDealerName(), getUserLoginName(), availableallCRE));

	}

	@Secure(clients = "FormClient")
	public Result reAssignAgentUpdate(long rowId, long wyzUserId, long post_id) {

		AssignedInteraction assignInteractionsData = call_int_repo.updateAssignedAgent(rowId, wyzUserId, post_id);
		String user = assignInteractionsData.getWyzUser().getUserName();
		// return assignInteractionsData;
		return ok(toJson(user));
	}

	@Secure(clients = "FormClient")
	public Result getCustomerByInteractionId() {
		long id = 1;
		Customer data1 = repo.getCustomerByInteractionId(id);

		CallInteraction data = repo.getInteractionCallByCustomer(id);
		logger.info("data :" + data.getCallCount());
		return ok("done");

	}

	private static CellProcessor[] getUserDataCellProcessorForDirectUpload() {
		return new CellProcessor[] { new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional() };

	}

	@Secure(clients = "FormClient")
	public Result getAssignedInteraction() {
		List<String> cresList = repo.getCREUserNameofCREManager(getUserLoginName(), getUserLogindealerCode());

		return ok(assignedInteractionPageForUpload.render(getUserLogindealerCode(), getUserLoginName()));

	}

	@Secure(clients = "FormClient")
	public Result toUploadInteractionsByCREManager() throws FileNotFoundException {

		ICsvBeanReader beanReader = null;
		MultipartFormData body = request().body().asMultipartFormData();
		MultipartFormData.FilePart profile = body.getFile("uploadfiles");
		File file = (File) profile.getFile();
		InputStream is = new FileInputStream(file);

		try {

			UploadFileFields uplodingData = null;

			beanReader = new CsvBeanReader(new BufferedReader(new InputStreamReader(is)),
					CsvPreference.STANDARD_PREFERENCE);

			final String[] header = beanReader.getHeader(true);

			final CellProcessor[] processors = getUserDataCellProcessorForDirectUpload();

			while ((uplodingData = beanReader.read(UploadFileFields.class, header, processors)) != null) {
				// process course

				if (uplodingData != null) {

					// if(uplodingData.getNextServiceDue()!=null){
					//
					// call_int_repo.addCustomerAndVehicalInfo(uplodingData);
					//
					//
					// }
				}
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (beanReader != null)
				try {
					beanReader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		return redirect(controllers.routes.CallInteractionController.getAssignedInteraction());

	}

	@Secure(clients = "FormClient")
	public Result getPageForAssigningCalls() {
		// List<String> cresList =
		// call_int_repo.getCREofCREManager(getUserLoginName(),
		// getUserLogindealerCode());

		List<String> cresList = call_int_repo.getCREandServiceAdvOFMan(getUserLoginName(), getUserLogindealerCode());
		List<Vehicle> scheduleData = new ArrayList<Vehicle>();
		List<AssignedListForCRE> psf_assign_data = new ArrayList<AssignedListForCRE>();

		// List<Service> serviceData=new ArrayList<Service>();
		String fromDate = "";
		String toDate = "";
		String customerCat = "";
		String serviceCat = "";
		// String fuelCat = "";
		long campaignTypeCat = 0;
		long campaignNameCat = 0;
		long campaignNamePSF = 0;
		boolean isRangeSelected = false;
		boolean toAssigncall = true;

		List<Campaign> campaign_list = call_int_repo.getAllCampaignList();
		List<Campaign> campaignList = call_int_repo.getCampaignNames();
		List<Location> locationList = call_int_repo.getLocationList();
		List<Campaign> campaignListPSF = call_int_repo.getCampaignNamesPSF();
		List<ServiceTypes> servicetypes=call_int_repo.getAllServiceTypeList();
		
		return ok(assignCallBasedOnSelectedRange.render(campaignNamePSF, campaignNameCat, campaignTypeCat, customerCat,
				serviceCat, fromDate, toDate, isRangeSelected, toAssigncall, cresList, scheduleData, getUserLoginName(),
				getDealerName(), campaign_list, locationList, campaignList, campaignListPSF, psf_assign_data,servicetypes));
	}

	@Secure(clients = "FormClient")
	public Result getSelectedAssignCalls() {

		Form<ListingForm> data_range = Form.form(ListingForm.class).bindFromRequest();
		ListingForm range = data_range.get();

		Form<Campaign> campaign_data = Form.form(Campaign.class).bindFromRequest();
		Campaign campaigninfo = campaign_data.get();

		String uploadAction = range.getSelectedFile();

		if (uploadAction == null || uploadAction.length() == 0) {
			return forbidden(views.html.error403.render());
		} else {

			String action = uploadAction;
			logger.info("uploadAction[0] first id value : " + action);
			if ("viewAssignCalls".equals(action)) {
				String fromDate = range.getSingleData();
				String toDate = range.getTranferData();
				String customerCat = range.getCust_category();
				String serviceCat = range.getService_type();
				long campaignTypeCat = range.getCampaignType();
				long campaignNameCat = range.getCampaignName();
				long campaignNamePSF = range.getCampaignNamePSF();
				
				long camName = range.getCampaignNameToAss();
				long psfName = range.getCampaignPSFToAss();

				String cityName = range.getCityName();

				
				long workshopName = range.getWorkshopId();
			
				logger.info("Data For Assignment is customerCat  is : " + customerCat + "serviceCat : " + serviceCat
						+ "campaignTypeCat : " + campaignTypeCat + "campaignNameCat : " + campaignNameCat
						+ "campaignName : " + campaignNamePSF);
				
				List<String> cresList = repo.getCREUserNameofCREManagerByLocation(getUserLoginName(),
						getUserLogindealerCode(), cityName, workshopName);

				SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date fromDateNew = new Date();
				Date toDateNew = new Date();
				try {
					fromDateNew = dmyFormat.parse(fromDate);
					toDateNew = dmyFormat.parse(toDate);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				List<Vehicle> scheduleData = new ArrayList<Vehicle>();

				List<AssignedListForCRE> psf_assign_data = new ArrayList<AssignedListForCRE>();

				String selectedCampaignType = call_int_repo.getCampaignTypeById(campaignTypeCat);

				logger.info("selectedCampaignType  is : " + selectedCampaignType);

				if (selectedCampaignType.equals("PSF")) {

					logger.info("PSf List for call assignment");

					psf_assign_data = call_int_repo.getAssignListOfPSFByProcedure(fromDateNew, toDateNew, customerCat,
							serviceCat, campaignTypeCat, campaignNameCat, campaignNamePSF);
				} else {

					scheduleData = call_int_repo.assignCallsToUser(fromDateNew, toDateNew, customerCat, serviceCat,
							campaignTypeCat, campaignNameCat, campaignNamePSF);
				}

				logger.info("list of vehicles to be assigned is :" + scheduleData.size());
				boolean isRangeSelected = false;
				boolean toAssigncall = false;
				if (scheduleData.size() > 0) {
					isRangeSelected = true;
					toAssigncall = false;
				} else if (psf_assign_data.size() > 0) {
					isRangeSelected = true;
					toAssigncall = false;
				} else {
					isRangeSelected = false;
					toAssigncall = true;
				}
				List<Campaign> campaign_list = call_int_repo.getAllCampaignList();
				List<Location> locationList = call_int_repo.getLocationList();
				List<Campaign> campaignList = call_int_repo.getCampaignNames();
				List<Campaign> campaignListPSF = call_int_repo.getCampaignNamesPSF();
				List<ServiceTypes> servicetypes=call_int_repo.getAllServiceTypeList();

				return ok(assignCallBasedOnSelectedRange.render(campaignNamePSF, campaignNameCat, campaignTypeCat,
						customerCat, serviceCat, fromDate, toDate, isRangeSelected, toAssigncall, cresList,
						scheduleData, getUserLoginName(), getDealerName(), campaign_list, locationList, campaignList,
						campaignListPSF, psf_assign_data,servicetypes));

			} else if ("assignCalls".equals(action)) {
				List<String> selectedList = range.getData();
				for (String s : selectedList) {
					logger.info("s user :" + s);

				}

				String fromDate = range.getFromDateToAss();
				String toDate = range.getToDateToAss();
				String customerCat = range.getCustomerCatToAss();
				String serviceCat = range.getServiceTypeToAss();
				long campaignCat = range.getCampaignTypeToAss();
				long campaignNameCat = range.getCampaignNameToAss();

				long campaignNamePSF = range.getCampaignPSFToAss();
				logger.info("campaignNamePSF2::" + campaignNamePSF);

				logger.info("campaignNameCat sub id in assign button:" + campaignNameCat);
				long campaignTypeCat = range.getCampaignType();

				String cityName = range.getCityName();
				long workshopName = range.getWorkshopId();
				// logger.info("customerCat is : " + customerCat + "serviceCat"
				// + serviceCat + "fuelCat" + fuelCat);
				// logger.info("fromDate from assign is : " + fromDate);
				// logger.info("toDate from assign date :" + toDate);

				boolean isRangeSelected = false;
				boolean toAssigncall = true;
				List<String> cresList = repo.getCREUserNameofCREManager(getUserLoginName(), getUserLogindealerCode());
				SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date fromDateNew = new Date();
				Date toDateNew = new Date();
				try {
					fromDateNew = dmyFormat.parse(fromDate);
					toDateNew = dmyFormat.parse(toDate);
				} catch (Exception e) {
					e.printStackTrace();
				}
				// logger.info("fromDateNew is : " + fromDateNew);
				// logger.info("toDateNew selected type date :" + toDateNew);

				List<Vehicle> scheduleData = new ArrayList<Vehicle>();
				List<AssignedListForCRE> psf_assign_data = new ArrayList<AssignedListForCRE>();

				String selectedCampaignType = call_int_repo.getCampaignTypeById(campaignCat);

				if (selectedCampaignType.equals("PSF")) {

					psf_assign_data = call_int_repo.getAssignListOfPSFByProcedure(fromDateNew, toDateNew, customerCat,
							serviceCat, campaignTypeCat, campaignNameCat, campaignNamePSF);
				} else {

					scheduleData = call_int_repo.assignCallsToUser(fromDateNew, toDateNew, customerCat, serviceCat,
							campaignCat, campaignNameCat, campaignNamePSF);
				}

				// logger.info("scheduleData is : " + scheduleData.size());
				int scheduledCallCount = scheduleData.size();

				int psfCallCount = psf_assign_data.size();

				int creSize = selectedList.size();
				// logger.info("the CRE of all managers are count is : " +
				// creSize);

				// PSF Assignment

				if (selectedCampaignType.equals("PSF")) {

					logger.info("PSf Assignemt");

					if (creSize != 0) {
						logger.info("count of CRE :" + creSize);

						int divCountRemainder = psfCallCount % creSize;

						logger.info("divCountRemainder of CRE and call from CRE Manager:" + divCountRemainder);

						int divCount = (psfCallCount) / (creSize);
						logger.info("divCount of CRE and call :" + divCount);

						Collections.shuffle(psf_assign_data);

						int calls_count = 0;
						for (int i = 0; i < creSize; i++) {

							for (int j = calls_count; j < divCount + calls_count; j++) {

								String userdata = selectedList.get(i);
								AssignedListForCRE psfAssignId = psf_assign_data.get(j);

								call_int_repo.assignCallByManagerForPSF(userdata, psfAssignId,
										getUserLogindealerCode());

							}

							calls_count = divCount + calls_count;
							if (calls_count >= psfCallCount) {
								break;
							}

						}
						int call_countcompleted = divCount * creSize;
						logger.info("call_countcompleted : " + call_countcompleted);

						for (int i = 0; i < creSize; i++) {

							if (call_countcompleted < psfCallCount) {

								String userdata = selectedList.get(i);
								AssignedListForCRE psfAssignId = psf_assign_data.get(call_countcompleted);

								call_int_repo.assignCallByManagerForPSF(userdata, psfAssignId,
										getUserLogindealerCode());

								call_countcompleted++;
							} else {
								break;
							}

						}

					}

				} else {

					// SMR and Campaign Assignment

					if (creSize != 0) {
						logger.info("count of CRE :" + creSize);

						int divCountRemainder = scheduledCallCount % creSize;

						logger.info("divCountRemainder of CRE and call from CRE Manager:" + divCountRemainder);

						int divCount = (scheduledCallCount) / (creSize);
						logger.info("divCount of CRE and call :" + divCount);

						Collections.shuffle(scheduleData);

						int calls_count = 0;
						for (int i = 0; i < creSize; i++) {

							for (int j = calls_count; j < divCount + calls_count; j++) {

								String userdata = selectedList.get(i);
								Vehicle scall = scheduleData.get(j);

								long scallId = scall.getVehicle_id();

								call_int_repo.assignCallByManager(userdata, scallId, getUserLogindealerCode(),
										campaignCat, campaignNameCat, campaignNamePSF);
							}

							calls_count = divCount + calls_count;
							if (calls_count >= scheduledCallCount) {
								break;
							}

						}
						int call_countcompleted = divCount * creSize;
						logger.info("call_countcompleted : " + call_countcompleted);

						for (int i = 0; i < creSize; i++) {

							if (call_countcompleted < scheduledCallCount) {

								String userdata = selectedList.get(i);

								Vehicle scall = scheduleData.get(call_countcompleted);

								long scallId = scall.getVehicle_id();

								call_int_repo.assignCallByManager(userdata, scallId, getUserLogindealerCode(),
										campaignCat, campaignNameCat, campaignNamePSF);
								call_countcompleted++;

							} else {
								break;
							}

						}

					}

				}

				// return
				// ok(assignCallBasedOnSelectedRange.render(fromDate,toDate,isRangeSelected,toAssigncall,cresList,scheduleData,getUserLoginName(),getDealerName()));
				return redirect(controllers.routes.CallInteractionController.getPageForAssigningCalls());

			} else {
				return forbidden(views.html.error403.render());

			}

		}
	}

	@Secure(clients = "FormClient")
	public Result assigningCallsToCRE(String selectAgent, String fromDate, String toDate) {
		List<String> selectedList = Arrays.asList(selectAgent.split(","));

		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDateNew = new Date();
		Date toDateNew = new Date();
		try {
			fromDateNew = dmyFormat.parse(fromDate);
			toDateNew = dmyFormat.parse(toDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("fromDateNew  is : " + fromDateNew);
		logger.info("toDateNew selected type date :" + toDateNew);
		/*
		 * List<Service>
		 * scheduleData=call_int_repo.assignCallsToUser(fromDateNew,toDateNew);
		 * 
		 * logger.info("scheduleData is : "+scheduleData.size()); int
		 * scheduledCallCount=scheduleData.size();
		 * 
		 * int creSize=selectedList.size(); logger.info(
		 * "the CRE of all managers are count is : "+creSize);
		 * 
		 * if(creSize!=0){ logger.info("count of CRE :" + creSize);
		 * 
		 * int divCountRemainder = scheduledCallCount % creSize;
		 * 
		 * logger.info("divCountRemainder of CRE and call from CRE Manager:" +
		 * divCountRemainder);
		 * 
		 * int divCount = (scheduledCallCount) / (creSize); logger.info(
		 * "divCount of CRE and call :" + divCount);
		 * 
		 * Collections.shuffle(scheduleData);
		 * 
		 * 
		 * int calls_count = 0; for (int i = 0; i < creSize; i++) {
		 * 
		 * for (int j = calls_count; j < divCount + calls_count; j++) {
		 * 
		 * String userdata = selectedList.get(i); Service scall =
		 * scheduleData.get(j);
		 * 
		 * long scallId = scall.getId();
		 * 
		 * call_int_repo.assignCallByManager(userdata,
		 * scallId,getUserLogindealerCode()); }
		 * 
		 * calls_count = divCount + calls_count; if (calls_count >=
		 * scheduledCallCount) { break; }
		 * 
		 * } int call_countcompleted = divCount * creSize; logger.info(
		 * "call_countcompleted : " + call_countcompleted);
		 * 
		 * for (int i = 0; i < creSize; i++) {
		 * 
		 * if (call_countcompleted < scheduledCallCount) {
		 * 
		 * String userdata = selectedList.get(i); Service scall =
		 * scheduleData.get(call_countcompleted);
		 * 
		 * long scallId = scall.getId();
		 * call_int_repo.assignCallByManager(userdata,
		 * scallId,getUserLogindealerCode()); call_countcompleted++;
		 * 
		 * } else { break; }
		 * 
		 * }
		 * 
		 * }
		 * 
		 */

		return ok("done");

	}

	@Secure(clients = "FormClient")
	public Result getAssignedCallsOfUser(String selectAgent) {

		List<AssignedInteraction> assignData = call_int_repo.getAssigendInteractionsOfUser(selectAgent);
		logger.info(" assignData :" + assignData.size());
		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();

		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<WyzUserDataOnTabLoad> wyz_data = new ArrayList<WyzUserDataOnTabLoad>();
		List<AssignedInteractionDataOnTabLoad> assg_data = new ArrayList<AssignedInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();
		for (AssignedInteraction assign : assignData) {

			if (assign.getCustomer() != null) {

				CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
				customer_data.setCustomerName(assign.getCustomer().getCustomerName());
				customer_data.setCustomerCategory(assign.getCustomer().getCustomerCategory());
				List<Phone> listPhone = assign.getCustomer().getPhones();
				for (Phone phoneList : listPhone) {

					if (phoneList.isPreferredPhone) {

						customer_data.setCustomerPhone(phoneList.getPhoneNumber());

					}

				}
				cust_data.add(customer_data);
			} else {
				logger.info("Customer is empty");
			}

			if (assign.getVehicle() != null) {

				VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

				ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
				Service service_data = call_int_repo.getLatestServiceData(assign.getVehicle().getVehicle_id());

				service_data1.setNextServicedate(service_data.getNextServiceDate());
				service_data1.setPsfstatus(service_data.getPsfStatus());
				serv_data.add(service_data1);

				veh_data1.setVehicleRegNo(assign.getVehicle().getVehicleRegNo());
				veh_data1.setModel(assign.getVehicle().getModel());
				veh_data1.setNextServicedate(assign.getVehicle().getNextServicedate());
				veh_data1.setNextServicetype(assign.getVehicle().getNextServicetype());
				veh_data1.setForecastLogic(assign.getVehicle().getForecastLogic());
				veh_data.add(veh_data1);
			} else {
				logger.info("Vehical is empty");
			}

			if (assign.getWyzUser() != null) {

				WyzUserDataOnTabLoad user_data = new WyzUserDataOnTabLoad();
				user_data.setUsername(assign.getWyzUser().getUserName());
				wyz_data.add(user_data);
			} else {
				logger.info("WyzUser is empty");
			}

			AssignedInteractionDataOnTabLoad assign_data = new AssignedInteractionDataOnTabLoad();
			assign_data.setId(assign.getId());

			assg_data.add(assign_data);
		}

		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setWyzUserLoadList(wyz_data);
		call_interaction.setAssignedInteractionLoadList(assg_data);
		call_interaction.setServiceLoadList(serv_data);

		return ok(toJson(call_interaction));

	}

	@Secure(clients = "FormClient")
	public Result searchByCustomer() {
		// List<CallInteraction>
		// dataList=call_int_repo.getSearchList(getUserLoginName(),getUserLogindealerCode());

		// List<Customer>
		// dataList=call_int_repo.getAllCustomers(getUserLogindealerCode());
		return ok(searchCustomerCRE.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(),
				getOEMOfDealer()));
	}
	@Secure(clients = "FormClient")
	public Result searchByCustomerManager(){
		return ok(searchCustomerManager.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(), getUserLoginName()));
	}


	/**
	 *
	 * @return
	 */
	@Secure(clients = "FormClient")
	public Result addCustomerCRE() {

		return ok(addCustomer.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(), getOEMOfDealer()));
	}

	@Secure(clients = "FormClient")
	public Result postAddCustomer() {
		Form<Vehicle> vehicle_data = Form.form(Vehicle.class).bindFromRequest();
		Vehicle vehicle = vehicle_data.get();

		Form<Customer> customer_data = Form.form(Customer.class).bindFromRequest();
		Customer customer = customer_data.get();

		Form<Phone> phone_data = Form.form(Phone.class).bindFromRequest();
		Phone phone = phone_data.get();

		Form<Email> email_data = Form.form(Email.class).bindFromRequest();
		Email email = email_data.get();

		Form<Address> address_data = Form.form(Address.class).bindFromRequest();
		Address address = address_data.get();
		
		if(vehicle.getChassisNo()!=null){
		customer.setDbVehRegNo(vehicle.getChassisNo());
		}
		
		if(vehicle.getVehicleRegNo()!=null){
		customer.setRegNoSMR(vehicle.getVehicleRegNo());
		}

		

		List<String> custAndVehiId = call_int_repo.getNewCustomer(vehicle, customer, phone, email, address,
				getUserLoginName());

		return redirect(controllers.routes.InsuranceController.getCommonCallDispositionForm(
				Long.parseLong(custAndVehiId.get(0)), Long.parseLong(custAndVehiId.get(1)), "service"));
	}

	@Secure(clients = "FormClient")
	public Result getMediaFileCallInteractions(long id) throws IOException {
		id = 20;
		CallInteraction call = call_int_repo.getCallLogsByCRE(id, "TataMotar");
		byte[] bytes = call.getMediaFileLob();
		logger.info("byte data :" + bytes);
		int size = bytes.length;
		response().setContentType("audio/mp3");
		response().setHeader("Content-Disposition", "attachment;filename=media.mp3");
		response().setHeader("Cache-Control", "no-cache");
		return ok(bytes);
	}

	// complaints method
	/**
	 *
	 * @return
	 */
	@Secure(clients = "FormClient")
	public Result addComplaints() {
		
		List<Workshop> listwork=call_int_repo.getWorkshopList();
		
		List<ComplaintSource> sourcelist=repo.getComplaintSourceList();

		return ok(complaints.render(sourcelist,listwork,getOEMOfDealer(), getUserLogindealerCode(), getDealerName(), getUserLoginName()));
	}

	/**
	 *
	 * @return
	 */
	@Secure(clients = "FormClient")
	public Result postComplaints() {

		Form<Complaint> complaints_data = Form.form(Complaint.class).bindFromRequest();
		Complaint complaint = complaints_data.get();

		Form<Vehicle> vehicle_data = Form.form(Vehicle.class).bindFromRequest();
		Vehicle vehicle = vehicle_data.get();

		Form<Phone> phone_data = Form.form(Phone.class).bindFromRequest();
		Phone phone = phone_data.get();

		Form<Email> email_data = Form.form(Email.class).bindFromRequest();
		Email email = email_data.get();

		Form<Address> address_data = Form.form(Address.class).bindFromRequest();
		Address address = address_data.get();

		complaint.setWyzUser(null);
		/// vehicle.set
		call_int_repo.addNewComplaint(complaint, vehicle, phone, email, address, getUserLoginName());

		// MessageTemplete message =
		// repo.getMessageTempleteOfDealerServiceBooked("Complaint",
		// getDealerName());
		//
		// com.typesafe.config.Config configuration =
		// com.typesafe.config.ConfigFactory.load();
		// String smsUrl = configuration.getString("app.smsUrl");
		// sms_repo.SMSTriggerBulkComplaints(getUserLogindealerCode(),
		// complaint.getComplaintNumber(),
		// complaint.getCustomerName(), complaint.getVehicleRegNo(),
		// complaint.getCustomerPhone(), message,
		// smsUrl);
		return redirect(controllers.routes.CallInteractionController.addComplaints());

		// return ok(toJson(complaint));
	}

	/**
	 *
	 * @return
	 */

	@Secure(clients = "FormClient")
	public Result updateComplaintsResolution(String complaintNum, String reasonFor, String complaintStatus,
			String customerStatus, String actionTaken, String resolutionBy) {
		logger.info("post action of resolve complaints");
		call_int_repo.updateComplaintsResolution(complaintNum, reasonFor, complaintStatus, customerStatus, actionTaken,
				resolutionBy);
		return redirect(controllers.routes.CallInteractionController.complaintsResolution());
	}

	@Secure(clients = "FormClient")
	public Result updateComplaintsResolutionClosed(String complaintNumClosed, String reasonForClosed,
			String complaintStatusClosed, String customerStatusClosed, String actionTakenClosed,
			String resolutionByClosed) {
		logger.info("post action of resolve complaints");
		call_int_repo.updateComplaintsResolutionClosed(complaintNumClosed, reasonForClosed, complaintStatusClosed,
				customerStatusClosed, actionTakenClosed, resolutionByClosed);
		return redirect(controllers.routes.CallInteractionController.complaintsResolution());
	}

	@Secure(clients = "FormClient")
	public Result updateComplaintsResolutionByManager(String complaintNum, String reasonFor, String complaintStatus,
			String customerStatus, String actionTaken, String resolutionBy) {
		logger.info("post action of resolve complaints");
		call_int_repo.updateComplaintsResolutionByManager(complaintNum, reasonFor, complaintStatus, customerStatus,
				actionTaken, resolutionBy);
		return redirect(controllers.routes.CallInteractionController.viewAllComplaints1());
	}

	@Secure(clients = "FormClient")
	public Result assignComplaints() {
		List<String> cresList = repo.getCREUserNameofCREManager(getUserLoginName(), getUserLogindealerCode());
		List<Complaint> complaintData = call_int_repo.getAllComplaint();
		logger.info("complaintData not asigned : " + complaintData.size());
		List<Location> locationList = call_int_repo.getLocationList();
		List<Workshop> workshopList = call_int_repo.getWorkshopList();
		return ok(assignComplaints.render(cresList, complaintData, getUserLoginName(), getDealerName(), locationList,
				workshopList));
	}

	@Secure(clients = "FormClient")
	public Result updateComplaints(long id, String comments, String selected_value) {

		call_int_repo.updateComplaintsByCRE(id, comments, selected_value);
		return ok("done");
	}

	// Roster of todays unAvailability update of users

	@Secure(clients = "FormClient")
	public Result getRoasterTable() {
		List<WyzUser> getUsersList = wyzRepo.getAllUsersAvailableToday(getUserLogindealerCode(), getUserLoginName());

		int row_count = getUsersList.size() - 1;
		return ok(roasterTableByCREMan.render(row_count, getUserLogindealerCode(), getDealerName(), getUserLoginName(),
				getUsersList));
	}

	@Secure(clients = "FormClient")
	public Result toUpdateRoasterUnAvailablity() {

		Form<ListingForm> list_data = Form.form(ListingForm.class).bindFromRequest();
		ListingForm listData = list_data.get();
		List<WyzUser> datalist = listData.getUserList();
		logger.info("number of user checked :" + datalist.size());

		String uploadAction = listData.getSelectedFile();

		if (uploadAction == null || uploadAction.length() == 0) {
			return forbidden(views.html.error403.render());
		} else {

			String action = uploadAction;

			logger.info("uploadAction[0] first id value : " + action);
			if ("CRE".equals(action)) {
				String roleIs = "CRE";
				// call_int_repo.resetTheStatus(roleIs);
				call_int_repo.updateTheUnavialbilityList(datalist, roleIs);

				logger.info("from CRE: " + action);

			} else if ("ServiceAdvisor".equals(action)) {
				String roleIs = "Service Advisor";
				// call_int_repo.resetTheStatus(roleIs);
				call_int_repo.updateTheUnavialbilityList(datalist, roleIs);
				logger.info("from ServiceAdvisor : " + action);

			} else if ("Driver".equals(action)) {
				String roleIs = "Driver";
				// call_int_repo.resetTheStatus(roleIs);
				call_int_repo.updateTheUnavialbilityList(datalist, roleIs);
				logger.info("from Driver : " + action);

			} else {

				return forbidden(views.html.error403.render());

			}

		}

		// return ok(toJson(datalist));
		return redirect(controllers.routes.CallInteractionController.getRoasterTable());

	}

	// Roster unAvailabilty range of user as mentioned from date to toDate

	@Secure(clients = "FormClient")
	public Result updateRangeOfUnavailabiltyOfUsers() {

		List<WyzUser> getUsersList = wyzRepo.getAllUsersAvailableToday(getUserLogindealerCode(), getUserLoginName());

		int row_count = getUsersList.size() - 1;
		return ok(rosterOfUnAvailibilityOfUserSelectedRange.render(row_count, getDealerName(), getUserLoginName(),
				getUsersList));
	}

	@Secure(clients = "FormClient")
	public Result postUpdateRangeOfUnavailabiltyOfUsers() {
		logger.info("post updat e range");
		Form<ListingForm> list_data = Form.form(ListingForm.class).bindFromRequest();
		ListingForm listData = list_data.get();
		List<WyzUser> datalist = listData.getUserList();
		List<UnAvailability> unavilabilityList = listData.getUnavaiabiltyList();
		logger.info("number of user checked :" + datalist.size());

		String uploadAction = listData.getSelectedFile();
		logger.info("post updat e range  selected tupe is : " + uploadAction);
		if (uploadAction == null || uploadAction.length() == 0) {
			return forbidden(views.html.error403.render());
		} else {

			String action = uploadAction;

			logger.info("uploadAction[0] first id value : " + action);
			if ("CRE".equals(action)) {

				call_int_repo.updateUnavailabiltyOfUserInRange(datalist, unavilabilityList);
				logger.info("from CRE: " + action);

			} else if ("ServiceAdvisor".equals(action)) {

				call_int_repo.updateUnavailabiltyOfUserInRange(datalist, unavilabilityList);
				logger.info("from ServiceAdvisor : " + action);

			} else if ("Driver".equals(action)) {

				call_int_repo.updateUnavailabiltyOfUserInRange(datalist, unavilabilityList);
				logger.info("from Driver : " + action);

			} else {

				return forbidden(views.html.error403.render());

			}

		}

		return redirect(controllers.routes.CallInteractionController.updateRangeOfUnavailabiltyOfUsers());
	}

	@Secure(clients = "FormClient")
	public Result toChangeAssigment() {

		List<WyzUser> notAvailableUserFrom = wyzRepo.getAllUsersNotAvailableToday(getUserLogindealerCode(),
				getUserLoginName());

		List<WyzUser> notAvailableUserFromCRE = new ArrayList<WyzUser>();
		for (WyzUser user_data : notAvailableUserFrom) {

			if (user_data.getRole().equals("CRE")) {

				notAvailableUserFromCRE.add(user_data);

			}

		}

		List<WyzUser> availableUserFromCRE = new ArrayList<WyzUser>();
		List<WyzUser> availableUsersTo = wyzRepo.getAllUsersAvailableToday(getUserLogindealerCode(),
				getUserLoginName());
		for (WyzUser user_data : availableUsersTo) {

			if (user_data.getRole().equals("CRE")) {

				availableUserFromCRE.add(user_data);

			}

		}
		// List<String>
		// fromTransfer=call_int_repo.getCRENotAvailable(getUserLogindealerCode(),
		// getUserLoginName());
		return ok(changeAssignmentByCREManager.render(notAvailableUserFromCRE, availableUserFromCRE, getDealerName(),
				getUserLoginName()));

	}

	@Secure(clients = "FormClient")
	public Result getReAssignmentCalls(String selectAgent) {
		logger.info("selectAgent for reassugnemnet " + selectAgent);
		List<String> selectedList = Arrays.asList(selectAgent.split(","));

		logger.info("selectedList for size " + selectedList.size());
		List<AssignedInteraction> listAssign = call_int_repo.getAssignedIntListForReAss(selectedList);
		logger.info("listAssign for reassugnemnet " + listAssign.size());
		// List<CallInteraction>
		// listTodaysFollowUp=call_int_repo.getFollowUpCallsOfUsersList(selectedList,getUserLogindealerCode());
		// logger.info("listTodaysFollowUp for reassugnemnet
		// "+listTodaysFollowUp.size());
		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();

		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<WyzUserDataOnTabLoad> wyz_data = new ArrayList<WyzUserDataOnTabLoad>();
		List<AssignedInteractionDataOnTabLoad> assg_data = new ArrayList<AssignedInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();
		List<SRDispositionDataOnTabLoad> sRD_data = new ArrayList<SRDispositionDataOnTabLoad>();
		List<CallInteractionDataOnTabLoad> int_data = new ArrayList<CallInteractionDataOnTabLoad>();

		for (AssignedInteraction assign : listAssign) {

			if (assign.getCustomer() != null) {

				CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
				customer_data.setCustomerName(assign.getCustomer().getCustomerName());
				customer_data.setCustomerCategory(assign.getCustomer().getCustomerCategory());
				customer_data.setCustomerPhone(assign.getCustomer().getPreferredPhone().getPhoneNumber());

				cust_data.add(customer_data);
			} else {
				logger.info("Customer is empty");
			}

			if (assign.getVehicle() != null) {

				VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

				ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
				Service service_data = call_int_repo.getLatestServiceData(assign.getVehicle().getVehicle_id());

				service_data1.setNextServicedate(service_data.getNextServiceDate());
				service_data1.setSaleDate(service_data.getSaleDate());
				serv_data.add(service_data1);

				veh_data1.setVehicleRegNo(assign.getVehicle().getVehicleRegNo());
				veh_data1.setModel(assign.getVehicle().getModel());
				veh_data1.setNextServicedate(assign.getVehicle().getNextServicedate());

				veh_data.add(veh_data1);
			} else {
				logger.info("Vehical is empty");
			}

			if (assign.getWyzUser() != null) {

				WyzUserDataOnTabLoad user_data = new WyzUserDataOnTabLoad();
				user_data.setUsername(assign.getWyzUser().getUserName());
				wyz_data.add(user_data);
			} else {
				logger.info("WyzUser is empty");
			}

		}

		// for follow up

		// for(CallInteraction addToAjaxData:listTodaysFollowUp){
		//
		// if (addToAjaxData.getCustomer() != null) {
		//
		// CustomerDataOnTabLoad customer_data=new CustomerDataOnTabLoad();
		// customer_data.setCustomerName(addToAjaxData.getCustomer().getCustomerName());
		// customer_data.setCustomerCategory(addToAjaxData.getCustomer().getCustomerCategory());
		// customer_data.setCustomerPhone(addToAjaxData.getCustomer().getPreferredPhone().getPhoneNumber());
		// cust_data.add(customer_data);
		// } else {
		// logger.info("Customer is empty");
		// }
		//
		// if (addToAjaxData.getVehicle() != null) {
		//
		// VehicleDataOnTabLoad veh_data1=new VehicleDataOnTabLoad();
		//
		// ServiceDataOnTabLoad service_data1=new ServiceDataOnTabLoad();
		// Service service_data =
		// call_int_repo.getLatestServiceData(addToAjaxData.getVehicle().getVehicle_id());
		//
		// service_data1.setNextServicedate(service_data.getNextServiceDate());
		// service_data1.setSaleDate(service_data.getSaleDate());
		// serv_data.add(service_data1);
		//
		// veh_data1.setVehicleRegNo(addToAjaxData.getVehicle().getVehicleRegNo());
		// veh_data1.setModel(addToAjaxData.getVehicle().getModel());
		// veh_data1.setNextServicedate(addToAjaxData.getVehicle().getNextServicedate());
		//
		// veh_data.add(veh_data1);
		// } else {
		// logger.info("Vehical is empty");
		// }
		//
		// if(addToAjaxData.getSrdisposition()!=null){
		// SRDispositionDataOnTabLoad sr_disposition_data=new
		// SRDispositionDataOnTabLoad();
		// sr_disposition_data.setCallDisposition(addToAjaxData.getSrdisposition().getCallDispositionData().getDisposition());
		// sr_disposition_data.setFollowUpDate(addToAjaxData.getSrdisposition().getFollowUpDate());
		// sr_disposition_data.setFollowUpTime(addToAjaxData.getSrdisposition().getFollowUpTime());
		// sRD_data.add(sr_disposition_data);
		// }else{
		// logger.info("srDisposition is empty");
		// }
		//
		// if(addToAjaxData.getServiceBooked()!=null){
		// ServicebookedDataOnTabLoad servicebooked_data=new
		// ServicebookedDataOnTabLoad();
		//
		// servicebooked_data.setServiceScheduledDate(addToAjaxData.getServiceBooked().getServiceScheduledDateStr());
		// servicebooked_data.setServiceScheduledTime(addToAjaxData.getServiceBooked().getServiceScheduledTimeStr());
		//
		// }else{
		// logger.info("ServiceBooked is empty");
		// }
		//
		// if (addToAjaxData.getWyzUser() != null) {
		//
		//
		// WyzUserDataOnTabLoad user_data=new WyzUserDataOnTabLoad();
		// user_data.setUsername(addToAjaxData.getWyzUser().getUserName());
		// wyz_data.add(user_data);
		// } else {
		// logger.info("WyzUser is empty");
		// }
		//
		//
		// CallInteractionDataOnTabLoad call_data_int=new
		// CallInteractionDataOnTabLoad();
		// call_data_int.setId(addToAjaxData.getId());
		// call_data_int.setDealerCode(addToAjaxData.getDealerCode());
		// int_data.add(call_data_int);
		//
		// }
		logger.info("wyz_data size :" + wyz_data.size());
		logger.info("cust_data size :" + cust_data.size());
		logger.info("veh_data size :" + veh_data.size());
		logger.info("assg_data size :" + assg_data.size());
		logger.info("serv_data size :" + serv_data.size());

		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setWyzUserLoadList(wyz_data);
		// call_interaction.setAssignedInteractionLoadList(assg_data);
		call_interaction.setServiceLoadList(serv_data);
		// call_interaction.setInteractionCallList(int_data);
		// call_interaction.setSrdispositionList(sRD_data);

		return ok(toJson(call_interaction));

	}

	@Secure(clients = "FormClient")
	public Result reAssigningCallsofselectCRE() {

		Form<ListingForm> list_data = Form.form(ListingForm.class).bindFromRequest();
		ListingForm listData = list_data.get();
		List<String> fromlist = listData.getSelectedValues();
		List<String> tolist = listData.getData();
		List<AssignedInteraction> listAssign = call_int_repo.getAssignedIntListForReAss(fromlist);
		logger.info("listAssign for reassugnemnet " + listAssign.size());
		// List<CallInteraction>
		// listTodaysFollowUp=call_int_repo.getFollowUpCallsOfUsersList(fromlist,getUserLogindealerCode());
		// logger.info("listTodaysFollowUp for reassugnemnet
		// "+listTodaysFollowUp.size());

		logger.info("scheduleData is : " + listAssign.size());
		int scheduledCallCount = listAssign.size();
		// int callIntCallCount=listTodaysFollowUp.size();

		int creSize = tolist.size();
		logger.info("the CRE of all managers are count is : " + creSize);

		if (creSize != 0) {
			if (scheduledCallCount != 0) {
				logger.info("count of CRE :" + creSize);

				int divCountRemainder = scheduledCallCount % creSize;

				logger.info("divCountRemainder of CRE and call from CRE Manager:" + divCountRemainder);

				int divCount = (scheduledCallCount) / (creSize);
				logger.info("divCount of CRE and call :" + divCount);

				Collections.shuffle(listAssign);

				int calls_count = 0;
				for (int i = 0; i < creSize; i++) {

					for (int j = calls_count; j < divCount + calls_count; j++) {

						String userdata = tolist.get(i);
						AssignedInteraction scall = listAssign.get(j);

						long scallId = scall.getId();

						call_int_repo.changeAssignmentCallByManager(userdata, scallId, getUserLogindealerCode());
					}

					calls_count = divCount + calls_count;
					if (calls_count >= scheduledCallCount) {
						break;
					}

				}
				int call_countcompleted = divCount * creSize;
				logger.info("call_countcompleted : " + call_countcompleted);

				for (int i = 0; i < creSize; i++) {

					if (call_countcompleted < scheduledCallCount) {

						String userdata = tolist.get(i);
						AssignedInteraction scall = listAssign.get(call_countcompleted);

						long scallId = scall.getId();
						call_int_repo.changeAssignmentCallByManager(userdata, scallId, getUserLogindealerCode());
						call_countcompleted++;

					} else {
						break;
					}

				}
			}
			// if(callIntCallCount!=0){
			//
			// logger.info("count of CRE :" + creSize);
			//
			// int divCountRemainder = callIntCallCount % creSize;
			//
			// logger.info("divCountRemainder of CRE and call from CRE Manager:"
			// + divCountRemainder);
			//
			// int divCount = (callIntCallCount) / (creSize);
			// logger.info("divCount of CRE and call :" + divCount);
			//
			// Collections.shuffle(listAssign);
			//
			//
			// int calls_count = 0;
			// for (int i = 0; i < creSize; i++) {
			//
			// for (int j = calls_count; j < divCount + calls_count; j++) {
			//
			// String userdata = tolist.get(i);
			// CallInteraction scall = listTodaysFollowUp.get(j);
			//
			// long scallId = scall.getId();
			//
			// call_int_repo.changeAssignCallFollowUp(userdata,
			// scallId,getUserLogindealerCode());
			// }
			//
			// calls_count = divCount + calls_count;
			// if (calls_count >= scheduledCallCount) {
			// break;
			// }
			//
			// }
			// int call_countcompleted = divCount * creSize;
			// logger.info("call_countcompleted : " + call_countcompleted);
			//
			// for (int i = 0; i < creSize; i++) {
			//
			// if (call_countcompleted < scheduledCallCount) {
			//
			// String userdata = tolist.get(i);
			// CallInteraction scall =
			// listTodaysFollowUp.get(call_countcompleted);
			//
			// long scallId = scall.getId();
			// call_int_repo.changeAssignCallFollowUp(userdata,
			// scallId,getUserLogindealerCode());
			// call_countcompleted++;
			//
			// } else {
			// break;
			// }
			//
			// }
			//
			//
			//
			//
			//
			// }
		}

		return redirect(controllers.routes.CallInteractionController.toChangeAssigment());

	}

	@Secure(clients = "FormClient")
	public Result upload_file_Format_show() {
		return ok(dataUploadFormatForm.render(getUserLoginName(), getDealerName()));
	}

	@Secure(clients = "FormClient")
	public Result ajax_master_data_upload_fileFormat(String selected_part) {
		logger.info("selected: " + selected_part);
		ArrayList<String> strColumn_Namelist = new ArrayList<String>();
		ArrayList<String> strColumn_Typelist = new ArrayList<String>();
		String strColumn_Name = null;
		String strColumn_Type = null;
		switch (selected_part) {

		case "sale_register":

			Field[] fieldstaxvalue = SaleRegisterExcel.class.getFields();
			for (Field data : fieldstaxvalue) {
				strColumn_Name = data.getName();
				strColumn_Type = data.getGenericType().getTypeName();
				strColumn_Namelist.add(strColumn_Name);
				strColumn_Typelist.add(strColumn_Type);
				logger.info(" strColumn_Name :" + strColumn_Name);
			}
			break;

		case "job_card":
			Field[] fields = JobCardStatus.class.getFields();

			for (Field data : fields) {
				strColumn_Name = data.getName();
				strColumn_Type = data.getGenericType().getTypeName();

				strColumn_Namelist.add(strColumn_Name);
				strColumn_Typelist.add(strColumn_Type);

				logger.info(" strColumn_Name :" + strColumn_Name);
			}
			break;

		case "bill":

			Field[] fieldstaxvaluecosting = WorkBillRegister.class.getFields();
			for (Field data : fieldstaxvaluecosting) {
				strColumn_Name = data.getName();
				strColumn_Type = data.getGenericType().getTypeName();
				strColumn_Namelist.add(strColumn_Name);
				strColumn_Typelist.add(strColumn_Type);
				logger.info(" strColumn_Name :" + strColumn_Name);
			}
			break;

		case "campaign":

			Field[] fieldstaxvaluecamp = CampaignExcel.class.getFields();
			for (Field data : fieldstaxvaluecamp) {
				strColumn_Name = data.getName();
				strColumn_Type = data.getGenericType().getTypeName();
				strColumn_Namelist.add(strColumn_Name);
				strColumn_Typelist.add(strColumn_Type);
				logger.info(" strColumn_Name :" + strColumn_Name);
			}
			break;

		case "SMR":

			Field[] fieldstaxvaluesmr = SMRData.class.getFields();
			for (Field data : fieldstaxvaluesmr) {
				strColumn_Name = data.getName();
				strColumn_Type = data.getGenericType().getTypeName();
				strColumn_Namelist.add(strColumn_Name);
				strColumn_Typelist.add(strColumn_Type);
				logger.info(" strColumn_Name :" + strColumn_Name);
			}
			break;

		}
		ArrayList<ArrayList<String>> parameter_propertires1 = new ArrayList<ArrayList<String>>();
		parameter_propertires1.add(strColumn_Namelist);
		parameter_propertires1.add(strColumn_Typelist);
		return ok(toJson(parameter_propertires1));
	}

	@Secure(clients = "FormClient")
	public Result upload_file_Format_submit() {
		Form<UploadMasterFormat> form = Form.form(UploadMasterFormat.class).bindFromRequest();
		Form<ListUploadMasterAlias> formList = Form.form(ListUploadMasterAlias.class).bindFromRequest();
		UploadMasterFormat format_data = form.get();
		ListUploadMasterAlias uploadmasterformat_data_list = formList.get();
		List<UploadMasterFormat> list_format = uploadmasterformat_data_list.getUpload_sheet_data();
		// logger.info("size of listUpload "+list_format.size());
		for (UploadMasterFormat uploadformat_data1 : list_format) {
			uploadformat_data1.setUpload_format_name(format_data.getUpload_format_name());
			// logger.info(" form "+uploadformat_data1.getUpload_format_name);
			call_int_repo.addUpload_Format_data(uploadformat_data1);
		}
		// List<String> org_name_list = repo_org.list_org_name();
		return ok(dataUploadFormatForm.render(getUserLoginName(), getDealerName()));
		// return ok(toJson(list_format));
	}

	@Secure(clients = "FormClient")
	public Result upload_Excel_Sheet(String uploadId, String uploadType, String resultDataCount) {
		List<Location> locationList = call_int_repo.getLocationList();
		List<Campaign> campaignList = call_int_repo.getCampaignNames();
		List<Integer> resultdata = new ArrayList();
		if (resultDataCount.equals("0")) {

		} else {

			List<String> selectedList = Arrays.asList(resultDataCount.split(","));
			resultdata.add(Integer.parseInt(selectedList.get(0)));
			resultdata.add(Integer.parseInt(selectedList.get(1)));

		}

		logger.info("campaign names list:" + campaignList.size());
		return ok(uploadMasterFilesForm.render(resultdata, uploadId, uploadType, campaignList, locationList,
				getUserLogindealerCode(), getUserLoginName()));
	}

	// DOWNLOAD EXCEL FORMATE

	@Secure(clients = "FormClient")
	public Result getDownloadExcelFormat() {

		return ok(downloadUploadExcelFormat.render(getUserLoginName(), getDealerName()));

	}

	@Secure(clients = "FormClient")
	public Result getExcelColumnsOFUploadFormat(String upload_format) {

		List<String> dataExcelColumns = call_int_repo.getExcelColumnsOfSelectedFormate(upload_format);

		return ok(toJson(dataExcelColumns));
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result ajax_transaction_data_upload(String selected_format) {
		JsonNode json_data = request().body().asJson();
		String jsonString = String.valueOf(json_data);
		logger.info("json_data: " + jsonString);
		ObjectMapper mapper = new ObjectMapper();
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		mapper.setDateFormat(df);
		if (json_data != null) {
			try {
				logger.info("selected_format is :" + selected_format);
				switch (selected_format) {
				case "new_customers":
					List<NewCustomer> cust_data = mapper.readValue(jsonString,
							TypeFactory.defaultInstance().constructCollectionType(List.class, NewCustomer.class));

					for (NewCustomer newCust_modified : cust_data) {
						logger.info("customer name :" + newCust_modified.getServiceType());
						call_int_repo.addNewCustomerData(newCust_modified);
					}
					break;
				case "job_card":
					List<JobCardDetailsUpload> job_data = mapper.readValue(jsonString, TypeFactory.defaultInstance()
							.constructCollectionType(List.class, JobCardDetailsUpload.class));

					for (JobCardDetailsUpload job_card_details : job_data) {
						logger.info("job Card" + job_card_details.getJobCardNumber());
						call_int_repo.addJobCardData(job_card_details);
					}
					Connection connection = null;
					CallableStatement callableStatement = null;
					try {
						DataSource datasource = DB.getDataSource();
						logger.info("the datasource is : " + datasource);
						connection = datasource.getConnection();

						String getDBUSERByUserIdSql = "{call forecastLogic()}";
						callableStatement = connection.prepareCall(getDBUSERByUserIdSql);
						callableStatement.executeUpdate();

					} catch (Exception ex) {

						ex.printStackTrace();

					} finally {

						try {
							if (callableStatement != null) {
								callableStatement.close();
								logger.info("callableStatement Statement closed");
							}
						} catch (Exception e) {
						}
						;
						try {
							if (connection != null) {
								if (!connection.isClosed()) {
									connection.close();
									logger.info("connection is closed");
								}
							}
						} catch (Exception e) {
						}
						;
					}

					break;

				case "bill":
					List<workshopBill> serv_data = mapper.readValue(jsonString,
							TypeFactory.defaultInstance().constructCollectionType(List.class, workshopBill.class));
					MessageTemplete message = repo.getMessageTempleteOfDealerServiceBooked("Service Due",
							getUserLogindealerCode());
					com.typesafe.config.Config configuration = com.typesafe.config.ConfigFactory.load();
					String smsUrl = configuration.getString("app.smsUrl");
					for (workshopBill serviceData : serv_data) {
						logger.info("workshop bill for loop");
						call_int_repo.addBillData(serviceData);
						Service veh_cal = call_int_repo.getVehicalByJobCard(serviceData.getJobCardNumber());
						logger.info("inside workshop bill loop" + veh_cal);
						// send an sms when bill is uploaded
						// sms_repo.SMSTriggerBulk(getUserLogindealerCode(),serviceData.getVehicle().getVehicle_id(),message,smsUrl);
						sms_repo.SMSTriggerBulk(getUserLogindealerCode(), veh_cal.getVehicle().getVehicle_id(), message,
								smsUrl);
					}
					break;

				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			;
		}

		return ok(toJson("uploded"));

	}

	@Secure(clients = "FormClient")
	public Result get_required_fields(String upload_master) {
		// logger.info("page selected: "+upload_master);
		PreparedStatement cs = null;
		ResultSet rs = null;
		Connection connection = null;
		List<UploadMasterFormat> upload_mater_fields = new ArrayList<UploadMasterFormat>();
		try {
			DataSource datasource = DB.getDataSource();
			connection = datasource.getConnection();
			cs = connection.prepareStatement(
					"select model_column,excel_column from UploadMasterFormat where upload_format_name=? ; ");
			cs.setString(1, upload_master);
			rs = cs.executeQuery();
			DecimalFormat df = new DecimalFormat("###.###");

			while (rs.next()) {

				// logger.info("model_column: "+rs.getString(1));
				UploadMasterFormat upload_data = new UploadMasterFormat();
				upload_data.setModel_column(rs.getString(1));
				upload_data.setExcel_column(rs.getString(2));
				upload_mater_fields.add(upload_data);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			inside_Finally_comment(cs, rs, connection);
		}
		return ok(toJson(upload_mater_fields));

	}

	private void inside_Finally_comment(PreparedStatement cs, ResultSet rs, Connection connection) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
		}
		;
		try {
			if (cs != null) {
				cs.close();
			}
		} catch (Exception e) {
		}
		;
		try {
			if (connection != null) {
				if (!connection.isClosed()) {
					connection.close();
				}
			}
		} catch (Exception e) {
		}
		;
	}

	@Secure(clients = "FormClient")
	public Result ajaxAddPhoneNumber(String phone_number, Long customer_id) {
		logger.info("phone_number adding:" + phone_number);
		call_int_repo.addNewPhoneByCustomer(customer_id, phone_number);

		// call_int_repo.getPhoneDetails(customer_id,phone_number);

		return ok("Done");
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result ajaxCallForAddcustomerinfo() throws IOException {

		JsonNode json_data = request().body().asJson();
		String jsonString = String.valueOf(json_data);
		logger.info("json_data: " + jsonString);
		ObjectMapper mapper = new ObjectMapper();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		mapper.setDateFormat(df);

		CustomerJson customer_detailed = mapper.readValue(jsonString, CustomerJson.class);
		call_int_repo.addCustomerAdditionalInfo(customer_detailed);

		return ok(toJson("added"));
	}

	@Secure(clients = "FormClient")
	public Result ajaxAddChassisno(String chassisNo, Long customer_id) {
		call_int_repo.getChassisNo(customer_id, chassisNo);
		return ok("Done");
	}

	@Secure(clients = "FormClient")
	public Result ajaxAddEngineno(String engineNo, Long customer_id) {
		call_int_repo.getEngineNo(customer_id, engineNo);
		return ok("Done");
	}

	@Secure(clients = "FormClient")
	public Result ajaxAddRegistrationno(String vehicalRegNo, Long customer_id) {
		call_int_repo.getVehicalRegNo(customer_id, vehicalRegNo);
		return ok("Done");
	}

	@Secure(clients = "FormClient")
	public Result getWorkShopServiceBooked(long workId, long userId) {

		List<ServiceBooked> services_booked = call_int_repo.getServiceBookedByUserforThisWorkShop(workId, userId);

		CallLogsOnLoadTabData list_date = new CallLogsOnLoadTabData();

		List<ServiceBooked> serv_list = new ArrayList<ServiceBooked>();
		List<Workshop> work_list = new ArrayList<Workshop>();
		List<WyzUser> user_list = new ArrayList<WyzUser>();
		for (ServiceBooked servData : services_booked) {

			if (servData.getWorkshop() != null) {
				servData.getWorkshop().setBays(null);
				servData.getWorkshop().setServicesBooked(null);
				// servData.getWorkshop().setUsers(null);
				servData.getWorkshop().setLocation(null);
				servData.getWorkshop().setWorkshopSummarys(null);

				work_list.add(servData.getWorkshop());

			} else {
				logger.info("Customer is empty");
			}
			servData.setCallInteraction(null);
			servData.setCustomer(null);
			servData.setWorkshop(null);
			servData.setServiceAdvisor(null);
			servData.setPickupDrop(null);
			serv_list.add(servData);

		}

		list_date.setWorkshopList(work_list);
		list_date.setServiceBookedList(serv_list);
		list_date.setWyzUserList(user_list);

		return ok(toJson(list_date));

	}

	@Secure(clients = "FormClient")
	public Result getSerAdvServiceBooked(long serviceAdvisorId, long userId) {

		List<ServiceBooked> services_booked = call_int_repo.getServiceBookedBySA(serviceAdvisorId, userId);
		// logger.info("service booked for this
		// advisor"+services_booked.size());
		CallLogsOnLoadTabData sa_data = new CallLogsOnLoadTabData();

		List<ServiceBooked> servBooked_list = new ArrayList<ServiceBooked>();
		List<Workshop> work_list = new ArrayList<Workshop>();
		List<ServiceAdvisor> serviceAdv_list = new ArrayList<ServiceAdvisor>();
		List<WyzUser> user_list = new ArrayList<WyzUser>();

		for (ServiceBooked servData : services_booked) {

			/*if (servData.getCallInteraction() != null) {

				servData.getCallInteraction().getWyzUser().setCallInteraction(null);

				servData.getCallInteraction().getWyzUser().setComplaints(null);
				// servData.getCallInteraction().getWyzUser().setWorkshop(null);
				servData.getCallInteraction().getWyzUser().setAssignedInteraction(null);
				servData.getCallInteraction().getWyzUser().setAvailabilities(null);
				servData.getCallInteraction().getWyzUser().setUnAvailabilities(null);

				user_list.add(servData.getCallInteraction().getWyzUser());

			}else {
				logger.info("WyzUser is empty");
			}*/

			if (servData.getServiceAdvisor() != null) {
				servData.getServiceAdvisor().setServicebooked(null);
				servData.getServiceAdvisor().setService(null);
				serviceAdv_list.add(servData.getServiceAdvisor());

				servData.getWorkshop().setBays(null);
				// servData.getWorkshop().setUsers(null);
				servData.getWorkshop().setServicesBooked(null);
				servData.getWorkshop().setLocation(null);
				servData.getWorkshop().setWorkshopSummarys(null);

				work_list.add(servData.getWorkshop());
			}

			else {

			}

			servData.setCallInteraction(null);
			servData.setCustomer(null);
			servData.setWorkshop(null);
			servData.setServiceAdvisor(null);
			servData.setPickupDrop(null);

			servBooked_list.add(servData);

		}

		logger.info("servBooked_list" + servBooked_list.size() + "user_list" + user_list.size() + "serviceAdv_list"
				+ serviceAdv_list.size());

		sa_data.setServiceBookedList(servBooked_list);
		sa_data.setWyzUserList(user_list);
		sa_data.setServiceAdvisorList(serviceAdv_list);
		sa_data.setWorkshopList(work_list);

		return ok(toJson(sa_data));
		// return ok("done");
	}

	@Secure(clients = "FormClient")
	public Result getDriverServiceBooked(long driverId, long userId) {
		List<PickupDrop> services_booked_list = call_int_repo.getServiceBookedForDriver(driverId, userId);
		CallLogsOnLoadTabData veh_data = new CallLogsOnLoadTabData();

		List<ServiceBooked> servBooked_list = new ArrayList<ServiceBooked>();
		// List<WyzUser> user_list=new ArrayList<WyzUser>();
		List<PickupDrop> pickup_list = new ArrayList<PickupDrop>();
		List<Driver> driver_list = new ArrayList<Driver>();

		logger.info("services_booked" + services_booked_list.size());
		logger.info("driver_list" + driver_list.size());

		for (PickupDrop pickData : services_booked_list) {

			if (pickData.getDriver() != null) {
				pickData.getDriver().setServicesBooked(null);
				driver_list.add(pickData.getDriver());

				logger.info("driver_list" + driver_list.size());
			} else {
				logger.info("Driver is empty");

			}

			if (pickData.getServiceBooked() != null) {

				pickData.getServiceBooked().setCallInteraction(null);
				pickData.getServiceBooked().setCustomer(null);
				pickData.getServiceBooked().setServiceAdvisor(null);
				pickData.getServiceBooked().setWorkshop(null);
				pickData.getServiceBooked().setPickupDrop(null);

				servBooked_list.add(pickData.getServiceBooked());

				logger.info("servBooked_list" + servBooked_list.size());

			} else {

				logger.info("driver is empty");
			}
			pickData.setDriver(null);
			pickData.setServiceBooked(null);
			pickup_list.add(pickData);

			logger.info("pickup_list :" + pickup_list.size() + "servBooked_list" + servBooked_list.size()
					+ "driver_list :" + driver_list.size());

			veh_data.setPickupDropList(pickup_list);
			veh_data.setServiceBookedList(servBooked_list);
			veh_data.setDriverList(driver_list);

		}
		return ok(toJson(veh_data));

	}
	// Call log on load For CREManager disposition bucket For CREManager

	@Secure(clients = "FormClient")
	public Result getAssignedCallsOfCREManager(String selectedAgentList) {
		List<String> selectedList = Arrays.asList(selectedAgentList.split(","));

		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();
		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<WyzUserDataOnTabLoad> wyz_data = new ArrayList<WyzUserDataOnTabLoad>();
		List<AssignedInteractionDataOnTabLoad> assg_data = new ArrayList<AssignedInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();
		for (String selectAgent : selectedList) {
			// logger.info("selected Agent in CREMAnager :"+selectAgent);
			List<AssignedInteraction> assignData = call_int_repo.getAssigendInteractionsOfUser(selectAgent);
			// logger.info(" assignData :" + assignData.size());

			for (AssignedInteraction assign : assignData) {

				if (assign.getCustomer() != null) {

					CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
					customer_data.setCustomerName(assign.getCustomer().getCustomerName());
					customer_data.setCustomerCategory(assign.getCustomer().getCustomerCategory());
					List<Phone> listPhone = assign.getCustomer().getPhones();
					for (Phone phoneList : listPhone) {

						if (phoneList.isPreferredPhone) {

							customer_data.setCustomerPhone(phoneList.getPhoneNumber());

						}

					}
					cust_data.add(customer_data);
				} else {
					logger.info("Customer is empty");
				}

				if (assign.getVehicle() != null) {

					VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

					ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
					Service service_data = call_int_repo.getLatestServiceData(assign.getVehicle().getVehicle_id());

					service_data1.setNextServicedate(service_data.getNextServiceDate());

					serv_data.add(service_data1);

					veh_data1.setVehicleRegNo(assign.getVehicle().getVehicleRegNo());
					veh_data1.setModel(assign.getVehicle().getModel());
					veh_data1.setNextServicedate(assign.getVehicle().getNextServicedate());

					veh_data.add(veh_data1);
				} else {
					logger.info("Vehical is empty");
				}

				if (assign.getWyzUser() != null) {

					WyzUserDataOnTabLoad user_data = new WyzUserDataOnTabLoad();
					user_data.setUsername(assign.getWyzUser().getUserName());
					wyz_data.add(user_data);
				} else {
					logger.info("WyzUser is empty");
				}

				AssignedInteractionDataOnTabLoad assign_data = new AssignedInteractionDataOnTabLoad();
				assign_data.setId(assign.getId());

				assg_data.add(assign_data);
			}

		}
		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setWyzUserLoadList(wyz_data);
		call_interaction.setAssignedInteractionLoadList(assg_data);
		call_interaction.setServiceLoadList(serv_data);

		return ok(toJson(call_interaction));
	}

	@Secure(clients = "FormClient")
	public Result getFollowUpRequiredDataCREMan(String selectedAgentList) {

		List<String> selectedList = Arrays.asList(selectedAgentList.split(","));
		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();

		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<SRDispositionDataOnTabLoad> sRD_data = new ArrayList<SRDispositionDataOnTabLoad>();
		List<CallInteractionDataOnTabLoad> int_data = new ArrayList<CallInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();
		List<WyzUserDataOnTabLoad> wyz_data = new ArrayList<WyzUserDataOnTabLoad>();
		for (String selectAgent : selectedList) {

			// logger.info("selected Agent in CREMAnager for
			// followup:"+selectAgent);

			List<CallInteraction> followUpCalls = call_int_repo.getFollowUpCallsOfUser(selectAgent,
					getUserLogindealerCode());
			// logger.info("size of follow up calls :"+followUpCalls.size() );

			for (CallInteraction addToAjaxData : followUpCalls) {

				if (addToAjaxData.getCustomer() != null) {

					CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
					customer_data.setCustomerName(addToAjaxData.getCustomer().getCustomerName());
					customer_data.setCustomerCategory(addToAjaxData.getCustomer().getCustomerCategory());
					List<Phone> listPhone = addToAjaxData.getCustomer().getPhones();
					for (Phone phoneList : listPhone) {

						if (phoneList.isPreferredPhone) {

							customer_data.setCustomerPhone(phoneList.getPhoneNumber());

						}

					}
					cust_data.add(customer_data);
				} else {
					logger.info("Customer is empty");
				}

				if (addToAjaxData.getVehicle() != null) {

					VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

					ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
					Service service_data = call_int_repo
							.getLatestServiceData(addToAjaxData.getVehicle().getVehicle_id());

					service_data1.setNextServicedate(service_data.getNextServiceDate());

					serv_data.add(service_data1);

					veh_data1.setVehicleRegNo(addToAjaxData.getVehicle().getVehicleRegNo());
					veh_data1.setModel(addToAjaxData.getVehicle().getModel());
					veh_data1.setNextServicedate(addToAjaxData.getVehicle().getNextServicedate());

					veh_data.add(veh_data1);
				} else {
					logger.info("Vehical is empty");
				}
				if (addToAjaxData.getSrdisposition() != null) {
					SRDispositionDataOnTabLoad sr_disposition_data = new SRDispositionDataOnTabLoad();
					sr_disposition_data.setCallDisposition(
							addToAjaxData.getSrdisposition().getCallDispositionData().getDisposition());
					sr_disposition_data.setFollowUpDate(addToAjaxData.getSrdisposition().getFollowUpDate());
					sr_disposition_data.setFollowUpTime(addToAjaxData.getSrdisposition().getFollowUpTime());
					sRD_data.add(sr_disposition_data);
				} else {
					logger.info("srDisposition is empty");
				}
				if (addToAjaxData.getWyzUser() != null) {

					WyzUserDataOnTabLoad user_data = new WyzUserDataOnTabLoad();
					user_data.setUsername(addToAjaxData.getWyzUser().getUserName());
					wyz_data.add(user_data);
				} else {
					// logger.info("WyzUser is empty");
				}

				CallInteractionDataOnTabLoad call_data_int = new CallInteractionDataOnTabLoad();
				call_data_int.setId(addToAjaxData.getId());
				call_data_int.setDealerCode(addToAjaxData.getDealerCode());
				int_data.add(call_data_int);

			}
		}

		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setSrdispositionLoadList(sRD_data);
		call_interaction.setCallInteractionLoadList(int_data);
		call_interaction.setServiceLoadList(serv_data);
		call_interaction.setWyzUserLoadList(wyz_data);
		return ok(toJson(call_interaction));
	}

	@Secure(clients = "FormClient")
	public Result getServiceBookedDataCREMan(String selectedAgentList) {

		List<String> selectedList = Arrays.asList(selectedAgentList.split(","));
		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();

		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<SRDispositionDataOnTabLoad> sRD_data = new ArrayList<SRDispositionDataOnTabLoad>();
		List<CallInteractionDataOnTabLoad> int_data = new ArrayList<CallInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();
		List<ServicebookedDataOnTabLoad> servbooked_data = new ArrayList<ServicebookedDataOnTabLoad>();
		List<WyzUserDataOnTabLoad> wyz_data = new ArrayList<WyzUserDataOnTabLoad>();
		// logger.info("Data enetered in serviceBookedCalls");
		for (String selectAgent : selectedList) {

			// logger.info("selected Agent in CREMAnager for service
			// booked:"+selectAgent);

			List<CallInteraction> serviceBookedCalls = call_int_repo.getServicBookedCalls(selectAgent,
					getUserLogindealerCode());
			// logger.info("serviceBookedCalls :"+serviceBookedCalls.size());

			for (CallInteraction assign : serviceBookedCalls) {

				if (assign.getCustomer() != null) {

					CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
					customer_data.setCustomerName(assign.getCustomer().getCustomerName());
					customer_data.setCustomerCategory(assign.getCustomer().getCustomerCategory());
					List<Phone> listPhone = assign.getCustomer().getPhones();
					for (Phone phoneList : listPhone) {

						if (phoneList.isPreferredPhone) {

							customer_data.setCustomerPhone(phoneList.getPhoneNumber());

						}

					}
					cust_data.add(customer_data);
				} else {
					// logger.info("Customer is empty");
				}

				if (assign.getVehicle() != null) {

					VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

					ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
					Service service_data = call_int_repo.getLatestServiceData(assign.getVehicle().getVehicle_id());

					service_data1.setNextServicedate(service_data.getNextServiceDate());

					serv_data.add(service_data1);

					veh_data1.setVehicleRegNo(assign.getVehicle().getVehicleRegNo());
					veh_data1.setModel(assign.getVehicle().getModel());
					veh_data1.setNextServicedate(assign.getVehicle().getNextServicedate());

					veh_data.add(veh_data1);
				} else {
					// logger.info("Vehical is empty");
				}
				if (assign.getSrdisposition() != null) {
					SRDispositionDataOnTabLoad sr_disposition_data = new SRDispositionDataOnTabLoad();
					sr_disposition_data
							.setCallDisposition(assign.getSrdisposition().getCallDispositionData().getDisposition());
					sRD_data.add(sr_disposition_data);
				} else {
					// logger.info("srDisposition is empty");
				}
				if (assign.getServiceBooked() != null) {
					ServicebookedDataOnTabLoad servicebooked_data = new ServicebookedDataOnTabLoad();

					servicebooked_data.setServiceScheduledDate(assign.getServiceBooked().getServiceScheduledDateStr());
					servicebooked_data.setServiceScheduledTime(assign.getServiceBooked().getServiceScheduledTimeStr());
					servbooked_data.add(servicebooked_data);
				} else {
					// logger.info("ServiceBooked is empty");
				}

				if (assign.getWyzUser() != null) {

					WyzUserDataOnTabLoad user_data = new WyzUserDataOnTabLoad();
					user_data.setUsername(assign.getWyzUser().getUserName());
					wyz_data.add(user_data);
				} else {
					logger.info("WyzUser is empty");
				}

				CallInteractionDataOnTabLoad call_data_int = new CallInteractionDataOnTabLoad();
				call_data_int.setId(assign.getId());
				call_data_int.setDealerCode(assign.getDealerCode());

				int_data.add(call_data_int);

			}

		}

		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setSrdispositionLoadList(sRD_data);
		call_interaction.setCallInteractionLoadList(int_data);
		call_interaction.setServiceLoadList(serv_data);
		call_interaction.setServicebookedLoadList(servbooked_data);
		call_interaction.setWyzUserLoadList(wyz_data);
		return ok(toJson(call_interaction));

	}

	@Secure(clients = "FormClient")
	public Result getServiceNotRequiredDataCREMan(String selectedAgentList) {

		List<String> selectedList = Arrays.asList(selectedAgentList.split(","));
		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();

		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<SRDispositionDataOnTabLoad> sRD_data = new ArrayList<SRDispositionDataOnTabLoad>();
		List<CallInteractionDataOnTabLoad> int_data = new ArrayList<CallInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();
		List<WyzUserDataOnTabLoad> wyz_data = new ArrayList<WyzUserDataOnTabLoad>();
		for (String selectAgent : selectedList) {

			// logger.info("selected Agent in CREMAnager for service not
			// required :"+selectAgent);

			// logger.info("Data enetered in ServiceNotRequired");
			List<CallInteraction> ServiceNotRequired = call_int_repo.getServiceNotRequiredCalls(selectAgent,
					getUserLogindealerCode());
			// logger.info("ServiceNotRequired :"+ServiceNotRequired.size());

			for (CallInteraction addToAjaxData : ServiceNotRequired) {

				if (addToAjaxData.getCustomer() != null) {

					CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
					customer_data.setCustomerName(addToAjaxData.getCustomer().getCustomerName());
					customer_data.setCustomerCategory(addToAjaxData.getCustomer().getCustomerCategory());
					List<Phone> listPhone = addToAjaxData.getCustomer().getPhones();
					for (Phone phoneList : listPhone) {

						if (phoneList.isPreferredPhone) {

							customer_data.setCustomerPhone(phoneList.getPhoneNumber());

						}

					}
					cust_data.add(customer_data);
				} else {
					// logger.info("Customer is empty");
				}

				if (addToAjaxData.getVehicle() != null) {

					VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

					ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
					Service service_data = call_int_repo
							.getLatestServiceData(addToAjaxData.getVehicle().getVehicle_id());

					service_data1.setNextServicedate(service_data.getNextServiceDate());

					serv_data.add(service_data1);

					veh_data1.setVehicleRegNo(addToAjaxData.getVehicle().getVehicleRegNo());
					veh_data1.setModel(addToAjaxData.getVehicle().getModel());
					veh_data1.setNextServicedate(addToAjaxData.getVehicle().getNextServicedate());

					veh_data.add(veh_data1);
				} else {
					// logger.info("Vehical is empty");
				}
				if (addToAjaxData.getSrdisposition() != null) {
					SRDispositionDataOnTabLoad sr_disposition_data = new SRDispositionDataOnTabLoad();
					sr_disposition_data.setCallDisposition(
							addToAjaxData.getSrdisposition().getCallDispositionData().getDisposition());
					sr_disposition_data.setNoServiceReason(addToAjaxData.getSrdisposition().getNoServiceReason());

					sRD_data.add(sr_disposition_data);
				} else {
					// logger.info("srDisposition is empty");
				}
				if (addToAjaxData.getWyzUser() != null) {

					WyzUserDataOnTabLoad user_data = new WyzUserDataOnTabLoad();
					user_data.setUsername(addToAjaxData.getWyzUser().getUserName());
					wyz_data.add(user_data);
				} else {
					// logger.info("WyzUser is empty");
				}

				CallInteractionDataOnTabLoad call_data_int = new CallInteractionDataOnTabLoad();
				call_data_int.setId(addToAjaxData.getId());
				call_data_int.setDealerCode(addToAjaxData.getDealerCode());
				int_data.add(call_data_int);

			}
		}

		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setSrdispositionLoadList(sRD_data);
		call_interaction.setCallInteractionLoadList(int_data);
		call_interaction.setServiceLoadList(serv_data);
		call_interaction.setWyzUserLoadList(wyz_data);
		return ok(toJson(call_interaction));

	}

	@Secure(clients = "FormClient")
	public Result getNonContactsDataCREMan(String selectedAgentList) {

		List<String> selectedList = Arrays.asList(selectedAgentList.split(","));

		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();

		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<SRDispositionDataOnTabLoad> sRD_data = new ArrayList<SRDispositionDataOnTabLoad>();
		List<CallInteractionDataOnTabLoad> int_data = new ArrayList<CallInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();
		List<WyzUserDataOnTabLoad> wyz_data = new ArrayList<WyzUserDataOnTabLoad>();

		for (String selectAgent : selectedList) {

			// logger.info("Data enetered in NonContacts");
			List<CallInteraction> NonContacts = call_int_repo.getNonContactsCalls(selectAgent,
					getUserLogindealerCode());
			// logger.info("NonContacts :"+NonContacts.size());

			for (CallInteraction addToAjaxData : NonContacts) {

				if (addToAjaxData.getCustomer() != null) {

					CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
					customer_data.setCustomerName(addToAjaxData.getCustomer().getCustomerName());
					customer_data.setCustomerCategory(addToAjaxData.getCustomer().getCustomerCategory());
					List<Phone> listPhone = addToAjaxData.getCustomer().getPhones();
					for (Phone phoneList : listPhone) {

						if (phoneList.isPreferredPhone) {

							customer_data.setCustomerPhone(phoneList.getPhoneNumber());

						}

					}
					cust_data.add(customer_data);
				} else {
					// logger.info("Customer is empty");
				}

				if (addToAjaxData.getVehicle() != null) {

					VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

					ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
					Service service_data = call_int_repo
							.getLatestServiceData(addToAjaxData.getVehicle().getVehicle_id());

					service_data1.setNextServicedate(service_data.getNextServiceDate());

					serv_data.add(service_data1);

					veh_data1.setVehicleRegNo(addToAjaxData.getVehicle().getVehicleRegNo());
					veh_data1.setModel(addToAjaxData.getVehicle().getModel());
					veh_data1.setNextServicedate(addToAjaxData.getVehicle().getNextServicedate());

					veh_data.add(veh_data1);
				} else {
					// logger.info("Vehical is empty");
				}

				if (addToAjaxData.getSrdisposition() != null) {
					SRDispositionDataOnTabLoad sr_disposition_data = new SRDispositionDataOnTabLoad();
					sr_disposition_data.setCallDisposition(
							addToAjaxData.getSrdisposition().getCallDispositionData().getDisposition());

					sRD_data.add(sr_disposition_data);
				} else {
					// logger.info("srDisposition is empty");
				}
				if (addToAjaxData.getWyzUser() != null) {

					WyzUserDataOnTabLoad user_data = new WyzUserDataOnTabLoad();
					user_data.setUsername(addToAjaxData.getWyzUser().getUserName());
					wyz_data.add(user_data);
				} else {
					// logger.info("WyzUser is empty");
				}

				CallInteractionDataOnTabLoad call_data_int = new CallInteractionDataOnTabLoad();
				call_data_int.setId(addToAjaxData.getId());
				call_data_int.setDealerCode(addToAjaxData.getDealerCode());
				int_data.add(call_data_int);

			}

		}
		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setSrdispositionLoadList(sRD_data);
		call_interaction.setCallInteractionLoadList(int_data);
		call_interaction.setServiceLoadList(serv_data);
		call_interaction.setWyzUserLoadList(wyz_data);
		return ok(toJson(call_interaction));
	}

	@Secure(clients = "FormClient")
	public Result getDroppedCallsDataCREMan(String selectedAgentList) {

		List<String> selectedList = Arrays.asList(selectedAgentList.split(","));
		CallLogsOnLoadTabData call_interaction = new CallLogsOnLoadTabData();

		List<CustomerDataOnTabLoad> cust_data = new ArrayList<CustomerDataOnTabLoad>();
		List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
		List<SRDispositionDataOnTabLoad> sRD_data = new ArrayList<SRDispositionDataOnTabLoad>();
		List<CallInteractionDataOnTabLoad> int_data = new ArrayList<CallInteractionDataOnTabLoad>();
		List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();
		List<WyzUserDataOnTabLoad> wyz_data = new ArrayList<WyzUserDataOnTabLoad>();

		for (String selectedAgent : selectedList) {

			// logger.info("Data enetered in DroppedCalls");
			List<CallInteraction> DroppedCalls = call_int_repo.getDroppedCalls(selectedAgent, getUserLogindealerCode());
			// logger.info("DroppedCalls :"+DroppedCalls.size());

			for (CallInteraction addToAjaxData : DroppedCalls) {

				if (addToAjaxData.getCustomer() != null) {

					CustomerDataOnTabLoad customer_data = new CustomerDataOnTabLoad();
					customer_data.setCustomerName(addToAjaxData.getCustomer().getCustomerName());
					customer_data.setCustomerCategory(addToAjaxData.getCustomer().getCustomerCategory());
					List<Phone> listPhone = addToAjaxData.getCustomer().getPhones();
					for (Phone phoneList : listPhone) {

						if (phoneList.isPreferredPhone) {

							customer_data.setCustomerPhone(phoneList.getPhoneNumber());

						}

					}
					cust_data.add(customer_data);
				} else {
					// logger.info("Customer is empty");
				}

				if (addToAjaxData.getVehicle() != null) {

					VehicleDataOnTabLoad veh_data1 = new VehicleDataOnTabLoad();

					ServiceDataOnTabLoad service_data1 = new ServiceDataOnTabLoad();
					Service service_data = call_int_repo
							.getLatestServiceData(addToAjaxData.getVehicle().getVehicle_id());

					service_data1.setNextServicedate(service_data.getNextServiceDate());

					serv_data.add(service_data1);

					veh_data1.setVehicleRegNo(addToAjaxData.getVehicle().getVehicleRegNo());
					veh_data1.setModel(addToAjaxData.getVehicle().getModel());
					veh_data1.setNextServicedate(addToAjaxData.getVehicle().getNextServicedate());

					veh_data.add(veh_data1);
				} else {
					// logger.info("Vehical is empty");
				}
				if (addToAjaxData.getSrdisposition() != null) {
					SRDispositionDataOnTabLoad sr_disposition_data = new SRDispositionDataOnTabLoad();
					sr_disposition_data.setCallDisposition(
							addToAjaxData.getSrdisposition().getCallDispositionData().getDisposition());

					sRD_data.add(sr_disposition_data);
				} else {
					// logger.info("srDisposition is empty");
				}
				if (addToAjaxData.getWyzUser() != null) {

					WyzUserDataOnTabLoad user_data = new WyzUserDataOnTabLoad();
					user_data.setUsername(addToAjaxData.getWyzUser().getUserName());
					wyz_data.add(user_data);
				} else {
					// logger.info("WyzUser is empty");
				}

				CallInteractionDataOnTabLoad call_data_int = new CallInteractionDataOnTabLoad();
				call_data_int.setId(addToAjaxData.getId());
				call_data_int.setDealerCode(addToAjaxData.getDealerCode());

				int_data.add(call_data_int);

			}

		}
		// logger.info("cust_data"+cust_data.size()+"veh_data"+veh_data.size()+"sRD_data"+sRD_data.size()+"int_data"+int_data.size()+"wyz_data"+wyz_data.size()+"serv_data"+serv_data.size());
		call_interaction.setCustomerLoadList(cust_data);
		call_interaction.setVehicleLoadList(veh_data);
		call_interaction.setSrdispositionLoadList(sRD_data);
		call_interaction.setCallInteractionLoadList(int_data);
		call_interaction.setServiceLoadList(serv_data);
		call_interaction.setWyzUserLoadList(wyz_data);
		return ok(toJson(call_interaction));

	}

	@Secure(clients = "FormClient")
	public Result getListWorkshopByLocation(String selectedCity) {

		// logger.info("selected city :"+selectedCity);
		List<Workshop> workshop_list = call_int_repo.getWorkshopListByLoca(selectedCity);

		List<Workshop> work_list = new ArrayList<Workshop>();
		for (Workshop listData : workshop_list) {

			listData.setBays(null);
			listData.setServicesBooked(null);
			listData.setUsers(null);
			listData.setLocation(null);
			listData.setWorkshopSummarys(null);
			listData.setService(null);
			listData.setServiceAdvisor(null);
			listData.setDriver(null);
			listData.setUserWorkshop(null);
			work_list.add(listData);
		}
		// logger.info("workshop llist from controller is :"+work_list.size());
		return ok(toJson(work_list));

	}

	@Secure(clients = "FormClient")
	public Result getWorkshopSummaryDetails(long selectedWorkshop, String schDate) {

		long workshop_summary = call_int_repo.getworkshopsummaryById(selectedWorkshop, schDate);

		return ok(toJson(workshop_summary));

	}

	@Secure(clients = "FormClient")
	public Result ajaxsearchVehicle(String veh_number) {
		Vehicle vehicle = call_int_repo.getSearchVeh(veh_number);

		// logger.info("data_veh"+vehicle);
		ComplaintInformation cInformation = new ComplaintInformation();
		if (vehicle != null) {
			if (vehicle.getServiceBooked() != null) {
				cInformation.setServiceAdvisor("");
			}
			cInformation.setModel(vehicle.getModel());
			cInformation.setChassisNo(vehicle.getChassisNo());
			cInformation.setVariant(vehicle.getVariant());
			cInformation.setSaleDate(vehicle.getSaleDate());
			cInformation.setLastServiceDate(vehicle.getLastServiceDate());

			if (vehicle.getCustomer().getPreferredEmail() != null) {
				cInformation.setCustomerEmail(vehicle.getCustomer().getPreferredEmail().getEmailAddress());
			}
			if (vehicle.getCustomer().getPreferredPhone() != null) {

				cInformation.setCustomerMobileNo(vehicle.getCustomer().getPreferredPhone().getPhoneNumber());

			}

			cInformation.setCustomerName(vehicle.getCustomer().getCustomerName());

			if (vehicle.getCustomer().getPreferredAdress() != null) {

				if (vehicle.getCustomer().getPreferredAdress().getConcatenatedAdress() != null
						&& vehicle.getCustomer().getPreferredAdress().getConcatenatedAdress() != "") {

					cInformation.setPreferredAdress(vehicle.getCustomer().getPreferredAdress().getConcatenatedAdress());
				} else {

					String address1 = "";
					String address2 = "";
					String address3 = "";

					if (vehicle.getCustomer().getPreferredAdress().getAddressLine1() != null) {
						address1 = vehicle.getCustomer().getPreferredAdress().getAddressLine1();
					}
					if (vehicle.getCustomer().getPreferredAdress().getAddressLine2() != null) {
						address2 = vehicle.getCustomer().getPreferredAdress().getAddressLine2();
					}
					if (vehicle.getCustomer().getPreferredAdress().getAddressLine3() != null) {
						address3 = vehicle.getCustomer().getPreferredAdress().getAddressLine3();
					}

					cInformation.setPreferredAdress(address1 + address2 + address3);
				}

			}

			return ok(toJson(cInformation));
		} else {
			cInformation.setIsError("true");
			return ok(toJson(cInformation));
		}

	}

	@Secure(clients = "FormClient")
	public Result complaintsResolution() {
		List<Complaint> complaintData = call_int_repo.getAllComplaintsForResolution(getUserLoginName());
		List<Complaint> compalintDataclosed = call_int_repo.getClosedComplaintsForResolution(getUserLoginName());
		return ok(complaintResolution.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(),
				getUserLoginName(), complaintData, compalintDataclosed));
	}

	@Secure(clients = "FormClient")
	public Result ajaxcomplaintNumber(String complaintNum, String veh_num) {
		Vehicle vehicle = call_int_repo.getSearchVeh(veh_num);
		Complaint complaint = call_int_repo.getSearchComplaintNum(complaintNum);

		ComplaintInformation cInformation = new ComplaintInformation();
		if (complaint != null) {

			cInformation.setVehicleRegNo(vehicle.getVehicleRegNo());
			cInformation.setModel(vehicle.getModel());
			cInformation.setChassisNo(vehicle.getChassisNo());
			cInformation.setVariant(vehicle.getVariant());
			cInformation.setSaleDate(vehicle.getSaleDate());
			cInformation.setCustomerName(vehicle.getCustomer().getCustomerName());
			cInformation.setFunctionName(complaint.getFunctionName());
			cInformation.setSourceName(complaint.getSourceName());
			cInformation.setComplaintType(complaint.getComplaintType());
			cInformation.setSubcomplaintType(complaint.getSubcomplaintType());
			cInformation.setDescription(complaint.getDescription());
			cInformation.setComplaintNumber(complaint.getComplaintNumber());
			cInformation.setComplaintStatus(complaint.getComplaintStatus());
			cInformation.setLastServiceDate(complaint.getLastServiceDate());
			cInformation.setServiceadvisor(complaint.getServiceadvisor());
			cInformation.setWorkshop(complaint.getWorkshop());

			if (vehicle.getCustomer().getPreferredEmail() != null) {
				cInformation.setCustomerEmail(vehicle.getCustomer().getPreferredEmail().getEmailAddress());
			}
			if (vehicle.getCustomer().getPreferredPhone() != null) {

				cInformation.setCustomerMobileNo(vehicle.getCustomer().getPreferredPhone().getPhoneNumber());

			}
			cInformation.setCustomerName(vehicle.getCustomer().getCustomerName());

			if (vehicle.getCustomer().getPreferredAdress() != null) {

				if (vehicle.getCustomer().getPreferredAdress().getConcatenatedAdress() != null
						&& vehicle.getCustomer().getPreferredAdress().getConcatenatedAdress() != "") {

					cInformation.setPreferredAdress(vehicle.getCustomer().getPreferredAdress().getConcatenatedAdress());
				} else {

					cInformation.setPreferredAdress(vehicle.getCustomer().getPreferredAdress().getAddressLine1()
							+ vehicle.getCustomer().getPreferredAdress().getAddressLine2()
							+ vehicle.getCustomer().getPreferredAdress().getAddressLine3());
				}

			}
			if (complaint.getLocation() != null) {

				cInformation.setLocation(complaint.getLocation().getName());
			}
			cInformation.setPriority(complaint.getPriority());

			return ok(toJson(cInformation));

		} else {
			cInformation.setIsError("true");
			return ok(toJson(cInformation));
		}

	}

	@Secure(clients = "FormClient")
	public Result ajaxcomplaintNumberClosed(String complaintNumClosed, String veh_numclosed) {
		Vehicle vehicle = call_int_repo.getSearchVehClosed(veh_numclosed);
		Complaint complaint = call_int_repo.getSearchComplaintNumClosed(complaintNumClosed);
		// logger.info("data_veh"+complaint);
		ComplaintInformation cInformation = new ComplaintInformation();
		if (complaint != null) {
			// if(complaint.vehicle.getServiceBooked()!=null){
			// cInformation.setServiceAdvisor(complaint.vehicle.getServiceBooked().getServiceAdvisor().getAdvisorName());
			// }
			cInformation.setVehicleRegNo(vehicle.getVehicleRegNo());
			cInformation.setModel(vehicle.getModel());
			cInformation.setChassisNo(vehicle.getChassisNo());
			cInformation.setVariant(vehicle.getVariant());
			cInformation.setSaleDate(vehicle.getSaleDate());
			if (vehicle.getCustomer().getPreferredEmail() != null) {
				cInformation.setCustomerEmail(vehicle.getCustomer().getPreferredEmail().getEmailAddress());
			}
			if (vehicle.getCustomer().getPreferredPhone() != null) {

				cInformation.setCustomerMobileNo(vehicle.getCustomer().getPreferredPhone().getPhoneNumber());

			}
			cInformation.setCustomerName(vehicle.getCustomer().getCustomerName());

			if (vehicle.getCustomer().getPreferredAdress() != null) {

				if (vehicle.getCustomer().getPreferredAdress().getConcatenatedAdress() != ""
						&& vehicle.getCustomer().getPreferredAdress().getConcatenatedAdress() != null) {

					cInformation.setPreferredAdress(vehicle.getCustomer().getPreferredAdress().getConcatenatedAdress());
				} else {

					cInformation.setPreferredAdress(vehicle.getCustomer().getPreferredAdress().getAddressLine1()
							+ vehicle.getCustomer().getPreferredAdress().getAddressLine2()
							+ vehicle.getCustomer().getPreferredAdress().getAddressLine3());
				}
			}

			cInformation.setFunctionName(complaint.getFunctionName());
			cInformation.setSourceName(complaint.getSourceName());
			cInformation.setComplaintType(complaint.getComplaintType());
			cInformation.setSubcomplaintType(complaint.getSubcomplaintType());
			cInformation.setDescription(complaint.getDescription());
			cInformation.setPriority(complaint.getPriority());
			cInformation.setComplaintStatus(complaint.getComplaintStatus());
			cInformation.setCustomerstatus(complaint.getCustomerstatus());
			cInformation.setWorkshop(complaint.getWorkshop());
			cInformation.setServiceadvisor(complaint.getServiceadvisor());
			if (complaint.getLocation() != null) {

				cInformation.setLocation(complaint.getLocation().getName());
			}
			// cInformation.setWyzUser(complaint.getWyzUser().getUserName());

			int countInteractionExist = complaint.getComplaintInteractions().size();
			if (countInteractionExist > 0) {

				List<ComplaintInteraction> interaction = complaint.getComplaintInteractions();
				ComplaintInteraction latestData = interaction.get(countInteractionExist - 1);
				cInformation.setReasonFor(latestData.getReasonFor());
				cInformation.setActionTaken(latestData.getActionTaken());
				cInformation.setResolutionBy(latestData.getResolutionBy());

			}

			return ok(toJson(cInformation));

		} else {
			cInformation.setIsError("true");
			return ok(toJson(cInformation));
		}

	}

	@Secure(clients = "FormClient")
	public Result saveaddComplaintAssignModile(String complaintNum, String city, String workshop, String functions,
			String ownership, String priority, String esclation1, String esclation2) {
		call_int_repo.getResolutionData(complaintNum, city, workshop, functions, ownership, priority, esclation1,
				esclation2);
		return ok("Done");
	}

	@Secure(clients = "FormClient")
	public Result complaintAssignment(String complaintNum, String veh_num) {
		Vehicle vehicle = call_int_repo.getSearchVeh(veh_num);
		Complaint complaint = call_int_repo.getSearchComplaintNum(complaintNum);
		// logger.info("data_veh"+complaint);
		ComplaintInformation cInformationData = new ComplaintInformation();
		if (complaint != null) {
			cInformationData.setVehicleRegNo(vehicle.getVehicleRegNo());
			cInformationData.setModel(vehicle.getModel());
			cInformationData.setChassisNo(vehicle.getChassisNo());
			cInformationData.setVariant(vehicle.getVariant());
			cInformationData.setSaleDate(vehicle.getSaleDate());
			// cInformationData.setCustomerEmail(vehicle.getCustomer().getCustomerEmail());
			List<Phone> listPhone = vehicle.getCustomer().getPhones();
			for (Phone phoneList : listPhone) {

				if (phoneList.isPreferredPhone) {

					cInformationData.setCustomerMobileNo(phoneList.getPhoneNumber());

				}

			}
			cInformationData.setCustomerName(vehicle.getCustomer().getCustomerName());
			if (vehicle.getCustomer().getPreferredAdress() != null) {

				if (vehicle.getCustomer().getPreferredAdress().getConcatenatedAdress() != null
						&& vehicle.getCustomer().getPreferredAdress().getConcatenatedAdress() != "") {

					cInformationData
							.setPreferredAdress(vehicle.getCustomer().getPreferredAdress().getConcatenatedAdress());
				} else {

					cInformationData.setPreferredAdress(vehicle.getCustomer().getPreferredAdress().getAddressLine1()
							+ vehicle.getCustomer().getPreferredAdress().getAddressLine2()
							+ vehicle.getCustomer().getPreferredAdress().getAddressLine3());
				}

			}
			cInformationData.setDescription(complaint.getDescription());
			cInformationData.setSourceName(complaint.getSourceName());
			cInformationData.setFunctionName(complaint.getFunctionName());
			cInformationData.setSubcomplaintType(complaint.getSubcomplaintType());
			cInformationData.setComplaintType(complaint.getComplaintType());
			cInformationData.setComplaintNumber(complaint.getComplaintNumber());
			cInformationData.setComplaintStatus(complaint.getComplaintStatus());
			cInformationData.setPriority(complaint.getPriority());

			return ok(toJson(cInformationData));

		} else {
			cInformationData.setIsError("true");
			return ok(toJson(cInformationData));
		}

	}

	// public Result viewAllComplaints(){
	// List<Complaint> complaintData =
	// call_int_repo.getAllComplaintsForManager();
	// List<Complaint> compalintDataclosed =
	// call_int_repo.getClosedComplaintsForManager();
	// return
	// ok(viewAllComplaints.render(getUserLogindealerCode(),getDealerName(),getUserLoginName(),complaintData,compalintDataclosed));
	// }

	@Secure(clients = "FormClient")
	public Result viewAllComplaints1() {
		List<String> cresList = repo.getCREUserNameofCREManager(getUserLoginName(), getUserLogindealerCode());
		List<Location> locationList = call_int_repo.getLocationList();

		List<String> distinctFunctions = call_int_repo.getDistinctFunctionComplaints();
		return ok(viewAllComplaints1.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(), cresList,
				locationList, distinctFunctions));
	}

	@Secure(clients = "FormClient")
	public Result getComplaintHistoryAll(String complaintNumber, String vehregnumber) {

		List<ComplaintInteraction> complaintHistory = call_int_repo.getComplaintHistory(complaintNumber, vehregnumber);
		List<ComplaintInformation> complaintHistoryData = new ArrayList<ComplaintInformation>();

		Complaint complaintRow = call_int_repo.getSearchComplaintNum(complaintNumber);
		ComplaintInformation ComplaintAssignInfo = new ComplaintInformation();

		if (complaintRow.getComplaintStatus().equals("Assigned")) {

			ComplaintAssignInfo.setComplaintNumber(complaintRow.getComplaintNumber());
			ComplaintAssignInfo.setWyzUser(complaintRow.getWyzUser().getUserName());
			ComplaintAssignInfo.setComplaintType(complaintRow.getComplaintType());
			ComplaintAssignInfo.setCustomerMobileNo(complaintRow.getCustomerPhone());
			ComplaintAssignInfo.setFunctionName(complaintRow.getFunctionName());
			ComplaintAssignInfo.setSourceName(complaintRow.getSourceName());
			ComplaintAssignInfo.setVehicleRegNo(complaintRow.getVehicleRegNo());
			ComplaintAssignInfo.setComplaintStatus(complaintRow.getComplaintStatus());
			ComplaintAssignInfo.setIssueDate(complaintRow.getIssueDate());
			ComplaintAssignInfo.setAgeOfComplaint(complaintRow.getAgeOfComplaint());
			// ComplaintAssignInfo.setResolutionBy("");
			complaintHistoryData.add(ComplaintAssignInfo);

		}

		for (ComplaintInteraction complainthist : complaintHistory) {
			ComplaintInformation ComplaintInfo = new ComplaintInformation();

			ComplaintInfo.setComplaintNumber(complainthist.getComplaint().getComplaintNumber());
			ComplaintInfo.setWyzUser(complainthist.getComplaint().getWyzUser().getUserName());
			ComplaintInfo.setComplaintType(complainthist.getComplaint().getComplaintType());
			ComplaintInfo.setCustomerMobileNo(complainthist.getComplaint().getCustomerPhone());
			ComplaintInfo.setFunctionName(complainthist.getComplaint().getFunctionName());
			ComplaintInfo.setSourceName(complainthist.getComplaint().getSourceName());
			ComplaintInfo.setVehicleRegNo(complainthist.getComplaint().getVehicleRegNo());
			ComplaintInfo.setComplaintStatus(complainthist.getComplaint().getComplaintStatus());
			ComplaintInfo.setIssueDate(complainthist.getComplaint().getIssueDate());
			ComplaintInfo.setAgeOfComplaint(complainthist.getComplaint().getAgeOfComplaint());
			// ComplaintInfo.setResolutionBy(complainthist.getResolutionBy());
			complaintHistoryData.add(ComplaintInfo);
		}
		return ok(toJson(complaintHistoryData));
	}

	@Secure(clients = "FormClient")
	public Result getComplaintsDataByFilter(String filterData, String loc, String func, String raisedDate, String endDate ) {
		logger.info("filterdata::" + filterData);

		// List<Complaint> complaintFilterAll =
		// call_int_repo.getComplaintsFilterData(filterData);

		List<Complaint> complaintFilterAll = call_int_repo.getComplaintsFilterDataByStatus(filterData, loc, func,raisedDate,endDate);

		List<ComplaintInformation> complaintFilterData = new ArrayList<ComplaintInformation>();
		for (Complaint complaintFilter : complaintFilterAll) {
			ComplaintInformation addComplaint = new ComplaintInformation();

			addComplaint.setComplaintNumber(complaintFilter.getComplaintNumber());
			addComplaint.setWyzUser(complaintFilter.getWyzUser().getUserName());
			addComplaint.setComplaintType(complaintFilter.getComplaintType());
			addComplaint.setCustomerMobileNo(complaintFilter.getCustomerPhone());
			addComplaint.setFunctionName(complaintFilter.getFunctionName());
			addComplaint.setSourceName(complaintFilter.getSourceName());
			addComplaint.setVehicleRegNo(complaintFilter.getVehicleRegNo());
			addComplaint.setComplaintStatus(complaintFilter.getComplaintStatus());
			addComplaint.setIssueDate(complaintFilter.getIssueDate());
			addComplaint.setAgeOfComplaint(complaintFilter.getAgeOfComplaint());
			complaintFilterData.add(addComplaint);
		}

		return ok(toJson(complaintFilterData));
	}

	// Roster of adding unavialabilty dates and seeing the unavailabilty Roaster
	// of that user

	@Secure(clients = "FormClient")
	public Result rosterOfUnavailabiltyByUser() {

		List<WyzUser> users = wyzRepo.getUsersofCREManager(getUserLoginName(), getUserLogindealerCode());

		return ok(rosterOfUnAvailibilityByUserSelectedRange.render(getDealerName(), getUserLoginName(), users));
	}

	@Secure(clients = "FormClient")
	public Result addRosterOfUserByAjax(String selectAgent, String fromDate, String toDate) {
		// logger.info("selectAgent : "+selectAgent+" fromDate : "+fromDate+"
		// toDate :"+toDate);

		String resultOfUpdate = call_int_repo.addRangeOfUnaviabilityOfUser(selectAgent, fromDate, toDate);
		List<UnAvailability> listUnavailabilty = call_int_repo.getRangeOfUnavailabilty(selectAgent, fromDate, toDate);

		List<Date> new_listUnavailabilty = new ArrayList<Date>();
		if (listUnavailabilty.size() != 0) {
			Date tmp = null;
			Date tmpmin = null;
			int i = 0;
			int j = 0;
			for (UnAvailability un_list : listUnavailabilty) {
				un_list.setWyzUser(null);

				if (i == 0) {
					tmp = un_list.getToDate();
					i++;
				} else {
					if (tmp.before(un_list.getToDate())) {
						tmp = un_list.getToDate();
						i++;
					}
				}

				if (j == 0) {
					tmpmin = un_list.getFromDate();
					j++;
				} else {
					if (tmpmin.after(un_list.getFromDate())) {
						tmpmin = un_list.getToDate();
						j++;
					}
				}

			}

			new_listUnavailabilty.add(listUnavailabilty.get(j - 1).getFromDate());
			new_listUnavailabilty.add(listUnavailabilty.get(i - 1).getToDate());

			logger.info("listUnavailabilty.get(i-1).getToDate() :" + listUnavailabilty.get(i - 1).getToDate()
					+ "listUnavailabilty.get(j-1).getFromDate() : " + listUnavailabilty.get(j - 1).getFromDate());

			return ok(toJson(listUnavailabilty));

		} else {

			return ok(toJson(resultOfUpdate));
		}

	}

	// Call Disposition on load Data

	@Secure(clients = "FormClient")
	public Result getRosterDataByUser(String selectAgent) {

		List<UnAvailability> unavialbilty_data = call_int_repo.getUnavialabilityListByUser(selectAgent);

		logger.info("unavialbilty_data size " + unavialbilty_data.size());

		for (UnAvailability unav : unavialbilty_data) {

			unav.setWyzUser(null);

		}
		return ok(toJson(unavialbilty_data));
	}

	@Secure(clients = "FormClient")
	public Result updateRosterOfUserByAjaxVal(String selectAgent, String From_Date, String To_Date) {
		List<UnAvailability> updateList = call_int_repo.getRangeOfUnavailabilty(selectAgent, From_Date, To_Date);
		long countOfupdateList = updateList.size();

		for (UnAvailability deleteData : updateList) {

			call_int_repo.deleteUnavialabilty(deleteData);

		}
		call_int_repo.addUnavialabilty(selectAgent, From_Date, To_Date);

		return ok("");
	}

	// delete the unavialbility

	@Secure(clients = "FormClient")
	public Result deleteUnavialbility(long id) {

		UnAvailability unavialability = call_int_repo.getUnavialabilityListByID(id);

		call_int_repo.deleteUnavialabilty(unavialability);

		return ok(toJson("success"));
	}
	// Call Disposition on load Data

	@Secure(clients = "FormClient")
	public Result getServiceDataOfCustomer(long customerId) {

	 logger.info(" customerId from controller is : "+customerId);

	List<Vehicle> vehicle_data = call_int_repo.getVehicleList(customerId);
	List<Service> service_data = call_int_repo.getAllServiceListOfVehicle(vehicle_data);

	List<VehicleDataOnTabLoad> veh_data = new ArrayList<VehicleDataOnTabLoad>();
	List<ServiceDataOnTabLoad> serv_data = new ArrayList<ServiceDataOnTabLoad>();

	for (Service addServiceData : service_data) {

	if (addServiceData.getVehicle() != null) {

	VehicleDataOnTabLoad vehicle = new VehicleDataOnTabLoad();
	vehicle.setVehicleRegNo(addServiceData.getVehicle().getVehicleRegNo());
	vehicle.setModel(addServiceData.getVehicle().getModel());
	vehicle.setVariant(addServiceData.getVehicle().getVariant());
	vehicle.setColor(addServiceData.getVehicle().getColor());
	if(addServiceData.getVehicle().getCustomer().getSegment() != null){
	vehicle.setCategory(addServiceData.getVehicle().getCustomer().getCustomerCategory());

	}
	veh_data.add(vehicle);

	}
	ServiceDataOnTabLoad service = new ServiceDataOnTabLoad();
	service.setJobCardDate(addServiceData.getJobCardDateStr());
	service.setJobCardNumber(addServiceData.getJobCardNumber());
	service.setServiceDate(addServiceData.getLastServiceDate());
	service.setServiceType(addServiceData.getLastServiceType());
	service.setLastServiceMeterReading(addServiceData.getLastServiceMeterReading());	
	service.setServiceAdvisor(addServiceData.getSaName());	
	service.setServiceLocaton(addServiceData.getWorkshop().getWorkshopName());
	service.setBillDate(addServiceData.getBillDate());
	service.setCustName(addServiceData.getCustomerName());
	service.setKilometer(addServiceData.getServiceOdometerReading());
	service.setPartAmt(addServiceData.getPartsAmt());
	service.setLabourAmt(addServiceData.getLabAmt());
	//service.setTotalAmt(addServiceData.getBillAmt());
	service.setBillAmt(addServiceData.getBillAmt());
	service.setMfgPartsTotal(addServiceData.getMfgPartsTotal());
	service.setLocalPartsTotal(addServiceData.getLocalPartsTotal());
	service.setMaximileTotal(addServiceData.getMaximileTotal());
	service.setOilConsumablesTotal(addServiceData.getOilConsumablesTotal());
	service.setMaxiCareTotal(addServiceData.getMaxiCareTotal());
	service.setMfgAccessoriesTotal(addServiceData.getMfgAccessoriesTotal());
	service.setLocalAccessoriesTotal(addServiceData.getLocalAccessoriesTotal());
	service.setInhouseLabourTotal(addServiceData.getInhouseLabourTotal());
	service.setOutLabourTotal(addServiceData.getOutLabourTotal());
	service.setMenuCodeDesc(addServiceData.getMenuCodeDesc());
	serv_data.add(service);
	}

	CallLogsOnLoadTabData serviceLoadData = new CallLogsOnLoadTabData();
	serviceLoadData.setServiceLoadList(serv_data);
	serviceLoadData.setVehicleLoadList(veh_data);
	// logger.info("serv_data : "+serv_data.size()+" veh_data :
	// "+veh_data.size());

	return ok(toJson(serviceLoadData));
	}
	// INTERACTION HISTORY

	@Secure(clients = "FormClient")
	public Result getComplaintHistoryVeh(long vehicalId) {
		List<Complaint> complaintVeh = repo.getAllComplaintsOFVehicle(vehicalId);

		List<ComplaintInformation> complaintHistory = new ArrayList<ComplaintInformation>();
		for (Complaint sa : complaintVeh) {

			ComplaintInformation compInf = new ComplaintInformation();

			compInf.setComplaintNumber(sa.getComplaintNumber());
			compInf.setSaleDate(sa.getIssueDate());
			compInf.setSourceName(sa.getSourceName());
			compInf.setFunctionName(sa.getFunctionName());
			compInf.setSubcomplaintType(sa.getSubcomplaintType());
			compInf.setDescription(sa.getComplaintStatus());

			complaintHistory.add(compInf);

		}

		return ok(toJson(complaintHistory));
	}

	@Secure(clients = "FormClient")
	public Result getSMSHistoryOfCustomer(long customerId) {

		List<SMSInteraction> sms_int = sms_repo.getAllSMSInteracOfCustomer(customerId);

		List<SMSInteractionhistory> sms_int_data = new ArrayList<SMSInteractionhistory>();
		for (SMSInteraction sms_li : sms_int) {

			SMSInteractionhistory smseachData = new SMSInteractionhistory();

			String user="";
					if(sms_li.getWyzUser()!=null){
						user=sms_li.getWyzUser().getUserName();
						}		
					
			smseachData.setInteractionDate(sms_li.getInteractionDate());
			smseachData.setInteractionTime(sms_li.getInteractionTime());
			smseachData.setCreId(user);
			smseachData.setScenario(sms_li.getInteractionType());
			smseachData.setMessage(sms_li.getSmsMessage());

			if (sms_li.isSmsStatus()) {
				smseachData.setStatus("Success");
			} else {

				smseachData.setStatus("Failure");
			}
			sms_int_data.add(smseachData);
		}

		return ok(toJson(sms_int_data));
	}

	@Secure(clients = "FormClient")
	public Result getCallHistoryOfvehicalId(long vehicalId) {

		logger.info("getCallHistoryOfvehicalId : " + vehicalId);

		CallLogsOnLoadTabData callhistoryData = new CallLogsOnLoadTabData();

		List<CallInteraction> getCallHistoryOfNumber = repo.CallHistoryOfCustomer(vehicalId, getUserLogindealerCode());

		/*List<ServiceAdvisorInteraction> interaction_List = repo.getSAInteractionList(vehicalId,
				getUserLogindealerCode());*/

		List<SRDispositionDataOnTabLoad> sRD_data = new ArrayList<SRDispositionDataOnTabLoad>();
		List<CallInteractionDataOnTabLoad> int_data = new ArrayList<CallInteractionDataOnTabLoad>();
		List<WyzUserDataOnTabLoad> wyz_data = new ArrayList<WyzUserDataOnTabLoad>();

		for (CallInteraction call_data : getCallHistoryOfNumber) {

			if (call_data.getInsuranceDisposition() != null) {
				
				WyzUserDataOnTabLoad user_data = new WyzUserDataOnTabLoad();
				user_data.setUsername(call_data.getWyzUser().getUserName());
				wyz_data.add(user_data);
				
				CallInteractionDataOnTabLoad interaction_data = new CallInteractionDataOnTabLoad();
				interaction_data.setCallDate(call_data.getCallDate());
				interaction_data.setCallTime(call_data.getCallTime());
				interaction_data.setId((long)call_data.getId());
				
				if(call_data.getCallType()!=null){
				interaction_data.setCallType(call_data.getCallType());
				}else{
					
					interaction_data.setCallType(call_data.getIsCallinitaited());	
				}
				int_data.add(interaction_data);

				SRDispositionDataOnTabLoad sr_data = new SRDispositionDataOnTabLoad();
				sr_data.setCallDisposition(call_data.getInsuranceDisposition().getCallDispositionData().getDisposition());
				sr_data.setNoServiceReason(call_data.getInsuranceDisposition().getRenewalNotRequiredReason());
				sr_data.setTypeOfCall("INS");

				if (call_data.getInsuranceDisposition().getComments() != null
						&& call_data.getInsuranceDisposition().getComments() != "") {

					sr_data.setComments(call_data.getInsuranceDisposition().getComments());

				} else if (call_data.getInsuranceDisposition().getOther() != null
						&& call_data.getInsuranceDisposition().getOther() != "") {

					sr_data.setComments(call_data.getInsuranceDisposition().getOther());

				} else if (call_data.getInsuranceDisposition().getReason() != null
						&& call_data.getInsuranceDisposition().getReason() != "") {

					sr_data.setComments(call_data.getInsuranceDisposition().getReason());

				} else if (call_data.getCustomer().getComments() != null
						&& call_data.getCustomer().getComments() != "") {

					sr_data.setComments(call_data.getCustomer().getComments());
				}

				sr_data.setFollowUpDate(call_data.getInsuranceDisposition().getFollowUpDate());
				sr_data.setFollowUpTime(call_data.getInsuranceDisposition().getFollowUpTime());
				
				if(call_data.getInsuranceDisposition().getCallDispositionData().getDispositionId() == 4){
					
					sr_data.setNoServiceReason(sr_data.getFollowUpDate() +" "+sr_data.getFollowUpTime() );
					
				}

				/*if (call_data.getInsuranceAssignedInteraction() != null) {

					if (call_data.getInsuranceAssignedInteraction().getCampaign() != null) {

						sr_data.setTypeOfCall(
								call_data.getInsuranceAssignedInteraction().getCampaign().getCampaignName());
					}
				}*/

				sRD_data.add(sr_data);
			}

			if (call_data.getSrdisposition() != null) {
				
				WyzUserDataOnTabLoad user_data = new WyzUserDataOnTabLoad();
				user_data.setUsername(call_data.getWyzUser().getUserName());
				wyz_data.add(user_data);
				
				CallInteractionDataOnTabLoad interaction_data = new CallInteractionDataOnTabLoad();
				interaction_data.setCallDate(call_data.getCallDate());
				interaction_data.setCallTime(call_data.getCallTime());
				interaction_data.setId((long)call_data.getId());
				
				if(call_data.getCallType()!=null){
				interaction_data.setCallType(call_data.getCallType());
				}else{
					
					interaction_data.setCallType(call_data.getIsCallinitaited());	
				}
				int_data.add(interaction_data);

				SRDispositionDataOnTabLoad sr_data = new SRDispositionDataOnTabLoad();
				sr_data.setCallDisposition(call_data.getSrdisposition().getCallDispositionData().getDisposition());
				sr_data.setNoServiceReason(call_data.getSrdisposition().getNoServiceReason());
				sr_data.setTypeOfCall("SMR");
				if (call_data.getSrdisposition().getComments() != null
						&& call_data.getSrdisposition().getComments() != "") {

					sr_data.setComments(call_data.getSrdisposition().getComments());

				} else if (call_data.getSrdisposition().getOther() != null
						&& call_data.getSrdisposition().getOther() != "") {

					sr_data.setComments(call_data.getSrdisposition().getOther());

				} else if (call_data.getSrdisposition().getReason() != null
						&& call_data.getSrdisposition().getReason() != "") {

					sr_data.setComments(call_data.getSrdisposition().getReason());

				}else if (call_data.getSrdisposition().getRemarks() != null
						&& call_data.getSrdisposition().getRemarks() != "") {

					sr_data.setComments(call_data.getSrdisposition().getRemarks());

				} else if (call_data.getCustomer().getComments() != null
						&& call_data.getCustomer().getComments() != "") {

					sr_data.setComments(call_data.getCustomer().getComments());
				}
				sr_data.setFollowUpDate(call_data.getSrdisposition().getFollowUpDate());
				sr_data.setFollowUpTime(call_data.getSrdisposition().getFollowUpTime());
				if(call_data.getSrdisposition().getCallDispositionData().getDispositionId() == 4){
					
					sr_data.setNoServiceReason(sr_data.getFollowUpDate() +" "+sr_data.getFollowUpTime() );
					
				}
				/*if (call_data.getAssignedInteraction() != null) {

					if (call_data.getAssignedInteraction().getCampaign() != null) {

						sr_data.setTypeOfCall(call_data.getAssignedInteraction().getCampaign().getCampaignName());
					}
				}*/

				sRD_data.add(sr_data);

			}

			if (call_data.getPsfdisposition() != null) {
				
				WyzUserDataOnTabLoad user_data = new WyzUserDataOnTabLoad();
				user_data.setUsername(call_data.getWyzUser().getUserName());
				wyz_data.add(user_data);
				
				CallInteractionDataOnTabLoad interaction_data = new CallInteractionDataOnTabLoad();
				interaction_data.setCallDate(call_data.getCallDate());
				interaction_data.setCallTime(call_data.getCallTime());
				interaction_data.setId((long)call_data.getId());
				
				if(call_data.getCallType()!=null){
				interaction_data.setCallType(call_data.getCallType());
				}else{
					
					interaction_data.setCallType(call_data.getIsCallinitaited());	
				}
				int_data.add(interaction_data);

				SRDispositionDataOnTabLoad sr_data = new SRDispositionDataOnTabLoad();
				sr_data.setCallDisposition(call_data.getPsfdisposition().getCallDispositionData().getDisposition());
				sr_data.setTypeOfCall("PSF");
				
				if(call_data.getPsfdisposition().getRemarks() != null
						&& call_data.getPsfdisposition().getRemarks() != "" ){
					
					sr_data.setComments(call_data.getPsfdisposition().getRemarks());
					
				}else if(call_data.getPsfdisposition().getOtherComments() != null
						&& call_data.getPsfdisposition().getOtherComments() != ""){
					
					sr_data.setComments(call_data.getPsfdisposition().getOtherComments());
					
				}
				
				
				sr_data.setFollowUpDate(call_data.getPsfdisposition().getPsfFollowUpDate());
				sr_data.setFollowUpTime(call_data.getPsfdisposition().getPsfFollowUpTime());
				
				if(call_data.getPsfdisposition().getCallDispositionData().getDispositionId() == 4){
					
					sr_data.setNoServiceReason(sr_data.getFollowUpDate() +" "+sr_data.getFollowUpTime() );
					
				}
				/*if (call_data.getPsfAssignedInteraction() != null) {

					if (call_data.getPsfAssignedInteraction().getCampaign() != null) {

						sr_data.setTypeOfCall(call_data.getPsfAssignedInteraction().getCampaign().getCampaignName());
					}
				}*/
				sRD_data.add(sr_data);

			}
			
			

		}


		callhistoryData.setSrdispositionLoadList(sRD_data);
		callhistoryData.setCallInteractionLoadList(int_data);
		callhistoryData.setWyzUserLoadList(wyz_data);

		// logger.info(" Call history sRD_data : "+sRD_data.size()+" int_data :
		// "+int_data.size()+" wyz_data : "+wyz_data.size());
		return ok(toJson(callhistoryData));

	}
	@Secure(clients = "FormClient")
	public Result getWorkshopListName(long workshopId) {

		String userName = getUserProfile().getId();

		WyzUser userdata = wyzRepo.getUserbyUserName(userName);

		List<Workshop> workshopList1 = new ArrayList<Workshop>();

		if (workshopId != 0) {

			List<Workshop> workshoplistdata = repo.getWorkshopListById(workshopId);
			for (Workshop worklist : workshoplistdata) {

				worklist.setCampaign(null);
				worklist.setLocation(null);
				// worklist.setUploadData(null);
				worklist.setWorkshopSummarys(null);
				worklist.setServicesBooked(null);
				worklist.setService(null);
				worklist.setServiceAdvisor(null);
				worklist.setBays(null);
				worklist.setDriver(null);
				worklist.setUsers(null);
				worklist.setUserWorkshop(null);
				workshopList1.add(worklist);

			}

		} else {

			List<Workshop> workshoplistdata = call_int_repo.getWorkshopList();
			for (Workshop worklist : workshoplistdata) {

				worklist.setCampaign(null);
				worklist.setLocation(null);
				// worklist.setUploadData(null);
				worklist.setWorkshopSummarys(null);
				worklist.setServicesBooked(null);
				worklist.setService(null);
				worklist.setServiceAdvisor(null);
				worklist.setBays(null);
				worklist.setDriver(null);
				worklist.setUsers(null);
				worklist.setUserWorkshop(null);
				workshopList1.add(worklist);

			}
		}
		return ok(toJson(workshopList1));

	}

	@Secure(clients = "FormClient")
	public Result getServiceAdvisorOfWorkshop(long workshopId) {

		// logger.info("workshopId is : "+workshopId);
		List<ServiceAdvisor> listOfAdvisorBasedOnCity = call_int_repo.getListOfAdvisorBasedOnCity(workshopId);

		logger.info("listOfAdvisorBasedOnCity count is : " + listOfAdvisorBasedOnCity.size());

		List<ServiceAdvisor> activeList = new ArrayList<ServiceAdvisor>();

		if (listOfAdvisorBasedOnCity.size() != 0) {

			for (ServiceAdvisor advisor : listOfAdvisorBasedOnCity) {
				advisor.setService(null);
				advisor.setWorkshop(null);
				advisor.setWyzUser(null);
				advisor.setServicebooked(null);
				if (advisor.isActive) {

					activeList.add(advisor);
				}
			}
		} else {

			List<ServiceAdvisor> allServiceAdvisor = call_int_repo.getAdvisorList();

			for (ServiceAdvisor advisor : allServiceAdvisor) {
				advisor.setService(null);
				advisor.setWorkshop(null);
				advisor.setWyzUser(null);
				advisor.setServicebooked(null);
				if (advisor.isActive) {

					activeList.add(advisor);
				}
			}

		}

		return ok(toJson(activeList));
	}

	@Secure(clients = "FormClient")
	public Result getLeadByUserLocation(long userLocation) {

		//List<TaggingUsers> listTagging = call_int_repo.getListTaggingById(userLocation);
		
		List<ComplaintTypes> listTagging = repo.listAllComplaintTaggingUsers();
		
		

		return ok(toJson(listTagging));

	}

	@Secure(clients = "FormClient")
	public Result getTagNameByUpselLeadType(long userLocation, long upselIDTag) {

		List<String> taggedName = call_int_repo.getTaggingPersonByUpsellId(userLocation, upselIDTag);
		return ok(toJson(taggedName));

	}

	@Secure(clients = "FormClient")
	public Result getLeadTagByDepartment(long userLocation, long departmentName) {

		List<String> taggedName = call_int_repo.getTaggingPersonByLocationDepartment(userLocation, departmentName);
		return ok(toJson(taggedName));
	}

	@Secure(clients = "FormClient")
	public Result saveaddcustomermobno(String custMobNo, Long wyzUser_id, Long customer_Id) {

		logger.info("custMobNo : " + custMobNo + " wyzUser_id : " + wyzUser_id + " customer_Id : " + customer_Id);

		long addnewdata = call_int_repo.getCustmobNo(wyzUser_id, custMobNo, customer_Id);

		return ok(toJson(addnewdata));
	}

	@Secure(clients = "FormClient")
	public Result saveaddcustomerEmail(String custEmail, Long wyzUser_id, Long customer_Id) {
		call_int_repo.getCustEmail(wyzUser_id, custEmail, customer_Id);
		return ok("Done");
	}

	@Secure(clients = "FormClient")
	public Result addComplaintsByManager() {
		
		List<Workshop> listwork=call_int_repo.getWorkshopList();
		
		List<ComplaintSource> sourcelist=repo.getComplaintSourceList();


		return ok(complaintsManager.render(sourcelist,listwork,getUserLogindealerCode(), getDealerName(), getUserLoginName()));

	}

	@Secure(clients = "FormClient")
	public Result postComplaintsByManager() {
		Form<Complaint> complaints_data = Form.form(Complaint.class).bindFromRequest();
		Complaint complaint = complaints_data.get();
		Form<Vehicle> vehicle_data = Form.form(Vehicle.class).bindFromRequest();
		Vehicle vehicle = vehicle_data.get();
		Form<Phone> phone_data = Form.form(Phone.class).bindFromRequest();
		Phone phone = phone_data.get();
		Form<Email> email_data = Form.form(Email.class).bindFromRequest();
		Email email = email_data.get();
		Form<Address> address_data = Form.form(Address.class).bindFromRequest();
		Address address = address_data.get();
		complaint.setWyzUser(null);
		/// vehicle.set
		call_int_repo.addNewComplaint(complaint, vehicle, phone, email, address, getUserLoginName());
		// MessageTemplete message =
		// repo.getMessageTempleteOfDealerServiceBooked("Complaint",
		// getDealerName());
		// com.typesafe.config.Config configuration =
		// com.typesafe.config.ConfigFactory.load();
		// String smsUrl = configuration.getString("app.smsUrl");
		// sms_repo.SMSTriggerBulkComplaints(getUserLogindealerCode(),
		// complaint.getComplaintNumber(),
		// complaint.getCustomerName(), complaint.getVehicleRegNo(),
		// complaint.getCustomerPhone(), message,
		// smsUrl);
		return redirect(controllers.routes.CallInteractionController.addComplaintsByManager());

		// return ok(toJson(complaint));
	}

	// Existong veicle reg number

	@Secure(clients = "FormClient")
	public Result getExistingVehicleRegCount(String vehicleReg) {

		long countOfVehicle = call_int_repo.getVehicleRegNoCount(vehicleReg);
		logger.info("count of existing vehicle : " + countOfVehicle);

		return ok(toJson(countOfVehicle));
	}

	/// SMS sending controller

	@Secure(clients = "FormClient")
	public Result sendCustomSMSAjax(long rerenceNumber) {

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());

		JsonNode json = request().body().asJson();

		logger.info("Received Reference number: " + rerenceNumber);

		if (json == null) {
			return badRequest("Expecting Json data");
		} else {
			String message = json.findPath("message").textValue();
			logger.info("SMS Message:" + message);
			String messageType = json.findPath("type").textValue();
			logger.info("SMS MessageType:" + messageType);
			if (message == null) {
				return badRequest("Missing parameter [message]");
			} else {

				boolean success = sms_repo.sendCustomSMS(rerenceNumber, message, messageType, userdata);
				//logger.info("success : "+success);

				if (!success) {
					return internalServerError("could not send sms");
				} else {
					return ok(toJson("Done"));
				}
			}
		}

	}

	@Secure(clients = "FormClient")
	public Result getCRESListBasedOnWorkshop(long workshopId) {

		List<WyzUser> listCREManCRE = wyzRepo.getCREUserNameByWorkshopId(workshopId);

		List<WyzUserDataOnTabLoad> listCRE = new ArrayList<WyzUserDataOnTabLoad>();

		for (WyzUser user : listCREManCRE) {
			WyzUserDataOnTabLoad adduser = new WyzUserDataOnTabLoad();
			adduser.setId(user.getId());
			adduser.setUsername(user.getUserName());
			listCRE.add(adduser);
		}
		logger.info("count of CRE for this workshop : " + listCRE.size());
		return ok(toJson(listCRE));

	}

	@Secure(clients = "FormClient")
	public Result getDriverListBasedOnworkshop(long workshopId) {

		// logger.info("workshopId is : "+workshopId);
		List<Driver> ListOfDriversBasedOnWorkshop = call_int_repo.getListOfDriversBasedOnWorkshop(workshopId);

		List<Driver> driverlist = new ArrayList<Driver>();

		if (ListOfDriversBasedOnWorkshop.size() != 0) {
			logger.info("ListOfDriversBasedOnWorkshop count is : " + ListOfDriversBasedOnWorkshop.size());

			for (Driver driver : ListOfDriversBasedOnWorkshop) {
				// driver.setUploadData(null);
				driver.setWorkshop(null);
				driver.setWyzUser(null);
				driver.setServicesBooked(null);
				driverlist.add(driver);
			}
		} else {

			List<Driver> allDriver = call_int_repo.getDriverList();

			for (Driver driver : allDriver) {
				// driver.setUploadData(null);
				driver.setWorkshop(null);
				driver.setWyzUser(null);
				driver.setServicesBooked(null);
				driverlist.add(driver);
			}

		}

		return ok(toJson(driverlist));
	}

	@Secure(clients = "FormClient")
	public Result getWorkshopsByLocations(String locations) {

		logger.info("location in call histopry:"+locations);
		List<String> selectedLocations = Arrays.asList(locations.split(","));

		List<String> selectedWorkshops = call_int_repo.getWorkshopListByLocaList(selectedLocations);

		return ok(toJson(selectedWorkshops));
	}

	@Secure(clients = "FormClient")
	public Result getCRESByWorkshops(String workshops) {

		List<String> selectedWorkshops = Arrays.asList(workshops.split(","));

		List<String> selectedCREs = call_int_repo.getCRESListByWorkshops(selectedWorkshops);

		return ok(toJson(selectedCREs));

	}

	@Secure(clients = "FormClient")
	public Result getCitiesByState(String selState) {

		List<String> cities = call_int_repo.getCitiesByState(selState);

		Collections.sort(cities);

		return ok(toJson(cities));

	}

	@Secure(clients = "FormClient")
	public Result downloadExcelFile() throws IOException, ParseException {

		Form<ComplaintInformation> data_range = Form.form(ComplaintInformation.class).bindFromRequest();
		ComplaintInformation range = data_range.get();

		if (range.getRaisedDate().equals("")) {
			range.setRaisedDate("0");
		}

		if (range.getEndDate().equals("")) {
			range.setEndDate("0");
		}
		logger.info("loc: " + range.getLocation() + " func : " + range.getFunctionName() + " rai : "
				+ range.getRaisedDate() + " sttus : " + range.getComplaintStatus());

		List<Complaint> complaintFilterAll = call_int_repo.getComplaintsFilterDataByStatus(range.getComplaintStatus(),
				range.getLocation(), range.getFunctionName(), range.getRaisedDate(), range.getEndDate());

		List<ComplaintInformation> complaintFilterData = new ArrayList<ComplaintInformation>();
		for (Complaint complaintFilter : complaintFilterAll) {
			ComplaintInformation addComplaint = new ComplaintInformation();

			addComplaint.setComplaintNumber(complaintFilter.getComplaintNumber());
			addComplaint.setWyzUser(complaintFilter.getWyzUser().getUserName());
			addComplaint.setComplaintType(complaintFilter.getComplaintType());
			addComplaint.setCustomerMobileNo(complaintFilter.getCustomerPhone());
			addComplaint.setFunctionName(complaintFilter.getFunctionName());
			addComplaint.setSourceName(complaintFilter.getSourceName());
			addComplaint.setVehicleRegNo(complaintFilter.getVehicleRegNo());
			addComplaint.setComplaintStatus(complaintFilter.getComplaintStatus());
			addComplaint.setIssueDate(complaintFilter.getIssueDate());
			addComplaint.setAgeOfComplaint(complaintFilter.getAgeOfComplaint());
			complaintFilterData.add(addComplaint);
		}

		Config configuration = ConfigFactory.load();
		String pathOfTemp = configuration.getString("app.configuration");

		String fileName = range.getLocation() + "_" + range.getComplaintStatus();
		File fis = new File(pathOfTemp + fileName + ".csv");

		ICsvBeanWriter csvwriter = new CsvBeanWriter(new FileWriter(fis, false), CsvPreference.STANDARD_PREFERENCE);
		final String[] headers = new String[] { "complaintNumber", "wyzUser", "complaintType", "customerMobileNo",
				"functionName", "sourceName", "vehicleRegNo", "complaintStatus", "issueDate", "ageOfComplaint" };

		try {

			final CellProcessor[] processors = getProcessors();

			csvwriter.writeHeader(headers);

			for (ComplaintInformation cihd : complaintFilterData) {

				csvwriter.write(cihd, headers, processors);
			}

			logger.info("csv created anfd ready to download");
		} catch (IOException ex) {
			logger.info("Error writing the CSV file: " + ex);
		} finally {
			try {
				logger.info("bean writer is not null ");
				csvwriter.close();
			} catch (IOException ex) {
				logger.info("Error closing the writer: " + ex);
			}
		}
		response().setContentType("application/x-download");
		response().setHeader("Content-disposition", "attachment; filename=" + fileName + ".csv");
		return ok(fis, fileName + ".csv");

	}

	private static CellProcessor[] getProcessors() {

		final CellProcessor[] processors = new CellProcessor[] { new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional() };

		return processors;
	}

	@Secure(clients = "FormClient")
	public Result viewAllComplaintsReadOnlyAccess() {
		List<String> cresList = repo.getCREUserNameofCREManager(getUserLoginName(), getUserLogindealerCode());
		List<Location> locationList = call_int_repo.getLocationList();
		List<Workshop> workshopList = call_int_repo.getWorkshopList();

		List<String> distinctFunctions = call_int_repo.getDistinctFunctionComplaints();
		return ok(viewAllComplaintsReadOnly.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(),
				getUserLoginName(), cresList, locationList, workshopList, distinctFunctions));

	}

	@Secure(clients = "FormClient")
	public Result getListWorkshopByLocationById(long selectedCity) {

		// logger.info("selected city :"+selectedCity);
		List<Workshop> workshop_list = call_int_repo.getWorkshopListByLocaId(selectedCity);

		List<Workshop> work_list = new ArrayList<Workshop>();
		for (Workshop listData : workshop_list) {

			listData.setBays(null);
			listData.setServicesBooked(null);
			listData.setUsers(null);
			listData.setLocation(null);
			listData.setWorkshopSummarys(null);
			listData.setService(null);
			listData.setServiceAdvisor(null);
			listData.setDriver(null);
      listData.setUserWorkshop(null);
			work_list.add(listData);
		}
		// logger.info("workshop list from controller is :"+work_list.size());
		return ok(toJson(work_list));

	}

	@Secure(clients = "FormClient")
	public Result getFunctionsListByLoc(String selectedCity) {

		List<String> functionsCom = call_int_repo.getComplaintStatusList(selectedCity);

		logger.info(" functionsCom count : " + functionsCom.size());

		return ok(toJson(functionsCom));

	}
	@Secure(clients = "FormClient")
	public Result getusersByFuncandLocation(String city,String func){
		
		Set<String>  userList=call_int_repo.getUsersByLocandFunc(city,func);
		
		return ok(toJson(userList));
	}
	@Secure(clients = "FormClient")
	public Result getCREListBasedOnWorkshopCallHistory(long workshopId) {

		List<WyzUser> listCREManCRE = wyzRepo.getCREUserNameByWorkshopId(workshopId);

		List<WyzUserDataOnTabLoad> listCRE = new ArrayList<WyzUserDataOnTabLoad>();

		for (WyzUser user : listCREManCRE) {
			WyzUserDataOnTabLoad adduser = new WyzUserDataOnTabLoad();
			adduser.setId(user.getId());
			adduser.setUsername(user.getUserName());
			listCRE.add(adduser);
		}
		logger.info("count of CRE for this workshop : " + listCRE.size());
		return ok(toJson(listCRE));

	}
	@Secure(clients = "FormClient")
		public Result getUsersBasedOnWorkshopSMSHistory(long workshopId) {
			List<WyzUser> listOfUsers = wyzRepo.getUserNameByWorkshopId(workshopId);
	
			List<WyzUserDataOnTabLoad> listCRE = new ArrayList<WyzUserDataOnTabLoad>();
	
			for (WyzUser user : listOfUsers) {
				WyzUserDataOnTabLoad adduser = new WyzUserDataOnTabLoad();
				adduser.setId(user.getId());
				adduser.setUsername(user.getUserName());
				listCRE.add(adduser);
			}
			logger.info("count of CRE for this workshop : " + listCRE.size());
			return ok(toJson(listCRE));
	
	
		}
	
	
	
	
	
	}
