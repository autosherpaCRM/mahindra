package controllers.webModel;

import java.util.ArrayList;

public class FileUploadResponse {
	
	private ArrayList<FileUploadData> files;

	public ArrayList<FileUploadData> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<FileUploadData> files) {
		this.files = files;
	}

}
