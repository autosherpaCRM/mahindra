/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;


import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;
import java.util.List;
import javax.inject.Inject;
import models.CallDispositionData;
import models.CallInteraction;
import models.Customer;
import models.Driver;
import models.ListingForm;
import models.Location;
import models.PickupDrop;
import models.SMSTemplate;
import models.SRDisposition;
import models.Service;
import models.ServiceBooked;
import models.ServiceTypes;
import models.SpecialOfferMaster;
import models.Vehicle;
import models.Workshop;
import models.WyzUser;
import org.hibernate.Hibernate;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.java.Secure;

import org.springframework.stereotype.Controller;
import play.Logger;
import play.data.Form;
import play.mvc.Result;
import static play.mvc.Results.forbidden;
import static play.mvc.Results.ok;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.InboundCallInteractionRepository;
import repositories.SMSTriggerRespository;
import repositories.WyzUserRepository;

/**
 *
 * @author W-885
 */
@Controller
public class InboundCallInteractionController extends play.mvc.Controller {

	private final CallInfoRepository repo;
	private final CallInteractionsRepository call_int_repo;
	private final WyzUserRepository wyzRepo;
	private final SMSTriggerRespository sms_repo;
	private final InboundCallInteractionRepository inbound_repo;
	private PlaySessionStore playSessionStore;


	Logger.ALogger logger = play.Logger.of("application");

	@Inject
	public InboundCallInteractionController(CallInfoRepository repository, org.pac4j.core.config.Config config,
			CallInteractionsRepository interRepo, WyzUserRepository wyzRepository, SMSTriggerRespository smsRepository,
			InboundCallInteractionRepository inboundRepository,PlaySessionStore plstore) {
		repo = repository;
		call_int_repo = interRepo;
		wyzRepo = wyzRepository;
		sms_repo = smsRepository;
		inbound_repo = inboundRepository;
		config = (org.pac4j.core.config.Config) config;
		playSessionStore = plstore;

	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	
	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	// INBOUND CALL OF DIPOSITION PAGE
//	@Secure(clients = "FormClient")
//	public Result getInBoundCallDispositionForm(String cid, String vehicle_id) {
//
//		logger.info("cid : " + cid + " vehicle_id : " + vehicle_id);
//
//		String userName = getUserProfile().getId();
//		WyzUser userdata = wyzRepo.getUserbyUserName(userName);
//		String dealername = getDealerName();
//		long uniqueid = repo.getUniqueIdForCallInitaiating();
//
//		Customer customerData = inbound_repo.getCustomerInforById(Long.parseLong(cid));
//		Vehicle vehicleData = inbound_repo.getVehicleInformationById(Long.parseLong(vehicle_id));
//
//		CallInteraction getLatestIntOfVehicle = inbound_repo.getLatestInterOfVehicleById(Long.parseLong(vehicle_id));
//
//		List<Workshop> workshopList = call_int_repo.getWorkshopListByLoca(userdata.getLocation().getName());
//		List<Location> locationList = call_int_repo.getLocationList();
//		List<ServiceTypes> servicetypeList = call_int_repo.getAllServiceTypeList();
//
//		long countOfServicePresent = call_int_repo.getCountOfServiceHistoryOfVehicle(vehicleData.getVehicle_id());
//		logger.info("countOfServicePresent count : " + countOfServicePresent);
//		Service latestService = new Service();
//
//		if (countOfServicePresent != 0) {
//
//			latestService = call_int_repo.getLatestServiceData(vehicleData.getVehicle_id());
//		}
//
//		List<SMSTemplate> templates = repo.getMessageTemplatesForDisplay();
//
//		for (SMSTemplate template : templates) {
//			if (vehicleData != null) {
//
//				template.setSmsTemplate(template.getSmsTemplate().replace("(Vehicle)", vehicleData.getVehicleRegNo()));
//
//				if (vehicleData.getNextServicedate() != null) {
//
//					template.setSmsTemplate(template.getSmsTemplate().replace("(Service Due Date)",
//							vehicleData.getNextServicedate().toString()));
//				} else {
//
//					template.setSmsTemplate(template.getSmsTemplate().replace("(Service Due Date)", ""));
//				}
//
//			} else {
//				template.setSmsTemplate(template.getSmsTemplate().replace("(Vehicle)", ""));
//			}
//		}
//
//		List<String> complaintOFCust = call_int_repo.getComplaintStatusandCount(customerData.getId());
//		List<SpecialOfferMaster> offersList = repo.getActiveSpecialOffers();
//		return ok(dispositionPageOfCall.render(uniqueid, getUserLogindealerCode(), dealername, userName, workshopList,
//				customerData, vehicleData, locationList, userdata, latestService, templates, complaintOFCust,
//				servicetypeList, getLatestIntOfVehicle, offersList));
//
//	}
//
//	@Secure(clients = "FormClient")
//	public Result postCallDispositionFormInbound() {
//
//		logger.info("postCallDispositionFormInbound  ");
//
//		Form<ListingForm> listData_form = Form.form(ListingForm.class).bindFromRequest();
//		ListingForm listData = listData_form.get();
//
//		String uploadAction = listData.getTypeOfsubmit();
//		String userName = getUserProfile().getId();
//		String dealername = getDealerName();
//
//		Form<CallInteraction> form = Form.form(CallInteraction.class).bindFromRequest();
//		CallInteraction call_interaction = form.get();
//
//		Form<ServiceBooked> service_Booked_data = Form.form(ServiceBooked.class).bindFromRequest();
//		ServiceBooked service_Booked = service_Booked_data.get();
//
//		Form<SRDisposition> sr_Disposition_data = Form.form(SRDisposition.class).bindFromRequest();
//		SRDisposition sr_Disposition = sr_Disposition_data.get();
//
//		Form<PickupDrop> pick_up_data = Form.form(PickupDrop.class).bindFromRequest();
//		PickupDrop pick_up = pick_up_data.get();
//
//		Form<CallDispositionData> disposition_data = Form.form(CallDispositionData.class).bindFromRequest();
//		CallDispositionData dispo_data = disposition_data.get();
//
//		if (uploadAction == null || uploadAction.length() == 0) {
//			return forbidden(views.html.error403.render());
//		} else {
//
//			String action = uploadAction;
//			logger.info("uploadAction[0] first id value : " + action);
//			if ("nonContact".equals(action)) {
//
//				call_interaction.setDroppedCount(call_interaction.getDroppedCount() + 1);
//
//				logger.info("nonContact " + dispo_data.getDisposition());
//				repo.addCallInteraction(call_interaction, listData, sr_Disposition, getUserLogindealerCode(),
//						dispo_data);
//				return redirect(routes.CallInteractionController.searchByCustomer());
//
//			} else if ("callMeLater".equals(action)) {
//
//				sr_Disposition.setIsFollowupRequired("Yes");
//				sr_Disposition.setIsFollowUpDone("No");
//				logger.info("callMeLater " + dispo_data.getDisposition());
//				repo.addCallInteraction(call_interaction, listData, sr_Disposition, getUserLogindealerCode(),
//						dispo_data);
//				return redirect(routes.CallInteractionController.searchByCustomer());
//
//			} else if ("bookMyservice".equals(action)) {
//
//				logger.info("bookMyservice " + dispo_data.getDisposition());
//
//				repo.addCallInteractionOfServiceBooked(call_interaction, sr_Disposition, listData, service_Booked,
//						pick_up, getUserLogindealerCode(), dispo_data);
//
//				// call_int_repo.updateAssignedCallMade(listData.getSingleId());
//				repo.addUpsellLead(sr_Disposition, listData, listData.getVehicleId_SB());
//
//				return redirect(routes.CallInteractionController.searchByCustomer());
//			} else if ("alreadyServiced".equals(action)) {
//				if (sr_Disposition.getServicedAtOtherDealerRadio() != null) {
//					if (sr_Disposition.getServicedAtOtherDealerRadio().equals("Non Autorized workshop")) {
//
//						sr_Disposition.setDateOfService(listData.getDateOfServiceNonAuth());
//
//					}
//				}
//				sr_Disposition.setComplaintOrFB_TagTo(listData.getComplaintOrFB_TagTo1());
//				sr_Disposition.setDepartmentForFB(listData.getDepartmentForFB1());
//				sr_Disposition.setRemarksOfFB(listData.getRemarksOfFB1());
//
//				logger.info("alreadyServiced " + sr_Disposition.getNoServiceReason());
//				repo.addCallInteraction(call_interaction, listData, sr_Disposition, getUserLogindealerCode(),
//						dispo_data);
//
//				repo.addUpsellLead(sr_Disposition, listData, listData.getVehicalId());
//
//				logger.info("alreadyServiced " + sr_Disposition.getNoServiceReason());
//				return redirect(routes.CallInteractionController.searchByCustomer());
//
//			} else if ("vehicleSold".equals(action)) {
//
//				logger.info("vehicleSold " + sr_Disposition.getNoServiceReason());
//
//				repo.addCallInteraction(call_interaction, listData, sr_Disposition, getUserLogindealerCode(),
//						dispo_data);
//
//				Form<Customer> customerData = Form.form(Customer.class).bindFromRequest();
//				Customer newCustomer = customerData.get();
//
//				call_int_repo.addNewCustomerInformation(newCustomer, listData.getPhoneList());
//
//				Form<Vehicle> vehicleData = Form.form(Vehicle.class).bindFromRequest();
//				Vehicle newVehicle = vehicleData.get();
//
//				call_int_repo.addNewVehicleInformation(newVehicle, listData.getCustomer_Id());
//
//				return redirect(routes.CallInteractionController.searchByCustomer());
//
//			} else if ("dissatifiedwithPreviousService".equals(action)) {
//
//				if (sr_Disposition.getNoServiceReason().equals("Dissatisfied with Insurance")) {
//
//					sr_Disposition.setNoServiceReasonTaggedTo(listData.getNoServiceReasonTaggedTo1());
//					sr_Disposition.setNoServiceReasonTaggedToComments(listData.getNoServiceReasonTaggedToComments1());
//
//				}
//
//				repo.addCallInteraction(call_interaction, listData, sr_Disposition, getUserLogindealerCode(),
//						dispo_data);
//				logger.info("dissatifiedwithPreviousService " + sr_Disposition.getNoServiceReason());
//
//				return redirect(routes.CallInteractionController.searchByCustomer());
//
//			} else if ("inBoundCallSubmit".equals(action)) {
//
//				logger.info("inBoundCallSubmit is: " + action);
//
//				sr_Disposition.setServiceType(listData.getServiceTypeAltId());
//				sr_Disposition.setComplaintOrFB_TagTo(listData.getComplaintOrFB_TagTo2());
//				sr_Disposition.setDepartmentForFB(listData.getDepartmentForFB2());
//				sr_Disposition.setRemarksOfFB(listData.getRemarksOfFB2());
//
//				dispo_data.setDisposition(listData.getContactType());
//				listData.setVehicleId_SB(listData.getVehicleIn());
//				listData.setWorkshopId(listData.getWorkshopIn());
//				listData.setServiceScheduledDate(listData.getServiceScheduledDateIn());
//				listData.setServiceScheduledTime(listData.getServiceScheduledTimeIn());
//				listData.setDriverId(listData.getDriverIdSelectIn());
//				listData.setTime_From(listData.getTime_FromIn());
//				listData.setTime_To(listData.getTime_ToIn());
//				listData.setServiceAdvisorId(listData.getServiceAdvisorIdIn());
//				service_Booked.setTypeOfPickup(listData.getTypeOfPickupIn());
//				service_Booked.setServiceBookedType(listData.getServiceTypeAltId());
//				pick_up.setPickUpAddress(listData.getPickUpAddressIn());
//
//				repo.addCallInteractionOfServiceBooked(call_interaction, sr_Disposition, listData, service_Booked,
//						pick_up, getUserLogindealerCode(), dispo_data);
//
//				repo.addUpsellLead(sr_Disposition, listData, listData.getVehicleId_SB());
//
//				return redirect(routes.CallInteractionController.searchByCustomer());
//
//			} else {
//
//				return forbidden(views.html.error403.render());
//
//			}
//
//		}
//
//	}
	
	

}
