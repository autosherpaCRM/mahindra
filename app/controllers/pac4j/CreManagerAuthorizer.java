package controllers.pac4j;

import java.util.List;
import java.util.Set;

import org.pac4j.core.authorization.authorizer.ProfileAuthorizer;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.exception.HttpAction;
import org.pac4j.core.profile.CommonProfile;

import models.Role;
import play.Logger.ALogger;
import repositories.WyzUserRepository;

public class CreManagerAuthorizer extends ProfileAuthorizer<CommonProfile> {

	public static String TENANT_KEY = "TENANT";

	protected static final ALogger logger = play.Logger.of(CreManagerAuthorizer.class);

	private WyzUserRepository wyzRepository;

	public WyzUserRepository getWyzRepository() {
		return wyzRepository;
	}

	public void setWyzRepository(WyzUserRepository wyzRepository) {
		this.wyzRepository = wyzRepository;
	}

	@Override
	public boolean isAuthorized(final WebContext context, final List<CommonProfile> profiles) throws HttpAction {
		return isAnyAuthorized(context, profiles);
	}

	@Override
	public boolean isProfileAuthorized(final WebContext context, final CommonProfile profile) {
		logger.debug("##Entering the authorization..");

		Set<String> roles = profile.getRoles();
		String ipaddr = (java.lang.String) profile.getAttribute(MyAppAuthenticator.IP_ADDR);
		logger.debug("Ip from Profile: " + ipaddr);
		String remoteAddr = context.getRemoteAddr();
		logger.debug("IP from request: " + remoteAddr);
		boolean ipmatch = false;
		if (ipaddr.equals(remoteAddr)) {
			ipmatch = true;
		}

		String foundRole = roles.stream().filter(role -> role.equals(Role.ROLE_CRE_MANAGER)).findAny().orElse(null);
		logger.debug("foundRole : "+foundRole);
		
		if (foundRole != null && ipmatch) {
			return true;
		} else {
			return false;
		}
	}

}
