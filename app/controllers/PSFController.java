/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;
import controllers.webmodels.CallLogAjaxLoad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.persistence.Column;
import javax.sql.DataSource;

import models.CallDispositionData;
import models.Complaint;
import models.Customer;
import models.Driver;
import models.ListingForm;
import models.PSFAssignedInteraction;
import models.PSFInteraction;
import models.PickupDrop;
import models.SMSTemplate;
import models.Segment;
import models.Service;
import models.Vehicle;
import models.Workshop;
import models.WyzUser;
import models.AssignedInteraction;
import controllers.webmodels.CallLogDispositionLoad;
import controllers.webmodels.DispositionPage;
import controllers.webmodels.PSFFollowupNotificationModel;
import play.data.Form;
import play.db.DB;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.java.Secure;

import org.springframework.beans.factory.annotation.Autowired;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Campaign;
import play.libs.Json;

import org.springframework.stereotype.Controller;
import play.Logger;
import static play.libs.Json.toJson;
import play.mvc.Result;
import static play.mvc.Results.ok;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.InboundCallInteractionRepository;
import repositories.InsuranceRepository;
import repositories.PSFRepository;
import repositories.WyzUserRepository;
import views.html.psfListForFeedBackOfService;

import views.html.psfList15ForFeedBackOfService;
import views.html.psfList30thForFeedBackOfService;

import views.html.commonPSFDispositionPage;
import views.html.psf6thDayDispo;
import views.html.psf15thDayDispo;
import views.html.psf30thDayDispo;
import views.html.PSFList3rdForFeedbackOfService;
import views.html.psf3rdDayDispo;
import views.html.psf15thDayGoldDispo;
import views.html.psf15thDayNonGoldDispo;
import views.html.psf2ndDispoFord;
import views.html.psfList2ndDayFord;
import views.html.psfNextDayDisposition;
import views.html.psf4thDayDisposition;
import views.html.psfListNextDayFeedBack;
import views.html.psfList4thFeedBackOfService;
import views.html.psfAgeAbove2;
import views.html.psfAgeBelow2;
import views.html.CommercialVehiclePSF;

/**
 *
 * @author W-885
 */
public class PSFController extends play.mvc.Controller {

	Logger.ALogger logger = play.Logger.of("application");

	private PSFRepository psfRepo;
	private final CallInfoRepository repo;
	private final CallInteractionsRepository call_int_repo;
	private final WyzUserRepository wyzRepo;
	private final InboundCallInteractionRepository inbound_repo;
	private PlaySessionStore playSessionStore;
	private final InsuranceRepository insurRepo;

	@Inject
	private Service service;

	@Inject
	public PSFController(PSFRepository psfRepository, InboundCallInteractionRepository inboundRepository,
			CallInteractionsRepository interRepo, WyzUserRepository wyzRepository, CallInfoRepository repository,
			org.pac4j.core.config.Config config, PlaySessionStore plstore, InsuranceRepository insurRepository) {

		psfRepo = psfRepository;
		repo = repository;
		call_int_repo = interRepo;
		wyzRepo = wyzRepository;
		inbound_repo = inboundRepository;
		config = (org.pac4j.core.config.Config) config;
		playSessionStore = plstore;
		insurRepo = insurRepository;

	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	private String getOEMOfDealer() {

		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;

	}

	// FORD
	@Secure(clients = "FormClient")
	public Result getPSF2ndDayList() {
		return ok(psfList2ndDayFord.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(),
				getOEMOfDealer()));
	}

	@Secure(clients = "FormClient")
	public Result getPSFNextDayList() {
		return ok(psfListNextDayFeedBack.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(),
				getOEMOfDealer()));
	}

	@Secure(clients = "FormClient")
	public Result getPSF4thDayList() {
		return ok(psfList4thFeedBackOfService.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(),
				getOEMOfDealer()));
	}

	@Secure(clients = "FormClient")
	public Result getPSF3rdDayList() {
		logger.info("inside 3rd day psf feedback");
		return ok(PSFList3rdForFeedbackOfService.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(),
				getOEMOfDealer()));
	}

	@Secure(clients = "FormClient")
	public Result getPSFList() {
		return ok(psfListForFeedBackOfService.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(),
				getOEMOfDealer()));
	}

	@Secure(clients = "FormClient")
	public Result getPSF15List() {

		return ok(psfList15ForFeedBackOfService.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(),
				getOEMOfDealer()));
	}

	@Secure(clients = "FormClient")
	public Result getPSF30List() {

		return ok(psfList30thForFeedBackOfService.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(),
				getOEMOfDealer()));
	}

	@Secure(clients = "FormClient")
	public Result getCommonPSFDispositionPage(long cid, long vehicle_id, long interactionid, long dispositionHistory,
			long typeOfPSF) {

		logger.info("cid : " + cid + " vehicle_id : " + vehicle_id + " interactionid : " + interactionid);

		String userName = getUserProfile().getId();
		WyzUser userdata = wyzRepo.getUserbyUserName(userName);
		String dealername = getDealerName();
		long uniqueid = repo.getUniqueIdForCallInitaiating();

		List<Workshop> workshopList = call_int_repo.getWorkshop();

		Customer customerData = inbound_repo.getCustomerInforById(cid);

		List<String> statesList = insurRepo.getAllDistinctStates();
		logger.info(" statesList : " + statesList.size());

		Collections.sort(statesList);

		logger.info("phone count : " + customerData.getPhones().size());

		Vehicle vehicleData = inbound_repo.getVehicleInformationById(vehicle_id);

		long countOfServicePresent = call_int_repo.getCountOfServiceHistoryOfVehicle(vehicleData.getVehicle_id());
		logger.info("countOfServicePresent count : " + countOfServicePresent);

		String modeOfServiceExisting = "";

		if (countOfServicePresent != 0) {

			service = call_int_repo.getLatestServiceDataFiltering(vehicleData.getVehicle_id());

			modeOfServiceExisting = call_int_repo.getLatestServiceBookedType(cid, vehicle_id, service);
		}
		
		PSFAssignedInteraction lastPSFAssignStatus=psfRepo.getLastPSFAssignStatusOfVehicle(vehicle_id);
		
		
		List<SMSTemplate> templates = repo.getMessageTemplatesForDisplay();

		for (SMSTemplate template : templates) {
			if (vehicleData != null) {

				template.setSmsTemplate(template.getSmsTemplate().replace("(Vehicle)", vehicleData.getVehicleRegNo()));
				if (vehicleData.getNextServicedate() != null) {

					template.setSmsTemplate(template.getSmsTemplate().replace("(Service Due Date)",
							vehicleData.getNextServicedate().toString()));
				} else {

					template.setSmsTemplate(template.getSmsTemplate().replace("(Service Due Date)", ""));
				}
			} else {
				template.setSmsTemplate(template.getSmsTemplate().replace("(Vehicle)", ""));
				template.setSmsTemplate(template.getSmsTemplate().replace("(Service Due Date)", ""));
			}
		}

		List<String> complaintOFCust = call_int_repo.getComplaintStatusandCount(customerData.getId());

		if (getOEMOfDealer().equals("MARUTHI")) {

			if (typeOfPSF == 4) {

				return ok(commonPSFDispositionPage.render(getOEMOfDealer(), typeOfPSF, dispositionHistory,
						interactionid, uniqueid, getUserLogindealerCode(), dealername, userName, customerData,
						vehicleData, userdata, service, templates, complaintOFCust, statesList,lastPSFAssignStatus,
						psf6thDayDispo.render(modeOfServiceExisting, vehicleData, userName)));

			} else if (typeOfPSF == 5) {

				return ok(commonPSFDispositionPage.render(getOEMOfDealer(), typeOfPSF, dispositionHistory,
						interactionid, uniqueid, getUserLogindealerCode(), dealername, userName, customerData,
						vehicleData, userdata, service, templates, complaintOFCust, statesList,lastPSFAssignStatus,
						psf15thDayDispo.render(vehicleData, userName)));

			} else if (typeOfPSF == 6) {

				return ok(commonPSFDispositionPage.render(getOEMOfDealer(), typeOfPSF, dispositionHistory,
						interactionid, uniqueid, getUserLogindealerCode(), dealername, userName, customerData,
						vehicleData, userdata, service, templates, complaintOFCust, statesList,lastPSFAssignStatus,
						psf30thDayDispo.render(vehicleData, userName)));

			} else {

				return forbidden(views.html.error403.render());
			}
		} else if (getOEMOfDealer().equals("MAHINDRA")) {

			if (typeOfPSF == 4) {

				return ok(commonPSFDispositionPage.render(getOEMOfDealer(), typeOfPSF, dispositionHistory,
						interactionid, uniqueid, getUserLogindealerCode(), dealername, userName, customerData,
						vehicleData, userdata, service, templates, complaintOFCust, statesList,lastPSFAssignStatus,
						psfNextDayDisposition.render(vehicleData, userName, dealername, service, workshopList)));
			} else if (typeOfPSF == 5) {

				if (vehicleData.isCommercialVehicle()) {

					return ok(commonPSFDispositionPage.render(getOEMOfDealer(), typeOfPSF, dispositionHistory,
							interactionid, uniqueid, getUserLogindealerCode(), dealername, userName, customerData,
							vehicleData, userdata, service, templates, complaintOFCust, statesList,lastPSFAssignStatus,
							psf4thDayDisposition.render(service, vehicleData, userName, dealername,
									CommercialVehiclePSF.render())));

				} else if (vehicleData.getAge_of_vehicle() >= 730) {
					return ok(commonPSFDispositionPage.render(getOEMOfDealer(), typeOfPSF, dispositionHistory,
							interactionid, uniqueid, getUserLogindealerCode(), dealername, userName, customerData,
							vehicleData, userdata, service, templates, complaintOFCust, statesList,lastPSFAssignStatus,
							psf4thDayDisposition.render(service, vehicleData, userName, dealername, psfAgeAbove2.render())));
				} else {

					return ok(commonPSFDispositionPage.render(getOEMOfDealer(), typeOfPSF, dispositionHistory,
							interactionid, uniqueid, getUserLogindealerCode(), dealername, userName, customerData,
							vehicleData, userdata, service, templates, complaintOFCust, statesList,lastPSFAssignStatus,
							psf4thDayDisposition.render(service, vehicleData, userName, dealername, psfAgeBelow2.render())));
				}

			} else {

				return forbidden(views.html.error403.render());
			}
		} else if (getOEMOfDealer().equals("HYUNDAI")) {

			if (typeOfPSF == 7) {

				return ok(commonPSFDispositionPage.render(getOEMOfDealer(), typeOfPSF, dispositionHistory,
						interactionid, uniqueid, getUserLogindealerCode(), dealername, userName, customerData,
						vehicleData, userdata, service, templates, complaintOFCust, statesList,lastPSFAssignStatus,
						psf3rdDayDispo.render(vehicleData, userName, dealername, service)));

			} else if (typeOfPSF == 5) {

				if (customerData.getSegment() != null) {

					List<Segment> seg = customerData.getSegment();

					String custType = "";

					for (Segment se : seg) {

						if (se.getType() != null) {

							custType = se.getType();

						}

					}

					if (custType.equals("GOLD")) {
						return ok(commonPSFDispositionPage.render(getOEMOfDealer(), typeOfPSF, dispositionHistory,
								interactionid, uniqueid, getUserLogindealerCode(), dealername, userName, customerData,
								vehicleData, userdata, service, templates, complaintOFCust, statesList,lastPSFAssignStatus,
								psf15thDayGoldDispo.render(vehicleData, userName, dealername, service)));
					} else {
						return ok(commonPSFDispositionPage.render(getOEMOfDealer(), typeOfPSF, dispositionHistory,
								interactionid, uniqueid, getUserLogindealerCode(), dealername, userName, customerData,
								vehicleData, userdata, service, templates, complaintOFCust, statesList,lastPSFAssignStatus,
								psf15thDayNonGoldDispo.render(vehicleData, userName, dealername, service)));

					}

				} else {

					return ok(commonPSFDispositionPage.render(getOEMOfDealer(), typeOfPSF, dispositionHistory,
							interactionid, uniqueid, getUserLogindealerCode(), dealername, userName, customerData,
							vehicleData, userdata, service, templates, complaintOFCust, statesList,lastPSFAssignStatus,
							psf15thDayNonGoldDispo.render(vehicleData, userName, dealername, service)));

				}
			} else {

				return forbidden(views.html.error403.render());
			}

		}else if (getOEMOfDealer().equals("FORD")) {

			if (typeOfPSF == 4) {

				return ok(commonPSFDispositionPage.render(getOEMOfDealer(), typeOfPSF, dispositionHistory,
						interactionid, uniqueid, getUserLogindealerCode(), dealername, userName, customerData,
						vehicleData, userdata, service, templates, complaintOFCust, statesList,lastPSFAssignStatus,
						psf2ndDispoFord.render(vehicleData, userName, dealername, service)));
			} else {

				return forbidden(views.html.error403.render());
			}
		}  else {

			return forbidden(views.html.error403.render());
		}

	}

	@Secure(clients = "FormClient")
	public Result postCommonPSFDispositionPage() {

		Form<DispositionPage> type_data = Form.form(DispositionPage.class).bindFromRequest();
		DispositionPage pageType = type_data.get();

		long psfType = pageType.getTypeOfPSF();
		String oemIs = pageType.getOem();

		Form<PSFInteraction> form = Form.form(PSFInteraction.class).bindFromRequest();
		PSFInteraction psf_interaction = form.get();

		Form<CallDispositionData> dispoform = Form.form(CallDispositionData.class).bindFromRequest();
		CallDispositionData call_dispo = dispoform.get();

		Form<ListingForm> listform = Form.form(ListingForm.class).bindFromRequest();
		ListingForm list_dispo = listform.get();

		Form<PickupDrop> pick_up_data = Form.form(PickupDrop.class).bindFromRequest();
		PickupDrop pick_up = pick_up_data.get();

		if (oemIs.equals("MARUTHI")) {

			if (psfType == 4) {

				logger.info(" Dispositon is : " + psfType);
				if (psf_interaction.getModeOfServiceDone().equals("Self Drive-in")) {

					psf_interaction.setSatisfiedWithWashing(list_dispo.getSatisfiedWithWashingSelfD());
					psf_interaction.setVehiclePerformanceAfterService(psf_interaction.getSelfDriveInFeedBack());
					if (psf_interaction.getIsSatisfiedWithServiceProvided() != null) {

						if (psf_interaction.getIsSatisfiedWithServiceProvided().equals("Dissatisfied")) {

							psf_interaction.setRemarks(list_dispo.getRemarksDissatisfied());

						}

					}

					if (psf_interaction.getIsDeliveredAsPromisedTime().equals("Yes")) {
						psf_interaction.setDelayInHours(0);
						psf_interaction.setDelayInMin(0);

					}

					psfRepo.addPSFFeedBackFormData(psf_interaction, call_dispo, list_dispo, getUserLogindealerCode(),
							pick_up);

				} else if (psf_interaction.getModeOfServiceDone().equals("Pick-Up")) {
					psf_interaction
							.setChargesInfoExplainedBeforeService(list_dispo.getChargesInfoExplainedBeforeServiceMMS());
					psf_interaction
							.setIsChargesAndRepairAsMentioned(list_dispo.getIsChargesAndRepairAsMentionedPickup());
					psf_interaction.setIsChargesAsEstimated(list_dispo.getIsChargesAsEstimatedpickUp());
					psf_interaction.setSatisfiedWithWashing(list_dispo.getSatisfiedWithWashingPickup());
					psf_interaction.setVehiclePerformanceAfterService(list_dispo.getVehiclePerformancePickUp());
					psf_interaction
							.setIsDemandOfSeriviceDoneInLastVisit(list_dispo.getIsDemandOfSerDoneInLVisitpickup());
					psf_interaction.setDelayInHours(0);
					psf_interaction.setDelayInMin(0);
					psf_interaction.setRemarks(list_dispo.getRemarksPickUp());
					psfRepo.addPSFFeedBackFormData(psf_interaction, call_dispo, list_dispo, getUserLogindealerCode(),
							pick_up);

				} else {

					if (psf_interaction.getModeOfServiceDone().equals("MMS")) {

						psf_interaction.setVehiclePerformanceAfterService(psf_interaction.getVehiclePerformance());

					}
					psf_interaction.setDelayInHours(0);
					psf_interaction.setDelayInMin(0);
					psfRepo.addPSFFeedBackFormData(psf_interaction, call_dispo, list_dispo, getUserLogindealerCode(),
							pick_up);
				}

				psfRepo.addUpsellLeadOfPSF(psf_interaction, list_dispo);

				return ok(psfListForFeedBackOfService.render(getUserLogindealerCode(), getDealerName(),
						getUserLoginName(), getOEMOfDealer()));

			} else if (psfType == 5) {
				logger.info(" Dispositon is : " + psfType);
				psfRepo.addPSFFeedBackFormData(psf_interaction, call_dispo, list_dispo, getUserLogindealerCode(),
						pick_up);
				psfRepo.addUpsellLeadOfPSF(psf_interaction, list_dispo);
				return ok(psfList15ForFeedBackOfService.render(getUserLogindealerCode(), getDealerName(),
						getUserLoginName(), getOEMOfDealer()));

			} else if (psfType == 6) {
				logger.info(" Dispositon is : " + psfType);

				psfRepo.addPSFFeedBackFormData(psf_interaction, call_dispo, list_dispo, getUserLogindealerCode(),
						pick_up);

				psfRepo.addUpsellLeadOfPSF(psf_interaction, list_dispo);

				return ok(psfList30thForFeedBackOfService.render(getUserLogindealerCode(), getDealerName(),
						getUserLoginName(), getOEMOfDealer()));

			} else {
				logger.info(" Dispositon is : " + psfType);

				return forbidden(views.html.error403.render());
			}

		} else if (oemIs.equals("HYUNDAI")) {

			if (psfType == 7) {
				logger.info("Dispositon is : " + psfType);
				psfRepo.addPSFFeedBackFormData(psf_interaction, call_dispo, list_dispo, getUserLogindealerCode(),
						pick_up);
				psfRepo.addUpsellLeadOfPSF(psf_interaction, list_dispo);

				return ok(PSFList3rdForFeedbackOfService.render(getUserLogindealerCode(), getDealerName(),
						getUserLoginName(), getOEMOfDealer()));

			} else if (psfType == 5) {
				logger.info(" Dispositon is : " + psfType);

				psfRepo.addPSFFeedBackFormData(psf_interaction, call_dispo, list_dispo, getUserLogindealerCode(),
						pick_up);

				psfRepo.addUpsellLeadOfPSF(psf_interaction, list_dispo);

				return ok(psfList15ForFeedBackOfService.render(getUserLogindealerCode(), getDealerName(),
						getUserLoginName(), getOEMOfDealer()));

			} else {
				logger.info(" Dispositon is : " + psfType);

				return forbidden(views.html.error403.render());
			}

		} else if (oemIs.equals("MAHINDRA")) {

			if (psfType == 4) {
				logger.info(" Dispositon is : " + psfType);

				if ((psf_interaction.getqM4_confirmingCustomer() != null
						&& psf_interaction.getqM4_confirmingCustomer().equals("No"))
						|| (psf_interaction.getqM4_confirmingCustomerRightTime() != null
								&& psf_interaction.getqM4_confirmingCustomerRightTime().equals("No"))) {

					call_dispo.setDisposition("Call Me Later");
				}
				if (psf_interaction.getqM1_SatisfiedWithQualityOfServ() != null
						&& psf_interaction.getqM1_SatisfiedWithQualityOfServ().equals("Yes")) {

					call_dispo.setDisposition("PSF_Yes");

				}
				if (psf_interaction.getqM1_SatisfiedWithQualityOfServ() != null
						&& psf_interaction.getqM1_SatisfiedWithQualityOfServ().equals("No")) {

					if ((psf_interaction.getPsfAppointmentDate() != null) || (pick_up.getPickupDate() != null)) {

						call_dispo.setDisposition("Book Appointment");
					} else if (psf_interaction.getqM3_SubReasonOfAreaOfImprovement1() != null && psf_interaction
							.getqM3_SubReasonOfAreaOfImprovement1().equals("Car is required at workshop")) {

						logger.info("Car is required at workshop");
						call_dispo.setDisposition("Book Appointment");
					} else if (psf_interaction.getqM3_SubReasonOfAreaOfImprovement2() != null && psf_interaction
							.getqM3_SubReasonOfAreaOfImprovement2().equals("Car is required at workshop")) {

						logger.info("Car is required at workshop 2");
						call_dispo.setDisposition("Book Appointment");
					}
				}

				psfRepo.addPSFFeedBackFormData(psf_interaction, call_dispo, list_dispo, getUserLogindealerCode(),
						pick_up);
				psfRepo.addUpsellLeadOfPSF(psf_interaction, list_dispo);

				return ok(psfListNextDayFeedBack.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(),
						getOEMOfDealer()));

			} else if (psfType == 5) {
				logger.info(" Dispositon is : " + psfType);

				if ((psf_interaction.getqM4_confirmingCustomer() != null
						&& psf_interaction.getqM4_confirmingCustomer().equals("No"))
						|| (psf_interaction.getqM4_RightTimeToEnquireOrderbillDate() != null
								&& psf_interaction.getqM4_RightTimeToEnquireOrderbillDate().equals("No"))) {

					call_dispo.setDisposition("Call Me Later");
				}
				if (psf_interaction.getqM4_RightTimeToEnquireOrderbillDate() != null
						&& psf_interaction.getqM4_RightTimeToEnquireOrderbillDate().equals("Yes")) {

					call_dispo.setDisposition("PSF_Yes");
					logger.info("list_dispo.getCommercialVeh() : " + list_dispo.getCommercialVeh());

					if (list_dispo.getCommercialVeh().equals("false")) {

						if (list_dispo.getAgeOfVehicleIS() <= 730) {

							if (Long.parseLong(psf_interaction.getqM1RatingOverAllAppointProcess()) <= 8
									|| Long.parseLong(psf_interaction.getqM2RatingTimeTakenVehiclehandOver()) <= 8
									|| Long.parseLong(psf_interaction.getqM3RatingServicAdvCourtesyResponse()) <= 8
									|| Long.parseLong(psf_interaction.getqM4RatingServiceAdvJobExpl()) <= 8
									|| Long.parseLong(psf_interaction.getqM5RatingCleanlinessOfDealer()) <= 8
									|| Long.parseLong(psf_interaction.getqM6RatingTimelinessVehicleDelivary()) <= 8
									|| Long.parseLong(psf_interaction.getqM7RatingFairnessOfCharge()) <= 8
									|| Long.parseLong(psf_interaction.getqM8RatingHelpFulnessOfStaff()) <= 8
									|| Long.parseLong(psf_interaction.getqM9RatingTotalTimeRequired()) <= 8
									|| Long.parseLong(psf_interaction.getqM10RatingQualityOfMaintenance()) <= 8
									|| Long.parseLong(psf_interaction.getqM11RatingCondAndCleanlinessOfVeh()) <= 8
									|| Long.parseLong(psf_interaction.getqM12RatingOverAllServExp()) <= 8) {

								call_dispo.setDisposition("Dissatisfied with PSF");

							}
						} else {

							if (Long.parseLong(psf_interaction.getqM1RatingOverAllAppointProcess()) <= 8
									|| Long.parseLong(psf_interaction.getqM2RatingTimeTakenVehiclehandOver()) <= 8
									|| Long.parseLong(psf_interaction.getqM3RatingServicAdvCourtesyResponse()) <= 8
									|| Long.parseLong(psf_interaction.getqM4RatingServiceAdvJobExpl()) <= 8
									|| Long.parseLong(psf_interaction.getqM7RatingFairnessOfCharge()) <= 8
									|| Long.parseLong(psf_interaction.getqM9RatingTotalTimeRequired()) <= 8
									|| Long.parseLong(psf_interaction.getqM10RatingQualityOfMaintenance()) <= 8
									|| Long.parseLong(psf_interaction.getqM11RatingCondAndCleanlinessOfVeh()) <= 8
									|| Long.parseLong(psf_interaction.getqM12RatingOverAllServExp()) <= 8) {

								call_dispo.setDisposition("Dissatisfied with PSF");

							}

						}
					} else {

						if ((psf_interaction.getqMC1_OverallServiceExp().equals("Not Happy")
								|| psf_interaction.getqMC1_OverallServiceExp().equals("No Ratings"))
								|| (psf_interaction.getqMC2_HappyWithRepairJob().equals("Not Happy")
										|| psf_interaction.getqMC2_HappyWithRepairJob().equals("No Ratings"))
								|| (psf_interaction.getqMC3_CoutnessHelpfulnessOfSA().equals("Not Happy")
										|| psf_interaction.getqMC3_CoutnessHelpfulnessOfSA().equals("No Ratings"))
								|| (psf_interaction.getqMC4_TimeTakenCompleteJob().equals("Not Happy")
										|| psf_interaction.getqMC4_TimeTakenCompleteJob().equals("No Ratings"))
								|| (psf_interaction.getqMC5_CostEstimateAdherance().equals("Not Happy")
										|| psf_interaction.getqMC5_CostEstimateAdherance().equals("No Ratings"))
								|| (psf_interaction.getqMC6_ServiceConvinenantTime().equals("Not Happy")
										|| psf_interaction.getqMC6_ServiceConvinenantTime().equals("No Ratings"))
								|| (psf_interaction.getqMC7_LikeToRevistDealer().equals("Not Happy")
										|| psf_interaction.getqMC7_LikeToRevistDealer().equals("No Ratings"))) {

							call_dispo.setDisposition("Dissatisfied with PSF");
						}
					}

				} else if (psf_interaction.getqM4_RightTimeToEnquireOrderbillDate() != null
						&& psf_interaction.getqM4_RightTimeToEnquireOrderbillDate().equals("No")) {

					call_dispo.setDisposition("Call Me Later");
				}

				if (psf_interaction.getPsfAppointmentDate() != null) {
					logger.info("Incomplete Survey");
					call_dispo.setDisposition("Incomplete Survey");
				}

				psfRepo.addPSFFeedBackFormData(psf_interaction, call_dispo, list_dispo, getUserLogindealerCode(),
						pick_up);

				psfRepo.addUpsellLeadOfPSF(psf_interaction, list_dispo);

				return ok(psfList4thFeedBackOfService.render(getUserLogindealerCode(), getDealerName(),
						getUserLoginName(), getOEMOfDealer()));
			} else {
				logger.info(" Dispositon is : " + psfType);

				return forbidden(views.html.error403.render());
			}

		}else if (oemIs.equals("FORD")) {

			if (psfType == 4) {
				logger.info(" Dispositon is : " + psfType);
				if(call_dispo.getDisposition()!=null && call_dispo.getDisposition().equals("PSF_Yes")){
				
				if (psf_interaction.getPsfAppointmentDate() != null) {
					logger.info("Incomplete Survey");
					call_dispo.setDisposition("Incomplete Survey");
				}else if (Long.parseLong(psf_interaction.getqFordQ1()) <= 8
						|| Long.parseLong(psf_interaction.getqFordQ2()) <= 8
						|| Long.parseLong(psf_interaction.getqFordQ3()) <= 8
						|| Long.parseLong(psf_interaction.getqFordQ4()) <= 8
						|| psf_interaction.getqFordQ5()== "No"
						|| psf_interaction.getqFordQ6()== "No"
						|| Long.parseLong(psf_interaction.getqFordQ7()) <= 8
						|| psf_interaction.getqFordQ8()== "No"
						|| psf_interaction.getqFordQ9()== "No"					
						|| Long.parseLong(psf_interaction.getqFordQ10()) <= 8
						|| psf_interaction.getqFordQ11()== "No"
						|| psf_interaction.getqFordQ12()== "No"
						|| Long.parseLong(psf_interaction.getqFordQ13()) <= 8
						|| psf_interaction.getqFordQ14()== "No"
						|| Long.parseLong(psf_interaction.getqFordQ15()) <= 8
						|| psf_interaction.getqFordQ16()== "No") {

					call_dispo.setDisposition("Dissatisfied with PSF");

				}
				}


				psfRepo.addPSFFeedBackFormData(psf_interaction, call_dispo, list_dispo, getUserLogindealerCode(),
						pick_up);

				psfRepo.addUpsellLeadOfPSF(psf_interaction, list_dispo);

				return ok(psfList2ndDayFord.render(getUserLogindealerCode(), getDealerName(),
						getUserLoginName(), getOEMOfDealer()));

			} else {
				logger.info(" Dispositon is : " + psfType);

				return forbidden(views.html.error403.render());
			}

		} else {

			return forbidden(views.html.error403.render());
		}
	}

	@Secure(clients = "FormClient")
	public Result assignedInteractionPSFData(String name) {

		logger.info("psf name for filter : " + name);
		String typeOfPSF = "4";

		if (name.equals("psf6thday")) {

			typeOfPSF = "4";

		} else if (name.equals("psf15thday")) {

			typeOfPSF = "5";

		} else if (name.equals("psf30thday")) {

			typeOfPSF = "6";

		} else if (name.equals("psf3rdday")) {

			typeOfPSF = "7";
		} else if (name.equals("psf1stday")) {

			typeOfPSF = "4";
		} else if (name.equals("psf4thday")) {

			typeOfPSF = "5";

		}

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String fromBillDate = "";
		String toBillDate = "";
		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromBillDate = paramMap.get("columns[1][search][value]")[0];
			logger.info("inside from bill date:" + fromBillDate);
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toBillDate = paramMap.get("columns[2][search][value]")[0];
		}
		if (searchPattern.length() == 0) {
			allflag = true;
		}

		logger.info("from Bil date:" + fromBillDate);
		logger.info("to bill date:" + toBillDate);
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		// long totalSize =
		// psfRepo.PSFServiceReminderwithSearchCount(userdata.getId(),
		// typeOfPSF);
		long totalSize = psfRepo.PSFScheduledCallsCount(fromBillDate, toBillDate, typeOfPSF, searchPattern,
				userdata.getId(), fromIndex, toIndex);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		// List<CallLogAjaxLoad> assignedList =
		// psfRepo.PSFServiceReminderwithSearch(searchPattern,
		// userdata.getId(),fromIndex, toIndex, typeOfPSF);
		List<CallLogAjaxLoad> assignedList = psfRepo.PSFScheduledCalls(fromBillDate, toBillDate, typeOfPSF,
				searchPattern, userdata.getId(), fromIndex, toIndex);

		if (!allflag) {
			patternCount = assignedList.size();
			logger.info("patternCount of assignedList : " + patternCount);

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}

		ArrayNode an = result.putArray("data");

		long dispositionHistory = 0;
		for (CallLogAjaxLoad c : assignedList) {

			String urlDisposition = "<a href='/CRE/psfDispo/" + c.getCustomer_id() + "/" + c.getVehicle_id() + "/"
					+ c.getAssigned_intercation_id() + "/" + dispositionHistory + "/" + typeOfPSF
					+ "'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			//logger.info("urlDisposition of PSF is : " + urlDisposition);

			//logger.info("psf date is: "+c.getCategory());
			
			ObjectNode row = Json.newObject();
			row.put("0", c.getCustomer_name());
			row.put("1", c.getMobile_number());
			row.put("2", c.getVehicle_RegNo());
			row.put("3", c.getModel());
			row.put("4", c.getRONumber());
			row.put("5", String.valueOf(c.getROdate()));
			row.put("6", String.valueOf(c.getBillDate()));
			row.put("7", c.getCategory());
			row.put("8", urlDisposition);

			an.add(row);
		}

		return ok(result);
	}

	// FollowUp Required LIST DATA
	@Secure(clients = "FormClient")
	public Result ajaxCallForFollowUpRequiredPSFData(String psfDay) {

		logger.info("psf name for filter : " + psfDay);
		String typeOfPSF = "4";

		if (psfDay.equals("psf6thday")) {

			typeOfPSF = "4";

		} else if (psfDay.equals("psf15thday")) {

			typeOfPSF = "5";

		} else if (psfDay.equals("psf30thday")) {

			typeOfPSF = "6";

		} else if (psfDay.equals("psf3rdday")) {

			typeOfPSF = "7";
		} else if (psfDay.equals("psf1stday")) {

			typeOfPSF = "4";
		} else if (psfDay.equals("psf4thday")) {

			typeOfPSF = "5";

		}

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String fromBillDate = "";
		String toBillDate = "";
		long dispoType = 4;
		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromBillDate = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toBillDate = paramMap.get("columns[2][search][value]")[0];
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		// long totalSize =
		// psfRepo.PSFFollowUPRequiredwithSearchCount(userdata.getId(),
		// typeOfPSF);
		long totalSize = psfRepo.PSFCountForFollowUpCompleteSurveyAppointments(fromBillDate, toBillDate, typeOfPSF,
				searchPattern, userdata.getId(), dispoType);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		List<CallLogDispositionLoad> assignedList = psfRepo.PSFCallsFollowUpCompleteSurveyAppointments(fromBillDate,
				toBillDate, typeOfPSF, searchPattern, userdata.getId(), dispoType, fromIndex, toIndex);

		if (!allflag) {
			patternCount = assignedList.size();
			logger.info("patternCount of assignedList : " + patternCount);

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}

		ArrayNode an = result.putArray("data");

		long dispositionHistory = 1;
		for (CallLogDispositionLoad c : assignedList) {

			String urlDisposition = "<a href='/CRE/psfDispo/" + c.getCustomer_id() + "/" + c.getVehicle_id() + "/"
					+ c.getCallinteraction_id() + "/" + dispositionHistory + "/" + typeOfPSF
					+ "'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";

			ObjectNode row = Json.newObject();
			row.put("0", c.getFollowUpDate());
			row.put("1", c.getFollowUpTime());
			row.put("2", String.valueOf(c.getCall_date()));
			row.put("3", c.getCustomer_name());
			row.put("4", c.getMobile_number());
			row.put("5", c.getVehicle_RegNo());
			row.put("6", c.getModel());
			row.put("7", String.valueOf(c.getROdate()));
			row.put("8", c.getRONumber());
			row.put("9", String.valueOf(c.getBillDate()));
			row.put("10", urlDisposition);
			an.add(row);
		}

		return ok(result);
	}

	@Secure(clients = "FormClient")
	public Result ajaxCallForCompletedSurveyPSFData(String psfDay) {

		logger.info("psf name for filter : " + psfDay);
		String typeOfPSF = "4";

		if (psfDay.equals("psf6thday")) {

			typeOfPSF = "4";

		} else if (psfDay.equals("psf15thday")) {

			typeOfPSF = "5";

		} else if (psfDay.equals("psf30thday")) {

			typeOfPSF = "6";

		} else if (psfDay.equals("psf3rdday")) {

			typeOfPSF = "7";
		} else if (psfDay.equals("psf1stday")) {

			typeOfPSF = "4";
		} else if (psfDay.equals("psf4thday")) {

			typeOfPSF = "5";

		}

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String fromBillDate = "";
		String toBillDate = "";
		long dispoType = 22;

		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromBillDate = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toBillDate = paramMap.get("columns[2][search][value]")[0];
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		// long totalSize = psfRepo.PSF_Servey_completeCount(userdata.getId(),
		// typeOfPSF);
		long totalSize = psfRepo.PSFCountForFollowUpCompleteSurveyAppointments(fromBillDate, toBillDate, typeOfPSF,
				searchPattern, userdata.getId(), dispoType);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		List<CallLogDispositionLoad> assignedList = psfRepo.PSFCallsFollowUpCompleteSurveyAppointments(fromBillDate,
				toBillDate, typeOfPSF, searchPattern, userdata.getId(), dispoType, fromIndex, toIndex);

		if (!allflag) {
			patternCount = assignedList.size();
			logger.info("patternCount of assignedList : " + patternCount);

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");
		long dispositionHistory = 1;
		for (CallLogDispositionLoad c : assignedList) {

			String urlDisposition = "<a href='/CRE/psfDispo/" + c.getCustomer_id() + "/" + c.getVehicle_id() + "/"
					+ c.getCallinteraction_id() + "/" + dispositionHistory + "/" + typeOfPSF
					+ "'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";

			ObjectNode row = Json.newObject();
			row.put("0", String.valueOf(c.getServey_date()));
			row.put("1", c.getCustomer_name());
			row.put("2", c.getCategory());
			row.put("3", c.getVehicle_RegNo());
			row.put("4", c.getModel());
			row.put("5", String.valueOf(c.getROdate()));
			row.put("6", c.getRONumber());
			row.put("7", String.valueOf(c.getBillDate()));
			row.put("8", urlDisposition);

			an.add(row);
		}

		return ok(result);
	}

	@Secure(clients = "FormClient")
	public Result ajaxCallForDissatisfiedPSFData(String psfDay) {

		logger.info("psf name for ajaxCallForDissatisfiedPSFData : " + psfDay);
		String typeOfPSF = "4";

		if (psfDay.equals("psf6thday")) {

			typeOfPSF = "4";

		} else if (psfDay.equals("psf15thday")) {

			typeOfPSF = "5";

		} else if (psfDay.equals("psf30thday")) {

			typeOfPSF = "6";

		} else if (psfDay.equals("psf3rdday")) {

			typeOfPSF = "7";
		} else if (psfDay.equals("psf1stday")) {

			typeOfPSF = "4";
		} else if (psfDay.equals("psf4thday")) {

			typeOfPSF = "5";

		}

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String fromBillDate = "";
		String toBillDate = "";
		long dispoType = 44;

		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromBillDate = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toBillDate = paramMap.get("columns[2][search][value]")[0];
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		// long totalSize = psfRepo.PSF_Servey_completeCount(userdata.getId(),
		// typeOfPSF);
		long totalSize = psfRepo.PSFCountForFollowUpCompleteSurveyAppointments(fromBillDate, toBillDate, typeOfPSF,
				searchPattern, userdata.getId(), dispoType);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		List<CallLogDispositionLoad> assignedList = psfRepo.PSFCallsFollowUpCompleteSurveyAppointments(fromBillDate,
				toBillDate, typeOfPSF, searchPattern, userdata.getId(), dispoType, fromIndex, toIndex);

		if (!allflag) {
			patternCount = assignedList.size();
			logger.info("patternCount of assignedList : " + patternCount);

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");
		long dispositionHistory = 1;
		for (CallLogDispositionLoad c : assignedList) {

			String urlDisposition = "<a href='/CRE/psfDispo/" + c.getCustomer_id() + "/" + c.getVehicle_id() + "/"
					+ c.getCallinteraction_id() + "/" + dispositionHistory + "/" + typeOfPSF
					+ "'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";

			ObjectNode row = Json.newObject();
			row.put("0", String.valueOf(c.getServey_date()));
			row.put("1", c.getCustomer_name());
			row.put("2", c.getCategory());
			row.put("3", c.getVehicle_RegNo());
			row.put("4", c.getModel());
			row.put("5", String.valueOf(c.getROdate()));
			row.put("6", c.getRONumber());
			row.put("7", String.valueOf(c.getBillDate()));
			row.put("8", urlDisposition);

			an.add(row);
		}

		return ok(result);
	}

	@Secure(clients = "FormClient")
	public Result ajaxCallForNonContactsPSFData(String psfDay) {

		logger.info("psf name for filter : " + psfDay);
		String typeOfPSF = "4";

		if (psfDay.equals("psf6thday")) {
			typeOfPSF = "4";
		} else if (psfDay.equals("psf15thday")) {
			typeOfPSF = "5";
		} else if (psfDay.equals("psf30thday")) {
			typeOfPSF = "6";

		} else if (psfDay.equals("psf3rdday")) {

			typeOfPSF = "7";
		} else if (psfDay.equals("psf1stday")) {
			typeOfPSF = "4";
		} else if (psfDay.equals("psf4thday")) {

			typeOfPSF = "5";

		}

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String fromBillDate = "";
		String toBillDate = "";
		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromBillDate = paramMap.get("columns[1][search][value]")[0];
			logger.info("from date" + fromBillDate);
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toBillDate = paramMap.get("columns[2][search][value]")[0];
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		long dispoType = 1;

		long totalSize = psfRepo.PSFNoncontactsCount(fromBillDate, toBillDate, typeOfPSF, searchPattern,
				userdata.getId(), dispoType);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		List<CallLogDispositionLoad> assignedList = psfRepo.PSFNoncontacts(fromBillDate, toBillDate, typeOfPSF,
				searchPattern, userdata.getId(), dispoType, fromIndex, toIndex);

		if (!allflag) {
			patternCount = assignedList.size();
			logger.info("patternCount of assignedList : " + patternCount);

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");
		long dispositionHistory = 1;
		for (CallLogDispositionLoad c : assignedList) {

			String urlDisposition = "<a href='/CRE/psfDispo/" + c.getCustomer_id() + "/" + c.getVehicle_id() + "/"
					+ c.getCallinteraction_id() + "/" + dispositionHistory + "/" + typeOfPSF
					+ "'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";
			logger.info("last dispo date is : "+c.getFollowupdate());
			
			ObjectNode row = Json.newObject();
			row.put("0", c.getCustomer_name());
			row.put("1", c.getCategory());
			row.put("2", c.getMobile_number());
			row.put("3", c.getVehicle_RegNo());
			row.put("4", c.getModel());
			row.put("5", String.valueOf(c.getROdate()));
			row.put("6", c.getRONumber());
			row.put("7", String.valueOf(c.getBillDate()));
			row.put("8", c.getFollowUpDate());
			row.put("9", String.valueOf(c.getCall_date()));
			row.put("10", urlDisposition);
			an.add(row);
		}

		return ok(result);
	}

	@Secure(clients = "FormClient")
	public Result ajaxCallForDroppedCallsPSFData(String psfDay) {

		logger.info("psf name for filter : " + psfDay);
		String typeOfPSF = "4";

		if (psfDay.equals("psf6thday")) {

			typeOfPSF = "4";

		} else if (psfDay.equals("psf15thday")) {

			typeOfPSF = "5";

		} else if (psfDay.equals("psf30thday")) {

			typeOfPSF = "6";

		} else if (psfDay.equals("psf3rdday")) {

			typeOfPSF = "7";

		} else if (psfDay.equals("psf1stday")) {

			typeOfPSF = "4";
		} else if (psfDay.equals("psf4thday")) {

			typeOfPSF = "5";

		}

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String fromBillDate = "";
		String toBillDate = "";
		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}

		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		long dispoType = 2;
		long totalSize = psfRepo.PSFNoncontactsCount(fromBillDate, toBillDate, typeOfPSF, searchPattern,
				userdata.getId(), dispoType);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}
		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		List<CallLogDispositionLoad> assignedList = psfRepo.PSFNoncontacts(fromBillDate, toBillDate, typeOfPSF,
				searchPattern, userdata.getId(), dispoType, fromIndex, toIndex);

		if (!allflag) {
			patternCount = assignedList.size();
			logger.info("patternCount of assignedList : " + patternCount);

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");
		long dispositionHistory = 1;
		for (CallLogDispositionLoad c : assignedList) {

			ObjectNode row = Json.newObject();
			row.put("0", c.getCustomer_name());
			row.put("1", c.getCategory());
			row.put("2", c.getMobile_number());
			row.put("3", c.getVehicle_RegNo());
			row.put("4", c.getModel());
			row.put("5", String.valueOf(c.getROdate()));
			row.put("6", c.getRONumber());
			row.put("7", String.valueOf(c.getBillDate()));
			row.put("8", c.getFollowUpDate());
			row.put("9", String.valueOf(c.getCall_date()));
			an.add(row);

		}

		return ok(result);
	}

	// incomplete survey
	@Secure(clients = "FormClient")
	public Result ajaxCallForIncompletedSurveyPSFData(String psfDay) {

		logger.info("psf name for filter : " + psfDay);
		String typeOfPSF = "4";

		if (psfDay.equals("psf6thday")) {

			typeOfPSF = "4";

		} else if (psfDay.equals("psf15thday")) {

			typeOfPSF = "5";

		} else if (psfDay.equals("psf30thday")) {

			typeOfPSF = "6";

		} else if (psfDay.equals("psf3rdday")) {

			typeOfPSF = "7";
		} else if (psfDay.equals("psf1stday")) {

			typeOfPSF = "4";
		} else if (psfDay.equals("psf4thday")) {

			typeOfPSF = "5";

		}

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String fromBillDate = "";
		String toBillDate = "";
		long dispoType = 36;
		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromBillDate = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toBillDate = paramMap.get("columns[2][search][value]")[0];
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		// long totalSize =
		// psfRepo.PSFFollowUPRequiredwithSearchCount(userdata.getId(),
		// typeOfPSF);
		long totalSize = psfRepo.PSFCountForFollowUpCompleteSurveyAppointments(fromBillDate, toBillDate, typeOfPSF,
				searchPattern, userdata.getId(), dispoType);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		List<CallLogDispositionLoad> assignedList = psfRepo.PSFCallsFollowUpCompleteSurveyAppointments(fromBillDate,
				toBillDate, typeOfPSF, searchPattern, userdata.getId(), dispoType, fromIndex, toIndex);

		if (!allflag) {
			patternCount = assignedList.size();
			logger.info("patternCount of assignedList : " + patternCount);

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}

		ArrayNode an = result.putArray("data");

		long dispositionHistory = 1;
		for (CallLogDispositionLoad c : assignedList) {

			String urlDisposition = "<a href='/CRE/psfDispo/" + c.getCustomer_id() + "/" + c.getVehicle_id() + "/"
					+ c.getCallinteraction_id() + "/" + dispositionHistory + "/" + typeOfPSF
					+ "'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";

			ObjectNode row = Json.newObject();
			row.put("0", String.valueOf(c.getPsfAppointmentDate()));
			row.put("1", c.getPsfAppointmentTime());
			row.put("2", String.valueOf(c.getCall_date()));
			row.put("3", c.getCustomer_name());
			row.put("4", c.getMobile_number());
			row.put("5", c.getVehicle_RegNo());
			row.put("6", c.getModel());
			row.put("7", String.valueOf(c.getROdate()));
			row.put("8", c.getRONumber());
			row.put("9", String.valueOf(c.getBillDate()));
			row.put("10", urlDisposition);
			an.add(row);
		}

		return ok(result);
	}

	// appointments
	@Secure(clients = "FormClient")
	public Result ajaxCallForAppointmentPSFData(String psfDay) {
		logger.info("psf name for filter : " + psfDay);
		String typeOfPSF = "4";

		if (psfDay.equals("psf6thday")) {

			typeOfPSF = "4";

		} else if (psfDay.equals("psf15thday")) {

			typeOfPSF = "5";

		} else if (psfDay.equals("psf30thday")) {

			typeOfPSF = "6";

		} else if (psfDay.equals("psf3rdday")) {

			typeOfPSF = "7";
		} else if (psfDay.equals("psf1stday")) {

			typeOfPSF = "4";
		} else if (psfDay.equals("psf4thday")) {

			typeOfPSF = "5";

		}

		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String searchPattern = "";
		String fromBillDate = "";
		String toBillDate = "";
		long dispoType = 25;

		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			fromBillDate = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			toBillDate = paramMap.get("columns[2][search][value]")[0];
		}
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		// long totalSize = psfRepo.PSF_Servey_completeCount(userdata.getId(),
		// typeOfPSF);
		long totalSize = psfRepo.PSFCountForFollowUpCompleteSurveyAppointments(fromBillDate, toBillDate, typeOfPSF,
				searchPattern, userdata.getId(), dispoType);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		List<CallLogDispositionLoad> assignedList = psfRepo.PSFCallsFollowUpCompleteSurveyAppointments(fromBillDate,
				toBillDate, typeOfPSF, searchPattern, userdata.getId(), dispoType, fromIndex, toIndex);

		if (!allflag) {
			patternCount = assignedList.size();
			logger.info("patternCount of assignedList : " + patternCount);

		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");
		long dispositionHistory = 1;
		for (CallLogDispositionLoad c : assignedList) {

			String urlDisposition = "<a href='/CRE/psfDispo/" + c.getCustomer_id() + "/" + c.getVehicle_id() + "/"
					+ c.getCallinteraction_id() + "/" + dispositionHistory + "/" + typeOfPSF
					+ "'><i class=\"fa fa-pencil-square\" data-toggel=\"tooltip\" title=\"Disposition\" style=\"font-size:30px;color:#DD4B39;\"></i></a>";

			ObjectNode row = Json.newObject();
			row.put("0", c.getCustomer_name());
			row.put("1", c.getVehicle_RegNo());
			row.put("2", String.valueOf(c.getPsfAppointmentDate()));
			row.put("3", c.getMobile_number());
			row.put("4", String.valueOf(c.getCall_date()));
			row.put("5", c.getPsfAppointmentTime());
			row.put("6", c.getRONumber());
			row.put("7", String.valueOf(c.getROdate()));
			row.put("8", String.valueOf(c.getBillDate()));
			row.put("9", String.valueOf(c.getServey_date()));
			row.put("10", c.getModel());
			row.put("11", urlDisposition);
			an.add(row);
		}

		return ok(result);
	}

	// PSF Notification

	@Secure(clients = "FormClient")
	public Result getPSFFollowUpNotificationToday() {
		// String dealercode = getUserLogindealerCode();
		String cre = getUserLoginName();
		System.out.println(cre);
		WyzUser userdata = wyzRepo.getUserbyUserName(cre);
		System.out.println("" + userdata.getId());
		List<PSFFollowupNotificationModel> folowupData = psfRepo.getFollowUpNotification(userdata.getId());
		return ok(toJson(folowupData));

	}

	private void inside_Finally_comment(PreparedStatement cs, ResultSet rs, Connection connection) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
		}
		;
		try {
			if (cs != null) {
				cs.close();
			}
		} catch (Exception e) {
		}
		;
		try {
			if (connection != null) {
				if (!connection.isClosed()) {
					connection.close();
				}
			}
		} catch (Exception e) {
		}
		;
	}

}
