/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

/**
 *
 * @author W-885
 */
import static play.libs.Json.toJson;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;
import play.db.DB;
import javax.sql.DataSource;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import controllers.webmodels.AssignedInteractionDataOnTabLoad;
import controllers.webmodels.CallInteractionDataOnTabLoad;
import play.data.Form;
import java.text.DateFormat;
import org.supercsv.cellprocessor.joda.ParseLocalDate;
import controllers.webmodels.CallLogsOnLoadTabData;
import controllers.webmodels.CampaignAjaxLoad;
import controllers.webmodels.CustomerDataOnTabLoad;
import controllers.webmodels.SRDispositionDataOnTabLoad;
import controllers.webmodels.ServiceDataOnTabLoad;
import controllers.webmodels.ServicebookedDataOnTabLoad;
import controllers.webmodels.VehicleDataOnTabLoad;
import controllers.webmodels.WorkShopSummaryDataOnTabLoad;
import controllers.webmodels.WyzUserDataOnTabLoad;
import controllers.webmodels.ComplaintInformation;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import static play.libs.Json.toJson;
import play.mvc.Http;
import play.mvc.Result;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;
import static play.mvc.Http.Response;
import javax.inject.Inject;
import javax.inject.Singleton;

import models.CallInteraction;
import models.Customer;
import models.Insurance;
import models.SRDisposition;
import models.Vehicle;
import models.ServiceBooked;
import models.UploadFileFields;
import models.AssignedInteraction;
import models.Complaint;
import models.ServiceAdvisor;
import play.Logger.ALogger;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.java.Secure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.ScheduledCallRepository;
import repositories.WyzUserRepository;
import repositories.SMSTriggerRespository;
import repositories.CampaignRepository;

import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;
import org.supercsv.io.*;
import java.text.*;
import models.Address;
import models.Campaign;
import models.CustomerJson;
import models.Driver;
import models.Email;
import models.JobCardDetailsUpload;
import models.ListUploadMasterAlias;
import models.ListingForm;
import models.Location;

import models.Service;
import models.UploadMasterFormat;
import models.MessageTemplete;
import models.PickupDrop;
import models.WyzUser;
import models.NewCustomer;
import models.Phone;
import models.TaggingUsers;
import models.UnAvailability;
import models.Workshop;
import models.WorkshopSummary;
import models.workshopBill;
import models.UploadData;
import static play.libs.Json.toJson;
import play.mvc.BodyParser;
import static play.mvc.Controller.response;
import static play.mvc.Results.ok;
import views.html.assignedInteractionPageForUpload;
import views.html.searchCustomerCRE;
import views.html.addCustomer;
import views.html.assignCallBasedOnSelectedRange;
import views.html.assignedInteractionTable;
import views.html.complaints;
import views.html.assignComplaints;
import views.html.roasterTableByCREMan;
import views.html.changeAssignmentByCREManager;
import views.html.dataUploadFormatForm;
import views.html.uploadMasterFilesForm;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;
import views.html.complaintResolution;
import views.html.rosterOfUnAvailibilityOfUserSelectedRange;
import views.html.rosterOfUnAvailibilityByUserSelectedRange;
import views.html.addCampaign;

@Controller
public class CampaignController extends play.mvc.Controller {
	
	ALogger logger = play.Logger.of("application");

	private final CallInfoRepository repo;
	private final CallInteractionsRepository call_int_repo;
	private final WyzUserRepository wyzRepo;
	private final SMSTriggerRespository sms_repo;
	private final CampaignRepository campaign_repo;
	
	private PlaySessionStore playSessionStore;

	private org.pac4j.core.config.Config config;

	@Inject
	public CampaignController(CallInfoRepository repository, org.pac4j.core.config.Config config,
			CallInteractionsRepository interRepo, WyzUserRepository wyzRepository, SMSTriggerRespository smsRepository,
			CampaignRepository campaignRepo,PlaySessionStore plstore) {
		repo = repository;
		call_int_repo = interRepo;
		wyzRepo = wyzRepository;
		sms_repo = smsRepository;
		campaign_repo = campaignRepo;
		config = (org.pac4j.core.config.Config) config;
		this.playSessionStore = plstore;

	}
	
	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}
	
	@Secure(clients = "FormClient")
	public Result addCampaign() {
		List<Location> locationList = call_int_repo.getLocationList();
		return ok(addCampaign.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(), locationList,"Campaign"));
	}
	
	@Secure(clients = "FormClient")
	public Result addCampaignInsurance() {
		List<Location> locationList = call_int_repo.getLocationList();
		return ok(addCampaign.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(), locationList,"Insurance"));
	}
	
	@Secure(clients = "FormClient")
	public Result postCampaign() {		
		Form<Campaign> campaign_data = Form.form(Campaign.class).bindFromRequest();
		Campaign Campaign = campaign_data.get();
		Campaign.setAssignedInteractions(null);
		String campaigtype = Campaign.getCampaignType();

		Form<Location> loc_upload_data = Form.form(Location.class).bindFromRequest();
		Location uploadData_loc = loc_upload_data.get();

		String locationdata = uploadData_loc.getName();

		Form<Workshop> loc_upload_work = Form.form(Workshop.class).bindFromRequest();
		Workshop uploadData_work = loc_upload_work.get();
		Long workid = uploadData_work.getId();
		// Long workshopId = uploadData_work.getId();

		campaign_repo.addNewCampaign(Campaign, getUserLoginName(), locationdata, uploadData_work, workid);
		
		if(campaigtype.equals("Insurance")){
		
			return redirect(controllers.routes.CampaignController.addCampaignInsurance());
		}else{
			
			return redirect(controllers.routes.CampaignController.addCampaign());
		}

		// return ok(toJson(complaint));
	}
	
	@Secure(clients = "FormClient")
	public Result getCampaignNamesByUpload(String uploadType){
		
		List<Campaign> campList=campaign_repo.getCampaignListUploadType(uploadType);
		
		List<CampaignAjaxLoad> campAjaxList=new ArrayList<CampaignAjaxLoad>();
		
		if(campList.size()!=0){
			
			for(Campaign sa:campList){
				CampaignAjaxLoad camp=new CampaignAjaxLoad();
				camp.setCampId(sa.getId());
				camp.setCampName(sa.getCampaignName());
				campAjaxList.add(camp);
				
			}
			
		}
		
		return ok(toJson(campAjaxList));
	}

}
