package controllers;

import static play.libs.Json.toJson;
import static play.mvc.Results.ok;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import controllers.webmodels.ServiceAdvisorIdName;
import play.Logger.ALogger;
import play.mvc.Result;
import repositories.AutoSelectionSARepository;
import utils.WyzCRMCache;

@Controller
public class AutoSelectionSAController extends play.mvc.Controller{

	ALogger logger = play.Logger.of("application");
	private final AutoSelectionSARepository repo;
	private PlaySessionStore playSessionStore;

	
	private final WyzCRMCache saCache;
	
	

	@Inject
	public AutoSelectionSAController(AutoSelectionSARepository repository,WyzCRMCache wyzsaCache,PlaySessionStore playses) {
		repo = repository;
		saCache=wyzsaCache;
		playSessionStore = playses;
	}

	@Secure(clients = "FormClient")
	public Result ajaxAutoSASelection(Long workshop_id, String date_value) throws java.lang.Exception {
		List<ServiceAdvisorIdName> sa = saCache.getServiceAdvisorRecomendation(stringToDate(date_value), workshop_id,getUserLogindealerCode());
		return ok(toJson(sa));
	}

	@Secure(clients = "FormClient")
	public Result ajaxAutoSASelectionList(String preSaDetails) throws java.lang.Exception {

		logger.info("preSaDetails sa automatic selection : " + preSaDetails);
		if (!preSaDetails.equals("NA")) {
			ServiceAdvisorIdName preSA = new ServiceAdvisorIdName();
			preSA = getSaObjectFromString(preSaDetails);
			saCache.getServiceAdvisorList(preSA.getDate(), preSA.getWorkshopId(), preSA,getUserLogindealerCode());
		}
		return ok();
	}

	@Secure(clients = "FormClient")
	public Result ajaxupdateSaChange(Long workshop_id, String date_value, String preSaDetails, String newSaDetails)
			throws java.lang.Exception {
		if (preSaDetails.equals("NA") && newSaDetails.equals("NA")) {
			saCache.updateServiceAdvisorChange(stringToDate(date_value), workshop_id, null, null,getUserLogindealerCode());
		} else if (preSaDetails.equals("NA") && !newSaDetails.equals("NA")) {
			saCache.updateServiceAdvisorChange(stringToDate(date_value), workshop_id, null,
					getSaObjectFromString(newSaDetails),getUserLogindealerCode());
		} else if (!preSaDetails.equals("NA") && newSaDetails.equals("NA")) {
			saCache.updateServiceAdvisorChange(stringToDate(date_value), workshop_id,
					getSaObjectFromString(preSaDetails), null,getUserLogindealerCode());
		} else {
			saCache.updateServiceAdvisorChange(stringToDate(date_value), workshop_id,
					getSaObjectFromString(preSaDetails), getSaObjectFromString(newSaDetails),getUserLogindealerCode());
		}

		return ok();
	}

	private ServiceAdvisorIdName getSaObjectFromString(String saDetails) {
		ServiceAdvisorIdName sa = new ServiceAdvisorIdName();
		List<String> items = Arrays.asList(saDetails.split("\\s*,\\s*"));
		sa.setAdvisorId(Long.parseLong(items.get(0)));
		sa.setAdvisorName(items.get(1));
		sa.setPriority(Long.parseLong(items.get(2)));
		sa.setDate(stringToDate(items.get(3)));
		sa.setWorkshopId(Long.parseLong(items.get(4)));
		return sa;
	}

	private Date stringToDate(String date_value) {
		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		try {
			date = dmyFormat.parse(date_value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}
	
	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	
	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}
}
