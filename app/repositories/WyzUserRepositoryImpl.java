package repositories;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import models.WyzUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import play.Logger.ALogger;
import play.data.Form;
import configs.JinqSource;
import java.util.ArrayList;
import java.util.HashMap;

import models.CallDispositionData;
import models.Campaign;
import models.Dealer;
import models.Location;
import models.Role;
import models.Tenant;
import models.UnAvailability;
import models.UserSession;
import models.SMSTemplate;
import org.hibernate.Hibernate;
import org.hibernate.Session;

@Repository("wyzUserRepository")
@Transactional
public class WyzUserRepositoryImpl implements WyzUserRepository {

	ALogger logger = play.Logger.of("application");

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Override
	public WyzUser getUserByPhoneNumber(String phoneNumber) {

		long count = source.wyzUsers(em).filter(u -> u.getPhoneNumber().toLowerCase().equals(phoneNumber.toLowerCase()))
				.count();

		if (count == 0)
			return null;
		return source.wyzUsers(em).where(u -> u.getPhoneNumber().toLowerCase().equals(phoneNumber.toLowerCase()))
				.getOnlyValue();
	}

	@Override
	public List<WyzUser> getAllUsers(String dealerDB) {

		return source.wyzUsers(em).toList();

	}

	@Override
	public WyzUser addWyzUser(WyzUser user, String dealerDB, String selectedDealerName, String selectedRole,
			String selectedManager) {

		if ((selectedRole.equals("CREManager")) || (selectedRole.equals("SalesManager"))) {

			user.setCreManager("");
			user.setDealerName(selectedDealerName);
			user.setDealerCode(dealerDB);
			user.setDealerId(dealerDB);
			em.persist(user);

			return user;
		} else {
			user.setCreManager(selectedManager);
			user.setDealerCode(dealerDB);
			user.setDealerId(dealerDB);
			user.setDealerName(selectedDealerName);
			em.persist(user);

			return user;

		}
	}

	@Override
	public WyzUser getDealerNameByManager(String selectedManager) {

		WyzUser userData = source.wyzUsers(em).where(u -> u.getUserName().equals(selectedManager)).getOnlyValue();

		return userData;

	}

	@Override
	public WyzUser deletUser(Long id, String dealerDB) {

		WyzUser del = em.find(WyzUser.class, id);
		em.remove(del);
		return del;
	}

	@Override
	public WyzUser editUser(Long id, String dealerDB) {

		WyzUser edi = em.find(WyzUser.class, id);

		return edi;

	}

	@Override
	public WyzUser editedData(Long id, WyzUser user, String dealerDB) {

		WyzUser edit = em.find(WyzUser.class, id);

		Form<WyzUser> form = Form.form(WyzUser.class).bindFromRequest();
		WyzUser userdata = form.get();

		edit.setFirstName(userdata.getFirstName());
		edit.setLastName(userdata.getLastName());
		edit.setUserName(userdata.getUserName());
		edit.setPhoneNumber(userdata.getPhoneNumber());
		edit.setPhoneIMEINo(userdata.getPhoneIMEINo());
		edit.setEmailId(userdata.getEmailId());
		edit.setPassword(userdata.getPassword());

		return user;

	}

	@Override
	public WyzUser getUserById(Long id, String dealerDB) {

		WyzUser del = em.find(WyzUser.class, id);

		return del;
	}

	@Override
	public WyzUser getUserByDealerId(String dealerId) {

		long count = source.wyzUsers(em).where(u -> u.dealerId.toLowerCase().equals(dealerId.toLowerCase())).count();

		if (count == 0)
			return null;
		return source.wyzUsers(em).where(u -> u.dealerId.toLowerCase().equals(dealerId.toLowerCase())).getOnlyValue();
	}

	@Override
	public boolean authenticateUser(String suserName, String password) {

		long count = source.wyzUsers(em).where(u -> u.getUserName().equals(suserName)).count();

		if (count == 0) {
			return false;
		} else {

			WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(suserName)).getOnlyValue();

			if (user.getPassword().equals(password))
				return true;
			else
				return false;
		}
	}

	@Override
	public String getUserRole(Long id, String dealerDB) {

		WyzUser user = getUserById(id, dealerDB);
		if (user == null)
			return null;

		return user.getRole();
	}

	@Override
	public WyzUser getUserbyUserName(String suserName) {

		long count = source.wyzUsers(em).where(u -> u.getUserName().equals(suserName)).count();

		if (count == 0)
			return null;

		else {
			WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(suserName)).getOnlyValue();
			Hibernate.initialize(user.getLocation());
			Hibernate.initialize(user.getRoles());

			return user;
		}

	}

	public List<String> getAllCreManagers(String dealerDB) {

		Query query = em.createQuery("SELECT a.userName FROM WyzUser a where role='" + "CREManager" + "'");
		List<String> data = query.getResultList();
		return data;
	}

	@Override
	public List<WyzUser> getAllCRE(String dealerDB) {

		return source.wyzUsers(em).where(u -> u.getRole().equals("CRE")).toList();
	}

	@Override
	public List<WyzUser> getCREofCREManager(String user, String dealerDB) {

		return source.wyzUsers(em).where(u -> u.getCreManager().equals(user) && u.getRole().equals("CRE")).toList();

	}

	@Override
	public List<WyzUser> getCREManagerbyDealerCode(String dealercode) {

		logger.info("listing the CRE managers of dealercode from selected database : " + dealercode);
		// TODO Auto-generated method stub
		return source.wyzUsers(em).where(u -> u.getDealerCode().equals(dealercode) && u.getRole().equals("CREManager"))
				.toList();

	}

	@Override
	public long getCREid(String userCRE, String dealerDB) {

		WyzUser userdata = getUserbyUserName(userCRE);
		return userdata.getId();
		/*
		 * Query query = em.createQuery(
		 * "SELECT a.id FROM WyzUser a where userName='"+userCRE+"'"); long
		 * data=(long)query.getSingleResult(); return data;
		 */
	}

	@Override
	public WyzUser getUserByPhoneNumberandIMEIno(String phoneNumber, String phoneIMEINo) {
		/*
		 * String query="USE wyzconnectadccweb;"; Session hibernateSession =
		 * em.unwrap(Session.class); hibernateSession.doWork(new
		 * org.hibernate.jdbc.Work() {
		 * 
		 * @Override public void execute(Connection connection) throws
		 * SQLException { Statement smt=connection.createStatement();
		 * smt.execute(query); }
		 * 
		 * });
		 */
		logger.info("inside repository phonenumber : " + phoneNumber);
		logger.info("inside repository phone IMEI no : " + phoneIMEINo);

		long count = source.wyzUsers(em).where(u -> u.getPhoneNumber().toLowerCase().equals(phoneNumber.toLowerCase()))
				.count();
		logger.info("inside repository countI no : " + count);

		if (count == 0)
			return null;

		else {
			WyzUser user = source.wyzUsers(em)
					.where(u -> u.getPhoneNumber().toLowerCase().equals(phoneNumber.toLowerCase())
							&& u.getPhoneIMEINo().equals(phoneIMEINo))
					.getOnlyValue();
			return user;
		}
	}

	@Override
	public Boolean changepasswordFunction(String userLogindealerCode, String userloginname, String newPswd, String currentPswd) {

		WyzUser idChange = source.wyzUsers(em)
				.where(u -> u.getUserName().toLowerCase().equals(userloginname.toLowerCase())).getOnlyValue();
		long idreq = idChange.getId();
		logger.info("id of username who password to be changed : " + idreq);
		
		if ((idChange.getPassword()).equals(currentPswd)) {
			idChange.setPassword(newPswd);
			em.merge(idChange);
			em.flush();
			logger.info("Current password is same as database password");
			
		Config configuration = ConfigFactory.load();
		String defaultDataBaseName = configuration.getString("app.defaultdatabase");
		logger.info("defaultDataBaseName : "+defaultDataBaseName);
		
		String query="USE "+defaultDataBaseName+";";
		Session hibernateSession = em.unwrap(Session.class);
		
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {
			
			 @Override
			 public void execute(Connection connection) throws SQLException {
			 Statement smt=connection.createStatement();
			 Boolean resultOfExecution=smt.execute(query);
			 Tenant tenant=source.tenant(em).where(u ->u.getTenantCode().equals(userLogindealerCode)).getOnlyValue();
			 WyzUser idChange = source.wyzUsers(em)
						.where(u -> u.getUserName().toLowerCase().equals(userloginname.toLowerCase())).getOnlyValue();
				long idreq = idChange.getId();
				idChange.setPassword(newPswd);
				em.flush();				
				em.merge(idChange);		 
			 }
			 });
		return true;
		}else{
			
			return false;
		}

	
		
	}


	@Override
	public List<WyzUser> getSalesManagerbyDealerCode(String dealercode) {

		logger.info("listing the Sales managers of dealercode from selected database : " + dealercode);
		// TODO Auto-generated method stub
		return source.wyzUsers(em)
				.where(u -> u.getDealerCode().equals(dealercode) && u.getRole().equals("SalesManager")).toList();
	}

	@Override
	public List<WyzUser> getSalesExecutiveofSalesManager(String user, String dealercode) {
		// TODO Auto-generated method stub
		return source.wyzUsers(em).where(u -> u.getCreManager().equals(user)).toList();
	}

	@Override
	public List<WyzUser> getSalesManagerbyDealerCodeBysuperAdmin() {
		// TODO Auto-generated method stub
		// String query="USE wyzconnectadccweb;";
		// Session hibernateSession = em.unwrap(Session.class);
		// hibernateSession.doWork(new org.hibernate.jdbc.Work() {
		//
		// @Override
		// public void execute(Connection connection) throws SQLException {
		// Statement smt=connection.createStatement();
		// Boolean resultOfExecution=smt.execute(query);
		//
		// }
		//
		// });

		return source.wyzUsers(em).where(u -> u.getRole().equals("SalesManager")).toList();
	}

	public List<WyzUser> getAllCreManagersBySuperAdmin() {

		return source.wyzUsers(em).where(u -> u.getRole().equals("CREManager")).toList();

	}

	@Override
	public Boolean checkTheUserExistingOrNot(String newUserName, String dealerCodeOfNewUser, WyzUser listData) {

		// String query="USE "+dealerCodeOfNewUser+";";
		// Session hibernateSession = em.unwrap(Session.class);
		// hibernateSession.doWork(new org.hibernate.jdbc.Work() {
		//
		// @Override
		// public void execute(Connection connection) throws SQLException {
		// Statement smt=connection.createStatement();
		// smt.execute(query);
		//
		//
		// }
		//
		// });
		logger.info("Entered in to the checking the user");
		long count = source.wyzUsers(em)
				.where(k -> k.getUserName().equals(newUserName) && k.getDealerCode().equals(dealerCodeOfNewUser))
				.count();

		if (count == 0) {

			Query query1 = em.createQuery("SELECT max(id) FROM WyzUser");
			long data = (long) query1.getSingleResult();
			logger.info("the max Id of WyzUser is : " + data);
			listData.setId(data + 1);
			em.merge(listData);
			em.flush();
			return true;
		}
		return false;
	}

	@Override
	public List<WyzUser> getAllUsersForSync() {

		// String query="USE wyzconnectadccweb;";
		// Session hibernateSession = em.unwrap(Session.class);
		// hibernateSession.doWork(new org.hibernate.jdbc.Work() {
		//
		// @Override
		// public void execute(Connection connection) throws SQLException {
		// Statement smt=connection.createStatement();
		// smt.execute(query);
		//
		//
		// }
		//
		// });
		return source.wyzUsers(em).toList();

	}

	@Override
	public List<WyzUser> getAllCREOFDealer(String dealer) {
		// TODO Auto-generated method stub
		String query = "USE " + dealer + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) throws SQLException {
				Statement smt = connection.createStatement();
				Boolean resultOfExecution = smt.execute(query);
			}
		});

		return source.wyzUsers(em).where(u -> u.getRole().equals("CRE") && u.getDealerCode().equals(dealer)).toList();
	}

	@Override
	public List<WyzUser> getAllSalesExecutivesForSyncReports(String dealer) {

		// String query="USE "+dealer+";";
		// Session hibernateSession = em.unwrap(Session.class);
		// hibernateSession.doWork(new org.hibernate.jdbc.Work() {
		//
		// @Override
		// public void execute(Connection connection) throws SQLException {
		// Statement smt=connection.createStatement();
		// Boolean resultOfExecution=smt.execute(query);
		// }});
		return source.wyzUsers(em).where(u -> u.getRole().equals("SalesExecutive") && u.getDealerCode().equals(dealer))
				.toList();
	}

	@Override
	public WyzUser toaddTheRegistrationIdTOUser(String phoneNumber, String phoneIMEINo, String registrationIdNo) {
		WyzUser user = source.wyzUsers(em).where(u -> u.getPhoneNumber().toLowerCase().equals(phoneNumber.toLowerCase())
				&& u.getPhoneIMEINo().equals(phoneIMEINo)).getOnlyValue();
		user.setRegistrationId(registrationIdNo);
		return user;

		// TODO Auto-generated method stub

	}

	@Override
	public void toaddTheRegistrationIdTOUserForTheParticulardatabase(String phoneNumber, String phoneIMEINo,
			String registrationIdNo, String dealercode) {
		String query = "USE " + dealercode + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) {
				Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution = smt.execute(query);
					WyzUser user = source.wyzUsers(em)
							.where(u -> u.getPhoneNumber().toLowerCase().equals(phoneNumber.toLowerCase())
									&& u.getPhoneIMEINo().equals(phoneIMEINo))
							.getOnlyValue();
					user.setRegistrationId(registrationIdNo);
				} catch (SQLException e) {

					logger.info("SQL Exception");
				}

			}
		});

	}

	@Override
	public List<WyzUser> getAllPSFroleCRES(String userloginname, String userLogindealerCode) {
		// TODO Auto-generated method stub
		return source.wyzUsers(em).where(u -> u.getCreManager().equals(userloginname) && u.getRole1().equals("PSF"))
				.toList();
	}

	private Date currentDate() {

		return new Date(System.currentTimeMillis());
	}

	@Override
	public List<WyzUser> getAllUsersAvailableToday(String userLogindealerCode, String user) {

		List<WyzUser> listAllUser = getAllUsers(userLogindealerCode);
		for (WyzUser userData : listAllUser) {

			Hibernate.initialize(userData.getUnAvailabilities());

		}

		List<WyzUser> availableUsers = new ArrayList<WyzUser>();

		java.util.Date todayData = new java.sql.Date(System.currentTimeMillis());

		for (WyzUser usersData : listAllUser) {

			List<UnAvailability> unaviabiltylist = usersData.getUnAvailabilities();

			if (unaviabiltylist.size() != 0) {

				int i = 0;
				for (UnAvailability unavailable : unaviabiltylist) {

					if (unavailable.getFromDate().before(todayData) && unavailable.getToDate().after(todayData)) {

						i = i + 1;

					} else if (unavailable.getFromDate().toString().equals(todayData.toString())) {
						i = i + 1;

					} else if (unavailable.getToDate().toString().equals(todayData.toString())) {
						i = i + 1;

					}
				}
				if (i == 0) {
					availableUsers.add(usersData);

				}

			} else {

				availableUsers.add(usersData);

			}

		}

		return availableUsers;
	}

	@Override
	public List<WyzUser> getAllUsersNotAvailableToday(String userLogindealerCode, String userLoginName) {

		List<WyzUser> listAllUser = getAllUsers(userLogindealerCode);
		for (WyzUser userData : listAllUser) {

			Hibernate.initialize(userData.getUnAvailabilities());

		}

		List<WyzUser> notAvailableUsers = new ArrayList<WyzUser>();

		java.util.Date todayData = new java.sql.Date(System.currentTimeMillis());
		// logger.info("todayData is : "+todayData);
		for (WyzUser usersData : listAllUser) {

			List<UnAvailability> unaviabiltylist = usersData.getUnAvailabilities();
			// logger.info("unaviabiltylist is : "+unaviabiltylist.size());
			if (unaviabiltylist.size() != 0) {

				int i = 0;
				for (UnAvailability unavailable : unaviabiltylist) {
					// logger.info("unaviabiltylist is :
					// "+unaviabiltylist.size()+" for user
					// "+usersData.getUserName());
					if (unavailable.getFromDate().before(todayData) && unavailable.getToDate().after(todayData)) {
						// logger.info("already not available today user is :
						// "+usersData.getUserName()+"unavailable.getToDate()"+unavailable.getToDate()+"unavailable.getFromDate()"+unavailable.getFromDate());
						i = i + 1;

					} else if (unavailable.getFromDate().toString().equals(todayData.toString())) {
						i = i + 1;
						// logger.info("from date equal");

					} else if (unavailable.getToDate().toString().equals(todayData.toString())) {
						i = i + 1;
						// logger.info("to date equal");

					}
				}
				if (i != 0) {
					notAvailableUsers.add(usersData);
					// logger.info("checked with all availabilty roaster
					// "+usersData.getUserName());

				}
			} else {

				// logger.info("avialbel users are ans not roster also:
				// "+usersData.getUserName());
				// notAvailableUsers.add(usersData);

			}

		}

		return notAvailableUsers;

	}

	@Override
	public List<WyzUser> getUsersofCREManager(String userLoginName, String userLogindealerCode) {

		return source.wyzUsers(em).where(u -> u.getCreManager().equals(userLoginName)).toList();
	}

	@Override
	public Map<String, String> updateRegistrationIdOfIMEINo(String phoneIMEINo, String registrationId) {

		Map<String, String> status = new HashMap<String, String>();
		long count = source.wyzUsers(em).where(u -> u.getPhoneIMEINo() != null)
				.where(u -> u.getPhoneIMEINo().toLowerCase().equals(phoneIMEINo.toLowerCase())).count();

		if (count == 0) {
			status.put("success", "0");
			status.put("message", "IMEI No Doesnt Exist");

			return status;

		} else {
			WyzUser userUpdate = source.wyzUsers(em).where(u -> u.getPhoneIMEINo() != null)
					.where(u -> u.getPhoneIMEINo().toLowerCase().equals(phoneIMEINo.toLowerCase())).getOnlyValue();
			userUpdate.setRegistrationId(registrationId);
			em.merge(userUpdate);

			status.put("success", "1");
			status.put("message", "Registration Id Updated");

			return status;
		}
	}

	@Override
	public List<WyzUser> getCREUserNameByWorkshopId(long workshopId) {

		return source.wyzUsers(em).where(u -> u.getWorkshop() != null).where(u -> u.getWorkshop().getId() == workshopId)
				.where(u -> u.getRole() != null).where(u -> u.getRole().equals("CRE")).toList();

	}

	@Override
	public void addSessionData(UserSession addnewSession) {

		em.persist(addnewSession);

	}

	@Override
	public void removeUserFromSession(String userLoginName) {

		UserSession userdata = source.userSessions(em).where(u -> u.getUserLogined().equals(userLoginName))
				.getOnlyValue();
		em.remove(userdata);

	}

	@Override
	public long existingCount(String referereAdress, String useris) {
		// TODO Auto-generated method stub
		return source.userSessions(em).where(u -> u.getIpAddress() != null && u.getIpAddress().equals(referereAdress))
				.where(u -> u.getUserLogined() != null && u.getUserLogined().equals(useris)).count();
	}

	@Override
	public WyzUser getUser(String username) {

		WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(username)).getOnlyValue();
		logger.debug("Obtained user object obtained having user name: " + username);

		return user;
	}

	@Override
	public Tenant getUserTenant(String username) {

		logger.info("get Tenents ");
		WyzUser user = getUser(username);

		Hibernate.initialize(user.getTenant());
		Tenant tenant = user.getTenant();

		logger.debug("Obtained Tenant: " + tenant.getTenantCode() + "for user:" + username);

		return tenant;
	}

	@Override
	public Tenant getUserTenant(Long userId) {

		WyzUser user = em.find(WyzUser.class, userId);

		logger.debug("Obtained User for userid: " + userId);

		Hibernate.initialize(user.getTenant());

		Tenant tenant = user.getTenant();
		// logger.debug("Obtained tenant for userid:" + userId);

		return tenant;
	}

	@Override
	public WyzUser getUserDetails(String username, String password) {

		Config configuration = ConfigFactory.load();
		String defaultDataBaseName = configuration.getString("app.defaultdatabase");

		logger.info("defaultDataBaseName  : " + defaultDataBaseName);

		logger.info("inside repository username : " + username);
		logger.info("inside repository password : " + password);

		String query = "USE " + defaultDataBaseName + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) throws SQLException {
				Statement smt = connection.createStatement();
				Boolean resultOfExecution = smt.execute(query);
			}
		});

		long count = source.wyzUsers(em).where(
				u -> u.getUserName().toLowerCase().equals(username.toLowerCase()) && u.getPassword().equals(password))
				.count();
		logger.info("inside repository countI no : " + count);

		if (count == 0)
			return null;

		else {
			WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().toLowerCase().equals(username.toLowerCase())
					&& u.getPassword().equals(password)).getOnlyValue();

			return user;

		}
	}

	@Override
	public String getOEMBYDealerCode(String dealerCode) {
		// TODO Auto-generated method stub
		return source.dealers(em).where(u -> u.getDealerCode().equals(dealerCode)).select(u -> u.getOEM())
				.getOnlyValue();
	}

	@Override
	public List<Role> getUserRoles(String username) {

		WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(username)).getOnlyValue();

		Hibernate.initialize(user.getRoles());

		List<Role> roles = user.getRoles();

		return roles;
	}

	@Override
	public List<CallDispositionData> getDispositionList() {
		// TODO Auto-generated method stub
		return source.callDispositionDatas(em).toList();
	}

	@Override
	public List<Campaign> getCampaignList() {
		// TODO Auto-generated method stub
		return source.campaigns(em).toList();
	}

	@Override
	public List<WyzUser> getUsersByRole(String roleIs) {
		// TODO Auto-generated method stub
		logger.info(" roleIs :" + roleIs);

		Role role = source.roles(em).where(u -> u.getRole().equals(roleIs)).getOnlyValue();
		Hibernate.initialize(role.getUsers());

		List<WyzUser> user = role.getUsers();
		for (WyzUser u : user) {

			logger.info("user id: " + u.getId() + " users are :" + u.getUserName());
		}
		return user;
	}

	@Override
	public List<Location> getUserLocationById(long userId) {
		// TODO Auto-generated method stub

		WyzUser user = em.find(WyzUser.class, userId);
		Hibernate.initialize(user.getLocationList());

		return user.getLocationList();
	}

	@Override
	public List<CallDispositionData> getDispositionListOfNoncontacts() {
		// TODO Auto-generated method stub

		return source
				.callDispositionDatas(em).where(u -> u.getDispositionId() == 6 || u.getDispositionId() == 7
						|| u.getDispositionId() == 8 || u.getDispositionId() == 9 || u.getDispositionId() == 10)
				.toList();
	}

	@Override
	public List<CallDispositionData> getDispositionListOfService() {
		// TODO Auto-generated method stub
		return source.callDispositionDatas(em)
				.where(u -> u.getDispositionId() == 3 || u.getDispositionId() == 4 || u.getDispositionId() == 5
						|| u.getDispositionId() == 6 || u.getDispositionId() == 7 || u.getDispositionId() == 8
						|| u.getDispositionId() == 9 || u.getDispositionId() == 10)
				.toList();
	}

	@Override
	public List<CallDispositionData> getDispositionListOfInsurance() {
		// TODO Auto-generated method stub
		return source.callDispositionDatas(em)
				.where(u -> u.getDispositionId() == 25 || u.getDispositionId() == 4 || u.getDispositionId() == 5
						|| u.getDispositionId() == 6 || u.getDispositionId() == 7 || u.getDispositionId() == 8
						|| u.getDispositionId() == 9 || u.getDispositionId() == 10)
				.toList();
	}

	@Override
	public List<CallDispositionData> getDispositionListOfPSF() {
		// TODO Auto-generated method stub
		return source.callDispositionDatas(em)
				.where(u -> u.getDispositionId() == 25 || u.getDispositionId() == 22 || u.getDispositionId() == 36
						|| u.getDispositionId() == 4 || u.getDispositionId() == 6 || u.getDispositionId() == 7
						|| u.getDispositionId() == 8 || u.getDispositionId() == 9 || u.getDispositionId() == 10)
				.toList();
	}

	@Override
		public List<WyzUser> getUserNameByWorkshopId(long workshopId) {
			return source.wyzUsers(em).where(u -> u.getWorkshop() != null).where(u -> u.getWorkshop().getId() == workshopId)
					.where(u -> u.getRole() != null).toList();
		}
	
	@Override
	public SMSTemplate getSMSbyLocAndSMSType(String locId, String smstypeid) {
			SMSTemplate smsnew = new SMSTemplate();
			long countofsms = source.smsTemplates(em).where(u -> u.getLocationId().equals(locId) && u.getSmsType().equals(smstypeid)).count();
			if(countofsms > 0){
			    SMSTemplate smstemplate = source.smsTemplates(em).where(u -> u.getLocationId().equals(locId) && u.getSmsType().equals(smstypeid)).getOnlyValue();
				return smstemplate;
			}else{
				return smsnew;
				
			}
		}
	
	
	
}
