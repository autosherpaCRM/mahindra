package repositories;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import configs.JinqSource;
import configs.WyzAppConnectionProviderImpl;
import controllers.webmodels.CallInteractionHistory;
import controllers.webmodels.CallInteractionHistoryCubeReport;
import controllers.webmodels.CallLogAjaxLoad;
import controllers.webmodels.CallLogDispositionLoad;
import controllers.webmodels.CallLogAjaxLoadMR;
import controllers.webmodels.CallLogDispositionLoadMR;
import controllers.webmodels.DailyAssignmentStatusData;
import controllers.webmodels.DailyAvailabilityData;
import controllers.webmodels.InsuranceHistoryReport;
import controllers.webmodels.UploadFileHistory;
import models.CallInteraction;
import models.CustomerSearchResult;
import models.SavedSearchResult;
import play.Logger.ALogger;
import play.db.DB;

@Repository("searchRepository")
@Transactional
public class SearchRepositoryImpl implements SearchRepository {

	ALogger logger = play.Logger.of("application");
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerSearchResult> searchCustomers(String pattern, long start, long length, int orderColumnNum,
			int orderDirection) {

		// query = "CALL
		// search_customer(:pattern,:start_with,:length,:orderColumnNum,orderDirection)
		// javax.persistence.Query query = em.createNativeQuery("CALL
		// search_new(:pattern, :start_with, :length, :orderColumnNum,
		// :orderDirection)");

		javax.persistence.Query query = em.createNativeQuery(
				"CALL search_new(:pattern, :start_with, :length, :orderColumnNum, :orderDirection)",
				"customerSearchResult");

		query.setParameter("pattern", pattern);
		query.setParameter("start_with", start);
		query.setParameter("length", length);
		query.setParameter("orderColumnNum", orderColumnNum);
		query.setParameter("orderDirection", orderDirection);
		logger.info("pattern : " + pattern + "start : " + start + "length :" + length + " orderColumnNum : "
				+ orderColumnNum + " orderDirection :" + orderDirection);

		// logger.info("result set data is :
		// "+query.getResultList().get(1).toString());

		return query.getResultList();
	}

	@Override
	public long getAllCustomerCount() {
		return source.customers(em).count();
	}

	@Override
	public long getCustomerCountForPattern(String pattern) {

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("search_count");

		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.setParameter("pattern", pattern);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public long getAllAssignedInteraction(long id) {

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("assigned_intercation_count");

		sQuery.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
		sQuery.setParameter("wyzuserid", (int) id);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<CallLogAjaxLoad> assignedListOfUser(String fromDateNew, String toDateNew, String campaignName, String serviceTypeName ,String pattern, long id, long fromIndex, long toIndex) {
	javax.persistence.Query query = em.createNativeQuery(
	"CALL ServiceReminderwithSearch(:instartdate,:inenddate,:incampaignname,:serviceTypeName,:pattern,:wyzuserid, :start_with, :length)", "CallLogAjaxLoad");
	
	query.setParameter("instartdate", fromDateNew);
	query.setParameter("inenddate", toDateNew);
	query.setParameter("incampaignname", campaignName);
	query.setParameter("serviceTypeName",serviceTypeName);
	query.setParameter("pattern", pattern);
	query.setParameter("wyzuserid", id);
	query.setParameter("start_with", fromIndex);
	query.setParameter("length", toIndex);

	logger.info("pattern : " + pattern + " Id : " + id + " fromIndex :" + fromIndex + "toIndex :" + toIndex);

	return query.getResultList();
	}

	@Override
	public long getAllDispositionRequiredData(long id, int typeOfDispo) {

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("DispositionContactsCount");

		sQuery.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
		sQuery.setParameter("wyzuserid", (int) id);

		sQuery.registerStoredProcedureParameter("dispositiontype", Integer.class, ParameterMode.IN);
		sQuery.setParameter("dispositiontype", typeOfDispo);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();

	}

	@Override
	public List<CallLogDispositionLoad> getDispositionListOfUser(String fromDateNew, String toDateNew, String campaignName, String serviceTypeName,String pattern, 
			long id, long typeOfDispo,long fromIndex, long toIndex,int orderDirection) {

		javax.persistence.Query query = em.createNativeQuery(
				"CALL dispositionFilter(:instartdate,:inenddate,:incampaignname,:induetype,:pattern,:wyzuserid,:dispositiontype, :start_with, :length,:orderDirection)",
				"CallLogDispositionLoad");
		query.setParameter("instartdate", fromDateNew);
		query.setParameter("inenddate", toDateNew);
		query.setParameter("incampaignname", campaignName);
		query.setParameter("induetype",serviceTypeName);
		query.setParameter("pattern", pattern);
		query.setParameter("wyzuserid", id);
		query.setParameter("dispositiontype", typeOfDispo);
		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);
		query.setParameter("orderDirection", orderDirection);


		return query.getResultList();

	}

	@Override
	public long getNoncontactsDataCount(long id) {

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("NonContactsCount");

		sQuery.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
		sQuery.setParameter("wyzuserid", (int) id);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();

	}

	@Override
	public List<CallLogDispositionLoad> getnonContactsListOfUser(String fromDateNew, String toDateNew,
	String campaignName, String typeOfDispo , String droppedCount, String searchPattern, long id,
	long fromIndex, long toIndex) {

	javax.persistence.Query query = em.createNativeQuery(
	"CALL NonContactsSearch(:instartdate,:inenddate,:incampaignname,:typeOfDispo,:droppedCount,:pattern,:wyzuserid, :start_with, :length)", "CallLogDispositionLoad");

	query.setParameter("instartdate", fromDateNew);
	query.setParameter("inenddate", toDateNew);
	query.setParameter("incampaignname", campaignName);
	query.setParameter("typeOfDispo",typeOfDispo);
	query.setParameter("droppedCount",droppedCount);
	query.setParameter("pattern", searchPattern);
	query.setParameter("wyzuserid", id);
	query.setParameter("start_with", fromIndex);
	query.setParameter("length", toIndex);

	logger.info("id is noncontacts : " + id + "fromIndex : " + fromIndex + "toIndex :" + toIndex);

	return query.getResultList();

	}

	@Override
	public long getDroppedDataCount(long id) {

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("DroppedCount");

		sQuery.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
		sQuery.setParameter("wyzuserid", (int) id);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();

	}

	@Override
	public List<CallLogDispositionLoad> getDroppedDataList(String pattern, long id, long fromIndex, long toIndex) {

		javax.persistence.Query query = em.createNativeQuery(
				"CALL droppeddatawithsearch(:pattern, :wyzuserid, :start_with, :length)", "CallLogDispositionLoad");

		query.setParameter("pattern", pattern);
		query.setParameter("wyzuserid", id);
		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);

		logger.info("id is dropped : " + id + "fromIndex : " + fromIndex + "toIndex :" + toIndex);

		return query.getResultList();

	}

	@Override
	public long getAllVehicleCount() {
		// TODO Auto-generated method stub
		return source.vehicle(em).filter(u -> u.getCustomer()!=null).count();
	}

	// Manager Server side repo

	@Override
	public List<CallLogDispositionLoadMR> getDispositionListOfUserMR(String fromDateNew, String toDateNew , String campaignName, String serviceTypeName,String pattern, String managerName, String CREIds,
			long typeOfDispo,long fromIndex, long toIndex, int orderDirection) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL cremanagerdispositionfilter(:instartdate,:inenddate,:incampaignname,:induetype,:pattern,:managerName,:CREIds,:dispositiontype, :start_with, :length,:orderDirection)",
				"CallLogDispositionLoadMR");
		query.setParameter("instartdate",fromDateNew);
		query.setParameter("inenddate",toDateNew);
		query.setParameter("incampaignname",campaignName);
		query.setParameter("induetype",serviceTypeName);
		query.setParameter("pattern", pattern);
		query.setParameter("managerName", managerName);
		query.setParameter("CREIds", CREIds);
		query.setParameter("dispositiontype", typeOfDispo);
		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);
		query.setParameter("orderDirection", orderDirection);
		// logger.info("id is : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();

	}

	@Override
	public List<CallLogDispositionLoadMR> getDroppedDataListMR(String pattern, String managerName, String CREIds,
			long fromIndex, long toIndex) {

		javax.persistence.Query query = em.createNativeQuery(
				"CALL cremanagerdroppedSearch(:pattern,:managerName,:CREIds, :start_with, :length)",
				"CallLogDispositionLoadMR");

		query.setParameter("pattern", pattern);
		query.setParameter("managerName", managerName);
		query.setParameter("CREIds", CREIds);
		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);

		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();

	}

	@Override
	public List<CallLogDispositionLoadMR> getnonContactsListOfUserMR(String fromDateNew, String toDateNew, String campaignName ,String lastDispo, String droppedCount,String pattern, String managerName, String CREIds,
			long fromIndex, long toIndex) {

		javax.persistence.Query query = em.createNativeQuery(
				"CALL cremanagerNonContactsSearch(:instartdate,:inenddate,:incampaignname,:typeOfDispo,:droppedCount,:pattern,:managerName,:CREIds, :start_with, :length)",
				"CallLogDispositionLoadMR");
		query.setParameter("instartdate",fromDateNew);
		query.setParameter("inenddate",toDateNew);
		query.setParameter("incampaignname",campaignName);
		query.setParameter("typeOfDispo",lastDispo);
		query.setParameter("droppedCount",droppedCount);
		query.setParameter("pattern", pattern);
		query.setParameter("managerName", managerName);
		query.setParameter("CREIds", CREIds);
		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);

		// logger.info("id is noncontacts : "+id+"fromIndex :
		// "+fromIndex+"toIndex :"+toIndex);

		return query.getResultList();

	}

	@Override
	public List<CallLogAjaxLoadMR> assignedListOfUserMR(String fromDateNew,String toDateNew,String campaignName,String serviceTypeName,String pattern,String managerName,String CREIds, long fromIndex,long toIndex) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL cremanagerServiceReminderwithSearch(:instartdate,:inenddate,:incampaignname,:induetype,:pattern,:managerName,:CREIds, :start_with, :length)",
				"CallLogAjaxLoadMR");
		query.setParameter("instartdate",fromDateNew);
		query.setParameter("inenddate",toDateNew);
		query.setParameter("incampaignname",campaignName);
		query.setParameter("induetype",serviceTypeName);
		query.setParameter("pattern", pattern);		
		query.setParameter("managerName", managerName);
		query.setParameter("CREIds", CREIds);
		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);
		
		// logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);
		return query.getResultList();
	}

	@Override
	public long getCountOfUploadFileHistory(String uploadReportId, String typeIds, Date fromDateNew, Date toDateNew,
			long fromIndex, long toIndex) {

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("upload_data_list_count");

		sQuery.registerStoredProcedureParameter("upload_data_type", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("uploaddataid", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("str_date", Date.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("end_date", Date.class, ParameterMode.IN);

		sQuery.setParameter("upload_data_type", typeIds);
		sQuery.setParameter("uploaddataid", uploadReportId);
		sQuery.setParameter("str_date", fromDateNew);
		sQuery.setParameter("end_date", toDateNew);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<UploadFileHistory> getListOfUploadFileHistory(String uploadReportId, String typeIds, Date fromDateNew,
			Date toDateNew, long fromIndex, long toIndex) {

		javax.persistence.Query query = em.createNativeQuery(
				"CALL upload_data_list(:upload_data_type,:uploaddataid,:str_date,:end_date, :start_index, :length)",
				"UploadFileHistory");
		query.setParameter("upload_data_type", typeIds);
		query.setParameter("uploaddataid", uploadReportId);
		query.setParameter("str_date", fromDateNew);
		query.setParameter("end_date", toDateNew);
		query.setParameter("start_index", fromIndex);
		query.setParameter("length", toIndex);

		return query.getResultList();

	}

	// @Override
	// public double getConversionRate(long id, String storedProcedureFun){
	//
	// StoredProcedureQuery sQuery =
	// em.createStoredProcedureQuery("conversion_rate_Crewise");
	//
	// sQuery.registerStoredProcedureParameter("wyzuser", Integer.class,
	// ParameterMode.IN);
	// sQuery.setParameter("wyzuser", (int)id);
	//
	// Double countIs=(Double)sQuery.getSingleResult();
	//
	// return countIs;
	// }

	@Override
	public double getConversionRate(long id, String storedProcedureFun) {

		double totalSize = 0;
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery(storedProcedureFun);

		sQuery.registerStoredProcedureParameter("wyzuser", Integer.class, ParameterMode.IN);

		sQuery.setParameter("wyzuser", (int)id);

		BigDecimal bigCount = (BigDecimal) sQuery.getSingleResult();
		
		logger.info("bigCount "+bigCount);
		
		if(bigCount!=null){
			
			totalSize = bigCount.doubleValue();
		}
		
		
		
		return totalSize;
	}

	@Override
	public long getPendingandtotalcount(long id, String storedProcedureFun) {
		long totalSize = 0;
		
		
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery(storedProcedureFun);

		sQuery.registerStoredProcedureParameter("wyzuser", Integer.class, ParameterMode.IN);

		sQuery.setParameter("wyzuser", (int)id);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		totalSize = bigCount.longValue();

		return totalSize;

	}

	private void inside_Finally_comment(PreparedStatement cs, ResultSet rs, Connection connection) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
		}
		;
		try {
			if (cs != null) {
				cs.close();
			}
		} catch (Exception e) {
		}
		;
		try {
			if (connection != null) {
				if (!connection.isClosed()) {
					connection.close();
				}
			}
		} catch (Exception e) {
		}
		;
	}

	@Override
	public List<Date> getUploadIdMaxMinDate(String uploadId, String uploadType) {

		Date maxDateIs = source.uploadDatas(em).where(u -> u.getUpload_id() != null)
				.where(u -> u.getUpload_id().equals(uploadId)).where(u -> u.getDataType() != null)
				.where(u -> u.getDataType().equals(uploadType)).max(u -> u.getRecordedDate());

		Date minDateIs = source.uploadDatas(em).where(u -> u.getUpload_id() != null)
				.where(u -> u.getUpload_id().equals(uploadId)).where(u -> u.getDataType() != null)
				.where(u -> u.getDataType().equals(uploadType)).min(u -> u.getRecordedDate());

		logger.info("maxDateIs is : " + maxDateIs);
		logger.info("minDateIs is : " + minDateIs);

		List<Date> combinedDate = new ArrayList();
		combinedDate.add(minDateIs);
		combinedDate.add(maxDateIs);
		return combinedDate;
	}

	@Override
	public double getConversionRateMAN(String userLoginName, String storedProcedureFun) {
		double totalSize = 0;
		
		logger.info("userLoginName :"+userLoginName);
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery(storedProcedureFun);

		sQuery.registerStoredProcedureParameter("wyzuser", String.class, ParameterMode.IN);

		sQuery.setParameter("wyzuser", userLoginName);

		BigDecimal bigCount = (BigDecimal) sQuery.getSingleResult();
		
		logger.info("bigCount "+bigCount);
		
		if(bigCount!=null){
			
			totalSize = bigCount.doubleValue();
		}
		
		
		
		return totalSize;
	}

	@Override
	public long getPendingAndScheduledcountMan(String userLoginName, String storedProcedureFun) {
		long totalSize = 0;
		
		
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery(storedProcedureFun);

		sQuery.registerStoredProcedureParameter("wyzuser", String.class, ParameterMode.IN);

		sQuery.setParameter("wyzuser", userLoginName);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		totalSize = bigCount.longValue();

		return totalSize;
	}

	@Override
	public long methodToFindTotalSizeParam4(String userIds, long servicebookedcount, String storedProcedureFun, int dispoId,String user) {
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery(storedProcedureFun);		
		

		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("mgr_user_id", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("users", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("dispositiontype", Integer.class, ParameterMode.IN);

		sQuery.setParameter("pattern", "");
		sQuery.setParameter("mgr_user_id", user);
		sQuery.setParameter("users", userIds);
		sQuery.setParameter("dispositiontype", dispoId);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public long methodToFindTotalSizeParam3(String cREIds, long totalSize, String storedProcedureFun,String user) {
		// TODO Auto-generated method stub
		
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery(storedProcedureFun);
		
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("mgr_user_id", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("users", String.class, ParameterMode.IN);
		
		sQuery.setParameter("pattern", "");
		sQuery.setParameter("mgr_user_id", user);
		sQuery.setParameter("users", cREIds);
		
		
		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}
	
	@Override
	public boolean saveSearchName(String searchName,Map<String,String> parameters){
		String custName = parameters.get(SearchRepository.CUST_NAME);
		String vehNum = parameters.get(SearchRepository.VEH_REG_NO);
		String phNumber = parameters.get(SearchRepository.PHONE_NUM);
		String serDateFrom =  parameters.get(SearchRepository.SEARCH_DT_FROM);
		String serDateTo =  parameters.get(SearchRepository.SEARCH_DT_TO);
		String model = parameters.get(SearchRepository.MODEL);
		String variant = parameters.get(SearchRepository.VARIANT);
		String custCat = parameters.get(SearchRepository.CUST_CAT);
		String lastDisp = parameters.get(SearchRepository.LAST_DISP);
		String startstr = parameters.get(SearchRepository.START_INDX);
		String sizestr =  parameters.get(SearchRepository.SIZE);
		long start = Long.parseLong(startstr);
		long length = Long.parseLong(sizestr);
		
		logger.info("1:"+custName);
		logger.info("2:"+vehNum);

		logger.info("custN3ame:"+serDateFrom);
		logger.info("4:"+serDateTo);
		logger.info("custN5ame:"+model);
		logger.info("6:"+variant);
		logger.info("custN7ame:"+custCat);
		logger.info("8:"+lastDisp);
		logger.info("9:"+startstr);
		logger.info("10:"+sizestr);
		logger.info("11:"+start);
		logger.info("12:"+length);


		javax.persistence.Query query = em.createNativeQuery(
				"CALL customer_search_by_individual_params(:incustname, :invehregno, :inphnumber, :innxtserstrdate, :innxtserenddate, :inmodel, :invariant, :custcat,:searchpattern, :incampaign_id :start_with,:length)",
				"customerSearchByIndv");
		
		query.setParameter(SearchRepository.CUST_NAME, custName);
		query.setParameter(SearchRepository.VEH_REG_NO, vehNum);
		query.setParameter(SearchRepository.PHONE_NUM, phNumber);
		query.setParameter(SearchRepository.SEARCH_DT_FROM, serDateFrom);
		query.setParameter(SearchRepository.SEARCH_DT_TO, serDateTo);
		query.setParameter(SearchRepository.MODEL, model);
		query.setParameter(SearchRepository.VARIANT, variant);
		query.setParameter(SearchRepository.CUST_CAT, custCat);
		query.setParameter(SearchRepository.SEARCHPATTERN, "");
		query.setParameter("incampaign_id", 4);
		query.setParameter(SearchRepository.START_INDX, start);
		query.setParameter(SearchRepository.SIZE, length);

		List<CustomerSearchResult> searchResults = query.getResultList();
		logger.info("size of saved result"+searchResults.size());
		
		SavedSearchResult savedSearchResult = new SavedSearchResult();
		
		searchResults.forEach(result ->{
			em.persist(result);
		});
		em.flush();
		
		savedSearchResult.setSavedName(searchName);
		savedSearchResult.setSearchResults(searchResults);		
		em.persist(savedSearchResult);
		
		
		return true;
	}
	


	@Override
	public long getCustomerCountIndv(String searchCust, String searchVeh, String searchPh, String searchserDateFrom,
			String searchDateTo, String searchModel, String searchVar, String searchCat, String searchDisp, String searchPattern) {

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("customer_search_by_individual_params_count");

		sQuery.registerStoredProcedureParameter("incustname", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("invehregno", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inphnumber", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("innxtserstrdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("innxtserenddate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inmodel", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("invariant", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("custcat", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("searchpattern", String.class, ParameterMode.IN);


		sQuery.setParameter("incustname", searchCust);
		sQuery.setParameter("invehregno", searchVeh);
		sQuery.setParameter("inphnumber", searchPh);
		sQuery.setParameter("innxtserstrdate", searchserDateFrom);
		sQuery.setParameter("innxtserenddate", searchDateTo);
		sQuery.setParameter("inmodel", searchModel);
		sQuery.setParameter("invariant", searchVar);
		sQuery.setParameter("custcat", searchCat);
		sQuery.setParameter("searchpattern",searchPattern);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	
	/*@Override
	public long getCustomerCountIndv(String custName, String vehNum, String phNumber, String serDateFrom,String serDateTo, String model,
			String variant, String custCat, String lastDisp) {

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("customer_search_by_individual_params_count");

		sQuery.registerStoredProcedureParameter("incustname", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("invehregno", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inphnumber", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("innxtserstrdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("innxtserenddate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inmodel", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("invariant", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inlastdispo", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("custcat", String.class, ParameterMode.IN);

		sQuery.setParameter("incustname", custName);
		sQuery.setParameter("invehregno", vehNum);
		sQuery.setParameter("inphnumber", phNumber);
		sQuery.setParameter("innxtserstrdate", serDateFrom);
		sQuery.setParameter("innxtserenddate", serDateTo);
		sQuery.setParameter("inmodel", model);
		sQuery.setParameter("invariant", variant);
		sQuery.setParameter("inlastdispo", lastDisp);
		sQuery.setParameter("custcat", custCat);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}*/
	@Override
	public List<CustomerSearchResult> searchCustomersIndv(String searchCust, String searchVeh, String searchPh,
			String searchserDateFrom, String searchDateTo, String searchModel, String searchVar, String searchCat,
			String searchDisp, String searchPattern, long fromIndex, long toIndex) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL customer_search_by_individual_params(:incustname, :invehregno, :inphnumber, :innxtserstrdate, :innxtserenddate, :inmodel, :invariant,:custcat,:searchpattern,:incampaign_id, :start_with,:length)",
				"customerSearchByIndv");
		
		
		query.setParameter(SearchRepository.CUST_NAME, searchCust);
		query.setParameter(SearchRepository.VEH_REG_NO, searchVeh);
		query.setParameter(SearchRepository.PHONE_NUM, searchPh);
		query.setParameter(SearchRepository.SEARCH_DT_FROM, searchserDateFrom);
		query.setParameter(SearchRepository.SEARCH_DT_TO, searchDateTo);
		query.setParameter(SearchRepository.MODEL, searchModel);
		query.setParameter(SearchRepository.VARIANT, searchVar);
		query.setParameter(SearchRepository.CUST_CAT, searchCat);			
		query.setParameter(SearchRepository.SEARCHPATTERN, searchPattern);
		query.setParameter("incampaign_id", 4);
		query.setParameter(SearchRepository.START_INDX, fromIndex);
		query.setParameter(SearchRepository.SIZE, toIndex);

		return query.getResultList();

	}
	
	/*@Override
	public List<CustomerSearchResult> searchCustomersIndv(String custName, String vehNum, String phNumber,
			String serDateFrom, String serDateTo, String model, String variant, String custCat, String lastDisp, long start, long length) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL customer_search_by_individual_params(:incustname, :invehregno, :inphnumber, :innxtserstrdate, :innxtserenddate, :inmodel, :invariant, :inlastdispo, :custcat, :start_with,:length)",
				"customerSearchByIndv");

		query.setParameter(SearchRepository.CUST_NAME, custName);
		query.setParameter(SearchRepository.VEH_REG_NO, vehNum);
		query.setParameter(SearchRepository.PHONE_NUM, phNumber);
		query.setParameter(SearchRepository.SEARCH_DT_FROM, serDateFrom);
		query.setParameter(SearchRepository.SEARCH_DT_TO, serDateTo);
		query.setParameter(SearchRepository.MODEL, model);
		query.setParameter(SearchRepository.VARIANT, variant);
		query.setParameter(SearchRepository.CUST_CAT, custCat);
		query.setParameter(SearchRepository.LAST_DISP, lastDisp);
		query.setParameter(SearchRepository.START_INDX, start);
		query.setParameter(SearchRepository.SIZE, length);

		return query.getResultList();

	}*/
		
		
	@Override
	public List<String> getBookedServiceType() { 	 
		List<String> bookedServiceTypeList = source.serviceBooked(em).where(u->u.getServiceBookedType() != null).select(u->u.getServiceBookedType()).distinct().toList();
		return bookedServiceTypeList;
	}
	@Override
	public long getCountForMissedIncominOutgoing(String managerName, String callType, String CREIds, String toCallDate,
			String fromCallDate, String searchPattern) {
		
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("callogMissedIncomingcount");

		sQuery.registerStoredProcedureParameter("manager_username", String.class, ParameterMode.IN); // coulmn names
		sQuery.registerStoredProcedureParameter("in_calltype", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inwyzuser_id", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("fromcalldate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("tocalldate", String.class , ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class , ParameterMode.IN);
		
		
		sQuery.setParameter("manager_username", managerName);
		sQuery.setParameter("in_calltype", callType);
		sQuery.setParameter("inwyzuser_id", CREIds);
		sQuery.setParameter("fromcalldate", fromCallDate);
		sQuery.setParameter("tocalldate", toCallDate);
		sQuery.setParameter("pattern", searchPattern);
		

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<CallLogDispositionLoadMR> getDataMissedIncominOutgoing(String managerName, String callType, String CREIds,
			String fromCallDate, String toCallDate, String searchPattern, long fromIndex, long toIndex) {
		
		javax.persistence.Query query = em.createNativeQuery(
				"CALL callogMissedIncoming(:manager_username,:in_calltype,:inwyzuser_id,:fromcalldate,:tocalldate,:pattern,:start_with,:length)",
				"CallLogDispositionLoadMROthers");

		query.setParameter("manager_username",managerName);
		query.setParameter("in_calltype",callType);
		query.setParameter("inwyzuser_id",CREIds);
		query.setParameter("fromcalldate",fromCallDate);
		query.setParameter("tocalldate",toCallDate);
		query.setParameter("pattern",searchPattern);
		query.setParameter("start_with",fromIndex);
		query.setParameter("length",toIndex);

		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();
	}

	@Override
	public long serviceBookDashboardCountManager(String userLoginName) {
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("DashServiceBookingCountOverall");

		sQuery.registerStoredProcedureParameter("mgr_user_id", String.class, ParameterMode.IN);
		sQuery.setParameter("mgr_user_id",userLoginName);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public long serviceBookDashboardCount(long id) {
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("DashServiceBookingCountCREwise");

		sQuery.registerStoredProcedureParameter("in_wyzuser_id", Integer.class, ParameterMode.IN);
		sQuery.setParameter("in_wyzuser_id", (int) id);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public long getTotalCountOfCustomers() {
		// TODO Auto-generated method stub
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("TotalCustomerCount");

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public long getAllCallhistoryCount(String userLoginName, String fromDateNew, String toDateNew, String location,
			String workshop, String workshops, String bookingstatus, String disposition, String creidsare,
			String patternCount){
		// TODO Auto-generated method stub
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("callhistorycubereportcount");

		sQuery.registerStoredProcedureParameter("mgrusername", String.class, ParameterMode.IN); 
		sQuery.registerStoredProcedureParameter("infromdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("intodate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("users", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("increworkshopid", String.class , ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inbookedworkshopid", String.class , ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("locationid", String.class , ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("indispo", String.class , ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inbookingStatus", String.class , ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class , ParameterMode.IN);

		sQuery.setParameter("mgrusername", userLoginName);
		sQuery.setParameter("infromdate", fromDateNew);
		sQuery.setParameter("intodate", toDateNew);
		sQuery.setParameter("users", creidsare);
		sQuery.setParameter("increworkshopid", workshop);
		sQuery.setParameter("inbookedworkshopid", workshops);
		sQuery.setParameter("locationid", location);
		sQuery.setParameter("indispo", disposition);
		sQuery.setParameter("inbookingStatus", bookingstatus);
		sQuery.setParameter("pattern", patternCount);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();	
		}

	@Override
	public List<CallInteractionHistoryCubeReport> getAllCallHistoryData(String userLoginName, String fromDateNew,
			String toDateNew, String location, String workshop,String workshops,String bookingstatus, String creidsare, String disposition, long fromIndex,
			long toIndex) {
		// TODO Auto-generated method stub
		javax.persistence.Query query = em.createNativeQuery(
				"CALL callhistorycubereport(:mgrusername,:infromdate,:intodate,:users,:increworkshopid,:inbookedworkshopid,:locationid,:indispo,:inbookingStatus,:startwith,:length)",
				"HistoryCubeReport");
		query.setParameter("mgrusername", userLoginName);
		query.setParameter("infromdate", fromDateNew);
		query.setParameter("intodate", toDateNew);
		query.setParameter("users", creidsare);
		query.setParameter("increworkshopid", workshop);
		query.setParameter("inbookedworkshopid", workshops);
		query.setParameter("locationid", location);
		query.setParameter("indispo", disposition);
		query.setParameter("inbookingStatus", bookingstatus);
		query.setParameter("startwith", fromIndex);
		query.setParameter("length", toIndex);
		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);
		return query.getResultList();
	}

	@Override
	public long getPSFassignedInteractionTableDataMRCount(String userLoginName, String fromDateNew, String toDateNew,
			String typeOfPSF, String searchPattern, String creidsare) {
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("CREManagerPSFScheduledCallsCount");

		sQuery.registerStoredProcedureParameter("mgrUserName", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("psfcampaignid", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("users", String.class, ParameterMode.IN);

		sQuery.setParameter("mgrUserName",userLoginName);
		sQuery.setParameter("instartdate",fromDateNew);
		sQuery.setParameter("inenddate",toDateNew);
		sQuery.setParameter("psfcampaignid",typeOfPSF);
		sQuery.setParameter("pattern",searchPattern);
		sQuery.setParameter("users",creidsare);


		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<CallLogAjaxLoadMR> PSFassignedListOfUserMR(String userLoginName, String fromDateNew, String toDateNew,
			String typeOfPSF, String searchPattern, String creidsare, long fromIndex, long toIndex) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL CREManagerPSFScheduledCalls(:mgrUserName,:instartdate,:inenddate,:psfcampaignid,:pattern,:users,:start_with,:length)",
				"CallLogAjaxLoadMRPSFAssigned");

		query.setParameter("mgrUserName",userLoginName);
		query.setParameter("instartdate",fromDateNew);
		query.setParameter("inenddate",toDateNew);
		query.setParameter("psfcampaignid",typeOfPSF);
		query.setParameter("pattern",searchPattern);
		query.setParameter("users",creidsare);
		query.setParameter("start_with",fromIndex);
		query.setParameter("length",toIndex);

		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();
	}


	@Override
	public long getPSFfollowUpCallLogTableDataMRCount(String userLoginName, String fromDateNew, String toDateNew,
			String campaignName, String searchPattern, String creidsare, long typeOfDispo) {
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("CreManagerPSFfileterForContactsCount");

		sQuery.registerStoredProcedureParameter("mgrUserName", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("psfcampaignid", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("users", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("dispositiontype", Long.class, ParameterMode.IN);

		sQuery.setParameter("mgrUserName",userLoginName);
		sQuery.setParameter("instartdate",fromDateNew);
		sQuery.setParameter("inenddate",toDateNew);
		sQuery.setParameter("psfcampaignid",campaignName);
		sQuery.setParameter("pattern",searchPattern);
		sQuery.setParameter("users",creidsare);
		sQuery.setParameter("dispositiontype",typeOfDispo);
		
		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<CallLogDispositionLoadMR> getPSFfollowUpCallLogTableDataMR(String userLoginName, String fromDateNew,
			String toDateNew, String campaignName, String searchPattern, String creidsare, long typeOfDispo,
			long fromIndex, long toIndex) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL CreManagerPSFfileterForContacts(:mgrUserName,:instartdate,:inenddate,:psfcampaignid,:pattern,:users,:dispositiontype,:start_with,:length)",
				"CallLogDispositionLoadMRPSFContacts");

		query.setParameter("mgrUserName",userLoginName);
		query.setParameter("instartdate",fromDateNew);
		query.setParameter("inenddate",toDateNew);
		query.setParameter("psfcampaignid",campaignName);
		query.setParameter("pattern",searchPattern);
		query.setParameter("users",creidsare);
		query.setParameter("dispositiontype",typeOfDispo);
		query.setParameter("start_with",fromIndex);
		query.setParameter("length",toIndex);

		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();
	}

	@Override
	public long getPSFnonContactsServerDataTableMRCount(String userLoginName, String fromDateNew, String toDateNew,
			String typeOfPSF, String searchPattern, String creidsare, long typeOfDispo) {
		// TODO Auto-generated method stub
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("creManagerPSFfileterForNonContactsCount");

		sQuery.registerStoredProcedureParameter("mgrUserName", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("psfcampaignid", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("users", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("dispositiontype", Long.class, ParameterMode.IN);

		sQuery.setParameter("mgrUserName",userLoginName);
		sQuery.setParameter("instartdate",fromDateNew);
		sQuery.setParameter("inenddate",toDateNew);
		sQuery.setParameter("psfcampaignid",typeOfPSF);
		sQuery.setParameter("pattern",searchPattern);
		sQuery.setParameter("users",creidsare);
		sQuery.setParameter("dispositiontype",typeOfDispo);
		
		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<CallLogDispositionLoadMR> getPSFnonContactsServerDataTableMR(String userLoginName, String fromDateNew,
			String toDateNew, String typeOfPSF, String searchPattern, String creidsare, long typeOfDispo,
			long fromIndex, long toIndex) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL creManagerPSFfileterForNonContacts(:mgrUserName,:instartdate,:inenddate,:psfcampaignid,:pattern,:users,:dispositiontype,:start_with,:length)",
				"CallLogDispositionLoadMRPSFNONContacts");

		query.setParameter("mgrUserName",userLoginName);
		query.setParameter("instartdate",fromDateNew);
		query.setParameter("inenddate",toDateNew);
		query.setParameter("psfcampaignid",typeOfPSF);
		query.setParameter("pattern",searchPattern);
		query.setParameter("users",creidsare);
		query.setParameter("dispositiontype",typeOfDispo);
		query.setParameter("start_with",fromIndex);
		query.setParameter("length",toIndex);

		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();
	}
	@Override
	public long getInsuranceassignedInteractionTableDataMRCount(String userLoginName, String creidsare ,String typeOfPSF ,String fromDateNew, String toDateNew,
			String renewalType, String searchPattern) {
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("cremanagerinsuranceScheduledCallsCount");

		sQuery.registerStoredProcedureParameter("mgrUserName", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("users", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("in_campaign_id", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("in_renewalType", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);


		sQuery.setParameter("mgrUserName",userLoginName);
		sQuery.setParameter("users",creidsare);
		sQuery.setParameter("in_campaign_id",typeOfPSF);
		sQuery.setParameter("instartdate",fromDateNew);
		sQuery.setParameter("inenddate",toDateNew);
		sQuery.setParameter("in_renewalType",renewalType);
		sQuery.setParameter("pattern",searchPattern);



		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<CallLogAjaxLoadMR> InsuranceassignedListOfUserMR(String userLoginName,String creidsare,String typeOfPSF, 
			String fromDateNew, String toDateNew,String renewalType,String searchPattern, long fromIndex, long toIndex) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL cremanagerinsuranceScheduledCalls(:mgrUserName,:users,:in_campaign_id,:instartdate,:inenddate,:in_RenewalType,:pattern,:start_with,:length)",
				"CallLogAjaxLoadMRInsuranceAssigned");

		query.setParameter("mgrUserName",userLoginName);
		query.setParameter("users",creidsare);
		query.setParameter("in_campaign_id",typeOfPSF);
		query.setParameter("instartdate",fromDateNew);
		query.setParameter("inenddate",toDateNew);
		query.setParameter("in_RenewalType",renewalType);
		query.setParameter("pattern",searchPattern);
		query.setParameter("start_with",fromIndex);
		query.setParameter("length",toIndex);

		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();
	}


	@Override
	public long getInsurancefollowUpCallLogTableDataMRCount(String userLoginName, String creidsare, String fromDateNew, String toDateNew,
			String typeOfPSF, String renewalType, String searchPattern, long typeOfDispo) {
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("cremanagerinsurancefilterforDispositionContactsCount");

		sQuery.registerStoredProcedureParameter("mgrUserName", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("users", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("in_campaign_id", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("induetype", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("dispositiontype", Long.class, ParameterMode.IN);

		sQuery.setParameter("mgrUserName",userLoginName);
		sQuery.setParameter("users",creidsare);
		sQuery.setParameter("instartdate",fromDateNew);
		sQuery.setParameter("inenddate",toDateNew);
		sQuery.setParameter("in_campaign_id",typeOfPSF);
		sQuery.setParameter("induetype",renewalType);
		sQuery.setParameter("pattern",searchPattern);
		sQuery.setParameter("dispositiontype",typeOfDispo);
		
		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<CallLogDispositionLoadMR> getInsurancefollowUpCallLogTableDataMR(String userLoginName, String creidsare, String fromDateNew, String toDateNew,
			String typeOfPSF,String renewalType, String searchPattern,long typeOfDispo, long fromIndex, long toIndex) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL cremanagerinsurancefilterforDispositionContacts(:mgrUserName,:users,:instartdate,:inenddate,:in_campaign_id,:induetype,:pattern,:dispositiontype,:start_with,:length)",
				"CallLogDispositionLoadMRInsuranceContacts");

		query.setParameter("mgrUserName",userLoginName);
		query.setParameter("users",creidsare);
		query.setParameter("instartdate",fromDateNew);
		query.setParameter("inenddate",toDateNew);
		query.setParameter("in_campaign_id",typeOfPSF);
		query.setParameter("induetype",renewalType);
		query.setParameter("pattern",searchPattern);
		query.setParameter("dispositiontype",typeOfDispo);
		query.setParameter("start_with",fromIndex);
		query.setParameter("length",toIndex);

		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();
	}

	@Override
	public long getInsurancenonContactsServerDataTableMRCount(String userLoginName, String creidsare, String fromDateNew, String toDateNew,
			String typeOfPSF,String renewalType, String searchPattern,long typeOfDispo,String droppedCount, String lastDipo) {
		// TODO Auto-generated method stub
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("cremanagerinsurancefilterforNonContactsCount");

		sQuery.registerStoredProcedureParameter("mgrUserName", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("users", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("in_campaign_id", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("induetype", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("dispositiontype", Long.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("indroppedcount", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inLastdisposition", String.class, ParameterMode.IN);

		sQuery.setParameter("mgrUserName",userLoginName);
		sQuery.setParameter("users",creidsare);
		sQuery.setParameter("instartdate",fromDateNew);
		sQuery.setParameter("inenddate",toDateNew);
		sQuery.setParameter("in_campaign_id",typeOfPSF);
		sQuery.setParameter("induetype",renewalType);
		sQuery.setParameter("pattern",searchPattern);
		sQuery.setParameter("dispositiontype",typeOfDispo);
		sQuery.setParameter("indroppedcount",droppedCount);
		sQuery.setParameter("inLastdisposition",lastDipo);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<CallLogDispositionLoadMR> getInsurancenonContactsServerDataTableMR(String userLoginName,String creidsare, String fromDateNew, String toDateNew,
			String typeOfPSF, String renewalType,String searchPattern, long typeOfDispo,String dropppedCount,String lastDipo, long fromIndex, long toIndex) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL cremanagerinsurancefilterforNonContacts(:mgrUserName,:users,:instartdate,:inenddate,:in_campaign_id,:induetype,:pattern,:dispositiontype,:indroppedcount,:inLastdisposition,:start_with,:length)",
				"CallLogDispositionLoadMRInsuranceNonContacts");

		query.setParameter("mgrUserName",userLoginName);
		query.setParameter("users",creidsare);
		query.setParameter("instartdate",fromDateNew);
		query.setParameter("inenddate",toDateNew);
		query.setParameter("in_campaign_id",typeOfPSF);
		query.setParameter("induetype",typeOfPSF);
		query.setParameter("pattern",searchPattern);
		query.setParameter("dispositiontype",typeOfDispo);
		query.setParameter("indroppedcount",typeOfPSF);
		query.setParameter("inLastdisposition",typeOfPSF);
		query.setParameter("start_with",fromIndex);
		query.setParameter("length",toIndex);

		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();
	}
	@Override
	public long getAllSMShistoryCount(String convertedFromDate, String convertedToDate, String creidsare,
			String sentStatus) {
		// TODO Auto-generated method stub
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("smsHistoryReportsCount");

		sQuery.registerStoredProcedureParameter("inStartdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inEndDate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inusers", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("sentStatus", String.class, ParameterMode.IN);

		sQuery.setParameter("inStartdate",convertedFromDate);
		sQuery.setParameter("inEndDate",convertedToDate);
		sQuery.setParameter("inusers",creidsare);
		sQuery.setParameter("sentStatus",sentStatus);
		
		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<CallInteractionHistory> getAllSMSHistoryData(String convertedFromDate, String convertedToDate, String creidsare,
			String sentStatus, long fromIndex, long toIndex) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL smsHistoryReports(:inStartdate,:inEndDate,:inusers,:sentStatus,:start_with,:length)",
				"SMSInteractionHistory");

		query.setParameter("inStartdate",convertedFromDate);
		query.setParameter("inEndDate",convertedToDate);
		query.setParameter("inusers",creidsare);
		query.setParameter("sentStatus",sentStatus);
		query.setParameter("start_with",fromIndex);
		query.setParameter("length",toIndex);

		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();
	}
	
	@Override
	public long getAllInsurancehistoryCount(String convertedFromDate, String convertedToDate, String creidsare,
			String campaignName) {
		// TODO Auto-generated method stub
				StoredProcedureQuery sQuery = em.createStoredProcedureQuery("InsuranceCallHistoryReportsCount");

				sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN);
				sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
				sQuery.registerStoredProcedureParameter("users", String.class, ParameterMode.IN);
				sQuery.registerStoredProcedureParameter("campaignName", String.class, ParameterMode.IN);

				sQuery.setParameter("instartdate",convertedFromDate);
				sQuery.setParameter("inenddate",convertedToDate);
				sQuery.setParameter("users",creidsare);
				sQuery.setParameter("campaignName",campaignName);
				
				BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

				return bigCount.longValue();
	}

	@Override
	public List<InsuranceHistoryReport> getAllInsuranceHistoryData(String convertedFromDate, String convertedToDate,
			String creidsare, String campaignName, long fromIndex, long toIndex) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL InsuranceCallHistoryReports(:instartdate,:inenddate,:users,:campaignName,:start_with,:length)",
				"InsuranceHistoryReport");

		query.setParameter("instartdate",convertedFromDate);
		query.setParameter("inenddate",convertedToDate);
		query.setParameter("users",creidsare);
		query.setParameter("campaignName",campaignName);
		query.setParameter("start_with",fromIndex);
		query.setParameter("length",toIndex);

		return query.getResultList();
	}
	
	@Override
	public List<Long> getInteractionID(long campainIdIS, long serviceId) {
		// TODO Auto-generated method stub
		
		List<Long> listData=new ArrayList<>();
		
		long countIs=source.psfAssignedInteraction(em)
				.where(u -> u.getCampaign()!=null && u.getService()!=null)
				.where(u -> u.getCallMade().equals("No")
				&& u.getCampaign().getId() == campainIdIS
				&& u.getService().getId() ==  serviceId).count();
		
		if(countIs != 0){
			
			long assignIs=source.psfAssignedInteraction(em)
					.where(u -> u.getCampaign()!=null && u.getService()!=null)
					.where(u -> u.getCallMade().equals("No")
					&& u.getCampaign().getId() == campainIdIS
					&& u.getService().getId() ==  serviceId).max(u -> u.getId());
			
			listData.add(0, (long) 0);
			listData.add(1,assignIs);
			
			
		}else{
			
			long assignIs=source.psfAssignedInteraction(em)
					.where(u -> u.getCampaign()!=null && u.getService()!=null)
					.where(u -> u.getCallMade().equals("Yes")
					&& u.getCampaign().getId() == campainIdIS
					&& u.getService().getId() ==  serviceId).max(u -> u.getId());
			
			
			long callIds=source.callinteractions(em).where(u ->u.getPsfAssignedInteraction()!=null && u.getPsfdisposition()!=null)
					.where(u -> u.getPsfAssignedInteraction().getId() == assignIs).count();
			
			if(callIds!=0){
				
				long callinteractionId=source.callinteractions(em).where(u ->u.getPsfAssignedInteraction()!=null && u.getPsfdisposition()!=null)
						.where(u -> u.getPsfAssignedInteraction().getId() == assignIs).max(u -> u.getId());
				listData.add(0, (long) 1);
				listData.add(1,callinteractionId );
				
			}else{
				listData.add(0, (long) 0);
				listData.add(1,assignIs);
			}
			
			
		}
		
		
		return listData;
	}
	@Override
	public long getdailyAvailabilityStatusreportCount(String userLoginName, String fromDateNew, String toDateNew,
			String creidsares, String workshopsBooked, String patternCount) {
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("dailyAvailabilityStatusCount");

		sQuery.registerStoredProcedureParameter("mgrusername", String.class, ParameterMode.IN); 
		sQuery.registerStoredProcedureParameter("inStartdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inEndDate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inusers", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("incampaignName", String.class , ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class , ParameterMode.IN);


		sQuery.setParameter("mgrusername", userLoginName);
		sQuery.setParameter("inStartdate", fromDateNew);
		sQuery.setParameter("inEndDate", toDateNew);
		sQuery.setParameter("inusers", creidsares);
		sQuery.setParameter("incampaignName", workshopsBooked);
		sQuery.setParameter("pattern", patternCount);


		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();	
	}

	@Override
	public List<DailyAvailabilityData> getdailyAvailabilityStatusreport(String userLoginName, String fromDateNew,
			String toDateNew, String creidsares, String workshopsBooked, long fromIndex, long toIndex) {
		
		
		// TODO Auto-generated method stub
		javax.persistence.Query query = em.createNativeQuery(
				"CALL dailyAvailabilityStatus(:mgrusername,:inStartdate,:inEndDate,:inusers,:incampaignName,:startwith,:length)",
				"DailyAvailabilityData");
		query.setParameter("mgrusername", userLoginName);
		query.setParameter("inStartdate", fromDateNew);
		query.setParameter("inEndDate", toDateNew);
		query.setParameter("inusers", creidsares);
		query.setParameter("incampaignName", workshopsBooked);
		query.setParameter("startwith", fromIndex);
		query.setParameter("length", toIndex);
		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);
		return query.getResultList();
	}

	@Override
	public long getdailyAssignmentStatusreportCount(String userLoginName, String fromDateNew, String toDateNew,
			String creidsares, String workshopsBooked, String patternCount) {
		
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("dailyAssignmentReportCount");

		
		sQuery.registerStoredProcedureParameter("mgrusername", String.class, ParameterMode.IN); 
		sQuery.registerStoredProcedureParameter("inStartdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inEndDate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inusers", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("incampaignName", String.class , ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class , ParameterMode.IN);


		sQuery.setParameter("mgrusername", userLoginName);
		sQuery.setParameter("inStartdate", fromDateNew);
		sQuery.setParameter("inEndDate", toDateNew);
		sQuery.setParameter("inusers", creidsares);
		sQuery.setParameter("incampaignName", workshopsBooked);
		sQuery.setParameter("pattern", patternCount);


		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<DailyAssignmentStatusData> getdailyAssignmentStatusreport(String userLoginName, String fromDateNew,
			String toDateNew, String creidsares, String workshopsBooked, long fromIndex, long toIndex) {

		// TODO Auto-generated method stub
		javax.persistence.Query query = em.createNativeQuery(
				"CALL dailyAssignmentReport(:mgrusername,:inStartdate,:inEndDate,:inusers,:incampaignName,:startwith,:length)",
				"DailyAssignmentStatusData");
		query.setParameter("mgrusername", userLoginName);
		query.setParameter("inStartdate", fromDateNew);
		query.setParameter("inEndDate", toDateNew);
		query.setParameter("inusers", creidsares);
		query.setParameter("incampaignName", workshopsBooked);
		query.setParameter("startwith", fromIndex);
		query.setParameter("length", toIndex);
		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);
		return query.getResultList();
	}
}
