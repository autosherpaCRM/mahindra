/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.Date;
import java.util.List;

import javax.inject.Named;

import models.ListingForm;
import models.Location;
import models.Role;
import models.Workshop;
import models.WyzUser;
import controllers.webmodels.AssignedIntreactionNoCall;
/**
 *
 * @author W-1004
 */

@Named
public interface SuperAdminRepository {


	public void addLocationdataBySuperAdmin(Location location, Workshop workshop, String userLoginName);

	public void addUsersBySuperAdmin(WyzUser wyzUser, Location location, Workshop workshop, Role role,String userLoginName,String userLogindealerCode);

	public List<WyzUser> getExistingWyzUsers(String uname);
	
	public List<AssignedIntreactionNoCall>getAssignedInteractionListData(long wyzuserId, String location,
			String moduleName, String campaignName, Date fromDate, Date toDate);

	public void deleteAssignedNoCall(ListingForm listing_Form) throws Exception;

	public List<String>getCampaignByType(String typeIs);
	

	//public List<SMSTemplate> getAllSMSTemplate(SMSTemplate smstemplate, String userLoginName, String userLogindealerCode);

}