package repositories;

import java.util.List;

import javax.inject.Named;

import controllers.webmodels.ChangeAssignment;
import models.Campaign;

@Named
public interface ChangeAssignmentRepository {

	List<ChangeAssignment> filteredAssignDataList(String selected_cre, String selected_dispositions, String fromdate, String todate,
			String campSelec,String campSelecType, long fromIndex,long toIndex);

	String getCampaignByID(long campaign_id);

	String getDisositionById(Long dispoId);

	void changeAssignedDataCalls(List<Long> callId, List<Long> campId, List<Long> userId);

	long filteredAssignDataListCount(String selected_cre, String selected_dispositions, String fromdate, String todate,
			String campSelec, String campSelecType);
	
	String getLocationIdByName(String cityName);
	
	public List<Campaign> getCampaignNameBasedOnType(String campaignTypeId);

}
