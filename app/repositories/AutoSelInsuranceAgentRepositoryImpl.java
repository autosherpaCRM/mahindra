package repositories;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import configs.JinqSource;
import controllers.webmodels.InsuranceAgentIdName;
import controllers.webmodels.ServiceAdvisorIdName;
import play.Logger.ALogger;
import utils.WyzCRMCacheInsurance;

@Repository("autoSelInsuranceAgentRepository")
@Transactional
public class AutoSelInsuranceAgentRepositoryImpl implements AutoSelInsuranceAgentRepository {

	ALogger logger = play.Logger.of("application");
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Autowired
	private WyzCRMCacheInsurance wyzCRMCacheInsurance;

	@Override
	public void getAllInsurAgentDetails(String dealercode) {
		// TODO Auto-generated method stub

		logger.info("Inside Repo initial setup");
		javax.persistence.Query query = em.createNativeQuery("CALL prioritised_insu_advisor_list()",
				"InsuranceAgentIdName");

		List<InsuranceAgentIdName> saList = query.getResultList();

		Map<Date, List<InsuranceAgentIdName>> mapsaList = saList.stream()
				.collect(Collectors.groupingBy(InsuranceAgentIdName::getDate));

		mapsaList.forEach((date, saData) -> {

			wyzCRMCacheInsurance.setupInsurnaceAgentQueue(date, saData,dealercode);

		});
	}

	@Override
	public List<InsuranceAgentIdName> getInsurAgentDetails(Date date) {
		javax.persistence.Query query = em.createNativeQuery("CALL prioritised_single_Insu_advisor(:date)",
				"InsuranceAgentIdName");

		query.setParameter("date", date);

		return query.getResultList();
	}

}
