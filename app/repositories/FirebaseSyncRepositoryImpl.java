/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.xml.bind.DatatypeConverter;

import models.AppointmentBooked;
import models.AssignedInteraction;
import models.CallDispositionData;
import models.CallInteraction;
import models.Customer;
import models.Dealer;
import models.Driver;
import models.Insurance;
import models.InsuranceAgent;
import models.InsuranceAssignedInteraction;
import models.InsuranceDisposition;
import models.PSFInteraction;
import models.PickupDrop;
import models.SRDisposition;
import models.Segment;
import models.Service;
import models.ServiceAdvisor;

import models.ServiceBooked;
import models.Vehicle;
import models.Workshop;
import models.WyzUser;
import models.PSFAssignedInteraction;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.google.firebase.database.DataSnapshot;

import actors.model.DriverPickupInfo;
import actors.model.ServiceAdvisorHistoryInfo;
import play.Logger.ALogger;
import configs.JinqSource;

@Repository("firebaseSyncRepository")
@Transactional
public class FirebaseSyncRepositoryImpl implements FirebaseSyncRepository {
	ALogger logger = play.Logger.of("application");

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Override
	public List<Dealer> getListOfDealers() {
		return source.dealers(em).toList();
	}

	@Override
	public List<CallInteraction> getListOfServicesNotSynced(String dealer) {

		List<CallInteraction> serviceBookedToday = new ArrayList<CallInteraction>();

		String query = "USE " + dealer + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) {
				Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution = smt.execute(query);

					String dispo = getDispositionValueById(3);

					List<CallInteraction> servicebooked_callinteraction = source.callinteractions(em)
							.where(u -> u.getSrdisposition().getCallDispositionData().getDisposition().equals(dispo)
									&& u.getSynchedToFirebase().equals("false"))
							.where(u -> u.getServiceBooked().getServiceBookStatus().getDisposition().equals(dispo))
							.toList();

					logger.info("serviceBookedToday count :" + serviceBookedToday.size());
					for (CallInteraction ser_data : servicebooked_callinteraction) {

						if (ser_data.getServiceBooked().getServiceScheduledDateStr()
								.equals(getCurrentDateInStringFormate())
								|| ser_data.getServiceBooked().getServiceScheduledDateStr()
										.equals(getNextDateInStringFormate())) {

							serviceBookedToday.add(ser_data);
							Hibernate.initialize(ser_data.getCustomer().getPreferredPhone());
							Hibernate.initialize(ser_data.getCustomer().getPreferredAdress());
							Hibernate.initialize(ser_data.getCustomer().getSegment());
						}

					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					logger.info("SQL Exception");
				}

			}
		});

		return serviceBookedToday;
	}

	@Override
	public Service getServiceDataByVehicleId(long vehicle_id) {

		logger.info("vehicle_id is " + vehicle_id);
		/*
		 * List<Service> service_list = source.services(em).where(u ->
		 * u.getVehicle().getVehicle_id() == vehicle_id) .toList();
		 * List<Service> finalData = new ArrayList<>(); if (service_list.size()
		 * != 0) {
		 * 
		 * Date tmp = null; int i = 0; if (service_list.size() != 0) {
		 * 
		 * for (Service uniqueNo_list : service_list) { if
		 * (uniqueNo_list.getLastServiceDate() != null) { if (i == 0) { tmp =
		 * uniqueNo_list.getLastServiceDate(); i++; } else { if
		 * (tmp.before(uniqueNo_list.getLastServiceDate())) { tmp =
		 * uniqueNo_list.getLastServiceDate(); i++; } } } // logger.info(
		 * "getting latest disposition :" + tmp);
		 * finalData.add(service_list.get(i - 1)); } }
		 * 
		 * return finalData.get(0); } else { Service data = new Service();
		 * return data; }
		 */

		List<Service> service_list = source.services(em).where(u -> u.getVehicle().getVehicle_id() == vehicle_id)
				.where(u -> u.getLastServiceDate() != null).toList();

		Service lastService = new Service();
		if (service_list.size() > 0) {

			lastService = Collections.max(service_list, Comparator.comparing(c -> c.lastServiceDate));
			return lastService;
		} else {
			return lastService;
		}

		// Service latestService=service_list
		// .stream()
		// .filter(u -> u.getLastServiceDate().max(Service::lastServiceDate()))
		// .collect();

	}

	@Override
	public void updateSynchedToFirebase(long id, String referencekey) {

		CallInteraction updateFirebase = em.find(CallInteraction.class, id);
		updateFirebase.setSynchedToFirebase("true");
		updateFirebase.setServiceAdvisorfirebaseKey(referencekey);
		em.merge(updateFirebase);
	}

	@Override
	public String getDispositionValueById(int disposition_id) {

		CallDispositionData call_disposition = source.callDispositionDatas(em)
				.where(u -> u.getDispositionId() == disposition_id).getOnlyValue();
		String data_disposition = call_disposition.getDisposition();
		return data_disposition;
	}

	@Override
	public ServiceBooked getSBbyCallInterId(long id) {

		ServiceBooked countSB = source.callinteractions(em).where(u -> u.getId() == id)
				.select(u -> u.getServiceBooked()).getOnlyValue();
		return countSB;
	}

	@Override
	public void updateStatusOfServiceBookedOfSA(String fkeyString, String vehicle_Received) {

		long countOfSASB = source.callinteractions(em).where(u -> u.getFirebaseKey().equals(fkeyString)).count();

		if (countOfSASB != 0) {

			CallInteraction addStatus = source.callinteractions(em).where(u -> u.getFirebaseKey().equals(fkeyString))
					.getOnlyValue();
			addStatus.getServiceBooked().setStatusOfSB(vehicle_Received);
			em.merge(addStatus);

		}

	}

	@Override
	public List<WyzUser> getListOfServiceAdvisorData() {
		return source.wyzUsers(em).where(u -> u.getRole().equals("Service Advisor")).toList();
	}

	@Override
	public List<ServiceBooked> getListOfServicebookedData(String user) {

		return source.serviceBooked(em).where(u -> u.getServiceAdvisor().getAdvisorName().equals(user)).toList();
	}

	@Override
	public List<ServiceBooked> getDriverPickupDropList(String user) {

		long drivercount = source.driver(em).where(d -> d.getDriverName().equals(user)).count();
		if (drivercount > 0) {
			Driver driver = source.driver(em).where(d -> d.getDriverName().equals(user)).getOnlyValue();

			Hibernate.initialize(driver.getServicesBooked());
			List<ServiceBooked> servicesBooked = driver.getServicesBooked();
			for (ServiceBooked serviceBooked : servicesBooked) {
				Hibernate.initialize(serviceBooked.getCustomer());
				Hibernate.initialize(serviceBooked.getCustomer().getAddresses());
				Hibernate.initialize(serviceBooked.getCustomer().getPhones());
				Hibernate.initialize(serviceBooked.getCustomer().getAddresses());
				Hibernate.initialize(serviceBooked.getVehicle());
			}

			return servicesBooked;
		} else {
			List<ServiceBooked> list = new ArrayList<ServiceBooked>();
			return list;
		}
	}

	@Override
	public List<Driver> getAllDrivers() {

		long count = source.driver(em).count();
		if (count > 0) {
			return source.driver(em).toList();
		} else {
			List<Driver> driverList = new ArrayList<Driver>();
			return driverList;
		}

	}

	public void addTestServiceBooked() {

		ServiceBooked booked = new ServiceBooked();
		Driver driver = em.find(Driver.class, new Long(1));
		booked.setCustomer(em.find(Customer.class, new Long(1)));
		booked.setDriver(driver);
		booked.setServiceAdvisor(em.find(ServiceAdvisor.class, new Long(1)));
		booked.setWorkshop(em.find(Workshop.class, new Long(1)));
		booked.setIsPickupRequired(true);
		booked.setVehicle(em.find(Vehicle.class, new Long(1)));

		PickupDrop pickupDrop = new PickupDrop();
		pickupDrop.setDriver(em.find(Driver.class, new Long(1)));
		pickupDrop.setIsdrop(false);
		pickupDrop.setIspickup(true);
		pickupDrop.setPickUpAddress("testAdress");
		pickupDrop.setTimeFrom(new Date());
		pickupDrop.setTimeTo(new Date());
		try {
			pickupDrop.setCRN(generateCRN());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		em.persist(booked);
		booked.setPickupDrop(pickupDrop);
		em.persist(pickupDrop);

	}

	private String generateCRN() throws NoSuchAlgorithmException {
		StringBuilder token = new StringBuilder();

		SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

		for (int i = 0; i < 6; i++) {
			token.append(random.nextInt(9));
		}
		return token.toString();
	}

	@Override
	public Segment getSegmentByCustomerId(long id) {

		List<Segment> segmentData = source.segments(em).where(u -> u.getCustomer().getId() == id).toList();

		if (segmentData.size() != 0) {

			return segmentData.get(0);

		} else {

			return new Segment();
		}
	}

	@Override
	public void addDriverInteraction(String firebaseKey, DriverPickupInfo info) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<ServiceAdvisor> getAllActiveServiceAdvisors(String dealer) {

		List<ServiceAdvisor> activeList = new ArrayList<ServiceAdvisor>();

		String query = "USE " + dealer + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) {
				Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution = smt.execute(query);

					List<ServiceAdvisor> allAdvisor = source.serviceadvisor(em).toList();

					for (ServiceAdvisor advisor : allAdvisor) {

						if (advisor.isActive) {

							activeList.add(advisor);
						}

					}

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					logger.info("SQL Exception");
				}

			}
		});
		return activeList;

	}

	@Override
	public void addServiceAdvisorHistory(String firebaseKey, ServiceAdvisorHistoryInfo callinfo,
			DataSnapshot dataSnapshot) {

		String dealer = callinfo.getDealerCode();

		String query = "USE " + dealer + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) {
				Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution = smt.execute(query);

					logger.info("firebaseKey from addServiceAdvisorHistory : " + firebaseKey);

					CallInteraction interaction = new CallInteraction();

					String date = callinfo.getCallDate();
					String time = callinfo.getCallTime();

					String dateTimeStr = date + " " + time;
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

					SimpleDateFormat formatSch = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
					logger.info("Call time time: " + dateTimeStr);
					logger.info("call Date :" + date + "Call Time :" + time);

					try {
						Date datetime = format.parse(dateTimeStr);
						interaction.setCallMadeDateAndTime(datetime);
					} catch (ParseException e) {
						logger.info("Could not parse date");
						e.printStackTrace();
					}

					String base64String = callinfo.getMediaFile();
					callinfo.setMediaFile("");

					byte[] lobvalue = null;

					if (base64String != null) {
						if (base64String.length() > 0) {
							lobvalue = DatatypeConverter.parseBase64Binary(base64String);
						}
					}

					if (callinfo.getFilePath() != null) {
						interaction.setFilePath(callinfo.getFilePath());
					}
					if (callinfo.getLatitude() != null) {
						interaction.setLatitude(callinfo.getLatitude());
					}
					if (callinfo.getLongitude() != null) {
						interaction.setLongitude(callinfo.getLongitude());
					}
					interaction.setMediaFileLob(lobvalue);
					interaction.setCallDate(date);
					interaction.setCallTime(time);
					interaction.setFirebaseKey(firebaseKey);
					interaction.setCallDuration(callinfo.getCallDuration());
					interaction.setRingTime(callinfo.getRingTime());
					interaction.setCallType(callinfo.getCallType());
					interaction.setMakeCallFrom(callinfo.getMakeCallFrom());
					interaction.setDealerCode(callinfo.getDealerCode());
					interaction.setDailedNoIs(callinfo.getCustomerPhone());

					WyzUser user = em.find(WyzUser.class, Long.parseLong(callinfo.getUserId()));
					Vehicle vehicle = em.find(Vehicle.class, Long.parseLong(callinfo.getVehicleId()));
					Customer customer = em.find(Customer.class, Long.parseLong(callinfo.getCustId()));
					ServiceBooked servicebooked = em.find(ServiceBooked.class,
							Long.parseLong(callinfo.getServiceBookedId()));

					/*
					 * ServiceAdvisorInteractionStatus statusUpdate =
					 * getStatusByInteractionKeyId(
					 * Integer.parseInt(callinfo.getStatus()));
					 */

					CallDispositionData serviceBookStatus = getStatusByInteractionKeyIdStatus(
							Integer.parseInt(callinfo.getStatus()));

					if (servicebooked.getCallInteraction() != null) {

						int maxServ = servicebooked.getCallInteraction().size();
						if (maxServ > 0) {

							AssignedInteraction assigndList = servicebooked.getCallInteraction().get(maxServ - 1)
									.getAssignedInteraction();

							assigndList.setLastDisposition(assigndList.getFinalDisposition().getDisposition());
							assigndList.setFinalDisposition(serviceBookStatus);
							em.merge(assigndList);

							interaction.setServiceAdvisorfirebaseKey(
									servicebooked.getCallInteraction().get(maxServ - 1).getServiceAdvisorfirebaseKey());
							interaction.setAssignedInteraction(
									servicebooked.getCallInteraction().get(maxServ - 1).getAssignedInteraction());
							interaction.setCampaign(servicebooked.getCallInteraction().get(maxServ - 1).getCampaign());
						}
					}

					interaction.setWyzUser(user);
					interaction.setVehicle(vehicle);
					interaction.setCustomer(customer);

					SRDisposition disposition = new SRDisposition();
					disposition.setCallDispositionData(serviceBookStatus);

					if (callinfo.getNoServiceReason() != null) {

						CallDispositionData statusUpdate1 = getStatusByInteractionKeyIdStatus(
								Integer.parseInt(callinfo.getNoServiceReason()));

						disposition.setNoServiceReason(statusUpdate1.getDisposition());
					}

					if (callinfo.getAlreadyservicedatId() != null) {

						CallDispositionData statusUpdate1 = getStatusByInteractionKeyIdStatus(
								Integer.parseInt(callinfo.getAlreadyservicedatId()));

						disposition.setReasonForHTML(statusUpdate1.getDisposition());
					}

					if (callinfo.getComments() != null) {
						disposition.setComments(callinfo.getComments());
					}

					if (callinfo.getMileage() != null) {
						disposition.setMileageAsOnDate(callinfo.getMileage());
					}

					if (callinfo.getDealerName() != null) {
						disposition.setDealerName(callinfo.getDealerName());
					}

					if (callinfo.getMileageAtService() != null) {
						disposition.setMileageAtService(callinfo.getMileageAtService());
					}

					if (callinfo.getDateOfService() != null) {
						disposition.setDateOfService(callinfo.getDateOfService());
					}

					if (callinfo.getCity() != null) {
						disposition.setCityName(callinfo.getCity());
					}

					if (callinfo.getStatus().equals("3")) {

						logger.info("Booking reschedule");

						ServiceBooked newServiceBooked = new ServiceBooked();

						newServiceBooked.setServiceAdvisor(servicebooked.getServiceAdvisor());
						newServiceBooked.setCustomer(servicebooked.getCustomer());
						newServiceBooked.setVehicle(servicebooked.getVehicle());
						newServiceBooked.setDriver(servicebooked.getDriver());
						newServiceBooked.setWorkshop(servicebooked.getWorkshop());
						newServiceBooked.setServiceBookType(servicebooked.getServiceBookType());
						newServiceBooked.setServiceBookedType(servicebooked.getServiceBookedType());
						newServiceBooked.setTypeOfPickup(servicebooked.getTypeOfPickup());
						newServiceBooked.setIsPickupRequired(servicebooked.isPickupRequired());
						newServiceBooked.setServiceBookingAddress(servicebooked.getServiceBookingAddress());
						newServiceBooked.setServiceBookStatus(serviceBookStatus);
						String callDate = callinfo.getFollowUpDate();
						String callTime = callinfo.getFollowUpTime();

						String schdateTimeStr = callDate + " " + callTime;
						SimpleDateFormat formating = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						try {
							Date datetime = formating.parse(schdateTimeStr);
							logger.info("datetime :" + datetime);
							newServiceBooked.setScheduledDateTime(datetime);
						} catch (ParseException e) {
							logger.info("Could not parse date");
							e.printStackTrace();
						}

						interaction.setSynchedToFirebase("true");
						interaction.setServiceBooked(newServiceBooked);

						int dispoId = 35;
						servicebooked.setStatusOfSB("Booking Resheduled");
						servicebooked.setServiceBookStatus(getStatusByInteractionKeyIdStatus(dispoId));
						em.merge(servicebooked);
						em.flush();

						interaction.setServiceBooked(newServiceBooked);

					} else {

						interaction.setServiceBooked(servicebooked);

					}

					// }
					em.persist(interaction);

					disposition.setCallInteraction(interaction);
					em.persist(disposition);
					lobvalue = null;
					dataSnapshot.getRef().removeValue();

				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					logger.info("SQL Exception");
				}

			}

		});

	}

	private CallDispositionData getStatusByInteractionKeyIdStatus(int statusId) {
		// TODO Auto-generated method stub

		CallDispositionData calldispo = source.callDispositionDatas(em).where(u -> u.getDispositionId() == statusId)
				.getOnlyValue();

		return calldispo;
	}

	/*
	 * private ServiceAdvisorInteractionStatus getStatusByInteractionKeyId(int
	 * statusId) { // TODO Auto-generated method stub return
	 * source.serviceAdvisorInteractionStatus(em).where(u ->
	 * u.getInteractionKey() == statusId).getOnlyValue(); }
	 */
	@Override
	public String getCurrentDateInStringFormate() {
		// TODO Auto-generated method stub

		String pattern = "dd/MM/yyyy";
		java.util.TimeZone tz = java.util.TimeZone.getTimeZone("IST");
		Calendar c = Calendar.getInstance(tz);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		simpleDateFormat.setTimeZone(tz);
		String currentDateIs = simpleDateFormat.format(c.getTime());
		// logger.info(" currentDateIs :"+currentDateIs);

		return currentDateIs;
	}

	private String getNextDateInStringFormate() {

		String pattern = "dd/MM/yyyy";
		java.util.TimeZone tz = java.util.TimeZone.getTimeZone("IST");
		Calendar c = Calendar.getInstance(tz);

		c.setTime(new Date()); // Now use today date.
		c.add(Calendar.DATE, 1);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		simpleDateFormat.setTimeZone(tz);
		String nextDateIs = simpleDateFormat.format(c.getTime());
		logger.info(" nextDateIs :" + nextDateIs);

		return nextDateIs;

	}

	@Override
	public long getVehicleRecivedToday(String fkeyString) {

		CallInteraction interaction = source.callinteractions(em)
				.where(u -> u.getServiceAdvisorfirebaseKey().equals(fkeyString)).getOnlyValue();
		long intId = interaction.getId();

		long countSB = source.callinteractions(em)
				.where(u -> u.getId() == intId
						&& u.getServiceBooked().getServiceScheduledDateStr().equals(getCurrentDateInStringFormate()))
				.count();

		return countSB;
	}

	@Override
	public Map<String, Long> getCountOfServicesBookedtoday(long advisorId) {

		List<ServiceBooked> servicesbooked = source.serviceBooked(em)
				.where(u -> u.getServiceAdvisor().getAdvisorId() == advisorId).toList();

		// List<ServiceBooked> servicesbooked1=servicesbooked.stream()
		// .filter(u ->
		// u.getServiceScheduledDateStr().equals(getCurrentDateInStringFormate()))
		// .collect(Collectors.toList());

		String dispo = getDispositionValueById(3);

		Map<String, Long> countOfServiceType = servicesbooked.stream()
				.filter(u -> u.getServiceBookStatus().getDisposition().equals(dispo))
				.filter(u -> u.getServiceScheduledDateStr().equals(getCurrentDateInStringFormate())
						|| u.getServiceScheduledDateStr().equals(getNextDateInStringFormate()))
				.collect(Collectors.groupingBy(ServiceBooked::getServiceBookedType, Collectors.counting()));

		logger.info("servicesbooked count : " + servicesbooked.size());

		// logger.info("servicesbooked1 count today: "+servicesbooked1.size());

		for (Map.Entry<String, Long> entry : countOfServiceType.entrySet()) {
			logger.info("Key : " + entry.getKey() + " Value : " + entry.getValue());
		}

		// TODO Auto-generated method stub
		return countOfServiceType;
	}

	@Override
	public List<PSFAssignedInteraction> getAllUnsynchedPSFMobileCalls(String dealer) {

		List<PSFAssignedInteraction> notassignList = new ArrayList<PSFAssignedInteraction>();

		String query = "USE " + dealer + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) {
				Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution = smt.execute(query);

					Dealer dealerData = source.dealers(em).where(u -> u.getDealerCode().equals(dealer)).getOnlyValue();

					if (dealerData.getOEM() != null && dealerData.getOEM().equals("MARUTHI")) {

						List<PSFAssignedInteraction> allassignList = source.psfAssignedInteraction(em)
								.where(u -> u.getCampaign() != null).where(u -> u.getCampaign().getId() == 7)
								.where(u -> u.getCallMade().equals("No")).toList();

						for (PSFAssignedInteraction sa : allassignList) {

							if (sa.isDisplayFlag()) {

								Hibernate.initialize(sa.getCustomer());
								Hibernate.initialize(sa.getCustomer().getAddresses());
								Hibernate.initialize(sa.getCustomer().getPhones());
								Hibernate.initialize(sa.getCustomer().getAddresses());
								Hibernate.initialize(sa.getVehicle());

								notassignList.add(sa);

							}

						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					logger.info("SQL Exception");
				}

			}
		});

		return notassignList;

	}

	@Override
	public void updateSynchedToFirebasePSFCall(long id, String referencekey) {

		PSFAssignedInteraction updateAssigned = em.find(PSFAssignedInteraction.class, id);
		updateAssigned.setDisplayFlag(true);
		updateAssigned.setCallMade("Yes");
		em.merge(updateAssigned);

	}

	@Override
	public void addServiceAdvisorPSFHistorySync(String firebaseKey, ServiceAdvisorHistoryInfo serviceAdvi_History) {
		// TODO Auto-generated method stub
		logger.info("firebaseKey from addServiceAdvisorHistory : " + firebaseKey);

		String dealer = serviceAdvi_History.getDealerCode();

		String query = "USE " + dealer + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) {
				Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution = smt.execute(query);

					CallInteraction interaction = new CallInteraction();

					String date = serviceAdvi_History.getCallDate();
					String time = serviceAdvi_History.getCallTime();

					String dateTimeStr = date + " " + time;
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					logger.info("Call time time: " + dateTimeStr);
					logger.info("call Date :" + date + "Call Time :" + time);

					try {
						Date datetime = format.parse(dateTimeStr);
						interaction.setCallMadeDateAndTime(datetime);
					} catch (ParseException e) {
						logger.info("Could not parse date");
						e.printStackTrace();
					}

					String base64String = serviceAdvi_History.getMediaFile();
					serviceAdvi_History.setMediaFile("");

					byte[] lobvalue = null;

					if (base64String != null) {
						if (base64String.length() > 0) {
							lobvalue = DatatypeConverter.parseBase64Binary(base64String);
						}
					}

					if (serviceAdvi_History.getFilePath() != null) {
						interaction.setFilePath(serviceAdvi_History.getFilePath());
					}
					if (serviceAdvi_History.getLatitude() != null) {
						interaction.setLatitude(serviceAdvi_History.getLatitude());
					}
					if (serviceAdvi_History.getLongitude() != null) {
						interaction.setLongitude(serviceAdvi_History.getLongitude());
					}
					interaction.setMediaFileLob(lobvalue);
					interaction.setCallDate(date);
					interaction.setCallTime(time);
					interaction.setFirebaseKey(firebaseKey);
					interaction.setCallDuration(serviceAdvi_History.getCallDuration());
					interaction.setRingTime(serviceAdvi_History.getRingTime());
					interaction.setCallType(serviceAdvi_History.getCallType());
					String statusIs = serviceAdvi_History.getStatus();

					WyzUser user = em.find(WyzUser.class, Long.parseLong(serviceAdvi_History.getUserId()));
					Vehicle vehicle = em.find(Vehicle.class, Long.parseLong(serviceAdvi_History.getVehicleId()));
					Customer customer = em.find(Customer.class, Long.parseLong(serviceAdvi_History.getCustId()));
					PSFAssignedInteraction assignId = em.find(PSFAssignedInteraction.class,
							Long.parseLong(serviceAdvi_History.getAssignedId()));

					/*
					 * ServiceAdvisorInteractionStatus statusOfpsf =
					 * source.serviceAdvisorInteractionStatus(em) .where(u ->
					 * u.getInteractionStatus().equals(statusIs)).getOnlyValue()
					 * ;
					 */

					CallDispositionData statusOfpsf = source.callDispositionDatas(em)
							.where(u -> u.getDisposition().equals(statusIs)).getOnlyValue();

					interaction.setWyzUser(user);
					interaction.setVehicle(vehicle);
					interaction.setCustomer(customer);
					interaction.setPsfAssignedInteraction(assignId);
					em.persist(interaction);

					PSFInteraction psfData = new PSFInteraction();

					psfData.setCallInteraction(interaction);

					if (serviceAdvi_History.getFollowUpTime() != null) {
						psfData.setPsfFollowUpTime(serviceAdvi_History.getFollowUpTime());
					}
					if (serviceAdvi_History.getIsFollowUpDone() != null) {
						psfData.setIsFollowUpDone(serviceAdvi_History.getIsFollowUpDone());
					}
					if (serviceAdvi_History.getIsFollowupRequired() != null) {
						psfData.setIsFollowupRequired(serviceAdvi_History.getIsFollowupRequired());
					}

					if (serviceAdvi_History.getAfterServiceComments() != null) {
						psfData.setAfterServiceComments(serviceAdvi_History.getAfterServiceComments());
					}
					if (serviceAdvi_History.getAfterServiceSatisfication() != null) {
						psfData.setAfterServiceSatisfication(serviceAdvi_History.getAfterServiceSatisfication());
					}
					if (serviceAdvi_History.getRecentServiceComments() != null) {
						psfData.setRecentServiceComments(serviceAdvi_History.getRecentServiceComments());
					}
					if (serviceAdvi_History.getRecentServiceSatisfication() != null) {
						psfData.setRecentServiceSatisfication(serviceAdvi_History.getRecentServiceSatisfication());
					}

					if (serviceAdvi_History.getFollowUpDate() != null) {

						String followupdate = serviceAdvi_History.getFollowUpDate();

						SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");

						try {
							Date datetime1 = format1.parse(followupdate);
							psfData.setPsfFollowUpDate(datetime1);
						} catch (ParseException e) {
							logger.info("Could not parse date");
							e.printStackTrace();
						}
					}
					em.persist(psfData);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					logger.info("SQL  EXception");
				}

			}
		});

	}

	@Override
	public List<CallInteraction> getAllNotSyncedAppointment(String dealer) {
		// TODO Auto-generated method stub

		List<CallInteraction> appointmentBookedToday = new ArrayList<CallInteraction>();

		String query = "USE " + dealer + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) {
				Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution = smt.execute(query);

					String dispo = getDispositionValueById(25);

					List<CallInteraction> appointment_callinteraction = source.callinteractions(em)
							.where(u -> u.getInsuranceDisposition().getCallDispositionData().getDisposition()
									.equals(dispo) && u.getSynchedToFirebase().equals("false"))
							.where(u -> u.getAppointmentBooked().getInsuranceBookStatus().getDisposition()
									.equals(dispo))
							.toList();

					logger.info("serviceBookedToday count :" + appointmentBookedToday.size());
					for (CallInteraction ser_data : appointment_callinteraction) {

						if (ser_data.getAppointmentBooked().getAppointmentDateStr()
								.equals(getCurrentDateInStringFormate())
								|| ser_data.getAppointmentBooked().getAppointmentDateStr()
										.equals(getNextDateInStringFormate())) {

							appointmentBookedToday.add(ser_data);
							Hibernate.initialize(ser_data.getCustomer().getPreferredPhone());
							Hibernate.initialize(ser_data.getCustomer().getPreferredAdress());
							Hibernate.initialize(ser_data.getCustomer().getSegment());
						}

					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					logger.info("SQL Exception");
				}

			}
		});

		return appointmentBookedToday;
	}

	@Override
	public AppointmentBooked getAppointmentById(long appointmentId) {
		// TODO Auto-generated method stub
		return source.appointmentsBooked(em).where(u -> u.getAppointmentId() == appointmentId).getOnlyValue();
	}

	@Override
	public Insurance getLatestInsuranceOfVehicle(long vehicle_id) {
		// TODO Auto-generated method stub

		List<Insurance> insurance_list = source.insurances(em).where(u -> u.getVehicle().getVehicle_id() == vehicle_id)
				.where(u -> u.getPolicyDueDate() != null).toList();
		Insurance lastInsurance = new Insurance();
		if (insurance_list.size() > 0) {

			lastInsurance = Collections.max(insurance_list, Comparator.comparing(c -> c.policyDueDate));
			return lastInsurance;
		} else {
			return lastInsurance;
		}
	}

	@Override
	public List<InsuranceAgent> getAllActiveInsuranceAgent(String dealer) {
		// TODO Auto-generated method stub
		List<InsuranceAgent> activeList = new ArrayList<InsuranceAgent>();

		String query = "USE " + dealer + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) {
				Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution = smt.execute(query);

					List<InsuranceAgent> allAgents = source.insuranceAgents(em).toList();

					for (InsuranceAgent agents : allAgents) {

						if (agents.isActive) {

							activeList.add(agents);
						}

					}

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					logger.info("SQL Exception");
				}

			}
		});
		return activeList;

	}

	@Override
	public void addInsuranceAgentCallHistory(String firebaseKey, ServiceAdvisorHistoryInfo insuranceHistory,
			DataSnapshot dataSnapshot) {
		// TODO Auto-generated method stub

		String dealer = insuranceHistory.getDealerCode();

		String query = "USE " + dealer + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) {
				Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution = smt.execute(query);

					logger.info("firebaseKey from addInsuranceHistory : " + firebaseKey);

					CallInteraction interaction = new CallInteraction();

					String date = insuranceHistory.getCallDate();
					String time = insuranceHistory.getCallTime();

					String dateTimeStr = date + " " + time;
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

					SimpleDateFormat formatSch = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
					logger.info("Call time time: " + dateTimeStr);
					logger.info("call Date :" + date + "Call Time :" + time);

					try {
						Date datetime = format.parse(dateTimeStr);
						interaction.setCallMadeDateAndTime(datetime);
					} catch (ParseException e) {
						logger.info("Could not parse date");
						e.printStackTrace();
					}

					String base64String = insuranceHistory.getMediaFile();
					insuranceHistory.setMediaFile("");

					byte[] lobvalue = null;

					if (base64String != null) {
						if (base64String.length() > 0) {
							lobvalue = DatatypeConverter.parseBase64Binary(base64String);
						}
					}

					if (insuranceHistory.getFilePath() != null) {
						interaction.setFilePath(insuranceHistory.getFilePath());
					}
					if (insuranceHistory.getLatitude() != null) {
						interaction.setLatitude(insuranceHistory.getLatitude());
					}
					if (insuranceHistory.getLongitude() != null) {
						interaction.setLongitude(insuranceHistory.getLongitude());
					}
					interaction.setMediaFileLob(lobvalue);
					interaction.setCallDate(date);
					interaction.setCallTime(time);
					interaction.setFirebaseKey(firebaseKey);
					interaction.setCallDuration(insuranceHistory.getCallDuration());
					interaction.setRingTime(insuranceHistory.getRingTime());
					interaction.setCallType(insuranceHistory.getCallType());
					interaction.setMakeCallFrom(insuranceHistory.getMakeCallFrom());
					interaction.setDealerCode(insuranceHistory.getDealerCode());
					interaction.setDailedNoIs(insuranceHistory.getCustomerPhone());

					WyzUser user = em.find(WyzUser.class, Long.parseLong(insuranceHistory.getUserId()));
					Vehicle vehicle = em.find(Vehicle.class, Long.parseLong(insuranceHistory.getVehicleId()));
					Customer customer = em.find(Customer.class, Long.parseLong(insuranceHistory.getCustId()));

					AppointmentBooked appointbooked = em.find(AppointmentBooked.class,
							Long.parseLong(insuranceHistory.getServiceBookedId()));

					CallDispositionData serviceBookStatus = getStatusByInteractionKeyIdStatus(
							Integer.parseInt(insuranceHistory.getStatus()));

					if (appointbooked.getCallInteraction() != null) {

						int maxServ = appointbooked.getCallInteraction().size();
						if (maxServ > 0) {

							InsuranceAssignedInteraction assigndList = appointbooked.getCallInteraction().get(maxServ - 1)
									.getInsuranceAssignedInteraction();

							assigndList.setLastDisposition(assigndList.getFinalDisposition().getDisposition());
							assigndList.setFinalDisposition(serviceBookStatus);
							em.merge(assigndList);

							interaction.setServiceAdvisorfirebaseKey(
									appointbooked.getCallInteraction().get(maxServ - 1).getServiceAdvisorfirebaseKey());
							interaction.setInsuranceAssignedInteraction(
									appointbooked.getCallInteraction().get(maxServ - 1).getInsuranceAssignedInteraction());
							interaction.setCampaign(appointbooked.getCallInteraction().get(maxServ - 1).getCampaign());
						}
					}

					interaction.setWyzUser(user);
					interaction.setVehicle(vehicle);
					interaction.setCustomer(customer);

					InsuranceDisposition disposition = new InsuranceDisposition();
					disposition.setCallDispositionData(serviceBookStatus);

					if (insuranceHistory.getComments() != null) {
						disposition.setComments(insuranceHistory.getComments());
					}

					if (insuranceHistory.getDealerName() != null) {
						disposition.setDealerName(insuranceHistory.getDealerName());
					}

					if (insuranceHistory.getCity() != null) {
						disposition.setCityName(insuranceHistory.getCity());
					}
					
					if (insuranceHistory.getCity() != null) {
						disposition.setCityName(insuranceHistory.getCity());
					}
					
					if (insuranceHistory.getPaymentReference() != null) {
						disposition.setPaymentReference(insuranceHistory.getPaymentReference());
					}
					
					if (insuranceHistory.getPaymentType() != null) {
						disposition.setPaymentType(insuranceHistory.getPaymentType());
					}

					if (insuranceHistory.getStatus().equals("25")) {

						logger.info("Appointment reschedule");

						AppointmentBooked newAppointment = new AppointmentBooked();
						
						newAppointment.setInsuranceAgent(appointbooked.getInsuranceAgent());
						newAppointment.setCustomer(appointbooked.getCustomer());
						newAppointment.setAppointmentFromTime(insuranceHistory.getAppointmentTime());
						newAppointment.setAddressOfVisit(appointbooked.getAddressOfVisit());
						newAppointment.setRenewalMode(appointbooked.getRenewalMode());
						newAppointment.setRenewalType(appointbooked.getRenewalType());
						newAppointment.setInsuranceBookStatus(serviceBookStatus);
						newAppointment.setTypeOfPickup(appointbooked.getTypeOfPickup());
						newAppointment.setInsuranceAgentData(appointbooked.getInsuranceAgentData());
						newAppointment.setInsuranceCompany(appointbooked.getInsuranceCompany());
						
						String callDate = insuranceHistory.getFollowUpDate();
						String callTime = insuranceHistory.getFollowUpTime();

						String schdateTimeStr = callDate + " " + callTime;
						SimpleDateFormat formating = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						try {
							Date datetime = formating.parse(schdateTimeStr);
							logger.info("datetime :" + datetime);
							newAppointment.setAppointmentDate(datetime);
						} catch (ParseException e) {
							logger.info("Could not parse date");
							e.printStackTrace();
						}

						interaction.setSynchedToFirebase("true");
						interaction.setAppointmentBooked(newAppointment);

						int dispoId = 35;

						appointbooked.setInsuranceBookStatus(getStatusByInteractionKeyIdStatus(dispoId));
						em.merge(appointbooked);
						em.flush();
						
						interaction.setAppointmentBooked(newAppointment);

					} else {

						interaction.setAppointmentBooked(appointbooked);

					}

					// }
					em.persist(interaction);

					disposition.setCallInteraction(interaction);
					em.persist(disposition);
					lobvalue = null;
					dataSnapshot.getRef().removeValue();

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					logger.info("SQL Exception");
				}
			}
		});

	}

	@Override
	public void startEventsOfCRM(String dealer) {
		// TODO Auto-generated method stub
		
		String query = "USE " + dealer + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) {
				Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution = smt.execute(query);
					
					StoredProcedureQuery sQuery = em.createStoredProcedureQuery("calldurationFlagUpdator");
					logger.info("calldurationFlagUpdator : "+sQuery.executeUpdate());
					
					StoredProcedureQuery sQuery1 = em.createStoredProcedureQuery("averageKmupdate");
					logger.info("averageKmupdate : "+sQuery1.executeUpdate());
					
					StoredProcedureQuery sQuery2 = em.createStoredProcedureQuery("InsurancecalldurationFlagUpdator");
					logger.info("InsurancecalldurationFlagUpdator : "+sQuery2.executeUpdate());




				} catch (SQLException e) {
					// TODO Auto-generated catch block
					logger.info("SQL Exception");
				}

			}
		});
		
	}

}
