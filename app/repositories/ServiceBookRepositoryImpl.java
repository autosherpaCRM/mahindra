/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.math.BigInteger;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import configs.JinqSource;
import controllers.webmodels.ServiceBookedResultList;
import models.AssignedInteraction;
import models.BookingDateTime;
import models.CallDispositionData;
import models.CallInteraction;
import models.Driver;
import models.PickupDrop;
import models.Role;
import models.SRDisposition;
import models.ServiceAdvisor;
import models.ServiceBooked;
import models.ServiceTypes;
import models.Workshop;
import models.WyzUser;
import play.Logger.ALogger;

/**
 *
 * @author W-885
 */
@Repository("ServiceBookRepository")
@Transactional
public class ServiceBookRepositoryImpl implements ServiceBookRepository {

	ALogger logger = play.Logger.of("application");

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Autowired
	private ServiceBookRepository serviceBookedRepo;

	@Override
	public List<WyzUser> getServiceAdvisorsByWorkshop(long workshopid) {		
		
		Role role=source.roles(em).where(u -> u.getRole().equals("Service Advisor")).getOnlyValue();
		Hibernate.initialize(role.getUsers());
		
		//Role role=source.roles(em).where(u -> u.getRole().equals("Driver")).getOnlyValue();
		
		//Role role=source.roles(em).where(u -> u.getRole().equals("CRE")).getOnlyValue();
		
		List<WyzUser> userslist=getUserListByWorkshopId(workshopid);
		
		
		return userslist.stream().filter(u -> u.getRoles().contains(role)).collect(Collectors.toList());
	}

	private List<WyzUser> getUserListByWorkshopId(long workshopid) {
		
		Workshop workshop=em.find(Workshop.class, workshopid);
		
		//Hibernate.initialize(workshop.getUserWorkshop());
		
		List<WyzUser> userslist=workshop.getUserWorkshop();
		
		return userslist;
	}
	
	
	
	@Override
	public List<WyzUser> getUserByWorkshopAndRole(long workshopid, String roles) {	
	
		Role role=source.roles(em).where(u -> u.getRole().equals(roles)).getOnlyValue();
		//Hibernate.initialize(role.getUsers());
		//Role role=source.roles(em).where(u -> u.getRole().equals("Driver")).getOnlyValue();	
		//Role role=source.roles(em).where(u -> u.getRole().equals("CRE")).getOnlyValue();
		List<WyzUser> userslist=getUserListByWorkshopId(workshopid);	
		return userslist.stream().filter(u -> u.getRoles().contains(role)).collect(Collectors.toList());
	}

	

	@Override
	public long getAllServiceBookedDataCount(String userLoginName, String searchBookingStatus, String searchDateFrom,
			String searchDateTo, String searchCRE, String searchDriver, String searchSA, String searchWorkshop,
			String searchPattern) {
		
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("AllServiceBookedRecordsCount");

		sQuery.registerStoredProcedureParameter("userLoginName", String.class, ParameterMode.IN);
		sQuery.setParameter("userLoginName", userLoginName);
		
		sQuery.registerStoredProcedureParameter("searchBookingStatus", String.class, ParameterMode.IN);
		sQuery.setParameter("searchBookingStatus", searchBookingStatus);

		sQuery.registerStoredProcedureParameter("searchDateFrom", String.class, ParameterMode.IN);
		sQuery.setParameter("searchDateFrom", searchDateFrom);
		
		sQuery.registerStoredProcedureParameter("searchDateTo", String.class, ParameterMode.IN);
		sQuery.setParameter("searchDateTo", searchDateTo);

		sQuery.registerStoredProcedureParameter("searchCRE", String.class, ParameterMode.IN);
		sQuery.setParameter("searchCRE", searchCRE);
		
		sQuery.registerStoredProcedureParameter("searchDriver", String.class, ParameterMode.IN);
		sQuery.setParameter("searchDriver", searchDriver);
		
		sQuery.registerStoredProcedureParameter("searchSA", String.class, ParameterMode.IN);
		sQuery.setParameter("searchSA", searchSA);
		
		sQuery.registerStoredProcedureParameter("searchWorkshop", String.class, ParameterMode.IN);
		sQuery.setParameter("searchWorkshop", searchWorkshop);

		sQuery.registerStoredProcedureParameter("searchPattern", String.class,ParameterMode.IN);
		sQuery.setParameter("searchPattern",searchPattern);
		
		
		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();
		return bigCount.longValue();

	}

	@Override
	public List<ServiceBookedResultList> AllServiceBookedRecords(String userLoginName, String searchBookingStatus,
			String searchDateFrom, String searchDateTo, String searchCRE, String searchDriver, String searchSA,
			String searchWorkshop, String searchPattern, long fromIndex, long toIndex) {
		

		javax.persistence.Query query = em.createNativeQuery(
				"CALL AllServiceBookedRecords(:userLoginName,:searchBookingStatus, :searchDateFrom, :searchDateTo, :searchCRE, :searchDriver, :searchSA, :searchWorkshop, :searchPattern, :fromIndex , :toIndex)", "ServiceBookedResultList");

		query.setParameter("userLoginName",userLoginName);
		query.setParameter("searchBookingStatus", searchBookingStatus);
		query.setParameter("searchDateFrom", searchDateFrom);
		query.setParameter("searchDateTo", searchDateTo);
		query.setParameter("searchCRE", searchCRE);
		query.setParameter("searchDriver", searchDriver);

		query.setParameter("searchSA" , searchSA);
		query.setParameter("searchWorkshop",searchWorkshop);
		query.setParameter("searchPattern",searchPattern);
		query.setParameter("fromIndex",fromIndex);
		query.setParameter("toIndex", toIndex);


		return query.getResultList();

		
	}

	@Override
	public long getSBCount(String userLoginName) {
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("AllServiceBookedCounts");

		
		sQuery.registerStoredProcedureParameter("userLoginName", String.class, ParameterMode.IN);
		sQuery.setParameter("userLoginName", userLoginName);


		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
		
	}



	
	
	
	
	
	
	
	//	CANCEL BOOKING
	@Override
	public String cancelPickupOfBooking(long servicebookedId) {
		
		String status="";
		ServiceBooked servicebook=em.find(ServiceBooked.class, servicebookedId);
		Hibernate.initialize(servicebook.getPickupDrop());
		
		if(servicebook.getPickupDrop()!=null){
			
			PickupDrop pickup=servicebook.getPickupDrop();
			servicebook.setPickupDrop(null);
			em.remove(pickup);
			status="Pick up Cancelled!";			
		}else{			
			status="Pick up Not present";
		}
		
		
		
		return status;
	}

	@Override
	public String cancelBooking(long servicebookedId) {
		// TODO Auto-generated method stub
		
		String status=cancelPickupOfBooking(servicebookedId);
		ServiceBooked servicebook=em.find(ServiceBooked.class, servicebookedId);
		
			
		long cancelledBookDispo=source.callDispositionDatas(em).
				where(u -> u.getDispositionId()== 35).count();
		
		if(cancelledBookDispo>0){
			
			long callId=35;			
			CallDispositionData dispoCancell=em.find(CallDispositionData.class, callId);
			servicebook.setServiceBookStatus(dispoCancell);
			em.merge(servicebook);
			
			int maxServ=servicebook.getCallInteraction().size();
			if(maxServ>0){
				if(servicebook.getCallInteraction().get(maxServ -1).getServiceAdvisorfirebaseKey()!=null){
				String firebasekey=servicebook.getCallInteraction().get(maxServ -1).getServiceAdvisorfirebaseKey(); 
				
				String firebaseUrl=play.Play.application().configuration().getString("app.myfirebaseurl");
				String fireUrl = firebaseUrl;
				fireUrl += servicebook.getServiceAdvisor().getWyzUser().getDealerCode();
				fireUrl += "/ServiceAgent/";
				fireUrl += servicebook.getServiceAdvisor().getAdvisorName();
				fireUrl += "/";
				fireUrl += "BookedDetails";
				fireUrl += "/";
				fireUrl += "CallInfo";
				fireUrl += "/";
				fireUrl += firebasekey;
				logger.info("serviceAdvisors firebaseUrl to delete: " + fireUrl);
				DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(fireUrl);
				ref.removeValue();
				}
				
				
				}
			
			
			
			
			return "Service Booking Cancelled!";
			
		}else{
			
			return "Service Booking Disposition doesnt Exist!";
		}
		
		
	}

	@Override
	public ServiceBooked getServiceBookDataByID(long servicebookedId) {
		// TODO Auto-generated method stub
		
		return em.find(ServiceBooked.class, servicebookedId);
	}

	@Override
	public SRDisposition getSRDispositionByCallId(long call_id) {
		// TODO Auto-generated method stub
		return source.srDisposition(em).where(u -> u.getCallInteraction()!=null)
				.where(u -> u.getCallInteraction().getId()== call_id).getOnlyValue();
	}

	@Override
	public ServiceBooked getServiceBookByCallId(long call_id) {
		// TODO Auto-generated method stub
		/*return source.serviceBooked(em).where(u -> u.getCallInteraction()!=null)
				.where(u -> u.getCallInteraction().getId()== call_id).getOnlyValue();*/
		
		return source.callinteractions(em).where(u -> u.getId() == call_id).select(u -> u.getServiceBooked()).getOnlyValue();
	}

	@Override
	public boolean resheduleBooking(CallInteraction call_int, SRDisposition srdispo, ServiceBooked servicebooked,String serviceAdvisorId,
			String workshopId, String typeOfPickup,String driver_id, String startTimeId, String endTimeId,WyzUser userData) {
		
		long serviceAdvId=Long.parseLong(serviceAdvisorId);
		long workId=Long.parseLong(workshopId);
		
		ServiceAdvisor service_advisor = em.find(ServiceAdvisor.class, serviceAdvId);
		Workshop work_shop = em.find(Workshop.class, workId);
		
		servicebooked.setPickupDrop(null);
		
		if (typeOfPickup.equals("true") || 
				typeOfPickup.equals("Maruthi Mobile Support")||
				typeOfPickup.equals("Road Side Assistance")||
				typeOfPickup.equals("Door Step Service")) {
			
			long wyzuser_id=Long.parseLong(driver_id);
			Driver driver=source.driver(em).where(u -> u.getWyzUser().getId() == wyzuser_id).getOnlyValue();
			
			PickupDrop pick_up=new PickupDrop();
			BookingDateTime bookingFrom=getBookingTimeById(Long.parseLong(startTimeId));
			BookingDateTime bookingTo=getBookingTimeById(Long.parseLong(endTimeId));			
			pick_up.setTimeFrom(bookingFrom.getStartTime());
			
			long ONE_MINUTE_IN_MILLIS=60000;
			long initailMin=bookingTo.getStartTime().getTime();
			long timeslotdifference=30;
			long afterAddingMins=initailMin + (timeslotdifference * ONE_MINUTE_IN_MILLIS);
			Time time=new Time(afterAddingMins);			
			pick_up.setTimeTo(time);
			pick_up.setPickupDate(servicebooked.getScheduledDateTime());

			servicebooked.setIsPickupRequired(true);
			servicebooked.setPickupDrop(pick_up);
			servicebooked.setDriver(driver);
			// pick_up.setServiceBooked(service_Booked);
			pick_up.setDriver(driver);
			pick_up.setPickUpAddress(servicebooked.getServiceBookingAddress());

			em.persist(pick_up);
			servicebooked.setPickupDrop(pick_up);

		}else{
			servicebooked.setDriver(null);
			servicebooked.setPickupDrop(null);
		}
		
		long serviceTypeId=Long.parseLong(servicebooked.getServiceBookedType());		
		ServiceTypes servicetypes=source.serviceTypes(em).where(u -> u.getId() == serviceTypeId).getOnlyValue();
		CallDispositionData calldispo=source.callDispositionDatas(em).where(u -> u.getDispositionId() == 3).getOnlyValue();
		servicebooked.setServiceBookedId(0);
		servicebooked.setServiceAdvisor(service_advisor);
		servicebooked.setWorkshop(work_shop);			
		servicebooked.setServiceBookType(servicetypes);
		servicebooked.setServiceBookedType(servicetypes.getServiceTypeName());
		servicebooked.setTypeOfPickup(typeOfPickup);
		servicebooked.setServiceBookStatus(calldispo);
		servicebooked.setWyzUser(userData);
		em.persist(servicebooked);
		
		AssignedInteraction assigndList=call_int.getAssignedInteraction();
		
		assigndList.setLastDisposition(assigndList.getFinalDisposition().getDisposition());
		assigndList.setFinalDisposition(calldispo);
		em.merge(assigndList);
		
		call_int.setId(0);		
		call_int.setSynchedToFirebase("false");
		call_int.setServiceBooked(servicebooked);
		call_int.setWyzUser(userData);
		call_int.setServiceAdvisorfirebaseKey(null);		
		em.persist(call_int);
		
		srdispo.setId(0);
		if(srdispo.getUpsellLead()!=null){
			//Hibernate.initialize(srdispo.getUpsellLead());
			srdispo.setUpsellLead(srdispo.getUpsellLead());
		}else{
			srdispo.setUpsellLead(null);
		}
		srdispo.setCallInteraction(call_int);
		em.persist(srdispo);		
		return false;
	}

	private BookingDateTime getBookingTimeById(long bookId) {
		// TODO Auto-generated method stub
		return source.bookingDateTime(em).where(u -> u.getId() == bookId).getOnlyValue();
	}

	@Override
	public void updateOldCallIntToReshedule(long serviceBookedId) {
		String status=cancelBooking(serviceBookedId) ;
		ServiceBooked servicebook=em.find(ServiceBooked.class, serviceBookedId);
		servicebook.setStatusOfSB("Booking Resceduled");
		em.merge(servicebook);
		
		/*Hibernate.initialize(servicebook.getCallInteraction().getSrdisposition());
		
		SRDisposition srdispo=servicebook.getCallInteraction().getSrdisposition();
		
		srdispo.setRemarks(srdispo.getRemarks()+" Booking Resceduled");
		em.merge(srdispo);*/
		
		
		
	}

	@Override
	public List<BookingDateTime> getTimeSlot() {
		// TODO Auto-generated method stub
		return source.bookingDateTime(em).toList();
	}

	@Override
	public List<List<Date>> getDriverScheduleByWyzUser(long id, String scheduleDate){
				
		long driver_id=source.driver(em).where(u -> u.getWyzUser().getId() == id).select(u -> u.getId()).getOnlyValue();
		
		List<PickupDrop> pickupListOf=source.pickupDrop(em).where(u -> u.getDriver().getId() == driver_id)
					.toList();
				
		List<Date> pickupListOfTodayFrom=pickupListOf.stream().filter(u -> u.getPickupDateString().equals(scheduleDate))			
				.map(PickupDrop ::getTimeFrom)
				.collect(Collectors.toList());
		
		List<Date> pickupListOfTodayTo=pickupListOf.stream().filter(u -> u.getPickupDateString().equals(scheduleDate))			
				.map(PickupDrop ::getTimeTo)
				.collect(Collectors.toList());
		
		List<List<Date>> pickupListOfToday=new ArrayList<List<Date>>();
		pickupListOfToday.add(pickupListOfTodayFrom);
		pickupListOfToday.add(pickupListOfTodayTo);
		
		logger.info("pickupListOfToday : "+pickupListOfToday.size()+" driver_id : "+driver_id);
		return pickupListOfToday;
		
	}

	@Override
	public long getServiceAdvisorByuserID(long id) {
		// TODO Auto-generated method stub	
		
		return source.serviceadvisor(em).where(u -> u.getWyzUser().getId() == id).select(u -> u.getWyzUser().getId()).getOnlyValue();
	}

	@Override
	public long getDriverByUserId(long id) {
		// TODO Auto-generated method stub
		return source.driver(em).where(u -> u.getWyzUser().getId() ==id).select(u -> u.getWyzUser().getId()).getOnlyValue();
	}

	@Override
	public void confirmStatusOfBooking(long servicebookedId) {
		// TODO Auto-generated method stub
		
		CallDispositionData calldispo=source.callDispositionDatas(em).where(u -> u.getDispositionId() == 41).getOnlyValue();		
		ServiceBooked servicebook=em.find(ServiceBooked.class, servicebookedId);
		servicebook.setServiceBookStatus(calldispo);
		em.merge(servicebook);		
	}
	
	
	



}