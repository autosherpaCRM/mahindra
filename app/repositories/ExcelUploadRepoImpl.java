package repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.hibernate.Hibernate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import configs.JinqSource;
import models.Address;
import models.AssignedInteraction;
import models.CallInteraction;
import models.Campaign;
import models.CampaignExcelError;
import models.Customer;
import models.Email;
import models.InsuranceExcelData;
import models.InsuranceExcelError;
import models.JobCardStatusError;
import models.Location;
import models.Outlet;
import models.PSFAssignedInteraction;
import models.Phone;
import models.SMRDataError;
import models.SRDisposition;
import models.SaleRegisterExcelError;
import models.Segment;
import models.Service;
import models.UploadData;
import models.Vehicle;
import models.WorkBillRegisterError;
import models.Workshop;
import models.WyzUser;
import play.Logger;
import play.db.DB;
import utils.CampaignExcel;
import utils.InsuranceExcel;
import utils.JobCardStatus;
import utils.SMRData;
import utils.SaleRegisterExcel;
import utils.WorkBillRegister;

@Repository("excelUploadRepo")
@Transactional
public class ExcelUploadRepoImpl implements ExcelUploadRepo {

	Logger.ALogger logger = play.Logger.of("application");
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	private java.sql.Date currentDate() {

		return new java.sql.Date(System.currentTimeMillis());
	}

	@Override
	public void insertExcelDataToDB(Object excelData, HashMap<String, String> formData,
			HashMap<Date, Integer> uploadSuccessRecord, HashMap<Date, Integer> uploadRejectedRecord) {

		String dataType = formData.get("dataType");
		String upload_id = formData.get("uploadId");
		String campaignStartDate = formData.get("campaignStartdate");
		String campaignExpiry = formData.get("campaignExpiry");

		logger.info("inside insert excen data:+" + campaignStartDate);

		logger.info("upload id" + upload_id);
		switch (dataType) {
		case "sale_register":
			insertSalesData(excelData, upload_id, uploadSuccessRecord, uploadRejectedRecord);
			break;
		case "job_card":
			insertJobCardData(excelData, upload_id, formData, uploadSuccessRecord, uploadRejectedRecord);
			break;
		case "bill":
			insertWorkBillData(excelData, upload_id, campaignExpiry, uploadSuccessRecord, uploadRejectedRecord);
			break;
		case "SMR":
			insertSMRData(excelData, upload_id, uploadSuccessRecord, uploadRejectedRecord);
			break;
		case "campaign":
			insertCampaignData(excelData, upload_id, campaignExpiry, uploadSuccessRecord, uploadRejectedRecord);
			break;
		case "Insurance":
			insertInsuranceData(excelData, upload_id, campaignExpiry, uploadSuccessRecord, uploadRejectedRecord);
			break;
		case "InsuranceHistory":
			insertInsuranceDataHistory(excelData, upload_id,uploadSuccessRecord, uploadRejectedRecord);
			break;
			
		}

	}


	private void insertSalesData(Object excelData, String upload_id, HashMap<Date, Integer> uploadSuccessRecord,
			HashMap<Date, Integer> uploadRejectedRecord) {

		SaleRegisterExcel saleRegister = (SaleRegisterExcel) excelData;

		logger.info("row number data : " + saleRegister.getRowNumber());
		logger.info("is Error data : " + saleRegister.isError());

		if (saleRegister.getInvDate() == null) {

			try {

				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				java.sql.Date sqlDate = new java.sql.Date(df.parse("1947-08-15").getTime());
				logger.info("date dateNew for foll : " + sqlDate);
				saleRegister.setInvDate(sqlDate);
			} catch (Exception ex) {

				logger.info("date exp : " + ex);

			}
		}

		if (!saleRegister.isError()) {

			String locCodeInfo = saleRegister.getLocCode();
			logger.info("second method");

			Customer cust_data = new Customer();
			cust_data.setCustomerEmail(saleRegister.getEmailId());
			cust_data.setCustomerName(saleRegister.getCustomerName());
			cust_data.setCustomerId(saleRegister.getCustomerId());
			cust_data.setUpload_id(upload_id);
			em.persist(cust_data);

			logger.info("saleRegister.getMobile2() :" + saleRegister.getMobile2());
			if (saleRegister.getMobile2() != null) {
				Phone phoneNo = new Phone();
				phoneNo.setPhoneNumber(saleRegister.getMobile2());
				phoneNo.setCustomer(cust_data);
				phoneNo.setPhoneTye(phoneNo.PHONE_TYPE_OFFICE);
				phoneNo.setIsPreferredPhone(true);
				phoneNo.setUpload_id(upload_id);
				em.persist(phoneNo);
			}
			if (saleRegister.getResCode() != null) {
				Phone phoneNum = new Phone();
				phoneNum.setPhoneNumber(saleRegister.getResCode());
				phoneNum.setCustomer(cust_data);
				phoneNum.setPhoneTye(phoneNum.PHONE_TYPE_RESIDENCE);
				phoneNum.setIsPreferredPhone(false);
				phoneNum.setUpload_id(upload_id);
				em.persist(phoneNum);
			}
			saleRegister.setRegNo(removeSpclChar(saleRegister.getRegNo()));
			Vehicle vehData = new Vehicle();
			vehData.setChassisNo(saleRegister.getChassis());
			vehData.setColor(saleRegister.getColorcode());
			vehData.setEngineNo(saleRegister.getEngine());
			vehData.setFuelType(saleRegister.getFuelType());
			vehData.setVariant(saleRegister.getVariantCode());
			vehData.setVehicleRegNo(removeSpclChar(saleRegister.getRegNo()));
			vehData.setModel(saleRegister.getModel());
			vehData.setUpload_id(upload_id);
			vehData.setIsAssigned("true");
			vehData.setCustomer(cust_data);
			vehData.setColorCode(saleRegister.getColorcode());
			vehData.setVariantCode(saleRegister.getVariantCode());
			vehData.setEnquiryNo(saleRegister.getEnquiryNo());
			vehData.setCustomerId(saleRegister.getCustomerId());
			vehData.setDistrict(saleRegister.getDistrict());
			vehData.setEvaluator_name_MSPIN(saleRegister.getEvaluatorMSPIN());
			vehData.setExch_Canc_Date(saleRegister.getInvCancelDate());
			vehData.setDeliveryDate_Sale(saleRegister.getDelDate());
			vehData.setExch_Canc_Reason(saleRegister.getExchCancReason());
			vehData.setExch_Canc_Date(saleRegister.getExchangeCancDate());
			vehData.setInvDate(saleRegister.getInvDate());
			vehData.setMiDate(saleRegister.getMiDate());
			vehData.setMiFlag(saleRegister.getMiFlag());
			vehData.setMiName(saleRegister.getMiname());
			vehData.setOldCar_regNum(saleRegister.getOldCarRegNo());
			vehData.setOld_Car_Relation(saleRegister.getOldCarRelation());
			vehData.setOldcar_Manufacturer(saleRegister.getOldCarMfg());
			vehData.setOldcar_Owner(saleRegister.getOldCarOwner());
			vehData.setOldcar_Status(saleRegister.getOldCarStatus());
			vehData.setOldcar_modelCode(saleRegister.getOldCarModelCode());
			vehData.setRefVehicle_regnNo(removeSpclChar(saleRegister.getRefRegNo()));
			vehData.setRef_MobileNo(saleRegister.getRefMobileNo());
			vehData.setReferenceType(saleRegister.getRefType());
			vehData.setReferredBy(saleRegister.getRefby());
			vehData.setRefferenceNo(removeSpclChar(saleRegister.getRegNo()));
			vehData.setEwType(saleRegister.getExWarrantyType());
			vehData.setExtWarrantyType(saleRegister.getExWarrantyType());
			vehData.setHyp_Address(saleRegister.getHyp_address());
			vehData.setHyp_Amount(saleRegister.getHypAmt());
			vehData.setHypo(saleRegister.getHypthecation());
			vehData.setInstitutionalCustomer(saleRegister.getInstiCust());
			vehData.setLoyalty_bonusDiscount(saleRegister.getLoyaltyBonusDisc());
			vehData.setMul_Inv_Dt(saleRegister.getMulInvDate());
			vehData.setMul_Inv_No(saleRegister.getMulInvNo());
			vehData.setSchemeof_GOI(saleRegister.getSchemeOfGoi());
			vehData.setCsdEx_showroomDiscount(saleRegister.getCsdExShowRoom());
			vehData.setStateDesc(saleRegister.getStateDesc());
			vehData.setStatus(saleRegister.getStatus());
			vehData.setStdCode_comp(saleRegister.getStdCodeComp());
			vehData.setStd_code(saleRegister.getStdCode());
			vehData.setAddtional_Tax(saleRegister.getAdditionalTax());
			vehData.setBasic_Price(saleRegister.getBasicPrice());
			vehData.setCancel_Date(saleRegister.getInvCancelDate());
			vehData.setCancel_No(saleRegister.getInvCancelNo());
			vehData.setClr_Desc(saleRegister.getColorDesc());
			vehData.setCreatedDate(currentDate());
			vehData.setCity(saleRegister.getCity());
			vehData.setTehsilDesc(saleRegister.getTehsilDesc());
			vehData.setDistrict(saleRegister.getDistrict());
			vehData.setStdCode_comp(saleRegister.getStdCodeComp());
			vehData.setStd_code(saleRegister.getStdCode());
			vehData.setValueadded_Tax(saleRegister.getVat());
			vehData.setVillageDesc(saleRegister.getVillageDesc());

			String outletcode = saleRegister.getOutlet();
			long outletcount = source.outlets(em).where(u -> u.getOutletCode().equals(outletcode)).count();

			if (outletcount > 0) {
				Outlet outlet = source.outlets(em).where(u -> u.getOutletCode().equals(outletcode)).getOnlyValue();
				vehData.setOutlets(outlet);
			}

			long countOfLocation = source.locations(em).where(u -> u.getLocCode().equals(locCodeInfo)).count();
			if (countOfLocation > 0) {
				Location locRowId = source.locations(em).where(u -> u.getLocCode().equals(locCodeInfo)).getOnlyValue();
				vehData.setLocation(locRowId);
			}
			em.persist(vehData);

			Address address_data = new Address();
			logger.info("saleRegister.getAddress1() :" + saleRegister.getAddress1());
			address_data.setAddressLine1(saleRegister.getAddress1());
			address_data.setAddressLine2(saleRegister.getAddress2());
			address_data.setAddressLine3(saleRegister.getAddress3());
			address_data.setAddressType(address_data.ADDRESS_TYPE_OFFICE);
			address_data.setIsPreferred(true);
			address_data.setCustomer(cust_data);
			address_data.setUpload_id(upload_id);
			em.persist(address_data);

			if (uploadSuccessRecord.containsKey(saleRegister.getInvDate())) {

				int vlaueOfKeyis = uploadSuccessRecord.get(saleRegister.getInvDate());
				vlaueOfKeyis++;

				uploadSuccessRecord.put(saleRegister.getInvDate(), vlaueOfKeyis);

			} else {

				uploadSuccessRecord.put(saleRegister.getInvDate(), 1);

			}

		} else {

			if (uploadRejectedRecord.containsKey(saleRegister.getInvDate())) {

				int vlaueOfKeyis = uploadRejectedRecord.get(saleRegister.getInvDate());
				vlaueOfKeyis++;

				uploadRejectedRecord.put(saleRegister.getInvDate(), vlaueOfKeyis);

			} else {

				uploadRejectedRecord.put(saleRegister.getInvDate(), 1);

			}

			Map<String, String> map = saleRegister.getErrorInfo();

			String[] listOfcolumns = new String[60];

			int i = 0;

			for (Map.Entry<String, String> entry : map.entrySet()) {
				// logger.info("add data to String array");
				listOfcolumns[i] = entry.getKey();
				i++;

			}

			SaleRegisterExcelError saleRegisterDataerror = new SaleRegisterExcelError();
			BeanUtils.copyProperties(saleRegister, saleRegisterDataerror, listOfcolumns);

			String errorAppendVariable = "";

			for (Map.Entry<String, String> entry : map.entrySet()) {
				System.out.println("Key for error is = " + entry.getKey() + ", Value = " + entry.getValue());

				String expeError = entry.getKey() + "|" + entry.getValue();

				if (errorAppendVariable.equals("")) {

					errorAppendVariable = expeError;

				} else {

					errorAppendVariable = errorAppendVariable + "," + expeError;
				}

				saleRegisterDataerror.setErrorInformation(errorAppendVariable);
				saleRegisterDataerror.setUpload_id(upload_id);

			}
			em.persist(saleRegisterDataerror);

			//

		}

	}

	private void insertJobCardData(Object excelData, String upload_id, HashMap<String, String> formData,
			HashMap<Date, Integer> uploadSuccessRecord, HashMap<Date, Integer> uploadRejectedRecord) {

		JobCardStatus jobCardData = (JobCardStatus) excelData;
		// Vehicle vehicleInfo = new Vehicle();

		if (jobCardData.getJobCardDate() != null) {

		} else {
			try {

				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				java.sql.Date sqlDate = new java.sql.Date(df.parse("1947-08-15").getTime());
				logger.info("date dateNew for foll : " + sqlDate);
				jobCardData.setJobCardDate(sqlDate);

			} catch (Exception ex) {

				logger.info("date exp : " + ex);

			}
		}

		logger.info("row number data : " + jobCardData.getRowNumber());
		logger.info("is Error data : " + jobCardData.isError());

		if (!jobCardData.isError()) {
			String vehRegistrationNo = removeSpclChar(jobCardData.getRegNo());
			String chass_no = jobCardData.getChassis();
			String jobCardNum = jobCardData.getJobCardNo();

			String workshop_id = formData.get("workShopId");
			long workshop_id_concver = Integer.parseInt(workshop_id);

			Workshop work_shop = em.find(Workshop.class, workshop_id_concver);

			logger.info("job card date is : " + jobCardData.getJobCardDate() + " job card number "
					+ jobCardData.getJobCardNo());

			Date billdate = jobCardData.getBillDate();

			// uploadLink =
			// getUploadDataLinkByRecordedDate(jobCardData.getJobCardDate(),
			// uploadLink);

			// check the vehicle count
			long countOfVehicle = source.vehicle(em).where(u -> u.getChassisNo().equals(chass_no)).count();

			if (countOfVehicle != 0) {

				logger.info("Entering in existing vehicle");

				logger.info("ciount of vehicle" + countOfVehicle);
				Vehicle vehRowId = source.vehicle(em).where(u -> u.getChassisNo().equals(chass_no)).getOnlyValue();

				long vehicleid = vehRowId.getVehicle_id();

				
				long countOfVehicleFollowUp = source.callinteractions(em).where(u -> u.getVehicle() != null)
						.where(u -> u.getVehicle().getVehicle_id() == vehicleid)
						.where(u -> u.getAssignedInteraction()!=null)
						.where(u -> u.getSrdisposition()!=null && u.getSrdisposition().getCallDispositionData() != null)
						.where(u -> u.getSrdisposition().getCallDispositionData().getDispositionId() == 4
								&& u.getSrdisposition().getIsFollowUpDone().equals("No"))
						.count();
				logger.info("countOfVehicleFollowUp : " + countOfVehicleFollowUp);

				if (countOfVehicleFollowUp > 0) {

					List<CallInteraction> listOfFollowCallInForecast = source.callinteractions(em)
							.where(u -> u.getVehicle() != null).where(u -> u.getVehicle().getVehicle_id() == vehicleid)
							.where(u -> u.getAssignedInteraction()!=null)
							.where(u -> u.getSrdisposition()!=null && u.getSrdisposition().getCallDispositionData() != null)
							.where(u -> u.getSrdisposition().getCallDispositionData().getDispositionId() == 4
									&& u.getSrdisposition().getIsFollowUpDone().equals("No"))
							.toList();

					for (CallInteraction sa : listOfFollowCallInForecast) {

						SRDisposition srDispo = sa.getSrdisposition();
						em.remove(srDispo);
						
					}

				}
				
				long countOfAssignedCallInForecast = source.assignedInteractions(em)
						.where(u -> u.getVehicle().getVehicle_id() == vehicleid && u.getCallMade().equals("No"))
						.count();

				if (countOfAssignedCallInForecast > 0) {

					List<AssignedInteraction> listOfAssignedCallInForecast = source.assignedInteractions(em)
							.where(u -> u.getVehicle().getVehicle_id() == vehicleid && u.getCallMade().equals("No"))
							.toList();

					for (AssignedInteraction sa : listOfAssignedCallInForecast) {
						sa.setCallMade("Yes");
						em.merge(sa);
					}

				}

				logger.info("countOfAssignedCallInForecast : " + countOfAssignedCallInForecast);


				long customer_id = vehRowId.getCustomer().getId();

				Customer customerData = em.find(Customer.class, customer_id); // this
																				// will
																				// give
																				// whole
																				// row
																				// of
																				// customer
				long vehicleId = vehRowId.getVehicle_id();
				customerData.setDob(String.valueOf(jobCardData.getDob()));
				customerData.setUpload_id(upload_id);
				em.merge(customerData);

				long custId = customerData.getId();

				long addressCount = source.address(em).where(u -> u.getCustomer().getId() == custId).count();
				if (addressCount > 0) {

					List<Address> addressList = source.address(em).where(u -> u.getCustomer().getId() == custId)
							.toList();

					for (Address address_list : addressList) {

						address_list.setIsPreferred(false);
						em.merge(address_list);

					}
					Address addressData = new Address();
					addressData.setAddressLine1(jobCardData.getAddress1());
					addressData.setAddressLine2(jobCardData.getAddress2());
					addressData.setAddressLine3(jobCardData.getAddress3());
					addressData.setCity(jobCardData.getCity());
					addressData.setPincode(jobCardData.getPincode());
					addressData.setCustomer(customerData);// linkiing custome to
															// address
					addressData.setUpload_id(upload_id);

					addressData.setIsPreferred(true);
					em.merge(addressData);

				} else {
					Address addressData = new Address();
					addressData.setAddressLine1(jobCardData.getAddress1());
					addressData.setAddressLine2(jobCardData.getAddress2());
					addressData.setAddressLine3(jobCardData.getAddress3());
					addressData.setCity(jobCardData.getCity());
					addressData.setPincode(jobCardData.getPincode());
					addressData.setCustomer(customerData);// linkiing custome to
															// address
					addressData.setAddressType(Address.ADDRESS_TYPE_OFFICE);
					addressData.setUpload_id(upload_id);
					addressData.setIsPreferred(true);
					em.merge(addressData);
				}

				long segmentCount = source.segments(em).where(u -> u.getCustomer().getId() == custId).count();
				logger.info("segment count" + segmentCount);

				if (segmentCount > 0) {
					Segment segmentInfo = source.segments(em).where(u -> u.getCustomer().getId() == custId)
							.getOnlyValue();
					segmentInfo.setName(jobCardData.getCustCat());
					em.merge(segmentInfo);

				} else {
					logger.info("else segment count" + segmentCount);
					Segment segmentInfo = new Segment();
					segmentInfo.setName(jobCardData.getCustCat());
					segmentInfo.setCustomer(customerData);
					em.persist(segmentInfo);
				}
				vehRowId.setVehicleRegNo(jobCardData.getRegNo());
				vehRowId.setJobCardNumber(jobCardData.getJobCardNo());
				vehRowId.setLastServiceDate(jobCardData.getBillDate());
				vehRowId.setLastServiceType(jobCardData.getServiceType());
				vehRowId.setCity(jobCardData.getCity());
				vehRowId.setModel(jobCardData.getModel());
				vehRowId.setOdometerReading(String.valueOf(jobCardData.getMileage()));
				vehRowId.setEngineNo(jobCardData.getEngineNo());
				vehRowId.setColor(jobCardData.getColor());
				vehRowId.setVariant(jobCardData.getVariant());
				vehRowId.setUpload_id(upload_id);
				em.merge(vehRowId);

				Service serviceData = new Service();
				serviceData.setLastServiceDate(jobCardData.getBillDate());
				serviceData.setLastServiceType(jobCardData.getServiceType());
				serviceData.setJobCardNumber(jobCardData.getJobCardNo());
				serviceData.setJobCardDate(jobCardData.getJobCardDate());
				serviceData.setJobCardNumber(jobCardData.getJobCardNo());
				serviceData.setPsfStatus(jobCardData.getPsfStatus());
				serviceData.setSaleDate(jobCardData.getSaleDate());
				serviceData.setTechnician(jobCardData.getTechnician());
				serviceData.setCircularNo(jobCardData.getCircularNo());
				serviceData.setServiceType(jobCardData.getServiceType());
				serviceData.setEstLabAmt(jobCardData.getEstLabAmt());
				serviceData.setEstPartAmt(jobCardData.getEstPartAmt());
				serviceData.setPromiseDate(jobCardData.getPromiseDt());
				serviceData.setRevisedPromiseDate(jobCardData.getRevPromisedDt());
				serviceData.setReadyDate(jobCardData.getReadyDateandTime());
				serviceData.setLabAmt(jobCardData.getLabAmt());
				serviceData.setPartsAmt(jobCardData.getPartsAmt());
				serviceData.setPickupDate(jobCardData.getPickupDate());
				serviceData.setBillDate(jobCardData.getBillDate());
				serviceData.setBillAmt(jobCardData.getBillAmt());
				serviceData.setBillNo(jobCardData.getBillNo());
				serviceData.setBillNumber(jobCardData.getBillNo());
				serviceData.setVehicle(vehRowId);// linking veh to service
				serviceData.setUpload_id(upload_id);
				serviceData.setWorkshop(work_shop);
				serviceData.setLastServiceMeterReading(String.valueOf(jobCardData.getMileage()));
				if (jobCardData.getMileage() != null) {
					serviceData.setServiceOdometerReading(String.valueOf(jobCardData.getMileage()));
				} else {

					serviceData.setServiceOdometerReading("0");
				}
				em.persist(serviceData);

				// Phone phoneInfo = new Phone();
				// phoneInfo.setPhoneNumber(jobCardData.getMobile());
				// phoneInfo.setCustomer(customerData);
				// phoneInfo.setUploadData(uploadLink);
				// em.persist(phoneInfo);

				long phonecount = source.phones(em).where(u -> u.getCustomer() != null)
						.where(u -> u.getCustomer().getId() == custId).count();
				if (phonecount > 0) {

					String phone_number = jobCardData.getMobile();

					long countOfphonelist = source.phones(em).where(u -> u.getCustomer() != null)
							.where(u -> u.getCustomer().getId() == custId)
							.where(u -> u.getPhoneNumber().equals(phone_number)).count();

					if (countOfphonelist == 0) {
						List<Phone> phoneList = source.phones(em).where(u -> u.getCustomer() != null)
								.where(u -> u.getCustomer().getId() == custId).toList();

						for (Phone phone_list : phoneList) {

							phone_list.setIsPreferredPhone(false);
							em.merge(phone_list);

						}
						Phone jobPhoneData = new Phone();
						jobPhoneData.setPhoneNumber(jobCardData.getMobile());
						jobPhoneData.setCustomer(customerData);
						jobPhoneData.setUpload_id(upload_id);
						jobPhoneData.setIsPreferredPhone(true);
						em.merge(jobPhoneData);
					}

				} else {

					Phone jobPhoneData = new Phone();
					jobPhoneData.setPhoneNumber(jobCardData.getMobile());
					jobPhoneData.setCustomer(customerData);
					jobPhoneData.setUpload_id(upload_id);
					jobPhoneData.setIsPreferredPhone(true);
					jobPhoneData.setPhoneTye(Phone.PHONE_TYPE_OFFICE);
					em.merge(jobPhoneData);

				}

				if (uploadSuccessRecord.containsKey(jobCardData.getJobCardDate())) {

					int vlaueOfKeyis = uploadSuccessRecord.get(jobCardData.getJobCardDate());
					vlaueOfKeyis++;

					uploadSuccessRecord.put(jobCardData.getJobCardDate(), vlaueOfKeyis);

				} else {

					uploadSuccessRecord.put(jobCardData.getJobCardDate(), 1);

				}

			} else {
				logger.info("vehicle not existing");
				Customer customerinfo = new Customer();
				customerinfo.setDob(String.valueOf(jobCardData.getDob()));
				customerinfo.setAnniversary_date(String.valueOf(jobCardData.getDoa()));
				customerinfo.setUpload_id(upload_id);
				customerinfo.setCustomerName(jobCardData.getCustName());
				em.persist(customerinfo);

				Vehicle vehicledataInfo = new Vehicle();
				vehicledataInfo.setJobCardNumber(jobCardData.getJobCardNo());
				vehicledataInfo.setLastServiceDate(jobCardData.getBillDate());
				vehicledataInfo.setLastServiceType(jobCardData.getServiceType());
				vehicledataInfo.setVehicleRegNo(vehRegistrationNo);
				vehicledataInfo.setChassisNo(chass_no);
				vehicledataInfo.setModel(jobCardData.getModel());
				vehicledataInfo.setOdometerReading(String.valueOf(jobCardData.getMileage()));
				vehicledataInfo.setEngineNo(jobCardData.getEngineNo());
				vehicledataInfo.setColor(jobCardData.getColor());
				vehicledataInfo.setVariant(jobCardData.getVariant());
				vehicledataInfo.setCustomer(customerinfo);
				vehicledataInfo.setCity(jobCardData.getCity());
				vehicledataInfo.setUpload_id(upload_id);
				em.persist(vehicledataInfo);

				Service serviceData = new Service();
				serviceData.setLastServiceDate(jobCardData.getBillDate());
				serviceData.setLastServiceType(jobCardData.getServiceType());
				serviceData.setJobCardNumber(jobCardData.getJobCardNo());
				serviceData.setJobCardDate(jobCardData.getJobCardDate());
				serviceData.setJobCardNumber(jobCardData.getJobCardNo());
				serviceData.setPsfStatus(jobCardData.getPsfStatus());
				serviceData.setSaleDate(jobCardData.getSaleDate());
				serviceData.setTechnician(jobCardData.getTechnician());
				serviceData.setCircularNo(jobCardData.getCircularNo());
				serviceData.setServiceType(jobCardData.getServiceType());
				serviceData.setEstLabAmt(jobCardData.getEstLabAmt());
				serviceData.setEstPartAmt(jobCardData.getEstPartAmt());
				serviceData.setPromiseDate(jobCardData.getPromiseDt());
				serviceData.setRevisedPromiseDate(jobCardData.getRevPromisedDt());
				serviceData.setReadyDate(jobCardData.getReadyDateandTime());
				serviceData.setLabAmt(jobCardData.getLabAmt());
				serviceData.setPartsAmt(jobCardData.getPartsAmt());
				serviceData.setPickupDate(jobCardData.getPickupDate());
				serviceData.setBillDate(jobCardData.getBillDate());
				serviceData.setBillAmt(jobCardData.getBillAmt());
				serviceData.setBillNo(jobCardData.getBillNo());
				serviceData.setBillNumber(jobCardData.getBillNo());
				serviceData.setVehicle(vehicledataInfo);
				serviceData.setUpload_id(upload_id);
				serviceData.setWorkshop(work_shop);
				serviceData.setLastServiceMeterReading(String.valueOf(jobCardData.getMileage()));

				if (jobCardData.getMileage() != null) {
					serviceData.setServiceOdometerReading(String.valueOf(jobCardData.getMileage()));
				} else {

					serviceData.setServiceOdometerReading("0");
				}
				em.persist(serviceData);

				Address addressData = new Address();
				addressData.setAddressLine1(jobCardData.getAddress1());
				addressData.setAddressLine2(jobCardData.getAddress2());
				addressData.setAddressLine3(jobCardData.getAddress3());
				addressData.setCity(jobCardData.getCity());
				addressData.setPincode(jobCardData.getPincode());
				addressData.setCustomer(customerinfo);
				addressData.setUpload_id(upload_id);
				addressData.setIsPreferred(true);
				addressData.setAddressType(Address.ADDRESS_TYPE_OFFICE);
				em.persist(addressData);

				Segment segmentInfo = new Segment();
				segmentInfo.setName(jobCardData.getCustCat());
				segmentInfo.setCustomer(customerinfo);
				em.persist(segmentInfo);
				logger.info("mobile number is : ");

				Phone phoneInfo = new Phone();
				phoneInfo.setPhoneNumber(jobCardData.getMobile());
				phoneInfo.setCustomer(customerinfo);
				phoneInfo.setUpload_id(upload_id);
				phoneInfo.setPhoneTye(Phone.PHONE_TYPE_OFFICE);
				phoneInfo.setIsPreferredPhone(true);
				em.persist(phoneInfo);

				if (uploadSuccessRecord.containsKey(jobCardData.getJobCardDate())) {

					int vlaueOfKeyis = uploadSuccessRecord.get(jobCardData.getJobCardDate());
					vlaueOfKeyis++;

					uploadSuccessRecord.put(jobCardData.getJobCardDate(), vlaueOfKeyis);

				} else {

					uploadSuccessRecord.put(jobCardData.getJobCardDate(), 1);

				}

			}
		} else {

			if (uploadRejectedRecord.containsKey(jobCardData.getJobCardDate())) {

				int vlaueOfKeyis = uploadRejectedRecord.get(jobCardData.getJobCardDate());
				vlaueOfKeyis++;

				uploadRejectedRecord.put(jobCardData.getJobCardDate(), vlaueOfKeyis);

			} else {

				uploadRejectedRecord.put(jobCardData.getJobCardDate(), 1);

			}

			Map<String, String> map = jobCardData.getErrorInfo();

			String[] listOfcolumns = new String[60];

			int i = 0;

			for (Map.Entry<String, String> entry : map.entrySet()) {
				// logger.info("add data to String array");
				listOfcolumns[i] = entry.getKey();
				i++;

			}

			JobCardStatusError jobCardDataerror = new JobCardStatusError();
			BeanUtils.copyProperties(jobCardData, jobCardDataerror, listOfcolumns);

			String errorAppendVariable = "";

			for (Map.Entry<String, String> entry : map.entrySet()) {
				System.out.println("Key for error is = " + entry.getKey() + ", Value = " + entry.getValue());

				String expeError = entry.getKey() + "|" + entry.getValue();

				if (errorAppendVariable.equals("")) {

					errorAppendVariable = expeError;

				} else {

					errorAppendVariable = errorAppendVariable + "," + expeError;
				}

				jobCardDataerror.setErrorInformation(errorAppendVariable);
				jobCardDataerror.setUpload_id(upload_id);

			}
			em.persist(jobCardDataerror);

			//

		}

	}

	private void insertWorkBillData(Object excelData, String upload_id, String campaignExpiry,
			HashMap<Date, Integer> uploadSuccessRecord, HashMap<Date, Integer> uploadRejectedRecord) {

		WorkBillRegister workBillData = (WorkBillRegister) excelData;

		logger.info("row number data : " + workBillData.getRowNumber());
		logger.info("is Error data : " + workBillData.isError());

		if (workBillData.getBillDate() == null) {
			try {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				java.sql.Date sqlDate = new java.sql.Date(df.parse("1947-08-15").getTime());
				logger.info("date dateNew for foll : " + sqlDate);

				workBillData.setBillDate(sqlDate);
			} catch (Exception ex) {

				logger.info("date exp : " + ex);

			}
		}

		if (!workBillData.isError()) {

			String jcnum = workBillData.getJobCardNo();
			String locText = workBillData.getLocation();

			logger.info("Work bill upload");

			// boolean insertRecord = false;

			long JCcountForVeh = source.services(em).where(u -> u.getJobCardNumber().equals(jcnum)).count();
			long workShopCount = source.workshop(em).where(u -> u.getWorkshopCode().equals(locText)).count();

			// uploadLink =
			// getUploadDataLinkByRecordedDate(workBillData.getBillDate(),
			// uploadLink);

			if (JCcountForVeh != 0 && workShopCount != 0) {

				logger.info("count for workshop::" + workShopCount);
				Workshop workshopCodes = source.workshop(em).where(u -> u.getWorkshopCode().equals(locText))
						.getOnlyValue();
				long workshopCodeID = workshopCodes.getId();

				long countOfSevice = source.services(em)
						.where(u -> u.getJobCardNumber().equals(jcnum) && u.getWorkshop().getId() == workshopCodeID)
						.count();

				if (countOfSevice > 0) {
					logger.info("Service count : " + countOfSevice);

					List<Service> serviceForJCnum = source.services(em)
							.where(u -> u.getJobCardNumber().equals(jcnum) && u.getWorkshop().getId() == workshopCodeID)
							.toList();
					serviceForJCnum.get(0).setBillNumber(workBillData.getBillNo());
					serviceForJCnum.get(0).setLastServiceDate(workBillData.getBillDateold());
					serviceForJCnum.get(0).setJobCardNumber(workBillData.getJobCardNo());
					serviceForJCnum.get(0).setBillDate(workBillData.getBillDate());
					serviceForJCnum.get(0).setPartBasic(workBillData.getPartBasic());
					serviceForJCnum.get(0).setPartCharges(workBillData.getPartCharges());
					serviceForJCnum.get(0).setLabourBasic(workBillData.getLabourBasic());
					serviceForJCnum.get(0).setLabourDiscount(workBillData.getLabourDisc());
					serviceForJCnum.get(0).setLabourCharges(workBillData.getLabourCharge());
					serviceForJCnum.get(0).setRoundOffamt(workBillData.getRoundOffAmt());
					serviceForJCnum.get(0).setBillAmt(workBillData.getBillAmt());
					// serviceForJCnum.get(0).setUploadDataFiletype("Bill");
					serviceForJCnum.get(0).setBillDate(workBillData.getBillDate());

					Date billDateforPsf2IfnoVeh = workBillData.getBillDate();
					Calendar cal1insert = Calendar.getInstance();
					cal1insert.setTime(billDateforPsf2IfnoVeh);
					cal1insert.add(Calendar.DATE, 6); // add 10 days
					billDateforPsf2IfnoVeh = cal1insert.getTime();
					logger.info("psf1 :" + billDateforPsf2IfnoVeh);
					serviceForJCnum.get(0).setPsf1(billDateforPsf2IfnoVeh);

					Date billDateforPsf3IfnoVeh = workBillData.getBillDate();
					Calendar cal2insert = Calendar.getInstance();
					cal2insert.setTime(billDateforPsf3IfnoVeh);
					cal2insert.add(Calendar.DATE, 15); // add 10 days
					billDateforPsf3IfnoVeh = cal2insert.getTime();
					serviceForJCnum.get(0).setPsf2(billDateforPsf3IfnoVeh);

					Date billDateforPsf4IfnoVeh = workBillData.getBillDate();
					Calendar cal3insert = Calendar.getInstance();
					cal3insert.setTime(billDateforPsf4IfnoVeh);
					cal3insert.add(Calendar.DATE, 30); // add 10 days
					billDateforPsf4IfnoVeh = cal3insert.getTime();
					serviceForJCnum.get(0).setPsf3(billDateforPsf4IfnoVeh);

					Date billDateforPsf1IfnoVeh = workBillData.getBillDate();
					Calendar calinsert = Calendar.getInstance();
					calinsert.setTime(billDateforPsf1IfnoVeh);
					calinsert.add(Calendar.DATE, 3); // add 10 days
					billDateforPsf1IfnoVeh = calinsert.getTime();
					logger.info("psf4 :" + billDateforPsf1IfnoVeh);
					serviceForJCnum.get(0).setPsf4(billDateforPsf1IfnoVeh);
					serviceForJCnum.get(0).setLastServiceDate(workBillData.getBillDate());
					serviceForJCnum.get(0).setUpload_id(upload_id);

					em.merge(serviceForJCnum.get(0));

					List<Campaign> psfCampaignList = source.campaigns(em).where(u -> u.getCampaignType().equals("PSF"))
							.toList();

					long serviceId = serviceForJCnum.get(0).getId();

					long countOfPSF1 = source.psfAssignedInteraction(em)
							.filter(u -> u.getService().getId() == serviceId).filter(u -> u.getCampaign().getId() == 4)
							.count();

					logger.info("countOfPSF1 : " + countOfPSF1);

					if (countOfPSF1 == 0) {

						PSFAssignedInteraction psf1AssignEntry = new PSFAssignedInteraction();
						psf1AssignEntry.setCallMade("No");
						psf1AssignEntry.setCustomer(serviceForJCnum.get(0).getVehicle().getCustomer());
						psf1AssignEntry.setVehicle(serviceForJCnum.get(0).getVehicle());
						psf1AssignEntry.setUplodedCurrentDate(currentDate());
						psf1AssignEntry.setService(serviceForJCnum.get(0));
						psf1AssignEntry.setCampaign(psfCampaignList.get(0));
						em.persist(psf1AssignEntry);
					}

					long countOfPSF2 = source.psfAssignedInteraction(em)
							.filter(u -> u.getService().getId() == serviceId).filter(u -> u.getCampaign().getId() == 5)
							.count();

					logger.info("countOfPSF2 : " + countOfPSF2);

					if (countOfPSF2 == 0) {
						PSFAssignedInteraction psf2AssignEntry = new PSFAssignedInteraction();
						psf2AssignEntry.setCallMade("No");
						psf2AssignEntry.setCustomer(serviceForJCnum.get(0).getVehicle().getCustomer());
						psf2AssignEntry.setVehicle(serviceForJCnum.get(0).getVehicle());
						psf2AssignEntry.setUplodedCurrentDate(currentDate());
						psf2AssignEntry.setService(serviceForJCnum.get(0));
						psf2AssignEntry.setCampaign(psfCampaignList.get(1));
						em.persist(psf2AssignEntry);
					}

					long countOfPSF3 = source.psfAssignedInteraction(em)
							.filter(u -> u.getService().getId() == serviceId).filter(u -> u.getCampaign().getId() == 6)
							.count();

					logger.info("countOfPSF3 : " + countOfPSF3);

					if (countOfPSF3 == 0) {
						PSFAssignedInteraction psf3AssignEntry = new PSFAssignedInteraction();
						psf3AssignEntry.setCallMade("No");
						psf3AssignEntry.setCustomer(serviceForJCnum.get(0).getVehicle().getCustomer());
						psf3AssignEntry.setVehicle(serviceForJCnum.get(0).getVehicle());
						psf3AssignEntry.setUplodedCurrentDate(currentDate());
						psf3AssignEntry.setService(serviceForJCnum.get(0));
						psf3AssignEntry.setCampaign(psfCampaignList.get(2));
						em.persist(psf3AssignEntry);
					}

					long countOfPSF4 = source.psfAssignedInteraction(em)
							.filter(u -> u.getService().getId() == serviceId).filter(u -> u.getCampaign().getId() == 7)
							.count();

					logger.info("countOfPSF4 : " + countOfPSF4);

					if (countOfPSF4 == 0) {

						PSFAssignedInteraction psfMobileAssignEntry = new PSFAssignedInteraction();
						psfMobileAssignEntry.setCallMade("No");
						psfMobileAssignEntry.setCustomer(serviceForJCnum.get(0).getVehicle().getCustomer());
						psfMobileAssignEntry.setVehicle(serviceForJCnum.get(0).getVehicle());
						psfMobileAssignEntry.setUplodedCurrentDate(currentDate());
						psfMobileAssignEntry.setService(serviceForJCnum.get(0));
						psfMobileAssignEntry.setCampaign(psfCampaignList.get(3));
						em.persist(psfMobileAssignEntry);

					}

					if (uploadSuccessRecord.containsKey(workBillData.getBillDate())) {

						int vlaueOfKeyis = uploadSuccessRecord.get(workBillData.getBillDate());
						vlaueOfKeyis++;

						uploadSuccessRecord.put(workBillData.getBillDate(), vlaueOfKeyis);

					} else {

						uploadSuccessRecord.put(workBillData.getBillDate(), 1);

					}

				} else {

					if (uploadRejectedRecord.containsKey(workBillData.getBillDate())) {

						int vlaueOfKeyis = uploadRejectedRecord.get(workBillData.getBillDate());
						vlaueOfKeyis++;

						uploadRejectedRecord.put(workBillData.getBillDate(), vlaueOfKeyis);

					} else {

						uploadRejectedRecord.put(workBillData.getBillDate(), 1);

					}

					WorkBillRegisterError workbillDataerror = new WorkBillRegisterError();
					BeanUtils.copyProperties(workBillData, workbillDataerror);

					workbillDataerror.setErrorInformation("Service Not Existing");
					workbillDataerror.setUpload_id(upload_id);
					em.persist(workbillDataerror);
				}

			} else {

				if (uploadRejectedRecord.containsKey(workBillData.getBillDate())) {

					int vlaueOfKeyis = uploadRejectedRecord.get(workBillData.getBillDate());
					vlaueOfKeyis++;

					uploadRejectedRecord.put(workBillData.getBillDate(), vlaueOfKeyis);

				} else {

					uploadRejectedRecord.put(workBillData.getBillDate(), 1);

				}

				WorkBillRegisterError workbillDataerror = new WorkBillRegisterError();
				BeanUtils.copyProperties(workBillData, workbillDataerror);
				workbillDataerror.setErrorInformation("Job Card and Workshop not matching");
				workbillDataerror.setUpload_id(upload_id);
				em.persist(workbillDataerror);

			}

		} else {

			if (uploadRejectedRecord.containsKey(workBillData.getBillDate())) {

				int vlaueOfKeyis = uploadRejectedRecord.get(workBillData.getBillDate());
				vlaueOfKeyis++;

				uploadRejectedRecord.put(workBillData.getBillDate(), vlaueOfKeyis);

			} else {

				uploadRejectedRecord.put(workBillData.getBillDate(), 1);

			}

			Map<String, String> map = workBillData.getErrorInfo();

			String[] listOfcolumns = new String[60];

			int i = 0;

			for (Map.Entry<String, String> entry : map.entrySet()) {
				// logger.info("add data to String array");
				listOfcolumns[i] = entry.getKey();
				i++;

			}

			WorkBillRegisterError workbillDataerror = new WorkBillRegisterError();
			BeanUtils.copyProperties(workBillData, workbillDataerror, listOfcolumns);

			String errorAppendVariable = "";

			for (Map.Entry<String, String> entry : map.entrySet()) {
				System.out.println("Key for error is = " + entry.getKey() + ", Value = " + entry.getValue());

				String expeError = entry.getKey() + "|" + entry.getValue();

				if (errorAppendVariable.equals("")) {

					errorAppendVariable = expeError;

				} else {

					errorAppendVariable = errorAppendVariable + "," + expeError;
				}

				workbillDataerror.setErrorInformation(errorAppendVariable);

			}
			workbillDataerror.setUpload_id(upload_id);
			em.persist(workbillDataerror);

			//

		}
	}

	private void insertSMRData(Object excelData, String upload_id, HashMap<Date, Integer> uploadSuccessRecord,
			HashMap<Date, Integer> uploadRejectedRecord) {
		SMRData smrData = (SMRData) excelData;

		logger.info("row number data : " + smrData.getRowNumber());
		logger.info("is Error data : " + smrData.isError());

		if (smrData.getFollowUpNew() == null) {
			try {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				java.sql.Date sqlDate = new java.sql.Date(df.parse("1947-08-15").getTime());
				logger.info("date dateNew for foll : " + sqlDate);
				smrData.setFollowUpNew(sqlDate);
			} catch (Exception ex) {

				logger.info("date exp : " + ex);

			}
		}

		if (!smrData.isError()) {

			String vehiReg = removeSpclChar(smrData.getRegNo());
			String chass = smrData.getChassis();
			logger.info("vehicleExistingcount excelData" + excelData);
			// Update
			long vehicleExistingcount = source.vehicle(em)
					.where(c -> c.getVehicleRegNo().equals(vehiReg) && c.getChassisNo().equals(chass)).count();
			logger.info("vehicleExistingcount count" + vehicleExistingcount);

			// uploadLink =
			// getUploadDataLinkByRecordedDate(smrData.getFollowUpNew(),
			// uploadLink);

			if (vehicleExistingcount != 0) {
				List<Vehicle> vehicleUpdate = source.vehicle(em)
						.where(c -> c.getVehicleRegNo().equals(vehiReg) && c.getChassisNo().equals(chass)).toList();
				Hibernate.initialize(vehicleUpdate.get(0).getCustomer());
				vehicleUpdate.get(0).setVehicleRegNo(removeSpclChar(smrData.getRegNo()));
				vehicleUpdate.get(0).setModel(smrData.getModel());
				vehicleUpdate.get(0).setFuelType(smrData.getFuelType());
				vehicleUpdate.get(0).setChassisNo(smrData.getChassis());
				vehicleUpdate.get(0).setOdometerReading(String.valueOf(smrData.getMileage()));
				vehicleUpdate.get(0).setNextServicedate(smrData.getDueDate());
				vehicleUpdate.get(0).setNextServicetype(smrData.getDueService());
				vehicleUpdate.get(0).setLastServiceDate(smrData.getLastServiceDate());
				vehicleUpdate.get(0).setLastServiceType(smrData.getLastServiceType());
				vehicleUpdate.get(0).setSaleDate(smrData.getSaleDate());
				vehicleUpdate.get(0).setExtendedWarentyDue(smrData.getExtWarranty());
				vehicleUpdate.get(0).setFollowUpDateSMR(smrData.getFollowUpNew());
				vehicleUpdate.get(0).setDeliveryDate_Sale(smrData.getDeliveryDate());
				vehicleUpdate.get(0).setIsAssigned("false");
				vehicleUpdate.get(0).setUpload_id(upload_id);
				vehicleUpdate.get(0).setOdometerReading(String.valueOf(smrData.getMileage()));

				em.merge(vehicleUpdate.get(0));

				long custId = vehicleUpdate.get(0).getCustomer().getId();
				long segmentCount = source.segments(em).where(u -> u.getCustomer().getId() == custId).count();
				logger.info("segment count" + segmentCount);

				if (segmentCount > 0) {
					Segment segmentInfo = source.segments(em).where(u -> u.getCustomer().getId() == custId)
							.getOnlyValue();
					segmentInfo.setName(smrData.getCustCat());
					segmentInfo.setDataUploadType(smrData.getFollowUpType());
					em.merge(segmentInfo);

				} else {
					logger.info("else segment count" + segmentCount);
					Segment segmentInfo = new Segment();
					segmentInfo.setName(smrData.getCustCat());
					segmentInfo.setDataUploadType(smrData.getFollowUpType());
					segmentInfo.setCustomer(vehicleUpdate.get(0).getCustomer());
					em.persist(segmentInfo);
				}

				long phonecount = source.phones(em).where(u -> u.getCustomer().getId() == custId).count();
				if (phonecount > 0) {
					logger.info("phone count SMR:" + phonecount + "custId : " + custId);
					List<Phone> phoneList = source.phones(em).where(u -> u.getCustomer().getId() == custId).toList();

					for (Phone phone_list : phoneList) {

						phone_list.setIsPreferredPhone(false);
						em.merge(phone_list);

					}
					Phone smrPhoneData = new Phone();
					smrPhoneData.setPhoneNumber(smrData.getMobile());
					smrPhoneData.setCustomer(vehicleUpdate.get(0).getCustomer());
					smrPhoneData.setUpload_id(upload_id);
					smrPhoneData.setIsPreferredPhone(true);
					em.merge(smrPhoneData);

				} else {

					Phone smrPhoneData = new Phone();
					smrPhoneData.setPhoneNumber(smrData.getMobile());
					smrPhoneData.setCustomer(vehicleUpdate.get(0).getCustomer());
					smrPhoneData.setUpload_id(upload_id);
					smrPhoneData.setIsPreferredPhone(true);
					em.merge(smrPhoneData);

				}

				long emailCount = source.emails(em).where(u -> u.getCustomer().getId() == custId).count();
				if (emailCount > 0) {
					logger.info("phone count SMR:" + phonecount + "custId : " + custId);
					List<Email> emailList = source.emails(em).where(u -> u.getCustomer().getId() == custId).toList();

					for (Email email_list : emailList) {

						email_list.setIsPreferredEmail(false);
						em.merge(email_list);

					}
					Email smrEmailData = new Email();
					smrEmailData.setEmailAddress(smrData.getCustEmailIdStatus());
					smrEmailData.setCustomer(vehicleUpdate.get(0).getCustomer());
					smrEmailData.setUpload_id(upload_id);
					smrEmailData.setIsPreferredEmail(true);

					em.persist(smrEmailData);
				} else {
					Email smrEmailData = new Email();
					smrEmailData.setEmailAddress(smrData.getCustEmailIdStatus());
					smrEmailData.setCustomer(vehicleUpdate.get(0).getCustomer());

					smrEmailData.setUpload_id(upload_id);
					smrEmailData.setIsPreferredEmail(true);

					em.persist(smrEmailData);
				}
				long addressCount = source.address(em).where(u -> u.getCustomer().getId() == custId).count();
				if (addressCount > 0) {
					logger.info("phone count SMR:" + phonecount + "custId : " + custId);
					List<Address> addressList = source.address(em).where(u -> u.getCustomer().getId() == custId)
							.toList();

					for (Address address_list : addressList) {

						address_list.setIsPreferred(false);
						em.merge(address_list);

					}
					Address smrAddressData = new Address();
					logger.info("smrAddressData" + smrAddressData);
					smrAddressData.setConcatenatedAdress(smrData.getAddress());
					smrAddressData.setAddressLine1(smrData.getAddress());
					smrAddressData.setCustomer(vehicleUpdate.get(0).getCustomer());
					smrAddressData.setUpload_id(upload_id);
					smrAddressData.setIsPreferred(true);

					em.persist(smrAddressData);
				} else {
					Address smrAddressData = new Address();
					logger.info("smrAddressData" + smrAddressData);
					smrAddressData.setConcatenatedAdress(smrData.getAddress());
					smrAddressData.setAddressLine1(smrData.getAddress());
					smrAddressData.setCustomer(vehicleUpdate.get(0).getCustomer());
					smrAddressData.setUpload_id(upload_id);
					smrAddressData.setIsPreferred(true);
					smrAddressData.setAddressType(Address.ADDRESS_TYPE_OFFICE);
					em.persist(smrAddressData);
				}

				if (uploadSuccessRecord.containsKey(smrData.getFollowUpNew())) {

					int vlaueOfKeyis = uploadSuccessRecord.get(smrData.getFollowUpNew());
					vlaueOfKeyis++;

					uploadSuccessRecord.put(smrData.getFollowUpNew(), vlaueOfKeyis);

				} else {

					uploadSuccessRecord.put(smrData.getFollowUpNew(), 1);

				}

			} else {

				Customer smrCustData = new Customer();
				smrCustData.setCustomerName(smrData.getCustName());
				// smrCustData.setUploadData(uploadLink);
				smrCustData.setUpload_id(upload_id);

				logger.info("smrData" + smrData.getCustName());

				em.persist(smrCustData);

				Vehicle smrVehData = new Vehicle();
				smrVehData.setVehicleRegNo(removeSpclChar(smrData.getRegNo()));
				smrVehData.setModel(smrData.getModel());
				smrVehData.setFuelType(smrData.getFuelType());
				smrVehData.setChassisNo(smrData.getChassis());
				smrVehData.setOdometerReading(String.valueOf(smrData.getMileage()));
				smrVehData.setNextServicedate(smrData.getDueDate());
				smrVehData.setNextServicetype(smrData.getDueService());
				smrVehData.setLastServiceDate(smrData.getLastServiceDate());
				smrVehData.setLastServiceType(smrData.getLastServiceType());
				smrVehData.setSaleDate(smrData.getSaleDate());
				logger.info("lst service dfate" + smrData.getLastServiceDate());
				logger.info("lst service Due date" + smrData.getSaleDate());
				smrVehData.setExtendedWarentyDue(smrData.getExtWarranty());
				smrVehData.setFollowUpDateSMR(smrData.getFollowUpNew());
				smrVehData.setDeliveryDate_Sale(smrData.getDeliveryDate());
				smrVehData.setIsAssigned("false");
				smrVehData.setCustomer(smrCustData);
				smrVehData.setUpload_id(upload_id);

				smrVehData.setOdometerReading(String.valueOf(smrData.getMileage()));

				em.persist(smrVehData);

				Segment smrSegmentData = new Segment();
				smrSegmentData.setName(smrData.getCustCat());
				smrSegmentData.setDataUploadType(smrData.getFollowUpType());
				smrSegmentData.setCustomer(smrCustData);

				em.persist(smrSegmentData);

				Address smrAddressData = new Address();
				smrAddressData.setConcatenatedAdress(smrData.getAddress());
				smrAddressData.setAddressLine1(smrData.getAddress());
				smrAddressData.setCustomer(smrCustData);
				smrAddressData.setUpload_id(upload_id);
				smrAddressData.setIsPreferred(true);

				em.persist(smrAddressData);

				Phone smrPhoneData = new Phone();
				smrPhoneData.setPhoneNumber(smrData.getMobile());
				smrPhoneData.setCustomer(smrCustData);
				smrPhoneData.setUpload_id(upload_id);
				smrPhoneData.setIsPreferredPhone(true);
				em.persist(smrPhoneData);

				Email smrEmailData = new Email();
				smrEmailData.setEmailAddress(smrData.getCustEmailIdStatus());
				smrEmailData.setCustomer(smrCustData);
				smrEmailData.setUpload_id(upload_id);
				smrEmailData.setIsPreferredEmail(true);

				em.persist(smrEmailData);

				logger.info("fourth method");

				if (uploadSuccessRecord.containsKey(smrData.getFollowUpNew())) {

					int vlaueOfKeyis = uploadSuccessRecord.get(smrData.getFollowUpNew());
					vlaueOfKeyis++;

					uploadSuccessRecord.put(smrData.getFollowUpNew(), vlaueOfKeyis);

				} else {

					uploadSuccessRecord.put(smrData.getFollowUpNew(), 1);

				}
			}
		} else {

			if (uploadRejectedRecord.containsKey(smrData.getFollowUpNew())) {

				int vlaueOfKeyis = uploadRejectedRecord.get(smrData.getFollowUpNew());
				vlaueOfKeyis++;

				uploadRejectedRecord.put(smrData.getFollowUpNew(), vlaueOfKeyis);

			} else {

				uploadRejectedRecord.put(smrData.getFollowUpNew(), 1);

			}

			Map<String, String> map = smrData.getErrorInfo();

			String[] listOfcolumns = new String[60];

			int i = 0;

			for (Map.Entry<String, String> entry : map.entrySet()) {
				// logger.info("add data to String array");
				listOfcolumns[i] = entry.getKey();
				i++;

			}

			SMRDataError smrDataerror = new SMRDataError();
			BeanUtils.copyProperties(smrData, smrDataerror, listOfcolumns);

			String errorAppendVariable = "";

			for (Map.Entry<String, String> entry : map.entrySet()) {
				System.out.println("Key for error is = " + entry.getKey() + ", Value = " + entry.getValue());

				String expeError = entry.getKey() + "|" + entry.getValue();

				if (errorAppendVariable.equals("")) {

					errorAppendVariable = expeError;

				} else {

					errorAppendVariable = errorAppendVariable + "," + expeError;
				}

				smrDataerror.setErrorInformation(errorAppendVariable);

			}
			smrDataerror.setUpload_id(upload_id);
			em.persist(smrDataerror);

			//

		}

	}

	private void insertCampaignData(Object excelData, String upload_id, String campaignExpiry,
			HashMap<Date, Integer> uploadSuccessRecord, HashMap<Date, Integer> uploadRejectedRecord) {

		logger.info("campaign method");
		CampaignExcel campaign = (CampaignExcel) excelData;

		logger.info("row number data : " + campaign.getRowNumber());
		logger.info("is Error data : " + campaign.isError());
		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date toDateNewDate = new Date();
		try {
			toDateNewDate = dmyFormat.parse(campaignExpiry);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (!campaign.isError()) {

			String vehReg = campaign.getVeh_reg_no();
			String chassiNo = campaign.getChassis();

			long countOfExistingVehicle = source.vehicle(em).where(u -> u.getVehicleRegNo() != null)
					.where(u -> u.getChassisNo() != null)
					.where(u -> u.getVehicleRegNo().equals(vehReg) && u.getChassisNo().equals(chassiNo)).count();

			logger.info(" countOfExistingVehicle : " + countOfExistingVehicle);

			// uploadLink = getUploadDataLinkByRecordedDate(toDateNewDate,
			// uploadLink);

			if (countOfExistingVehicle > 0) {

				logger.info("this vehicle is already existing");

				Vehicle vehicaldata = source.vehicle(em).where(u -> u.getVehicleRegNo() != null)
						.where(u -> u.getChassisNo() != null)
						.where(u -> u.getVehicleRegNo().equals(vehReg) && u.getChassisNo().equals(chassiNo))
						.getOnlyValue();

				vehicaldata.setFollowUpDateSMR(toDateNewDate);
				vehicaldata.setIsAssigned("false");
				vehicaldata.setUpload_id(upload_id);

				Customer customerData = vehicaldata.getCustomer();

				customerData.setCustomerName(campaign.getCustomerName());
				customerData.setUpload_id(upload_id);
				long custId = customerData.getId();

				long phonecount = source.phones(em).where(u -> u.getCustomer() != null)
						.where(u -> u.getCustomer().getId() == custId).count();
				if (phonecount > 0) {
					logger.info("phone count SMR:" + phonecount + "custId : " + custId);

					String phone_number = campaign.getMobile1();

					long countOfphonelist = source.phones(em).where(u -> u.getCustomer() != null)
							.where(u -> u.getCustomer().getId() == custId)
							.where(u -> u.getPhoneNumber().equals(phone_number)).count();

					if (countOfphonelist == 0) {
						List<Phone> phoneList = source.phones(em).where(u -> u.getCustomer() != null)
								.where(u -> u.getCustomer().getId() == custId).toList();

						for (Phone phone_list : phoneList) {

							phone_list.setIsPreferredPhone(false);
							em.merge(phone_list);

						}
						Phone smrPhoneData = new Phone();
						smrPhoneData.setPhoneNumber(campaign.getMobile1());
						smrPhoneData.setCustomer(customerData);
						smrPhoneData.setUpload_id(upload_id);
						smrPhoneData.setIsPreferredPhone(true);
						em.merge(smrPhoneData);
					}

				} else {

					Phone smrPhoneData = new Phone();
					smrPhoneData.setPhoneNumber(campaign.getMobile1());
					smrPhoneData.setCustomer(customerData);
					smrPhoneData.setUpload_id(upload_id);
					smrPhoneData.setIsPreferredPhone(true);
					em.merge(smrPhoneData);

				}

				em.merge(vehicaldata);
				em.merge(customerData);

				if (uploadSuccessRecord.containsKey(toDateNewDate)) {

					int vlaueOfKeyis = uploadSuccessRecord.get(toDateNewDate);
					vlaueOfKeyis++;

					uploadSuccessRecord.put(toDateNewDate, vlaueOfKeyis);

				} else {

					uploadSuccessRecord.put(toDateNewDate, 1);

				}

			} else {

				Customer cust_data = new Customer();
				cust_data.setCustomerEmail(campaign.getEmail_id());
				cust_data.setCustomerName(campaign.getCustomerName());
				cust_data.setSalutation(campaign.getSalutation());
				cust_data.setSourceType(campaign.getSource());
				cust_data.setUpload_id(upload_id);

				Vehicle vehData = new Vehicle();
				vehData.setChassisNo(campaign.getChassis());
				vehData.setColor(campaign.getColorDesc());
				vehData.setEngineNo(campaign.getEngine());
				vehData.setFuelType(campaign.getVehicle_types());
				vehData.setVariant(campaign.getVaraintDesc());
				vehData.setVehicleRegNo(removeSpclChar(campaign.getVeh_reg_no()));
				vehData.setMobile1(campaign.getMobile1());
				vehData.setMobile2(campaign.getMobile2());
				vehData.setCity(campaign.getCity());
				vehData.setPincode(campaign.getPincode());
				vehData.setEnquirySource(campaign.getSource());

				vehData.setFollowUpDateSMR(toDateNewDate);
				vehData.setIsAssigned("false");
				vehData.setCustomer(cust_data);
				vehData.setUpload_id(upload_id);

				Address address_data = new Address();
				address_data.setAddressLine1(campaign.getAddress1());
				address_data.setAddressLine2(campaign.getAddress2());
				address_data.setAddressLine3(campaign.getAddress3());
				String add1 = address_data.getAddressLine1();
				String add2 = address_data.getAddressLine2();
				String add3 = address_data.getAddressLine3();
				String add = add1 + "," + add2 + "," + add3;
				address_data.setConcatenatedAdress(add);
				address_data.setCity(campaign.getCity());
				address_data.setIsPreferred(true);
				address_data.setPincode(Long.parseLong(campaign.getPincode()));
				address_data.setCustomer(cust_data);
				address_data.setUpload_id(upload_id);
				Segment segment_Data = new Segment();
				segment_Data.setDataUploadType("Campaign");
				segment_Data.setCustomer(cust_data);

				Phone phoneData = new Phone();
				phoneData.setPhoneNumber(campaign.getMobile1());
				phoneData.setIsPreferredPhone(true);
				phoneData.setCustomer(cust_data);
				phoneData.setUpload_id(upload_id);

				Email mail = new Email();
				mail.setEmailAddress(campaign.getEmail_id());
				mail.setIsPreferredEmail(true);
				mail.setCustomer(cust_data);
				mail.setUpload_id(upload_id);

				em.persist(mail);
				em.persist(phoneData);
				em.persist(cust_data);
				em.persist(vehData);
				em.persist(address_data);
				em.persist(segment_Data);

				if (uploadSuccessRecord.containsKey(toDateNewDate)) {

					int vlaueOfKeyis = uploadSuccessRecord.get(toDateNewDate);
					vlaueOfKeyis++;

					uploadSuccessRecord.put(toDateNewDate, vlaueOfKeyis);

				} else {

					uploadSuccessRecord.put(toDateNewDate, 1);

				}
			}

		}

		else {

			if (uploadRejectedRecord.containsKey(toDateNewDate)) {

				int vlaueOfKeyis = uploadRejectedRecord.get(toDateNewDate);
				vlaueOfKeyis++;

				uploadRejectedRecord.put(toDateNewDate, vlaueOfKeyis);

			} else {

				uploadRejectedRecord.put(toDateNewDate, 1);

			}

			Map<String, String> map = campaign.getErrorInfo();

			String[] listOfcolumns = new String[60];

			int i = 0;

			for (Map.Entry<String, String> entry : map.entrySet()) {
				// logger.info("add data to String array");
				listOfcolumns[i] = entry.getKey();
				i++;

			}

			CampaignExcelError campaignDataerror = new CampaignExcelError();
			BeanUtils.copyProperties(campaign, campaignDataerror, listOfcolumns);

			String errorAppendVariable = "";

			for (Map.Entry<String, String> entry : map.entrySet()) {
				System.out.println("Key for error is = " + entry.getKey() + ", Value = " + entry.getValue());

				String expeError = entry.getKey() + "|" + entry.getValue();

				if (errorAppendVariable.equals("")) {

					errorAppendVariable = expeError;

				} else {

					errorAppendVariable = errorAppendVariable + "," + expeError;
				}

				campaignDataerror.setErrorInformation(errorAppendVariable);

			}
			campaignDataerror.setUpload_id(upload_id);
			em.persist(campaignDataerror);

			//

		}
	}

	private void insertInsuranceData(Object excelData, String upload_id, String campaignExpiry,
			HashMap<Date, Integer> uploadSuccessRecord, HashMap<Date, Integer> uploadRejectedRecord) {
				
		logger.info("InsuranceExcel method");
		InsuranceExcel insurance = (InsuranceExcel) excelData;

		logger.info("row number data : " + insurance.getRowNumber());
		logger.info("is Error data : " + insurance.isError());
		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date toDateNewDate = new Date();
		try {
			toDateNewDate = dmyFormat.parse(campaignExpiry);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (!insurance.isError()) {
			
			logger.info("success");

			InsuranceExcelData insuranceDataExcel = new InsuranceExcelData();
			BeanUtils.copyProperties(insurance, insuranceDataExcel);
			
			
			insuranceDataExcel.setUpload_id(upload_id);
			em.persist(insuranceDataExcel);

			

				if (uploadSuccessRecord.containsKey(toDateNewDate)) {

					int vlaueOfKeyis = uploadSuccessRecord.get(toDateNewDate);
					vlaueOfKeyis++;

					uploadSuccessRecord.put(toDateNewDate, vlaueOfKeyis);

				} else {

					uploadSuccessRecord.put(toDateNewDate, 1);

				}	
				
				
				

		}else {
			
			logger.info("failure");

			if (uploadRejectedRecord.containsKey(toDateNewDate)) {

				int vlaueOfKeyis = uploadRejectedRecord.get(toDateNewDate);
				vlaueOfKeyis++;

				uploadRejectedRecord.put(toDateNewDate, vlaueOfKeyis);

			} else {

				uploadRejectedRecord.put(toDateNewDate, 1);

			}

		
		
		Map<String, String> map = insurance.getErrorInfo();

		String[] listOfcolumns = new String[60];

		int i = 0;

		for (Map.Entry<String, String> entry : map.entrySet()) {
			logger.info("add data to String array : "+listOfcolumns[i]);
			listOfcolumns[i] = entry.getKey();
			i++;

		}

		InsuranceExcelError insuranceDataerror = new InsuranceExcelError();
		BeanUtils.copyProperties(insurance, insuranceDataerror, listOfcolumns);

		String errorAppendVariable = "";

		for (Map.Entry<String, String> entry : map.entrySet()) {
			System.out.println("Key for error is = " + entry.getKey() + ", Value = " + entry.getValue());

			String expeError = entry.getKey() + "|" + entry.getValue();

			if (errorAppendVariable.equals("")) {

				errorAppendVariable = expeError;

			} else {

				errorAppendVariable = errorAppendVariable + "," + expeError;
			}

			insuranceDataerror.setErrorInformation(errorAppendVariable);

		}
		insuranceDataerror.setUpload_id(upload_id);
		em.persist(insuranceDataerror);
		
	}
		
	}
	
	
	
	
	public String removeSpclChar(String str) {
		String temp = str;
		if (temp != null) {
			temp = str.trim();
			if (temp.substring(0, 1).equals("$") || temp.substring(0, 1).equals("#")) {
				temp = temp.substring(1).trim();
			}

		}
		return temp;
	}

	@Override
	public void addTheUplodedExcelData(UploadData insertInUpload) {

		int totalRecordPresent = insertInUpload.getRejectedRecords() + insertInUpload.getSuccessRecords();
		insertInUpload.setTotalRecords(totalRecordPresent);

		em.persist(insertInUpload);

	}

	@Override
	public List<String> getExcelTotalAndRejectedCounts(String upload_id) {

		long countOfSuccess = source.uploadDatas(em).where(u -> u.getUpload_id().equals(upload_id))
				.sumInteger(s -> s.getSuccessRecords());
		logger.info("countOfRej : " + countOfSuccess);

		long countOftotl = source.uploadDatas(em).where(u -> u.getUpload_id().equals(upload_id))
				.sumInteger(s -> s.getTotalRecords());

		logger.info("countOftotl : " + countOftotl);

		List<String> addRecordCountd = new ArrayList();
		addRecordCountd.add(String.valueOf(countOfSuccess));
		addRecordCountd.add(String.valueOf(countOftotl));

		return addRecordCountd;
	}
	
	@Override
	public long getCampaignIdByCamp(String campName, String typeData){
		
		Campaign camp=source.campaigns(em).where(u -> u.getCampaignType()!=null && u.getCampaignName()!=null)
		.where(u -> u.getCampaignType().equals(typeData) && u.getCampaignName().equals(campName))
		.getOnlyValue();
		
		return camp.getId();
	}
	
	@Override
	public void insertDataByStoreProcedure(String uploadId){		
		logger.info("store procedure call : "+uploadId);
		
		PreparedStatement cs = null;		
		Connection connection = null;
		try {
		DataSource datasource = DB.getDataSource();
		connection = datasource.getConnection();
		cs= connection.prepareStatement("call insert_ins_exceldata_to_resp_tables(?);");
		cs.setString(1, uploadId);
		cs.executeQuery();
		
   		
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			}
		finally
		 {
			 try {
				 if (cs != null) {
					 cs.close();
			 		}
			 	}
			 catch (Exception e) {
			 	};
			 try {
				 if (connection != null) {
					 if (!connection.isClosed()) {
						 connection.close();
					 }
				 	}
			 	}
			 catch (Exception e) {
			 };
		}
		
	}

	@Override
	public UploadData setRequiredFieldsForUploadInsurance(long userLoginId, String sheetname,
			String locationdata, Long workshopId, String finalName, String finalUploadId) {
		logger.info("Before Insert");

			WyzUser userIs = em.find(WyzUser.class, userLoginId);
	
			UploadData uploadNew = new UploadData();
			Location loc = source.locations(em).where(u -> u.getName().equals(locationdata)).getOnlyValue();
			Workshop workShopID = em.find(Workshop.class, workshopId);

			uploadNew.setDataType("Insurance");
			uploadNew.setSheetName(sheetname);
			uploadNew.setLocation(loc);
			uploadNew.setUploadedDate(currentDate());
			uploadNew.setUploadedDateAndTime(new Date());
			uploadNew.setWorkshop(workShopID);
			uploadNew.setFileNametimeStamp(finalName);			
			uploadNew.setWyzUser(userIs);
			uploadNew.setRejectedRecords(0);
			uploadNew.setTotalRecords(0);
			uploadNew.setSuccessRecords(0);
			uploadNew.setUpload_id(finalUploadId);
			logger.info("Inserted successfully");

			return uploadNew;
		

	}
	

	

	private void insertInsuranceDataHistory(Object excelData, String upload_id,
			HashMap<Date, Integer> uploadSuccessRecord, HashMap<Date, Integer> uploadRejectedRecord) {
		
		logger.info("Insurance history query data");		
		
		InsuranceExcel insurance = (InsuranceExcel) excelData;

		logger.info("row number data : " + insurance.getRowNumber());
		logger.info("is Error data : " + insurance.isError());
		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date toDateNewDate = new Date();
		String strDate = dmyFormat.format(toDateNewDate);
		try {
			toDateNewDate = dmyFormat.parse(strDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (!insurance.isError()) {
			
			logger.info("success");

			InsuranceExcelData insuranceDataExcel = new InsuranceExcelData();
			BeanUtils.copyProperties(insurance, insuranceDataExcel);
			
			
			insuranceDataExcel.setUpload_id(upload_id);
			em.persist(insuranceDataExcel);

			

				if (uploadSuccessRecord.containsKey(toDateNewDate)) {

					int vlaueOfKeyis = uploadSuccessRecord.get(toDateNewDate);
					vlaueOfKeyis++;

					uploadSuccessRecord.put(toDateNewDate, vlaueOfKeyis);

				} else {

					uploadSuccessRecord.put(toDateNewDate, 1);

				}	
				
				
				

		}else {
			
			logger.info("failure");

			if (uploadRejectedRecord.containsKey(toDateNewDate)) {

				int vlaueOfKeyis = uploadRejectedRecord.get(toDateNewDate);
				vlaueOfKeyis++;

				uploadRejectedRecord.put(toDateNewDate, vlaueOfKeyis);

			} else {

				uploadRejectedRecord.put(toDateNewDate, 1);

			}

		
		
		Map<String, String> map = insurance.getErrorInfo();

		String[] listOfcolumns = new String[60];

		int i = 0;

		for (Map.Entry<String, String> entry : map.entrySet()) {
			logger.info("add data to String array : "+listOfcolumns[i]);
			listOfcolumns[i] = entry.getKey();
			i++;

		}

		InsuranceExcelError insuranceDataerror = new InsuranceExcelError();
		BeanUtils.copyProperties(insurance, insuranceDataerror, listOfcolumns);

		String errorAppendVariable = "";

		for (Map.Entry<String, String> entry : map.entrySet()) {
			System.out.println("Key for error is = " + entry.getKey() + ", Value = " + entry.getValue());

			String expeError = entry.getKey() + "|" + entry.getValue();

			if (errorAppendVariable.equals("")) {

				errorAppendVariable = expeError;

			} else {

				errorAppendVariable = errorAppendVariable + "," + expeError;
			}

			insuranceDataerror.setErrorInformation(errorAppendVariable);

		}
		insuranceDataerror.setUpload_id(upload_id);
		em.persist(insuranceDataerror);
		
	}
		
	}
}
