package repositories;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import configs.JinqSource;
import controllers.webmodels.ChangeAssignment;
import models.AssignedInteraction;
import models.Campaign;
import models.InsuranceAssignedInteraction;
import models.PSFAssignedInteraction;
import models.WyzUser;
import play.Logger.ALogger;

@Repository("changeAssignmentRepository")
@Transactional
public class ChangeAssignmentRepositoryImpl implements ChangeAssignmentRepository {

	ALogger logger = play.Logger.of("application");
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Override
	public List<ChangeAssignment> filteredAssignDataList(String selected_cre, String selected_dispositions,
			String fromdate, String todate, String  campSelec,String campSelecType, long fromIndex, long toIndex) {
		// TODO Auto-generated method stub

		//Campaign camp = em.find(Campaign.class, campSelec);

		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDateNew = new Date();
		Date toDateNew = new Date();
		try {
			fromDateNew = dmyFormat.parse(fromdate);
			toDateNew = dmyFormat.parse(todate);
		} catch (Exception e) {
			e.printStackTrace();
		}

		javax.persistence.Query query = em.createNativeQuery(
				"CALL change_assignment_list(:users, :dispostion, :fromdate, :todate, :campaign_type,:campaign_name, :start_with, :length)",
				"ChangeAssignment");

		query.setParameter("users", selected_cre);
		query.setParameter("dispostion", selected_dispositions);
		query.setParameter("fromdate", fromDateNew);
		query.setParameter("todate", toDateNew);
		query.setParameter("campaign_type", campSelecType);
		query.setParameter("campaign_name", campSelec);
		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);

		return query.getResultList();
	}
	@Override
	public String getCampaignByID(long campaign_id) {
		// TODO Auto-generated method stub
		return source.campaigns(em).where(u -> u.getId() == campaign_id).select(u -> u.getCampaignName())
				.getOnlyValue();
	}

	@Override
	public String getDisositionById(Long dispoId) {
		// TODO Auto-generated method stub
		return source.callDispositionDatas(em).where(u -> u.getId() == dispoId).select(u -> u.getDisposition())
				.getOnlyValue();
	}

	@Override
	public void changeAssignedDataCalls(List<Long> callId, List<Long> campId, List<Long> userId) {

		int creSize = userId.size();
		int scheduledCallCount = callId.size();

		if (creSize != 0) {
			logger.info("count of CRE :" + creSize);

			int divCountRemainder = scheduledCallCount % creSize;

			logger.info("divCountRemainder of CRE and call from CRE Manager:" + divCountRemainder);

			int divCount = (scheduledCallCount) / (creSize);
			logger.info("divCount of CRE and call :" + divCount);

			int calls_count = 0;
			for (int i = 0; i < creSize; i++) {

				for (int j = calls_count; j < divCount + calls_count; j++) {

					long userdata = userId.get(i);
					long scall = callId.get(j);
					long campDataId = campId.get(j);

					assignCallByManagerForChange(userdata, scall, campDataId);
				}

				calls_count = divCount + calls_count;
				if (calls_count >= scheduledCallCount) {
					break;
				}

			}
			int call_countcompleted = divCount * creSize;
			logger.info("call_countcompleted : " + call_countcompleted);

			for (int i = 0; i < creSize; i++) {

				if (call_countcompleted < scheduledCallCount) {

					long userdata = userId.get(i);
					long scall = callId.get(call_countcompleted);
					long campDataId = campId.get(call_countcompleted);

					assignCallByManagerForChange(userdata, scall, campDataId);
					call_countcompleted++;

				} else {
					break;
				}

			}

		}

	}

	private void assignCallByManagerForChange(long userdata, long scall, long campDataId) {
		// TODO Auto-generated method stub
		WyzUser user = em.find(WyzUser.class, userdata);
		Campaign campaign = em.find(Campaign.class, campDataId);

		logger.info(" campaign.getCampaignType() : " + campaign.getCampaignType());
		if (campaign.getCampaignType().equals("PSF")) {

			PSFAssignedInteraction psfassign = em.find(PSFAssignedInteraction.class, scall);
			psfassign.setWyzUser(user);
			em.merge(psfassign);

		} else if (campaign.getCampaignType().equals("Insurance")) {
			InsuranceAssignedInteraction insAssign = em.find(InsuranceAssignedInteraction.class, scall);
			insAssign.setWyzUser(user);
			em.merge(insAssign);

		} else {
			AssignedInteraction assignInt = em.find(AssignedInteraction.class, scall);
			assignInt.setWyzUser(user);
			em.merge(assignInt);

		}

	}

	@Override
	public long filteredAssignDataListCount(String selected_cre, String selected_dispositions, String fromdate,
			String todate, String campSelec,String campSelecType) {
		// TODO Auto-generated method stub
		//Campaign camp = em.find(Campaign.class, campSelec);

		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDateNew = new Date();
		Date toDateNew = new Date();
		try {
			fromDateNew = dmyFormat.parse(fromdate);
			toDateNew = dmyFormat.parse(todate);
		} catch (Exception e) {
			e.printStackTrace();
		}

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("change_assignment_list_count");

		sQuery.registerStoredProcedureParameter("users", String.class,ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("dispostion", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("fromdate", Date.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("todate", Date.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("campaign_type", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("campaign_name", String.class, ParameterMode.IN);

		sQuery.setParameter("users", selected_cre);
		sQuery.setParameter("dispostion", selected_dispositions);
		sQuery.setParameter("fromdate", fromDateNew);
		sQuery.setParameter("todate", toDateNew);
		sQuery.setParameter("campaign_type", campSelecType);
		sQuery.setParameter("campaign_name", campSelec);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}
	@Override
	public String getLocationIdByName(String cityName) {
		// TODO Auto-generated method stub
		return source.locations(em).where(u -> u.getName().equals(cityName)).select(u -> u.getCityId()).getOnlyValue().toString();
	}
	@Override
	public List<Campaign> getCampaignNameBasedOnType(String campaignTypeId) {
		return source.campaigns(em).where(u -> u.getCampaignType().equals(campaignTypeId)).toList();
	}
}
