package repositories;

import java.util.List;

import javax.inject.Named;

import controllers.webmodels.ReportSummary;
import models.UploadReportFiles;

@Named
public interface ReportsRepository {

	List<ReportSummary> getUploadReportSummaryList(String cremanager,String uploadTypeIs, String fromDate, String toDate, long fromIndex,
			long toIndex);

	long getUploadReportSummaryListCount(String userName, String uploadTypeIs, String fromDate, String toDate);
	
	List<UploadReportFiles> uploadReportFilesList();

}
