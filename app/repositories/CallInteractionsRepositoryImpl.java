/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import configs.JinqSource;
import controllers.webmodels.AssignedListForCRE;
import controllers.webmodels.CustomerDataOnTabLoad;
import controllers.webmodels.FollowUpNotificationModel;
import controllers.webmodels.PSFFollowupNotificationModel;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import play.Logger.ALogger;
import play.db.DB;

import javax.persistence.*;
import javax.sql.DataSource;

import models.ServiceTypes;
import models.CallInteraction;
import models.ServiceBooked;
import models.Customer;
import models.SRDisposition;
import models.SavedSearchResult;
import models.PickupDrop;
import models.Role;
import models.Vehicle;
import models.VehicleSummaryCube;
import models.ServiceAdvisor;
import models.Workshop;
import models.Driver;
import models.UploadData;
import models.SMSTemplate;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Address;
import models.AssignedCallsReport;
import models.AssignedInteraction;
import models.CallDispositionData;
import models.CallHistoryCube;
import models.Campaign;
import models.JobCardDetailsUpload;
import models.ListingForm;
import models.NewCustomer;
import models.PSFAssignedInteraction;
import models.Service;
import models.UploadFileFields;
import models.UploadMasterFormat;
import models.WyzUser;
import models.workshopBill;
import models.Complaint;
import models.CustomerJson;
import models.DeletedStatusReport;
import models.Email;
import models.InsuranceCallHistoryCube;
import models.InsuranceDisposition;
import models.Location;
import models.Phone;
import models.Segment;
import models.TaggingUsers;
import models.UnAvailability;
import models.WorkshopSummary;
import models.ComplaintInteraction;
import models.ComplaintAssignedInteraction;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author W-885
 */
@Repository("callInteractionsRepository")
@Transactional
public class CallInteractionsRepositoryImpl implements CallInteractionsRepository {

	ALogger logger = play.Logger.of("application");

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Autowired
	private SMSTriggerRespository smsTriggerRepo;

	@Override
	public List<CallInteraction> getFollowUpCallsOfUser(String username, String dealercode) {

		List<String> vehical_numbers = source.vehicle(em).select(u -> u.getVehicleRegNo()).distinct().toList();

		List<CallInteraction> finalData = new ArrayList<CallInteraction>();

		for (String vehical_num : vehical_numbers) {

			List<CallInteraction> intData = source.callinteractions(em)
					// .where(k ->
					// k.getWyzUser().getUserName().equals(username))
					.where(k -> k.getVehicle().getVehicleRegNo().equals(vehical_num)).toList();

			Date tmp = null;
			int i = 0;
			if (intData.size() != 0) {

				for (CallInteraction uniqueNo_list : intData) {

					if (i == 0) {

						tmp = uniqueNo_list.getCallMadeDateAndTime();
						i++;
					} else {

						if (tmp.before(uniqueNo_list.getCallMadeDateAndTime())) {

							tmp = uniqueNo_list.getCallMadeDateAndTime();
							i++;
						}

					}

				}
				// logger.info("getting latest disposition");

				finalData.add(intData.get(i - 1));

			}
		}

		// logger.info("after sorting based on date : "+finalData.size());
		List<CallInteraction> finalDatanonContactsCall = new ArrayList<CallInteraction>();

		if (finalData.size() != 0) {

			for (CallInteraction addingData : finalData) {
				// logger.info("addingData.getSrdisposition().getCallDispositionData().getDispositionId()"+addingData.getSrdisposition().getCallDispositionData().getDispositionId()+""+addingData.getSrdisposition().getCallDispositionData().STATUS_CALL_ME_LATER);
				if (addingData.getSrdisposition() != null) {
					if (addingData.getSrdisposition().getCallDispositionData() != null) {

						if (addingData.getSrdisposition().getCallDispositionData()
								.getDispositionId() == (addingData.getSrdisposition()
										.getCallDispositionData().STATUS_CALL_ME_LATER)
								&& addingData.getWyzUser().getUserName().equals(username)) {

							finalDatanonContactsCall.add(addingData);

						}
					}
				}

			}

		}
		for (CallInteraction assdata : finalDatanonContactsCall) {

			Hibernate.initialize(assdata.getCustomer().getPhones());

		}

		logger.info("interaction userss: " + finalDatanonContactsCall.size());

		return finalDatanonContactsCall;
	}

	@Override
	public List<CallInteraction> getServicBookedCalls(String username, String dealercode) {

		List<String> vehical_numbers = source.vehicle(em).select(u -> u.getVehicleRegNo()).distinct().toList();
		// logger.info("vehical_numbers
		// getServicBookedCalls:"+vehical_numbers.size());
		List<CallInteraction> finalData = new ArrayList<CallInteraction>();

		for (String vehical_num : vehical_numbers) {
			List<CallInteraction> serviceBookedcalls = source.callinteractions(em)
					.where(c -> c.getWyzUser().getUserName().equals(username))
					.where(c -> c.getVehicle().getVehicleRegNo().equals(vehical_num)).toList();
			// logger.info(" serviceBookedcalls:"+serviceBookedcalls.size());
			Date tmp = null;
			int i = 0;
			if (serviceBookedcalls.size() != 0) {

				for (CallInteraction uniqueNo_list : serviceBookedcalls) {

					if (i == 0) {

						tmp = uniqueNo_list.getCallMadeDateAndTime();
						i++;
					} else {

						if (tmp.before(uniqueNo_list.getCallMadeDateAndTime())) {

							tmp = uniqueNo_list.getCallMadeDateAndTime();
							i++;
						}

					}

				}
				// logger.info("getServicBookedCalls getting latest
				// disposition");

				finalData.add(serviceBookedcalls.get(i - 1));

			}
		}
		List<CallInteraction> serviceBookedcalls = new ArrayList<CallInteraction>();
		if (finalData.size() != 0) {
			for (CallInteraction addingData : finalData) {
				if (addingData.getSrdisposition().getCallDispositionData().getDispositionId() == (addingData
						.getSrdisposition().getCallDispositionData().STATUS_BOOK_MY_SERVICE)) {

					serviceBookedcalls.add(addingData);

				}

			}

		}
		for (CallInteraction assdata : serviceBookedcalls) {

			Hibernate.initialize(assdata.getCustomer().getPhones());

		}
		// logger.info("total call of service booked is
		// :"+serviceBookedcalls.size());
		return serviceBookedcalls;

	}

	@Override
	public List<CallInteraction> getNonContactsCalls(String username, String dealercode) {
		List<String> vehical_numbers = source.vehicle(em).select(u -> u.getVehicleRegNo()).distinct().toList();
		List<CallInteraction> finalData = new ArrayList<CallInteraction>();

		for (String vehical_num : vehical_numbers) {

			List<CallInteraction> nonContactscalls = source.callinteractions(em)
					.where(c -> c.getWyzUser().getUserName().equals(username))
					.where(c -> c.getVehicle().getVehicleRegNo().equals(vehical_num)).toList();
			Date tmp = null;
			int i = 0;
			if (nonContactscalls.size() != 0) {

				for (CallInteraction uniqueNo_list : nonContactscalls) {

					if (i == 0) {

						tmp = uniqueNo_list.getCallMadeDateAndTime();
						i++;
					} else {

						if (tmp.before(uniqueNo_list.getCallMadeDateAndTime())) {

							tmp = uniqueNo_list.getCallMadeDateAndTime();
							i++;
						}

					}

				}

				finalData.add(nonContactscalls.get(i - 1));

			}

		}

		List<CallInteraction> nonContactscallsdata = new ArrayList<CallInteraction>();
		if (finalData.size() != 0) {
			for (CallInteraction addingData : finalData) {
				if (addingData.srdisposition != null) {

					if (addingData.getDroppedCount() < 4) {

						if (addingData.srdisposition.getCallDispositionData()
								.getDispositionId() == (addingData.srdisposition
										.getCallDispositionData().STATUS_RINGING_NO_RESPONSE)) {

							nonContactscallsdata.add(addingData);

						} else if (addingData.srdisposition.getCallDispositionData()
								.getDispositionId() == (addingData.srdisposition
										.getCallDispositionData().STATUS_INVALID_NUMBER)) {

							nonContactscallsdata.add(addingData);

						} else if (addingData.srdisposition.getCallDispositionData()
								.getDispositionId() == (addingData.srdisposition
										.getCallDispositionData().STATUS_BUSY)) {

							nonContactscallsdata.add(addingData);

						} else if (addingData.srdisposition.getCallDispositionData()
								.getDispositionId() == (addingData.srdisposition
										.getCallDispositionData().STATUS_OTHERS)) {

							nonContactscallsdata.add(addingData);

						} else if (addingData.srdisposition.getCallDispositionData()
								.getDispositionId() == (addingData.srdisposition
										.getCallDispositionData().STATUS_SWITCHED_OFF_UNREACHABLE)) {

							nonContactscallsdata.add(addingData);

						} else {
							// logger.info("no
							// info"+addingData.srdisposition.getCallDispositionData().getDisposition());
						}
					}
				}
			}
		}
		for (CallInteraction assdata : nonContactscallsdata) {

			Hibernate.initialize(assdata.getCustomer().getPhones());

		}
		return nonContactscallsdata;
	}

	@Override
	public List<CallInteraction> getDroppedCalls(String username, String dealercode) {
		// List<String> vehical_numbers=source.vehicle(em).select(u ->
		// u.getVehicleRegNo()).distinct().toList();
		// logger.info("vehical_numbers :"+vehical_numbers.size());
		// List<CallInteraction> finalData=new ArrayList<CallInteraction>();
		//
		//// String data1=getDispositionValueById("6");
		//// String data2=getDispositionValueById("9");
		//// String data3=getDispositionValueById("7");
		//// String data4=getDispositionValueById("10");
		//// String data5=getDispositionValueById("8");
		//
		// for(String vehical_num:vehical_numbers){
		//
		// List<CallInteraction> callinfoDump=source.callinteractions(em)
		// .where(k -> k.getWyzUser().getUserName().equals(username))
		// .where(k -> k.getVehicle().getVehicleRegNo().equals(vehical_num))
		// .where(k ->
		// k.getSrdisposition().getCallDispositionData().getDispositionId()==k.getSrdisposition().getCallDispositionData().STATUS_RINGING_NO_RESPONSE
		// ||
		// k.getSrdisposition().getCallDispositionData().getDispositionId()==k.getSrdisposition().getCallDispositionData().STATUS_INVALID_NUMBER
		// ||
		// k.getSrdisposition().getCallDispositionData().getDispositionId()==k.getSrdisposition().getCallDispositionData().STATUS_BUSY
		// ||
		// k.getSrdisposition().getCallDispositionData().getDispositionId()==k.getSrdisposition().getCallDispositionData().STATUS_OTHERS
		// ||
		// k.getSrdisposition().getCallDispositionData().getDispositionId()==k.getSrdisposition().getCallDispositionData().STATUS_SWITCHED_OFF_UNREACHABLE)
		// .toList();
		//
		//
		// logger.info("dropped count is size is:"+callinfoDump.size());
		// Date tmp=null;
		// int i=0;
		// if(callinfoDump.size()!=0){
		// for(CallInteraction uniqueNo_list:callinfoDump){
		// if(i==0){
		// tmp=uniqueNo_list.getCallMadeDateAndTime();
		// i++;
		// }else{
		// if(tmp.before(uniqueNo_list.getCallMadeDateAndTime())){
		// tmp=uniqueNo_list.getCallMadeDateAndTime();
		// i++;
		// }
		// }
		// }
		// finalData.add(callinfoDump.get(i-1));
		// }
		// }
		List<String> vehical_numbers = source.vehicle(em).select(u -> u.getVehicleRegNo()).distinct().toList();
		List<CallInteraction> finalData = new ArrayList<CallInteraction>();

		for (String vehical_num : vehical_numbers) {

			List<CallInteraction> nonContactscalls = source.callinteractions(em)
					.where(c -> c.getWyzUser().getUserName().equals(username))
					.where(c -> c.getVehicle().getVehicleRegNo().equals(vehical_num)).toList();
			Date tmp = null;
			int i = 0;
			if (nonContactscalls.size() != 0) {

				for (CallInteraction uniqueNo_list : nonContactscalls) {

					if (i == 0) {

						tmp = uniqueNo_list.getCallMadeDateAndTime();
						i++;
					} else {

						if (tmp.before(uniqueNo_list.getCallMadeDateAndTime())) {

							tmp = uniqueNo_list.getCallMadeDateAndTime();
							i++;
						}

					}

				}

				finalData.add(nonContactscalls.get(i - 1));

			}

		}

		List<CallInteraction> nonContactscallsdata = new ArrayList<CallInteraction>();
		if (finalData.size() != 0) {
			for (CallInteraction addingData : finalData) {
				if (addingData.srdisposition != null) {

					if (addingData.getDroppedCount() > 3) {

						if (addingData.srdisposition.getCallDispositionData()
								.getDispositionId() == (addingData.srdisposition
										.getCallDispositionData().STATUS_RINGING_NO_RESPONSE)) {

							nonContactscallsdata.add(addingData);

						} else if (addingData.srdisposition.getCallDispositionData()
								.getDispositionId() == (addingData.srdisposition
										.getCallDispositionData().STATUS_INVALID_NUMBER)) {

							nonContactscallsdata.add(addingData);

						} else if (addingData.srdisposition.getCallDispositionData()
								.getDispositionId() == (addingData.srdisposition
										.getCallDispositionData().STATUS_BUSY)) {

							nonContactscallsdata.add(addingData);

						} else if (addingData.srdisposition.getCallDispositionData()
								.getDispositionId() == (addingData.srdisposition
										.getCallDispositionData().STATUS_OTHERS)) {

							nonContactscallsdata.add(addingData);

						} else if (addingData.srdisposition.getCallDispositionData()
								.getDispositionId() == (addingData.srdisposition
										.getCallDispositionData().STATUS_SWITCHED_OFF_UNREACHABLE)) {

							nonContactscallsdata.add(addingData);

						} else {
							// logger.info("no
							// info"+addingData.srdisposition.getCallDispositionData().getDisposition());
						}
					}
				}
			}
		}

		List<CallInteraction> finalDataForDroppedBucket = new ArrayList<CallInteraction>();
		for (CallInteraction dropData : nonContactscallsdata) {
			if (dropData.getDroppedCount() != 0) {
				if (dropData.getDroppedCount() >= 4) {

					finalDataForDroppedBucket.add(dropData);

				}

			}
		}
		for (CallInteraction assdata : finalDataForDroppedBucket) {

			Hibernate.initialize(assdata.getCustomer().getPhones());

		}

		return finalDataForDroppedBucket;

	}

	@Override
	public List<CallInteraction> getServiceNotRequiredCalls(String username, String dealercode) {
		List<String> vehical_numbers = source.vehicle(em).select(u -> u.getVehicleRegNo()).distinct().toList();
		// logger.info("vehical_numbers :"+vehical_numbers.size());
		List<CallInteraction> finalData = new ArrayList<CallInteraction>();

		for (String vehical_num : vehical_numbers) {
			List<CallInteraction> serviceNotRequiredData = source.callinteractions(em)
					.where(c -> c.getWyzUser().getUserName().equals(username))
					.where(c -> c.getVehicle().getVehicleRegNo().equals(vehical_num)).toList();
			Date tmp = null;
			int i = 0;
			if (serviceNotRequiredData.size() != 0) {
				for (CallInteraction uniqueNo_list : serviceNotRequiredData) {
					if (i == 0) {
						tmp = uniqueNo_list.getCallMadeDateAndTime();
						i++;
					} else {
						if (tmp.before(uniqueNo_list.getCallMadeDateAndTime())) {
							tmp = uniqueNo_list.getCallMadeDateAndTime();
							i++;
						}
					}
				}
				// logger.info("getting latest disposition");
				finalData.add(serviceNotRequiredData.get(i - 1));
			}
		}
		List<CallInteraction> serviceNotRequiredcalls = new ArrayList<CallInteraction>();
		if (finalData.size() != 0) {
			for (CallInteraction addingData : finalData) {
				if (addingData.getSrdisposition().getCallDispositionData().getDispositionId() == (addingData
						.getSrdisposition().getCallDispositionData().STATUS_SERVICE_NOT_REQUIRED)) {
					serviceNotRequiredcalls.add(addingData);
				}
			}
		}
		for (CallInteraction assdata : serviceNotRequiredcalls) {

			Hibernate.initialize(assdata.getCustomer().getPhones());

		}
		return serviceNotRequiredcalls;
	}

	@Override
	public void updateTheMediaData(CallInteraction callinfo, String key, CustomerDataOnTabLoad customerinfo,
			DataSnapshot dataSnapshot) {
		int uniqueId = callinfo.getUniqueidForCallSync();
		
		
		Calendar c = Calendar.getInstance();
		c.setTime(new java.util.Date()); // Now use today date.
		c.add(Calendar.MINUTE, -20);
		
		Date currentDateIs=c.getTime();
		logger.info(" currentDateIs : "+currentDateIs);
		logger.info(" callinfo.getCallMadeDateAndTime() : "+callinfo.getCallMadeDateAndTime());
		/*if(currentDateIs.before(callinfo.getCallMadeDateAndTime())){
			
			logger.info(" Before");
			
		}
		
		if(currentDateIs.after(callinfo.getCallMadeDateAndTime())){
			
			logger.info(" After");
		}*/
		

		String username = callinfo.getAgentName();
		String dealercode = callinfo.getDealerCode();
		logger.info("updateTheMediaData : " + key);

		String query = "USE " + dealercode + ";";

		Session hibernateSession = em.unwrap(Session.class);

		try {
			hibernateSession.doWork(new org.hibernate.jdbc.Work() {

				@Override
				public void execute(Connection connection) {
					Statement smt;
					try {
						smt = connection.createStatement();

						Boolean resultOfExecution = smt.execute(query);

						long userExist = source.wyzUsers(em)
								.where(u -> u.getUserName().equals(username) && u.getDealerCode().equals(dealercode))
								.count();

						long keyExist = source.callinteractions(em).where(u -> u.getFirebaseKey() != null)
								.where(u -> u.getFirebaseKey().equals(key)).count();

						if (userExist != 0 && keyExist == 0) {

							logger.info("unique id to sync and user exist : " + uniqueId + " customerName : "
									+ customerinfo.getCustomerName() + " Customer phone"
									+ customerinfo.getCustomerPhone() + " dealercode : " + dealercode);

							WyzUser userLink = source.wyzUsers(em).where(
									u -> u.getUserName().equals(username) && u.getDealerCode().equals(dealercode))
									.getOnlyValue();

							if (uniqueId != 0) {

								List<CallInteraction> call_int = source.callinteractions(em)
										.where(c -> c.getUniqueidForCallSync() == uniqueId
												&& c.getDealerCode().equals(dealercode))
										.toList();

								if (call_int.size() != 0) {
									logger.info("updating call Interactio : " + dataSnapshot.getRef());
									CallInteraction call = call_int.get(0);
									
									long callid=call.getId();
									//call.setCallDate(callinfo.getCallDate());
									call.setCallDuration(callinfo.getCallDuration());
									//call.setCallTime(callinfo.getCallTime());
									call.setCallType(callinfo.getCallType());
									call.setDealerCode(callinfo.getDealerCode());
									//call.setCallMadeDateAndTime(callinfo.getCallMadeDateAndTime());
									
									call.setRingTime(callinfo.getRingTime());
									call.setMediaFileLob(callinfo.getMediaFileLob());
									call.setLatitude(callinfo.getLatitude());
									call.setLongitude(callinfo.getLongitude());
									call.setFilePath(callinfo.getFilePath());
									call.setFirebaseKey(key);
									call.setDailedNoIs(customerinfo.getCustomerPhone());
									em.merge(call);
									
									long callHistoryCubeCount=source.callHistoryCube(em).where( u -> u.getCall_interaction_id() == callid).count();
									
									if(callHistoryCubeCount > 0){
										
										CallHistoryCube callHistoryCube=source.callHistoryCube(em).where( u -> u.getCall_interaction_id() == callid).getOnlyValue();
										callHistoryCube.setCallDuration(callinfo.getCallDuration());
										callHistoryCube.setRingtime(callinfo.getRingTime());
										callHistoryCube.setCallDurationUpdated(true);
										callHistoryCube.setUpdatedDate(currentDateIs);
										em.merge(callHistoryCube);
										
										
									}
									
									long InsuranceCallHistoryCubeCount = source.insuranceCallHistoryCube(em)
											.where(u -> u.getCicallinteraction_id() == callid).count();
									if (InsuranceCallHistoryCubeCount > 0) {

										InsuranceCallHistoryCube insuranceCallHistoryCube = source
												.insuranceCallHistoryCube(em)
												.where(u -> u.getCicallinteraction_id() == callid).getOnlyValue();
										insuranceCallHistoryCube.setCallDuration(callinfo.getCallDuration());
										insuranceCallHistoryCube.setRingTime(callinfo.getRingTime());
										insuranceCallHistoryCube.setCallDurationUpdated(true);
										insuranceCallHistoryCube.setUpdatedDate(currentDateIs);
										em.merge(insuranceCallHistoryCube);
									}

									dataSnapshot.getRef().removeValue();

								}else{
									
									if(currentDateIs.after(callinfo.getCallMadeDateAndTime())){
										CallInteraction call = new CallInteraction();
										String phoneNumberIs=customerinfo.getCustomerPhone();
										
										long customerExistcount=source.phones(em)
												.where(u -> u.getPhoneNumber().equals(phoneNumberIs)).count();
										
										if(customerExistcount >0){
											List<Customer> customerIdExist=source.phones(em)
													.where(u -> u.getPhoneNumber().equals(phoneNumberIs))
													.select(u -> u.getCustomer()).toList();
											
											call.setCustomer(customerIdExist.get(0));
											Hibernate.initialize(customerIdExist.get(0).getVehicles());
											if(customerIdExist.get(0).getVehicles()!=null){
												call.setVehicle(customerIdExist.get(0).getVehicles().get(0));
												}
									}
										
										logger.info("updating call Interaction  : " + dataSnapshot.getRef()+" Unique Id : "+uniqueId);
										
										call.setUniqueidForCallSync(uniqueId);
										call.setCallDate(callinfo.getCallDate());
										call.setCallDuration(callinfo.getCallDuration());
										call.setCallTime(callinfo.getCallTime());
										call.setCallType(callinfo.getCallType());
										call.setDealerCode(callinfo.getDealerCode());
										call.setCallMadeDateAndTime(callinfo.getCallMadeDateAndTime());										
										call.setRingTime(callinfo.getRingTime());
										call.setMediaFileLob(callinfo.getMediaFileLob());
										call.setLatitude(callinfo.getLatitude());
										call.setLongitude(callinfo.getLongitude());
										call.setFilePath(callinfo.getFilePath());
										call.setFirebaseKey(key);
										call.setDailedNoIs(customerinfo.getCustomerPhone());
										call.setIsCallinitaited("Not Disposed");
										call.setWyzUser(userLink);
										em.merge(call);

										dataSnapshot.getRef().removeValue();
										
									}
									
								}
							} else {

								logger.info("incoiming and missing");
								CallInteraction callInsert = new CallInteraction();
								
								
								String phoneNumberIs=customerinfo.getCustomerPhone();
								long countOfNo=phoneNumberIs.length();
								boolean b=phoneNumberIs.startsWith("+91");	
								if(countOfNo>0){						
																	
									
									long customerPhoneExistcount=source.phones(em)
											.where(u -> u.getPhoneNumber().equals(phoneNumberIs)).count();
									
									if(customerPhoneExistcount >0){
										List<Customer> customerIdExist=source.phones(em)
												.where(u -> u.getPhoneNumber().equals(phoneNumberIs))
												.select(u -> u.getCustomer()).toList();
										
										callInsert.setCustomer(customerIdExist.get(0));
										Hibernate.initialize(customerIdExist.get(0).getVehicles());
										if(customerIdExist.get(0).getVehicles()!=null){
											callInsert.setVehicle(customerIdExist.get(0).getVehicles().get(0));
											}
								}else{
									
									if(b){
										long customerExistcount=source.phones(em)
												.where(u -> u.getPhoneNumber().equals(phoneNumberIs)).count();
										
										if(customerExistcount >0){
											List<Customer> customerIdExist=source.phones(em)
													.where(u -> u.getPhoneNumber().equals(phoneNumberIs))
													.select(u -> u.getCustomer()).toList();
											
											callInsert.setCustomer(customerIdExist.get(0));
											Hibernate.initialize(customerIdExist.get(0).getVehicles());
											if(customerIdExist.get(0).getVehicles()!=null){
												callInsert.setVehicle(customerIdExist.get(0).getVehicles().get(0));
												}
									}
										
									}else{
										String concNumber="+91"+phoneNumberIs;
										long customerExistcount=source.phones(em)
												.where(u -> u.getPhoneNumber().equals(phoneNumberIs)
														|| u.getPhoneNumber().equals(concNumber)).count();
										
										if(customerExistcount >0){
											List<Customer> customerIdExist=source.phones(em)
													.where(u -> u.getPhoneNumber().equals(phoneNumberIs)
															|| u.getPhoneNumber().equals(concNumber))
													.select(u -> u.getCustomer()).toList();
											
											callInsert.setCustomer(customerIdExist.get(0));
											Hibernate.initialize(customerIdExist.get(0).getVehicles());
											if(customerIdExist.get(0).getVehicles()!=null){
												callInsert.setVehicle(customerIdExist.get(0).getVehicles().get(0));
												}
									}
										
									}
								}
								}
								
								callInsert.setDailedNoIs(customerinfo.getCustomerPhone());
								callInsert.setCallDate(callinfo.getCallDate());
								callInsert.setCallDuration(callinfo.getCallDuration());
								callInsert.setRingTime(callinfo.getRingTime());
								callInsert.setCallTime(callinfo.getCallTime());
								callInsert.setCallType(callinfo.getCallType());
								callInsert.setDealerCode(callinfo.getDealerCode());
								callInsert.setCallMadeDateAndTime(callinfo.getCallMadeDateAndTime());
								callInsert.setMediaFileLob(callinfo.getMediaFileLob());
								callInsert.setLatitude(callinfo.getLatitude());
								callInsert.setLongitude(callinfo.getLongitude());
								callInsert.setFilePath(callinfo.getFilePath());
								callInsert.setWyzUser(userLink);
								callInsert.setFirebaseKey(key);
								callInsert.setIsCallinitaited(callinfo.getCallType());
								em.persist(callInsert);

								logger.info("incoiming and missing dataSnapshot.getRef() " + dataSnapshot.getRef());

								dataSnapshot.getRef().removeValue();

							}

						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						logger.info("SQL Exception Occured For : " + dealercode);
					}

				}
			});

		} catch (HibernateException e) {
			logger.info("Hibernate Exception Occured For : " + dealercode);
		}

	}

	@Override
	public void postEditOfCallInteractions(Customer customer, CallInteraction call_interaction,
			ServiceBooked service_Booked, SRDisposition sr_Disposition, PickupDrop pick_up, Vehicle vehicle, Long id) {

		CallInteraction edit_data = em.find(CallInteraction.class, id);
		// srdisposition
		edit_data.getSrdisposition().setComments(sr_Disposition.getComments());
		edit_data.getSrdisposition().setFollowUpTime(sr_Disposition.getFollowUpTime());
		edit_data.getSrdisposition().setFollowUpDate(sr_Disposition.getFollowUpDate());
		edit_data.getVehicle().setModel(vehicle.getModel());
		edit_data.getCustomer().setCustomerName(customer.getCustomerName());
		// edit_data.getCustomer().setCustomerEmail(customer.getCustomerEmail());
		// edit_data.getCustomer().setCustomerAddress(customer.getCustomerAddress());

	}

	@Override
	public CallInteraction getCallLogsByCRE(Long id, String dealerDB) {
		CallInteraction edi = em.find(CallInteraction.class, id);
		return edi;
	}

	@Override
	public void addCustomerAndVehicalInfo(UploadFileFields uplodingData) {

		Customer cust = new Customer();
		cust.setCustomerName(uplodingData.getCustomerName());
		// cust.setCustomerPhone(uplodingData.getCustomerPhone());

		em.persist(cust);

		Vehicle veh = new Vehicle();

		veh.setIsAssigned("false");
		veh.setCustomer(cust);
		em.persist(veh);

		Service service_upload = new Service();

		em.persist(service_upload);

	}

	private Service getLatestNextServiceData(long vehicle_id) {

		// logger.info("getLatestNextServiceData list"+vehicle_id);
		List<Service> service_list = source.services(em).where(u -> u.getVehicle().getVehicle_id() == vehicle_id)
				.toList();
		List<Service> finalData = new ArrayList<>();
		if (service_list.size() != 0) {

			Date tmp = null;
			int i = 0;
			if (service_list.size() != 0) {
				for (Service uniqueNo_list : service_list) {
					if (i == 0) {
						tmp = uniqueNo_list.getNextServiceDate();
						i++;
					} else {
						if (tmp.before(uniqueNo_list.getNextServiceDate())) {
							tmp = uniqueNo_list.getNextServiceDate();
							i++;
						}
					}

					// logger.info("getting latest getNextServiceDate :"+tmp);
				}
				// logger.info("getting latest getNextServiceDate :"+tmp);
				finalData.add(service_list.get(i - 1));
			}

			return finalData.get(0);
		} else {
			Service data = new Service();
			return data;
		}

	}
	// PSF assign List

	@Override
	public List<AssignedListForCRE> getAssignListOfPSFByProcedure(Date fromDateNew, Date toDateNew, String customerCat,
			String serviceCat, long campaignTypeCat, long campaignNameCat, long campaignNamePSF) {

		String customer_cat = customerCat;
		String service_cat = serviceCat;

		if (customer_cat.equals("All")) {

			customer_cat = "";
		}
		if (service_cat.equals("All")) {

			service_cat = "";
		}

		javax.persistence.Query query = em.createNativeQuery(
				"CALL psfassignlist(:campaignNamePSF,:fromDateNew,:toDateNew,:customer_cat, :service_cat)",
				"AssignedListForCRE");
		query.setParameter("campaignNamePSF", campaignNamePSF);
		query.setParameter("fromDateNew", fromDateNew);
		query.setParameter("toDateNew", toDateNew);
		query.setParameter("customer_cat", customer_cat);
		query.setParameter("service_cat", service_cat);

		return query.getResultList();
	}

	@Override
	public List<AssignedListForCRE> getAssignListOfPSFByProcedureServer(Date fromDateNew, Date toDateNew,
			String customerCat, String serviceCat, long campaignTypeCat, long campaignNameCat, long campaignNamePSF,String modelname,
			long fromIndex, long toIndex,String cityId,String workshopname) {

		String customer_cat = customerCat;
		String service_cat = serviceCat;

		if (customer_cat.equals("All")) {

			customer_cat = "";
		}
		if (service_cat.equals("All")) {

			service_cat = "";
		}

		javax.persistence.Query query = em.createNativeQuery(
				"CALL psfassignlist(:campaignNamePSF,:fromDateNew,:toDateNew,:customer_cat, :service_cat,:inmodel,:inlocation, :inworkshop_id, :startwith,:length)",
				"AssignedListForCRE");
		query.setParameter("campaignNamePSF", campaignNamePSF);
		query.setParameter("fromDateNew", fromDateNew);
		query.setParameter("toDateNew", toDateNew);
		query.setParameter("customer_cat", customer_cat);
		query.setParameter("service_cat", service_cat);
		query.setParameter("inmodel", modelname);
		query.setParameter("inlocation", cityId);
		query.setParameter("inworkshop_id", workshopname);
		query.setParameter("startwith", (int) fromIndex);
		query.setParameter("length", (int) toIndex);

		return query.getResultList();

	}

	// SMR and Campaign Assign List
	@Override
	public List<Vehicle> assignCallsToUser(Date fromDateNew, Date toDateNew, String customerCat, String serviceCat,
			long campaignTypeCat, long campaignNameCat, long campaignNamePSF) {

		logger.info("inside assignCallsToUse--" + campaignTypeCat);
		logger.info("inside assignCallsToUse campaign name--" + campaignNameCat);

		logger.info("campaignNamePSF4::" + campaignNamePSF);

		List<Vehicle> serviceData = new ArrayList<Vehicle>();

		Campaign campType = em.find(Campaign.class, campaignTypeCat);

		// logger.info("campType:" + camName);
		if (campType.getCampaignType().equals("Campaign")) { // campIGN

			List<Vehicle> vehicle_data = source.vehicle(em)
					.where(u -> u.getIsAssigned() != null && u.getIsAssigned().equals("false"))
					.where(u -> u.getFollowUpDateSMR() != null).toList();

			if (vehicle_data.size() != 0) {

				for (Vehicle veh : vehicle_data) {
					// Hibernate.initialize(veh.getCustomer().getPreferredPhone());

					if (veh.getFollowUpDateSMR() != null) {
						if (veh.getFollowUpDateSMR().equals(fromDateNew)
								|| veh.getFollowUpDateSMR().equals(toDateNew)) {

							serviceData.add(veh);

						} else if (veh.getFollowUpDateSMR().before(toDateNew)
								&& veh.getFollowUpDateSMR().after(fromDateNew)) {

							serviceData.add(veh);
						}

					}

				}

				List<Vehicle> finalListDataCustomerCat = new ArrayList<Vehicle>();

				for (Vehicle ser_data : serviceData) {

					// Hibernate.initialize(ser_data.getCustomer().getSegment());

					if (customerCat.equals("All")) {
						finalListDataCustomerCat.add(ser_data);

					} else {

						if (ser_data.getCustomer().getSegment() != null) {

							List<Segment> list_segment = ser_data.getCustomer().getSegment();

							if (list_segment.size() != 0) {

								if (list_segment.get(0).getName() != null) {

									if (list_segment.get(0).getName().equals(customerCat)) {

										if (list_segment.get(0).getDataUploadType() != null
												&& list_segment.get(0).getDataUploadType().equals("Campaign")) {

											finalListDataCustomerCat.add(ser_data);

										}
									}
								}
							}

						}

					}

				}

				List<Vehicle> finalListDataService = new ArrayList<Vehicle>();

				for (Vehicle ser_data : finalListDataCustomerCat) {

					if (serviceCat.equals("All")) {

						finalListDataService.add(ser_data);

					} else {

						if (ser_data.getNextServicetype().equals(serviceCat)) {

							finalListDataService.add(ser_data);

						}

					}

				}

				for (Vehicle vehcalData : finalListDataService) {
					Hibernate.initialize(vehcalData.getCustomer().getPreferredPhone());

				}

				return finalListDataService;

			} else {

				return serviceData;
			}
		} else if (campType.getCampaignType().equals("SMR")) {

			List<Vehicle> vehicle_data = source.vehicle(em).where(u -> u.getIsAssigned() != null)
					.where(u -> u.getIsAssigned().equals("false")).where(u -> u.getFollowUpDateSMR() != null).toList();

			if (vehicle_data.size() != 0) {

				for (Vehicle veh : vehicle_data) {
					// Hibernate.initialize(veh.getCustomer().getPreferredPhone());

					if (veh.getFollowUpDateSMR() != null) {
						if (veh.getFollowUpDateSMR().equals(fromDateNew)
								|| veh.getFollowUpDateSMR().equals(toDateNew)) {

							serviceData.add(veh);

						} else if (veh.getFollowUpDateSMR().before(toDateNew)
								&& veh.getFollowUpDateSMR().after(fromDateNew)) {

							serviceData.add(veh);
						}

					}

				}

				List<Vehicle> finalListDataCustomerCat = new ArrayList<Vehicle>();

				for (Vehicle ser_data : serviceData) {

					// Hibernate.initialize(ser_data.getCustomer().getSegment());
					if (customerCat.equals("All")) {
						finalListDataCustomerCat.add(ser_data);

					} else if (ser_data.getCustomer().getSegment() != null) {

						List<Segment> list_segment = ser_data.getCustomer().getSegment();

						if (list_segment.size() != 0) {

							if (list_segment.get(0).getName() != null) {

								if (list_segment.get(0).getName().equals(customerCat)) {

									if (list_segment.get(0).getDataUploadType() != null
											&& list_segment.get(0).getDataUploadType().equals("SMR")) {

										finalListDataCustomerCat.add(ser_data);

									}
								}
							}
						}

					}

				}

				List<Vehicle> finalListDataService = new ArrayList<Vehicle>();

				for (Vehicle ser_data : finalListDataCustomerCat) {

					if (serviceCat.equals("All")) {

						finalListDataService.add(ser_data);

					} else if (ser_data.getNextServicetype() != null
							&& ser_data.getNextServicetype().equals(serviceCat)) {

						finalListDataService.add(ser_data);

					}

				}
				logger.info(" count of SMR :" + finalListDataService.size());

				for (Vehicle vehcalData : finalListDataService) {
					Hibernate.initialize(vehcalData.getCustomer().getPreferredPhone());

				}
				return finalListDataService;

			} else {

				return serviceData;
			}
		} else {

			List<Vehicle> all_vehical = source.vehicle(em).toList();
			if (all_vehical.size() != 0) {
				for (Vehicle veh : all_vehical) {

					// Hibernate.initialize(veh.getCustomer().getPreferredPhone());
					if (veh.getNextServicedate() != null && veh.getIsAssigned() != null
							&& veh.getIsAssigned().equals("false")) {
						if (veh.getNextServicedate().equals(fromDateNew)
								|| veh.getNextServicedate().equals(toDateNew)) {

							serviceData.add(veh);

						} else if (veh.getNextServicedate().before(toDateNew)
								&& veh.getNextServicedate().after(fromDateNew)) {

							serviceData.add(veh);
						}
					}

				}

				logger.info("total calll :" + serviceData.size());

				List<Vehicle> finalListDataCustomerCat = new ArrayList<Vehicle>();

				for (Vehicle ser_data : serviceData) {

					// Hibernate.initialize(ser_data.getCustomer().getSegment());
					if (customerCat.equals("All")) {
						finalListDataCustomerCat.add(ser_data);

					} else if (ser_data.getCustomer().getSegment() != null) {

						List<Segment> list_segment = ser_data.getCustomer().getSegment();

						if (list_segment.size() != 0) {

							if (list_segment.get(0).getName() != null) {

								if (list_segment.get(0).getName().equals(customerCat)) {

									if (list_segment.get(0).getDataUploadType() != null) {
									} else {
										finalListDataCustomerCat.add(ser_data);

									}
								}
							}
						}

					}

				}

				List<Vehicle> finalListDataService = new ArrayList<Vehicle>();

				for (Vehicle ser_data : finalListDataCustomerCat) {

					if (serviceCat.equals("All")) {

						finalListDataService.add(ser_data);

					} else if (ser_data.getNextServicetype().equals(serviceCat)) {

						finalListDataService.add(ser_data);

					}

				}
				for (Vehicle vehcalData : finalListDataService) {
					Hibernate.initialize(vehcalData.getCustomer().getPreferredPhone());

				}

				return finalListDataService;
			} else {

				return serviceData;
			}
		}
	}

	private Service getLatestServiceDataForFiltering(long vehicle_id) {

		List<Service> service_list = source.services(em).where(u -> u.getVehicle().getVehicle_id() == vehicle_id)
				.where(u -> u.getUploadDataFiletype().equals("Bill")).toList();

		logger.info("service_list upload bill :" + service_list.size());
		List<Service> finalData = new ArrayList<>();
		if (service_list.size() != 0) {

			Date tmp = null;
			int i = 0;
			if (service_list.size() != 0) {
				for (Service uniqueNo_list : service_list) {

					if (uniqueNo_list.getLastServiceDate() != null) {
						if (i == 0) {
							tmp = uniqueNo_list.getLastServiceDate();
							i++;
						} else {
							if (tmp.before(uniqueNo_list.getLastServiceDate())) {
								tmp = uniqueNo_list.getLastServiceDate();
								i++;
							}
						}
					}
				}
				// logger.info("getting latest disposition :"+tmp);
				finalData.add(service_list.get(i - 1));
			}
			Hibernate.initialize(finalData.get(0).getServiceAdvisor());
			Hibernate.initialize(finalData.get(0).getWorkshop());

			return finalData.get(0);
		} else {
			Service data = new Service();
			Hibernate.initialize(data.getServiceAdvisor());
			Hibernate.initialize(data.getWorkshop());
			return data;
		}

	}

	@Override
	public void assignCallByManager(String userdata, long scallId, String userLogindealerCode, long campaignCat,
			long campaignNameCat, long campaignNamePSF) {
		WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(userdata)).getOnlyValue();
		// logger.info("Assign call from CRE the username is :
		// "+user.getUserName());

		Campaign assign_cam1 = em.find(Campaign.class, campaignCat);
		if (assign_cam1.getCampaignType().equals("SMR")) {

			Vehicle scall = em.find(Vehicle.class, scallId);
			scall.setIsAssigned("true");
			em.merge(scall);
			AssignedInteraction newData = new AssignedInteraction();
			newData.setWyzUser(user);
			newData.setCustomer(scall.getCustomer());
			newData.setVehicle(scall);
			newData.setUplodedCurrentDate(currentDate());
			newData.setCallMade("No");
			newData.setCampaign(assign_cam1);
			em.persist(newData);

			String servicedue = SMSTemplate.SERVICE_DUE;
			String message = source.smsTemplates(em).where(m -> m.getSmsType().equals(servicedue))
					.select(m -> m.getSmsTemplate()).getOnlyValue();

			message = message.replace("(customer name)", scall.getCustomer().getCustomerName());
			message = message.replace("(Model)", scall.getModel());

			message = message.replace("(Date)", scall.getNextServicedate().toString());
			// message = message.replace("(Mobile number)", (Mobile
			// number).toString());
			String dealerNameCity = user.getUserName() + "," + user.getDealerCode() + ","
					+ user.getLocation().getName();
			message = message.replace("(Name, dealer name & city)", dealerNameCity);
			logger.info("message : " + message);

			// static MyInterface ref=new MyInterface();
			smsTriggerRepo.sendCustomSMS(scall.getVehicle_id(), message, "Service Due", user);
		}

		logger.info("campaign sub id:" + campaignNameCat);
		logger.info("campaignNamePSF3::" + campaignNamePSF);
		if (campaignNameCat > 0) {
			Campaign assign_cam = em.find(Campaign.class, campaignNameCat);
			Vehicle scall = em.find(Vehicle.class, scallId);
			scall.setIsAssigned("true");
			em.merge(scall);
			AssignedInteraction newData = new AssignedInteraction();
			newData.setWyzUser(user);
			newData.setCustomer(scall.getCustomer());
			newData.setVehicle(scall);
			newData.setUplodedCurrentDate(currentDate());
			newData.setCallMade("No");
			newData.setCampaign(assign_cam);
			em.persist(newData);

		} else if (campaignNamePSF > 0) {

			Campaign assign_cam = em.find(Campaign.class, campaignNamePSF);
			Vehicle scall = em.find(Vehicle.class, scallId);
			scall.setIsAssigned("true");
			em.merge(scall);
			AssignedInteraction newData = new AssignedInteraction();
			newData.setWyzUser(user);
			newData.setCustomer(scall.getCustomer());
			newData.setVehicle(scall);
			newData.setUplodedCurrentDate(currentDate());
			newData.setCallMade("No");
			newData.setCampaign(assign_cam);
			em.persist(newData);
		}

	}

	@Override
	public void assignCallByManagerForPSF(String userdata, AssignedListForCRE psfAssignId, String userLogindealerCode) {

		WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(userdata)).getOnlyValue();
		long psfId = psfAssignId.getPsfinteraction_id();

		PSFAssignedInteraction updatePSFAssign1 = source.psfAssignedInteraction(em).where(u -> u.getId() == psfId).getOnlyValue();
		updatePSFAssign1.setWyzUser(user);
		updatePSFAssign1.setDisplayFlag(true);
		updatePSFAssign1.setLastDisposition("Not yet Called");
		em.merge(updatePSFAssign1);

		PSFAssignedInteraction updatePSFAssign = source.psfAssignedInteraction(em).where(u -> u.getId() == psfId).getOnlyValue();

		AssignedCallsReport addRecordAssigned = new AssignedCallsReport();

		addRecordAssigned.setWyzuserId(user.getId());
		addRecordAssigned.setUploadId(updatePSFAssign.getVehicle().getUpload_id());
		addRecordAssigned.setAssignedDate(new Date());
		addRecordAssigned.setAssignInteractionID(psfId);
		addRecordAssigned.setAssignmentType("PSF");
		addRecordAssigned.setVehicleId(updatePSFAssign.getVehicle().getVehicle_id());

		em.persist(addRecordAssigned);

		DeletedStatusReport delStatusReport = new DeletedStatusReport();
		delStatusReport.setAssigned_id(updatePSFAssign.getId());
		delStatusReport.setVehicle_id(updatePSFAssign.getVehicle().getVehicle_id());
		delStatusReport.setCustomer_id(updatePSFAssign.getCustomer().getId());
		delStatusReport.setCampaign_id(updatePSFAssign.getCampaign().getId());
		delStatusReport.setUploaded_date(updatePSFAssign.getUplodedCurrentDate());
		delStatusReport.setWyzuser_id(user.getId());
		em.persist(delStatusReport);
		
		VehicleSummaryCube vehicleSummaryCube = new VehicleSummaryCube();
		vehicleSummaryCube.setChassisNo(updatePSFAssign.getVehicle().getChassisNo());
		vehicleSummaryCube.setVehicleRegNo(updatePSFAssign.getVehicle().getVehicleRegNo());
		vehicleSummaryCube.setPhoneNumber(updatePSFAssign.getCustomer().getPreferredPhone().getPhoneNumber());
		vehicleSummaryCube.setCustomerName(updatePSFAssign.getCustomer().getCustomerName());
		vehicleSummaryCube.setCampaign(updatePSFAssign.getCampaign().getCampaignName());
		vehicleSummaryCube.setServiceDueDate(updatePSFAssign.getVehicle().getNextServicedate());
		vehicleSummaryCube.setServiceDueType(updatePSFAssign.getVehicle().getNextServicetype());
		vehicleSummaryCube.setUploadedDate(updatePSFAssign.getUplodedCurrentDate());
		vehicleSummaryCube.setAssignedDate(updatePSFAssign.getUplodedCurrentDate());
		vehicleSummaryCube.setAssignedCre(user.getUserName());
		long v1 = updatePSFAssign.getVehicle().getVehicle_id();
		if (getLatestServiceDataFiltering(v1) != null) {
			vehicleSummaryCube.setRepairOrderNo(getLatestServiceDataFiltering(updatePSFAssign.getVehicle().getVehicle_id()).getJobCardNumber());
			vehicleSummaryCube.setRoDate(getLatestServiceDataFiltering(updatePSFAssign.getVehicle().getVehicle_id()).getJobCardDate());
		}
		vehicleSummaryCube.setVehicle_id(updatePSFAssign.getVehicle().getVehicle_id());
		vehicleSummaryCube.setCustomer_id(updatePSFAssign.getCustomer().getId());
		vehicleSummaryCube.setAssignedWyzuserID(user.getId());
		vehicleSummaryCube.setAssignedInteractionID(updatePSFAssign.getId());
		vehicleSummaryCube.setUploadVehicle_id(Long.parseLong(updatePSFAssign.getVehicle().getUpload_id()));
		em.persist(vehicleSummaryCube);
	}

	private java.sql.Date currentDate() {

		return new java.sql.Date(System.currentTimeMillis());
	}

	@Override
	public List<AssignedInteraction> getAssigendInteractionsOfUser(String selectAgent) {

		List<AssignedInteraction> assign_data = source.assignedInteractions(em)
				.where(u -> u.getWyzUser().getUserName().equals(selectAgent) && u.getCallMade().equals("No")).toList();
		for (AssignedInteraction assdata : assign_data) {

			Hibernate.initialize(assdata.getCustomer().getPhones());

		}

		return assign_data;
	}

	@Override
	public AssignedInteraction getAssignedInteractionsbyId(long id, String dealercode) {
		AssignedInteraction assign = em.find(AssignedInteraction.class, id);

		Hibernate.initialize(assign.getCustomer().getPhones());
		Hibernate.initialize(assign.getCustomer().getAddresses());
		Hibernate.initialize(assign.getCustomer().getPreferredAdress());
		Hibernate.initialize(assign.getCustomer().getPreferredEmail());
		Hibernate.initialize(assign.getCustomer().getPreferredPhone());
		Hibernate.initialize(assign.getCustomer().getSegment());
		Hibernate.initialize(assign.getCustomer().getVehicles());
		Hibernate.initialize(assign.getCustomer().getInsurances());
		Hibernate.initialize(assign.getCustomer().getOfficeAddressData());
		Hibernate.initialize(assign.getCustomer().getResidenceAddressData());
		Hibernate.initialize(assign.getCustomer().getPermanentAddressData());
		Hibernate.initialize(assign.getVehicle());

		return assign;
	}

	@Override
	public void updateAssignedCallMade(long singleId) {

		AssignedInteraction assign_data = em.find(AssignedInteraction.class, singleId);

		assign_data.setCallMade("Yes");
	}

	@Override
	public List<CallInteraction> getSearchList(String userLoginName, String userLogindealerCode) {
		List<CallInteraction> datainfo = source.callinteractions(em).toList();

		return datainfo;
	}

	@Override
	public List<CallInteraction> addCustomer(String userLoginName, String userLogindealerCode) {
		List<CallInteraction> addcustomer = new ArrayList<CallInteraction>();
		return addcustomer;
	}

	@Override
	public List<String> getNewCustomer(Vehicle vehicle, Customer customer, Phone phone, Email email, Address address,
			String userLoginName) {

		em.persist(customer);

		vehicle.setCustomer(customer);
		vehicle.setIsAssigned("true");
		em.persist(vehicle);

		phone.setPhoneTye(Phone.PHONE_TYPE_OFFICE);
		phone.setIsPreferredPhone(true);
		phone.setUpdatedBy(userLoginName);
		phone.setCustomer(customer);
		em.persist(phone);

		email.setCustomer(customer);
		email.setUpdatedBy(userLoginName);
		email.setIsPreferredEmail(true);
		em.persist(email);

		address.setCustomer(customer);
		address.setAddressType(Address.ADDRESS_TYPE_OFFICE);
		address.setIsPreferred(true);
		em.persist(address);

		List<String> lsitIds = new ArrayList<String>();

		// logger.info(" customer.getId() : "+customer.getId()+"
		// vehicle.getVehicle_id() : "+vehicle.getVehicle_id());

		lsitIds.add(String.valueOf(customer.getId()));
		lsitIds.add(String.valueOf(vehicle.getVehicle_id()));

		return lsitIds;

	}

	@Override
	public List<AssignedInteraction> getAssigendIntOfManager(String userLoginName) {

		List<AssignedInteraction> assingdata = source.assignedInteractions(em)
				.where(u -> u.getWyzUser().getCreManager().equals(userLoginName)
						&& u.getWyzUser().getRole().equals("CRE") && u.getCallMade().equals("No"))
				.toList();

		for (AssignedInteraction ass : assingdata) {
			Hibernate.initialize(ass.getCustomer().getPreferredPhone());

		}

		return assingdata;

	}

	@Override
	public List<Vehicle> getVehicleList(long id) {

		List<Vehicle> dataVeh = source.vehicle(em).where(u -> u.getCustomer().getId() == id).toList();

		return dataVeh;
	}

	@Override
	public List<ServiceAdvisor> getAdvisorList() {
		return source.serviceadvisor(em).toList();
	}

	@Override
	public List<Driver> getDriverList() {
		return source.driver(em).toList();
	}

	@Override
	public Customer getCustomerListById(long id) {
		AssignedInteraction assignedInt = em.find(AssignedInteraction.class, id);

		return assignedInt.getCustomer();
	}

	@Override
	public List<Workshop> getWorkshop() {
		return source.workshop(em).toList();

	}

	@Override
	public Service getLatestServiceData(long vehicle_id) {
		//logger.info("vehicle_id is : " + vehicle_id);

		List<Service> service_list = source.services(em).where(u -> u.getVehicle().getVehicle_id() == vehicle_id)
				.where(u -> u.getLastServiceDate() != null).toList();
		List<Service> finalData = new ArrayList<>();
		if (service_list.size() != 0) {

			Date tmp = null;
			int i = 0;
			if (service_list.size() != 0) {
				for (Service uniqueNo_list : service_list) {
					if (uniqueNo_list.getLastServiceDate() != null) {

						if (i == 0) {
							tmp = uniqueNo_list.getLastServiceDate();
							i++;
						} else {
							if (tmp.before(uniqueNo_list.getLastServiceDate())) {
								tmp = uniqueNo_list.getLastServiceDate();
								logger.info("latest temp :"+tmp+" i :"+i);
								i++;
							}
						}
					}
				}
				 
				logger.info("index is "+i);
				finalData.add(service_list.get(i - 1));
			}
			Hibernate.initialize(finalData.get(0).getServiceAdvisor());
			Hibernate.initialize(finalData.get(0).getWorkshop());

			return finalData.get(0);
		} else {
			Service data = new Service();
			Hibernate.initialize(data.getServiceAdvisor());
			Hibernate.initialize(data.getWorkshop());
			return data;
		}

	}

	public List<Complaint> getComplaintsFilterData(String filterData) {

		if (filterData.equals("Assigned")) {
			return source.complaint(em)
					.where(u -> u.getComplaintStatus().equals(filterData) || u.getComplaintStatus().equals("Open"))
					.toList();

		} else {
			logger.info("filterdata is::" + filterData);
			return source.complaint(em).where(u -> u.getComplaintStatus().equals(filterData)).toList();
		}
	}

	public List<ComplaintInteraction> getComplaintHistory(String complaintNumber, String vehregnumber) {
		// c1000 in complaint, wlill give me id of complaint
		long complaintCount = source.complaint(em).where(u -> u.getComplaintNumber().equals(complaintNumber)).count();
		if (complaintCount > 0) {
			Complaint complaintId = source.complaint(em).where(u -> u.getComplaintNumber().equals(complaintNumber))
					.getOnlyValue();
			long compid = complaintId.getId();
			List<ComplaintInteraction> complaintinfoData = source.complaintInteractions(em)
					.where(u -> u.getComplaint().getId() == compid).toList();
			return complaintinfoData;
		} else {
			return new ArrayList<ComplaintInteraction>();
		}
	}

	@Override
	public List<Service> getServiceListByVehicle(List<Vehicle> vehicleList) {

		List<Service> ser_list = new ArrayList<Service>();
		if (vehicleList.size() != 0) {

			for (Vehicle vehicle_list : vehicleList) {

				long veh_id = vehicle_list.getVehicle_id();
				List<Service> data = source.services(em).where(c -> c.getVehicle().getVehicle_id() == veh_id).toList();

				List<Service> finalData = new ArrayList<>();
				if (data.size() != 0) {

					Date tmp = null;
					int i = 0;
					if (data.size() != 0) {
						for (Service uniqueNo_list : data) {
							if (i == 0) {
								tmp = uniqueNo_list.getLastServiceDate();
								i++;
							} else {
								if (tmp.before(uniqueNo_list.getLastServiceDate())) {
									tmp = uniqueNo_list.getLastServiceDate();
									i++;
								}
							}
						}
						// logger.info("getting latest disposition :"+tmp);
						finalData.add(data.get(i - 1));
					}

					ser_list.add(finalData.get(0));
				}

			}

			// logger.info("service list size from repo :"+ser_list.size());
			return ser_list;
		} else {

			List<Service> data = new ArrayList<Service>();
			return data;

		}

	}

	@Override
	public void updateTheUnavialbilityList(List<WyzUser> datalist, String roleIs) {

		// logger.info("Entering in to update unavailability");

		for (WyzUser data_list : datalist) {
			String user = data_list.getUserName();
			List<WyzUser> wyz_data = source.wyzUsers(em).where(u -> u.getUserName().equals(user)).toList();
			if (wyz_data.size() != 0) {

				WyzUser updateUser = wyz_data.get(0);
				if (updateUser.getRole().equals(roleIs)) {

					updateUser.setUnAvailable(data_list.unAvailable);
					em.merge(updateUser);
					if (data_list.unAvailable) {
						UnAvailability un_availability = new UnAvailability();
						un_availability.setFromDate(currentDate());
						un_availability.setToDate(currentDate());
						un_availability.setWyzUser(updateUser);
						em.persist(un_availability);
					}

				}
			}

		}

	}

	@Override
	public void updateUnavailabiltyOfUserInRange(List<WyzUser> datalist, List<UnAvailability> unavilabilityList) {
		for (int i = 0; i < datalist.size(); i++) {

			long userId = datalist.get(i).getId();

			long userExisting = source.wyzUsers(em).where(u -> u.getId() == userId).count();
			if (userExisting != 0) {

				WyzUser useris = source.wyzUsers(em).where(u -> u.getId() == userId).getOnlyValue();
				// logger.info("From date is
				// "+unavilabilityList.get(i).getFromDate()+" user is
				// "+useris.getUserName());
				if (unavilabilityList.get(i).getFromDate() != null && unavilabilityList.get(i).getToDate() != null) {

					unavilabilityList.get(i).setWyzUser(useris);
					em.persist(unavilabilityList.get(i));

				}

			}

		}
	}

	@Override
	public List<String> getCREofCREManager(String username, String userLogindealerCode) {
		List<String> availableUserList = new ArrayList<String>();
		List<WyzUser> userList = source.wyzUsers(em)
				.where(k -> k.getCreManager().equals(username) && k.getRole().equals("CRE")).toList();
		if (userList.size() != 0) {

			for (WyzUser d : userList) {
				if (d.isUnAvailable() == false) {
					availableUserList.add(d.getUserName());
					// logger.info("users is :"+d.getUserName());
				}

			}

		}
		return availableUserList;

	}

	@Override
	public List<String> getCREandServiceAdvOFMan(String username, String userLogindealerCode) {
		List<String> availableUserList = new ArrayList<String>();
		List<WyzUser> userList = source.wyzUsers(em).where(k -> k.getCreManager().equals(username))
				.where(k -> k.getRole().equals("CRE")).toList();
		if (userList.size() != 0) {

			for (WyzUser d : userList) {
				if (d.isUnAvailable() == false) {
					availableUserList.add(d.getUserName());
					// logger.info("users is :"+d.getUserName());
				}

			}

		}

		List<String> availableUsers = new ArrayList<String>();

		java.util.Date todayData = new java.sql.Date(System.currentTimeMillis());

		for (String userData : availableUserList) {

			WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(userData)).getOnlyValue();

			Hibernate.initialize(user.getUnAvailabilities());

			List<UnAvailability> unaviabiltylist = user.getUnAvailabilities();

			if (unaviabiltylist.size() != 0) {

				int i = 0;
				for (UnAvailability unavailable : unaviabiltylist) {

					if (unavailable.getFromDate().before(todayData) && unavailable.getToDate().after(todayData)) {

						i = i + 1;

					} else if (unavailable.getFromDate().toString().equals(todayData.toString())) {
						i = i + 1;

					} else if (unavailable.getToDate().toString().equals(todayData.toString())) {
						i = i + 1;

					}
				}
				if (i == 0) {
					availableUsers.add(user.getUserName());

				}

			} else {

				availableUsers.add(user.getUserName());

			}

		}
		return availableUsers;

	}

	@Override
	public List<String> getCRENotAvailable(String userLogindealerCode, String username) {
		List<String> availableUserList = new ArrayList<String>();
		List<WyzUser> userList = source.wyzUsers(em)
				.where(k -> k.getCreManager().equals(username) && k.getRole().equals("CRE")).toList();
		if (userList.size() != 0) {

			for (WyzUser d : userList) {
				if (d.isUnAvailable() == true) {
					availableUserList.add(d.getUserName());
					// logger.info("users is :"+d.getUserName());
				}

			}

		}
		return availableUserList;
	}

	@Override
	public List<AssignedInteraction> getAssignedIntListForReAss(List<String> selectedList) {
		List<AssignedInteraction> addList = new ArrayList<AssignedInteraction>();
		for (String user : selectedList) {

			List<AssignedInteraction> listData = source.assignedInteractions(em)
					.where(u -> u.getWyzUser().getUserName().equals(user) && u.getCallMade().equals("No")).toList();
			addList.addAll(listData);
		}
		for (AssignedInteraction data_assign : addList) {

			Hibernate.initialize(data_assign.getCustomer().getPreferredPhone());

		}
		return addList;
	}

	@Override
	public List<CallInteraction> getFollowUpCallsOfUsersList(List<String> selectedList, String dealercode) {

		List<CallInteraction> todaysFollowUp = new ArrayList<CallInteraction>();
		for (String user : selectedList) {

			List<CallInteraction> followupData = getFollowUpCallsOfUser(user, dealercode);

			if (followupData.size() != 0) {
				for (CallInteraction list : followupData) {

					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Calendar c = Calendar.getInstance();
					c.setTime(new Date());
					Date currentDate = Calendar.getInstance().getTime();
					String date1 = simpleDateFormat.format(list.getSrdisposition().getFollowUpDate());
					String date2 = simpleDateFormat.format(currentDate);
					// logger.info("date1 :"+date1+"date2 :"+date2);
					Date currDate = new Date();
					Date follDate = new Date();
					try {
						simpleDateFormat.parse(date1);
						simpleDateFormat.parse(date2);
					} catch (ParseException ex) {
						// logger.info("exception");
					}
					// logger.info("before currentDate
					// :"+currDate+"followUpdate"+follDate);
					if (follDate.equals(currDate)) {
						// logger.info("currentDate
						// :"+currDate+"followUpdate"+follDate);
						todaysFollowUp.add(list);
					}
				}
			}
		}

		for (CallInteraction call_data : todaysFollowUp) {

			Hibernate.initialize(call_data.getCustomer().getPreferredPhone());

		}

		return todaysFollowUp;
	}

	@Override
	public void changeAssignmentCallByManager(String userdata, long scallId, String userLogindealerCode) {
		AssignedInteraction changeAss = em.find(AssignedInteraction.class, scallId);
		WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(userdata)).getOnlyValue();
		changeAss.setWyzUser(user);
	}

	@Override
	public void changeAssignCallFollowUp(String userdata, long scallId, String userLogindealerCode) {

		CallInteraction changeAss = em.find(CallInteraction.class, scallId);
		WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(userdata)).getOnlyValue();
		changeAss.setWyzUser(user);
	}

	@Override
	public void addUpload_Format_data(UploadMasterFormat uploadmasterformat_data) {

		uploadmasterformat_data.setCreated_by("Admin");
		uploadmasterformat_data.setCreated_on(currentDate());
		em.persist(uploadmasterformat_data);

	}

	@Override
	public void addNewCustomerData(NewCustomer newCust_modified) {

		Customer new_cust = new Customer();
		new_cust.setCustomerCategory(newCust_modified.getCustomerCategory());
		new_cust.setCreatedDate(currentDate());
		em.persist(new_cust);

		Vehicle new_veh = new Vehicle();
		new_veh.setVehicleRegNo(newCust_modified.getVehicalRegNo());
		new_veh.setVehicleNumber(newCust_modified.getVehicleNumber());
		new_veh.setModel(newCust_modified.getModel());
		new_veh.setCreatedDate(currentDate());
		new_veh.setCustomer(new_cust);
		new_veh.setIsAssigned("false");
		em.persist(new_veh);

		Service new_ser = new Service();
		new_ser.setJobCardDate(newCust_modified.getJobCardDate());
		new_ser.setJobCardNumber(newCust_modified.getJobCardNumber());
		new_ser.setGroupData(newCust_modified.getGroup());
		new_ser.setTechnician(newCust_modified.getTechnician());
		new_ser.setSaleDate(newCust_modified.getSaleDate());
		new_ser.setSelling(newCust_modified.getSelling());
		new_ser.setLastVisitType(newCust_modified.getServiceType());
		new_ser.setIsAssigned("false");
		new_ser.setVehicle(new_veh);
		em.persist(new_ser);
		Address add_new = new Address();
		add_new.setCustomer(new_cust);
		em.persist(add_new);
	}

	@Override
	public void addJobCardData(JobCardDetailsUpload job_card_details) {
		String jobCard_Number = job_card_details.getJobCardNumber();
		long countOfService = source.services(em).where(u -> u.getJobCardNumber().equals(jobCard_Number)).count();
		if (countOfService != 0) {
			Service serviceData = source.services(em).where(u -> u.getJobCardNumber().equals(jobCard_Number))
					.getOnlyValue();
			serviceData.setPromiseDate(job_card_details.getPromiseDate());
			serviceData.setRevisedPromiseDate(job_card_details.getRevisedPromiseDate());
			serviceData.setReadyDate(job_card_details.getReadyDate());
			em.merge(serviceData);

			Customer customerData = serviceData.getVehicle().getCustomer();
			customerData.setCustomerName(job_card_details.getCustomerName());
			// customerData.setCustomerPhone(job_card_details.getCustomerPhone());
			em.merge(customerData);

			Phone phoneData = new Phone();
			phoneData.setPhoneNumber(job_card_details.getCustomerPhone());
			phoneData.setCustomer(customerData);
			em.persist(phoneData);
		} else {
			Customer customerDetails = new Customer();
			customerDetails.setCustomerName(job_card_details.getCustomerName());
			// customerDetails.setCustomerPhone(job_card_details.getCustomerPhone());
			customerDetails.setCustomerCategory(job_card_details.getCustomerCat());
			customerDetails.setAnniversary_date(job_card_details.getDoa());
			customerDetails.setDob(job_card_details.getDob());
			customerDetails.setCreatedDate(currentDate());
			em.persist(customerDetails);

			Vehicle vehicleDetails = new Vehicle();
			vehicleDetails.setVehicleRegNo(job_card_details.getVehicleRegnumber());
			vehicleDetails.setJobCardNumber(job_card_details.getJobCardNumber());
			vehicleDetails.setChassisNo(job_card_details.getChassisNo());
			vehicleDetails.setModel(job_card_details.getModel());
			vehicleDetails.setEngineNo(job_card_details.getEngineNo());
			vehicleDetails.setVariant(job_card_details.getVariant());
			vehicleDetails.setColor(job_card_details.getColor());

			vehicleDetails.setCreatedDate(currentDate());
			vehicleDetails.setCustomer(customerDetails);
			vehicleDetails.setIsAssigned("false");
			em.persist(vehicleDetails);

			Service new_ser = new Service();
			new_ser.setJobCardDate(job_card_details.getJobCardDate());
			new_ser.setJobCardNumber(job_card_details.getJobCardNumber());
			new_ser.setJobCardDate(job_card_details.getJobCardDate());
			new_ser.setRepeat_revisit(job_card_details.getRepeat_revisit());
			new_ser.setPsfStatus(job_card_details.getPsfStatus());

			new_ser.setSaleDate(job_card_details.getSaleDate());

			new_ser.setTechnician(job_card_details.getTechnician());
			new_ser.setCircularNo(job_card_details.getCircularNo());
			new_ser.setServiceType(job_card_details.getServicetype());

			new_ser.setEstLabAmt(job_card_details.getEstLabAmt());
			new_ser.setLabAmt(job_card_details.getLabAmt());
			new_ser.setPromiseDate(job_card_details.getPromiseDate());
			new_ser.setRevisedPromiseDate(job_card_details.getRevisedPromiseDate());
			new_ser.setStatus(job_card_details.getStatus());
			new_ser.setReadyDate(job_card_details.getReadyDate());
			new_ser.setBillNo(job_card_details.getBillNo());
			new_ser.setLabAmt(job_card_details.getLabAmt());
			new_ser.setPartsAmt(job_card_details.getPartsAmt());

			new_ser.setPickupDate(job_card_details.getPickupDate());

			new_ser.setBillNo(job_card_details.getBillNo());
			new_ser.setBillAmt(job_card_details.getBillAmt());

			new_ser.setGroupData(job_card_details.getGroupData());
			new_ser.setIsAssigned("false");
			new_ser.setVehicle(vehicleDetails);
			em.persist(new_ser);
		}

	}

	/**
	 *
	 * @return
	 */
	@Override
	public List<Vehicle> getAllvehList() {
		List<Vehicle> vehicleList = source.vehicle(em).toList();
		return vehicleList;
	}

	@Override
	public void addNewComplaint(Complaint complaint, Vehicle vehicle, Phone phone, Email email, Address address,
			String userLoginName) {
		// take user and link to complaint
		// logger.info("in complaints" + userLoginName);
		String regNo = complaint.getVehicleRegNo();
		String customername = complaint.getCustomerName();
		String functionName = complaint.getFunctionName();
		String sourceName = complaint.getSourceName();
		String subType = complaint.getSubcomplaintType();
		String complaintCat = complaint.getComplaintType();
		String description = complaint.getDescription();
		String model = vehicle.getModel();
		String workshop = complaint.getWorkshop();

		Location selectedLocation = getLocationByWorkshop(workshop);

		WyzUser userData = source.wyzUsers(em).where(u -> u.getUserName().equals(userLoginName)).toList().get(0);

		long vehicleCount = source.vehicle(em).where(v -> v.getVehicleRegNo().equals(regNo)).count();

		String complaintNameAfterTime = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		complaint.setComplaintNumber("C" + complaintNameAfterTime);

		logger.info("complaintNameAfterTime::" + complaintNameAfterTime);

		if (vehicleCount > 0) {

			Vehicle vehicleInfo = source.vehicle(em).where(v -> v.getVehicleRegNo().equals(regNo)).getOnlyValue();

			long vrh_Id = vehicleInfo.getVehicle_id();

			Vehicle vehRow = em.find(Vehicle.class, vrh_Id);
			long vehid = vehRow.getVehicle_id();

			complaint.setCustomer(vehicleInfo.getCustomer());
			complaint.setWyzUser(userData);
			complaint.setComplaintStatus("New");
			complaint.setAgeOfComplaint(1);
			complaint.setIssueDate(currentDate());
			complaint.setRaisedDate(currentDate());
			complaint.setFunctionName(functionName);
			complaint.setSourceName(sourceName);
			complaint.setComplaintType(complaintCat);
			complaint.setSubcomplaintType(subType);
			complaint.setDescription(description);
			complaint.setCustomerName(customername);
			complaint.setIsAssigned("false");
			complaint.setComplaintSource("Complaint");
			complaint.setVehicle(vehRow);
			complaint.setLocation(selectedLocation);
			em.persist(complaint);

			sendComplaintMessagetoCustomer(complaint.getCustomerPhone(), vehicleInfo, vehicleInfo.getCustomer(),
					userData, complaintNameAfterTime);

		} else {
			// if new veh

			logger.info("complaint.getCustomerPhone() : " + complaint.getCustomerPhone());
			Customer cut_new = new Customer();
			cut_new.setCustomerName(complaint.getCustomerName());
			em.persist(cut_new);

			Phone phoneNew = new Phone();
			phoneNew.setPhoneNumber(complaint.getCustomerPhone());
			phoneNew.setIsPreferredPhone(true);
			phoneNew.setCustomer(cut_new);
			em.persist(phoneNew);

			Email emailNew = new Email();
			emailNew.setEmailAddress(email.getEmailAddress());
			emailNew.setIsPreferredEmail(true);
			emailNew.setCustomer(cut_new);
			em.persist(emailNew);

			Address addressNew = new Address();
			addressNew.setConcatenatedAdress(address.getAddressLine1());
			addressNew.setIsPreferred(true);
			addressNew.setCustomer(cut_new);
			em.persist(addressNew);

			Vehicle veh_new = new Vehicle();
			veh_new.setVehicleRegNo(complaint.getVehicleRegNo());
			veh_new.setModel(model);
			veh_new.setChassisNo(vehicle.getChassisNo());
			veh_new.setVariant(vehicle.getVariant());
			veh_new.setSaleDate(vehicle.getSaleDate());
			veh_new.setLastServiceType(vehicle.getLastServiceType());
			veh_new.setCustomer(cut_new);
			em.persist(veh_new);

			complaint.setCustomer(cut_new);
			complaint.setWyzUser(userData);
			complaint.setComplaintStatus("New");
			complaint.setAgeOfComplaint(1);
			complaint.setIssueDate(currentDate());
			complaint.setRaisedDate(currentDate());
			complaint.setFunctionName(functionName);
			complaint.setSourceName(sourceName);
			complaint.setComplaintType(complaintCat);
			complaint.setSubcomplaintType(subType);
			complaint.setDescription(description);
			complaint.setIsAssigned("false");
			complaint.setCustomerName(customername);
			complaint.setComplaintSource("Complaint");
			complaint.setVehicle(veh_new);
			complaint.setLocation(selectedLocation);
			em.persist(complaint);

			logger.info("vehi : " + veh_new.getVehicle_id() + " custo : " + cut_new.getId());

			sendComplaintMessagetoCustomer(complaint.getCustomerPhone(), veh_new, cut_new, userData,
					complaintNameAfterTime);
		}
	}

	private Location getLocationByWorkshop(String workshop) {
		// TODO Auto-generated method stub
		Workshop workshopis = source.workshop(em).where(u -> u.getWorkshopName().equals(workshop)).getOnlyValue();
		return workshopis.getLocation();
	}

	private void sendComplaintMessagetoCustomer(String customerPhone, Vehicle veh_new, Customer cut_new,
			WyzUser userData, String complaintNameAfterTime) {

		String complaintType = SMSTemplate.COMPLIANT;
		String message = source.smsTemplates(em).where(m -> m.getSmsType().equals(complaintType))
				.select(m -> m.getSmsTemplate()).getOnlyValue();

		message = message.replace("(Registration number)", veh_new.getVehicleRegNo());
		message = message.replace("(complaintnum)", complaintNameAfterTime);

		logger.info(" vehdata data : " + veh_new.getVehicle_id());

		smsTriggerRepo.sendComplaintSMS(customerPhone, veh_new, cut_new, message, "COMPLAINT", userData);
	}

	@Override
	public void assignComplaints() {
	}

	@Override
	public List<Complaint> getAllComplaint() {
		List<Complaint> listOpened = source.complaint(em)
				.where(u -> u.getComplaintStatus().equals("Open") && u.getIsAssigned().equals("false")).toList();
		List<Complaint> notAssigned = new ArrayList<Complaint>();

		for (Complaint not_assigned : listOpened) {
			if (not_assigned.getAssignedUserTo() != null) {

			} else {

				notAssigned.add(not_assigned);

			}
		}
		return notAssigned;
	}

	@Override
	public void updateComplaintsByCRE(long id, String comments, String selected_value) {
		Complaint complaint = em.find(Complaint.class, id);
		complaint.setComplaintStatus(selected_value);
		complaint.setDescription(comments);
		em.merge(complaint);
	}

	@Override
	public void addCustomerAdditionalInfo(CustomerJson cust_details) {
		Customer customer_detailed = em.find(Customer.class, cust_details.customerId);
		// Hibernate.initialize(customer_detailed.getAddresses());

		customer_detailed.setCustomerName(cust_details.getCustomerNameEdit());
		customer_detailed.setUserDriver(cust_details.getDriverNameEdit());
		customer_detailed.setDob(cust_details.getDob());
		customer_detailed.setAnniversary_date(cust_details.getAnniversary_date());
		customer_detailed.setPreferred_time_start(cust_details.getPreferred_time_start());
		customer_detailed.setPreferred_time_end(cust_details.getPreferred_time_end());
		customer_detailed.setMode_of_contact(cust_details.getMode_of_contact());
		customer_detailed.setPreferred_day(cust_details.getPreferred_day());
		customer_detailed.setComments(cust_details.getCustomerComments());
		customer_detailed.setDoNotDisturb(cust_details.isDnd_flag());
		em.merge(customer_detailed);
		long cust_id = cust_details.customerId;

		List<Phone> phoneList = source.phones(em).where(u -> u.getCustomer().getId() == cust_id).toList();

		for (Phone phone_list : phoneList) {

			phone_list.setIsPreferredPhone(false);
			em.merge(phone_list);

		}

		// String cust_phone_id = cust_details.getPreferred_mobile_no();
		//
		// logger.info("preferred mobile : " + cust_phone_id);
		//
		// List<Phone> smrPhoneData = source.phones(em).where(u ->
		// u.getPhoneNumber().equals(cust_phone_id)).toList();
		// smrPhoneData.get(0).setIsPreferredPhone(true);
		// em.merge(smrPhoneData.get(0));

		long cust_phone_id = Long.parseLong(cust_details.getPreferred_mobile_no());

		logger.info("preferred mobile : " + cust_phone_id);
		logger.info("preffered Flag of address " + cust_details.getPreffed_flag());

		Phone smrPhoneData = em.find(Phone.class, cust_phone_id);
		smrPhoneData.setIsPreferredPhone(true);
		em.merge(smrPhoneData);

		if (cust_details.getEmail().equals("")) {

			logger.info("email new is empty : " + cust_details.getEmail());

		} else {
			String newEmail = cust_details.getEmail();

			List<Email> emailList = source.emails(em).where(u -> u.getCustomer().getId() == cust_id).toList();

			if (emailList.size() != 0) {

				List<Email> emailList1 = source.emails(em).where(u -> u.getCustomer().getId() == cust_id)
						.where(u -> u.getEmailAddress().equals(newEmail)).toList();

				if (emailList1.size() != 0) {
					for (Email emailipdate : emailList) {

						emailipdate.setIsPreferredEmail(false);
						em.merge(emailipdate);
					}

					emailList1.get(0).setUpdatedBy(String.valueOf(cust_details.getWyzuser_id()));
					emailList1.get(0).setIsPreferredEmail(true);
					emailList1.get(0).setCustomer(customer_detailed);
					em.merge(emailList.get(0));

				} else {
					for (Email emailipdate : emailList) {

						emailipdate.setIsPreferredEmail(false);
						em.merge(emailipdate);
					}

					Email newEmaildata = new Email();
					logger.info("newEmail is1" + newEmail);
					newEmaildata.setEmailAddress(newEmail);
					newEmaildata.setUpdatedBy(String.valueOf(cust_details.getWyzuser_id()));
					newEmaildata.setIsPreferredEmail(true);
					newEmaildata.setCustomer(customer_detailed);
					em.persist(newEmaildata);

				}

			} else {

				Email newEmaildata = new Email();

				logger.info("newEmail is2" + newEmail);

				newEmaildata.setEmailAddress(newEmail);
				newEmaildata.setUpdatedBy(String.valueOf(cust_details.getWyzuser_id()));
				newEmaildata.setIsPreferredEmail(true);
				newEmaildata.setCustomer(customer_detailed);
				em.persist(newEmaildata);

			}

		}

		List<String> officeAddress = Arrays.asList(cust_details.getOfficeAddress().split("_"));
		List<String> residenceAddress = Arrays.asList(cust_details.getResidenceAddress().split("_"));
		List<String> permanentAddress = Arrays.asList(cust_details.getPermanentAddress().split("_"));

		for (String sa : officeAddress) {

			logger.info(" office address : " + sa);

		}

		int permanentAddressSize = permanentAddress.size();
		int officeAddressSize = officeAddress.size();
		int residenceAddressSize = residenceAddress.size();

		List<Address> getAddresseslist = getAddressesByCustomerId(cust_details.customerId);
		if (permanentAddressSize != 1 || officeAddressSize != 1 || residenceAddressSize != 1) {

			for (Address address : getAddresseslist) {
				address.setIsPreferred(false);
				em.merge(address);
			}
		}

		logger.info("preffered flag of address is : " + cust_details.getPreffed_flag());

		// List<Address> getAddresseslist=customer_detailed.getAddresses();
		logger.info("no of adress liked to this address is : " + getAddresseslist.size());

		logger.info("permanentAddress size : " + permanentAddress.size());
		logger.info("officeAddress size : " + officeAddress.size());
		logger.info("residenceAddress size : " + residenceAddress.size());

		// logger.info(officeAddress.get(0));

		boolean residence = false;
		boolean office = false;
		boolean permanent = false;

		for (Address addressData : getAddresseslist) {

			logger.info("address type is : " + addressData.getAddressType());

			if (addressData.getAddressType() == 1) {

				office = true;

				if (officeAddressSize > 3) {
					addressData.setAddressLine1(officeAddress.get(0));
					addressData.setAddressLine2(officeAddress.get(1));
					addressData.setAddressLine3(officeAddress.get(2));
					addressData.setCity(officeAddress.get(3));
					addressData.setState(officeAddress.get(4));
					logger.info("pincode : " + officeAddress.get(5));
					try {
						addressData.setPincode(Integer.parseInt(officeAddress.get(5)));
					} catch (NumberFormatException ex) {

						logger.info("ex");
						addressData.setPincode(0);
					}

					if (officeAddressSize > 6) {
						addressData.setCountry(officeAddress.get(6));
					}
					logger.info("office flag: " + cust_details.getPreffed_flag());

					if (cust_details.getPreffed_flag() == 1) {
						addressData.setIsPreferred(true);
					} else {
						addressData.setIsPreferred(false);
					}
					em.merge(addressData);
				}

			}
			if (addressData.getAddressType() == 2) {

				logger.info("residence address");

				residence = true;

				if (residenceAddressSize > 3) {
					addressData.setAddressLine1(residenceAddress.get(0));
					addressData.setAddressLine2(residenceAddress.get(1));
					addressData.setAddressLine3(residenceAddress.get(2));
					addressData.setCity(residenceAddress.get(3));
					addressData.setState(residenceAddress.get(4));

					logger.info(" residence pincode : " + residenceAddress.get(5));

					try {
						addressData.setPincode(Integer.parseInt(residenceAddress.get(5)));
					} catch (NumberFormatException ex) {

						logger.info("ex");
						addressData.setPincode(0);
					}
					// addressData.setPincode(Integer.parseInt(residenceAddress.get(5)));

					if (residenceAddressSize > 6) {
						addressData.setCountry(residenceAddress.get(6));
					}

					logger.info("residence flag: " + cust_details.getPreffed_flag());

					if (cust_details.getPreffed_flag() == 2) {
						addressData.setIsPreferred(true);
					} else {
						addressData.setIsPreferred(false);
					}
					em.merge(addressData);
				}

			}
			if (addressData.getAddressType() == 3) {

				logger.info("permanent address");

				permanent = true;
				if (permanentAddressSize > 3) {

					addressData.setAddressLine1(permanentAddress.get(0));
					addressData.setAddressLine2(permanentAddress.get(1));
					addressData.setAddressLine3(permanentAddress.get(2));
					addressData.setCity(permanentAddress.get(3));
					addressData.setState(permanentAddress.get(4));

					try {
						addressData.setPincode(Integer.parseInt(permanentAddress.get(5)));
					} catch (NumberFormatException ex) {

						logger.info("ex");
						addressData.setPincode(0);
					}

					// addressData.setPincode(Integer.parseInt(permanentAddress.get(5)));
					if (permanentAddressSize > 6) {
						addressData.setCountry(permanentAddress.get(6));
					}

					logger.info("permanaent flag: " + cust_details.getPreffed_flag());

					if (cust_details.getPreffed_flag() == 0) {
						addressData.setIsPreferred(true);
					} else {
						addressData.setIsPreferred(false);
					}
					em.merge(addressData);
				}

			}

		}
		if (!office) {
			logger.info("status of office : " + office);

			if (officeAddressSize > 3) {
				Address addingOfficeAdd = new Address();
				if (officeAddress.get(0) != "" || officeAddress.get(0) != null) {
					addingOfficeAdd.setAddressLine1(officeAddress.get(0));
				}
				if (officeAddress.get(1) != "" || officeAddress.get(1) != null) {
					addingOfficeAdd.setAddressLine2(officeAddress.get(1));
				}
				if (officeAddress.get(2) != "" || officeAddress.get(2) != null) {
					addingOfficeAdd.setAddressLine3(officeAddress.get(2));
				}
				if (officeAddress.get(3) != "" || officeAddress.get(3) != null) {
					addingOfficeAdd.setCity(officeAddress.get(3));
				}
				if (officeAddress.get(4) != "" || officeAddress.get(4) != null) {
					addingOfficeAdd.setState(officeAddress.get(4));
				}
				if (officeAddress.get(5) != "" || officeAddress.get(5) != null) {
					addingOfficeAdd.setPincode(Integer.parseInt(officeAddress.get(5)));
				}

				if (officeAddressSize > 6) {

					if (officeAddress.get(6) != "" || officeAddress.get(6) != null) {
						addingOfficeAdd.setCountry(officeAddress.get(6));
					}

				}

				addingOfficeAdd.setCustomer(customer_detailed);

				addingOfficeAdd.setAddressType(1);
				if (cust_details.getPreffed_flag() == 1) {
					addingOfficeAdd.setIsPreferred(true);
				} else {
					addingOfficeAdd.setIsPreferred(false);
				}

				em.persist(addingOfficeAdd);
			}

		}
		if (!residence) {
			logger.info("status of residence : " + residence);
			if (residenceAddressSize > 3) {

				Address addingResidenceAdd = new Address();

				addingResidenceAdd.setAddressLine1(residenceAddress.get(0));
				addingResidenceAdd.setAddressLine2(residenceAddress.get(1));
				addingResidenceAdd.setAddressLine3(residenceAddress.get(2));
				addingResidenceAdd.setCity(residenceAddress.get(3));
				addingResidenceAdd.setState(residenceAddress.get(4));

				logger.info(" residence pincode : " + residenceAddress.get(5));

				try {
					addingResidenceAdd.setPincode(Integer.parseInt(residenceAddress.get(5)));
				} catch (NumberFormatException ex) {

					logger.info("ex");
					addingResidenceAdd.setPincode(0);
				}

				// addingResidenceAdd.setPincode(Integer.parseInt(residenceAddress.get(5)));

				if (residenceAddressSize > 6) {
					addingResidenceAdd.setCountry(residenceAddress.get(6));
				}
				addingResidenceAdd.setCustomer(customer_detailed);
				addingResidenceAdd.setAddressType(2);
				if (cust_details.getPreffed_flag() == 2) {
					addingResidenceAdd.setIsPreferred(true);
				} else {
					addingResidenceAdd.setIsPreferred(false);
				}

				em.persist(addingResidenceAdd);

			}

		}

		if (!permanent) {
			logger.info("status of permanent : " + permanent);

			if (permanentAddressSize > 3) {

				Address addingPermanantAdd = new Address();
				addingPermanantAdd.setAddressLine1(permanentAddress.get(0));
				addingPermanantAdd.setAddressLine2(permanentAddress.get(1));
				addingPermanantAdd.setAddressLine3(permanentAddress.get(2));
				addingPermanantAdd.setCity(permanentAddress.get(3));
				addingPermanantAdd.setState(permanentAddress.get(4));

				logger.info(" permanent pincode : " + permanentAddress.get(5));

				try {
					addingPermanantAdd.setPincode(Integer.parseInt(permanentAddress.get(5)));
				} catch (NumberFormatException ex) {

					logger.info("ex");
					addingPermanantAdd.setPincode(0);
				}

				if (permanentAddressSize > 6) {
					addingPermanantAdd.setCountry(permanentAddress.get(6));
				}

				addingPermanantAdd.setCustomer(customer_detailed);
				addingPermanantAdd.setAddressType(3);
				if (cust_details.getPreffed_flag() == 0) {
					addingPermanantAdd.setIsPreferred(true);
				} else {
					addingPermanantAdd.setIsPreferred(false);
				}

				em.persist(addingPermanantAdd);
			}

		}

		List<Address> addresslist = source.address(em).where(u -> u.getCustomer().getId() == cust_id).toList();
		for (Address sa : addresslist) {

			if (cust_details.getPreffed_flag() == 0) {

				if (sa.getAddressType() == 3) {

					for (Address subAdd : addresslist) {

						if (subAdd.getAddressType() == 3) {

							subAdd.setIsPreferred(true);
							em.merge(subAdd);
						} else {

							subAdd.setIsPreferred(false);
							em.merge(subAdd);
						}

					}

				}

			} else if (cust_details.getPreffed_flag() == 2) {

				if (sa.getAddressType() == 2) {

					for (Address subAdd : addresslist) {

						if (subAdd.getAddressType() == 2) {

							subAdd.setIsPreferred(true);
							em.merge(subAdd);
						} else {

							subAdd.setIsPreferred(false);
							em.merge(subAdd);
						}

					}

				}

			} else if (cust_details.getPreffed_flag() == 1) {

				if (sa.getAddressType() == 1) {

					for (Address subAdd : addresslist) {

						if (subAdd.getAddressType() == 1) {

							subAdd.setIsPreferred(true);
							em.merge(subAdd);
						} else {

							subAdd.setIsPreferred(false);
							em.merge(subAdd);
						}

					}

				}

			}
		}

	}

	@Override
	public void getPhoneDetails(long id, String phone_number) {
		logger.info("phone_number1:" + phone_number);
		Customer customer_details = em.find(Customer.class, id);
		List<Phone> phone_data = customer_details.getPhones();

		em.merge(phone_data);

	}

	@Override
	public void getcallinfoDetails(long customer_id, String preferred_mobile_no) {

		Customer customer_detailed = em.find(Customer.class, customer_id);
		// customer_detailed.setPreferred_mobile_no(preferred_mobile_no);

		em.merge(customer_detailed);
	}

	@Override
	public void updateMessageSentStatus(long id, String message) {
		Service scall = em.find(Service.class, id);
		scall.setMessageSentStatus(message);
		em.merge(scall);
	}

	@Override
	public void getChassisNo(Long customer_id, String chassisNo) {
		Vehicle vehicle_chesis = em.find(Vehicle.class, customer_id);
		vehicle_chesis.setChassisNo(chassisNo);

		em.merge(vehicle_chesis);
	}

	@Override
	public void getEngineNo(Long customer_id, String engineNo) {
		Vehicle vehicle_engine = em.find(Vehicle.class, customer_id);
		vehicle_engine.setEngineNo(engineNo);

		em.merge(vehicle_engine);
	}

	@Override
	public void getVehicalRegNo(Long customer_id, String vehicalRegNo) {
		Vehicle vehicle_registration = em.find(Vehicle.class, customer_id);
		vehicle_registration.setVehicleRegNo(vehicalRegNo);

		em.merge(vehicle_registration);
	}

	@Override
	public Service getVehicalByJobCard(String jobCardNumber) {
		logger.info("jobcard number or upload to send sms" + jobCardNumber);
		return source.services(em).where(u -> u.getJobCardNumber().equals(jobCardNumber)).getOnlyValue();

	}

	@Override
	public List<ServiceBooked> getServiceBookedByUserforThisWorkShop(long workId, long userId) {

		List<ServiceBooked> serv_data=new ArrayList<ServiceBooked>();
		
		List<ServiceBooked> list_data = source.serviceBooked(em)
				.where(u -> u.getWorkshop().getId() == workId)
				.toList();
		
		for(ServiceBooked sa :list_data){
			
			CallInteraction callint=getCallInteractionByBookingId(sa.getServiceBookedId());
			
			if(callint.getWyzUser().getId() == userId){
				serv_data.add(sa);
				
			}
			
		}
		
		

		return serv_data;
	}

	@Override
	public List<ServiceBooked> getServiceBookedBySA(long serviceAdvisorId, long userId) {

		List<ServiceBooked> serv_booked = source.serviceBooked(em)
				.where(u -> u.getServiceAdvisor().getAdvisorId() == serviceAdvisorId).toList();
		return serv_booked;
	}

	@Override
	public List<PickupDrop> getServiceBookedForDriver(long driverId, long userId) {

		List<PickupDrop> driv_list = source.pickupDrop(em).where(u -> u.getDriver().getId() == driverId).toList();
		logger.info("driv_list list is :" + driv_list.size());
		return driv_list;

	}

	@Override
	public void addBillData(workshopBill serviceData) {

		String jobCard_Number = serviceData.getJobCardNumber();
		logger.info("jobCard_Number data :" + jobCard_Number);

		long countOfService = source.services(em).where(u -> u.getJobCardNumber().equals(jobCard_Number)).count();
		if (countOfService != 0) {
			Service serviceInfo = source.services(em).where(u -> u.getJobCardNumber().equals(jobCard_Number))
					.getOnlyValue();
			// Service serviceInfo = source.services(em).where(u ->
			// u.getJobCardNumber().equals(workshopBillinfo)).getOnlyValue();
			serviceInfo.setBillNumber(serviceData.getBillNumber());
			serviceInfo.setMcpNo(serviceData.getMcpNo());
			serviceInfo.setBillDate(serviceData.getBillDate());
			serviceInfo.setJobCardNumber(serviceData.getJobCardNumber());

			serviceInfo.setBillAmt(serviceData.getBillAmt());
			em.merge(serviceInfo);

		}
	}

	@Override
	public AssignedInteraction updateAssignedAgent(long rowId, long wyzUserId, long post_id) {

		AssignedInteraction assignedCallsData = em.find(AssignedInteraction.class, rowId);

		WyzUser selectedUser = em.find(WyzUser.class, post_id);

		assignedCallsData.setWyzUser(selectedUser);
		em.merge(assignedCallsData);

		return assignedCallsData;
	}

	@Override
	public List<Location> getLocationList() {
		return source.locations(em).toList();
	}

	@Override
	public List<Workshop> getWorkshopListByLoca(String selectedCity) {

		// logger.info("selectedCity :"+selectedCity);

		long locationCount = source.locations(em).where(u -> u.getName().equals(selectedCity)).count();

		if (locationCount > 0) {
			Location location = source.locations(em).where(u -> u.getName().equals(selectedCity)).getOnlyValue();
			Hibernate.initialize(location.getWorkshops());
			List<Workshop> work_list = location.getWorkshops();

			return work_list;

		} else {
			List<Workshop> work_list = new ArrayList<Workshop>();

			return work_list;
		}

	}

	@Override
	public List<PickupDrop> getPickupDropList() {
		return source.pickupDrop(em).toList();
	}

	@Override
	public long getworkshopsummaryById(long selectedWorkshop, String schDate) {

		List<ServiceBooked> listWorkshop = source.serviceBooked(em).where(u -> u.getWorkshop() != null)
				.where(u -> u.getWorkshop().getId() == selectedWorkshop).toList();

		if (listWorkshop.size() > 0) {

			long filteredData = listWorkshop.stream().filter(u -> u.getServiceScheduledDateStrSqlFor().equals(schDate))
					.collect(Collectors.counting());

			logger.info("countOfSBForWor : " + listWorkshop.size());
			logger.info("filteredData : " + filteredData);

			return filteredData;
		} else {

			return 0;
		}

	}

	@Override
	public Vehicle getSearchVeh(String veh_number) {
		long count = source.vehicle(em).where(u -> u.getVehicleRegNo().equals(veh_number)).count();
		if (count > 0) {
			// logger.info("this is the veh number in :" + veh_number);
			List<Vehicle> vehicleNum = source.vehicle(em).where(u -> u.getVehicleRegNo().equals(veh_number)).toList();
			// logger.info("vehical from repo" + vehicleNum.size());
			Vehicle vehicle = vehicleNum.get(0);
			Hibernate.initialize(vehicle.getCustomer());
			Hibernate.initialize(vehicle.getCustomer().getPreferredEmail());
			Hibernate.initialize(vehicle.getServiceBooked());
			Hibernate.initialize(vehicle.getCustomer().getPreferredPhone());
			Hibernate.initialize(vehicle.getCustomer().getPreferredAdress());

			return vehicle;
		} else {
			return null;
		}

	}

	@Override
	public Complaint getSearchComplaintNum(String complaintNum) {
		Complaint complaint = source.complaint(em).where(u -> u.getComplaintNumber().equals(complaintNum))
				.getOnlyValue();
		// logger.info("complaintnumber is :"+complaint.getComplaintNumber());
		return complaint;
	}

	@Override
	public List<Complaint> getAllComplaintsForResolution(String userLoginName) {
		long count = source.wyzUsers(em).where(u -> u.getUserName().equals(userLoginName)).count();
		logger.info("inside resolution" + userLoginName);
		if (count > 0) {
			WyzUser wyzUserId = source.wyzUsers(em).where(u -> u.getUserName().equals(userLoginName)).getOnlyValue();
			// logger.info("wys user id in resolution"+ wyzUserId);
			long data = wyzUserId.getId();
			logger.info("rytierytertierytuieyrtu" + data);
			List<Complaint> complaints_details = source.complaint(em)
					.where(u -> u.getComplaintStatus().equals("Assigned") || u.getComplaintStatus().equals("Open"))
					.where(u -> u.getIsAssigned().equals("true") && u.getAssignedUserTo().getId() == data).toList();
			logger.info("complaints_details.size : " + complaints_details.size());
			return complaints_details;
		} else {
			return new ArrayList<Complaint>();
		}

	}

	@Override
	public void updateComplaintsResolution(String complaintNum, String reasonFor, String complaintStatus,
			String customerStatus, String actionTaken, String resolutionBy) {
		logger.info("complaint num::" + complaintNum);
		logger.info("reason for num::" + reasonFor);
		logger.info("complaint sdfsdf::" + complaintStatus);
		logger.info("complaint asdasdasdasd::" + customerStatus);
		logger.info("action taken::" + actionTaken);

		Complaint complaintData = source.complaint(em).where(u -> u.getComplaintNumber().equals(complaintNum))
				.getOnlyValue(); // take the latest disposition
		complaintData.setComplaintStatus(complaintStatus);
		complaintData.setCustomerstatus(customerStatus);
		// complaintData.setreasonFor);
		// complaintData.setActionTaken(actionTaken);
		// complaintData.setResolutionBy(resolutionBy);
		// complaintData.setIsAssigned("false");
		em.merge(complaintData);

		ComplaintInteraction complaintint = new ComplaintInteraction();
		complaintint.setComplaintStatus(complaintStatus);
		complaintint.setCustomerstatus(customerStatus);
		complaintint.setReasonFor(reasonFor);
		complaintint.setActionTaken(actionTaken);
		complaintint.setResolutionBy(resolutionBy);
		// complaintint.setIsAssigned("yes");
		complaintint.setUpdatedDate(currentDate());
		complaintint.setComplaint(complaintData);
		complaintint.setCustomer(complaintData.getCustomer());
		complaintint.setVehicle(complaintData.getVehicle());
		complaintint.setLocation(complaintData.getLocation());
		em.persist(complaintint);
	}

	@Override
	public void updateComplaintsResolutionByManager(String complaintNum, String reasonFor, String complaintStatus,
			String customerStatus, String actionTaken, String resolutionBy) {
		logger.info("complaint num::" + complaintNum);
		logger.info("reason for num::" + reasonFor);
		logger.info("complaint sdfsdf::" + complaintStatus);
		logger.info("complaint asdasdasdasd::" + customerStatus);
		logger.info("action taken::" + actionTaken);

		Complaint complaintData = source.complaint(em).where(u -> u.getComplaintNumber().equals(complaintNum))
				.getOnlyValue(); // take the latest disposition
		complaintData.setComplaintStatus(complaintStatus);
		complaintData.setCustomerstatus(customerStatus);
		// complaintData.setIsAssigned("false");
		em.merge(complaintData);

		ComplaintInteraction complaintint = new ComplaintInteraction();
		complaintint.setComplaintStatus(complaintStatus);
		complaintint.setCustomerstatus(customerStatus);
		complaintint.setReasonFor(reasonFor);
		complaintint.setActionTaken(actionTaken);
		complaintint.setResolutionBy(resolutionBy);
		// complaintint.setIsAssigned("yes");
		complaintint.setUpdatedDate(currentDate());
		complaintint.setComplaint(complaintData);
		complaintint.setCustomer(complaintData.getCustomer());
		complaintint.setVehicle(complaintData.getVehicle());
		complaintint.setLocation(complaintData.getLocation());

		em.persist(complaintint);
	}

	public List<Complaint> getClosedComplaintsForResolution(String userLoginName) {
		long count = source.wyzUsers(em).where(u -> u.getUserName().equals(userLoginName)).count();
		// logger.info("inside resolution"+userLoginName);
		if (count > 0) {
			WyzUser wyzUserId = source.wyzUsers(em).where(u -> u.getUserName().equals(userLoginName)).getOnlyValue();
			// logger.info("wys user id in resolution"+ wyzUserId);
			long data = wyzUserId.getId();
			// logger.info("rytierytertierytuieyrtu"+data);
			return source.complaint(em)
					.where(u -> u.getComplaintStatus().equals("Closed") && u.getAssignedUserTo().getId() == data)
					.toList();
		} else {
			return new ArrayList<Complaint>();
		}
	}

	public Vehicle getSearchVehClosed(String veh_numclosed) {
		long count = source.vehicle(em).where(u -> u.getVehicleRegNo().equals(veh_numclosed)).count();
		if (count > 0) {
			logger.info("this is the veh number in :" + veh_numclosed);
			List<Vehicle> vehicleNum = source.vehicle(em).where(u -> u.getVehicleRegNo().equals(veh_numclosed))
					.toList();
			logger.info("vehical from repo" + vehicleNum.size());
			Vehicle vehicle = vehicleNum.get(0);
			Hibernate.initialize(vehicle.getCustomer());
			Hibernate.initialize(vehicle.getCustomer().getPreferredEmail());
			Hibernate.initialize(vehicle.getServiceBooked());
			Hibernate.initialize(vehicle.getCustomer().getPreferredPhone());
			Hibernate.initialize(vehicle.getCustomer().getPreferredAdress());

			return vehicle;
		} else {
			return null;
		}
	}

	public Complaint getSearchComplaintNumClosed(String complaintNumClosed) {
		Complaint complaint = source.complaint(em).where(u -> u.getComplaintNumber().equals(complaintNumClosed))
				.getOnlyValue();

		Hibernate.initialize(complaint.getComplaintInteractions());
		logger.info("complaintnumber is :" + complaint.getComplaintNumber());
		return complaint;
	}

	public void updateComplaintsResolutionClosed(String complaintNumClosed, String reasonForClosed,
			String complaintStatusClosed, String customerStatusClosed, String actionTakenClosed,
			String resolutionByClosed) {
		logger.info(" reasonForClosed : " + reasonForClosed);

		long countOfComplaintInt = source.complaintInteractions(em)
				.where(u -> u.getComplaint().getComplaintNumber().equals(complaintNumClosed)).count();

		if (countOfComplaintInt > 0) {

			List<ComplaintInteraction> listCompInt = source.complaintInteractions(em)
					.where(u -> u.getComplaint().getComplaintNumber().equals(complaintNumClosed)).toList();
			int listcount = listCompInt.size();
			ComplaintInteraction complaintInt = listCompInt.get(listcount - 1);

			logger.info("updatting complaint interCTION : " + complaintInt.getId());
			complaintInt.setComplaintStatus(complaintStatusClosed);
			complaintInt.setCustomerstatus(customerStatusClosed);
			complaintInt.setReasonFor(reasonForClosed);
			complaintInt.setActionTaken(actionTakenClosed);
			complaintInt.setResolutionBy(resolutionByClosed);

			em.merge(complaintInt);
		}
	}

	public void getResolutionData(String complaintNum, String city, String workshop, String functions, String ownership,
			String priority, String esclation1, String esclation2) {
		logger.info("complaints city" + city + "complaintNum : " + complaintNum + "ownership : " + ownership);
		/// after the assign complaints submit
		long countOfcomplaint = source.complaint(em).where(u -> u.getComplaintNumber().equals(complaintNum)).count();

		if (countOfcomplaint > 0) {

			WyzUser wyzuser_data = source.wyzUsers(em).where(u -> u.getUserName().equals(ownership)).getOnlyValue();
			// long wyzuser_dataID = wyzuser_data.getId();
			logger.info("wyz user in get resolution to assiugn compliant ::" + wyzuser_data);
			Complaint complaint_data = source.complaint(em).where(u -> u.getComplaintNumber().equals(complaintNum))
					.getOnlyValue();
			Location locationId = source.locations(em).where(u -> u.getName().equals(city)).getOnlyValue();
			Workshop workshopId = source.workshop(em).where(u -> u.getWorkshopName().equals(workshop)).getOnlyValue();
			long workshopid = workshopId.getId();
			logger.info("complaint id to update " + complaint_data.getId());
			// changed on jan 20th 5:31 pm

			// complaint_data.setComplaintNumber(complaintNum);
			complaint_data.setComplaintStatus("Assigned");
			complaint_data.setLocation(locationId);
			complaint_data.setFunctionName(functions);
			complaint_data.setPriority(priority);
			complaint_data.setLocation(locationId);
			complaint_data.setIsAssigned("true");
			// complaint_data.setWyzUser(wyzuser_data);
			complaint_data.setAssignedUserTo(wyzuser_data);
			em.merge(complaint_data);

			ComplaintAssignedInteraction complaint_assign = new ComplaintAssignedInteraction();
			complaint_assign.setCallMade("No");
			complaint_assign.setComplaint(complaint_data);
			complaint_assign.setAssignedTo(wyzuser_data);
			complaint_assign.setIsAssigned("true");
			complaint_assign.setComplaintStatus("Assigned");
			complaint_assign.setAssignedDate(currentDate());
			em.persist(complaint_assign);

			// ComplaintInteraction complaint_assign = new
			// ComplaintInteraction();
			// complaint_assign.setComplaintNumber(complaintNum);
			// complaint_assign.setComplaint(complaint_data);
			// complaint_assign.setWyzUser(wyzuser_data);
			// complaint_assign.setAssignedUser(wyzuser_data);
			// complaint_assign.setComplaintStatus("Open");
			// complaint_assign.setIsAssigned("yes");
			// complaint_assign.setFunctionName(functions);
			// complaint_assign.setAssignedDate(currentDate());
			// complaint_assign.setCompliant(complaint_data);
			// em.persist(complaint_assign);
			// complaint_assign.setLocation(locationId);
			// complaint_assign.setWorkshop(workshopId);
			// complaint_assign.setFunctionName(functions);
			// complaint_assign.setPriority(priority);

			// em.persist(complaint_assign);

			// complaint_data.setAssignedUser(wyzuser_data);
			// complaint_data.setFunctionName(functions);
			// complaint_data.setPriority(priority);
			// complaint_data.setLocation(locationId);
			// em.merge(complaint_data);

			// ComplaintInteraction complaint_assign = new
			// ComplaintInteraction();
			// // complaint_assign.set("Complaint");
			// complaint_assign.setCompliant(complaint_data);
			// complaint_assign.setWyzUser(wyzuser_data);
			// complaint_assign.setAssignedUser(wyzuser_data);
			// complaint_assign.setFunctionName(functions);
			// complaint_assign.setPriority(priority);
			// complaint_assign.setLocation(locationId);
			// em.persist(complaint_assign);
		}

	}

	@Override
	public List<Complaint> getComplaintByCustomerId(long id) {

		return source.complaint(em).where(u -> u.getCustomer().getId() == id).toList();
	}

	@Override
	public void addNewPhoneByCustomer(Long customer_id, String phone_number) {
		Customer customer_data = em.find(Customer.class, customer_id);
		Phone addphone = new Phone();
		addphone.setCustomer(customer_data);
		addphone.setIsPreferredPhone(false);
		addphone.setPhoneTye(1);
		addphone.setPhoneNumber(phone_number);
		em.persist(addphone);

	}

	public List<Complaint> getAllComplaintsForManager() {

		return source.complaint(em).where(u -> u.getComplaintStatus().equals("New")).toList();

	}

	public List<Complaint> getClosedComplaintsForManager() {

		return source.complaint(em).where(u -> u.getComplaintStatus().equals("Closed")).toList();

	}

	public List<Complaint> getClosedComplaintsAssignedForManager() {

		return source.complaint(em).where(u -> u.getComplaintStatus().equals("Assigned")).toList();

	}

	@Override
	public List<Service> getAllServiceListOfVehicle(List<Vehicle> vehicle_data) {

		List<Service> list_services = new ArrayList<Service>();
		for (Vehicle vehicle_iterate : vehicle_data) {
			long vehicleId = vehicle_iterate.getVehicle_id();
			List<Service> servicesList = source.services(em).where(u -> u.getVehicle().getVehicle_id() == vehicleId)
					.toList();
			list_services.addAll(servicesList);

		}

		long countOfService=list_services.stream().filter(u -> u.getJobCardDate()!=null).count();
		long countOflistService=list_services.size();
		
		logger.info("countOfService : "+countOfService+" countOflistService :"+countOflistService);
		
		if(countOfService == countOflistService){
			
			return list_services.stream().sorted(Comparator.comparing(Service::getJobCardDate).reversed()).collect(Collectors.toList());
		}else if(countOfService<countOflistService){
			
			List<Service> list_services_new = new ArrayList<Service>();
			
			List<Service> nulllistis=list_services.stream().filter(u -> u.getJobCardDate()==null).collect(Collectors.toList());
			
			List<Service> newlist=list_services.stream().filter(u -> u.getJobCardDate()!=null).collect(Collectors.toList());			
			List<Service> sortedlistIS=newlist.stream().sorted(Comparator.comparing(Service::getJobCardDate).reversed()).collect(Collectors.toList());
			
			logger.info("sortedlistIS : "+sortedlistIS.size()+"nulllistis : "+nulllistis);
			
			list_services_new.addAll(sortedlistIS);
			list_services_new.addAll(nulllistis);
			
			return list_services_new;
			
		}else{
			
			return list_services;
		}
		
		
		
		

	}

	@Override
	public List<Customer> getAllCustomers(String userLogindealerCode) {

		return source.customers(em).toList();
	}

	@Override
	public List<Workshop> getWorkshopList() {
		return source.workshop(em).toList();
	}

	@Override
	public List<ServiceAdvisor> getListOfAdvisorBasedOnCity(long workshopId) {
		List<ServiceAdvisor> dataList = source.serviceadvisor(em).where(u -> u.getWorkshop() != null)
				.where(u -> u.getWorkshop().getId() == workshopId).toList();
		logger.info("dataList size of service advisors : " + dataList.size());
		return dataList;
	}

	@Override
	public List<TaggingUsers> getListTaggingById(long userLocation) {

		List<TaggingUsers> listtagg = source.taggingUsers(em).where(u -> u.getLocation().getCityId() == userLocation)
				.toList();

		return listtagg;
	}

	@Override
	public List<String> getTaggingPersonByLocationDepartment(long userLocation, long departmentName) {
		logger.info("departmentName : " + departmentName);
		List<String> taggedPerson = source.complaintTypes(em).where(u -> u.getId() == departmentName)
				.select(u -> u.getTaggedUserName()).toList();

		return taggedPerson;
	}

	@Override
	public List<Address> getAddressesByCustomerId(long customerId) {

		return source.address(em).where(u -> u.getCustomer().getId() == customerId).toList();
	}

	@Override
	public void addNewCustomerInformation(long insId, long srId, Customer newCustomer, ListingForm listdata,
			Address newCustomerAdress) {

		List<String> phoneList = listdata.getPhoneList();

		if (listdata.getVehicleSoldYes() != null && listdata.getVehicleSoldYes().equals("VehicleSold Yes")) {
			newCustomer.setCustomerName(newCustomer.getCustomerFName() + newCustomer.getCustomerLName());
			newCustomer.setCreatedDate(currentDate());
			em.persist(newCustomer);

			if (insId != 0) {

				InsuranceDisposition insDispo = em.find(InsuranceDisposition.class, insId);

				insDispo.setComments("New Customer is added Name : " + newCustomer.getCustomerFName()
						+ newCustomer.getCustomerLName());
				em.merge(insDispo);

			} else if (srId != 0) {

				SRDisposition srDispo = em.find(SRDisposition.class, srId);
				srDispo.setComments("New Customer is added Name : " + newCustomer.getCustomerFName()
						+ newCustomer.getCustomerLName());

				em.merge(srDispo);

			}

			logger.info("phone list is : " + phoneList.size());

			logger.info("phoneList.get(0) : " + phoneList.get(0) + "sfdsfdf" + phoneList.get(1) + "snmddk"
					+ phoneList.get(2) + "sdjnds" + phoneList.get(3));
			if (phoneList.get(0) != "") {
				Phone addingnewPhone = new Phone();
				addingnewPhone.setPhoneNumber(phoneList.get(0));
				addingnewPhone.setCustomer(newCustomer);
				addingnewPhone.setIsPreferredPhone(true);
				em.persist(addingnewPhone);

				if (insId != 0) {

					InsuranceDisposition insDispo = em.find(InsuranceDisposition.class, insId);

					insDispo.setComments(insDispo.getComments() + " Phone : " + phoneList.get(0));
					em.merge(insDispo);
				} else if (srId != 0) {

					SRDisposition srDispo = em.find(SRDisposition.class, srId);
					srDispo.setComments(srDispo.getComments() + " Phone : " + phoneList.get(0));
					em.merge(srDispo);

				}

			}
			if (phoneList.get(1) != "") {
				Phone addingnewPhone = new Phone();
				addingnewPhone.setPhoneNumber(phoneList.get(1));
				addingnewPhone.setCustomer(newCustomer);

				em.persist(addingnewPhone);

				// customerComments.setComments(customerComments.getComments()+phoneList.get(1));
			}
			if (phoneList.get(3) != "") {
				Phone addingnewPhone = new Phone();
				addingnewPhone.setPhoneNumber(phoneList.get(2) + phoneList.get(3));
				addingnewPhone.setCustomer(newCustomer);
				em.persist(addingnewPhone);

				// customerComments.setComments(customerComments.getComments()+phoneList.get(3));
			}

			newCustomerAdress.setCustomer(newCustomer);
			newCustomerAdress.setIsPreferred(true);
			newCustomerAdress.setAddressType(Address.ADDRESS_TYPE_OFFICE);
			em.persist(newCustomerAdress);

		}

	}

	@Override
	public void addNewVehicleInformation(Vehicle newVehicle, ListingForm listData) {
		long customer_Id = listData.getCustomer_Id();

		logger.info("registration number is : " + newVehicle.getVehicleRegNo());
		if (listData.getPurchaseYes() != null && listData.getPurchaseYes().equals("Purchase Yes")) {

			Customer cust_details = em.find(Customer.class, customer_Id);

			newVehicle.setCustomer(cust_details);
			newVehicle.setCreatedDate(currentDate());
			newVehicle.setIsAssigned("false");
			em.persist(newVehicle);
			
			
			cust_details.setRegNoSMR(newVehicle.getVehicleRegNo());
			cust_details.setDbVehRegNo(newVehicle.getChassisNo());
			em.merge(cust_details);
		}

	}

	@Override
	public void updateFollowUpDoneStatus(long callInteractionId) {

		logger.info("updating the followup");
		SRDisposition updatesrDispo = source.srDisposition(em)
				.where(u -> u.getCallInteraction().getId() == callInteractionId).getOnlyValue();
		updatesrDispo.setIsFollowUpDone("Yes");
		em.merge(updatesrDispo);
	}

	@Override
	public long getCountOfServiceHistoryOfVehicle(long vehicle_id) {

		long service_list_count = source.services(em).where(u -> u.getVehicle().getVehicle_id() == vehicle_id)
				.where(u -> u.getLastServiceDate() != null).count();

		return service_list_count;
	}

	public List<UnAvailability> getRangeOfUnavailabilty(String selectAgent, String fromDate, String toDate) {

		List<UnAvailability> addExistingList = new ArrayList<UnAvailability>();

		long existingUser = source.wyzUsers(em).where(u -> u.getUserName().equals(selectAgent)).count();

		if (existingUser != 0) {
			WyzUser userIs = source.wyzUsers(em).where(u -> u.getUserName().equals(selectAgent)).getOnlyValue();

			SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date fromDateNew = new Date();
			Date toDateNew = new Date();
			try {
				fromDateNew = dmyFormat.parse(fromDate);
				toDateNew = dmyFormat.parse(toDate);
			} catch (Exception e) {
				e.printStackTrace();
			}

			List<UnAvailability> existingCounts = source.unAvailabilitys(em)
					.where(u -> u.getWyzUser().getUserName().equals(selectAgent)).toList();

			logger.info("existingCounts : " + existingCounts.size());
			boolean roasterInsert = false;
			for (UnAvailability unavia_data : existingCounts) {

				logger.info("unavilable from date : " + unavia_data.getFromDate());
				if ((fromDateNew.before(unavia_data.getFromDate()) && toDateNew.before(unavia_data.getFromDate()))
						|| (fromDateNew.after(unavia_data.getToDate()) && toDateNew.after(unavia_data.getToDate()))) {

					logger.info("inside for loop");

				} else if ((fromDateNew.equals(unavia_data.getFromDate()) && toDateNew.after(unavia_data.getToDate()))
						|| (fromDateNew.equals(unavia_data.getFromDate()) && toDateNew.before(unavia_data.getToDate()))
						|| (fromDateNew.after(unavia_data.getFromDate()) && toDateNew.before(unavia_data.getToDate()))
						|| (fromDateNew.before(unavia_data.getFromDate()) && toDateNew.after(unavia_data.getFromDate()))
						|| (fromDateNew.after(unavia_data.getFromDate()) && toDateNew.after(unavia_data.getToDate()))) {
					roasterInsert = true;
					addExistingList.add(unavia_data);

					logger.info("outside for looping");
				} else {
					logger.info("outside for loop");

				}

			}
			return addExistingList;

		} else {

			return addExistingList;

		}

	}

	@Override
	public String addRangeOfUnaviabilityOfUser(String selectAgent, String fromDate, String toDate) {
		long existingUser = source.wyzUsers(em).where(u -> u.getUserName().equals(selectAgent)).count();

		if (existingUser != 0) {
			WyzUser userIs = source.wyzUsers(em).where(u -> u.getUserName().equals(selectAgent)).getOnlyValue();

			SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date fromDateNew = new Date();
			Date toDateNew = new Date();
			try {
				fromDateNew = dmyFormat.parse(fromDate);
				toDateNew = dmyFormat.parse(toDate);
			} catch (Exception e) {
				e.printStackTrace();
			}

			List<UnAvailability> existingCounts = source.unAvailabilitys(em)
					.where(u -> u.getWyzUser().getUserName().equals(selectAgent)).toList();

			logger.info("existingCounts : " + existingCounts.size());
			boolean roasterInsert = false;
			for (UnAvailability unavia_data : existingCounts) {

				logger.info("unavilable from date : " + unavia_data.getFromDate());
				if ((fromDateNew.before(unavia_data.getFromDate()) && toDateNew.before(unavia_data.getFromDate()))
						|| (fromDateNew.after(unavia_data.getToDate()) && toDateNew.after(unavia_data.getToDate()))) {

					logger.info("inside for loop");

				} else if ((fromDateNew.equals(unavia_data.getFromDate()) && toDateNew.after(unavia_data.getToDate()))
						|| (fromDateNew.equals(unavia_data.getFromDate()) && toDateNew.before(unavia_data.getToDate()))
						|| (fromDateNew.after(unavia_data.getFromDate()) && toDateNew.before(unavia_data.getToDate()))
						|| (fromDateNew.before(unavia_data.getFromDate()) && toDateNew.after(unavia_data.getFromDate()))
						|| (fromDateNew.after(unavia_data.getFromDate()) && toDateNew.after(unavia_data.getToDate()))) {
					roasterInsert = true;
					logger.info("outside for looping");
				} else {
					logger.info("outside for loop");

				}

			}

			if (!roasterInsert) {
				UnAvailability addUnavialibilty = new UnAvailability();
				addUnavialibilty.setFromDate(fromDateNew);
				addUnavialibilty.setToDate(toDateNew);
				addUnavialibilty.setWyzUser(userIs);
				em.persist(addUnavialibilty);
				return "success";
			} else {

				return "update";
			}

		} else {

			return "failure";

		}

	}

	@Override
	public List<UnAvailability> getUnavialabilityListByUser(String selectAgent) {

		// WyzUser userdata=source.wyzuser(em).where(u ->
		// u.getUserName().equals(selectAgent)).getOnlyValue();

		// long userId=userdata.getId();

		return source.unAvailabilitys(em).where(u -> u.getWyzUser().getUserName().equals(selectAgent)).toList();
	}

	@Override
	public void deleteUnavialabilty(UnAvailability deleteData) {
		deleteData.setWyzUser(null);
		em.remove(em.merge(deleteData));

		em.close();
	}

	@Override
	public UnAvailability getUnavialabilityListByID(long id) {

		return em.find(UnAvailability.class, id);
	}

	@Override
	public void addUnavialabilty(String selectAgent, String From_Date, String To_Date) {

		long existingUser = source.wyzUsers(em).where(u -> u.getUserName().equals(selectAgent)).count();

		if (existingUser != 0) {
			WyzUser userIs = source.wyzUsers(em).where(u -> u.getUserName().equals(selectAgent)).getOnlyValue();

			SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date fromDateNew = new Date();
			Date toDateNew = new Date();
			try {
				fromDateNew = dmyFormat.parse(From_Date);
				toDateNew = dmyFormat.parse(To_Date);
			} catch (Exception e) {
				e.printStackTrace();
			}

			logger.info("new entry added");
			UnAvailability addUnavialibilty = new UnAvailability();
			addUnavialibilty.setFromDate(fromDateNew);
			addUnavialibilty.setToDate(toDateNew);
			addUnavialibilty.setWyzUser(userIs);
			em.persist(addUnavialibilty);

		}

	}

	@Override

	public UploadData setRequiredFieldsForUpload(long userId, String sheetname, String datatype, String locationdata,
			Long workshopId, String finalName, String camFromDate, String camExpDate, String finalUploadId) {

		logger.info("Before Insert");

		WyzUser userIs = em.find(WyzUser.class, userId);

		long existingCity = source.locations(em).where(u -> u.getName().equals(locationdata)).count();
		UploadData uploadNew = new UploadData();
		Vehicle vehCampaignDate = new Vehicle();

		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date toDateNewDate = new Date();
		try {
			// toDateNewDate = dmyFormat.parse(camExpDate);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (existingCity != 0) {

			Location loc = source.locations(em).where(u -> u.getName().equals(locationdata)).getOnlyValue();
			Workshop workShopID = em.find(Workshop.class, workshopId);

			uploadNew.setDataType(datatype);
			uploadNew.setSheetName(sheetname);
			uploadNew.setLocation(loc);
			uploadNew.setUploadedDate(currentDate());
			uploadNew.setUploadedDateAndTime(new Date());
			uploadNew.setWorkshop(workShopID);
			uploadNew.setFileNametimeStamp(finalName);
			uploadNew.setCampaignStartdate(camFromDate);
			uploadNew.setCampaignExpiry(camExpDate);
			uploadNew.setWyzUser(userIs);
			uploadNew.setRejectedRecords(0);
			uploadNew.setTotalRecords(0);
			uploadNew.setSuccessRecords(0);
			uploadNew.setUpload_id(finalUploadId);

			// em.persist(uploadNew);

			//
			// vehCampaignDate.setFollowUpDateSMR(toDateNewDate);
			// em.persist(vehCampaignDate);

			logger.info("Inserted successfully");

			return uploadNew;
		} else {
			uploadNew.setDataType(datatype);
			uploadNew.setSheetName(sheetname);
			uploadNew.setUploadedDate(currentDate());
			uploadNew.setUploadedDateAndTime(new Date());
			uploadNew.setFileNametimeStamp(finalName);
			uploadNew.setCampaignStartdate(camFromDate);
			uploadNew.setCampaignExpiry(camExpDate);
			uploadNew.setWyzUser(userIs);
			uploadNew.setRejectedRecords(0);
			uploadNew.setTotalRecords(0);
			uploadNew.setUpload_id(finalUploadId);

			// em.persist(uploadNew);

			// vehCampaignDate.setFollowUpDateSMR(toDateNewDate);
			// em.persist(vehCampaignDate);

			return uploadNew;

		}

		// uploadNew.setLocation(location);

	}

	@Override
	public List<Campaign> getAllCampaignList() {
		List<String> distinctNames = source.campaigns(em).select(u -> u.getCampaignType()).distinct().toList();

		List<Campaign> otherList = source.campaigns(em).toList();

		List<Campaign> listnew = new ArrayList<Campaign>();

		for (String listing : distinctNames) {

			for (Campaign filteringdata : otherList) {

				if (listing.equals(filteringdata.getCampaignType())) {

					listnew.add(filteringdata);
					break;
				}

			}

		}
		logger.info("size of filtered data " + listnew.size());
		// return source.campaigns(em).toList();
		return listnew.stream().sorted(Comparator.comparing(Campaign::getCampaignType).reversed()).collect(Collectors.toList());
		
	}

	@Override
	public List<Campaign> getCampaignNames() {

		return source.campaigns(em).where(u -> u.getCampaignType().equals("Campaign")).toList();
	}

	@Override
	public long getCountOfServiceHistory(long vehicle_id) {

		return source.services(em).where(u -> u.getVehicle().getVehicle_id() == vehicle_id).count();

	}

	@Override
	public Service getLatestServiceDataFiltering(long vehicle_id) {

		List<Service> service_list = source.services(em).where(u -> u.getVehicle().getVehicle_id() == vehicle_id)
				.where(u -> u.getLastServiceDate()!=null)
				.toList();

		Service lastService=new Service();

		if(service_list.size()!=0){
		 lastService = Collections.max(service_list, Comparator.comparing(c -> c.lastServiceDate));
		}
		Hibernate.initialize(lastService.getPsfAssignedInteraction());

		// Service latestService=service_list
		// .stream()
		// .filter(u -> u.getLastServiceDate().max(Service::lastServiceDate()))
		// .collect();

		return lastService;
	}

	@Override
	public List<Campaign> getCampaignNamesPSF() {
		return source.campaigns(em).where(u -> u.getCampaignType().equals("PSF")).toList();

	}
	
	@Override
	public List<Campaign>  getCampaignNamesBYType(String typeIs){
		
		return source.campaigns(em).where(u -> u.getCampaignType().equals(typeIs)).toList();
		
	}

	@Override
	public String getLatestServiceBookedType(long cid, long vehicle_id, Service service) {

		String modeType = "";
		long countOfServiceBooked = source.serviceBooked(em)
				.where(u -> u.getCustomer() != null && u.getVehicle() != null)
				.where(u -> u.getCustomer().getId() == cid).where(u -> u.getVehicle().getVehicle_id() == vehicle_id)
				.count();

		logger.info("countOfServiceBooked : " + countOfServiceBooked);

		if (countOfServiceBooked != 0) {

			long service_booked = source.serviceBooked(em).where(u -> u.getCustomer() != null && u.getVehicle() != null)
					.where(u -> u.getCustomer().getId() == cid).where(u -> u.getVehicle().getVehicle_id() == vehicle_id)
					.max(u -> u.getServiceBookedId());

			logger.info("service_booked id is : " + service_booked);

			ServiceBooked service_bookedData = em.find(ServiceBooked.class, service_booked);

			if (service_bookedData.getTypeOfPickup().equals("true")) {

				modeType = "Pick-Up";

				return modeType;

			} else if (service_bookedData.getTypeOfPickup().equals("Customer Drive-In")) {

				modeType = "Self Drive-in";

				return modeType;

			} else if (service_bookedData.getTypeOfPickup().equals("Maruthi Mobile Support")) {

				modeType = "MMS";

				return modeType;

			}

		}

		return "";
	}

	@Override
	public long getCustmobNo(Long wyzUser_id, String custMobNo, Long customer_Id) {

		long phoneCount = source.phones(em).where(u -> u.getCustomer() != null)
				.where(u -> u.getCustomer().getId() == customer_Id).where(u -> u.getPhoneNumber().equals(custMobNo))
				.count();

		if (phoneCount != 0) { // phone number is there

			return 0;
		} else {

			List<Phone> phoneList = source.phones(em).where(u -> u.getCustomer().getId() == customer_Id).toList();

			for (Phone phone_list : phoneList) {

				phone_list.setIsPreferredPhone(false);
				em.merge(phone_list);

			}

			Customer custData = em.find(Customer.class, customer_Id);
			Phone phoneData = new Phone();
			phoneData.setPhoneNumber(custMobNo);
			phoneData.setUpdatedBy(String.valueOf(wyzUser_id));
			phoneData.setIsPreferredPhone(true);
			phoneData.setCustomer(custData);
			em.persist(phoneData);

			return phoneData.getPhone_Id();

		}

	}

	@Override
	public void getCustEmail(Long wyzUser_id, String custEmail, Long customer_Id) {

		long emailCount = source.emails(em).where(u -> u.getEmailAddress().equals(custEmail)).count();

		if (emailCount != 0) { // email is there

		} else {

			List<Email> emailList = source.emails(em).where(u -> u.getCustomer().getId() == customer_Id).toList();

			for (Email email_list : emailList) {

				email_list.setIsPreferredEmail(false);
				em.merge(email_list);

			}

			Customer custData = em.find(Customer.class, customer_Id);
			Email emailData = new Email();
			emailData.setEmailAddress(custEmail);
			emailData.setUpdatedBy(String.valueOf(wyzUser_id));
			emailData.setIsPreferredEmail(true);
			emailData.setCustomer(custData);
			em.persist(emailData);

		}

	}

	@Override
	public List<String> getTaggingPersonByUpsellId(long userLocation, long upselIDTag) {
		logger.info("upsel id : " + upselIDTag);

		int upselData = (int) upselIDTag;
		List<String> taggedPerson = source.taggingUsers(em)
				.where(u -> u.getLocation().getCityId() == userLocation && u.getUpsellLeadId() == upselData)
				.select(u -> u.getName()).toList();

		return taggedPerson;

	}

	@Override
	public long getVehicleRegNoCount(String vehicleReg) {

		return source.vehicle(em).where(u -> u.getVehicleRegNo() != null)
				.where(u -> u.getVehicleRegNo().equals(vehicleReg)).count();
	}

	@Override
	public List<Driver> getListOfDriversBasedOnWorkshop(long workshopId) {
		List<Driver> dataList1 = source.driver(em).where(u -> u.getWorkshop() != null)
				.where(u -> u.getWorkshop().getId() == workshopId).toList();
		logger.info("dataList size of drivers : " + dataList1.size());
		return dataList1;
	}

	@Override
	public List<String> getExcelColumnsOfSelectedFormate(String upload_format) {

		return source.uploadMasterFormat(em).where(u -> u.getUpload_format_name() != null)
				.where(u -> u.getUpload_format_name().equals(upload_format)).select(u -> u.getExcel_column()).toList();
	}

	@Override
	public List<String> getComplaintStatusandCount(long cust_id) {

		logger.info("customer id is : " + cust_id);

		long openCountOfCust = source.complaint(em).where(u -> u.getCustomer() != null)
				.where(u -> u.getCustomer().getId() == cust_id).where(u -> u.getComplaintStatus().equals("Open")
						|| u.getComplaintStatus().equals("Assigned") || u.getComplaintStatus().equals("New"))
				.count();
		
		logger.info("openCountOfCust : "+openCountOfCust);
		

		long closeCountOfCustomer = source.complaint(em).where(u -> u.getCustomer() != null)
				.where(u -> u.getCustomer().getId() == cust_id).where(u -> u.getComplaintStatus().equals("Closed"))
				.count();
		
		logger.info("closeCountOfCustomer : "+closeCountOfCustomer);
		
		long countis=source.complaint(em).where(u -> u.getCustomer() != null)
		.where(u -> u.getCustomer().getId() == cust_id).count();
		
		logger.info("countis : "+countis);

		List<Complaint> lastRaisedBylist = source.complaint(em).where(u -> u.getCustomer() != null)
				.where(u -> u.getCustomer().getId() == cust_id).toList();
		
		logger.info("lastRaisedBylist : "+lastRaisedBylist.size());

		Complaint lastRaisedBy = new Complaint();

		String lastRaisedByIs = "";
		String lastResolvedByIs = "";

		if (lastRaisedBylist.size() > 0) {

			lastRaisedBy = lastRaisedBylist.stream().max(Comparator.comparing(u -> u.getId())).get();

			lastRaisedByIs = lastRaisedBy.getIssueDate().toString();

		}

		List<ComplaintInteraction> lastRresolvedBylist = source.complaintInteractions(em)
				.where(u -> u.getCustomer() != null).where(u -> u.getCustomer().getId() == cust_id).toList();

		ComplaintInteraction lastRresolvedBy = new ComplaintInteraction();

		if (lastRresolvedBylist.size() > 0) {

			lastRresolvedBy = lastRresolvedBylist.stream().max(Comparator.comparing(u -> u.getId())).get();
			lastResolvedByIs = lastRresolvedBy.getResolutionBy();
		}

		logger.info("lastRaisedBylist.size() : " + lastRaisedBylist.size() + " lastRresolvedBylist.size() : "
				+ lastRresolvedBylist.size());
		logger.info(" openCountOfCust : " + openCountOfCust + " closeCountOfCustomer : " + closeCountOfCustomer
				+ " lastRaisedBy : " + lastRaisedByIs + " lastRresolvedBy :" + lastResolvedByIs);
		
		logger.info(" openCountOfCust : " + openCountOfCust + " closeCountOfCustomer : " + closeCountOfCustomer
				+ " lastRaisedBy : " + lastRaisedByIs + " lastRresolvedBy :" + lastResolvedByIs);

		List<String> complaintUpdate = new ArrayList<String>();

		complaintUpdate.add(String.valueOf(openCountOfCust));
		complaintUpdate.add(String.valueOf(closeCountOfCustomer));
		complaintUpdate.add(lastRaisedByIs);
		complaintUpdate.add(lastResolvedByIs);

		return complaintUpdate;

	}

	@Override
	public List<ServiceTypes> getAllServiceTypeList() {

		return source.serviceTypes(em).toList();
	}

	@Override
	public String getCampaignTypeById(long id) {

		Campaign camp = em.find(Campaign.class, id);

		return camp.getCampaignType();
	}

	@Override
	public List<String> getWorkshopListByLocaList(List<String> locations) {
		// TODO Auto-generated method stub

		List<String> workList = new ArrayList<String>();

		long countOfSelLoc = locations.size();

		if (countOfSelLoc != 0) {

			for (String location : locations) {

				List<Workshop> workshops = getWorkshopListByLoca(location);
				for (Workshop sa : workshops) {
					workList.add(sa.getWorkshopName());
				}

			}

			return workList;
		} else {

			return workList;
		}

	}

	@Override
	public List<String> getCRESListByWorkshops(List<String> selectedWorkshops) {
		// TODO Auto-generated method stub

		List<String> crelist = new ArrayList<String>();

		long countOfWorkshops = selectedWorkshops.size();

		if (countOfWorkshops != 0) {

			for (String work_shop : selectedWorkshops) {

				List<WyzUser> userList = getUsersByWorkshopName(work_shop);
				for (WyzUser sa : userList) {
					crelist.add(sa.getUserName());
				}
			}

			return crelist;

		} else {
			return crelist;
		}
	}

	private List<WyzUser> getUsersByWorkshopName(String work_shop) {
		long workshopCount = source.workshop(em).where(u -> u.getWorkshopName().equals(work_shop)).count();

		List<WyzUser> user_list = new ArrayList<WyzUser>();
		if (workshopCount > 0) {
			Workshop workshop = source.workshop(em).where(u -> u.getWorkshopName().equals(work_shop)).getOnlyValue();
			Hibernate.initialize(workshop.getUsers());
			return workshop.getUsers();

		} else {

			return user_list;
		}
	}

	@Override
	public List<String> getCitiesByState(String selState) {
		// TODO Auto-generated method stub
		List<String> city_list = source.citystates(em).where(u -> u.getState().equals(selState))
				.select(u -> u.getCity()).distinct().toList();

		// TODO Auto-generated method stub
		return city_list;
	}

	@Override
	public List<String> getDistinctFunctionComplaints() {
		// TODO Auto-generated method stub

		List<String> distinctFunction = source.complaint(em).where(u -> u.getFunctionName() != null)
				.select(u -> u.getFunctionName()).distinct().toList();
		return distinctFunction;
	}
	@Override
	public List<Complaint> getComplaintsFilterDataByStatus(String filterData, String loc, String func,
			String raisedDateIs , String endDate) {

		logger.info(" filterData : " + filterData + " loc : " + loc + " func : " + func + 
				" raisedDate : " + raisedDateIs+
				" endDate : " + endDate);

		Date toDateNewDate    = null;
		Date toDateNewDateend = null;
		List<Complaint> listCompl = new ArrayList<Complaint>();
		if (filterData.equals("Assigned")) {

			List<Complaint> compl = source.complaint(em)
					.where(u -> u.getComplaintStatus().equals(filterData) || u.getComplaintStatus().equals("Open"))
					.where(u -> u.getLocation() != null).where(u -> u.getLocation().getName().equals(loc)).toList();
			if (func.equals("0")) {
				listCompl.addAll(compl);

			} else {

				listCompl = compl.stream().filter(u -> u.getFunctionName().equals(func)).collect(Collectors.toList());
			}

			// return listCompl;

		} else {
			logger.info("filterdata is::" + filterData);
			
			if(filterData.equals("0")){

			List<Complaint> compl = source.complaint(em)
					.where(u -> u.getLocation() != null).where(u -> u.getLocation().getName().equals(loc)).toList();
			if (func.equals("0")) {
				listCompl.addAll(compl);
			} else {

				listCompl = compl.stream().filter(u -> u.getFunctionName() != null && u.getFunctionName().equals(func))
						.collect(Collectors.toList());
			}
			}else{
				List<Complaint> compl = source.complaint(em).where(u -> u.getComplaintStatus().equals(filterData))
						.where(u -> u.getLocation() != null).where(u -> u.getLocation().getName().equals(loc)).toList();
				if (func.equals("0")) {
					listCompl.addAll(compl);
				} else {

					listCompl = compl.stream().filter(u -> u.getFunctionName() != null && u.getFunctionName().equals(func))
							.collect(Collectors.toList());
				}
				
			}

		}

		if (raisedDateIs.equals("0") || endDate.equals("0")) {

		} else {

			logger.info("raisedDateIs is not 0");
			SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");

			try {
				toDateNewDate = dmyFormat.parse(raisedDateIs);
				toDateNewDateend=dmyFormat.parse(endDate);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			

			List<Complaint> comlist = new ArrayList<Complaint>();
			for (int i = 0; i < listCompl.size(); i++) {
			
			if (listCompl.get(i).getRaisedDate() != null) {
				if (listCompl.get(i).getRaisedDate().equals(toDateNewDate)
						|| listCompl.get(i).getRaisedDate().equals(toDateNewDateend)) {
					logger.info("Equals raised date : " + listCompl.get(i).getRaisedDate()
							+ "toDateNewDate : " + toDateNewDate
							+ "toDateNewDate : " + toDateNewDateend);

					comlist.add(listCompl.get(i));

				}else if(listCompl.get(i).getRaisedDate().after(toDateNewDate)){
					logger.info("greater   : " + listCompl.get(i).getRaisedDate()							
							+ "toDateNewDate : " + toDateNewDate);
					
					if (listCompl.get(i).getRaisedDate().before(toDateNewDateend)) {
						logger.info("Less   : " + listCompl.get(i).getRaisedDate()							
								+ "toDateNewDateend : " + toDateNewDateend);

						comlist.add(listCompl.get(i));
					}
					
				} 

			}
			
			}
			listCompl = new ArrayList<Complaint>();
			listCompl.addAll(comlist);

		}
		return listCompl;

	}

	@Override
	public List<String> getComplaintStatusList(String city) {
		// TODO Auto-generated method stub

		List<String> tag = source.roles(em).select(u -> u.getRole()).toList();

		return tag;

	}

	@Override
	public List<Workshop> getWorkshopListByLocaId(long selectedCity) {

		Location location = em.find(Location.class, selectedCity);
		Hibernate.initialize(location.getWorkshops());
		List<Workshop> work_list = location.getWorkshops();

		return work_list;

	}

	@Override
	public Set<String> getUsersByLocandFunc(String city, String func) {
		// TODO Auto-generated method stub
		List<String> user = new ArrayList<String>();
		List<WyzUser> userslist = source.wyzUsers(em).toList();

		for (WyzUser userIs : userslist) {
			Hibernate.initialize(userIs.getRoles());

			List<Role> roles = userIs.getRoles();

			for (Role sa : roles) {

				if (sa.getRole().equals(func)) {

					user.add(userIs.getUserName());
				}
			}

		}

		Set<String> uniquelist = new HashSet<String>(user);

		return uniquelist;
	}

	@Override
	public void changeAssignmentCallBy(long id, long assignId) {
		AssignedInteraction changeAss = em.find(AssignedInteraction.class, assignId);

		logger.info(" changeAss before: " + changeAss.getWyzUser().getId());
		WyzUser user = source.wyzUsers(em).where(u -> u.getId() == (id)).getOnlyValue();
		changeAss.setWyzUser(user);
		em.persist(changeAss);
		logger.info(" changeAss after: " + changeAss.getWyzUser().getId());

	}

	@Override
	public List<FollowUpNotificationModel> getFollowUpNotification(long id, String dealercode) {
		javax.persistence.Query query = em.createNativeQuery("CALL FollowUpFilter(:id,:dealercode)",
				"FollowUpNotificationModel");
		query.setParameter("id", id);
		query.setParameter("dealercode", dealercode);
		return query.getResultList();
	}

	@Override
	public List<AssignedListForCRE> getAssignListOfCampaignAndSMRByProcedure(Date fromDateNew, Date toDateNew,
			String customerCat, String serviceCat, long campaignTypeCat, String location, long fromIndex,
			long toIndex) {
		String customer_cat = customerCat;
		String service_cat = serviceCat;

		if (customer_cat.equals("All")) {

			customer_cat = "";
		}
		if (service_cat.equals("All")) {

			service_cat = "";
		}

		javax.persistence.Query query = em.createNativeQuery(
				"CALL smrcampaign_assign_list(:incampaign_id,:start_date,:end_date,:incategory,:inservicetype, :inlocation, :startwith,:length)",
				"AssignedListForCRECampSMR");
		query.setParameter("incampaign_id", campaignTypeCat);
		query.setParameter("start_date", fromDateNew);
		query.setParameter("end_date", toDateNew);
		query.setParameter("incategory", customer_cat);
		query.setParameter("inservicetype", service_cat);
		query.setParameter("inlocation", location);
		query.setParameter("startwith", (int) fromIndex);
		query.setParameter("length", (int) toIndex);

		return query.getResultList();
	}

	@Override
	public void assignCallByManagerSMRCamp(String userdata, long scallId, String userLogindealerCode) {

		WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(userdata)).getOnlyValue();
		long assignId = scallId;

		AssignedInteraction updateAssign1 = source.assignedInteractions(em).where(u -> u.getId() == assignId)
				.getOnlyValue();
		updateAssign1.setWyzUser(user);
		updateAssign1.setDisplayFlag(true);
		updateAssign1.setLastDisposition("Not yet Called");
		em.merge(updateAssign1);

		AssignedInteraction updateAssign = source.assignedInteractions(em).where(u -> u.getId() == assignId)
				.getOnlyValue();
		
		AssignedCallsReport addRecordAssigned = new AssignedCallsReport();

		addRecordAssigned.setWyzuserId(user.getId());
		addRecordAssigned.setUploadId(updateAssign.getVehicle().getUpload_id());
		addRecordAssigned.setAssignedDate(new Date());
		addRecordAssigned.setAssignInteractionID(assignId);
		addRecordAssigned.setAssignmentType("SMR");
		addRecordAssigned.setVehicleId(updateAssign.getVehicle().getVehicle_id());
		em.persist(addRecordAssigned);


		DeletedStatusReport delStatusReport = new DeletedStatusReport();
		delStatusReport.setAssigned_id(updateAssign.getId());
		delStatusReport.setVehicle_id(updateAssign.getVehicle().getVehicle_id());
		delStatusReport.setCustomer_id(updateAssign.getCustomer().getId());
		delStatusReport.setCampaign_id(updateAssign.getCampaign().getId());
		delStatusReport.setUploaded_date(updateAssign.getUplodedCurrentDate());
		delStatusReport.setWyzuser_id(user.getId());
		em.persist(delStatusReport);
		
		VehicleSummaryCube vehicleSummaryCube = new VehicleSummaryCube();
		vehicleSummaryCube.setChassisNo(updateAssign.getVehicle().getChassisNo());
		vehicleSummaryCube.setVehicleRegNo(updateAssign.getVehicle().getVehicleRegNo());
		vehicleSummaryCube.setPhoneNumber(updateAssign.getCustomer().getPreferredPhone().getPhoneNumber());
		vehicleSummaryCube.setCustomerName(updateAssign.getCustomer().getCustomerName());
		vehicleSummaryCube.setCampaign(updateAssign.getCampaign().getCampaignName());
		vehicleSummaryCube.setServiceDueDate(updateAssign.getVehicle().getNextServicedate());
		vehicleSummaryCube.setServiceDueType(updateAssign.getVehicle().getNextServicetype());
		vehicleSummaryCube.setUploadedDate(updateAssign.getUplodedCurrentDate());
		vehicleSummaryCube.setAssignedDate(updateAssign.getUplodedCurrentDate());
		vehicleSummaryCube.setAssignedCre(user.getUserName());
		long v1 = updateAssign.getVehicle().getVehicle_id();
		if (getLatestServiceDataFiltering(v1) != null) {
			vehicleSummaryCube.setRepairOrderNo(getLatestServiceDataFiltering(updateAssign.getVehicle().getVehicle_id()).getJobCardNumber());
			vehicleSummaryCube.setRoDate(getLatestServiceDataFiltering(updateAssign.getVehicle().getVehicle_id()).getJobCardDate());
		}
		vehicleSummaryCube.setVehicle_id(updateAssign.getVehicle().getVehicle_id());
		vehicleSummaryCube.setCustomer_id(updateAssign.getCustomer().getId());
		vehicleSummaryCube.setAssignedWyzuserID(user.getId());
		vehicleSummaryCube.setAssignedInteractionID(updateAssign.getId());
		vehicleSummaryCube.setUploadVehicle_id(Long.parseLong(updateAssign.getVehicle().getUpload_id()));
		em.persist(vehicleSummaryCube);

	}

	@Override
	public long getAssignListOfCampaignAndSMRByProcedureCount(Date fromDateNew, Date toDateNew, String customerCat,
			String serviceCat, long campaignTypeCat, String cityId) {

		String customer_cat = customerCat;
		String service_cat = serviceCat;

		if (customer_cat.equals("All")) {

			customer_cat = "";
		}
		if (service_cat.equals("All")) {

			service_cat = "";
		}

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("smrcampaign_assign_list_count");

		sQuery.registerStoredProcedureParameter("incampaign_id", Long.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("start_date", Date.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("end_date", Date.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("incategory", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inservicetype", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inlocation", String.class, ParameterMode.IN);

		sQuery.setParameter("incampaign_id", campaignTypeCat);
		sQuery.setParameter("start_date", fromDateNew);
		sQuery.setParameter("end_date", toDateNew);
		sQuery.setParameter("incategory", customer_cat);
		sQuery.setParameter("inservicetype", service_cat);
		sQuery.setParameter("inlocation", cityId);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();

	}

	@Override
	public long getAssignListOfPSFByProcedureCount(Date fromDateNew, Date toDateNew, String customerCat,
			String serviceCat, long campaignNamePSF,String modelname,String cityId,String workshopname) {

		String customer_cat = customerCat;
		String service_cat = serviceCat;

		if (customer_cat.equals("All")) {

			customer_cat = "";
		}
		if (service_cat.equals("All")) {

			service_cat = "";
		}
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("psfassignlist_count");

		sQuery.registerStoredProcedureParameter("campaignNamePSF", Long.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("fromDateNew", Date.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("toDateNew", Date.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("customer_cat", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("service_cat", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inmodel", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inlocation", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inworkshop_id", String.class, ParameterMode.IN);
		
		sQuery.setParameter("campaignNamePSF", campaignNamePSF);
		sQuery.setParameter("fromDateNew", fromDateNew);
		sQuery.setParameter("toDateNew", toDateNew);
		sQuery.setParameter("customer_cat", customer_cat);
		sQuery.setParameter("service_cat", service_cat);
		sQuery.setParameter("inmodel", modelname);
		sQuery.setParameter("inlocation", service_cat);
		sQuery.setParameter("inworkshop_id", modelname);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<Role> getRoles() {
		return source.roles(em).toList();
	}

	@Override
	public CallInteraction getCallInteractionById(long callInteractionId) {

		
		return source.callinteractions(em).where( u -> u.getId() == callInteractionId).getOnlyValue();
	}

	@Override
	public Customer getCustomerById(long customerId) {
		// TODO Auto-generated method stub
		return em.find(Customer.class, customerId);
	}
	
	@Override
	public List<CallInteraction> getAllCallInteraction(long startId, long endId){

	List<CallInteraction> callList= source.callinteractions(em).
	where(u -> u.getMediaFileLob()!=null)
	.where(u-> u.getCallType().equals("OUTGOING"))	
	.toList();
	
	List<CallInteraction> callListis=callList.stream()
			.filter(u -> u.getId() >= startId && u.getId()<= endId)
			.collect(Collectors.toList()); 
		
	logger.info("callListis : "+callListis.size());
	
	return callListis;
	}
	
	@Override
	public void updateMediaByteData(CallInteraction callInt){
		em.merge(callInt);
	}
	
	@Override
	public CallInteraction getCallInteractionByBookingId(long bookedId) {
		// TODO Auto-generated method stub
		return source.callinteractions(em).where(u -> u.getServiceBooked().getServiceBookedId()==bookedId).getOnlyValue();
	}
	
	//Insurance agent branch commit and merge
	//Insurance agent branch commit and rebase

	@Override
	public List<Campaign> getAllCampaignListPSF(){
		
		return source.campaigns(em).where(u->u.getCampaignType().equals("PSF")).toList();
	}
	
	@Override
	public List<Campaign> getAllCampaignListInsurance(){
		
		return source.campaigns(em).where(u->u.getCampaignType().equals("Insurance")).toList();
	}

	@Override
	public long checkIfSearchNameExists(String searchnamevalue) {
		// TODO Auto-generated method stub
		return source.savedSearchs(em).where(u->u.getSavedName().equals(searchnamevalue)).count();
	}

	@Override
	public CallInteraction getCallinteractionByUniqueId(int uniqueId) {
		// TODO Auto-generated method stub
		
		long countOfExist=source.callinteractions(em).where(u -> u.getUniqueidForCallSync() == uniqueId).count();
		
		CallInteraction callint=new CallInteraction();
		if(countOfExist>0){
		
			List<CallInteraction> callintList=source.callinteractions(em).where(u -> u.getUniqueidForCallSync() == uniqueId).toList();
			if(countOfExist==1){
				
				callint=callintList.get(0);
								
			}else{
				int lastindex=(int) (countOfExist-1);
				callint=callintList.get(lastindex);
			}
			
		}
		
		return callint;
		
	}
	
	@Override
	public List<String> getCampaignNamesAll() {

		return source.campaigns(em).select(u -> u.getCampaignName()).distinct().toList();

	}
}