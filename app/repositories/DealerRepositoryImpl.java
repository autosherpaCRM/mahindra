package repositories;

import play.Logger.ALogger;
import play.data.Form;
import configs.JinqSource;
import models.Dealer;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created by enda on 31/08/15.
 */
@Repository("dealerRepository")
@Transactional
public class DealerRepositoryImpl implements DealerRepository {
	ALogger logger = play.Logger.of("application");

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private JinqSource source;
    
    
    
    @Override
	public Dealer addDealerData(Dealer adddealer) {
    	
    	String query="USE wyzconnectadccweb;";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

	        @Override
	        public void execute(Connection connection){
	        	Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution=smt.execute(query);
					
					em.persist(adddealer);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					logger.info("SQL EXception");
				}
				
	        }
	        
		});
    	
		
		return adddealer;
	}
    @Override
    public  List<Dealer> getAllDealer(){
    	
    	String query="USE wyzconnectadccweb;";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

	        @Override
	        public void execute(Connection connection) {
	        	Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution=smt.execute(query);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				 
				
	        }});
		
		List<Dealer> dealerData=source.dealers(em).toList();    	
        return dealerData;
    }
    @Override
    public Dealer getDealerById(Long did){
    	
        Dealer dataDealerById=em.find(Dealer.class,did);
        return dataDealerById;
    }
    @Override
    public Dealer deleteDealer(Long did){
    	
        Dealer deleteDealerById=em.find(Dealer.class,did);
        em.remove(deleteDealerById);
        return deleteDealerById;
    }
    @Override
    public Dealer editDealer(Long did){
    	
        Dealer editDealerById=em.find(Dealer.class,did);
        return editDealerById;
        
    }
    @Override
    public Dealer posteditedDealerData(Long did,Dealer dealer){
    	
        
        Form<Dealer> form=Form.form(Dealer.class).bindFromRequest();
	    Dealer deal=form.get();
	    
	    Dealer editDealerById=em.find(Dealer.class,did);
	    if(editDealerById!=null) 
        {
	    editDealerById.setDealerId(deal.getDealerId());
	    editDealerById.setDealerName(deal.getDealerName());
	    editDealerById.setDealerAddress(deal.getDealerAddress());
	    editDealerById.setNoOfUsers(deal.getNoOfUsers());
	    editDealerById.setDealerCode(deal.getDealerCode());
        }
	    return dealer;
    }
	@Override
	public String getDealerCodebyAdmin(String dealercode) {
		
		Query query = em.createQuery("SELECT a.dealerName FROM Dealer a where dealerCode='"+dealercode+"'");
		String data=(String)query.getSingleResult();
		
		
		return data;
	}
	
	@Override
	public String getOEMbyDealer() {
		// TODO Auto-generated method stub
		return source.dealers(em).select(u ->u.getOEM()).getOnlyValue();
	}
	
        
}
