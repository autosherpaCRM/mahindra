/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.List;
import java.util.Map;

import javax.inject.Named;

import models.Customer;
import models.Driver;
import models.MessageTemplete;
import models.SMSInteraction;
import models.Vehicle;
import models.WyzUser;

@Named
/**
 *
 * @author wct-09
 */
public interface SMSTriggerRespository {

	public void SMSTriggerBulk(String userLogindealerCode, long vehicleId_SB,
			MessageTemplete message, String smsUrl);

	

   
    
	public void SMSTriggerBulkComplaints(String userLogindealerCode, 
            String complaintNumber, String customerName, String vehicleRegNo, String customerPhone, 
            MessageTemplete message, String smsUrl);
	
	
	public boolean sendCustomSMS(long schcallid,String message,String type,WyzUser userdata);
	
	public List<SMSInteraction> getAllSMSInteracOfCustomer(long customerId);
	
	public boolean sendComplaintSMS(String customerPhone,Vehicle veh_new,Customer cut_new,String message, String type, WyzUser userData);
	
	public void sendBulkSMSAtOnce(List<Long> vehList,String smsTemplete,long userId,String tenentCode,String type);
	
	boolean SendBulkSMSToUsers(String userLoginName, Map<String, String> dataList, long smstemplateId);
	
	public boolean sendDriverSMS(Driver driver_link, String message, String string, WyzUser wyz);

}
