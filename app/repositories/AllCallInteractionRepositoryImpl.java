package repositories;

import java.io.IOException;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;

import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import configs.JinqSource;
import controllers.webmodels.CallInteractionHistory;
import controllers.webmodels.CallLogDispositionLoad;
import controllers.webmodels.MediaModel;
import models.CallDispositionData;
import models.CallInteraction;
import play.Logger.ALogger;
import play.db.DB;
import play.mvc.Result;
import java.io.*;
import java.nio.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Repository("allCallInteractionRepository")
@Transactional
public class AllCallInteractionRepositoryImpl extends play.mvc.Controller implements AllCallInteractionRepository{
	
	ALogger logger = play.Logger.of("application");
	
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;
	int count = 0;
	
	public List<CallInteractionHistory> getallcallHistory(String manager) {
		
	       
        javax.persistence.Query query = em.createNativeQuery("CALL new_report(:mgr_user_name)","CallInteractionHistory");
        query.setParameter("mgr_user_name", manager);
		return query.getResultList();
        
    }
	@Override
	public List<CallInteractionHistory> getAllCallInteractionDetails(String userName,Date fromdate,Date todate,String filter_crename,String filter_disposition,String filter_locations) throws ParseException {
		
		logger.info("got all the parameters required : "+fromdate+" "+todate+" "+filter_crename+" "+filter_disposition+" "+filter_locations);
		
		List<CallInteractionHistory> allcallInteraction = getallcallHistory(userName);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date callmadedateandtime = null;
		if(fromdate == null && todate != null){
			List<CallInteractionHistory> temp_cih = new ArrayList<CallInteractionHistory>();
			for(CallInteractionHistory cih : allcallInteraction){
				callmadedateandtime = format.parse(cih.getCallMadeDateAndTime());
				if(callmadedateandtime.before(todate)){
					temp_cih.add(cih);
				}
			}
			allcallInteraction = temp_cih;
			temp_cih = null;
		}else if(fromdate != null && todate == null){
			List<CallInteractionHistory> temp_cih = new ArrayList<CallInteractionHistory>();
			for(CallInteractionHistory cih : allcallInteraction){
				callmadedateandtime = format.parse(cih.getCallMadeDateAndTime());
				if(callmadedateandtime.after(fromdate)){
					temp_cih.add(cih);
				}
			}
			allcallInteraction = temp_cih;
			temp_cih = null;
		}else if(fromdate != null && todate != null){
			List<CallInteractionHistory> temp_cih = new ArrayList<CallInteractionHistory>();
			for(CallInteractionHistory cih : allcallInteraction){
				callmadedateandtime = format.parse(cih.getCallMadeDateAndTime());
				if(callmadedateandtime.after(fromdate) && callmadedateandtime.before(todate)){
					temp_cih.add(cih);
				}
			}
			allcallInteraction = temp_cih;
			temp_cih = null;
			
		}
		
		
		List<String> filter_cre = null ;
		if((!filter_crename.equals("undefined"))){
		String[] fc = filter_crename.split(",");
		filter_cre = Arrays.asList(fc);
		}
		List<String> filter_dispo = null;
		if((!filter_disposition.equals("undefined"))){
			String[] fc = filter_disposition.split(",");
			filter_dispo = Arrays.asList(fc);
		}
		
		List<String> filter_loc = null;
		if((!filter_locations.equals("undefined"))){
			String[] fc = filter_locations.split(",");
			filter_loc = Arrays.asList(fc);
		}
		
		if (allcallInteraction.size() != 0 && (!filter_crename.equals("")) && (!filter_crename.equals("undefined"))) {
			List<CallInteractionHistory> temp_callinteraction = new ArrayList<CallInteractionHistory>();
			for (CallInteractionHistory aci : allcallInteraction) {
				
				if (filter_cre.contains(aci.getCre_Name())) {
					temp_callinteraction.add(aci);
				}
			}
			allcallInteraction = temp_callinteraction;
			temp_callinteraction = null;
		}
		if(allcallInteraction.size() != 0 && (!filter_disposition.equals("")) && (!filter_disposition.equals("undefined"))){
			List<CallInteractionHistory> temp_callinteraction = new ArrayList<CallInteractionHistory>();
			for (CallInteractionHistory aci : allcallInteraction) {
				
				if (filter_dispo.contains(aci.getSecondary_dispostion())) {
					temp_callinteraction.add(aci);
				}
			}
			allcallInteraction = temp_callinteraction;
			temp_callinteraction = null;
			
		}
		if(allcallInteraction.size() != 0 && (!filter_locations.equals("")) && (!filter_locations.equals("undefined"))){
			List<CallInteractionHistory> temp_callinteraction = new ArrayList<CallInteractionHistory>();
			for (CallInteractionHistory aci : allcallInteraction) {
				
				if (filter_loc.contains(aci.getLocation())) {
					temp_callinteraction.add(aci);
				}
			}
			allcallInteraction = temp_callinteraction;
			temp_callinteraction = null;
			
		}
		
		return allcallInteraction;
	}
	
	@Override
	public List<String> getAllCreNames(String userName){
		
		List<String> allcre = source.wyzUsers(em).where(t->t.getCreManager().equals(userName)).where(t->t.getRole().equals("CRE")).select(t->t.getUserName()).toList();
		
		return allcre;
		
	}
	@Override
	public List<String> getAllLocations(){
		List<String> allLocations = source.locations(em).select(t->t.getName()).toList();
		return allLocations;
		
	}
	@Override
	public List<CallDispositionData> getAllDispositions(){
		List<CallDispositionData> allDispositions = new ArrayList<CallDispositionData>();
		List<Long> dispo_long = source.srDisposition(em).where(t -> t.getCallDispositionData()!=null).select(t->t.getCallDispositionData().getId()).toList(); 
		Set<Long> dispo_ids = new HashSet(dispo_long);
		List<Long> dispo_list = new ArrayList(dispo_ids);
		for(long dispo_id : dispo_list){			
						
			CallDispositionData dispo_name = source.callDispositionDatas(em).where(t->t.getId()==dispo_id).getOnlyValue();
			allDispositions.add(dispo_name);
		}
		return allDispositions;
	}
	@Override
	public byte[] getMediaFilefromDB(String calldate,String calltime) throws IOException{
		long id = source.callinteractions(em).where(t->t.getCallDate().equals(calldate)).where(t->t.getCallTime().equals(calltime)).select(t->t.getId()).getOnlyValue();
		byte[] rs = getMediaFileMR(id);
		return rs;
	}
	@Override
	public byte[] getMediaFileMR(long callInteractionId)  throws IOException{
		
		javax.persistence.Query query = em.createNativeQuery(
				"CALL media_download_mr(:callInteractionId)","MediaModel");
		query.setParameter("callInteractionId", (int)callInteractionId);
		List<MediaModel> media=query.getResultList();
		byte[] bytes=media.get(0).getMediaFileLob();
		return bytes;
		
			
	}
	
	@Override
	public List<String> getCRENAmeAndNumber(String calldate, String calltime) {
		// TODO Auto-generated method stub
		
		CallInteraction callIs = source.callinteractions(em).where(t->t.getCallDate().equals(calldate)).where(t->t.getCallTime().equals(calltime)).getOnlyValue();
		
		List<String> data=new ArrayList<String>();
		
		data.add(callIs.getWyzUser().getUserName());
		data.add(callIs.getCustomer().getPreferredPhone().getPhoneNumber());
		
		return data;
	}
	
	@Override
		public List<String> getAllUsers(){
			
			List<String> allusers = source.wyzUsers(em).select(t->t.getUserName()).toList();
			return allusers;
		}
	@Override
    public List<String> getAllWorkshops(){

		List<String> allWorkshops = source.workshop(em).select(t->t.getWorkshopName()).toList();
		return allWorkshops;
	
	}

@Override
public String getWorkshopIdsMulti(String workshopstring) {
	List<String> listWS = new ArrayList<>();
			
	List<String> selectedWS = Arrays.asList(workshopstring.split(","));
	
	//logger.info("workshop in in call histopry repo getWorkshopIdsMulti:"+workshopstring+"length : "+selectedWS.size());
	
	if(selectedWS.size() > 0){
		
		for(String w:selectedWS){
			
			
			
			if(w.equals("")){

		 	}else{
		 		logger.info("inside for :"+w+"us");
		 		long wsIds = source.workshop(em).where(t->t.getWorkshopName().equals(w)).select(t->t.getId()).getOnlyValue();
				  listWS.add(String.valueOf(wsIds));
				
		 	}
		  
		}
		logger.info(listWS.stream().collect(Collectors.joining(",")));
		return listWS.stream().collect(Collectors.joining(","));
		
	}else{
		return "";
	}
	
			
	
}
@Override
public List<String> getWorkshopMulti(List<String> selectedworkshop) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public List<String> getWorkshopListMulti(List<String> selectedLocations) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public String getCREMulti(String creidsare) {
	
	List<String> listWS = new ArrayList<>();
	
	List<String> selectedWS = Arrays.asList(creidsare.split(","));
	
	//logger.info("workshop in in call histopry repo getWorkshopIdsMulti:"+creidsare+"length : "+selectedWS.size());
	
	if(selectedWS.size() > 0){
		
		for(String w:selectedWS){
			
			
			
			if(w.equals("")){

		 	}else{
		 		logger.info("inside for :"+w+"us");
				 long wsIds = source.wyzUsers(em).where(t->t.getUserName().equals(w)).select(t->t.getId()).getOnlyValue();
				  listWS.add(String.valueOf(wsIds));
				
		 	}
		  
		}
		logger.info(listWS.stream().collect(Collectors.joining(",")));
		return listWS.stream().collect(Collectors.joining(","));
		
	}else{
		return "";
	}

}	
	}
