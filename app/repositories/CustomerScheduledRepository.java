package repositories;

import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import models.Customer;

@Named

public interface CustomerScheduledRepository {

	Customer getUserByCustomerMobileNo(String customerMobileNo);
	
	Customer addCustomerData(Customer custScheduled);
	
	List<Customer> getAllCustScheduleds();
	
	Customer editCustomer(Long cid);	
    Customer getUserById(Long cid);
    Customer posteditedData(Long cid,Customer customer);
    Customer deleteCustomer(Long cid);
}
	