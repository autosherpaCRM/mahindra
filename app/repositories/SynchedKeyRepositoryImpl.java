package repositories;



import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import models.SynchedKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import configs.JinqSource;

@Repository("synchedKeyRepository")
@Transactional
public class SynchedKeyRepositoryImpl implements SynchedKeyRepository {

	@PersistenceContext
    private EntityManager em;

    @Autowired
    private JinqSource source;
    
   
	
	@Override
	public boolean checkIfSynched(String key,String dealerDB) {
		
		
		long count = source.synchedKey(em).where(k -> k.firebaseKey.equals(key)).count();
		
		if(count == 0) return false;
		return true;
	}

	@Override
	public void addToSynchedKeyList(String key,String dealerDB) {
		
		SynchedKey fKey = new SynchedKey();
		fKey.setFirebaseKey(key);
		em.persist(key);
	}

}
