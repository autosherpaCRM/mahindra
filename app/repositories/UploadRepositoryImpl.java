package repositories;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import configs.JinqSource;
import models.ColumnDefinition;
import models.FormData;
import models.Location;
import models.Service;
import models.Upload;
import models.UploadType;
import models.Vehicle;
import models.Campaign;
import play.Logger.ALogger;

@Repository("uploadRepository")
@Transactional
public class UploadRepositoryImpl implements UploadRepository {

	ALogger logger = play.Logger.of(UploadRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Override
	public List<ColumnDefinition> getColumnDefinitions(String uploadType) {
		UploadType upType = source.uploadTypes(em).where(ut -> ut.getUploadTypeName().equals(uploadType))
				.getOnlyValue();
		logger.debug("Obtained upload type:" + uploadType);

		Hibernate.initialize(upType.getDefinitions());
		List<ColumnDefinition> colDefs = upType.getDefinitions();

		logger.debug("Total colum definitions obtained:" + colDefs.size());
		return colDefs;
	}

	@Override
	public void addUpload(Upload upload) {

		em.persist(upload);
		em.flush();
	}

	@Override
	public UploadType getUploadType(String uploadType) {

		return source.uploadTypes(em).where(ut -> ut.getUploadTypeName().equals(uploadType)).getOnlyValue();
	}

	@Override
	public List<Upload> getLast3DaysUploads(String userName, Date currentDate, String uploadType) {

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(currentDate);

		cal.add(Calendar.DATE, -3);

		Date pastDate = cal.getTime();

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		logger.debug("Getting all records after date:" + format.format(pastDate));

		return source.uploads(em)
				.where(up -> up.getUploadType().getUploadTypeName().equals(uploadType)
						&& up.getUploadedBy().getUserName().equals(userName)
						&& (up.getUploadedDateTime().after(pastDate) && !up.getProcessingStatus().equals("STARTED")))
				.toList();
		//
		//
	}

	@Override
	public UploadType getDefaultUploadType() {

		return source.uploadTypes(em).findFirst().get();
	}

	@Override
	public List<UploadType> getUploadTypes() {

		return source.uploadTypes(em).toList();
	}

	@Override
	public List<Upload> getLast3DaysWithIgnore(String userName, Date currentDate, String uploadType,
			List<Upload> ignoreList) {

		List<Upload> originalList = getLast3DaysUploads(userName, currentDate, uploadType);

		originalList.removeAll(ignoreList);

		return originalList;
	}

	@Override
	public Upload getUpload(long id) {
		Upload up = em.find(Upload.class, id);
		Hibernate.initialize(up.getFormDataList());

		logger.info("upload id formdata size  is  : " + up.getFormDataList().size());
		em.flush();
		return up;
	}

	@Override
	public void deleteUpload(long id) {

		Upload upload = em.find(Upload.class, id);
		em.remove(upload);
		em.flush();

	}

	@Override
	public void updateProcessingStarted(long id) {

		Upload upload = em.find(Upload.class, id);
		upload.setProcessingStarted(true);
		upload.setProcessingStatus(Upload.PROCESSING_STARTED);
		em.flush();

	}

	@Override
	public List<Upload> getAllUploads(String uploadType) {
		
		List<Upload> list_uploads=source.uploads(em).where(up -> up.getUploadType().getUploadTypeName().equals(uploadType)).toList();
		
		return list_uploads.stream().sorted(Comparator.comparing(Upload::getUploadedDateTime).reversed()).collect(Collectors.toList());

		 
	}

	@Override
	public List<Location> getLocationList() {
		// TODO Auto-generated method stub
		return source.locations(em).toList();
	}

	@Override
	public void saveFormDataList(List<FormData> parameterList, Upload upload) {
		// TODO Auto-generated method stub
		for (FormData sa : parameterList) {

			if (sa.getValue().equals("")) {
				//parameterList.remove(sa);

			} else {
				logger.info("sa " + sa.getValue() + "sa name : " + sa.getName());
				sa.setUpload(upload);

			}

		}

	}

	@Override
	public List<Upload> getAllUploadsById(long upId) {
		// TODO Auto-generated method stub
		return source.uploads(em).where(up -> up.getId() == upId).toList();
	}
	
	@Override
	public Upload getUploadTypeBasedOnId(long upId) {
		// TODO Auto-generated method stub
		Upload uploadData= source.uploads(em).where(up -> up.getId() == upId && up.getUploadType().getUploadDisplayName() != null).getOnlyValue();
		return uploadData;
	}

	@Override
	public List<Vehicle> getVehicleBasedOnUpId(long upId) {
		logger.info("upload id inside getVehicleBasedOnUpId:"+ upId);
		// TODO Auto-generated method stub
		String uploadId=String.valueOf(upId);
			return source.vehicle(em).where(up -> up.getUpload_id().equals(uploadId)).toList();
	}

	@Override
	public FormData getCampaignNameByUploadID(long upId) {
		// TODO Auto-generated method stub
		long formdatacount    =  source.formdata(em).where(fd -> fd.getUpload().getId()== upId && fd.getName().equals("campaignName")).count();
		FormData formdatainfo = new FormData();
		if(formdatacount > 0){
			formdatainfo      = source.formdata(em).where(fd -> fd.getUpload().getId()== upId && fd.getName().equals("campaignName")).getOnlyValue();
			return formdatainfo;

		}else{
			return null;

			
		}
	}

	@Override
	public Campaign getcampaignIdByFormData(long formvalueid) {
		// TODO Auto-generated method stub
		long campCount =  source.campaigns(em).where(c -> c.getId() == formvalueid && c.getCampaignTypeIdValue() != null).count();
		Campaign camp = new Campaign();
		if(campCount > 0){
			camp =  source.campaigns(em).where(c -> c.getId() == formvalueid && c.getCampaignTypeIdValue() != null).getOnlyValue();	
			return camp;
		}else{
			return null;

			
		}
	}

}
