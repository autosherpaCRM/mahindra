package repositories;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.*;

import configs.JinqSource;
import play.Logger.ALogger;
import controllers.webmodels.ServiceAdvisorIdName;
import utils.WyzCRMCache;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("autoSelectionSARepository")
@Transactional
public class AutoSelectionSARepoImpl implements AutoSelectionSARepository {

	ALogger logger = play.Logger.of("application");
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;
	
	@Autowired(required=true)
	private WyzCRMCache wyzCache;
	
	@Override
	public void getAllSaDetails(String dealercodeIs){
		logger.info("Inside Repo initial setup");
		javax.persistence.Query query = em.createNativeQuery("CALL prioritised_salist()","ServiceAdvisorIdName");
		
		List<ServiceAdvisorIdName> saList =  query.getResultList();
		
		Map<Date, Map<Long, List<ServiceAdvisorIdName>>> mapsaList = saList.stream()
			    .collect(Collectors.groupingBy(ServiceAdvisorIdName::getDate,
			        Collectors.groupingBy(ServiceAdvisorIdName::getWorkshopId)));
		
		mapsaList.forEach((date,mapWorkshopId) -> {
			mapWorkshopId.forEach((workshopId, saData) -> {
				wyzCache.setupServiceAdvisorQueue(date,workshopId,saData,dealercodeIs);
			});
		});
	}
	
	@Override
	public List<ServiceAdvisorIdName> getSaDetails(Date date, Long workshopId) {

		javax.persistence.Query query = em.createNativeQuery("CALL prioritised_single_sa(:date,:workshopId)","ServiceAdvisorIdName");
		
		query.setParameter("date", date);
        query.setParameter("workshopId", workshopId); 
            
        return query.getResultList();
	}

	
}
