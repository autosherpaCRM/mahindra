package repositories;

import java.util.Date;
import java.util.List;

import models.AppointmentBooked;
import models.CallDispositionData;
import models.CallInteraction;
import models.InsuranceDisposition;
import models.InsuranceExcelError;
import models.ListingForm;
import models.InsuranceAssignedInteraction;
import models.Campaign;
import models.Insurance;
import models.ShowRooms;
import models.InsuranceAgent;
import controllers.webmodels.CallLogAjaxLoadInsurance;
import controllers.webmodels.CallLogDispositionLoadInsurance;
import controllers.webmodels.CallRecordingDataTable;
import controllers.webmodels.FollowUpNotificationModel;
import controllers.webmodels.NewAddress;

public interface InsuranceRepository {

	void addInsurCallInter(ListingForm listData, CallInteraction call_interaction, CallDispositionData dispo_data,
			InsuranceDisposition insurDispo_data, String userLogindealerCode);

	void addBookAppointmentOfInsurance(ListingForm listData, CallInteraction call_interaction,
			CallDispositionData dispo_data, InsuranceDisposition insurDispo_data, AppointmentBooked bookAppointment,
			String userLogindealerCode, ShowRooms showroom, Insurance insuranceData);

	void addUpsellLeadOfInsurance(InsuranceDisposition insurDispo_data, ListingForm listData, long vehicalId);

	CallInteraction getLatestInterOfVehicleFromInsurance(long vehicle_id);

	List<InsuranceAssignedInteraction> getAllAssignedListByUser(long userLoginId);

	long getAllInsuranceAssignedInteraction(long id,String campaignType,String fromDueDate,String toDueDate,String renewalType,String searchPattern);

	List<CallLogAjaxLoadInsurance> assignedInsuranceListOfUser(long id,
			String campaignType,String fromDueDate,String toDueDate,String renewalType,String searchPattern,
			long fromIndex,long toIndex);

	long getAllInsuranceDispoInter(String fromDueDate,String toDueDate,String campaignType,String renewalType,String searchPattern,long id,long typeOfdispo);

	List<CallLogDispositionLoadInsurance> dispoInsuranceListOfUser(String fromDueDate , String toDueDate, String campaignType , String renewalType, String searchPattern, long id, long typeOfdispo,
			long fromIndex, long toIndex);

	long getAllInsuranceNonContactInter(String fromDueDate,String toDueDate,String campaignType,String renewalType,String searchPattern,long id,long typeOfdispo,String droppedcount,String lastdispo);

	List<CallLogDispositionLoadInsurance> nonContactInsuranceListOfUser(String fromDueDate,String toDueDate,String campaignType,String renewalType,String searchPattern,long id,long typeOfdispo,String droppedcount,String lastdispo,long fromIndex,long toIndex);

	List<Campaign> getCampaignNamesOfinsurance();

	List<Campaign> getCampainListOfInsurance();

	List<Double> getODPercentageByRestParams(String cubicCap, String vehAge, String zoneid);

	List<ShowRooms> getShowRoomListData();

	List<InsuranceExcelError> getErrorOccuredDataInsurance(String uploadId);

	List<CallLogAjaxLoadInsurance> getAssignedListOfInsurance(Date fromDateNew, Date toDateNew, long campaignTypeCat, long campaignNameCat,
			String cityName, long workshopName);

	void assignCallByManagerForInsurance(String userdata, long insAssigId, String userLogindealerCode);

	long getLatestInsuranceOfVehicle(long vehicle_id);

	Insurance getLatestInsuranceData(long vehicle_id);

	List<AppointmentBooked> getAppointMentsByCustomerId(long customerId);

	List<InsuranceAgent> getAllInsuranceAgents();

	void addNewAddressToCustomer(NewAddress newAdressData, long customer_Id);

	List<String> getAllDistinctStates();

	List<FollowUpNotificationModel> getFollowupNotificationOfInsu(long id, String dealercode);

	long getAllRecordingHistoryCount(String userLoginName, String startDate, String endDate, String calltype,
			String campainName, String calledStatus, String dispoType, String users, String pattern, int modelType);

	List<CallRecordingDataTable> getAllRecordingHistoryData(String userLoginName, String fromDateNew, String toDateNew,
			String callType, String campaignName, String creInitiated, String disposition, String searchPattern,
			String usernameslist, int modelType, long fromIndex, long toIndex);

	long getOtherAllRecordingHistoryCount(String userLoginName, String fromDateNew, String toDateNew,
			String usernameslist, String callType, String searchPattern);

	List<CallRecordingDataTable> getOtherAllRecordingHistoryData(String userLoginName, String fromDateNew,
			String toDateNew, String usernameslist, String callType, String searchPattern, long fromIndex,
			long toIndex);
	
	String getLocationIdByName(String cityName);

	
}
