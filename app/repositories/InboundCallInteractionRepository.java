/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import javax.inject.Named;

import models.CallInteraction;
import models.Customer;
import models.Vehicle;

/**
 *
 * @author W-885
 */
@Named
public interface InboundCallInteractionRepository {

    public Customer getCustomerInforById(long customerid);

    public Vehicle getVehicleInformationById(long vehicleid);

	public CallInteraction getLatestInterOfVehicleById(long vehiId);
    
}
