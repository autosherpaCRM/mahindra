package repositories;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.inject.Named;
import models.CallDispositionData;
import models.SMSTemplate;
import models.CallInteraction;
import models.CallSummary;
import models.Customer;
import models.ListingForm;
import models.MessageTemplete;
import models.Phone;
import models.PickupDrop;
import models.SRDisposition;
import models.ServiceBooked;
import models.Workshop;
import models.Complaint;
import models.ComplaintSource;
import models.ComplaintTypes;
import models.UpsellLead;
import models.SpecialOfferMaster;
import models.TaggingUsers;

@Named
public interface CallInfoRepository {

	public void deletCall(Long id, String dealerDB);

	public CallInteraction getCallInteractionbyId(Long id, String dealerDB);

	// public List<CallInteraction> getCallLogsByCREManager(String
	// userName,String dealerDB);

	public List<List> getServiceBookedBetweenTimeCREManager(String creMan,
			String dealerDB);

	public List<String> getCallTypePie(String creMan, String dealerDB);

	public List<String> getCallTypePieForCRE(String cre,
			String userLogindealerCode);

	public List<List> getServiceBookedBetweenTimeForCRE(String cre,
			String userLogindealerCode);

	public List<String> getCallTypeCountsForCRE(String cre,
			String userLogindealerCode, Date currentDate, Date addedDate);

	public List<String> getCallsMadeVsServiceBookedForCRE(String cre,
			String userLogindealerCode, Date currentDate, Date addedDate);

	public MessageTemplete getMessageTempleteOfDealerServiceBooked(
			String string, String dealer);

	public void updateMessageSentStatusServiceBooked(long id, String string);

	public void updateMessageSentStatusServiceBookedOneDayBefore(long id,
			String string);

	public long getCountOfFollowUpsTobeDoneToday(String cre,
			String userLogindealerCode);

	public void updateCallCountOfCallAndFollowUp(long id, String dealercode);

	public List<CallInteraction> getServicBookedCalls(String username,
			String dealercode);

	public List<CallInteraction> getServiceNotRequiredCalls(String username,
			String dealercode);

	public List<CallInteraction> getNonContactsCalls(String username,
			String dealercode);

	public List<CallInteraction> getCallHistoryByPhoneNumber(
			String phonenumber, String cre, String dealercode);

	public List<String> getCREUserNameofCREManager(String userName,
			String dealercode);

	public List<String> getPendingCallByUserListForChart(String userlist,
			String dealercode, Date currentDate, Date addedDate);

	public List<String> getFollowUpCallsCountForReportPage(String userlist,
			String dealercode, Date currentDate, Date addedDate);

	public List<CallSummary> getAllCallCountsOfCREs(List<String> crelist,
			String dealercode);

	public void deletSchCall(Long id, String dealerDB);

	public long getUniqueIdForCallInitaiating();

	public long getUniqueIdForListInitaiating();

	public Customer getCustomerByInteractionId(long id);

	public CallInteraction getInteractionCallByCustomer(long id);

	public void addCallInteraction(CallInteraction call_interaction,
			ListingForm listData, SRDisposition sr_Disposition,
			String dealercode,CallDispositionData dispo_data);

	public void addCallInteractionOfServiceBooked(
			CallInteraction call_interaction, SRDisposition sr_Disposition,
			ListingForm listData, ServiceBooked service_Booked,
			PickupDrop pick_up, String dealercode,CallDispositionData dispo_data);

	public List<CallInteraction> CallHistoryOfCustomer(long vehicle_id,
			String dealercode);

	

	public List<CallInteraction> getCallLogsByManager(String userName,
			String userLogindealerCode);
        public CallDispositionData getCallDispositionVariable(String callDisposition);

    public List<CallInteraction> getListOfFollowUpOfTodayCRE(List<Long> folowupDataId);

    public List<CallInteraction> getListOfFollowUpMissedByAgent(String cre, String dealercode);

    public void addUpsellLead(SRDisposition sr_Disposition, ListingForm listData,long vehicleid);

    public List<String> getCREUserNameofCREManagerByLocation(String userLoginName, String userLogindealerCode, String cityName, long workshopName);
    
   /* public List<ServiceAdvisorInteraction> getSAInteractionList(long vehicle_id,
			String dealercode);*/
    
    public List<Workshop> getWorkshopListById(long workshop_id);
    
    public HashMap<Long,String> getCREUserNameIdofCREManager(String username, String dealercode);
	public List<Complaint> getAllComplaintsOFVehicle(long vehicle_id);
	
	public List<SMSTemplate> getMessageTemplatesForDisplay();

	public List<UpsellLead> getUpselLeadSelectedBySRDispo(long sr_int_id);

	public List<SpecialOfferMaster> getActiveSpecialOffers();

	public Phone getPhoneById(long phoneNumberId);

	public String getLeadNameById(long leadId);

	public void sendComplaintSMS(CallInteraction call_interaction);

	public long getVehicleCountExisting(String vehicleReg);

	public List<TaggingUsers> listAllTaggingUsers();

	public List<ComplaintSource> getComplaintSourceList();
	
	public List<ComplaintTypes> listAllComplaintTaggingUsers();
}
