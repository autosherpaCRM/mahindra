/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.inject.Named;
import javax.inject.Singleton;

import com.google.firebase.database.DataSnapshot;

import models.Address;
import models.AssignedInteraction;
import models.CallInteraction;
import models.Campaign;
import models.Complaint;
import models.ServiceBooked;
import models.ComplaintInteraction;
import models.Customer;
import models.CustomerJson;
import models.SRDisposition;
import models.SavedSearchResult;
import models.PickupDrop;
import models.Role;
import models.UploadFileFields;
import models.Vehicle;
import models.ServiceAdvisor;
import models.Workshop;
import models.Driver;
import models.Email;
import models.JobCardDetailsUpload;
import models.ListingForm;
import models.Location;
import models.NewCustomer;
import models.Phone;
import models.Service;
import models.TaggingUsers;
import models.UnAvailability;
import models.UploadData;
import models.UploadMasterFormat;
import models.WorkshopSummary;
import models.WyzUser;
import models.workshopBill;
import models.ServiceTypes;
import controllers.webmodels.AssignedListForCRE;
import controllers.webmodels.CustomerDataOnTabLoad;
import controllers.webmodels.FollowUpNotificationModel;

/**
 *
 * @author W-885
 */
@Named
public interface CallInteractionsRepository {
	public List<CallInteraction> getFollowUpCallsOfUser(String username, String dealercode);

	public List<CallInteraction> getServicBookedCalls(String username, String dealercode);

	public List<CallInteraction> getNonContactsCalls(String username, String dealercode);

	public List<CallInteraction> getDroppedCalls(String userName, String dealercode);

	public List<CallInteraction> getServiceNotRequiredCalls(String username, String dealercode);

	public void updateTheMediaData(CallInteraction callinfo, String key, CustomerDataOnTabLoad customerinfo,
			DataSnapshot dataSnapshot);

	public void postEditOfCallInteractions(Customer customer, CallInteraction call_interaction,
			ServiceBooked service_Booked, SRDisposition sr_Disposition, PickupDrop pick_up, Vehicle vehicle, Long id);

	public CallInteraction getCallLogsByCRE(Long id, String dealerDB);
	public List<Campaign> getAllCampaignListPSF();

	public void addCustomerAndVehicalInfo(UploadFileFields uplodingData);

	// public List<Vehicle> assignCallsToUser(Date fromDateNew,Date
	// toDateNew,String customerCat,String serviceCat,long campaignTypeCat);

	public List<Vehicle> assignCallsToUser(Date fromDateNew, Date toDateNew, String customerCat, String serviceCat,
			long campaignTypeCat, long campaignNameCat, long campaignNamePSF);

	public List<AssignedListForCRE> getAssignListOfPSFByProcedure(Date fromDateNew, Date toDateNew, String customerCat,
			String serviceCat, long campaignTypeCat, long campaignNameCat, long campaignNamePSF);
	// public void assignCallByManager(String userdata, long scallId, String
	// userLogindealerCode,long campaignCat);

	public void assignCallByManager(String userdata, long scallId, String userLogindealerCode, long campaignCat,
			long campaignNameCat, long campaignNamePSF);

	public void assignCallByManagerForPSF(String userdata, AssignedListForCRE psfAssignId, String userLogindealerCode);

	public List<AssignedInteraction> getAssigendInteractionsOfUser(String selectAgent);

	public AssignedInteraction getAssignedInteractionsbyId(long id, String dealercode);

	public void updateAssignedCallMade(long singleId);

	public List<CallInteraction> getSearchList(String userLoginName, String userLogindealerCode);

	public List<CallInteraction> addCustomer(String userLoginName, String userLogindealerCode);

	public List<AssignedInteraction> getAssigendIntOfManager(String userLoginName);

	public List<Vehicle> getVehicleList(long id);

	public List<ServiceAdvisor> getAdvisorList();

	public List<Driver> getDriverList();

	public Customer getCustomerListById(long id);

	public List<Workshop> getWorkshop();

	public Service getLatestServiceData(long vehicle_id);

	public List<Service> getServiceListByVehicle(List<Vehicle> vehicleList);

	public void updateTheUnavialbilityList(List<WyzUser> datalist, String roleIs);

	// public void resetTheStatus(String roleIs);

	public List<String> getCREofCREManager(String userLoginName, String userLogindealerCode);

	public List<String> getCRENotAvailable(String userLogindealerCode, String userLoginName);

	public List<AssignedInteraction> getAssignedIntListForReAss(List<String> selectedList);

	public List<CallInteraction> getFollowUpCallsOfUsersList(List<String> selectedList, String dealercode);

	public void changeAssignmentCallByManager(String userdata, long scallId, String userLogindealerCode);

	public void changeAssignCallFollowUp(String userdata, long scallId, String userLogindealerCode);

	public void addUpload_Format_data(UploadMasterFormat uploadmasterformat_data);

	public void addNewCustomerData(NewCustomer newCust_modified);

	public void addJobCardData(JobCardDetailsUpload job_card_details);

	public void addBillData(workshopBill serviceData);

	// complaint
	public List<Vehicle> getAllvehList();

	public void addNewComplaint(Complaint complaint, Vehicle vehicle, Phone phone, Email email, Address address,
			String userLoginName);

	public void assignComplaints();

	public List<Complaint> getAllComplaint();

	public void updateComplaintsByCRE(long id, String comments, String selected_value);

	// Customer Update
	public void getcallinfoDetails(long customer_id, String preferred_mobile_no);

	public void addCustomerAdditionalInfo(CustomerJson cust_details);

	public void getPhoneDetails(long customer_id, String phone_number);

	public Service getVehicalByJobCard(String jobCardNumber);

	public void getChassisNo(Long customer_id, String chassisNo);

	public void getEngineNo(Long customer_id, String engineNo);

	public void getVehicalRegNo(Long customer_id, String vehicalRegNo);

	public void updateMessageSentStatus(long id, String yes);

	public List<ServiceBooked> getServiceBookedByUserforThisWorkShop(long workId, long userId);

	public List<ServiceBooked> getServiceBookedBySA(long serviceAdvisorId, long userId);

	public List<PickupDrop> getServiceBookedForDriver(long driverId, long userId);

	public AssignedInteraction updateAssignedAgent(long rowId, long wyzUserId, long post_id);

	public List<Location> getLocationList();

	public List<Workshop> getWorkshopListByLoca(String selectedCity);

	public List<PickupDrop> getPickupDropList();

	public long getworkshopsummaryById(long selectedWorkshop,String schDate);

	public Vehicle getSearchVeh(String veh_number);

	public Complaint getSearchComplaintNum(String complaintNum);

	// public List<Complaint> getAllComplaintsForResolution();

	public void updateComplaintsResolution(String complaintNum, String reasonFor, String complaintStatus,
			String customerStatus, String actionTaken, String resolutionBy);

	// public List<Complaint> getClosedComplaintsForResolution();

	public Vehicle getSearchVehClosed(String veh_numclosed);

	public Complaint getSearchComplaintNumClosed(String complaintNumClosed);

	public void updateComplaintsResolutionClosed(String complaintNumClosed, String reasonForClosed,
			String complaintStatusClosed, String customerStatusClosed, String actionTakenClosed,
			String resolutionByClosed);

	public void getResolutionData(String complaintNum, String city, String workshop, String functions, String ownership,
			String priority, String esclation1, String esclation2);

	public List<Complaint> getComplaintByCustomerId(long id);

	public void addNewPhoneByCustomer(Long customer_id, String phone_number);

	public void updateUnavailabiltyOfUserInRange(List<WyzUser> datalist, List<UnAvailability> unavilabilityList);

	public List<Complaint> getAllComplaintsForManager();

	public List<Complaint> getClosedComplaintsForManager();

	public List<Complaint> getAllComplaintsForResolution(String userLoginName);

	public List<Complaint> getClosedComplaintsForResolution(String userLoginName);

	public String addRangeOfUnaviabilityOfUser(String selectAgent, String fromDate, String toDate);

	public List<UnAvailability> getUnavialabilityListByUser(String selectAgent);

	public List<UnAvailability> getRangeOfUnavailabilty(String selectAgent, String fromDate, String toDate);

	public void deleteUnavialabilty(UnAvailability deleteData);

	public void addUnavialabilty(String selectAgent, String From_Date, String To_Date);

	public List<Service> getAllServiceListOfVehicle(List<Vehicle> vehicle_data);

	public List<Customer> getAllCustomers(String userLogindealerCode);

	public List<Workshop> getWorkshopList();

	public List<ServiceAdvisor> getListOfAdvisorBasedOnCity(long workshopId);

	public List<TaggingUsers> getListTaggingById(long userLocation);

	public List<String> getTaggingPersonByLocationDepartment(long userLocation, long departmentName);

	public List<Address> getAddressesByCustomerId(long customerId);

	//public void addNewCustomerInformation(Customer newCustomer, List<String> phoneList ,Address newCustomerAdress);

	public void addNewVehicleInformation(Vehicle newVehicle, ListingForm customer_Id);

	public void updateFollowUpDoneStatus(long callInteractionId);

	public long getCountOfServiceHistoryOfVehicle(long vehicle_id);

	public List<String> getNewCustomer(Vehicle vehicle, Customer customer, Phone phone, Email email, Address address,
			String userLoginName);

	public List<Campaign> getAllCampaignList();
	
	public List<Campaign>  getCampaignNamesBYType(String typeIs);

	public UnAvailability getUnavialabilityListByID(long id);

	public UploadData setRequiredFieldsForUpload(long userId, String sheetname, String dataTypefile,
			String locationdata, Long workshopId, String finalName, String camFromDate, String camExpDate,
			String finalUploadId);

	public List<Campaign> getCampaignNames();

	public List<Campaign> getCampaignNamesPSF();
	public List<Campaign> getAllCampaignListInsurance();

	public long getCountOfServiceHistory(long vehicle_id);

	public Service getLatestServiceDataFiltering(long vehicle_id);

	public String getLatestServiceBookedType(long cid, long vehicle_id, Service service);

	public long getCustmobNo(Long wyzUser_id, String custMobNo, Long customer_Id);

	public List<String> getCREandServiceAdvOFMan(String username, String userLogindealerCode);

	public void getCustEmail(Long wyzUser_id, String custEmail, Long customer_Id);

	public List<Complaint> getClosedComplaintsAssignedForManager();

	public List<Complaint> getComplaintsFilterData(String filterData);

	public void updateComplaintsResolutionByManager(String complaintNum, String reasonFor, String complaintStatus,
			String customerStatus, String actionTaken, String resolutionBy);

	public List<ComplaintInteraction> getComplaintHistory(String complaintNumber, String vehregnumber);

	public List<String> getTaggingPersonByUpsellId(long userLocation, long upselIDTag);

	public long getVehicleRegNoCount(String vehicleReg);

	public List<Driver> getListOfDriversBasedOnWorkshop(long workshopId);

	public List<String> getExcelColumnsOfSelectedFormate(String upload_format);

	public List<String> getComplaintStatusandCount(long id);

	public List<ServiceTypes> getAllServiceTypeList();
	
	public String getCampaignTypeById(long id);
	
	public void addNewCustomerInformation(long insId, long srId, Customer newCustomer, ListingForm listData,
			Address newCustomerAdress);

	public List<String> getWorkshopListByLocaList(List<String> selectedlocations);

	public List<String> getCRESListByWorkshops(List<String> selectedWorkshops);

	public List<String> getCitiesByState(String selState);

	public List<String> getDistinctFunctionComplaints();

	public List<Complaint> getComplaintsFilterDataByStatus(String filterData, String loc, String func,
			String raisedDate, String endDate);

	public List<String> getComplaintStatusList(String city);

	public List<Workshop> getWorkshopListByLocaId(long selectedCity);

	public Set<String> getUsersByLocandFunc(String city, String func);

	public void changeAssignmentCallBy(long id, long assignId);

	public List<FollowUpNotificationModel> getFollowUpNotification(long id, String dealercode);

	public List<AssignedListForCRE> getAssignListOfCampaignAndSMRByProcedure(Date fromDateNew, Date toDateNew,
			String customerCat, String serviceCat, long campaignTypeCat,String location, long fromIndex, long toIndex);

	public void assignCallByManagerSMRCamp(String userdata, long scallId, String userLogindealerCode);

	public long getAssignListOfCampaignAndSMRByProcedureCount(Date fromDateNew, Date toDateNew, String customerCat,
			String serviceCat, long campaignTypeCat, String cityId);

	public long getAssignListOfPSFByProcedureCount(Date fromDateNew, Date toDateNew, String customerCat,
			String serviceCat, long campaignNamePSF,String modelname,String cityId,String workshopname);

	public List<AssignedListForCRE> getAssignListOfPSFByProcedureServer(Date fromDateNew, Date toDateNew,
			String customerCat, String serviceCat, long campaignTypeCat, long campaignNameCat, long campaignNamePSF,String modelname,
			long fromIndex, long toIndex,String cityId,String workshopname);
	public List<Role> getRoles();

	public CallInteraction getCallInteractionById(long callInteractionId);

	public Customer getCustomerById(long customerId);
	
	public List<CallInteraction> getAllCallInteraction(long startId, long endId);
	
	public void updateMediaByteData(CallInteraction callInt);
	
	public CallInteraction getCallInteractionByBookingId(long bookedId);

	public long checkIfSearchNameExists(String searchnamevalue);

	public CallInteraction getCallinteractionByUniqueId(int uniqueId);

	List<String> getCampaignNamesAll();

}
