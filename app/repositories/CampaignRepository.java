package repositories;

import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;
import models.Campaign;

import models.Customer;
import models.Workshop;

@Named

public interface CampaignRepository {

    //public void addNewCampaign(Campaign Campaign, String userLoginName);

   //public void addNewCampaign(Campaign Campaign, String userLoginName, String locationdata, Workshop uploadData_work);


    //public void addNewCampaign(Campaign Campaign, String userLoginName, String locationdata, Workshop uploadData_work, long workshopData);

    public void addNewCampaign(Campaign Campaign, String userLoginName, String locationdata, Workshop uploadData_work, long workid);

	public List<Campaign> getCampaignListUploadType(String uploadType);

	
}
	