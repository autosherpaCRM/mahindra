package repositories;

import java.util.List;

import javax.inject.Named;

import models.Dealer;
import models.MessageTemplete;
import models.ServiceDoneSchCall;
import models.WyzUser;

@Named
public interface ScheduledCallRepository {

	void deleteAllScheduledCalls(String dealerDB);

	void deleteScheduledCallsbydealerCode(String dealer_Code);

	void assign(WyzUser user, long scallId, String dealerDB);

	/* void assigning(int id,String user_id); */

	void updateSychedFlag(Long id, String fkey, String dealerDB);


	boolean isAssignmentRequired(String dealerDB);

	boolean isAssigned(String dealerDB);

	List<String> getDealerCodes(String dealerDB);

	void changeAssignment(long changeid, String toid, String dealerDB);

	
	// Reports
	long getScheduledCallCountByCREMan(String creMan, String dealerDB);



	void deleteScheduledCallsbyId(long id, String dealerDB);

	String getthefirebasekeyofscheduledcall(long getthekey, String dealerDB);

	long getidofscheduledcall(long getthekey, String dealerDB);

	
	
	void assignbyCREManager(String userdata, long scallId,
			String userLogindealerCode);

	
	void changeAssignForPendingCallsbyCREManager(String creData, long scallId,
			String userLogindealerCode);

	void toTransferTheCompleteHistory(String selectedCRE, String transferCRE,
			String dealercode);

	long getScheduledCallCountByOfCRE(String creUser, String dealerDB);

	long getScheduledCallsPendingCountForCRE(String creUser,
			String userLogindealerCode);

	long getServiceBookedCountForCRE(String creUser, String userLogindealerCode);

	double getConversionPercentageOfCRE(String creUser,
			String userLogindealerCode);

	
	/*
	 * void transferCompleteNoCallHistroyScheduledCalls(List<String>
	 * transferCRE, ArrayList<ScheduledCall> noHistoryList, String
	 * userLogindealerCode);
	 */

	/*
	 * void
	 * transferCompleteCallInfoCallHistoryNotLinkedtoScheduledCalls(List<String>
	 * transferCRE, List<CallInfo> listCallsNotFromScheduledCalls,String
	 * userLogindealerCode);
	 * 
	 * void changeCallInfoHistorybyCREManager(String creData,long scallId,
	 * String userLogindealerCode);
	 * 
	 * void toTransferCallsDoneByServiceRemainderAndCallHistory(List<String>
	 * transferCRE,ArrayList<ScheduledCall> containsHistoryList,String
	 * dealercode);
	 */

	
	void assigningCallsDirectlyFromCSVuploadFile(String userNameCallToAssign,
			long schCallId, String dealercode);

	
	void updateServiceBookedStatus(String fkeyString, String status,
			String dealer);

	
	void addServiceBookedCalls(ServiceDoneSchCall schcall, String dealercode);

	List<ServiceDoneSchCall> getAllServiceBookedCAlls(String dealercode);

	void updateServiceBookedStatus(String custPhone, String vehicalNumber,
			String cre, String dealercode);

	List<MessageTemplete> getAllMessageDataByDealer(String dealercode);

	
	MessageTemplete getMessageTempleteOfDealer(String messagetype, String dealer);

	void updateMessageSentStatus(long id, String string);

	void updateCallCountOfSCh(long id, String dealercode, String lastStatus);

	
	List<Dealer> getListOfDealers();

	

}
