package repositories;

import java.util.List;
import java.util.Map;

import javax.inject.Named;

import models.CallDispositionData;
import models.Campaign;
import models.Dealer;
import models.Location;
import models.Role;
import models.Tenant;
import models.UserSession;
import models.WyzUser;
import models.SMSTemplate;
@Named
public interface WyzUserRepository {

	WyzUser getUserByPhoneNumber(String phoneNumber);

	WyzUser addWyzUser(WyzUser user, String dealerDB,
			String selectedDealerName, String selectedRole,
			String selectedManager);

	WyzUser deletUser(Long id, String dealerDB);

	// WyzUser editUser(String phoneNumber);

	List<WyzUser> getAllUsers(String dealerDB);

	WyzUser editUser(Long id, String dealerDB);

	WyzUser getUserById(Long id, String dealerDB);

	WyzUser editedData(Long id, WyzUser user, String dealerDB);

	WyzUser getUserByDealerId(String dealerId);

	/* Dealer getDealerForUser(Long id); */
	boolean authenticateUser(String userName, String password);

	String getUserRole(Long id, String dealerDB);

	WyzUser getUserbyUserName(String userName);

	/*
	 * Boolean assignDealer(Dealer dealer,String userdealerCode,long
	 * userdealerId);
	 */
	List<String> getAllCreManagers(String dealerDB);

	List<WyzUser> getAllCRE(String dealerDB);

	List<WyzUser> getCREofCREManager(String user, String dealerDB);

	List<WyzUser> getCREManagerbyDealerCode(String dealercode);

	List<WyzUser> getSalesManagerbyDealerCode(String dealercode);

	long getCREid(String userCRE, String dealerDB);

	WyzUser getUserByPhoneNumberandIMEIno(String phoneNumber, String phoneIMEINo);

	Boolean changepasswordFunction(String userLogindealerCode,String userloginname, String confirmPswd,
			String currentPswd);

	List<WyzUser> getSalesExecutiveofSalesManager(String user, String dealercode);

	List<WyzUser> getSalesManagerbyDealerCodeBysuperAdmin();

	List<WyzUser> getAllCreManagersBySuperAdmin();

	WyzUser getDealerNameByManager(String selectedManager);

	Boolean checkTheUserExistingOrNot(String newUserName,
			String dealerCodeOfNewUser, WyzUser list);

	List<WyzUser> getAllUsersForSync();

	List<WyzUser> getAllCREOFDealer(String dealer);

	List<WyzUser> getAllSalesExecutivesForSyncReports(String dealer);

	WyzUser toaddTheRegistrationIdTOUser(String phoneNumber,
			String phoneIMEINo, String registrationIdNo);

	void toaddTheRegistrationIdTOUserForTheParticulardatabase(
			String phoneNumber, String phoneIMEINo, String registrationIdNo,
			String dealercode);

	List<WyzUser> getAllPSFroleCRES(String userloginname,
			String userLogindealerCode);

    public List<WyzUser> getAllUsersAvailableToday(String userLogindealerCode,String user);

    public List<WyzUser> getAllUsersNotAvailableToday(String userLogindealerCode, String userLoginName);
	public List<WyzUser> getUsersofCREManager(String userLoginName, String userLogindealerCode);

	public Map<String ,String>  updateRegistrationIdOfIMEINo(String phoneIMEINo, String registrationId);
	
	public List<WyzUser> getCREUserNameByWorkshopId(long workshopId);

	void addSessionData(UserSession addnewSession);

	void removeUserFromSession(String userLoginName);

	long existingCount(String referereAdress, String useris);
	
    
	public Tenant getUserTenant(String username);
	
	public WyzUser getUser(String username);
	
	public Tenant getUserTenant(Long userId);
	
	public WyzUser getUserDetails(String username, String password);

	String getOEMBYDealerCode(String dealerCode);
	
	public List<Role> getUserRoles(String username);

	List<CallDispositionData> getDispositionList();
	
	List<CallDispositionData> getDispositionListOfNoncontacts();

	List<Campaign> getCampaignList();

	List<WyzUser> getUsersByRole(String roleIs);

	List<Location> getUserLocationById(long userId);

	List<CallDispositionData> getDispositionListOfService();
	List<CallDispositionData> getDispositionListOfInsurance();
	List<CallDispositionData> getDispositionListOfPSF();
	List<WyzUser> getUserNameByWorkshopId(long workshopId);
	public SMSTemplate getSMSbyLocAndSMSType(String locId,String smstypeid);
}
