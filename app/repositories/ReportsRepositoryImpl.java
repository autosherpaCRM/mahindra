package repositories;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import configs.JinqSource;
import controllers.webmodels.ReportSummary;
import models.UploadReportFiles;
import play.Logger.ALogger;

@Repository("reportsRepository")
@Transactional
public class ReportsRepositoryImpl implements ReportsRepository{
	
	ALogger logger = play.Logger.of("application");

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;
	
	
	public String getCurrentDateInStringFormate() {
		// TODO Auto-generated method stub

		String pattern = "yyyy-MM-dd";
		java.util.TimeZone tz = java.util.TimeZone.getTimeZone("IST");
		Calendar c = Calendar.getInstance(tz);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		simpleDateFormat.setTimeZone(tz);
		String currentDateIs = simpleDateFormat.format(c.getTime());
		// logger.info(" currentDateIs :"+currentDateIs);

		return currentDateIs;
	}


	@Override
	public List<ReportSummary> getUploadReportSummaryList(String cremanager, String uploadTypeIs, String fromDate,
			String toDate, long fromIndex, long toIndex) {
		// TODO Auto-generated method stub
		
		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDateNew = new Date();
		Date toDateNew = new Date();
		if(fromDate.equals("") && toDate.equals("")){
			
		}else{
			try {
				fromDateNew = dmyFormat.parse(fromDate);
				
			} catch (Exception e) {
				
				try {
					fromDateNew=dmyFormat.parse(getCurrentDateInStringFormate());
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
			
			try {
				
				toDateNew = dmyFormat.parse(toDate);
			} catch (Exception e) {
				
				
				try {
					toDateNew=dmyFormat.parse(getCurrentDateInStringFormate());
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
		if( uploadTypeIs.equals("")){
			uploadTypeIs="1";
		}
		
		
		javax.persistence.Query query = em.createNativeQuery(
				"CALL uploadreportSummary(:inuploadtype,:instartdate,:enddate,:startlength, :length)", "ReportSummary");
				
				query.setParameter("inuploadtype",uploadTypeIs);
				query.setParameter("instartdate",fromDateNew);
				query.setParameter("enddate",toDateNew);
				query.setParameter("startlength", fromIndex);
				query.setParameter("length", toIndex);

				return query.getResultList();

		
	}

	@Override
	public long getUploadReportSummaryListCount(String userName, String uploadTypeIs, String fromDate, String toDate) {
		// TODO Auto-generated method stub
		
		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDateNew = new Date();
		Date toDateNew = new Date();
		if(fromDate.equals("") && toDate.equals("")){
			
		}else{
			try {
				fromDateNew = dmyFormat.parse(fromDate);
				
			} catch (Exception e) {
				
				try {
					fromDateNew=dmyFormat.parse(getCurrentDateInStringFormate());
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
			
			try {
				
				toDateNew = dmyFormat.parse(toDate);
			} catch (Exception e) {
				
				
				try {
					toDateNew=dmyFormat.parse(getCurrentDateInStringFormate());
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}		if( uploadTypeIs.equals("")){
			uploadTypeIs="1";
		}
		
		//logger.info("uploadTypeIs :"+uploadTypeIs+"fromDate"+fromDateNew+"toDate"+toDateNew);
		
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("uploadreportSummaryCount");

		sQuery.registerStoredProcedureParameter("inuploadtype", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("instartdate", Date.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("enddate", Date.class, ParameterMode.IN);
		
		sQuery.setParameter("inuploadtype",uploadTypeIs);
		sQuery.setParameter("instartdate",fromDateNew);
		sQuery.setParameter("enddate",toDateNew);
		
		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<UploadReportFiles> uploadReportFilesList() {
		// TODO Auto-generated method stub
		return source.uploadReportFiles(em).toList();
	}

}
