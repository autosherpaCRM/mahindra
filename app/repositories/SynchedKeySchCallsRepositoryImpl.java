package repositories;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import models.SynchedKey;
import models.SynchedKeySchCalls;

import org.springframework.beans.factory.annotation.Autowired;

import configs.JinqSource;

public class SynchedKeySchCallsRepositoryImpl implements SynchedKeySchCallsRepository {

	@PersistenceContext
    private EntityManager em;

    @Autowired
    private JinqSource source;
    
   
	
	@Override
	public boolean checkIfSynched(String key,String dealerDB) {
		
		long count = source.sychedKeySchCalls(em).where(k -> k.firebaseKey.equals(key)).count();
		
		if(count == 0) return false;
		return true;
	}

	@Override
	public void addToSynchedKeyList(String key,String dealerDB) {
		
		SynchedKeySchCalls fKey = new SynchedKeySchCalls();
		fKey.setFirebaseKey(key);
		em.persist(key);
	}

}
