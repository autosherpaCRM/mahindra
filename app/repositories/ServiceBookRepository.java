/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.Date;
import java.util.List;

import javax.inject.Named;

import controllers.webmodels.ServiceBookedResultList;
import models.BookingDateTime;
import models.CallDispositionData;
import models.CallInteraction;
import models.ListingForm;
import models.SRDisposition;
import models.ServiceBooked;
import models.WyzUser;

/**
 *
 * @author W-885
 */
@Named
public interface ServiceBookRepository {

	List<WyzUser> getServiceAdvisorsByWorkshop(long workshopid);

	String cancelPickupOfBooking(long servicebookedId);

	String cancelBooking(long servicebookedId);

	ServiceBooked getServiceBookDataByID(long servicebookedId);
	
	List<WyzUser> getUserByWorkshopAndRole(long workshopid,String roles);

	List<ServiceBookedResultList> AllServiceBookedRecords(String userLoginName,String searchBookingStatus, String searchDateFrom,
			String searchDateTo, String searchCRE, String searchDriver , String searchSA, String searchWorkshop, String searchPattern, long fromIndex, long toIndex);


	long getAllServiceBookedDataCount(String userLoginName, String searchBookingStatus, String searchDateFrom,
			String searchDateTo, String searchCRE, String searchDriver, String searchSA, String searchWorkshop, String searchPattern);

	long getSBCount(String userLoginName);

	SRDisposition getSRDispositionByCallId(long call_id);

	ServiceBooked getServiceBookByCallId(long call_id);

	boolean resheduleBooking(CallInteraction call_int, SRDisposition srdispo, ServiceBooked servicebooked, String serviceAdvisorId, String workshopId, String typeOfPickup,
			String driver_id, String startTimeId, String endTimeId, WyzUser userdata);

	void updateOldCallIntToReshedule(long serviceBookedId);

	List<BookingDateTime> getTimeSlot();

	List<List<Date>> getDriverScheduleByWyzUser(long id, String scheduleDate);

	long getServiceAdvisorByuserID(long id);

	long getDriverByUserId(long id);

	void confirmStatusOfBooking(long servicebookedId);

	
	

}
