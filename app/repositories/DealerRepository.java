package repositories;

import models.Dealer;

import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by enda on 31/08/15.
 */
@Named

public interface DealerRepository {
    
    Dealer addDealerData(Dealer adddealer);
    List<Dealer> getAllDealer();
    Dealer getDealerById(Long did);
    Dealer deleteDealer(Long did);
    Dealer editDealer(Long did);
    Dealer posteditedDealerData(Long did,Dealer dealer); 
    String getDealerCodebyAdmin(String dealercode);
    String getOEMbyDealer();
 
}