package repositories;

import java.util.Date;
import java.util.List;

import javax.inject.Named;

import models.ColumnDefinition;
import models.FormData;
import models.Location;
import models.Upload;
import models.UploadType;
import models.Vehicle;
import models.Campaign;

@Named
public interface UploadRepository {

	public List<ColumnDefinition> getColumnDefinitions(String uploadType);
	
	public void addUpload(Upload upload);
	
	public Upload getUpload(long id);
	
	public void deleteUpload(long id);
	
	public UploadType getUploadType(String uploadType);
	
	public List<Upload> getLast3DaysUploads(String userName, Date currentDate, String uploadType);
	
	public List<Upload> getLast3DaysWithIgnore(String userName, Date currentDate,String uploadType, List<Upload> ignoreList);
	
	public UploadType getDefaultUploadType();
	
	public List<UploadType>getUploadTypes();
	
	public void updateProcessingStarted(long id);
	
	public List<Upload> getAllUploads(String uploadType);

	public List<Location> getLocationList();

	public void saveFormDataList(List<FormData> parameterList,Upload upload);

	public List<Upload> getAllUploadsById(long upId);
	public Upload getUploadTypeBasedOnId(long upId);
	public List<Vehicle> getVehicleBasedOnUpId(long upId);
	public FormData getCampaignNameByUploadID(long upId); 
	public Campaign getcampaignIdByFormData(long formvalueid);
	
}
