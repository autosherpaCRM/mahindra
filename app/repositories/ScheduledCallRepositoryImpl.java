package repositories;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import models.CallDispositionData;
import models.CallInteraction;
import models.Dealer;
import models.MessageTemplete;
import models.ServiceDoneSchCall;
import models.WyzUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import play.Logger.ALogger;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import configs.JinqSource;

// import com.firebase.client.Firebase.AuthResultHandler;

@Repository("scheduledCallRepository")
@Transactional
public class ScheduledCallRepositoryImpl implements ScheduledCallRepository {
	ALogger logger = play.Logger.of("application");

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Override
	public List<Dealer> getListOfDealers() {

		List<Dealer> listDealer = source.dealers(em).toList();

		List<Dealer> dealerAndroidAccess = new ArrayList<Dealer>();

		for (Dealer de : listDealer) {

			if (de.isAndroidAuthentication() == true
					|| de.isWebAndAndroidAuthen() == true) {

				logger.info("DealerCode from  with android and mobile access :"
						+ de.getDealerCode());

				dealerAndroidAccess.add(de);
			}

		}

		return dealerAndroidAccess;

	}

	// Deleting the scheduled calls
	@Override
	public void deleteAllScheduledCalls(String dealerDB) {

		em.createNativeQuery("DELETE from ScheduledCall").executeUpdate();

	}

	// Deleting the Scheduled Calls based on dealer code
	@Override
	public void deleteScheduledCallsbydealerCode(String dealer_Code) {

		em.createNativeQuery(
				"DELETE from ScheduledCall  where dealerCode='" + dealer_Code
						+ "'").executeUpdate();

	}

	@Override
	public boolean isAssignmentRequired(String dealerDB) {

		/*
		 * long allCount = source.ScheduledCalls(em).count();
		 * 
		 * if (allCount == 0) { return false; } else {
		 * 
		 * long count = source.ScheduledCalls(em) .where(s -> s.getWyzUser() ==
		 * null).count(); if (count > 0) return true; else return false;
		 * 
		 * }
		 */
		return true;
	}

	@Override
	public boolean isAssigned(String dealerDB) {

		/*
		 * long allCount = source.ScheduledCalls(em).count();
		 * 
		 * if (allCount > 0) { long assignedCount = source.ScheduledCalls(em)
		 * .where(s -> s.getWyzUser() != null).count(); if (assignedCount > 0)
		 * return true; else return false; } else {
		 * 
		 * return false; }
		 */return true;
	}

	@Override
	public List<String> getDealerCodes(String dealerDB) {

		return source.wyzUsers(em).select(u -> u.getDealerCode()).distinct()
				.toList();

	}

	@Override
	public void changeAssignment(long changeid, String suserName,
			String dealerDB) {

		/*
		 * WyzUser userdata = source.wyzUsers(em) .where(u ->
		 * u.getUserName().equals(suserName)).getOnlyValue();
		 * 
		 * long iddata = userdata.getId();
		 * 
		 * AssignedInteraction scall = em .find(AssignedInteraction.class,
		 * changeid);
		 * 
		 * // removing the changed key from firebase if (scall.getFirebaseKey()
		 * != null) {
		 * logger.info("Deleting the scheduled key of particular agent"); String
		 * fireUrl1 = play.Play.application().configuration()
		 * .getString("app.myfirebaseurl");
		 * 
		 * fireUrl1 += scall.getDealerCode(); fireUrl1 += "/Service/"; fireUrl1
		 * += "ScheduledCalls/"; fireUrl1 += scall.getAgentName(); fireUrl1 +=
		 * "/"; fireUrl1 += "CallInfo"; fireUrl1 += "/"; fireUrl1 +=
		 * scall.getFirebaseKey();
		 * logger.info("fire base key to be deleted from forebase : " +
		 * scall.getFirebaseKey());
		 * 
		 * DatabaseReference ref1 = FirebaseDatabase.getInstance()
		 * .getReferenceFromUrl(fireUrl1); logger.info("firebase reference : " +
		 * ref1); ref1.removeValue(); }
		 * 
		 * // adding the change scheduled to the agent
		 * 
		 * WyzUser changeuser = em.find(WyzUser.class, iddata);
		 * scall.setWyzUser(changeuser);
		 * scall.setAgentId(changeuser.getUserName());
		 * scall.setAgentName(changeuser.getUserName());
		 * scall.setSyncedToFireBase("false");
		 * 
		 * logger.info("CRE username id to whome ot is changing : " +
		 * changeuser.getUserName());
		 * 
		 * em.merge(scall); em.flush();
		 * 
		 * logger.info("Adding the scheduled key of particular agent"); String
		 * fireUrl = play.Play.application().configuration()
		 * .getString("app.myfirebaseurl");
		 * 
		 * fireUrl += userdata.getDealerCode(); fireUrl += "/Service/"; fireUrl
		 * += "ScheduledCalls/"; fireUrl += userdata.getUserName(); fireUrl +=
		 * "/"; fireUrl += "CallInfo/";
		 * 
		 * DatabaseReference ref = FirebaseDatabase.getInstance()
		 * .getReferenceFromUrl(fireUrl); //
		 * ref.authWithCustomToken("L6FlQ082WldrWTfRQqKSeKdlh1hUFXmFwf1iOqG1",
		 * // new AuthResultHandler() { // // @Override // public void
		 * onAuthenticationError(FirebaseError err) { //
		 * logger.error("Firebase authentication failed with error:"
		 * +err.getDetails()); // // } // // @Override // public void
		 * onAuthenticated(AuthData data) { //
		 * logger.info("Firebase authentication succeded"); // // } // });
		 * 
		 * DatabaseReference newref = ref.push(); String key = newref.getKey();
		 * logger.info("Changeassighnment key is : " + key);
		 * scall.setFirebaseKey(key); newref.setValue(scall);
		 * logger.info("Changeassighnment key"); // updateSychedFlag(new
		 * Long(userdata.getId()),key);
		 */
	}

	@Override
	public long getScheduledCallCountByCREMan(String creMan, String dealerDB) {

		/*
		 * // tdataSource.setDatabaseName(dealerDB);
		 * 
		 * return source.ScheduledCalls(em) .where(u ->
		 * u.getWyzUser().getCreManager().equals(creMan)) .count();
		 */return 0;
	}

	@Override
	public void assignbyCREManager(String userdata, long scallId,
			String userLogindealerCode) {
		/*
		 * WyzUser user = source.wyzUsers(em) .where(u ->
		 * u.getUserName().equals(userdata)).getOnlyValue();
		 * logger.info("Assign call from CRE the username is : " +
		 * user.getUserName()); AssignedInteraction scall =
		 * em.find(AssignedInteraction.class, scallId); scall.setWyzUser(user);
		 * scall.setAgentId(user.getUserName());
		 * scall.setAgentName(user.getUserName()); em.merge(scall); em.flush();
		 */
	}

	@Override
	public void changeAssignForPendingCallsbyCREManager(String creData,
			long scallId, String userLogindealerCode) {
		/*
		 * WyzUser user = source.wyzUsers(em) .where(u ->
		 * u.getUserName().equals(creData)).getOnlyValue(); logger.info(
		 * "Assign call from changeAssignForPendingCallsbyCREManager() the username is : "
		 * + user.getUserName()); AssignedInteraction scall =
		 * em.find(AssignedInteraction.class, scallId); scall.setWyzUser(user);
		 * scall.setAgentId(user.getUserName());
		 * scall.setAgentName(user.getUserName());
		 * scall.setSyncedToFireBase("false"); em.merge(scall); em.flush();
		 */

	}

	@Override
	public void toTransferTheCompleteHistory(String selectedCRE,
			String transferCRE, String dealercode) {
		String fireUrl1 = play.Play.application().configuration()
				.getString("app.myfirebaseurl");

		fireUrl1 += dealercode;
		fireUrl1 += "/Service/";
		fireUrl1 += "CallDisposition/";
		fireUrl1 += selectedCRE;
		fireUrl1 += "/";
		fireUrl1 += "FollowUpCalls";

		String fireUrl2 = play.Play.application().configuration()
				.getString("app.myfirebaseurl");

		fireUrl2 += dealercode;
		fireUrl2 += "/Service/";
		fireUrl2 += "CallDisposition/";
		fireUrl2 += transferCRE;
		fireUrl2 += "/";
		fireUrl2 += "FollowUpCalls";

		logger.info("old path fire base key to be deleted from firebase by CREMAnager in changing the pending calls : "
				+ fireUrl1);

		DatabaseReference ref1 = FirebaseDatabase.getInstance()
				.getReferenceFromUrl(fireUrl1);
		DatabaseReference data = ref1.child("/FollowUpCalls");
		logger.info("the key of path is : " + ref1.getKey());
		// Firebase.get

		// logger.info("firebase reference : "+ref1);
		// logger.info("get ref : "+ref1.getRef());
		// logger.info("get class : "+ref1.getClass());
		// logger.info("getParent : "+ref1.getParent());
		// logger.info("get the app :"+ref1.getApp());

		// ref1.removeValue();

	}

	@Override
	public long getScheduledCallCountByOfCRE(String creUser, String dealerDB) {
		// TODO Auto-generated method stub
		logger.info("cre user" + creUser);
		return source.assignedInteractions(em)
				.where(u -> u.getWyzUser().getUserName().equals(creUser))
				.count();
	}

	@Override
	public long getScheduledCallsPendingCountForCRE(String creUser,
			String userLogindealerCode) {
		// TODO Auto-generated method stub
		return source.assignedInteractions(em)
				.where(s -> s.getWyzUser().getUserName().equals(creUser))
                                .filter(s -> s.getCallMade() != null)
				.filter(s -> s.getCallMade().equalsIgnoreCase("No")).count();
	}

	@Override
	public long getServiceBookedCountForCRE(String creUser,
			String userLogindealerCode) {
		List<CallInteraction> latest_dispo_Data = getUniqueAndLatestDispositionsOfUser(creUser);
		List<CallInteraction> serviceBookedcalls = new ArrayList<CallInteraction>();
		if (latest_dispo_Data.size() != 0) {

			for (CallInteraction addingData : latest_dispo_Data) {
				if (addingData.getSrdisposition().getCallDispositionData().getDispositionId()==addingData.getSrdisposition().getCallDispositionData().STATUS_BOOK_MY_SERVICE) {

					serviceBookedcalls.add(addingData);

				}

			}
			logger.info("count of service booked :" + serviceBookedcalls.size());
			return serviceBookedcalls.size();
		} else {

			return 0;

		}
		// TODO Auto-generated method stub
		// source.callInfos(em).where(c -> c.getAgentName().equals(creUser))
		// .filter(c -> c.getStatus() != null)
		// .filter(c ->
		// c.getStatus().equalsIgnoreCase(getDispositionValueById("3")))
		// .count();

	}

	@Override
	public double getConversionPercentageOfCRE(String creUser,
			String userLogindealerCode) {
		long bookedCount = getServiceBookedCountForCRE(creUser,
				userLogindealerCode);
		long totalCount = source.assignedInteractions(em)
				.where(u -> u.getWyzUser().getUserName().equals(creUser))
				.count();
		// long bookedCount = source.callInfos(em)
		// .where(c -> c.getAgentName().equals(creUser))
		// .filter(c -> c.getStatus() != null)
		// .filter(c ->
		// c.getStatus().equalsIgnoreCase(getDispositionValueById("3")))
		// .count();

		if (totalCount != 0) {
			return ((double) bookedCount / totalCount) * 100;
		} else {

			return 0;
		}
	}

	@Override
	public void assigningCallsDirectlyFromCSVuploadFile(
			String userNameCallToAssign, long schCallId, String dealercode) {

		/*
		 * WyzUser user = source.wyzUsers(em) .where(u ->
		 * u.getUserName().equals(userNameCallToAssign)) .getOnlyValue();
		 * logger.info("Assign call from CRE the username is : " +
		 * user.getUserName()); AssignedInteraction scall =
		 * em.find(AssignedInteraction.class, schCallId);
		 * scall.setWyzUser(user); scall.setAgentId(user.getUserName());
		 * em.merge(scall); em.flush();
		 */

	}

	private Date currentDate() {

		return new Date(System.currentTimeMillis());
	}

	@Override
	public void addServiceBookedCalls(ServiceDoneSchCall schcall,
			String dealercode) {
		logger.info("Adding service booked");

		schcall.setServiceBookeduplodedCurrentDate(currentDate());
		em.persist(schcall);

	}

	@Override
	public List<ServiceDoneSchCall> getAllServiceBookedCAlls(String dealercode) {
		// TODO Auto-generated method stub
		return source.serviceDoneSchCalls(em).toList();
	}

	@Override
	public List<MessageTemplete> getAllMessageDataByDealer(String dealercode) {
		// TODO Auto-generated method stub
		return source.messageTempletes(em)
				.where(u -> u.getDealerCode().equals(dealercode)).toList();
	}

	@Override
	public MessageTemplete getMessageTempleteOfDealer(String messagetype,
			String dealer) {

		// MessageTemplete messageData=source.messageTempletes(em).where(u ->
		// u.getDealerCode().equals(dealer) &&
		// u.getMessageType().equals(messagetype)).getOnlyValue();

		List<MessageTemplete> message = source.messageTempletes(em)
				.where(u -> u.getDealerCode().equals(dealer)).toList();
		MessageTemplete messageData = new MessageTemplete();
		for (MessageTemplete data : message) {

			if (data.getMessageType().equals(messagetype)) {

				messageData.setId(data.getId());
				messageData.setDealerCode(data.getDealerCode());
				messageData.setMessageBody(data.getMessageBody());
				messageData.setMessageHeader(data.getMessageHeader());
				messageData.setMessageType(data.getMessageType());

			}

		}

		return messageData;
	}

	private List<CallInteraction> getUniqueAndLatestDispositionsOfUser(
			String username) {

		List<String> vehical_numbers = source.vehicle(em)
				.select(u -> u.getVehicleNumber()).distinct().toList();
		logger.info("vehical_numbers :" + vehical_numbers.size());

		List<CallInteraction> finalData = new ArrayList<CallInteraction>();

		for (String vehical_num : vehical_numbers) {

			List<CallInteraction> intData = source
					.callinteractions(em)
					.where(k -> k.getWyzUser().getUserName().equals(username))
					.where(k -> k.getVehicle().getVehicleNumber()
							.equals(vehical_num)).toList();

			java.util.Date tmp = null;
			int i = 0;
			if (intData.size() != 0) {

				for (CallInteraction uniqueNo_list : intData) {

					if (i == 0) {

						tmp = uniqueNo_list.getCallMadeDateAndTime();
						i++;
					} else {

						if (tmp.before(uniqueNo_list.getCallMadeDateAndTime())) {

							tmp = uniqueNo_list.getCallMadeDateAndTime();
							i++;
						}

					}

				}
				logger.info("getting latest disposition");

				finalData.add(intData.get(i - 1));

			}
		}

		return finalData;
	}

	

	@Override
	public void assign(WyzUser user, long scallId, String dealerDB) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateSychedFlag(Long id, String fkey, String dealerDB) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteScheduledCallsbyId(long id, String dealerDB) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getthefirebasekeyofscheduledcall(long getthekey,
			String dealerDB) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getidofscheduledcall(long getthekey, String dealerDB) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateServiceBookedStatus(String fkeyString, String status,
			String dealer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateServiceBookedStatus(String custPhone,
			String vehicalNumber, String cre, String dealercode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateMessageSentStatus(long id, String string) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCallCountOfSCh(long id, String dealercode,
			String lastStatus) {
		// TODO Auto-generated method stub

	}

}
