package repositories;

import javax.inject.Named;
import javax.inject.Singleton;

@Named

public interface SynchedKeyRepository {
	
	public boolean checkIfSynched(String key,String dealerDB);
	
	public void addToSynchedKeyList(String key,String dealerDB);

}
