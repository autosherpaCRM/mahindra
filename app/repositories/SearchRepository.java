package repositories;

import controllers.webmodels.UploadFileHistory;
import controllers.webmodels.CallInteractionHistory;
import controllers.webmodels.CallLogAjaxLoad;
import controllers.webmodels.CallLogDispositionLoad;
import controllers.webmodels.CallLogAjaxLoadMR;
import controllers.webmodels.CallLogDispositionLoadMR;
import controllers.webmodels.DailyAssignmentStatusData;
import controllers.webmodels.DailyAvailabilityData;
import controllers.webmodels.InsuranceHistoryReport;
import controllers.webmodels.CallInteractionHistoryCubeReport;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map;


import javax.inject.Named;
import models.CallInteraction;

import models.CustomerSearchResult;

@Named
public interface SearchRepository {
	public static String CUST_NAME="incustname";
	public static String VEH_REG_NO = "invehregno";
	public static String PHONE_NUM="inphnumber";
	public static String SEARCH_DT_FROM="innxtserstrdate";
	public static String SEARCH_DT_TO="innxtserenddate";
	public static String MODEL="inmodel";
	public static String VARIANT="invariant";
	public static String CUST_CAT= "custcat";
	public static String LAST_DISP = "inlastdispo";
	public static String START_INDX = "start_with";
	public static String SIZE = "length";
	public static String SEARCHPATTERN="searchpattern"; 

	
	public List<CustomerSearchResult> searchCustomers(String pattern,long start, long length, int orderColumnNum, int orderDirection);

	public long getAllCustomerCount();
	
	public long getCustomerCountForPattern(String pattern);

    public long getAllAssignedInteraction(long id);

    public List<CallLogAjaxLoad> assignedListOfUser(String fromDateNew, String toDateNew, String campaignName, String serviceTypeName ,String pattern, long id, long fromIndex, long toIndex);

    public List<CallLogDispositionLoad> getDispositionListOfUser(String fromDateNew, String toDateNew,
			String campaignName, String serviceTypeName, String searchPattern, long id, long typeOfDispo,
			long fromIndex, long toIndex, int orderDirection);

    public long getAllDispositionRequiredData(long id, int typeOfDispo);

    public long getNoncontactsDataCount(long id);

    public List<CallLogDispositionLoad> getnonContactsListOfUser(String fromDateNew, String toDateNew,
    		String campaignName, String lastDispo, String dropperCount, String pattern, long id,
    		long fromIndex, long toIndex);

    public long getDroppedDataCount(long id);

    public List<CallLogDispositionLoad> getDroppedDataList(String pattern,long id, long fromIndex, long toIndex);

	public long getAllVehicleCount();
	
	//Manager server side
	
	public List<CallLogAjaxLoadMR> assignedListOfUserMR(String fromDateNew,String toDateNew,String campaignName,String serviceTypeName,String searchPattern,String managerName,String CREIds, long fromIndex,long toIndex);
    
	public List<CallLogDispositionLoadMR> getDispositionListOfUserMR(String fromDateNew, String toDateNew , String campaignName, String serviceTypeName,String searchPattern, String managerName, String CREIds,
			long typeOfDispo,long fromIndex, long toIndex, int orderDirection);


    public List<CallLogDispositionLoadMR> getnonContactsListOfUserMR(String fromDateNew, String toDateNew, String campaignName ,String lastDispo, String droppedCount,String pattern,String managerName,String CREIds, long fromIndex, long toIndex);
    
    public List<CallLogDispositionLoadMR> getDroppedDataListMR(String pattern,String managerName,String CREIds, long fromIndex, long toIndex);

	
	public long getCountOfUploadFileHistory(String uploadReportId, String typeIds, Date fromDateNew, Date toDateNew, long fromIndex, long toIndex);

	public List<UploadFileHistory> getListOfUploadFileHistory(String uploadReportId,String typeIds, Date fromDateNew, Date toDateNew,
			long fromIndex, long toIndex);

	public double getConversionRate(long id, String string);

	public long getPendingandtotalcount(long id, String string);

	public List<Date> getUploadIdMaxMinDate(String uploadId, String uploadType);

	public double getConversionRateMAN(String userLoginName, String string);

	public long getPendingAndScheduledcountMan(String userLoginName, String string);

	public long methodToFindTotalSizeParam4(String string, long servicebookedcount, String storedProcedureFun, int i,String user);

	public long methodToFindTotalSizeParam3(String cREIds, long totalSize, String storedProcedureFun,String user);

	public long getCustomerCountIndv(String searchCust, String searchVeh, String searchPh, String searchserDateFrom,
			String searchDateTo, String searchModel, String searchVar, String searchCat, String searchDisp,String searchPattern);

	/*public long getCustomerCountIndv(String searchCust, String searchVeh, String searchPh, String searchserDateFrom,
			String searchDateTo, String searchModel, String searchVar, String searchCat, String searchDisp);
*/
	
	public List<CustomerSearchResult> searchCustomersIndv(String searchCust, String searchVeh, String searchPh,
			String searchserDateFrom, String searchDateTo, String searchModel, String searchVar, String searchCat,
			String searchDisp,String searchPattern, long fromIndex, long toIndex);

	/*public List<CustomerSearchResult> searchCustomersIndv(String searchCust, String searchVeh, String searchPh,
			String searchserDateFrom, String searchDateTo, String searchModel, String searchVar, String searchCat,
			String searchDisp, long fromIndex, long toIndex);
	*/
	
	public List<String> getBookedServiceType();

	boolean saveSearchName(String searchName, Map<String, String> parameters);

	public long getCountForMissedIncominOutgoing(String managerName,String callType,String CREIds,String toCallDate,String fromCallDate,String searchPattern);
    
	public List<CallLogDispositionLoadMR> getDataMissedIncominOutgoing(String managerName,String callType,String CREIds,String fromCallDate,String toCallDate,String searchPattern,long fromIndex,long toIndex);

	public long serviceBookDashboardCountManager(String userLoginName);

	public long serviceBookDashboardCount(long id);

	public long getTotalCountOfCustomers();

	public long getAllCallhistoryCount(String userLoginName, String fromDateNew, String toDateNew, String location,
			String workshop,String workshops,String bookingstatus, String disposition, String creidsare, String patternCount);

	public List<CallInteractionHistoryCubeReport> getAllCallHistoryData(String userLoginName, String fromDateNew,
			String toDateNew, String location, String workshop, String workshops,String bookingstatus,String creidsare, String disposition, long fromIndex,
			long toIndex);
	public long getPSFassignedInteractionTableDataMRCount(String userLoginName, String fromDateNew, String toDateNew,
			String typeOfPSF, String searchPattern, String creidsare);

	public List<CallLogAjaxLoadMR> PSFassignedListOfUserMR(String userLoginName, String fromDateNew, String toDateNew,
			String typeOfPSF, String searchPattern, String creidsare, long fromIndex, long toIndex);

	public long getPSFfollowUpCallLogTableDataMRCount(String userLoginName, String fromDateNew, String toDateNew,
			String typeOfPSF, String searchPattern, String creidsare, long typeOfDispo);

	public List<CallLogDispositionLoadMR> getPSFfollowUpCallLogTableDataMR(String userLoginName, String fromDateNew, String toDateNew,
			String typeOfPSF, String searchPattern, String creidsare, long typeOfDispo, long fromIndex, long toIndex);
    
	public long getPSFnonContactsServerDataTableMRCount(String userLoginName, String fromDateNew, String toDateNew,
			String typeOfPSF, String searchPattern, String creidsare, long typeOfDispo);
	
	public List<CallLogDispositionLoadMR> getPSFnonContactsServerDataTableMR(String userLoginName, String fromDateNew, String toDateNew,
	String typeOfPSF, String searchPattern, String creidsare, long typeOfDispo, long fromIndex, long toIndex);
    
	public long getInsuranceassignedInteractionTableDataMRCount(String userLoginName, String creidsare ,String typeOfPSF ,String fromDateNew, String toDateNew,
			String renewalType, String searchPattern);

	public List<CallLogAjaxLoadMR> InsuranceassignedListOfUserMR(String userLoginName,String creidsare,String typeOfPSF, String fromDateNew, String toDateNew,String renewalType,String searchPattern, long fromIndex, long toIndex);

	public long getInsurancefollowUpCallLogTableDataMRCount(String userLoginName, String creidsare, String fromDateNew, String toDateNew,
			String typeOfPSF, String renewalType, String searchPattern, long typeOfDispo);

	public List<CallLogDispositionLoadMR> getInsurancefollowUpCallLogTableDataMR(String userLoginName, String creidsare, String fromDateNew, String toDateNew,
			String typeOfPSF,String renewalType, String searchPattern,long typeOfDispo, long fromIndex, long toIndex);
    
	public long getInsurancenonContactsServerDataTableMRCount(String userLoginName, String creidsare, String fromDateNew, String toDateNew,
			String typeOfPSF,String renewalType, String searchPattern,long typeOfDispo,String droppedCount, String lastDipo);
	
	public List<CallLogDispositionLoadMR> getInsurancenonContactsServerDataTableMR(String userLoginName,String creidsare, String fromDateNew, String toDateNew,
	String typeOfPSF, String renewalType,String searchPattern, long typeOfDispo,String dropppedCount,String lastDipo, long fromIndex, long toIndex);
    
	public long getAllSMShistoryCount(String convertedFromDate, String convertedToDate,String creidsare,String sentStatus);
	
	public List<CallInteractionHistory> getAllSMSHistoryData(String convertedFromDate, String convertedToDate,String creidsare,String sentStatus,long fromIndex, long toIndex);
    
	 public long getAllInsurancehistoryCount(String convertedFromDate, String convertedToDate,String creidsare,String campaignName);
	    
		public List<InsuranceHistoryReport> getAllInsuranceHistoryData(String convertedFromDate, String convertedToDate,String creidsare,String campaignName,long fromIndex, long toIndex);
		
		public List<Long> getInteractionID(long campainIdIS, long serviceId);
		List<DailyAvailabilityData> getdailyAvailabilityStatusreport(String userLoginName, String fromDateNew, String toDateNew, String creidsares, String workshopsBooked, long fromIndex, long toIndex);
		

		public long getdailyAvailabilityStatusreportCount(String userLoginName, String fromDateNew, String toDateNew,
				String creidsares, String workshopsBooked, String searchPattern);

		public long getdailyAssignmentStatusreportCount(String userLoginName, String fromDateNew, String toDateNew,
				String creidsares, String workshopsBooked, String patternCount);

		public List<DailyAssignmentStatusData> getdailyAssignmentStatusreport(String userLoginName, String fromDateNew,
				String toDateNew, String creidsares, String workshopsBooked, long fromIndex, long toIndex);
}
