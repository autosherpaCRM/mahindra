/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Level;
//import models.GrinModified;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.springframework.beans.factory.annotation.Autowired;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import models.UploadData;
import utils.InsuranceExcel;
import play.Logger;
import repositories.ExcelUploadRepo;

/**
 *
 * @author W-117
 */
public class SheetHandler extends DefaultHandler {

	Logger.ALogger logger = play.Logger.of("application");
	private SharedStringsTable sst;
	private String lastContents;
	private boolean nextIsString;
	private int row_num;
	private String col_name;
	private HashMap<Integer, String> hmModelColumn; // Model column of any Model
	private HashMap<Integer, String> hmExcelColumn; // Excel Column matching
													// Model column
	private HashMap<String, String> hmMappedModelColumn; // Matching Index for
															// the Excel sheet
															// column with the
															// hmExcelColumn
	private HashMap<String, String> hmMappedDataType; // Matching Index for the
														// Excel sheet column
														// with the
														// hmExcelColumn
	private HashMap<String, String> formData;
	private HashMap<String, String> errorInfoData = new HashMap<String, String>();
	private boolean SUBMITTED;
	private Object uploadModel;
	private HashMap<Date, Integer> uploadSuccessRecord;
	private HashMap<Date, Integer> uploadRejectedRecord;

	private ExcelUploadRepo excelRepo;

	@SuppressWarnings("unchecked")
	public SheetHandler(SharedStringsTable sst, List<HashMap> mappingFields, HashMap formData, Object uploadObject,
			ExcelUploadRepo xlRepo, HashMap uploadSuccessRecord, HashMap uploadRejectedRecord) {
		this.sst = sst;
		this.hmModelColumn = mappingFields.get(0);
		this.hmExcelColumn = mappingFields.get(1);
		this.formData = formData;
		this.uploadModel = uploadObject;
		hmMappedModelColumn = new HashMap<String, String>();
		hmMappedDataType = new HashMap<String, String>();
		this.errorInfoData = errorInfoData;
		SUBMITTED = false;
		excelRepo = xlRepo;
		this.uploadSuccessRecord = uploadSuccessRecord;
		this.uploadRejectedRecord = uploadRejectedRecord;
	}

	public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
		// c => cell
		if (name.equals("row")) {
			SUBMITTED = false;
			row_num = Integer.parseInt(attributes.getValue("r"));
		}
		if (name.equals("c")) {
			String[] arrColName = attributes.getValue("r").toString().split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
			col_name = arrColName[0];
			// Figure out if the value is an index in the SST
			String cellType = attributes.getValue("t");
			if (cellType != null && cellType.equals("s")) {
				nextIsString = true;
			} else {
				nextIsString = false;
			}
		}
		// Clear contents cache
		lastContents = "";
	}

	public void endElement(String uri, String localName, String name) throws SAXException {
		if (nextIsString) {
			int idx = Integer.parseInt(lastContents);
			lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
			if (row_num == 1) {
				if (hmExcelColumn != null) {
					for (int j = 0; j < hmExcelColumn.size(); j++) {
						if (hmExcelColumn.get(j).equalsIgnoreCase(lastContents)) {
							hmMappedModelColumn.put(col_name, hmModelColumn.get(j));
							Field object_filed;
							try {
								object_filed = uploadModel.getClass().getDeclaredField(hmModelColumn.get(j));
								String data_type = object_filed.getGenericType().getTypeName();
								hmMappedDataType.put(col_name,
										data_type.substring(data_type.lastIndexOf('.') + 1).trim());
							} catch (NoSuchFieldException ex) {
								java.util.logging.Logger.getLogger(SheetHandler.class.getName()).log(Level.SEVERE, null,
										ex);
							} catch (SecurityException ex) {
								java.util.logging.Logger.getLogger(SheetHandler.class.getName()).log(Level.SEVERE, null,
										ex);
							}
							break;
						}
					}
				}
			}
			nextIsString = false;
		}

		// v => contents of a cell
		// Output after we've seen the string contents
		if (name.equals("v")) {
			if (row_num != 1) {
				try {
					DynamicDataUpload();
				} catch (IllegalArgumentException ex) {
					java.util.logging.Logger.getLogger(SheetHandler.class.getName()).log(Level.SEVERE, null, ex);
				} catch (SecurityException ex) {
					java.util.logging.Logger.getLogger(SheetHandler.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		if (name.equals("row") && row_num != 1 && SUBMITTED == false) {
			try {

				excelRepo.insertExcelDataToDB(uploadModel, formData, uploadSuccessRecord, uploadRejectedRecord);
				
				SUBMITTED = true;
				
				String modelType = formData.get("modelType");
				
				try {
					Class<?> clazz = Class.forName(modelType);
					uploadModel =clazz.newInstance();
					
				} catch (ClassNotFoundException | InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				
				
				Field fieldData1 = uploadModel.getClass().getField("isError");
				Field fieldData2 = uploadModel.getClass().getField("rowNumber");
				
				fieldData1.set(uploadModel, false);
				fieldData2.set(uploadModel, row_num);				
				
			} catch (IllegalArgumentException ex) {
				java.util.logging.Logger.getLogger(SheetHandler.class.getName()).log(Level.SEVERE, null, ex);
			} catch (SecurityException ex) {
				java.util.logging.Logger.getLogger(SheetHandler.class.getName()).log(Level.SEVERE, null, ex);
			} catch (IllegalAccessException ex) {
				java.util.logging.Logger.getLogger(SheetHandler.class.getName()).log(Level.SEVERE, null, ex);
			} catch (NoSuchFieldException ex) {
				java.util.logging.Logger.getLogger(SheetHandler.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

	}

	public void characters(char[] ch, int start, int length) throws SAXException {
		lastContents += new String(ch, start, length);
	}

	public void DynamicDataUpload() {
		// logger.info("hmMappedModelColumn.get(col_name) :" );
		// hmMappedModelColumn.get(col_name));
		// The below line is used To get the object field by using name
		if (hmMappedModelColumn.get(col_name) != null) {
			try {
				Field object_filed = uploadModel.getClass().getDeclaredField(hmMappedModelColumn.get(col_name));

				if (object_filed != null && hmMappedDataType.get(col_name) != null) {

					try {
						switch (hmMappedDataType.get(col_name)) {
						case "String":
							object_filed.set(uploadModel, lastContents);
							break;
						case "Date":
							addErrorData(lastContents, col_name);
							Date formated_date = DateUtil.getJavaDate(Double.parseDouble(lastContents));
							java.sql.Date sqlDate = new java.sql.Date(formated_date.getTime());
							object_filed.set(uploadModel, sqlDate);

							break;
						case "Integer":
							addErrorData(lastContents, col_name);
							object_filed.set(uploadModel, Integer.parseInt(lastContents));
							break;
						case "Double":
							addErrorData(lastContents, col_name);
							object_filed.set(uploadModel, Double.parseDouble(lastContents));
							break;
						case "double":
							addErrorData(lastContents, col_name);
							object_filed.set(uploadModel, Double.parseDouble(lastContents));
							break;
						case "Boolean":
							addErrorData(lastContents, col_name);
							object_filed.set(uploadModel, Boolean.getBoolean(lastContents));
							break;
						case "Long":
							addErrorData(lastContents, col_name);
							object_filed.set(uploadModel, Long.parseLong(lastContents));
							break;
						case "long":
							addErrorData(lastContents, col_name);
							object_filed.set(uploadModel, Long.parseLong(lastContents));
							break;
						case "float":
							addErrorData(lastContents, col_name);
							object_filed.set(uploadModel, Float.parseFloat(lastContents));
							break;
						}

					} catch (IllegalArgumentException | IllegalAccessException ex) {
						// TODO Auto-generated catch block

						Field fieldData = uploadModel.getClass().getField("rowNumber");
						Field fieldData1 = uploadModel.getClass().getField("isError");
						Field fieldData2 = uploadModel.getClass().getField("errorInfo");

						try {

							errorInfoData.put(hmMappedModelColumn.get(col_name),
									ex.getClass().getName().replace("java.lang.", "") + " |Value:" + lastContents);

							fieldData.set(uploadModel, row_num);
							fieldData1.set(uploadModel, true);
							fieldData2.set(uploadModel, errorInfoData);

						} catch (IllegalArgumentException | IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						

					}

				}
			} catch (SecurityException | NoSuchFieldException e) {
				// TODO Auto-generated catch block
				logger.info("Exception Exception " + e);
			}

		}
	}

	private void addErrorData(String lastContents2, String col_name2) {
		
		logger.info(" addErrorData : ");

		try {
			Field fieldData = uploadModel.getClass().getField(hmMappedModelColumn.get(col_name) + "Str");
			try {
				fieldData.set(uploadModel, lastContents2);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				logger.info("" + e);
			}
		} catch (SecurityException | NoSuchFieldException e) {
			// TODO Auto-generated catch block
			logger.info("add last content : " + lastContents2+"col_name : "+col_name);

		}

	}
}
