package utils;

public class WorkBillRegister extends CommanUploadModel {

	public String billNo;
	public String mcpNo;
	public java.sql.Date billDateold;
	public String jobCardNo;
	public java.sql.Date beforematching;
	public java.sql.Date billDate;
	public String servicetype;
	public String regno;
	public String custId;
	public String partyName;
	public Double partBasic;
	public Double partDisc;
	public Double partCharges;
	public Double labourBasic;
	public Double labourDisc;
	public Double labourCharge;
	public Double roundOffAmt;
	public Double billAmt;
	public String location;
	public Double year;
	public Double month;
	public String slNo;
	
	public String billDateoldStr;
	public String beforematchingStr;
	public String billDateStr;
	public String partBasicStr;
	public String partDiscStr;
	public String partChargesStr;
	public String labourBasicStr;
	public String labourDiscStr;
	public String labourChargeStr;
	public String roundOffAmtStr;
	public String billAmtStr;
	public String yearStr;
	public String monthStr;

	public String getSlNo() {
		return slNo;
	}

	public void setSlNo(String slNo) {
		this.slNo = slNo;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getMcpNo() {
		return mcpNo;
	}

	public void setMcpNo(String mcpNo) {
		this.mcpNo = mcpNo;
	}

	public java.sql.Date getBillDateold() {
		return billDateold;
	}

	public void setBillDateold(java.sql.Date billDateold) {
		this.billDateold = billDateold;
	}

	public String getJobCardNo() {
		return jobCardNo;
	}

	public void setJobCardNo(String jobCardNo) {
		this.jobCardNo = jobCardNo;
	}

	public java.sql.Date getBeforematching() {
		return beforematching;
	}

	public void setBeforematching(java.sql.Date beforematching) {
		this.beforematching = beforematching;
	}

	public java.sql.Date getBillDate() {
		return billDate;
	}

	public void setBillDate(java.sql.Date billDate) {
		this.billDate = billDate;
	}

	public String getServicetype() {
		return servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}

	public String getRegno() {
		return regno;
	}

	public void setRegno(String regno) {
		this.regno = regno;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public Double getPartBasic() {
		return partBasic;
	}

	public void setPartBasic(Double partBasic) {
		this.partBasic = partBasic;
	}

	public Double getPartDisc() {
		return partDisc;
	}

	public void setPartDisc(Double partDisc) {
		this.partDisc = partDisc;
	}

	public Double getPartCharges() {
		return partCharges;
	}

	public void setPartCharges(Double partCharges) {
		this.partCharges = partCharges;
	}

	public Double getLabourBasic() {
		return labourBasic;
	}

	public void setLabourBasic(Double labourBasic) {
		this.labourBasic = labourBasic;
	}

	public Double getLabourDisc() {
		return labourDisc;
	}

	public void setLabourDisc(Double labourDisc) {
		this.labourDisc = labourDisc;
	}

	public Double getLabourCharge() {
		return labourCharge;
	}

	public void setLabourCharge(Double labourCharge) {
		this.labourCharge = labourCharge;
	}

	public Double getRoundOffAmt() {
		return roundOffAmt;
	}

	public void setRoundOffAmt(Double roundOffAmt) {
		this.roundOffAmt = roundOffAmt;
	}

	public Double getBillAmt() {
		return billAmt;
	}

	public void setBillAmt(Double billAmt) {
		this.billAmt = billAmt;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Double getYear() {
		return year;
	}

	public void setYear(Double year) {
		this.year = year;
	}

	public Double getMonth() {
		return month;
	}

	public void setMonth(Double month) {
		this.month = month;
	}

	public String getBillDateoldStr() {
		return billDateoldStr;
	}

	public void setBillDateoldStr(String billDateoldStr) {
		this.billDateoldStr = billDateoldStr;
	}

	public String getBeforematchingStr() {
		return beforematchingStr;
	}

	public void setBeforematchingStr(String beforematchingStr) {
		this.beforematchingStr = beforematchingStr;
	}

	public String getBillDateStr() {
		return billDateStr;
	}

	public void setBillDateStr(String billDateStr) {
		this.billDateStr = billDateStr;
	}

	public String getPartBasicStr() {
		return partBasicStr;
	}

	public void setPartBasicStr(String partBasicStr) {
		this.partBasicStr = partBasicStr;
	}

	public String getPartDiscStr() {
		return partDiscStr;
	}

	public void setPartDiscStr(String partDiscStr) {
		this.partDiscStr = partDiscStr;
	}

	public String getPartChargesStr() {
		return partChargesStr;
	}

	public void setPartChargesStr(String partChargesStr) {
		this.partChargesStr = partChargesStr;
	}

	public String getLabourBasicStr() {
		return labourBasicStr;
	}

	public void setLabourBasicStr(String labourBasicStr) {
		this.labourBasicStr = labourBasicStr;
	}

	public String getLabourDiscStr() {
		return labourDiscStr;
	}

	public void setLabourDiscStr(String labourDiscStr) {
		this.labourDiscStr = labourDiscStr;
	}

	public String getLabourChargeStr() {
		return labourChargeStr;
	}

	public void setLabourChargeStr(String labourChargeStr) {
		this.labourChargeStr = labourChargeStr;
	}

	public String getRoundOffAmtStr() {
		return roundOffAmtStr;
	}

	public void setRoundOffAmtStr(String roundOffAmtStr) {
		this.roundOffAmtStr = roundOffAmtStr;
	}

	public String getBillAmtStr() {
		return billAmtStr;
	}

	public void setBillAmtStr(String billAmtStr) {
		this.billAmtStr = billAmtStr;
	}

	public String getYearStr() {
		return yearStr;
	}

	public void setYearStr(String yearStr) {
		this.yearStr = yearStr;
	}

	public String getMonthStr() {
		return monthStr;
	}

	public void setMonthStr(String monthStr) {
		this.monthStr = monthStr;
	}
	
	

}
