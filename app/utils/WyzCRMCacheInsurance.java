package utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import controllers.webmodels.InsuranceAgentIdName;

import play.Logger.ALogger;
import play.cache.Cache;
import repositories.AutoSelInsuranceAgentRepository;


@Component
public class WyzCRMCacheInsurance {
	
	ALogger logger = play.Logger.of("application");
	public static final String SEPARATOR="$";
	
	@Autowired(required=true)
	private AutoSelInsuranceAgentRepository saRepo;
	
	public void setupInsurnaceAgentQueue(Date date, List<InsuranceAgentIdName> saData,String dealercodeIs) {
		// TODO Auto-generated method stub
		
		logger.info("Inside Wyzchzche setupinsuranceAgentQueue");
		PriorityBlockingQueue<InsuranceAgentIdName> insuranceAgentQueue = new PriorityBlockingQueue<InsuranceAgentIdName>(saData);
		String key = getCacheKey(date,dealercodeIs);
		Cache.set(key, insuranceAgentQueue);
		
	}



	public List<InsuranceAgentIdName> getInsuranceAgentRecomendation(Date date,String dealercodeIs) throws Exception {
		
		logger.info("Inside Wyzchzche getInsuranceAgentRecomendation");
		
		String key = getCacheKey(date,dealercodeIs);
		Object cacheObject =  Cache.get(key);
		try {
			if(cacheObject ==  null) throw new Exception("Data");
		} catch (Exception e) {
			Date newDate = new Date();
			
			Calendar c = Calendar.getInstance();
			c.setTime(new Date()); // Now use today date.
			c.add(Calendar.DATE, 30); // Adding 5 days
			newDate = c.getTime();
			
			if((newDate.compareTo(date)) < 0 ){ //newDate < date, returns less than 0  // date is greater than 30 days from current Date.
				logger.info("Inside Wyzchzche > 30");
				List<InsuranceAgentIdName> serviceAdvisors = saRepo.getInsurAgentDetails(date);
				setupInsurnaceAgentQueue(date,serviceAdvisors,dealercodeIs);
			}
			else{
				logger.info("Inside Wyzchzche < 30 and initial setup");
				saRepo.getAllInsurAgentDetails(dealercodeIs);
			}
			cacheObject = Cache.get(key);
			if(cacheObject ==  null) throw new Exception("Details are not availabe for that Date");
		}
		PriorityBlockingQueue <InsuranceAgentIdName> serviceAdvisorQueue = (PriorityBlockingQueue<InsuranceAgentIdName>)cacheObject;
		List<InsuranceAgentIdName> saList = new ArrayList<InsuranceAgentIdName>(serviceAdvisorQueue);
		InsuranceAgentIdName sa = serviceAdvisorQueue.take();
		sa.setPriority(sa.getPriority()+1);
		logger.info("Logger SA:" + sa.getAdvisorName());
		serviceAdvisorQueue.put(sa);
		return saList;
	}


	public void getInsurAgentList(Date date, InsuranceAgentIdName saDetails,String dealerId) throws Exception {
		logger.info("Inside Wyzchzche getServiceAdvisorList");
		logger.info("Inside Wyzchzche getServiceAdvisorList preSA :" + saDetails.getAdvisorName());
		String key = getCacheKey(date,dealerId);
		Object cacheObject = Cache.get(key);
		if(cacheObject ==  null) throw new Exception("Service Advisor Cache is not setup");
		PriorityBlockingQueue <InsuranceAgentIdName> serviceAdvisorQueue = (PriorityBlockingQueue<InsuranceAgentIdName>)cacheObject;
		serviceAdvisorQueue.remove(saDetails);
		saDetails.setPriority(saDetails.getPriority()-1);
		serviceAdvisorQueue.put(saDetails);
		
	}



	public void updateInsurAgentChange(Date date, InsuranceAgentIdName preSelectedSaDetails,InsuranceAgentIdName newSelectedSaDetails,String dealerId) throws Exception {
		logger.info("Inside Wyzchzche updateServiceAdvisorChange");
		String key = getCacheKey(date,dealerId);
		Object cacheObject = Cache.get(key);
		if(cacheObject ==  null) throw new Exception("Service Advisor Cache is not setup");
		PriorityBlockingQueue <InsuranceAgentIdName> serviceAdvisorQueue = (PriorityBlockingQueue<InsuranceAgentIdName>)cacheObject;
		if(preSelectedSaDetails != null)
		{
			serviceAdvisorQueue.remove(preSelectedSaDetails);
			preSelectedSaDetails.setPriority(preSelectedSaDetails.getPriority()-1);
			serviceAdvisorQueue.put(preSelectedSaDetails);
		}
		if(newSelectedSaDetails != null)
		{
			serviceAdvisorQueue.remove(newSelectedSaDetails);
			preSelectedSaDetails.setPriority(newSelectedSaDetails.getPriority()+1);
			serviceAdvisorQueue.put(newSelectedSaDetails);
		}
		
	}



	

	private String getCacheKey(Date date,String dealercodeIs) {
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = formatter.format(date);
		String key = dateStr+SEPARATOR+dealercodeIs;;
		return key;
	}

}
