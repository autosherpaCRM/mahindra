package wyzcrmTestCode;

import java.beans.PropertyVetoException;
import java.util.HashMap;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import configs.JinqSource;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "repositories","utils" })
public class TestDataConfig {

	@Bean
	public EntityManagerFactory entityManagerFactory() throws PropertyVetoException {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabase(Database.MYSQL);
		vendorAdapter.setShowSql(false);
		vendorAdapter.setGenerateDdl(true);
		LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactory.setPackagesToScan("models");
		entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
		entityManagerFactory.setDataSource(dataSource());
		HashMap<String, Object> propertiesMap = new HashMap<String, Object>();
		propertiesMap.put("hibernate.hbm2ddl.auto", "update");
		propertiesMap.put("hibernate.id.new_generator_mappings", "true");
		propertiesMap.put("hibernate.format_sql", "true");

		entityManagerFactory.setJpaPropertyMap(propertiesMap);
		entityManagerFactory.afterPropertiesSet();
		return entityManagerFactory.getObject();
	}

	@Bean
	public PlatformTransactionManager transactionManager() throws PropertyVetoException {
		return new JpaTransactionManager(entityManagerFactory());
	}
	

	@Bean
	public DataSource dataSource() throws PropertyVetoException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		Config configuration = ConfigFactory.load();
		dataSource.setDriverClass(configuration.getString("db.spring.driver"));
		dataSource.setJdbcUrl(configuration.getString("db.spring.url"));
		dataSource.setUser(configuration.getString("db.spring.user"));
		dataSource.setPassword(configuration.getString("db.spring.password"));

		// the settings below are optional -- c3p0 can work with defaults
		dataSource.setMinPoolSize(3);
		dataSource.setAcquireIncrement(2);
		dataSource.setMaxPoolSize(5);

		// The DataSource cpds is now a fully configured and usable pooled
		// DataSource

		return dataSource;
	}

	@Bean
	public JinqSource jinqSource() {
		return new JinqSource();
	}

}
