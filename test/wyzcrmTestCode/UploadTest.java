package wyzcrmTestCode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import configs.JinqSource;
import models.ColumnDefinition;
import models.Role;
import models.Tenant;
import models.Upload;
import models.UploadType;
import models.WyzUser;
import repositories.UploadRepository;
import repositories.WyzUserRepository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class UploadTest extends AbstractIntegrationTest {
	
	public static String USER_NAME = "MyAppTestUser1";
	public static String PASSWORD = "MyAppPass";
	public static String USER_PHONE ="99999999991";
	public static String TENANT_CODE = "TEST2";
	public static String USER_ROLE = "GENERICUSER";
	
	
	public static String UPLOAD_TYPE ="TESTTYPE";

	
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	private List<Object> tobeDeleted;

	@Autowired
	private WyzUserRepository userRepository;
	
	@Autowired
	private UploadRepository uploadRepository;

	@Before
	public void setupData() {
		Tenant tenant = new Tenant();
		tenant.setTenantCode(TENANT_CODE);
		tenant.setTenantPhone("12344");
		tenant.setTenentAddress("test address");
		em.persist(tenant);

		em.flush();
		Long tenantid = tenant.getId();

		tobeDeleted = new ArrayList();
		tobeDeleted.add(tenant);

		Role role = new Role();
		role.setRole(USER_ROLE);

		em.persist(role);
		em.flush();
		Long roleid = role.getId();
		tobeDeleted.add(role);

		WyzUser user = new WyzUser();
		user.setFirstName("TestF");
		user.setLastName("TestL");
		user.setUserName(USER_NAME);
		user.setPassword(PASSWORD);
		user.setTenant(tenant);
		List roleList = new ArrayList();
		roleList.add(role);
		user.setRoles(roleList);
		em.persist(user);
		em.flush();
		Long userid = user.getId();

		
		tobeDeleted.add(user);

		
		ColumnDefinition def1 = new ColumnDefinition();
		def1.setColumnCategory(ColumnDefinition.DATA_CAT_Categorical);
		def1.setColumnDataType(ColumnDefinition.DATA_TYPE_INTEGER);
		def1.setDestinationColumnName("slno");
		def1.setFactorable(false);
		def1.setMeasure(false);
		def1.setMetaColumnName("slno");
		def1.setSourceColumnName("S No");
		
		em.persist(def1);
		em.flush();
		
		UploadType type = new UploadType();
		type.setUploadTypeName(UPLOAD_TYPE);
		ArrayList<ColumnDefinition> list = new ArrayList<ColumnDefinition>();
		list.add(def1);
		type.setDefinitions(list);
		
		em.persist(type);
		em.flush();
		
		
		tobeDeleted.add(type);
	
	} 
	
	@Test
	public void testUploadType(){
		
		List<ColumnDefinition> defs = uploadRepository.getColumnDefinitions(UPLOAD_TYPE);
		assertTrue(defs.size() >0);
	}
	
	@Test
	public void testAdddUpload(){
		Upload upload = new Upload();
		upload.setProcessed(false);
		WyzUser user = userRepository.getUser(USER_NAME);
		assertNotNull(user);
		upload.setUploadedBy(user);
		upload.setUploadedDateTime(new GregorianCalendar().getTime());
		upload.setUploadPath("test");
		uploadRepository.addUpload(upload);
		long id = upload.getId();
		assertNotNull(id);
		assertTrue(id > 0);
		
		em.remove(upload);
		em.flush();
	}
	
	public void testDeleteUpload(){
		
		Upload upload = new Upload();
		upload.setProcessed(false);
		WyzUser user = userRepository.getUser(USER_NAME);
		assertNotNull(user);
		upload.setUploadedBy(user);
		upload.setUploadedDateTime(new GregorianCalendar().getTime());
		upload.setUploadPath("test");
		uploadRepository.addUpload(upload);
		long id = upload.getId();
		assertNotNull(id);
		assertTrue(id > 0);
		
		Upload uploadAdd = uploadRepository.getUpload(id);
		
		assertNotNull(uploadAdd);
		
		uploadRepository.deleteUpload(id);
		
		Upload uploadDel = em.find(Upload.class, id);
		
		assertTrue(uploadDel == null);
		
		
	}
	
	public void testUploadStateChange(){
		Upload upload = new Upload();
		
		WyzUser user = userRepository.getUser(USER_NAME);
		assertNotNull(user);
		upload.setUploadedBy(user);
		upload.setUploadedDateTime(new GregorianCalendar().getTime());
		upload.setUploadPath("test");
		upload.setProcessingStarted(false);
		uploadRepository.addUpload(upload);
		long id = upload.getId();
		assertNotNull(id);
		assertTrue(id > 0);
		
		uploadRepository.updateProcessingStarted(id);
		
		Upload newUpload = uploadRepository.getUpload(id);
		assertTrue(newUpload.isProcessingStarted());
	}
	
	@Test
	public void testLast3DaysUploadQuery(){
		GregorianCalendar cal4 = new GregorianCalendar();
/*		Upload upload1 = new Upload();
		upload1.setNumberDiscarded(0);
		upload1.setProcessed(false);
		WyzUser user = userRepository.getUser(USER_NAME);
		assertNotNull(user);
		upload1.setUploadedBy(user);
		UploadType uptye = uploadRepository.getUploadType(UPLOAD_TYPE);
		upload1.setUploadType(uptye);
		
		GregorianCalendar cal = new GregorianCalendar();
		cal.add(Calendar.DATE, -3);
		upload1.setUploadedDateTime(cal.getTime());
		

		Upload upload2 = new Upload();
		upload2.setNumberDiscarded(0);
		upload2.setProcessed(false);
		upload2.setUploadedBy(user);
		upload2.setUploadType(uptye);
		
		GregorianCalendar cal2 = new GregorianCalendar();
		cal2.add(Calendar.DATE, -2);
		upload2.setUploadedDateTime(cal2.getTime());

		Upload upload3 = new Upload();
		upload3.setNumberDiscarded(0);
		upload3.setProcessed(false);
		upload3.setUploadedBy(user);
		upload3.setUploadType(uptye);
		
		GregorianCalendar cal3 = new GregorianCalendar();
		cal3.add(Calendar.DATE, -1);
		upload3.setUploadedDateTime(cal3.getTime());

		Upload upload4 = new Upload();
		upload4.setNumberDiscarded(0);
		upload4.setProcessed(false);
		upload4.setUploadedBy(user);
		upload4.setUploadType(uptye);
		

		upload4.setUploadedDateTime(cal4.getTime());
		
		Upload upload5 = new Upload();
		upload5.setNumberDiscarded(0);
		upload5.setProcessed(false);
		upload5.setUploadedBy(user);
		upload5.setUploadType(uptye);
		
		GregorianCalendar cal5 = new GregorianCalendar();
		cal5.add(Calendar.DATE, -4);
		upload5.setUploadedDateTime(cal5.getTime());
		
		em.persist(upload1);
		em.persist(upload2);
		em.persist(upload3);
		em.persist(upload4);
		em.persist(upload5);
		em.flush();

		tobeDeleted.add(upload1);
		tobeDeleted.add(upload2);
		tobeDeleted.add(upload3);
		tobeDeleted.add(upload4);
		tobeDeleted.add(upload5);
		
*/		
		List<Upload> uploads = uploadRepository.getLast3DaysUploads("guru", cal4.getTime(), "SALES_BOOKING REPORT");
		assertNotNull(uploads);
		
		System.out.println(".............Uploads Size.........:"+uploads.size());
		
		//assertTrue(uploads.size() == 4);

	
		
	}
	
	@Test
	public void testGetAllUploadsForType(){
		GregorianCalendar cal4 = new GregorianCalendar();
		Upload upload1 = new Upload();
		upload1.setNumberDiscarded(0);
		upload1.setProcessed(false);
		WyzUser user = userRepository.getUser(USER_NAME);
		assertNotNull(user);
		upload1.setUploadedBy(user);
		UploadType uptye = uploadRepository.getUploadType(UPLOAD_TYPE);
		upload1.setUploadType(uptye);
		
		GregorianCalendar cal = new GregorianCalendar();
		cal.add(Calendar.DATE, -3);
		upload1.setUploadedDateTime(cal.getTime());
		

		Upload upload2 = new Upload();
		upload2.setNumberDiscarded(0);
		upload2.setProcessed(false);
		upload2.setUploadedBy(user);
		upload2.setUploadType(uptye);
		
		GregorianCalendar cal2 = new GregorianCalendar();
		cal2.add(Calendar.DATE, -2);
		upload2.setUploadedDateTime(cal2.getTime());

		Upload upload3 = new Upload();
		upload3.setNumberDiscarded(0);
		upload3.setProcessed(false);
		upload3.setUploadedBy(user);
		upload3.setUploadType(uptye);
		
		GregorianCalendar cal3 = new GregorianCalendar();
		cal3.add(Calendar.DATE, -1);
		upload3.setUploadedDateTime(cal3.getTime());

		Upload upload4 = new Upload();
		upload4.setNumberDiscarded(0);
		upload4.setProcessed(false);
		upload4.setUploadedBy(user);
		upload4.setUploadType(uptye);
		

		upload4.setUploadedDateTime(cal4.getTime());
		
		Upload upload5 = new Upload();
		upload5.setNumberDiscarded(0);
		upload5.setProcessed(false);
		upload5.setUploadedBy(user);
		upload5.setUploadType(uptye);
		
		GregorianCalendar cal5 = new GregorianCalendar();
		cal5.add(Calendar.DATE, -4);
		upload5.setUploadedDateTime(cal5.getTime());
		
		em.persist(upload1);
		em.persist(upload2);
		em.persist(upload3);
		em.persist(upload4);
		em.persist(upload5);
		em.flush();

		tobeDeleted.add(upload1);
		tobeDeleted.add(upload2);
		tobeDeleted.add(upload3);
		tobeDeleted.add(upload4);
		tobeDeleted.add(upload5);
		
		List<Upload> uploads= uploadRepository.getAllUploads(UPLOAD_TYPE);
		assertNotNull(uploads);
		assertTrue(uploads.size()>=5);
	}
	
	@Test
	public void testGetUploadTypesAll(){
		List<UploadType> ulist = uploadRepository.getUploadTypes();
		
		assertTrue(ulist.size() > 0);
		
		UploadType utype = ulist.get(0);
		
		assertNotNull(utype);
		
	}
	
	
	@After
	public void tearDownData() {

		tobeDeleted.forEach((object) -> {

			em.remove(object);
			em.flush();

		});

	}

}
