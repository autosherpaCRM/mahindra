package wyzcrmTestCode;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import configs.JinqSource;
import models.Role;
import models.Tenant;
import models.WyzUser;
import repositories.WyzUserRepository;

public class WyzUserRepositoryTest extends AbstractIntegrationTest {
	
	public static String USER_NAME = "MyAppTestUser";
	public static String PASSWORD = "MyAppPass";
	public static String USER_PHONE ="9999999999";
	public static String TENANT_CODE = "TEST1";
	public static String USER_ROLE = "GENERICUSER";

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	private List<Object> tobeDeleted;

	@Autowired
	private WyzUserRepository userRepository;

	@Before
	public void setupData() {

		Tenant tenant = new Tenant();
		tenant.setTenantCode(TENANT_CODE);
		tenant.setTenantPhone("12344");
		tenant.setTenentAddress("test address");
		em.persist(tenant);

		em.flush();
		Long tenantid = tenant.getId();

		tobeDeleted = new ArrayList();
		tobeDeleted.add(tenant);

		Role role = new Role();
		role.setRole(USER_ROLE);

		em.persist(role);
		em.flush();
		Long roleid = role.getId();
		tobeDeleted.add(role);

		WyzUser user = new WyzUser();
		user.setFirstName("TestF");
		user.setLastName("TestL");
		user.setUserName(USER_NAME);
		user.setPassword(PASSWORD);
		user.setTenant(tenant);
		List roleList = new ArrayList();
		roleList.add(role);
		user.setRoles(roleList);
		em.persist(user);
		em.flush();
		Long userid = user.getId();

		
		tobeDeleted.add(user);

	}
	@Test
	public void testAuthentication() {

		boolean result = userRepository.authenticateUser(USER_NAME, PASSWORD);

		assertTrue(result);
	}

	@Test
	public void testGetRolesForAUser() {

		List<Role> roles = userRepository.getUserRoles(USER_NAME);

		assertTrue(roles.size() > 0);
		assertTrue(roles.get(0).getRole().equals(USER_ROLE));
	}
	
	@Test
	public void testObtainTenantFromUser(){
		
		Tenant tenant = userRepository.getUserTenant(USER_NAME);
		assertNotNull(tenant);
		
		assertTrue(tenant.getTenantCode().equals(TENANT_CODE));
		
		WyzUser user = userRepository.getUser(USER_NAME);
		
		assertNotNull(user);
		long userid = user.getId();
		
		Tenant tenant2 = userRepository.getUserTenant(userid);
		assertNotNull(tenant2);
		assertTrue(tenant2.getTenantCode().equals(TENANT_CODE));
		
	}
	
	@After
	public void tearDownData() {

		tobeDeleted.forEach((object) -> {

			em.remove(object);
			em.flush();

		});

	}

}
