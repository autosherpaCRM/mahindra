package wyzcrmTestCode;

import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import configs.JinqSource;
import models.AssignedInteraction;
import models.Role;
import models.Tenant;
import models.WyzUser;
import controllers.webmodels.CallLogDispositionLoad;
import play.Logger.ALogger;
import repositories.CallInteractionsRepository;
import repositories.WyzUserRepository;
import repositories.SearchRepository;

public class TestFiltersForCallLogs extends AbstractIntegrationTest{


	public static String TENANT_CODE = "TEST1";
	public static String USER_ROLE = "GENERICUSER";
	
//	instartdate 
//	inenddate  
//	incampaignname
//	induetype 
//	pattern 
//	wyzuserid 
//	dispositiontype 
//	start_with 
//	length 
//	orderDirection 
//	
	
	public static String FROM_DATE = "2016-08-20";
	public static String TO_DATE ="2018-08-20";
	public static String CAMPAIGN_NAME = "Service Reminder";
	public static String SERVICE_TYPE_ID = "";
	public static String SEARCH_PATTERN ="";
	public static long ID = 3;
	public static int TYPE_OF_DISPO = 4;
	public static long FROM_INDEX = 0;
	public static long TO_INDEX = 100;
	public static int ORDER_DIRECTION = 1;
	
	
	
	ALogger logger = play.Logger.of("application");
	

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	private List<Object> tobeDeleted;

	@Autowired
	private SearchRepository searchRepo;
	
	@Autowired
	private WyzUserRepository userRepository;
	
	@Before
	public void setupData() {


	} 
	
	@Test
	public void testFilterForCallLogs(){
		List<CallLogDispositionLoad> finaldata = searchRepo.getDispositionListOfUser(FROM_DATE,TO_DATE,CAMPAIGN_NAME,SERVICE_TYPE_ID,SEARCH_PATTERN,ID,TYPE_OF_DISPO,FROM_INDEX,TO_INDEX,ORDER_DIRECTION);
		assertTrue(finaldata != null);
		assertTrue(finaldata.size());
	}
	
	
	
}
