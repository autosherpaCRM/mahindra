name := "wyzcrmMulti"

version := "2.0"

lazy val root = (project in file(".")).enablePlugins(PlayJava,LauncherJarPlugin)

scalaVersion := "2.11.7"

javaOptions ++= Seq("-Xmx2048M", "-Xms512M", "-XX:MaxMetaspaceSize=256M")


libraryDependencies ++= Seq(
  filters,
  javaCore,
  javaJdbc,
  cache,
  javaWs % "test",
  "javax.inject" % "javax.inject" % "1",
  "org.jinq" % "jinq-jpa" % "1.8.4",
  "com.h2database" % "h2" % "1.4.188",
 "org.springframework" % "spring-context" % "4.3.7.RELEASE",
  "org.springframework" % "spring-webmvc" % "4.3.7.RELEASE",
  "org.springframework" % "spring-orm" % "4.3.7.RELEASE",
  "org.springframework" % "spring-jdbc" % "4.3.7.RELEASE",
  "org.springframework" % "spring-tx" % "4.3.7.RELEASE",
  "org.springframework" % "spring-expression" % "4.3.7.RELEASE",
  "org.springframework" % "spring-aop" % "4.3.7.RELEASE",
  "org.springframework" % "spring-test" % "4.3.7.RELEASE" % "test",  
  "org.hibernate" % "hibernate-entitymanager" % "5.2.9.Final",
  "org.hibernate" % "hibernate-c3p0" % "5.2.9.Final",
  "dom4j" % "dom4j" % "1.6.1",
  "net.sf.supercsv" % "super-csv" % "2.4.0",
  "net.sf.supercsv" % "super-csv-joda" % "2.3.1",
  "mysql" % "mysql-connector-java" % "5.1.36",
  "org.apache.commons" % "commons-io" % "1.3.2",
  "commons-beanutils" % "commons-beanutils" % "1.9.2",
  "org.pac4j" % "play-pac4j" % "3.0.0-RC2-SNAPSHOT",
  "org.pac4j" % "pac4j-http" % "2.0.0-RC2-SNAPSHOT",
  "org.pac4j" % "pac4j-sql" % "2.0.0-RC2-SNAPSHOT",
  "org.springframework" % "spring-web" % "3.0.2.RELEASE",
  "com.firebase" % "firebase-token-generator" % "2.0.0",
  "commons-dbcp" % "commons-dbcp" % "1.4",
  "org.json" % "json" % "20160212",
  "commons-io" % "commons-io" % "2.4",   
  "com.google.firebase" % "firebase-server-sdk" % "3.0.3",
  "org.apache.poi" % "poi" % "3.15",
  "org.apache.poi" % "poi-ooxml" % "3.15",
  "org.apache.poi" % "poi-ooxml-schemas" % "3.15",
  "org.apache.metamodel" % "MetaModel-full" % "4.6.0",
  "c3p0" % "c3p0" % "0.9.1.2",
  "com.mchange" % "mchange-commons-java" % "0.2.11",
  "org.apache.httpcomponents" % "httpclient" % "4.5.3",
  "be.objectify" %% "deadbolt-java" % "2.5.1"
  )

resolvers += 
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

routesGenerator := InjectedRoutesGenerator


fork in run := true



fork in run := true